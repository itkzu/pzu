<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('../assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: none;
    }
    .radio {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    
    .checkbox label, .radio label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 5px;
        font-weight: bold;
        cursor: pointer;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'target' => '_blank'
                    , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes); 
            ?> 

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-submit"> <i class="fa fa-save"></i> Simpan </button> 
                                    <button type="button" class="btn btn-default btn-batal"> <i class="fa fa-step-backward"></i> Batal </button>    
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <!-- <button type="button" class="btn btn-primary btn-imp"> <i class="fa fa-download"></i> Tarik Assist </button>     -->
                                </div>
                            </div>
                            <div class="col-md-12"> 
                                <div style="border-top: 2px solid #ddd; height: 10px;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php  
                                        echo form_label(' Pilih No SO dari ASTRA <small> (No SO / Nama Pelanggan)</small>'); 
                                        echo form_dropdown($form['notrm']['name'],$form['notrm']['data'] ,$form['notrm']['value'] ,$form['notrm']['attr']);
                                        echo form_error('notrm','<div class="note">','</div>'); 
                                     ?>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 cab">
                        <div class="row">
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['bag_kdsup']['placeholder']); 
                                     ?> 
                            </div> 
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php   
                                        echo form_dropdown($form['bag_kdsup']['name'],$form['bag_kdsup']['data'] ,$form['bag_kdsup']['value'] ,$form['bag_kdsup']['attr']);
                                        echo form_error('bag_kdsup','<div class="note">','</div>'); 
                                     ?>
                                </div>
                            </div>  
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['bag_alamat']['placeholder']); 
                                     ?> 
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php   
                                        echo form_input($form['bag_alamat']);
                                        echo form_error('bag_alamat','<div class="note">','</div>');
                                     ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12 pel">
                        <div class="row">
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['nama']['placeholder']); 
                                     ?> 
                            </div>  
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php   
                                        echo form_input($form['nama']);
                                        echo form_error('nama','<div class="note">','</div>');
                                     ?>
                                </div>
                            </div> 
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['alamat']['placeholder']); 
                                     ?> 
                            </div> 
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php   
                                        echo form_input($form['alamat']);
                                        echo form_error('alamat','<div class="note">','</div>');
                                     ?>
                                </div>
                            </div> 
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['nik']['placeholder']); 
                                     ?> 
                            </div> 
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php   
                                        echo form_input($form['nik']);
                                        echo form_error('nik','<div class="note">','</div>');
                                     ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['tipebayar']['placeholder']); 
                                     ?> 
                            </div> 
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php   
                                        echo form_dropdown($form['tipebayar']['name'],$form['tipebayar']['data'] ,$form['tipebayar']['value'] ,$form['tipebayar']['attr']);
                                        echo form_error('tipebayar','<div class="note">','</div>'); 
                                     ?>
                                </div>
                            </div> 
                            <div class="col-md-2"> 
                                    <?php  
                                        echo form_label($form['tglso']['placeholder']); 
                                     ?> 
                            </div> 
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php   
                                        echo form_input($form['tglso']);
                                        echo form_error('tglso','<div class="note">','</div>');
                                     ?>
                                </div>
                            </div> 
                            <div class="col-md-12"> 
                                <div style="border-top: 2px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12"> 
                                <button type="button" class="btn btn-primary btn-add"> <i class="fa fa-plus"></i> Tambah </button> 
                            </div>  
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 1%;text-align: center;">No</th>
                                        <th style="text-align: center;">Kode Sparepart</th>
                                        <th style="text-align: center;">Qty</th>
                                        <th style="text-align: center;">Harga Beli</th>
                                        <th style="text-align: center;">Discount</th>
                                        <th style="text-align: center;">Total</th>
                                        <th style="text-align: center;">Edit</th> 
                                        <th style="text-align: center;">Hapus</th> 
                                    </tr>
                                </thead>
                                <tbody></tbody> 
                                <tfoot>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: right;"></th>
                                        <th style="text-align: right;"></th> 
                                        <th style="text-align: right;"></th> 
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body --> 
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- modal dialog -->
<div id="modal_edit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Jurnal</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">                                       

                    <div class="col-md-5">
                        <div class="row">

                            <!-- <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['m_nofa']['placeholder']);
                                            echo form_input($form['m_nofa']);
                                            echo form_error('m_nofa','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div>  -->

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['kdpart']['placeholder']);
                                            echo form_input($form['kdpart']);
                                            echo form_error('kdpart','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nmpart']['placeholder']);
                                            echo form_input($form['nmpart']);
                                            echo form_error('nmpart','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['qty']['placeholder']);
                                            echo form_input($form['qty']);
                                            echo form_error('qty','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>                                    

                    <div class="col-md-1">
                        <div class="row"> 
                        </div>
                    </div>                                   

                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['harga']['placeholder']);
                                            echo form_input($form['harga']);
                                            echo form_error('harga','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['discper']['placeholder']);
                                            echo form_input($form['discper']);
                                            echo form_error('discper','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['disc']['placeholder']);
                                            echo form_input($form['disc']);
                                            echo form_error('disc','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['total']['placeholder']);
                                            echo form_input($form['total']);
                                            echo form_error('total','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                  

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button> 
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
</div>

<!-- modal dialog -->
<div id="modal_add" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalformadd" name="modalformadd" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Jurnal</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">                                       

                    <div class="col-md-5">
                        <div class="row"> 

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_kdpart']['placeholder']); 
                                            echo form_dropdown($form['add_kdpart']['name'],$form['add_kdpart']['data'] ,$form['add_kdpart']['value'] ,$form['add_kdpart']['attr']);
                                            echo form_error('add_kdpart','<div class="note">','</div>'); 
                                          ?>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_nmpart']['placeholder']);
                                            echo form_input($form['add_nmpart']);
                                            echo form_error('add_nmpart','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_qty']['placeholder']);
                                            echo form_input($form['add_qty']);
                                            echo form_error('add_qty','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                    

                    <div class="col-md-1">
                        <div class="row"> 
                        </div>
                    </div>                                   

                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_harga']['placeholder']);
                                            echo form_input($form['add_harga']);
                                            echo form_error('add_harga','<div class="note">','</div>');
                                          ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_discper']['placeholder']);
                                            echo form_input($form['add_discper']);
                                            echo form_error('add_discper','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_disc']['placeholder']);
                                            echo form_input($form['add_disc']);
                                            echo form_error('add_disc','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['add_total']['placeholder']);
                                            echo form_input($form['add_total']);
                                            echo form_error('add_total','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                  

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan2">Simpan</button> 
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>




<script type="text/javascript">
    $(document).ready(function () {
        $('.cab').show();
        $('.pel').hide();
        $('#load').hide();
        $("#notrm").val('');  
        $("#notrm").select2({});   
        var kdsup = $('#bag_kdsup').val(); 
        set_bagal(kdsup);
        // set_bagal($("#bag_alamat").val());
        $("#add_kdpart").select2({ width: '100%' }); 
        get_table();
        $('#harga').autoNumeric('init');
        $('#disc').autoNumeric('init');
        $("#discper").autoNumeric('init', {vMin: '0', vMax: '100' });
        $('#total').autoNumeric('init');
        $('#add_harga').autoNumeric('init');
        $('#add_disc').autoNumeric('init');
        $("#add_discper").autoNumeric('init', {vMin: '0', vMax: '100' });
        $('#add_total').autoNumeric('init');

        if($("#notrm").val()===''){
            $(".btn-add").show();
        } else {
            $(".btn-add").hide();
        }

        //keyup
        $('#harga').keyup(function() {
            var qty     = $('#qty').val();
            var harga   = $('#harga').autoNumeric('get');
            var discper = $('#discper').autoNumeric('get');
            var disc   = harga * (discper/100);
            $('#disc').autoNumeric('set',disc);
            var total   = (harga * qty) - (disc * qty);
            $('#total').autoNumeric('set',total);
        });
        
        $('#discper').keyup(function() {
            var qty     = $('#qty').val();
            var harga   = $('#harga').autoNumeric('get');
            var discper = $('#discper').autoNumeric('get');
            var disc   = harga * (discper/100);
            $('#disc').autoNumeric('set',disc);
            var total   = (harga * qty) - disc;
            $('#total').autoNumeric('set',total);
        }); 
        
        $('#disc').keyup(function() {
            var qty     = $('#qty').val();
            var harga   = $('#harga').autoNumeric('get');
            var disc = $('#disc').autoNumeric('get');
            var discper   = Math.round(disc/harga  * 100);
            $('#discper').autoNumeric('set',discper);
            var total   = (harga * qty) - disc;
            $('#total').autoNumeric('set',total);
        }); 

        $('#add_qty').keyup(function() {
            var qty     = $('#add_qty').val();
            var harga   = $('#add_harga').autoNumeric('get');
            var discper = $('#add_discper').autoNumeric('get');
            var disc   = harga * (discper/100);
            $('#add_disc').autoNumeric('set',disc);
            var total   = (harga * qty) - (disc * qty);
            $('#add_total').autoNumeric('set',total);
        });

        $('#add_harga').keyup(function() {
            var qty     = $('#add_qty').val();
            var harga   = $('#add_harga').autoNumeric('get');
            var discper = $('#add_discper').autoNumeric('get');
            var disc   = harga * (discper/100);
            $('#add_disc').autoNumeric('set',disc);
            var total   = (harga * qty) - (disc * qty);
            $('#add_total').autoNumeric('set',total);
        });
        
        $('#add_discper').keyup(function() {
            var qty     = $('#add_qty').val();
            var harga   = $('#add_harga').autoNumeric('get');
            var discper = $('#add_discper').autoNumeric('get');
            var disc   = harga * (discper/100);
            $('#add_disc').autoNumeric('set',disc);
            var total   = (harga * qty) - (disc * qty);
            $('#add_total').autoNumeric('set',total);
        }); 
        
        $('#add_disc').keyup(function() {
            var qty     = $('#add_qty').val();
            var harga   = $('#add_harga').autoNumeric('get');
            var disc = $('#add_disc').autoNumeric('get');
            var discper   = Math.round(disc/harga * 100);
            $('#add_discper').autoNumeric('set',discper);
            var total   = (harga * qty) - (disc * qty);
            $('#add_total').autoNumeric('set',total);  
        }); 
        
        //change
        $('#notrm').change(function() { 
            var notrm = $('#notrm').val(); 
            get_table();
            if(notrm===''){ 
                $(".btn-add").show();
                $('.cab').show();
                $('.pel').hide();
            } else { 
                $(".btn-add").hide();
                $('.cab').hide();
                $('.pel').show();
                set_nmpel();
            }
        }); 

        $('#add_kdpart').change(function() { 
            var kdpart = $('#add_kdpart').val(); 
            set_nmpart(kdpart);
        }); 

        $('#bag_kdsup').change(function() { 
            var kdsup = $('#bag_kdsup').val(); 
            set_bagal(kdsup);
        }); 

        //button
        $('.btn-imp').click(function(){ 

            var date7 = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-01 00:00:00');
            var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD 23:00:00'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            var kddiv       = 'SHOWROOM'; 
            get_PO(secret_key,api_key,req_time,rdate1,rdate7,kddiv); 
            // alert(rdate1);
        });

        $(".btn-add").click(function(){
            add();
        });

        $(".btn-simpan").click(function(){
            update();
        });

        $(".btn-simpan2").click(function(){
            det_update();
        });

        $(".btn-batal").click(function(){
            batal();
        });

        $(".btn-submit").click(function(){
            submit();
        });
    });

    //PO
    function get_PO(secret_key,api_key,req_time,rdate1,rdate7,kddiv){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('partsales/get_PO'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7, "kddiv" : kddiv },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                console.log(secret_key,api_key,req_time,rdate1,rdate7,kddiv);
                ins_PO(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_PO(resp){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('partsales/ins_PO'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){ 
                    save_PO(); 
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_PO(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('partsales/save_PO'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        // tbl_spk.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    //table
    function get_table(){ 
        var notrm = $('#notrm').val();
        
        var column = [];
 

        column.push({
            "aTargets": [ 3,4,5 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });


        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"  },
                { "data": "kodepart"   },
                { "data": "qty"  },
                { "data": "harga" },
                { "data": "discount"  },
                { "data": "total"},
                { "data": "edit"},
                { "data": "hapus"},
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "notrm", "value": notrm});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){

            },
            "sAjaxSource": "<?=site_url('partsales/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;


                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var pokok = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 ); 
                // Update footer by showing the total with the reference of the column index
                $( api.column( 4 ).footer() ).html('Total');
                $( api.column( 5 ).footer() ).html(numeral(pokok).format('0,0.00')); 
            },
            buttons: [
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                }
            ],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        }); 
    }

    //set
    function set_nmpart(materialno){  
        $.ajax({
            type: "POST",
            url: "<?=site_url('partsales/set_nmpart');?>",
            data: {"materialno":materialno},
            success: function(resp){ 
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, data){
                    $("#nmpart").val(data.nmpart); 
                    $("#add_nmpart").val(data.nmpart); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_bagal(kdsup){  
        $.ajax({
            type: "POST",
            url: "<?=site_url('partsales/set_bagal');?>",
            data: {"kdsup":kdsup},
            success: function(resp){ 
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, data){
                    $("#bag_alamat").val(data.alamat); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_nmpel(){  
        var notrm = $('#notrm').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('partsales/set_nmpel');?>",
            data: {"notrm":notrm},
            success: function(resp){ 
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, data){
                    $("#nama").val(data.namacustomer); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    //aksi
    function edit(nofaktur,materialno,qtypo,hargabeli,nilaidiscount,totalharga,uangbayar,kdgrup) { 
        $('#modal_edit').modal('toggle');
        $('#nofaktur').val(nofaktur);
        $('#kdpart').val(materialno);
        $('#qty').val(qtypo);
        $('#harga').autoNumeric('set',hargabeli);
        $('#discper').autoNumeric('set',0);
        $('#disc').autoNumeric('set',nilaidiscount);
        $('#total').autoNumeric('set',totalharga);
        set_nmpart(materialno);
    }

    function add(){ 
        $('#modal_add').modal('toggle'); 
        $('#add_kdpart').val('');
        $('#add_nmpart').val('');
        $('#add_qty').val('');
        $('#add_harga').autoNumeric('set',0);
        $('#add_discper').autoNumeric('set',0);
        $('#add_disc').autoNumeric('set',0);
        $('#add_total').autoNumeric('set',0);
    }

    function del(noref,kdpart) { 
        var notrm = 'DEFAULT';
        if($('#notrm').val()===''){
            $.ajax({
                type: "POST",
                url: "<?=site_url("partsales/del");?>",
                data: {"notrm":notrm,"kdpart":kdpart },
                success: function(resp){
                    get_table(); 
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        } else {
            swal({
                title: 'Tidak bisa dihapus',
                text: 'Tidak dapat dihapus karena data dari Astra',
                type: 'warning'
            }); 
        }
    }

    function update(){
        var notrm   = $('#notrm').val().toUpperCase();
        if(notrm===''){
            notrm = 'DEFAULT';
        } else {
            notrm = notrm;
        }
        var kdpart  = $('#kdpart').val().toUpperCase();
        var qty     = $('#qty').val();
        var harga   = $('#harga').autoNumeric('get');
        var disc    = $('#disc').autoNumeric('get');
        var total   = $('#total').autoNumeric('get');
        $.ajax({
            type: "POST",
            url: "<?=site_url('partsales/update');?>",
            data: {"notrm"      :notrm
                    ,"kdpart"   :kdpart
                    ,"qty"      :qty
                    ,"harga"    :harga
                    ,"disc"     :disc
                    ,"total"    :total},
            success: function(resp){ 
                $("#modal_edit").modal("hide");
                get_table();  
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function batal(){
        var notrm = $("#notrm").val(); 
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("partsales/batal");?>",
                    data: {"notrm":notrm },
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            },function () {
                                window.location.href = '<?=site_url('partsales');?>';
                            }); 
                        });
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });

            }); 
    }

    function det_update(){  
        var kdpart = $("#add_kdpart").val().toUpperCase();
        var qty = $("#add_qty").val();
        var kdgrup = $("#grup").val();
        var harga = $("#add_harga").autoNumeric('get');
        var disc = $("#add_disc").autoNumeric('get');
        var total = $("#add_total").autoNumeric('get');
        $.ajax({
            type: "POST",
            url: "<?=site_url("partsales/det_update");?>",
            data: {"kdpart":kdpart
                    ,"qty":qty
                    ,"kdgrup":kdgrup
                    ,"harga":harga
                    ,"disc":disc
                    ,"total":total },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    $("#modal_add").modal("hide");
                    get_table();
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var notrm = $("#notrm").val().toUpperCase();
            if(notrm===''){
                notrm   = 'DEFAULT';
                var alamat  = '';
                var nik     = '';
            } else {
                notrm = notrm;
                var alamat  = $("#alamat").val().toUpperCase();
                var nik     = $("#nik").val().toUpperCase();
            }
            var kdsup = $("#bag_kdsup").val().toUpperCase();
            var tipebayar = $("#tipebayar").val();
            var tglso = $("#tglso").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("partsales/submit");?>",
                data: {"notrm":notrm
                        ,"kdsup":kdsup
                        ,"tipebayar":tipebayar
                        ,"tglso":tglso
                        ,"alamat":alamat
                        ,"nik":nik },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        },function () {
                            if (data.tipe==='success'){
                                if($("#notrm").val()===''){
                                    window.open('html2/'+data.noso, '_blank');   
                                } else { 
                                    window.open('html/'+data.noso, '_blank');    
                                }
                                
                                window.location.href = '<?=site_url('partsales');?>';

                            }
                        });
                        
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

</script>

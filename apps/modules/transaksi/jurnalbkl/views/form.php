<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row"> 

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tgljurnal']['placeholder']);
                                            echo form_input($form['tgljurnal']);
                                            echo form_error('tgljurnal','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['noref']['placeholder']);
                                            echo form_input($form['noref']);
                                            echo form_error('noref','<div class="note">','</div>');
                                        ?>
                                     </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_input($form['nojurnal']);
                                            echo form_error('nojurnal','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['trans']['placeholder']);
                                            echo form_input($form['trans']);
                                            echo form_error('trans','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=form_label($form['kddiv']['placeholder']);?>
                                        <div class="input-group">
                                            <?php
                                                echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                                echo form_error('kddiv', '<div class="note">', '</div>');
                                            ?>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info btn-set" id="btn-set">Set</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php
                                                echo form_input($form['nourut']);
                                                echo form_error('nourut', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-add">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">Kode Akun</th>
                                            <th style="text-align: center;">Nama Akun</th>
                                            <th style="width: 150px;text-align: center;">Debet</th>
                                            <th style="width: 150px;text-align: center;">Kredit</th>
                                            <th style="width: 150px;text-align: center;">Keterangan</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                            <th style="width: 10px;text-align: center;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-submit">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_promo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Jurnal</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label('Pilih Kode Akun');
                                    echo form_dropdown($form['kdakun']['name'],$form['kdakun']['data'] ,$form['kdakun']['value'] ,$form['kdakun']['attr']);
                                    echo form_error('kdakun','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['dk']['placeholder']);
                                    echo form_dropdown($form['dk']['name'],$form['dk']['data'] ,$form['dk']['value'] ,$form['dk']['attr']);
                                    echo form_error('dk','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['nominal']['placeholder']);
                                    echo form_input($form['nominal']);
                                    echo form_error('nominal','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['ket']['placeholder']);
                                    echo form_input($form['ket']);
                                    echo form_error('ket','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                        <button type="button" class="btn btn-success btn-elevate btn-update">Update</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#nojurnal').hide();
        if($('#nojurnal').val()===''){
            $('.btn-add').hide();
            enabled();
        } else {
            var no = $('#nojurnal').val();
            nojurnal = no.substring(4,13);
            // alert(nojurnal);
            $('#kddiv').val(nojurnal);
            // $("kddiv").trigger("chosen:updated");
            // $('#kddiv').select2({
            //           dropdownAutoWidth : true,
            //           width: '100%'
            //         });
            $('.btn-add').show();
            // tbl();
            getKodeAkun();
            getData();
            disabled();
        }
        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                noref : "required",
                ket   : "required"
            },
            messages : {
                noref : "Masukkan Nomor Referensi Jurnal",
                ket : "Masukkan Keterangan Jurnal"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdakun  : "required",
                nominal : "required"
            },
            messages : {
                kdakun  : "Masukkan Kode Akun",
                nominal : "Masukkan Nominal"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        
        $("#nominal").val('');
        $('#nominal').autoNumeric();
 
        $('.btn-set').click(function(){
            // $('.btn-add').hide();
            $('.btn-add').show();
            // tbl();
            getData();
            getKodeAkun();
            disabled();
        });

        $('.btn-add').click(function(){
            validator.resetForm();
          	$('#modal_promo').modal('toggle');
            $('.btn-simpan').show();
          	$('.btn-update').hide();
            $('#kdakun').select2({
                placeholder: '-- Pilih Kode Akun --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            clear();
        });

        $('.btn-simpan').click(function(){
            if ($("#modalform").valid()) {
                addDetail();
            }
        });

        $('.btn-update').click(function(){
            if ($("#modalform").valid()) {
                UpdateDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){
            if ($("#myform").valid()) {
              submit();
            }
        });
    });



    /*
    function myFunction() {
        var x = document.getElementById("noref");
        var y = document.getElementById("ket");
        x.value = x.value.toUpperCase();
        y.value = y.value.toUpperCase();
    }
    */

    function tbl(){
        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 2,3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 4,5 ],
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "kdakun"  },
                { "data": "nmakun" },
                { "data": "debet"   },
                { "data": "kredit"  },
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nojurnal", "value": $("#nojurnal").val('')});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('jurnalbkl/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var debet = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var kredit = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 1 ).footer() ).html('Total');
                $( api.column( 2 ).footer() ).html(numeral(debet).format('0,0.00'));
                $( api.column( 3 ).footer() ).html(numeral(kredit).format('0,0.00'));
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    }

    function clear(){
        $("#kdakun").val('');
        $("#nominal").val('');
        $("#ket").val('');
    }

    function disabled(){
        $('.btn-set').hide();
        $('#kddiv').attr('disabled',true);
        $(".btn-add").attr('disabled',false);
    }

    function enabled(){
        $('.btn-set').show();
        $(".btn-add").attr('disabled',true);
    }

    function getData(){

        var column = [];

        column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });

        column.push({
            "aTargets": [ 2,3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
              },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 4,5 ],
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "kdakun"  },
                { "data": "nmakun" },
                { "data": "debet"   },
                { "data": "kredit"  },
                { "data": "ket"  },
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nojurnal", "value": $("#nojurnal").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('jurnalbkl/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var debet = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var kredit = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 1 ).footer() ).html('Total');
                $( api.column( 2 ).footer() ).html(numeral(debet).format('0,0.00'));
                $( api.column( 3 ).footer() ).html(numeral(kredit).format('0,0.00'));
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    }

    function getKodeAkun(){
        var kddiv = $("#kddiv").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("jurnalbkl/getKodeAkun");?>",
            data: {"kddiv":kddiv},
            beforeSend: function() {
                $('#kdakun').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kode Akun --'));
                $("#kdakun").trigger("change.chosen");
                if ($('#kdakun').hasClass("chosen-hidden-accessible")) {
                    $('#kdakun').select2('destroy');
                    $("#kdakun").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdakun')
                        .append($('<option>', { value : value.kdakun })
                        .html("<b style='font-size: 14px;'>" + value.kdakun + " - " + value.nmakun + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.kdakun + " - " + repo.nmakun + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function edit(nourut,kddiv,kdakun,nmakun,dk,nominal,ket){
        $("#nourut").val(nourut);
        $("#kdakun").val(kdakun);
        //$("#kdakun").val(nmakun);
        $("kdakun").trigger("chosen:updated");
        $('#kdakun').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });

        $("#dk").val(dk);
        $("#nominal").autoNumeric('set',nominal);
        $("#ket").val(ket);

        $('#modal_promo').modal('toggle');
        $('.btn-simpan').hide();
        $('.btn-update').show();
    }

    function addDetail(kdakun,dk,nominal){

        var nojurnal = $("#nojurnal").val();
        var tgljurnal = $("#tgljurnal").val();
        var kddiv = $("#kddiv").val();
        var kdakun = $("#kdakun").val();
        var dk = $("#dk").val();
        var nominal = $("#nominal").autoNumeric('get');
        var ket = $("#ket").val().toUpperCase();

        $.ajax({
            type: "POST",
            url: "<?=site_url("jurnalbkl/addDetail");?>",
            data: {"kdakun":kdakun
                    ,"nojurnal":nojurnal
                    ,"tgljurnal":tgljurnal
                    ,"kddiv":kddiv
                    ,"dk":dk
                    ,"nominal":nominal
                    ,"ket":ket  },
            success: function(resp){ 

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    if (data.tipe==="success"){
                         
                        $("#nojurnal").val(data.nojurnal);
                        $(".modal").modal("hide");
                        disabled();
                        refresh();
                        clear(); 
                    }else{
                        //refresh();
                    }

                    
                });

            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function UpdateDetail(nourut,kddiv,kdakun,dk,nominal,ket){

        var nojurnal = $("#nojurnal").val();
        var nourut = $("#nourut").val();
        var kddiv = $("#kddiv").val();
        var kdakun = $("#kdakun").val();
        var dk = $("#dk").val();
        var nominal = $("#nominal").autoNumeric('get');
        var ket = $("#ket").val().toUpperCase();

        $.ajax({
            type: "POST",
            url: "<?=site_url("jurnalbkl/UpdateDetail");?>",
            data: {"nourut":nourut
                    ,"nojurnal":nojurnal
                    ,"kdakun":kdakun
                    ,"kddiv":kddiv
                    ,"dk":dk
                    ,"nominal":nominal
                    ,"ket":ket  },
            success: function(resp){
                disabled();
                $(".modal").modal("hide");
                refresh();
                clear();

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    if (data.tipe==="success"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            //refresh();
                        });
                    }else{
                        //refresh();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(nourut){
        var nojurnal = $('#nojurnal').val();
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("jurnalbkl/detaildeleted");?>",
                data: {"nojurnal":nojurnal ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("jurnalbkl/batal");?>",
                data: {},
                success: function(resp){
                    var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                        //  localStorage.removeItem("refkb_m");
                          window.location.href = '<?=site_url('jurnalbkl');?>';
                      });
                    });
                  },
                  error:function(event, textStatus, errorThrown) {
                      swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  }
            });
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nojurnal = $("#nojurnal").val();
            var tgljurnal = $("#tgljurnal").val();
            var noref = $("#noref").val().toUpperCase();
            var trans = $("#trans").val().toUpperCase();
            var kddiv = $("#kddiv").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("jurnalbkl/submit");?>",
                data: {"nojurnal":nojurnal
                    ,"tgljurnal":tgljurnal
                    ,"noref":noref
                    ,"trans":trans
                    ,"kddiv":kddiv  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('jurnalbkl');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

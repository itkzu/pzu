<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Piutangmx_qry
 *
 * @author adi
 */
class Piutangmx_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    } 

    public function set_nokb() {
        $nokb = $this->input->post('nokb');  
        $query = $this->db->query("select * from pzu.vm_matriks where nokb =  '".$nokb."'"); 
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    } 

    public function set_do() {
        $nodo = $this->input->post('nodo');  
        $query = $this->db->query("select * FROM pzu.vl_ar_matriks WHERE nodo = '".$nodo."'"); 
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    } 

    public function select_data() {
        $nokb = $this->uri->segment(3);  
        $this->db->select('*');
        $this->db->where('nokb',$nokb);
        $q = $this->db->get("pzu.vm_matriks");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    } 

    function getKey(){
        $q = $this->input->post('q');
        $kdleasing = $this->input->post('kdleasing');
        $this->db->where("kdleasing",$kdleasing);
        $this->db->where("byr_ar<nilai_ar");
        $this->db->where("(LOWER(nodo) like '%".strtolower($q)."%' OR LOWER(nama_s) like '%".strtolower($q)."%' OR LOWER(nama) like '%".strtolower($q)."%' OR LOWER(nama_p) like '%".strtolower($q)."%')");  
        $this->db->order_by("case
                                when LOWER(nodo) like '".strtolower($q)."' then 1
                                when LOWER(nodo) like '".strtolower($q)."%' then 2
                                when LOWER(nodo) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nodo");
        $query = $this->db->get('pzu.vl_ar_matriks');
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['nodo'],
                    'text' => $value['nama'],
                    'alamat' => $value['alamat'],
                    'nama_p' => $value['nama_p'],
                    'nama_s' => $value['nama_s'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama_p' => '',
                'nama_s' => '',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }

    public function json_dgview() {
        error_reporting(-1);

        $aColumns = array('nmkb',
                          'nocetak',
                          'nokb',
                          'tglkb',
                          'nmleasing',
                          'nilai',
                          'ket',
                          'no');
        $sIndexColumn = "nokb";
        $sLimit = "";  
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = "( select ''::varchar as no, nmkb, nocetak, nokb, tglkb, nmleasing, nilai, ket from pzu.vm_matriks_list) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                // echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by nokb ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            } 

            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('piutangmx/edit/'.$aRow['nokb'])."\">Edit</a>"; 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }

        $aColumns = array('no',
                          'nodo',
                          'tgldo',
                          'noso',
                          'tglso',
                          'nama_p',
                          'nama',
                          'nama_s',
                          'alamat',
                          'nilai',
                          'nokb');
	    $sIndexColumn = "nokb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select ''::varchar as no, nodo, tgldo, noso, tglso, nama_p, nama, nama_s, alamat, nilai, nokb FROM pzu.vm_matriks_d WHERE nokb = '".$nokb."' order by nodo ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }/*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nourut']==$value['nourut']){
                    $aRow['detail'][]= $value;
                }
            }*/
            // $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nokb']."','".$aRow['kode']."','".$aRow['kdtipe']."','".$aRow['nmtipe']."','".$aRow['qty']."',".$aRow['harga'].",".$aRow['nourut'].");\">Edit</button>"; 
            // $aRow['hapus'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nokb']."','".$aRow['kode']."','".$aRow['kdtipe']."','".$aRow['nmtipe']."','".$aRow['qty']."',".$aRow['harga'].",".$aRow['nourut'].");\">Edit</button>"; 

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }

    public function getdetPO() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }

        $aColumns = array('no',
                          'nodo',
                          'tgldo',
                          'noso',
                          'tglso',
                          'nama',
                          'nama_p',
                          'nama_s',
                          'alamat',
                          'nilai',
                          'nokb');
      $sIndexColumn = "nokb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select ''::varchar as no, nodo, tgldo, noso, tglso, nama, nama_s, nama_p, alamat, nilai, nokb from pzu.vm_matriks_d where nokb = '".$nokb."' order by nodo ) AS a";


        //$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
        $sWhere = "";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
          if($_GET['iDisplayStart']>0){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                intval( $_GET['iDisplayStart'] );
          }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
          $sOrder = " ORDER BY  ";
          for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
          {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
          }

          $sOrder = substr_replace( $sOrder, "", -2 );
          if ( $sOrder == " ORDER BY" )
          {
              $sOrder = "";
          }
        }

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " AND ";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
            if ( $sWhere == "" )
            {
              $sWhere = " WHERE ";
            }
            else
            {
              $sWhere .= " AND ";
            }
            //echo $sWhere."<br>";
            $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
        }


        /*
         * SQL queries
         */
        $sQuery = "
            SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";

            //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";   //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo json_encode( $output );
    }

    public function addDetail() {
        $nokb         = $this->input->post('nokb');
        $nodo         = $this->input->post('nodo');
        $nilai   = $this->input->post('nilai'); 
        $q = $this->db->query("select title,msg,tipe from pzu.mx_ins_d( '". $nokb ."',
                                                                        '". $nodo ."',
                                                                        ". $nilai .")");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    public function delDetail() {
        $nokb         = $this->input->post('nokb');
        $nodo         = $this->input->post('nodo');
        $nilai        = $this->input->post('nilai'); 
        $q = $this->db->query("select title,msg,tipe from pzu.mx_del_d( '". $nokb ."',
                                                                        '". $nodo ."',
                                                                        ". $nilai .")");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    public function batal() {
        $nokb         = $this->input->post('nokb'); 
        $q = $this->db->query("select title,msg,tipe from pzu.mx_btl( '". $nokb ."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }  

    public function submit() { 
        $nokb = $this->input->post('nokb'); 
        $selisih = $this->input->post('selisih'); 
        $q = $this->db->query("select title,msg,tipe from pzu.mx_ins( '". $nokb ."',
                                                                        ". $selisih .")");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

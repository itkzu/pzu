<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['noaksa']['placeholder']);
                                            echo form_input($form['noaksa']);
                                            echo form_error('noaksa','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglaksa']['placeholder']);
                                            echo form_input($form['tglaksa']);
                                            echo form_error('tglaksa','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['ket']['placeholder']);
                                            echo form_input($form['ket']);
                                            echo form_error('ket','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php
                                                echo form_input($form['nourut']);
                                                echo form_error('nourut', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-add">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">Nama Barang</th>
                                            <th style="width: 100px;text-align: Right;">Quantity</th>
                                            <th style="width: 150px;text-align: Right;">Total Harga</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                            <th style="width: 10px;text-align: center;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-submit">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Transaksi</h4>
                </div>

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kdaks']['placeholder']);
                                    echo form_dropdown( $form['kdaks']['name'],
                                                        $form['kdaks']['data'] ,
                                                        $form['kdaks']['value'] ,
                                                        $form['kdaks']['attr']);
                                    echo form_error('kdaks','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['qty']['placeholder']);
                                    echo form_input($form['qty']);
                                    echo form_error('qty','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo "<b>Harga @</b>";
                                    echo form_input($form['harga']);
                                    echo form_error('harga','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {

        autoNum();
      //	getKodeaksa();
        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                kdaks : "required"
            },
            messages : {
                kdaks : "Masukkan Barang Aksesoris"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdakun  : "required",
                kdaks : "required"
            },
            messages : {
                kdakun  : "Masukkan Kode Akun",
                kdaks : "Masukkan Nama Barang"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 3,4 ],
            "sClass": "center"
        });

        column.push({
            "aTargets": [ 1 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
            },
            "sClass": "right"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nmaks"},
                { "data": "qty" },
                { "data": "nilai" },
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noaksa", "value": $("#noaksa").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('undian/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 1 ).footer() ).html('Total');
                $( api.column( 2 ).footer() ).html(numeral(total).format('0,0.00'));
                getKodeaksa();
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });


        $('.btn-add').click(function(){
        	  getKodeaksa();
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kdaks').select2({
                placeholder: '-- Pilih Barang --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            clear();
        });

        $('.btn-simpan').click(function(){
          //alert($('#kdaks').val());
            if ($("#modalform").valid()) {
                status();
            }
        });

        $('.btn-update').click(function(){
            if ($("#modalform").valid()) {
                UpdateDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){
            if ($("#myform").valid()) {
              submit();
            }
        });
    });

    function autoNum(){
        $("#harga").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#qty").autoNumeric('init',{ 	aSep: '.' , aDec: ','  , mDec:'0'});
    }


    function status(){
      var qty = $('#qty').autoNumeric('get');
      var nilai = $('#harga').autoNumeric('get');
      var total = parseInt(qty) + parseInt(nilai);
      if(qty==0 && nilai==0){
        alert("Semua Data Tidak Boleh 0");
      } else if(qty==0 && nilai<0){
        alert("Nilai Tidak Boleh Kurang Dari 0");
    //  } else if(total==0){
    //    alert("Data Tidak Boleh Memiliki Total 0");
      } else {
          addDetail();
      }

    }

    function getKodeaksa(){
       //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("undian/getKodeaksa");?>",
            data: {},
            beforeSend: function() {
                $('#kdaks').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Barang --'));
                $("#kdaks").trigger("change.chosen");
                if ($('#kdaks').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks').select2('destroy');
                    $("#kdaks").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdaks')
                        .append($('<option>', { value : value.kdaks })
                        .html("<b style='font-size: 14px;'>" + value.nmaks + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmaks + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function clear(){
        $("#kdaks").val('');
        $("#harga").val('');
        $("#qty").val('');
    }

    function edit(kdaks,nmaks,qty,harga,nourut){
        $("#nourut").val(nourut);

        $("#kdaks").val(kdaks);
        //$("#kdakun").val(nmakun);
        $("#kdaks").trigger("change");
        $('#kdaks').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });
        $("#qty").autoNumeric('set',qty);
        $("#harga").autoNumeric('set',harga);

        $('#modal_transaksi').modal('toggle');
    }


    function addDetail(){

        var noaksa = $("#noaksa").val();
        var tglaksa = $("#tglaksa").val();
        var ket = $("#ket").val();
        var nourut = $("#nourut").val();
        var kdaks = $("#kdaks").val();
        var qty = $("#qty").autoNumeric('get');
        if ($("#harga").autoNumeric('get')==""){
          var harga = '0';
        } else {
          var harga = $("#harga").autoNumeric('get');
        }
        $.ajax({
            type: "POST",
            url: "<?=site_url("undian/addDetail");?>",
            data: {"noaksa":noaksa
                    ,"tglaksa":tglaksa
                    ,"ket":ket
                    ,"nourut":nourut
                    ,"kdaks":kdaks
                    ,"qty":qty
                    ,"harga":harga },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                refresh();
                clear();

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $("#noaksa").val(data.noaksa);
                    if (data.tipe==="success"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    }else{
                        //refresh();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(noaksa,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("undian/detaildeleted");?>",
                data: {"noaksa":noaksa ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('aksa');?>';
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var noaksa = $("#noaksa").val();
            var tglaksa = $("#tglaksa").val();
            var kdsup = $("#kdsup").val();
            var ket = $("#ket").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("undian/submit");?>",
                data: {"noaksa":noaksa
                    ,"tglaksa":tglaksa
                    ,"ket":ket  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('aksa');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

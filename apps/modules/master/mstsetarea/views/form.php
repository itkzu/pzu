<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<style>
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: none;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes); 
        ?> 
          <div class="box-body">    
            <div class="form-group">
                <?php
                    echo form_label('Pilih POS');
                    echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                    echo form_error('kddiv','<div class="note">','</div>'); 
                ?>
            </div> 
            <div class="form-group">
                <?php
                    echo form_input($form['id']);
                    echo form_input($form['kec']);
                
                    echo form_label("Kode Ring Area");
                    echo form_input($form['nama']);
                    echo form_error('nama','<div class="note">','</div>'); 
                ?>
            </div>    
            <div class="form-group">
                <?php
                    echo form_label('Pilih Area');
                    echo form_dropdown($form['area']['name'],$form['area']['data'] ,$form['area']['value'] ,$form['area']['attr']);
                    echo form_error('area','<div class="note">','</div>'); 
                ?>
            </div>        
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>    
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        getnamaarea();
        var kec = $("#kec").val();
        if(kec){
            var res = kec.split(",");
            $("#area").val(res).trigger("change");
        }
        $("#area").select2({
            placeholder: "Pilih Area",
        });
        
        $("#kddiv").change(function(){
            getnamaarea();
        });
    });
    
    function getnamaarea(){
        var kddiv = $("#kddiv").val();
        var id = $("#id").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('mstsetarea/getnamaarea');?>",
            data: {"kddiv":kddiv,"id":id},
            success: function(resp){  
                $("#nama").val(resp);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbklrl
 *
 * @author adi
 */
class Rptbklrl extends MY_Controller {
	protected $data = '';
	protected $val = '';
	public function __construct()
	{
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,

			'submit' => site_url('rptbklrl/submit'),
			'add' => site_url('rptbklrl/add'),
			'edit' => site_url('rptbklrl/edit'),
			'reload' => site_url('rptbklrl'),
		);
		$this->load->model('rptbklrl_qry');

		/*
		$kddiv = $this->rptbklrl_qry->getDivisiPos();
		$this->data['kddiv'] = array();
		if($kddiv){
			foreach ($kddiv as $value) {
				$this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
			}
		}
		*/

		for ($x = 1; $x <= 13; $x++) {
			$this->data['bulan'][$x] = strmonth($x).". ".month($x);
		}

		for ($x = date('Y'); $x >= 2015; $x--) {
			$this->data['tahun'][$x] = $x;
		}
		$kddiv = $this->rptbklrl_qry->getDataCabang();
		foreach ($kddiv as $value) {
			$this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
		}
		$kddivunit = $this->rptbklrl_qry->getDataUnitCabang();
		foreach ($kddivunit as $value) {
			$this->data2['kddiv'][$value['kddiv']] = $value['nmdiv'];
		}
	}

	public function index(){
		$this->_init_add();
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('index',$this->data);
	}

	public function submit() {
		$this->data['rptakun'] = $this->rptbklrl_qry->submit();

		if(empty($this->data['rptakun'])){
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('debug/err_000',$this->data);
		}else{
			$array = $this->input->post();
			if($array['submit']){
				$this->template
					->title($this->data['msg_main'],$this->apps->name)
					->set_layout('print-layout')
					->build('html',$this->data);
			}else{
				redirect("rptbklrl");
			}
		}
	}

	private function _init_add(){
		$this->data['form'] = array(
			'kddiv2'=> array(
					'attr'        => array(
						'id'    => 'kddiv2',
						'class' => 'form-control select',
					),
					'data'        => $this->data2['kddiv'],
					'value'       => '',
					'name'        => 'kddiv2',
					'placeholder' => 'Laporan Per Cabang',
			),
			'kddiv'=> array(
					'attr'        => array(
						'id'    => 'kddiv',
						'class' => 'form-control select',
					),
					'data'        => $this->data['kddiv'],
					'value'       => '',
					'name'        => 'kddiv',
					'placeholder' => 'Laporan Per POS',
			),
			'nmdiv'=> array(
					'placeholder' => 'Nama Divisi',
					// 'type'        => 'hidden',
					'id'          => 'nmdiv',
					'name'        => 'nmdiv',
					'value'       => set_value('nmdiv'),
					'class'       => 'form-control',
					'required'    => '',
			),
			'bulan_awal'=> array(
					'attr'        => array(
						'id'    => 'bulan_awal',
						'class' => 'form-control',
					),
					'data'        => $this->data['bulan'],
					'value'       => $this->mm,
					'name'        => 'bulan_awal',
			),
			'bulan_akhir'=> array(
					'attr'        => array(
						'id'    => 'bulan_akhir',
						'class' => 'form-control',
					),
					'data'        => $this->data['bulan'],
					'value'       => $this->mm,
					'name'        => 'bulan_akhir',
			),
			'tahun'=> array(
					'attr'        => array(
						'id'    => 'tahun',
						'class' => 'form-control',
					),
					'data'        => $this->data['tahun'],
					'value'       => '',
					'name'        => 'tahun',
			),
		);
	}
}

﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $template['title'];?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url('assets/dist/img/logo-honda-prima-small.png');?>" type="image/png">

    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/square/blue.css');?>">
    <!-- Animate -->
    <link rel="stylesheet" href="<?=base_url('assets/custom/animate.css');?>">
    <!-- Pace -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/pace/themes/white/pace-theme-minimal.css');?>">
    <style type="text/css">
        .img-login{
            background-color: #f1ebeb;
            height: 250px;
            transform: rotate(-10deg);
            border: solid 5px #fff;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.3);
        }
        
        .login-logo, .register-logo {
            margin-bottom: 0px;
        }
        
        .login-box, .register-box {
            width: 320px;
            margin: 2% auto;
        }
        
        .login-page{
            background: linear-gradient(110deg, #d73925, #bd2714, #9d1909, #9d1909, #bd2714, #d73925);
            background-size: 1000% 1000%;

            -webkit-animation: AnimationName 15s ease infinite;
            -moz-animation: AnimationName 15s ease infinite;
            -o-animation: AnimationName 15s ease infinite;
            animation: AnimationName 15s ease infinite;
            
            height: auto;
        }
        
        .rainbow-login-page{
            background: linear-gradient(270deg, #662424, #665624, #4a6624, #276624, #246652, #245766, #243466, #3d2466, #542466, #66244c, #66242f, #662424);
            background-size: 2400% 2400%;

            -webkit-animation: Rainbow 30s ease infinite;
            -moz-animation: Rainbow 30s ease infinite;
            -o-animation: Rainbow 30s ease infinite;
            animation: Rainbow 30s ease infinite;
        }
        
        .login-box-body, .register-box-body{
            background: none;
        }
        
        .login-logo a, .register-logo a{
            color: #fff;
        }
        
        .login-box-msg, .register-box-msg {
            color: #fff;
        }
        
        @-webkit-keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-moz-keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-o-keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @keyframes AnimationName { 
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        
        @-webkit-keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-moz-keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-o-keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @keyframes Rainbow { 
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @media (max-width: 768px){
            .img-side{
                display: none;
            }
        }
    </style>
</head>

<body class="hold-transition login-page">
    <?php echo $template['body'];?>
    <!-- jQuery 2.2.3 -->
    <script src="<?=base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- iCheck -->
    <script src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>    
    <!-- Pace -->
    <script src="<?=base_url('assets/plugins/pace/pace.min.js');?>"></script>
    <script>
      $(function () {
          Pace.restart();
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
        
        $(".alert").fadeOut(5000);
      });
    </script>
</body>

</html>
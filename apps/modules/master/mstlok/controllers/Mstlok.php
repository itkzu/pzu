<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstlok
 *
 * @author adi
 */
class Mstlok extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstlok/submit'),
            'add' => site_url('mstlok/add'),
            'edit' => site_url('mstlok/edit'),
            'reload' => site_url('mstlok'),
        );
        $this->load->model('mstlok_qry');
        $kategori = $this->mstlok_qry->getKdProv();
        foreach ($kategori as $value) {
            $this->data['kode_provinsi'][$value['kode_provinsi']] = $value['nmprov'];
        }

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstlok_qry->json_dgview();
    }

    public function submit() {
        echo $this->mstlok_qry->submit();
    }

    public function getKdKota() {
        echo $this->mstlok_qry->getKdKota();
    }

    public function getKdKec() {
        echo $this->mstlok_qry->getKdKec();
    }

    public function update() {
        echo $this->mstlok_qry->update();
    }

    public function delete() {
        echo $this->mstlok_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdlurah'=> array(
                    // 'type'        => 'hidden',
                    'placeholder' => 'Kode Lurah',
                    'id'          => 'kdlurah',
                    'name'        => 'kdlurah',
                    'value'       => set_value('kdlurah'),
                    'class'       => 'form-control',
                    'required'    => '',
                    // 'readonly'    => '',
            ),
           'nmlurah'=> array(
                    'placeholder' => 'Nama Lurah',
                    'id'      => 'nmlurah',
                    'name'        => 'nmlurah',
                    'value'       => set_value('nmlurah'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdprovinsi'=> array(
                    'attr'        => array(
                        'id'    => 'kdprovinsi',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kode_provinsi'],
                    'value'    => set_value('kdprovinsi'),
                    'name'     => 'kdprovinsi',
                    'placeholder' => 'Pilih Provinsi',
                    'required' => ''
            ),
            'kdkota'=> array(
                    'attr'        => array(
                        'id'    => 'kdkota',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdkota'),
                    'name'     => 'kdkota',
                    'placeholder' => 'Pilih Kota / Kabupaten',
                    'required' => ''
            ),
            'kdkec'=> array(
                    'attr'        => array(
                        'id'    => 'kdkec',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdkec'),
                    'name'     => 'kdkec',
                    'placeholder' => 'Pilih Kecamatan',
                    'required' => ''
            ),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdlurah'=> array(
                    // 'type'        => 'hidden',
                    'placeholder' => 'Kode Lurah / Desa',
                    'id'          => 'kdlurah',
                    'name'        => 'kdlurah',
                    'value'       => $this->val[0]['kdlurah'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmlurah'=> array(
                    'placeholder' => 'Nama Lurah',
                    'id'          => 'nmlurah',
                    'name'        => 'nmlurah',
                    'value'       => $this->val[0]['nmlurah'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'kdprovinsi'=> array(
                    'attr'        => array(
                        'id'    => 'kdprovinsi',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdprovinsi'],
                    'value'       => $this->val[0]['kdprovinsi'],
                    'name'     => 'kdprovinsi',
                    'placeholder' => 'Pilih Provinsi',
                    'required' => ''
            ),
            'kdkec'=> array(
                    'attr'        => array(
                        'id'    => 'kdkec',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdkec'],
                    'value'       => $this->val[0]['kdkec'],
                    'name'     => 'kdkec',
                    'placeholder' => 'Pilih Kecamatan',
                    'required' => ''
            ),
            'kdkota'=> array(
                    'attr'        => array(
                        'id'    => 'kdkota',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdkota'],
                    'value'       => $this->val[0]['kdkota'],
                    'name'     => 'kdkota',
                    'placeholder' => 'Pilih Kota / Kabupaten',
                    'required' => ''
            ),
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstlok_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdkota',
                    'label' => 'Kode Kota',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmkota',
                    'label' => 'Nama Kota',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

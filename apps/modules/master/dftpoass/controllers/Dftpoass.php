<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dftpoass
 *
 * @author
 */
class Dftpoass extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('dftpoass/submit'),
            'add' => site_url('dftpoass/add'),
            'edit' => site_url('dftpoass/edit'),
            'reload' => site_url('dftpoass'),
        );
        $this->load->model('dftpoass_qry'); 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->dftpoass_qry->json_dgview();
    }

    private function _init_add(){


    		if(isset($_POST['hrgblmada']) && strtoupper($_POST['hrgblmada']) == 'OK'){
    			$chk_hrg = TRUE;
    		} else{
    			$chk_hrg = FALSE;
    		}

        $this->data['form'] = array(
           'nodftpoass'=> array(
                    'placeholder' => 'No. Transdftpoass',
                    //'type'        => 'hidden',
                    'id'          => 'nodftpoass',
                    'name'        => 'nodftpoass',
                    'value'       => set_value('nodftpoass'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tgldftpoass'=> array(
                    'placeholder' => 'Tanggal Transdftpoass',
                    'id'          => 'tgldftpoass',
                    'name'        => 'tgldftpoass',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
            'kdsup'=> array(
                    'attr'        => array(
                        'id'    => 'kdsup',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kdsup'],
                    'value'    => set_value('kdsup'),
                    'name'     => 'kdsup',
                    'required'    => '',
                    'placeholder' => 'Supplier',
            ),
           'nmsup'=> array(
                    'placeholder' => 'Supplier',
                    'id'          => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => set_value('nmsup'),
                    'class'       => 'form-control',
                    'required'    => '',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
      		   'stathrg'=> array(
                'placeholder' => 'Harga Sudah diketahui',
      					'id'          => 'stathrg',
      					'value'       => 't',
      					'checked'     => '',
      					'class'       => 'filled-in',
      					'name'		  => 'faktif'
      			),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => ''
            ),
            'kdaks'=> array(
                    'attr'        => array(
                        'id'    => 'kdaks',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdaks'),
                    'name'     => 'kdaks',
                    'placeholder' => 'Cari Barang',
                    'required' => ''
            ),
            'nmaks'=> array(
                    'attr'        => array(
                        'id'    => 'nmaks',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmaks'),
                    'name'     => 'nmaks',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            ),
            'harga'=> array(
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => set_value('harga'),
                    'name'     => 'harga',
            ),
            'total'=> array(
                    'placeholder' => 'Total',
                    'id'    => 'total',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('total'),
                    'name'     => 'total',
                    'readonly' => ''
            ),
            'diskon'=> array(
                    'placeholder' => 'Diskon <small>dalam rupiah </small>(-)',
                    'id'    => 'diskon',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => 0,
                    'name'     => 'diskon',
                    'onkeyup' => 'status()'
            ),
            'biaya_lain'=> array(
                    'placeholder' => 'Biaya Lainnya (+)',
                    'id'    => 'biaya_lain',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => 0,
                    'name'     => 'biaya_lain',
                    'onkeyup' => 'status()'
            ),
            'total_net'=> array(
                    'placeholder' => 'Total Net',
                    'id'    => 'total_net',
                    'class' => 'form-control',
                    'value'    => set_value('total_net'),
                    'name'     => 'total_net',
                    'readonly' => ''
            ),
            'statppn'=> array(
                    'placeholder' => 'Status PPN',
                    'attr'        => array(
                        'id'    => 'statppn',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['statppn'],
                    'class' => 'form-control',
                    'value'    => set_value('statppn'),
                    'name'     => 'statppn',
                    'readonly' => '',
            ),
            'dpp'=> array(
                    'placeholder' => 'DPP',
                    'id'    => 'dpp',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('dpp'),
                    'name'     => 'dpp',
                    'readonly' => ''
            ),
            'ppn'=> array(
                    'placeholder' => 'PPN',
                    'id'    => 'ppn',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ppn'),
                    'name'     => 'ppn',
                    'readonly' => ''
            ),
            'totppn'=> array(
                    'placeholder' => 'Total',
                    'id'    => 'totppn',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('totppn'),
                    'name'     => 'totppn',
                    'readonly' => ''
            )
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->dftpoass_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

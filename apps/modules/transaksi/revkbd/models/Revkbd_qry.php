<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Revkbd_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

	public function getjnstrans() {
        $this->db->select("jnstrx,nourut,case when jnstrx like '%(D)%' THEN 'D' ELSE 'K' END AS kd");
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->order_by('nourut');
        $this->db->group_by('jnstrx,nourut');
        $q = $this->db->get("pzu.kasbank_trx");
        return $q->result_array();
    }

	public function proses_dk() {
        $jnstrans = $this->input->post('jnstrans');
        $this->db->select("jnstrx,nourut,case when jnstrx like '%(D)%' THEN 'D' ELSE 'K' END AS kd");
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('jnstrx',$jnstrans);
        $this->db->order_by('nourut');
        $this->db->group_by('jnstrx,nourut');
        $q = $this->db->get("pzu.kasbank_trx");
          if($q->num_rows()>0){
              $res = $q->result_array();
          }else{
              $res = "empty";
          }
          return json_encode($res); 
    }

	public function getKdRef() {
        $jenis = $this->input->post('kdrefkb');
        $this->db->select('*');
        $this->db->where('faktif',true);
        $this->db->where('dk',$jenis);
        $this->db->order_by('kdrefkb');
        $q = $this->db->get("pzu.refkb_m"); 
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    }

	public function getjnskredit() {
        $this->db->select('kdrefkb as kdkredit,nmrefkb,dk,faktif');
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->where('dk','K');
        $this->db->order_by('kdrefkb');
        $q = $this->db->get("pzu.refkb_m");
        return $q->result_array();
    }

	public function set_jenis() {
    	$nourut = $this->input->post('nourut');
    	$nokb = $this->input->post('nokb');
    	$query = $this->db->query("select * from pzu.t_kb_d where nokb = '".$nokb."' and nourut = ".$nourut);
    	// echo $this->db->last_query();
    	$res = $query->result_array();
        return json_encode($res);
    }

    public function select_data($param,$param2) {
        $this->db->select("nokb, nocetak, to_char(tglkb,'DD-MM-YYYY') as tglkb, kdkb, nmkb"
                            . ", jenis, nofaktur, drkpd, debet, kredit, ket, nourut");
        $this->db->where('nokb',$param);
        $this->db->where('nourut',$param2);
        $query = $this->db->get('pzu.v_kb_d');
        return $query->result_array();
    }

	public function getDataTrx() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.v_kasbank_trx");
        return $q->result_array();
    }

    public function getData() {
		error_reporting(-1);

        //$nokb = $this->input->post('nokb');

		if( isset($_GET['nokb']) ){
			if($_GET['nokb']){
				$nokb = $_GET['nokb'];
			}else{
				$nokb = '';
			}
    	}else{
      		$nokb = '';
    	}

		//var_dump($periode_awal, $periode_awal);
		$aColumns = array('no',
                      	'nocetak',
                      	'nokb',
                      	'tglkb',
                      	'jenis',
                      	'drkpd',
                      	'ket',
                      	'debet',
                      	'kredit',
						'nourut',
                        );


		$sIndexColumn = "nokb";

		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}

		$sTable = " (SELECT '' as no, nocetak, to_char(tglkb,'DD-MM-YYYY') AS tglkb, nokb, jenis,drkpd,ket,debet,kredit,nourut
					    FROM pzu.v_kb_d WHERE nokb = '". $nokb ."' ORDER BY ordered,nourut
					) AS a";


		//$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
		$sWhere = "";

		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
					$sOrder = "";
			}
		}

		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere =  "AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{

			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}


		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";

        //echo $sQuery;

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";	//SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if($aRow[ $aColumns[$i] ]===null){
					$aRow[ $aColumns[$i] ] = '';
				}
				if(is_numeric($aRow[ $aColumns[$i] ])){
					$row[] = floatval($aRow[ $aColumns[$i] ]);
				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$row[9] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs btn-edit \" href=\"".site_url('revkbd/edit/'.$aRow['nokb'].'/'.$aRow['nourut'])."\">Edit</a>";

      $output['aaData'][] = $row;
		}
		echo json_encode( $output );
	}

    public function proses() {
		$nokb = $this->input->post('nokb');
		$nourut = $this->input->post('nourut');
		$nofaktur = $this->input->post('nofaktur');
		$drkpd = $this->input->post('drkpd');
		$ket = $this->input->post('ket');
		$jenis = $this->input->post('jenis') ? $this->input->post('jenis') : '';
		$debet = $this->input->post('debet');
		$kredit = $this->input->post('kredit');
		
		// Remove thousand separators and convert comma to dot for decimal
		$debet = str_replace('.', '', $debet);
		$debet = str_replace(',', '.', $debet);
		$kredit = str_replace('.', '', $kredit);
		$kredit = str_replace(',', '.', $kredit);
	

		// Debugging input values
		error_log("nokb: " . $nokb);
		error_log("nourut: " . $nourut);
		error_log("nofaktur: " . $nofaktur);
		error_log("drkpd: " . $drkpd);
		error_log("ket: " . $ket);
		error_log("jenis: " . $jenis);  // Pastikan ini ada
		error_log("debet: " . $debet);
		error_log("kredit: " . $kredit);

		$q = $this->db->query("select title,msg,tipe from pzu.kb_d_desc_upd3('".$nokb."',".$nourut.",'".$nofaktur."','".$drkpd."','".$ket."','".$jenis."','".$debet."','".$kredit."','".$this->session->userdata('username')."')");
		
		// echo $this->db->last_query();  // Print the last executed query
	
		if($q->num_rows()>0){
			$res = $q->result_array();
		}else{
			$res = "";
		}
		return json_encode($res);
	}
    
    public function submit() {
        try {
            $array = $this->input->post();
            $array['nokb'] = $this->nokb;
            $array['nourut'] = $this->nourut;
            if(!empty($array['nokb']) && empty($array['nourut'])){
      				//echo "<script> console.log('post qry: ". json_encode($array) ."');</script>";

      				$this->db->where('nourut', $array['nokb']);
      				unset($array['nokb']);
      				$resl = $this->db->update('v_kb_d', $array);
      				if( ! $resl){
      					$err = $this->db->error();
      					$this->res = " Error : ". $this->apps->err_code($err['message']);
      					$this->state = "0";
      				}else{
      					$this->res = "Data Berhasil Diupdate";
      					$this->state = "1";
      				}

      			}

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

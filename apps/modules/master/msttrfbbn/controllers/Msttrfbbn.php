<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Msttrfbbn
 *
 * @author adi
 */
class Msttrfbbn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('msttrfbbn/submit'),
            'add' => site_url('msttrfbbn/add'),
            'edit' => site_url('msttrfbbn/edit'),
            'reload' => site_url('msttrfbbn'),
        );
        $this->load->model('msttrfbbn_qry'); 

    }

    //redirect if needed, otherwise display the user list

    public function index(){ 
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->msttrfbbn_qry->json_dgview();
    }

    public function submit() {
        echo $this->msttrfbbn_qry->submit();
    }

    public function update() {
        echo $this->msttrfbbn_qry->update();
    }

    private function _init_add(){

        $this->data['form'] = array( 
           'kota'=> array(
                    'placeholder' => 'Nama Kota / Kabupaten',
                    'id'      => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'kota2'=> array(
                    'placeholder' => 'Nama Kota / Kabupaten',
                    'id'      => 'kota2',
                    'type'        => 'hidden',
                    'name'        => 'kota2',
                    'value'       => set_value('kota2'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'byproses'=> array(
                    'placeholder' => 'Biaya Proses',
                    'id'      => 'byproses',
                    'name'        => 'byproses',
                    'value'       => set_value('byproses'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'nominal();'
            ), 
           'byjasa'=> array(
                    'placeholder' => 'Biaya Jasa',
                    'id'      => 'byjasa',
                    'name'        => 'byjasa',
                    'value'       => set_value('byjasa'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'nominal();'
            ), 
           'ket'=> array(
                    'placeholder' => 'Biaya Jasa',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'type'        => 'hidden',
                    'value'       => '0', 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;', 
            ),  
        );
    }

    private function _init_edit(){
        $kota = $this->uri->segment(3);
        $this->_check_id($kota); 
        $this->data['form'] = array(
           'kota'=> array(
                    'placeholder' => 'Nama Kota / Kabupaten',
                    'id'      => 'kota',
                    'name'        => 'kota',
                    'value'       => $this->val[0]['kota'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'kota2'=> array(
                    'placeholder' => 'Nama Kota / Kabupaten',
                    'id'      => 'kota2',
                    'type'        => 'hidden',
                    'name'        => 'kota2',
                    'value'       => $this->val[0]['kota'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            // 'kota'=> array(
            //         'attr'        => array(
            //             'id'    => 'kota',
            //             'class' => 'form-control chosen-select',
            //         ),
            //         'data'     =>  $this->data['kota'],
            //         'value'    => set_value('kota'),
            //         'name'     => 'kota',
            //         'placeholder' => 'Nama Kota / Kabupaten',
            //         'required' => ''
            // ),
           'byproses'=> array(
                    'placeholder' => 'Biaya Proses',
                    'id'      => 'byproses',
                    'name'        => 'byproses',
                    'value'       => $this->val[0]['byproses'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'nominal();'
            ), 
           'byjasa'=> array(
                    'placeholder' => 'Biaya Jasa',
                    'id'      => 'byjasa',
                    'name'        => 'byjasa',
                    'value'       => $this->val[0]['byjasa'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'nominal();'
            ), 
           'ket'=> array(
                    'placeholder' => 'Biaya Jasa',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'type'        => 'hidden',
                    'value'       => '1', 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;', 
            ),  
        );
    }

    private function _check_id($kota){
        if(empty($kota)){
            redirect($this->data['add']);
        }

        $this->val= $this->msttrfbbn_qry->select_data($kota);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'Kode Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmaks',
                    'label' => 'Nama Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptmstkpb_qry
 *
 * @author adi
 */
class Rptmstkpb_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
			}else{
				$periode_awal = '';
			} 
		}else{
			$periode_awal = '';
		}        
		
		if( isset($_GET['periode_akhir']) ){
			if($_GET['periode_akhir']){
				//$tgl2 = explode('-', $_GET['periode_akhir']);
				$periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl2[1].$tgl2[0];
			}else{
				$periode_akhir = '';
			} 
		}else{
			$periode_akhir = '';
		} 

		$aColumns = array('kdsin'
							, 'kdtipe'
							, 'nmtipe'
							, 'oli'
							, 'kpb1'
							, 'kpb2'
							, 'kpb3'
							, 'kpb4'
		);


		$sIndexColumn = "kdsin";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}


		$sTable = "(
						SELECT kdsin,kdtipe,nmtipe,oli,kpb1,kpb2,kpb3,kpb4
						FROM m_kpb
					) AS A ";


//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		
		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
//        echo $sQuery;
		
		$rResult = $dbbengkel->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $dbbengkel->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $dbbengkel->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}
	
	public function submit(){

	}
}

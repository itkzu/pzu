<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
      <form id="modalform" name="modalform" method="post">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">
          <div class="box-header box-view">
              <!-- <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a>  -->
            <button type="button" class="btn btn-primary btn-add">Simpan</button>
            <button type="button" class="btn btn-danger btn-batal">Batal</button>
          </div> 

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['nodoin']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nodoin']);
                                    echo form_error('nodoin','<div class="note">','</div>');
                                ?>
                            </div>
                        </div> 
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['tgldoin']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['tgldoin']);
                                    echo form_error('tgldoin','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>  
                      </div>
                  </div>

                  <div class="col-xs-12">
                      <br>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['nmcabang']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['nmcabang']['name'],$form['nmcabang']['data'] ,$form['nmcabang']['value'] ,$form['nmcabang']['attr']);
                                    echo form_error('nmcabang','<div class="note">','</div>');
                                  // echo form_label($form['kdtipe']['placeholder']);
                                  // echo form_input($form['kdtipe']);
                                  // echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-2">
                            <label>
                              <?php echo form_label($form['nosin']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nosin']);
                                      echo form_error('nosin','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div> 

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nora']);
                                      echo form_error('nora','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['nopo']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nopo']);
                                      echo form_error('nopo','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['kdtipe']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['kdtipe']);
                                  echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nmtipe']);
                                    echo form_error('nmtipe','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['tglpo']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['tglpo']);
                                  echo form_error('tglpo','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['warna']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['warna']);
                                  echo form_error('warna','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                        <div class="col-xs-1">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['tahun']);
                                  echo form_error('tahun','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['hrgjual']['placeholder']); ?>
                          </label>
                        </div> 

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['hrgjual']);
                                  echo form_error('hrgjual','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <br>
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['ket']['placeholder']); ?>
                          </label>
                        </div> 

                        <div class="col-xs-4">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['ket']);
                                  echo form_error('ket','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div> 


              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->
      </form>
    </div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#hrgjual').autoNumeric('init');

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                nosin  : "required",
                ket : "required"
            },
            messages : {
                nosin  : "Masukkan Nomer Mesin",
                ket : "Masukkan Keterangan"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        GetNoID(); 
        disabled();
        $("#nosin").mask("***-***-***-***");
        
        $('#nosin').change(function(){
            setnosin();
        });
        
        $('.btn-add').click(function(){
            if ($("#modalform").valid()) {
                add_doin();
            }
        });

        $(".btn-batal").click(function(){
            batal(); 
        });

        $("#tgldoin").change(function(){
          GetNoID();
        });
    });

    // no id
    function GetNoID(){ 
        //alert(kddiv);
        var tgldoin = $('#tgldoin').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("doinnew/getNoID");?>",
            data: {"tgldoin":tgldoin},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $.mask.definitions['9'] = '';
                  $.mask.definitions['d'] = '[0-9]';

                  if(value.jml > 99){  
                      $('#nodoin').mask(value.kddoi+"-"+value.prd+"-"+value.jml); 
                  }else if(value.jml > 9){
                      $('#nodoin').mask(value.kddoi+"-"+value.prd+"-0"+value.jml); 
                  }else if(value.jml > 0){
                      $('#nodoin').mask(value.kddoi+"-"+value.prd+"-00"+value.jml); 
                  }
                   
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    // get no do
    function setnosin(){
        var nosin = $('#nosin').val();
        var nosin = nosin.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("doinnew/setnosin");?>",
            data: {"nosin":nosin},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                if(obj===''){
                  swal({
                      title: 'Unit Tidak Ada',
                      text: 'warning',
                      type: 'error'
                      }, function(){
                          $('#nora').val(''); 
                          $('#nopo').val('');
                          $('#kdtipe').val('');
                          $('#nmtipe').val('');
                          $('#tglpo').val($.datepicker.formatDate('dd-mm-yy', new Date()));
                          $('#warna').val('');
                          $('#tahun').val('');
                          $('#hrgjual').autoNumeric('set',0); 
                  }); 
                }
                $.each(obj, function(key, value){
                    $('#nora').val(value.nora2); 
                    $('#nopo').val(value.nopox);
                    $('#kdtipe').val(value.kdtipe);
                    $('#nmtipe').val(value.nmtipe);
                    $('#tglpo').val($.datepicker.formatDate('dd-mm-yy', new Date(value.tglpo)));
                    $('#warna').val(value.warna);
                    $('#tahun').val(value.tahun);
                    $('#hrgjual').autoNumeric('set',value.dpp_beli); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    // get no do
    function add_doin(){
        var tgldoin = $('#tgldoin').val();  
        var nmcabang = $('#nmcabang').val();  
        var nosin = $('#nosin').val();  
        var nilai = $('#hrgjual').autoNumeric('get');  
        var ket = $('#ket').val();  
        var kdsup = $('#nmcabang').val();  
        $.ajax({
            type: "POST",
            url: "<?=site_url("doinnew/add_doin");?>",
            data: {"tgldoin":tgldoin,"nmcabang":nmcabang,"nosin":nosin,"nilai":nilai,"ket":ket,"kdsup":kdsup},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, data){    
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            window.location.href = '<?=site_url('doinnew');?>';
                        }); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    function disabled(){
      $('#nodoin').prop('disabled',true);
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('doinnew');?>';
        });
    }
</script>

<?php
/*
 * ***************************************************************
 * Script : adh.php
 * Version :
 * Date : Nov 21, 2017 10:11:14 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
	.dt-buttons{
		margin-bottom: 10px;
	}
</style>
<!-- Main row -->
<div class="row">

	<div class="col-lg-12">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-balance-scale"></i> Posisi Kas / Bank Bengkel
			</div>
			<div class="box-body">
				<div id="" style="min-height: 300px; overflow-y: auto;overflow-x: hidden;" align="center">
					<table class="table table-hover table-bordered trn_kas_bank_bkl" style="margin-top: 0px !important;">
						<thead>
							<tr>
								<th  style="width:10px;">No.</th>
								<th>Item Rekening</th>
								<th style="width: 100px;">Periode Aktif</th>
							</tr>
						</thead>
						<!--
						<tfoot>
							<tr>
								<th  style="width:10px;">No.</th>
								<th>Item Rekening</th>
								<th style="width: 100px;">Periode Aktif</th>
							</tr>
						</tfoot>
						-->
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ChartJS -->
<script src="<?= base_url('assets/plugins/chartjs/Chart.bundle.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/chartjs/utils.js'); ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<script>
	$(function () {
		$(".btn-tampil").click(function () {
			trn_kas_bank_bkl.ajax.reload();
		});



		trn_kas_bank_bkl = $('.trn_kas_bank_bkl').DataTable({
			"aoColumnDefs": [
				{
					"aTargets": [2],
					"mRender": function (data, type, full) {
						return type === 'export' ? data : data; //numeral(data).format('0,0');
						// return type === 'export' ? data : numeral(data).format('0,0.00');
						// return formmatedvalue;
					}
				}],
			"fixedColumns": {
				leftColumns: 2
			},
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"aaSorting": [],
			"fnServerData": function (sSource, aoData, fnCallback) {
				aoData.push({"name": "key", "value": "pzu"}
				);
				$.ajax({
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			'rowCallback': function (row, data, index) {
				//if(data[23]){
				//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?= site_url('dashboard/getTrnKasBankbkl'); ?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win) {
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-12 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
		});

		$('.trn_kas_bank_bkl').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});


		$('.trn_kas_bank_bkl tfoot th').each(function () {
			var title = $('.trn_kas_bank_bkl thead th').eq($(this).index()).text();
			if (title !== "Edit" && title !== "Delete" && title !== "Periode Aktif" && title !== "No.") {
				$(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
			} else {
				$(this).html('');
			}
		});


		trn_kas_bank_bkl.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
				that
						.search(this.value)
						.draw();
				//}
			});
		}); 
	});

</script>

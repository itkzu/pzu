<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">
              <div class="row">
                  <div class="col-md-6 col-lg-6">
                      <div class="row">
                          <div class="col-xs-6">
                            <div class="form-group">
                                <?=form_label($form['kddiv']['placeholder']);?>
                                <div class="input-group">
                                  <?php
                                      echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                      echo form_error('kddiv', '<div class="note">', '</div>');
                                    ?>
                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-set" id="btn-set">Set</button>
                                  </div>
                                </div>
                              </div>

                          </div>
                          <div class="col-xs-12">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['kdrefkb']);
                                    echo form_input($form['nourut']);

                                    echo form_label($form['nmrefkb']['placeholder']);
                                    echo form_input($form['nmrefkb']);
                                    echo form_error('nmrefkb','<div class="note">','</div>');
                                ?>
                            </div>

                          </div>
                          <div class="col-xs-6">
                            <div class="form-group">
                                <?php
                                    echo form_label('Debit/Kredit');
                                    echo form_dropdown($form['dk']['name'],$form['dk']['data'] ,$form['dk']['value'] ,$form['dk']['attr']);
                                    echo form_error('dk','<div class="note">','</div>');
                                ?>
                            </div>

                          </div>
                          <div class="col-xs-6">
                            <div class="form-group">
                                <?php
                                    echo form_label('Status Aktif');
                                    echo form_dropdown($form['faktif']['name'],$form['faktif']['data'] ,$form['faktif']['value'] ,$form['faktif']['attr']);
                                    echo form_error('faktif','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <?php
                            echo form_label($form['ket']['placeholder']);
                            echo form_textarea($form['ket']);
                            echo form_error('ket','<div class="note">','</div>');
                        ?>
                    </div>
                  </div>
                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <?php
                            echo form_label('Pilih Kode Akun');
                            echo form_dropdown($form['kdakun']['name'],$form['kdakun']['data'] ,$form['kdakun']['value'] ,$form['kdakun']['attr']);
                            echo form_error('kdakun','<div class="note">','</div>');
                        ?>
                    </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                            <?php
                                echo form_label($form['mpl']['placeholder']);
                            ?>
                            <div class="input-group">
                                <?php
                                    echo form_input($form['mpl']);
                                    echo form_error('mpl','<div class="note">','</div>');
                                ?>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-add">
                                        Tambah Akun
                                    </button>
                                    <button type="button" class="btn btn-default btn-reset-detail">
                                        Reset
                                    </button>
                                </span>
                            </div>
                      </div>
                  </div>
                  <div class="col-xs-12">
                      <div class="table-responsive">
                        <table class="table table-hover dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 10px;text-align: center;">
                                        No.
                                    </th>
                                    <th style="width: 120px;text-align: center;">
                                        Kode Akun
                                    </th>
                                    <th style="text-align: center;">
                                        Nama Akun
                                    </th>
                                    <th style="width: 200px;text-align: center;">
                                        Persentase(%)
                                    </th>
                                    <th style="width: 10px;text-align: center;">
                                        Edit
                                    </th>
                                    <th style="width: 10px;text-align: center;">
                                        Hapus
                                    </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <button type="button" class="btn btn-default btn-batal">
                Batal
            </button>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

      getHeader();
      if ($('#kdrefkb').val()){
        $('.btn-set').hide();
        $('#kddiv').attr("disabled",true);
        //$('#kddiv').hide();
        getKodeAkun();
        getData();
      } else {
          $('.btn-set').show();
          $('#kddiv').attr("disabled",false);
          $('#kddiv').show(); 


        table = $('.dataTable').DataTable({
            "columns": [
                { "data": "nourut" },
                { "data": "kdakun" },
                { "data": "nmakun" },
                { "data": "mpl" },
                { "data": "edit" },
                { "data": "delete" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push();
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){

            },
            "sAjaxSource": "<?=site_url('mstrefposbkl/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            buttons: [
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                }
            ],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $("#mpl").keydown(function (e) {
         // Allow: backspace, delete, tab, escape, enter and .
         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
              // Allow: Ctrl+A, Command+A
             (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
              // Allow: home, end, left, right, down, up
             (e.keyCode >= 35 && e.keyCode <= 40)) {
                  // let it happen, don't do anything
                  return;
         }
         // Ensure that it is a number and stop the keypress
         if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
             e.preventDefault();
         }
     });
}
        $('.btn-set').click(function(){
            getKodeAkun();
            getData();
            //alert($('#kddiv').val());
            $('.btn-set').hide();
            $('#kddiv').attr("disabled",true);
        });

        $(".btn-add").click(function(){
            addDetail();
        });

        $(".btn-reset-detail").click(function(){
            resetDetail();
        });

        $(".btn-batal").click(function(){
            batal();
        });

        $(".btn-submit").click(function(){
            submit();
        });
    });

    function getData(){

      table = $('.dataTable').DataTable({
          "columns": [
              { "data": "nourut" },
              { "data": "kdakun" },
              { "data": "nmakun" },
              { "data": "mpl" },
              { "data": "edit" },
              { "data": "delete" }
          ],
          "lengthMenu": [[ -1], [ "Semua Data"]],
          "bProcessing": true,
          "bServerSide": true,
          "bDestroy": true,
          "bAutoWidth": false,
          "fnServerData": function ( sSource, aoData, fnCallback ) {
              aoData.push(
                          { "name": "kdrefkb", "value": $("#kdrefkb").val() }
                         ,{ "name": "kddiv", "value": $("#kddiv").val() }
                        );
              $.ajax( {
                  "dataType": 'json',
                  "type": "GET",
                  "url": sSource,
                  "data": aoData,
                  "success": fnCallback
              } );
          },
          'rowCallback': function(row, data, index){

          },
          "sAjaxSource": "<?=site_url('mstrefposbkl/json_dgview_detail');?>",
          "oLanguage": {
              "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
          },
          buttons: [
              {
                  extend:    'excelHtml5',
                  text:      'Export To Excel',
                  titleAttr: 'Excel',
                  "oSelectorOpts": { filter: 'applied', order: 'current' },
                  "sFileName": "report.xls",
                  action : function( e, dt, button, config ) {
                      exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                  },
                  exportOptions: {orthogonal: 'export'}

              }
          ],
          "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
      });

      $("#mpl").keydown(function (e) {
       // Allow: backspace, delete, tab, escape, enter and .
       if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
           (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
           (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
       }
       // Ensure that it is a number and stop the keypress
       if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
           e.preventDefault();
       }
      });
    }

    function getKodeAkun(){
        var kddiv = $("#kddiv").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("mstrefposbkl/getKodeAkun");?>",
            data: {"kddiv":kddiv},
            beforeSend: function() {
                $('#kdakun').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kode Akun --'));
                $("#kdakun").trigger("change.chosen");
                if ($('#kdakun').hasClass("chosen-hidden-accessible")) {
                    $('#kdakun').select2('destroy');
                    $("#kdakun").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdakun')
                        .append($('<option>', { value : value.kdakun })
                        .html("<b style='font-size: 14px;'>" + value.kdakun + " - " + value.nmakun + " </b>"));
                });

                $('#kdakun').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.kdakun + " - " + repo.nmakun + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function addDetail(){

        var kddiv = $("#kddiv").val();
        var kdrefkb = $("#kdrefkb").val();
        var nmrefkb = $("#nmrefkb").val();
        var dk = $("#dk").val();
        var faktif = $("#faktif").val();
        var ket = $("#ket").val();

        var nourut = $("#nourut").val();
        var kdakun = $("#kdakun").val();
        var mpl = $("#mpl").val();
        $.ajax({
                type: "POST",
                url: "<?=site_url("mstrefposbkl/addDetail");?>",
                data: {"kddiv":kddiv
                        ,"kdrefkb":kdrefkb
                        ,"nmrefkb":nmrefkb
                        ,"dk":dk
                        ,"faktif":faktif
                        ,"ket":ket
                        ,"nourut":nourut
                        ,"kdakun":kdakun
                        ,"mpl":mpl},
                beforeSend: function() {
                    refresh();
                },
                success: function(resp){
                    var obj = jQuery.parseJSON(resp);
                    if(obj.state!==0){
                        $("#kdrefkb").val(obj.kdrefkb);
                        refresh();
                        resetDetail();
                        getKodeAkun();
                        localStorage.setItem("refkb_m", JSON.stringify({"kdrefkb":obj.kdrefkb
                                                            ,"nmrefkb":nmrefkb
                                                            ,"dk":dk
                                                            ,"faktif":faktif
                                                            ,"ket":ket}));
                    }else{
                        swal("Error !", 'Error Message: ' + obj.msg, "error");
                    }
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    refresh();
                }
            });
    }

    function resetDetail(){
        $("#nourut").val('');
        $("#mpl").val('');

        //$("#kdakun").val(nmakun);
    }

    function deleted(kdrefkb,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("mstrefposbkl/addDetail");?>",
                data: {"kdrefkb":kdrefkb
                        ,"nourut":nourut
                        ,"stat":"delete"},
                success: function(resp){
                    var obj = jQuery.parseJSON(resp);
                    if(obj.state==="1"){
                        swal({
                            title: "Terhapus",
                            text: obj.msg,
                            type: "success"
                        }, function(){
                            refresh();
                        });
                    }else{
                        swal("Error", obj.msg, "error");
                    }
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        var stat = "<?php echo $this->uri->segment(2);?>";
        var kdrefkb = $("#kdrefkb").val();
        var kddiv = $("#kddiv").val();
        if(stat==="add" && kdrefkb!==""){
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
                var kdrefkb = $("#kdrefkb").val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("mstrefposbkl/submit");?>",
                    data: {"kdrefkb":kdrefkb
                            ,"stat":"delete"
                            ,"kddiv":kddiv},
                    success: function(resp){
                        var obj = jQuery.parseJSON(resp);
                        if(obj.state==="1"){
                            swal({
                                title: "Transaksi Dibatalkan",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                localStorage.removeItem("refkb_m");
                                window.location.href = '<?=site_url('mstrefposbkl');?>';
                            });
                        }else{
                            swal("Error", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            });

        }else{
            window.location.href = '<?=site_url('mstrefposbkl');?>';
        }
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var kdrefkb = $("#kdrefkb").val();
            var nmrefkb = $("#nmrefkb").val();
            var kddiv = $("#kddiv").val();
            var dk = $("#dk").val();
            var faktif = $("#faktif").val();
            var ket = $("#ket").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("mstrefposbkl/validasi");?>",
                data: {"kddiv":kddiv
                    ,"kdrefkb":kdrefkb
                    ,"nmrefkb":nmrefkb
                    ,"dk":dk
                    ,"faktif":faktif
                    ,"ket":ket},
                success: function(resp){
                    if(resp==="1.00" || resp==="1.0"|| resp==="1"){
                        swal({
                            title: "Transaksi Simpan",
                            text: "Transaksi Berhasil Di Simpan",
                            type: "success"
                        }, function(){
                            localStorage.removeItem("refkb_m");
                            window.location.href = '<?=site_url('mstrefposbkl');?>';
                        });
                    }else if(resp==="0"){
                        swal("Error", "Detail Data Akun Belum Di Isi", "error");
                    }else{
                        swal("Error", "Jumlah Persentase(%) Masih Salah ["+ (Number(resp) * 100) +"], Tidak boleh kurang dari 100 dan Tidak Boleh Lebih Dari 100", "error");
                    }
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function edit(kdrefkb,nourut,kdakun,mpl){
        $("#kdrefkb").val(kdrefkb);
        $("#nourut").val(nourut);
        $("#kdakun").val(kdakun);
        //$("#kdakun").val(nmakun);
        $("kdakun").trigger("chosen:updated");
        $('#kdakun').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });
        $("#mpl").val(mpl);
    }

    function getHeader(){
        if(localStorage.refkb_m){
            var obj = jQuery.parseJSON(localStorage.refkb_m);
            $("#kdrefkb").val(obj.kdrefkb);
            $("#nmrefkb").val(obj.nmrefkb);
            $("#dk").val(obj.dk);
            $("#faktif").val(obj.faktif);
            $("#ket").val(obj.ket);
        }
    }
</script>

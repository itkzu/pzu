<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>
			<div class="box-body"> 
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label('Pilih Periode Awal'); 
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Pilih Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							 ?>
						</div>

					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				</div>

				<div class="table-responsive">
					<table style="width: 100%;" class="dataTable table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th rowspan="2" style="width: 5%; text-align: center; vertical-align: middle;">NIK Sales</th>
								<th rowspan="2" style="width: 15%; text-align: center; vertical-align: middle;">Nama Sales</th>
								<th rowspan="2" style="width: 5%; text-align: center; vertical-align: middle;">NIK SPV</th>
								<th rowspan="2" style="width: 10%; text-align: center; vertical-align: middle;">Nama SPV</th>
								<th rowspan="2" style="width: 10%; text-align: center; vertical-align: middle;">NIK Kacab</th>
								<th rowspan="2" style="width: 10%; text-align: center; vertical-align: middle;">Nama Kacab</th>
								<th rowspan="2" style="width: 10%; text-align: center; vertical-align: middle;">Cabang</th> 
								<th colspan="3" style="width: 15%; text-align: center;">Tunai</th>
								<th colspan="3" style="width: 15%; text-align: center;">Kredit</th>
								<th rowspan="2" style="width: 5%; text-align: center; vertical-align: middle;">Total</th>
								
							</tr>
							<tr>
								<th style="width: 5%; text-align: center;">Non</th>
								<th style="width: 5%; text-align: center;">Half</th>
								<th style="width: 5%; text-align: center;">Full</th> 
								<th style="width: 5%; text-align: center;">Non</th>
								<th style="width: 5%; text-align: center;">Half</th>
								<th style="width: 5%; text-align: center;">Full</th> 
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		disable();  
		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});
		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$(".btn-tampil").click(function(){
			$(".dataTable").show();
			tampilData();
		});
	});

	function tampilData(){
  
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val(); 

		var column = []; 

        column.push({ 
                "aTargets": [ 7,8,9,10,11,12,13 ],
                "sClass": "right"
        });

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "nik" },
				{ "data": "nmsales" }, 
				{ "data": "nikspv" }, 
				{ "data": "nmspv" }, 
				{ "data": "nikkacab" }, 
				{ "data": "nmkacab" },
				{ "data": "cabang" },
				{ "data": "t_non" },
				{ "data": "t_half" },
				{ "data": "t_full" },
				{ "data": "k_non" },
				{ "data": "k_half" },
				{ "data": "k_full" },
				{ "data": "total" }, 
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( 
							{ "name": "periode_awal", "value": periode_awal },
							{ "name": "periode_akhir", "value": periode_akhir }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
        		$('td', row).eq(7).css('background-color', '#aeeeff');
        		$('td', row).eq(8).css('background-color', '#aeeeff');
        		$('td', row).eq(9).css('background-color', '#aeeeff');
        		$('td', row).eq(10).css('background-color', '#aeeeff');
        		$('td', row).eq(11).css('background-color', '#aeeeff');
        		$('td', row).eq(12).css('background-color', '#aeeeff');
        		$('td', row).eq(13).css('background-color', '#aeeeff'); 
			},
			"sAjaxSource": "<?=site_url('rptinsso/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
          	    $( api.column( 6 ).footer() ).html('Total');
                for(var i=7; i<=13; i++) {
		            // let sum = api.column(i).data().sum();
		            var sum = api
		            	.column(i)
		            	.data()
		            	.reduce( function (a, b) {
		            		return intVal(a) + intVal(b);
		            	}, 0);
		            $(api.column(i).footer()).html(numeral(sum).format('0,0'));
		        }
                // computing column Total of the complete result
                // var scooter_gear = api
                //     .column( 5 )
                //     .data()
                //     .reduce( function (a, b) {
                //         return intVal(a) + intVal(b);
                //     }, 0 );
              },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});
	}

	function disable(){
		$(".dataTable").hide();  
	} 
</script>

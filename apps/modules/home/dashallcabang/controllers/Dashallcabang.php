<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashallcabang
 *
 * @author adi
 */
class Dashallcabang extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('dashallcabang/submit'),
            'add' => site_url('dashallcabang/add'),
            'edit' => site_url('dashallcabang/edit'),
            'reload' => site_url('dashallcabang'),
        );
        $this->load->model('dashallcabang_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
        );
    }


    public function get_penjualan() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_Jual_All_Cab($value, $periode);
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"     => $value,
                    "mtd"       => "0",
                    "mtdx"      => "0",
                    "mtdiff"    => "0",
                    "ytd"       => "0",
                    "ytdx"      => "0",
                    "ytdiff"    => "0",
                    "stock"     => "0"
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    }



    public function get_target_penjualan() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_target_jual($value, $periode);
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"     => $value,
                    "target"    => "0",
                    "aktual"    => "0",
                    "diff"      => "0"
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    }




    public function get_blm_trm_po_leasing() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = date('Y-m-d');
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_blm_trm_po_leasing($value, $periode);
            if(json_decode($res) !== null){
                array_push($data, json_decode($res));
            }
        }
        echo json_encode($data);
    }
    
    public function get_blm_trm_po_leasing_detail() {
        $data = $this->dashallcabang_qry->get_blm_trm_po_leasing_detail();
        $arr = json_decode($data, true);
        $res = array();
        $no = 0;
        foreach ($arr as $dt) {
            foreach ($dt as $k => $val) {
                if(is_numeric($val)){
                    $res[$no][$k] = (float) $val;
                }else{
                    $res[$no][$k] = $val;
                }   
            }
            $no++;
        }
        echo json_encode($res);
    }

    public function getJualTunaiKreditPerBulan() {
        echo $this->dashallcabang_qry->getJualTunaiKreditPerBulan(); 
    } 


    public function get_penjualan_leas() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_Jual_All_Cab_leasing($value, $periode);
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"     => $value,
                    "mtdadr"    => "0",
                    "mtdfif"    => "0",
                    "mtdbca"    => "0",
                    "mtdoto"    => "0",
                    "mtdmuf"    => "0",
                    "mtdothers" => "0", 
                    "ytdadr"    => "0",
                    "ytdfif"    => "0",
                    "ytdbca"    => "0",
                    "ytdoto"    => "0",
                    "ytdmuf"    => "0",
                    "ytdothers" => "0", 
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    }


    public function get_penjualan_tipex() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_Jual_tipex_All_Cab($value, $periode);
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"         => $value,
                    "cub_l"         => "0",
                    "cub_m"         => "0",
                    "cub_h"         => "0",
                    "at_l"          => "0",
                    "at_m"          => "0",
                    "at_h"          => "0",
                    "sport_l"       => "0",
                    "sport_m"       => "0",
                    "sport_h"       => "0",
                    "prem"          => "0"
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    }


    public function get_penjualan_tipe() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_Jual_tipe_All_Cab($value, $periode);
            // echo $res;
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"         => $value,
                    "cub_l"         => "0",
                    "cub_m"         => "0",
                    "cub_h"         => "0",
                    "at_l"          => "0",
                    "at_m"          => "0",
                    "at_h"          => "0",
                    "sport_l"       => "0",
                    "sport_m"       => "0",
                    "sport_h"       => "0",
                    "prem"          => "0"
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    } 


    public function get_penjualan_tipe_year() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_Jual_tipe_year_All_Cab($value, $periode);
            // echo $res;
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"         => $value,
                    "cub_l"         => "0",
                    "cub_m"         => "0",
                    "cub_h"         => "0",
                    "at_l"          => "0",
                    "at_m"          => "0",
                    "at_h"          => "0",
                    "sport_l"       => "0",
                    "sport_m"       => "0",
                    "sport_h"       => "0",
                    "prem"          => "0",
                    "stok"          => "0"
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    } 


    public function get_stok_m() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = $this->apps->dateConvert( $this->input->post('periode') );
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_stok_m_All_Cab($value, $periode);
            // echo $res;
            if(json_decode($res) !== null and json_decode($res) !== 0){
                array_push($data, json_decode($res));
            } else{


                $data2 = array(
                    array(
                    "kddiv"         => $value,
                    "cub_l"         => "0",
                    "cub_m"         => "0",
                    "cub_h"         => "0",
                    "at_l"          => "0",
                    "at_m"          => "0",
                    "at_h"          => "0",
                    "sport_l"       => "0",
                    "sport_m"       => "0",
                    "sport_h"       => "0",
                    "prem"          => "0",
                    "stok"          => "0"
                    ) );

                array_push($data, $data2);
            }
        }
        echo json_encode($data);
    } 
}

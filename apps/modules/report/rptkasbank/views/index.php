<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <?php
                            echo form_label('Pilih Ref Kas/Bank');
                            echo form_dropdown($form['kdkb']['name'],$form['kdkb']['data'] ,$form['kdkb']['value'] ,$form['kdkb']['attr']);
                            echo form_error('kdkb','<div class="note">','</div>'); 
                        ?>
                    </div> 
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_awal']['placeholder']);
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_akhir']['placeholder']);
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                <table style="width: 1500px;" class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="width: 10px; text-align: center;">NO. CETAK</th>
                            <th style="width: 100px; text-align: center;">NO. K/B</th>
                            <th style="width: 50px; text-align: center;">TANGGAL</th>
                            <th style="text-align: center;">JENIS</th>
                            <th style="text-align: center;">DARI/KEPADA</th>
                            <th style="text-align: center;">KETERANGAN</th>
                            <th style="width: 120px; text-align: center;">DEBET</th>
                            <th style="width: 120px; text-align: center;">KREDIT</th>
                            <th style="width: 120px; text-align: center;">SALDO</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">NO. CETAK</th>
                            <th style="text-align: center;">NO. K/B</th>
                            <th style="text-align: center;">TANGGAL</th>
                            <th style="text-align: center;">JENIS</th>
                            <th style="text-align: center;">DARI/KEPADA</th>
                            <th style="text-align: center;">KETERANGAN</th>
                            <th style="text-align: center;">DEBET</th>
                            <th style="text-align: center;">KREDIT</th>
                            <th style="text-align: center;">SALDO</th>
                        </tr>                        
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });


        var column = [];
        for (i = 6; i <= 8; i++) { 
            if(i===8){
                column.push({ 
                    "bSortable": false,
                    "aTargets": [ i ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return formmatedvalue;
                        },
                    "sClass": "right"
                    });
            }else{
                column.push({ 
                    "aTargets": [ i ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return formmatedvalue;
                        },
                    "sClass": "right"
                    });
            }
        }

        column.push({ 
                "aTargets": [ 2 ],
                "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                },
                "sClass": "center"
        });



        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,

            "fixedColumns": {
                leftColumns: 1
            },



            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "kdkb", "value": $("#kdkb").val() }
                            ,{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            ,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptkasbank/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete"  && title!=="SALDO"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>
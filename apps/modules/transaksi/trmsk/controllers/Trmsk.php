<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Trmsk
 *
 * @author
 */
class Trmsk extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(    
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('trmsk/submit'),
            'add' => site_url('trmsk/add'),
            'edit' => site_url('trmsk/edit'),
            'reload' => site_url('trmsk'),
        );
        $this->load->model('trmsk_qry');
        $supplier = $this->trmsk_qry->getkota();
        foreach ($supplier as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 

    public function json_dgview() {
        echo $this->trmsk_qry->json_dgview();
    }

    public function set_trmsk() {
        echo $this->trmsk_qry->set_trmsk();
    }

    public function set_nosin() {
        echo $this->trmsk_qry->set_nosin();
    }

    public function set_ket() {
        echo $this->trmsk_qry->set_ket();
    }

    public function getDtsk() {
        echo $this->trmsk_qry->getDtsk();
    }   

    public function json_dgview_detail() {
        echo $this->trmsk_qry->json_dgview_detail();
    }   

    public function addDetail() {
        echo $this->trmsk_qry->addDetail();
    }   

    public function submit() {
        echo $this->trmsk_qry->submit();
    }

    public function batal() {
        echo $this->trmsk_qry->batal();
    }
    private function _init_add(){ 

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
            'cari'=> array(
                    'attr'        => array(
                            'id'    => 'cari',
                            'class' => 'form-control ',
                    ),
                    'data'     => '',
                    'placeholder' => 'Cari',
                    'name'        => 'cari',
                    'value'       => set_value('cari'),
            ), 
            'nosin'     => array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),
            'nora'     => array(
                    'placeholder' => 'No. Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'kdtipe'     => array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nama'     => array(
                    'placeholder' => 'Nama Konsumen',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'alamat'     => array(
                    'placeholder' => 'Alamat Konsumen',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'kota'     => array(
                    'placeholder' => '',
                    //'type'        => 'hidden',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'jnsbayar'     => array(
                    'placeholder' => 'Tunai/Kredit',
                    //'type'        => 'hidden',
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'value'       => set_value('jnsbayar'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nodo'     => array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tgldo'     => array(
                    'placeholder' => 'Tanggal DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nmsales'     => array(
                    'placeholder' => 'Sales',
                    //'type'        => 'hidden',
                    'id'          => 'nmsales',
                    'name'        => 'nmsales',
                    'value'       => set_value('nmsales'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 


            //table
            'trm_nts'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_nts',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_nts',
                    'type'        => 'checkbox',
            ),
            'trm_stnk'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_stnk',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_stnk',
                    'type'        => 'checkbox',
            ),
            'trm_tnkb'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_tnkb',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_tnkb',
                    'type'        => 'checkbox',
            ),
            'trm_bpkb'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_bpkb',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_bpkb',
                    'type'        => 'checkbox',
            ),
            'srh_nts'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_nts',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_nts',
                    'type'        => 'checkbox',
            ),
            'srh_stnk'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_stnk',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_stnk',
                    'type'        => 'checkbox',
            ),
            'srh_tnkb'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_tnkb',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_tnkb',
                    'type'        => 'checkbox',
            ),
            'srh_bpkb'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_bpkb',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_bpkb',
                    'type'        => 'checkbox',
            ),

            // tambahan table
            'nopol'     => array(
                    'placeholder' => 'Nopol',
                    //'type'        => 'hidden',
                    'id'          => 'nopol',
                    'name'        => 'nopol',
                    'value'       => set_value('nopol'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'nonotis'     => array(
                    'placeholder' => 'No. Notis',
                    //'type'        => 'hidden',
                    'id'          => 'nonotis',
                    'name'        => 'nonotis',
                    'value'       => set_value('nonotis'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'nostnk'     => array(
                    'placeholder' => 'No. STNK',
                    //'type'        => 'hidden',
                    'id'          => 'nostnk',
                    'name'        => 'nostnk',
                    'value'       => set_value('nostnk'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'pajak'     => array(
                    'placeholder' => 'Pajak',
                    //'type'        => 'hidden',
                    'id'          => 'pajak',
                    'name'        => 'pajak',
                    'value'       => set_value('pajak'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'tglttp'     => array(
                    'placeholder' => 'Tgl Penetapan',
                    //'type'        => 'hidden',
                    'id'          => 'tglttp',
                    'name'        => 'tglttp',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nobpkb'     => array(
                    'placeholder' => 'No. BPKB',
                    //'type'        => 'hidden',
                    'id'          => 'nobpkb',
                    'name'        => 'nobpkb',
                    'value'       => set_value('nobpkb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nodo = $this->uri->segment(3);
            // echo $nodo;
        }
        $this->_check_id($nodo);

        if($this->val[0]['tgl_trm_notis'] === null){
            $trm_notis = false;
        } else {
            $trm_notis = true;
        }

        if($this->val[0]['tgl_trm_stnk'] === null){
            $trm_stnk = false;
        } else {
            $trm_stnk = true;
        }

        if($this->val[0]['tgl_trm_tnkb'] === null){
            $trm_tnkb = false;
        } else {
            $trm_tnkb = true;
        }

        if($this->val[0]['tgl_trm_bpkb'] === null){
            $trm_bpkb = false;
        } else {
            $trm_bpkb = true;
        }

        if($this->val[0]['tgl_srh_notis'] === null){
            $srh_notis = false;
        } else {
            $srh_notis = true;
        }

        if($this->val[0]['tgl_srh_stnk'] === null){
            $srh_stnk = false;
        } else {
            $srh_stnk = true;
        }

        if($this->val[0]['tgl_srh_tnkb'] === null){
            $srh_tnkb = false;
        } else {
            $srh_tnkb = true;
        }

        if($this->val[0]['tgl_srh_bpkb'] === null){
            $srh_bpkb = false;
        } else {
            $srh_bpkb = true;
        }
        $this->data['form'] = array(      
            'nosin'     => array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => $this->val[0]['nosin'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'nora'     => array(
                    'placeholder' => 'No. Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => $this->val[0]['nora'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'kdtipe'     => array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => $this->val[0]['kdtipe'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nama'     => array(
                    'placeholder' => 'Nama Konsumen',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => $this->val[0]['nama'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'alamat'     => array(
                    'placeholder' => 'Alamat Konsumen',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => $this->val[0]['alamat'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'kota'     => array(
                    'placeholder' => '',
                    //'type'        => 'hidden',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => $this->val[0]['kota'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'jnsbayar'     => array(
                    'placeholder' => 'Tunai/Kredit',
                    //'type'        => 'hidden',
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'value'       => $this->val[0]['tk'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nodo'     => array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => $this->val[0]['nodo'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tgldo'     => array(
                    'placeholder' => 'Tanggal DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => $this->apps->dateConvert($this->val[0]['tgldo']),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nmsales'     => array(
                    'placeholder' => 'Sales',
                    //'type'        => 'hidden',
                    'id'          => 'nmsales',
                    'name'        => 'nmsales',
                    'value'       => $this->val[0]['nmsales'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 


            //table
            'trm_nts'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_nts',
                    // 'value'       => 't',
                    'checked'     => $trm_notis,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_nts',
                    'type'        => 'checkbox',
            ),
            'trm_stnk'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_stnk',
                    // 'value'       => 't', 
                    'checked'     => $trm_stnk,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_stnk',
                    'type'        => 'checkbox',
            ),
            'trm_tnkb'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_tnkb',
                    // 'value'       => 't',
                    'checked'     => $trm_tnkb,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_tnkb',
                    'type'        => 'checkbox',
            ),
            'trm_bpkb'=> array(
                    'placeholder' => '',
                    'id'          => 'trm_bpkb',
                    // 'value'       => 't',
                    'checked'     => $trm_bpkb,
                    'class'       => 'custom-control-input',
                    'name'        => 'trm_bpkb',
                    'type'        => 'checkbox',
            ),
            'srh_nts'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_nts',
                    // 'value'       => 't',
                    'checked'     => $srh_notis,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_nts',
                    'type'        => 'checkbox',
            ),
            'srh_stnk'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_stnk',
                    // 'value'       => 't',
                    'checked'     => $srh_stnk,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_stnk',
                    'type'        => 'checkbox',
            ),
            'srh_tnkb'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_tnkb',
                    // 'value'       => 't',
                    'checked'     => $srh_tnkb,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_tnkb',
                    'type'        => 'checkbox',
            ),
            'srh_bpkb'=> array(
                    'placeholder' => '',
                    'id'          => 'srh_bpkb',
                    // 'value'       => 't',
                    'checked'     => $srh_bpkb,
                    'class'       => 'custom-control-input',
                    'name'        => 'srh_bpkb',
                    'type'        => 'checkbox',
            ),

            //detail
            'nopol'     => array(
                    'placeholder' => 'Nopol',
                    //'type'        => 'hidden',
                    'id'          => 'nopol',
                    'name'        => 'nopol',
                    'value'       => $this->val[0]['nopol'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),  
            'nonotis'     => array(
                    'placeholder' => 'No. Notis',
                    //'type'        => 'hidden',
                    'id'          => 'nonotis',
                    'name'        => 'nonotis',
                    'value'       => $this->val[0]['nonotis'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),  
            'nostnk'     => array(
                    'placeholder' => 'No. STNK',
                    //'type'        => 'hidden',
                    'id'          => 'nostnk',
                    'name'        => 'nostnk',
                    'value'       => $this->val[0]['nostnk'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),  
            'pajak'     => array(
                    'placeholder' => 'Pajak',
                    //'type'        => 'hidden',
                    'id'          => 'pajak',
                    'name'        => 'pajak',
                    'value'       => $this->val[0]['pajak_bbn'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    // 'readonly' => '',
            ), 
            'tglttp'     => array(
                    'placeholder' => 'Tgl Penetapan',
                    //'type'        => 'hidden',
                    'id'          => 'tglttp',
                    'name'        => 'tglttp',
                    'value'       => $this->apps->dateConvert($this->val[0]['tgl_penetapan_notis']),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly' => '',
            ),
            'nobpkb'     => array(
                    'placeholder' => 'No. BPKB',
                    //'type'        => 'hidden',
                    'id'          => 'nobpkb',
                    'name'        => 'nobpkb',
                    'value'       => $this->val[0]['nobpkb'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),  
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->trmsk_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

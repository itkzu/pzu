<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Unpostkb
 *
 * @author
 */

class Unpostkb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('unpostkb/submit'),
            'add' => site_url('unpostkb/add'),
            'edit' => site_url('unpostkb/edit'),
            'reload' => site_url('unpostkb'),
        );
        $this->load->model('unpostkb_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function getData() {
        echo $this->unpostkb_qry->getData();
    }
    
    public function proses(){
        echo $this->unpostkb_qry->proses();
    }

    /*
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->unpostkb_qry->submit();
            echo $res;
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    */
    
    private function _init_add(){
        $this->data['form'] = array(
           'nokb'=> array(
                    'placeholder' => 'Nomor Transaksi',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    //'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }
    
    /*
    private function validate() {
        $config = array(
            array(
                    'field' => 'nodo',
                    'label' => 'No. DO',
                    'rules' => 'required|max_length[10]',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
    */
}

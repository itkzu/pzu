<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <?php 
                                echo form_label($form['nopo']['placeholder']);
                            ?>

                            <div class="input-group">
                                <?php 
                                    echo form_input($form['nopo']);
                                    echo form_error('nopo','<div class="note">','</div>'); 
                                ?>

                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-tampil" id="btn-tampil">Tampil</button>
                                    <button type="button" class="btn btn-success btn-reset">Reset</button>
                                    <button type="button" class="btn btn-danger btn-unposting">Unposting</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- detail transaksi via ajax -->
                <div id="detail"></div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //$("#nopo").mask("a99-999999");

        //init
        $(".btn-unposting").hide();
        $(".btn-reset").hide();
        $("#nopo").focus();

        $(".btn-tampil").click(function(){           
            getData();
        });

        $(".btn-reset").click(function(){
            reset();
        });

        $(".btn-unposting").click(function(){
            unposting();
        });
        



        var input = document.getElementById("nopo");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("btn-tampil").click();
            }
        }); 


    });

    function buttonState(){
        $(".btn-tampil").toggle();  
        $(".btn-unposting").toggle();  
        $(".btn-reset").toggle();  
    }

    function reset(){
        //$(".form-control").val("");
        buttonState();
        $("#detail").html('');

        //$("#nopo").val('');
        $("#nopo").prop("readonly", false);
        $("#nopo").focus(); $("#nopo").select();
    }
    
    function getData(){
        var nopo = $("#nopo").val();
        $.ajax({
            type: "POST",

            //dataType: "html",

            url: "<?=site_url('unpostpo/getData');?>",
            data: {"nopo":nopo},
            success: function(resp){  
                if(resp){

                    var obj = JSON.parse(resp);
                    //alert(JSON.stringify(resp));

                    if (jQuery.isEmptyObject(obj)){
                        //alert('empty');
                        swal({
                            title: "Error",
                            text: "Data No. PO : " + nopo + " tidak ditemukan! ",
                            type: "error"
                        }, function(){
                            
                            $("#nopo").val('');
                            $("#nopo").focus();
                        });
                    } else{
                        $("#nopo").prop("readonly", true);
                        var detailhtml = "";
                        $.each(obj, function(key, data){
                            detailhtml = '<hr>'+
                                            '<div class="row">'+
                                            '    <div class="col-lg-6">'+
                                            '        <div class="form-group">'+
                                            '            <label>Tanggal Faktur</label><input type="text" value="'+data.tglpo+'" placeholder="" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Tanggal Terima</label><input type="text" value="'+data.tglterima+'" placeholder="" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '    </div>'+

                                            '    <div class="col-lg-6">'+
                                            '        <div class="form-group">'+
                                            '            <label>Supplier</label><input type="text" value="'+data.nmsup+'" placeholder="" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Jatuh Tempo</label><input type="text" value="'+data.tgljt+'" placeholder="" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '    </div>'+

                                            '</div>'+
                                            ' ';
                        });

                        $("#detail").html(detailhtml);

                        buttonState();
                    }
            
                }else{
                    swal("Error", obj.msg, "error");
                }
            },
            error: function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });        
    }
    
    function unposting(){
        swal({
            title: "Konfirmasi",
            text: "Proses Unposting Transaksi Pembelian akan dilakukan, data tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            }, 
            
            function () {
                var nopo = $("#nopo").val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('unpostpo/proses');?>",
                    data: {"nopo":nopo},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                $("#nopo").val('');
                                reset();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }

    /*
    function batalFaktur(){
        swal({
            title: "Konfirmasi Pembatalan Faktur !",
            text: "Faktur Astra akan dibatalkan, data tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                var submit = "<?php echo $submit;?>"; 
                var nopo = $("#iddo").val();
                $.ajax({
                    type: "POST",
                    url: submit,
                    data: {"nopo":nopo,"stat":"cancel"},
                    success: function(resp){   
                        var obj = jQuery.parseJSON(resp);
                        if(obj.state==="1"){
                            swal({
                                title: "Pembatalan Berhasil",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                reset();
                            });
                        }else{
                            swal("Pembatalan Gagal", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
    */
</script>
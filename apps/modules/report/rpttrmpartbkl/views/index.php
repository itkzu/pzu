<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: none;
    }
    .radio {
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .checkbox label, .radio label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 5px;
        font-weight: bold;
        cursor: pointer;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'target' => '_blank'
                    , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>
            <div class="box-body"> 
                
                <div class="form-group">
                   
                    <?php 
                        echo form_label($form['kddiv']['placeholder']);
                        echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                        echo form_error('kddiv','<div class="note">','</div>');  
                    ?>
                    
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php 
                                echo form_label('Pilih Periode Awal');
                                echo form_input($form['periode_awal']);
                                echo form_error('periode_awal','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">

                            <?php
                            
                               echo form_label('Pilih Periode Akhir'); 
                                echo form_input($form['periode_akhir']);
                                echo form_error('periode_akhir','<div class="note">','</div>');
                             ?>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                </div>

                <div class="col-md-12">
                    <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                </div>

                    <!-- <div class="main-form" style="width: 900px;"> -->
                    <div class="nav-tabs-custom tab">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active">
                                <a href="#tab_1_1" class="tab_tunai" data-toggle="tab"> Pembelian Tunai </a>
                            </li>
                            <li>
                                <a href="#tab_1_2" class="tab_astra" data-toggle="tab"> Pembelian ASTRA </a>
                            </li> 
                            <li>
                                <a href="#tab_1_3" class="tab_bag" data-toggle="tab"> Pembelian BAG </a>
                            </li> 
                        </ul>

                        <div class="tab-content">

                            <!--tab_1_1-->
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="row"> 

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            
                                            <button type="submit" class="btn btn-default" name="submit" value="tunai" >
                                                <i class="fa fa-print"></i> Cetak
                                            </button>
                                            <button type="submit" class="btn btn-success" name="submit" value="tunai_excel">
                                                <i class="fa fa-file-excel-o"></i> Excel
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-md-12"> 
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered display nowrap tunai_dtTable" style="width: 100%;" class="display select">
                                                <thead>
                                                    <tr> 
                                                        <th style="width: 10%;text-align: center;">No</th>
                                                        <th style="width: 5%;text-align: center;">Tanggal</th>
                                                        <th style="width: 20%;text-align: center;">No. Faktur</th>
                                                        <th style="width: 20%;text-align: center;">Nama Supplier</th>
                                                        <th style="width: 20%;text-align: center;">Kode Sparepart</th>
                                                        <th style="width: 20%;text-align: center;">Nama Sparepart</th> 
                                                        <th style="width: 5%;text-align: center;">Quantity</th>
                                                        <th style="width: 10%;text-align: center;">Harga /pcs</th>
                                                        <th style="width: 10%;text-align: center;">Discount</th>
                                                        <th style="width: 10%;text-align: center;">Harga Neto /pcs</th> 
                                                        <th style="width: 10%;text-align: center;">Jumlah</th> 
                                                    </tr>
                                                </thead>
                                                <tbody class="tunai_bd"></tbody>
                                            </table>
                                        </div>
                                    </div>  
                                </div>
                            </div> 

                            <!-- tab_1_2 -->
                            <div class="tab-pane" id="tab_1_2">
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            
                                            <button type="submit" class="btn btn-default" name="submit" value="astra" >
                                                <i class="fa fa-print"></i> Cetak
                                            </button>
                                            <button type="submit" class="btn btn-success" name="submit" value="astra_excel">
                                                <i class="fa fa-file-excel-o"></i> Excel
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-md-12"> 
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered display nowrap astra_dtTable" style="width: 100%;" class="display select">
                                                <thead>
                                                    <tr> 
                                                        <th style="width: 10%;text-align: center;">No</th>
                                                        <th style="width: 5%;text-align: center;">Tanggal</th>
                                                        <th style="width: 20%;text-align: center;">No. Faktur</th>
                                                        <th style="width: 20%;text-align: center;">Nama Supplier</th>
                                                        <th style="width: 20%;text-align: center;">Kode Sparepart</th>
                                                        <th style="width: 20%;text-align: center;">Nama Sparepart</th> 
                                                        <th style="width: 5%;text-align: center;">Quantity</th>
                                                        <th style="width: 10%;text-align: center;">Jumlah</th>
                                                        <th style="width: 10%;text-align: center;">HPP /unit</th>
                                                        <th style="width: 10%;text-align: center;">Total HPP</th> 
                                                        <th style="width: 10%;text-align: center;">PPN</th> 
                                                    </tr>
                                                </thead>
                                                <tbody class="astra_bd"></tbody>
                                            </table>
                                        </div>
                                    </div> 
                                </div>
                            </div> 

                                <!--tab_1_3-->
                            <div class="tab-pane" id="tab_1_3">
                                <div class="row"> 
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            
                                            <button type="submit" class="btn btn-default" name="submit" value="bag" >
                                                <i class="fa fa-print"></i> Cetak
                                            </button>
                                            <button type="submit" class="btn btn-success" name="submit" value="bag_excel">
                                                <i class="fa fa-file-excel-o"></i> Excel
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-md-12"> 
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered display nowrap bag_dtTable" style="width: 100%;" class="display select">
                                                <thead>
                                                    <tr> 
                                                        <th style="width: 10%;text-align: center;">No</th>
                                                        <th style="width: 5%;text-align: center;">Tanggal</th>
                                                        <th style="width: 20%;text-align: center;">No. Faktur</th>
                                                        <th style="width: 20%;text-align: center;">Nama Supplier</th>
                                                        <th style="width: 20%;text-align: center;">Kode Sparepart</th>
                                                        <th style="width: 20%;text-align: center;">Nama Sparepart</th> 
                                                        <th style="width: 5%;text-align: center;">Quantity</th>
                                                        <th style="width: 10%;text-align: center;">Harga /pcs</th>
                                                        <th style="width: 10%;text-align: center;">Discount</th>
                                                        <th style="width: 10%;text-align: center;">Total</th> 
                                                    </tr>
                                                </thead>
                                                <tbody class="bag_bd"></tbody>
                                            </table>
                                        </div>
                                    </div> 
                                </div>
                            </div> 

                            <!--end tab-pane-->
                        </div>
                    </div> 
                <?php echo form_close(); ?>  
            </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        disable(); 

        $('#periode_awal').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        $('#periode_akhir').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        $(".btn-tampil").click(function(){
            $(".tab").show();
            get_data_tunai();
        });

        $('.tab_tunai').click(function(){  
            get_data_tunai();
        });

        $('.tab_astra').click(function(){  
            get_data_astra();
        });

        $('.tab_bag').click(function(){  
            get_data_bag();
        }); 
    });
    
    function get_data_tunai(){
        if ( $.fn.DataTable.isDataTable('.tunai_dtTable') ) {
          $('.tunai_dtTable').DataTable().destroy();
          $(".tunai_bd").html('');
        }
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();
        var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rpttrmpartbkl/get_data_tunai');?>",
            data: {"periode_awal":periode_awal, "periode_akhir":periode_akhir, "kddiv":kddiv},
            beforeSend: function(){
                $(".tunai_bd").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){  
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var grand_harga_total = 0;
                    var grand_disc_total = 0;
                    var grand_neto_total = 0;
                    var grand_tot_total = 0;
                    $.each(obj.leasing, function(key, leas){
                        var sub_harga_total = 0;
                        var sub_disc_total = 0;
                        var sub_neto_total = 0;
                        var sub_tot_total = 0;
                        $.each(obj.data, function(key, data){
                            if(leas===data.kdgrup){
                                $(".tunai_bd").append('<tr>' + 
                                            '<td style="text-align: center;">'+data.no+'</td>' + 
                                            '<td style="text-align: center;">'+data.podate+'</td>' + 
                                            '<td style="text-align: left;">'+data.nofaktur+'</td>' + 
                                            '<td style="text-align: left;">'+data.nmsup+'</td>' + 
                                            '<td style="text-align: left;">'+data.materialno+'</td>' + 
                                            '<td style="text-align: left;">'+data.nmpart+'</td>' +   
                                            '<td style="text-align: center;">'+data.qty+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.hargabeli)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.nilaidiscount)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.hrgneto)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.tot)).format('0,0.00')+'</td>' +  
                                        '</tr>');
                                sub_harga_total = Number(sub_harga_total) + Number(data.hargabeli);
                                sub_disc_total  = Number(sub_disc_total) + Number(data.nilaidiscount);
                                sub_neto_total  = Number(sub_neto_total) + Number(data.hrgneto);
                                sub_tot_total   = Number(sub_tot_total) + Number(data.tot);
                            }
                        }); 
                        
                    $(".tunai_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;">TOTAL '+leas+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_harga_total)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_disc_total)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_neto_total)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_tot_total)).format('0,0.00')+'</td>' + 
                            '</tr>');
                        grand_harga_total = Number(grand_harga_total) + Number(sub_harga_total);
                        grand_disc_total = Number(grand_disc_total) + Number(sub_disc_total);
                        grand_neto_total = Number(grand_neto_total) + Number(sub_neto_total);
                        grand_tot_total = Number(grand_tot_total) + Number(sub_tot_total);
                    });
                    
                        
                    $(".tunai_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;">GRAND TOTAL</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_harga_total)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_disc_total)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_neto_total)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_tot_total)).format('0,0.00')+'</td>' + 
                            '</tr>');
                }    
              $(".tunai_dtTable").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    },
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false
                });                
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",true);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
    
    function get_data_astra(){
        if ( $.fn.DataTable.isDataTable('.astra_dtTable') ) {
          $('.astra_dtTable').DataTable().destroy();
          $(".astra_bd").html('');
        }
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();
        var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rpttrmpartbkl/get_data_astra');?>",
            data: {"periode_awal":periode_awal, "periode_akhir":periode_akhir, "kddiv":kddiv},
            beforeSend: function(){
                $(".astra_bd").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){  
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var grand_jumlah = 0;
                    var grand_hppunit = 0;
                    var grand_hpptot = 0;
                    var grand_ppn = 0;
                    $.each(obj.leasing, function(key, leas){
                        var sub_jumlah = 0;
                        var sub_hppunit = 0;
                        var sub_hpptot = 0;
                        var sub_ppn = 0;
                        $.each(obj.data, function(key, data){
                            if(leas===data.kdgrup){
                                $(".astra_bd").append('<tr>' + 
                                            '<td style="text-align: center;">'+data.no+'</td>' + 
                                            '<td style="text-align: center;">'+data.podate+'</td>' + 
                                            '<td style="text-align: left;">'+data.nofaktur+'</td>' + 
                                            '<td style="text-align: left;">'+data.nmsup+'</td>' + 
                                            '<td style="text-align: left;">'+data.materialno+'</td>' + 
                                            '<td style="text-align: left;">'+data.nmpart+'</td>' +   
                                            '<td style="text-align: center;">'+data.qty+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.jumlah)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.hppunit)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.hpptot)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.ppn)).format('0,0.00')+'</td>' +  
                                        '</tr>');
                                sub_jumlah = Number(sub_jumlah) + Number(data.jumlah);
                                sub_hppunit  = Number(sub_hppunit) + Number(data.hppunit);
                                sub_hpptot  = Number(sub_hpptot) + Number(data.hpptot);
                                sub_ppn   = Number(sub_ppn) + Number(data.ppn);
                            }
                        }); 
                        
                    $(".astra_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;">TOTAL '+leas+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_jumlah)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_hppunit)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_hpptot)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_ppn)).format('0,0.00')+'</td>' + 
                            '</tr>');
                        grand_jumlah = Number(grand_jumlah) + Number(sub_jumlah);
                        grand_hppunit = Number(grand_hppunit) + Number(sub_hppunit);
                        grand_hpptot = Number(grand_hpptot) + Number(sub_hpptot);
                        grand_ppn = Number(grand_ppn) + Number(sub_ppn);
                    });
                    
                        
                    $(".astra_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;">GRAND TOTAL</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_jumlah)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_hppunit)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_hpptot)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_ppn)).format('0,0.00')+'</td>' + 
                            '</tr>');
                }    
              $(".astra_dtTable").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    },
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false
                });                
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",true);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
    
    function get_data_bag(){
        if ( $.fn.DataTable.isDataTable('.bag_dtTable') ) {
          $('.bag_dtTable').DataTable().destroy();
          $(".bag_bd").html('');
        }
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();
        var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rpttrmpartbkl/get_data_bag');?>",
            data: {"periode_awal":periode_awal, "periode_akhir":periode_akhir, "kddiv":kddiv},
            beforeSend: function(){
                $(".bag_bd").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){  
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var g_hargabeli = 0;
                    var g_nilaidiscount = 0;
                    var g_hrgtot = 0; 
                    $.each(obj.leasing, function(key, leas){
                        var s_hargabeli = 0;
                        var s_nilaidiscount = 0;
                        var s_hrgtot = 0; 
                        $.each(obj.data, function(key, data){
                            if(leas===data.kdgrup){
                                $(".bag_bd").append('<tr>' + 
                                            '<td style="text-align: center;">'+data.no+'</td>' + 
                                            '<td style="text-align: center;">'+data.podate+'</td>' + 
                                            '<td style="text-align: left;">'+data.nofaktur+'</td>' + 
                                            '<td style="text-align: left;">'+data.nmsup+'</td>' + 
                                            '<td style="text-align: left;">'+data.materialno+'</td>' + 
                                            '<td style="text-align: left;">'+data.nmpart+'</td>' +   
                                            '<td style="text-align: center;">'+data.qty+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.hargabeli)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.nilaidiscount)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.hrgtot)).format('0,0.00')+'</td>' + 
                                        '</tr>');
                                s_hargabeli = Number(s_hargabeli) + Number(data.hargabeli);
                                s_nilaidiscount  = Number(s_nilaidiscount) + Number(data.nilaidiscount);
                                s_hrgtot  = Number(s_hrgtot) + Number(data.hrgtot); 
                            }
                        }); 
                        
                    $(".bag_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;">TOTAL '+leas+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(s_hargabeli)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(s_nilaidiscount)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(s_hrgtot)).format('0,0.00')+'</td>' +  
                            '</tr>');
                        g_hargabeli = Number(g_hargabeli) + Number(s_hargabeli);
                        g_nilaidiscount = Number(g_nilaidiscount) + Number(s_nilaidiscount);
                        g_hrgtot = Number(g_hrgtot) + Number(s_hrgtot); 
                    });
                    
                        
                    $(".bag_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;">GRAND TOTAL</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(g_hargabeli)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(g_nilaidiscount)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(g_hrgtot)).format('0,0.00')+'</td>' +  
                            '</tr>');
                }    
              $(".bag_dtTable").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    },
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false
                });                
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",true);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }

    function disable(){
        $(".tab").hide();
    } 
</script>
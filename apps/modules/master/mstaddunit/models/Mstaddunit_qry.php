<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstaddunit_qry
 *
 * @author adi
 */
class Mstaddunit_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function select_data($kode) {
        $this->db->select("*");
        $this->db->where('kode',$kode);
        $query = $this->db->get('pzu.tipe');
        return $query->result_array();
    }

    public function getKdSeries() { 
      $query = $this->db->query("select a.kdtipesr,a.nmtipesr,b.nmtipegrp from pzu.tipe_series a inner join pzu.tipe_group b on a.kdtipegrp=b.kdtipegrp where a.faktif=true order by a.nmtipesr");
      // echo $this->db->last_query(); 
        return $query->result_array();
    }             

    public function gettipegrp() { 
      $kdtipesr = $this->input->post('kdtipesr');
      $query = $this->db->query("select a.kdtipesr,a.nmtipesr,b.nmtipegrp from pzu.tipe_series a inner join pzu.tipe_group b on a.kdtipegrp=b.kdtipegrp where a.faktif=true and a.kdtipesr = '".$kdtipesr."' order by a.nmtipesr ");
      // echo $this->db->last_query(); 
        $res = $query->result_array();
      return json_encode($res);
    }             

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kode']) ){
            $id = $_GET['kode'];
        }else{
            $id = '';
        }

        $aColumns = array('no',
                            'kode',
                            'kdtipe',
                            'nmtipe',
                            'nmtipesr',
                            'nmtipegrp',
                            'beli',
                            'jual',
                            'jual_off',
                            'bbn',
                            'faktifx',
                            'kode');
	$sIndexColumn = "kode";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT ''::varchar as no,kode,kdtipe,nmtipe,kdtipesr,nmtipesr,nmtipegrp,beli,jual,jual_off,bbn,faktif, CASE WHEN faktif=true THEN 'AKTIF' ELSE 'TIDAK AKTIF' END AS faktifx FROM pzu.v_tipe ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
      {
          if($_GET['iDisplayStart']>0){
              $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                      intval( $_GET['iDisplayStart'] );
          }
      }

      $sOrder = "";
      if ( isset( $_GET['iSortCol_0'] ) )
      {
              $sOrder = " ORDER BY  ";
              for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
              {
                      if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                      {
                              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                      ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                      }
              }

              $sOrder = substr_replace( $sOrder, "", -2 );
              if ( $sOrder == " ORDER BY" )
              {
                      $sOrder = "";
              }
      }
      $sWhere = "";

      if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
      {
  $sWhere = " Where (";
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  }
  $sWhere = substr_replace( $sWhere, "", -3 );
  $sWhere .= ')';
      }

      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
              if ( $sWhere == "" )
              {
                  $sWhere = " WHERE ";
              }
              else
              {
                  $sWhere .= " AND ";
              }
              //echo $sWhere."<br>";
              $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
      }


      /*
       * SQL queries
       * QUERY YANG AKAN DITAMPILKAN
       */
      $sQuery = "
              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
              FROM   $sTable
              $sWhere
              $sOrder
              $sLimit
              ";

      // echo $sQuery;

      $rResult = $this->db->query( $sQuery);

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";    //SELECT FOUND_ROWS()

      $rResultFilterTotal = $this->db->query( $sQuery);
      $aResultFilterTotal = $rResultFilterTotal->result_array();
      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";
      $rResultTotal = $this->db->query( $sQuery);
      $aResultTotal = $rResultTotal->result_array();
      $iTotal = $aResultTotal[0]['jml'];

      $output = array(
              "sEcho" => intval($_GET['sEcho']),
              "iTotalRecords" => $iTotal,
              "iTotalDisplayRecords" => $iFilteredTotal,
              "aaData" => array()
      );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                } 

                $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('mstaddunit/edit/'.$aRow['kode'])."\">Edit</a>";
                $aRow['hapus'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kode']."');\">Hapus</button>";
                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    } 

    public function submit() { 
        try {
            $array = $this->input->post();   
            $array['kode']          = strtoupper($array['kode']);
            $array['kdtipe']        = strtoupper($array['kdtipe']);
            $array['nmtipe']        = strtoupper($array['nmtipe']); 
            $array['kdtipesr']      = $array['kdtipesr']; 
            $array['beli']          = $array['beli']; 
            $array['jual']          = $array['jual']; 
            $array['jual_off']      = $array['jual_off'];  

            $resl = $this->db->insert('tipe',$array);
            if( ! $resl){
                $err = $this->db->error();
                $this->res = " Error : ". $this->apps->err_code($err['message']);
                $this->state = "0";
            }else{
                $this->res = "Data Tersimpan";
                $this->state = "1";
            }
        }catch (Exception $e) {           
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array( 
            'state' => $this->state, 
            'msg' => $this->res,
            );
        // $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr); 
    }

    public function update() {
      $kode = $this->input->post('kode');
      $kode2 = $this->input->post('kode2');
      $kdtipe = $this->input->post('kdtipe');
      $nmtipe = $this->input->post('nmtipe');
      $kdtipesr = $this->input->post('kdtipesr');
      $beli = $this->input->post('beli');
      $jual = $this->input->post('jual');
      $jual_off= $this->input->post('jual_off');
      $faktif= $this->input->post('faktif');
      $q = $this->db->query("select title,msg,tipe from pzu.tipe_upd_w('" . $kode . "','" . $kode2 . "','" . $kdtipe . "','" . $nmtipe . "'," . $kdtipesr . "," . $beli . "," . $jual . "," . $jual_off . ",'" . $faktif . "','".$this->session->userdata("username")."')");

      //echo $this->db->last_query();
      if($q->num_rows()>0){
          $res = $q->result_array();
      }else{
          $res = "";
      }

      return json_encode($res);
    }

    public function delete() {
        $kode = $this->input->post('kode');
        $q = $this->db->query("select title,msg,tipe from pzu.tipe_del_w('". $kode ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

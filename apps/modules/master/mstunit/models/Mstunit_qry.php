<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of mstunit_qry
 *
 * @author
 */
class Mstunit_qry extends CI_Model {

    //put your code here
    protected $res = "";
    protected $delete = "";
    protected $state = "";
    protected $kd_cabang = "";

    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function select_data($param) {
        $this->db->select("kode,kdtipe,
                            nmtipe,
                            nmtipegrp,
                            faktif");
        $this->db->where('kode', $param);
        $query = $this->db->get('pzu.v_tipe');
        return $query->result_array();
    }

    public function json_dgview() {
        error_reporting(-1);

        $aColumns = array(
            'kode',
            'kode',
            'kdtipe',
            'nmtipe',
            'nmtipegrp',
            'faktifx',
        );

        $sIndexColumn = "kode";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() over(order by kode) as no,
                        kdtipe,nmtipe,nmtipegrp,faktifx,kode
                        FROM pzu.v_tipe order by kode
                    ) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
//            $row[6] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"" . site_url('mstunit/edit/' . $aRow['kode']) . "\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
//            $row[6] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('" . $aRow['kode'] . "');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function proses_setaktif() {
        $id_data = $this->input->post('iddata');
        $faktif = $this->input->post('faktif');
        $q = $this->db->query("select title,msg,tipe from pzu.unit_set_aktif('{".$id_data."}',".$faktif.")");
        // echo $this->db->last_query();
// 
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }


    public function submit() {
        try {
            $array = $this->input->post();
            //$array['user'] = $this->session->userdata('username');
            if (empty($array['kode'])) {
                unset($array['kode']);

                $str = "SELECT pzu.v_tipe_ins(
                    '" . strtoupper($array['kdtipe']) . "',
                    '" . strtoupper($array['nmtipe']) . "',
                    '" . strtoupper($array['nmtipegrp']) . "',
                    '" . strtoupper($array['faktif']) . "',
                    '" . $this->session->userdata('username') ."'
                );";

                $resl = $this->db->query($str);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->res = " Error : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }
            } else {
                $array['kdtipe'] = strtoupper($array['kdtipe']);
                $array['faktif'] = strtoupper($array['faktif']);

                $this->db->where('kode', $array['kode']);
                $resl = $this->db->update('pzu.v_tipe', $array);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->res = " Error : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }


    // utk transaksi tambah (baru)
    public function getDetail() {
        error_reporting(-1);

        $aColumns = array(
            'no',
            'kode',
            'kdtipe',
            'nmtipe',
            'faktif',
            'nourut',
        );

        $sIndexColumn = "kode";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        $sTable = " (SELECT row_number() over(order by a.nourut) as no,
                        a.kode,b.kdtipe,b.nmtipe,a.nourut
                        FROM pzu.v_tipe_d_tmp a JOIN pzu.tipe b ON a.kode=b.kode
                        order by a.nourut
                    ) AS a";

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $row[4] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deletedet('" . $aRow['nourut'] . "');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></a>";
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function ListKodeUnit() {
        error_reporting(-1);

        $aColumns = array(
            'no',
            'kode',
            'kdtipe',
            'nmtipe',
            'nmtipegrp',
            'faktif',

        );

        $sIndexColumn = "kode";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        $sTable = " (SELECT row_number() over(order by kode) as no,
                        kode,kdtipe,nmtipe
                        FROM pzu.tipe WHERE faktif = true AND kode not in (SELECT kode from pzu.v_tipe_d_tmp)
                        order by kode
                    ) AS a";

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function SimpanDetail(){
        try {
            $array = $this->input->post();

            if (empty($array['kode'])) {
                unset($array['kode']);
            }

            $resl = $this->db->insert('pzu.v_tipe_d_tmp',$array);
            if (!$resl) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "Data Tersimpan";
                $this->state = "1";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        return json_encode($arr);
    }

    public function HapusDetail(){
        try {
            $array = $this->input->post();

            $this->db->where('nourut', $array['nourut']);
            $resl = $this->db->delete('pzu.v_tipe_d_tmp');
            if (!$resl) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "Data Sukses Dihapus";
                $this->state = "1";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        return json_encode($arr);
    }


    //utk transaksi edit

    public function getDetailEd() {
        error_reporting(-1);
        if( isset($_GET['kode']) ){
            if($_GET['kode']){
                $kode = $_GET['kode'];
            }else{
                $kode = '0';
            }
        }else{
            $kode = '0';
        }

        $aColumns = array(
            'no',
            'kode',
            'kdtipe',
            'nmtipe',
            'nmtipegrp',
            'faktif',
            'nourut',
        );

        $sIndexColumn = "kode";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        $sTable = " (SELECT row_number() over(order by a.nourut) as no,
                        a.kode,b.kdtipe,b.nmtipe,a.nourut
                        FROM pzu.v_tipe_d a JOIN pzu.tipe b ON a.kode=b.kode
                        WHERE a.kode = " . $kode . "
                        order by a.nourut
                    ) AS a";

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $row[4] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deletedet('" . $aRow['nourut'] . "');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></a>";
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function ListKodeUnitEd() {
        error_reporting(-1);

        if( isset($_GET['kode']) ){
            if($_GET['kode']){
                $kode = $_GET['kode'];
            }
            else {
                $kode = '';
            }
        }
        else{
            $kode = '';
        }





        $aColumns = array(
            'no',
            'kode',
            'kdtipe',
            'nmtipe',
        );
        $sIndexColumn = "kode";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        $sTable = " (SELECT row_number() over(order by kode) as no,
                        kode,kdtipe,nmtipe
                        FROM pzu.tipe WHERE faktif = true AND kode not in (SELECT kode from pzu.v_tipe_d WHERE kode = ". $kode .")
                        order by kode
                    ) AS a";

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function SimpanDetailEd(){
        try {
            $array = $this->input->post();
            $resl = $this->db->insert('pzu.v_tipe_d',$array);
            if (!$resl) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "Data Tersimpan";
                $this->state = "1";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        return json_encode($arr);
    }

    public function HapusDetailEd(){
        try {
            $array = $this->input->post();

            //$this->db->where('kode', $array['kode']);
            //$this->db->where('kode', $array['kode']);
            $this->db->where('nourut', $array['nourut']);
            $resl = $this->db->delete('pzu.v_tipe_d');
            if (!$resl) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "Data Sukses Dihapus";
                $this->state = "1";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        return json_encode($arr);
    }
}

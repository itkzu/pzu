<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label('Pilih Ref Kas/Bank');
                                      echo form_dropdown($form['kdkb']['name'],$form['kdkb']['data'] ,$form['kdkb']['value'] ,$form['kdkb']['attr']);
                                      echo form_error('kdkb','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['tglkb']['placeholder']);
                                      echo form_input($form['tglkb']);
                                      echo form_error('tglkb','<div class="note">','</div>');
                                  ?>
                               </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['so_awal']['placeholder']);
                                      echo form_input($form['so_awal']);
                                      echo form_error('so_awal','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['so_fisik']['placeholder']);
                                      echo form_input($form['so_fisik']);
                                      echo form_error('so_fisik','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['tot_debet']['placeholder']);
                                      echo form_input($form['tot_debet']);
                                      echo form_error('tot_debet','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['sel_kb']['placeholder']);
                                      echo form_input($form['sel_kb']);
                                      echo form_error('sel_kb','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['tot_kredit']['placeholder']);
                                      echo form_input($form['tot_kredit']);
                                      echo form_error('tot_kredit','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['so_kasbon']['placeholder']);
                                      echo form_input($form['so_kasbon']);
                                      echo form_error('so_kasbon','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['so_akhir']['placeholder']);
                                      echo form_input($form['so_akhir']);
                                      echo form_error('so_akhir','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                      </div>
                  </div>

                  <div class="col-md-12">
                      <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-md-6 col-lg-6">
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <button type="button" class="btn btn-primary btn-proses">Proses</button>
                              </div>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
      clear();
      autoNum();
      tglkb();
      setNilai();

      $('#kdkb').change(function () {
          tglkb();
          setNilai();
      });

      $('#so_fisik').keyup(function(){
         status();
      });

      $('#so_kasbon').keyup(function(){
         status();
      });

      $('.btn-proses').click(function(){
         proses();
      });
    });

    function status(){
      so_fisik = $("#so_fisik").autoNumeric('get');
      so_kasbon = $("#so_kasbon").autoNumeric('get');
      //  tot = $("#total").val('');
      so_akhir =   parseFloat(so_fisik) + parseFloat(so_kasbon);
        if (!isNaN(so_akhir)){
            $('#so_akhir').autoNumeric('set',so_akhir);
            sel_kb = parseFloat(so_fisik) - parseFloat(so_akhir);
            if (!isNaN(sel_kb)){
              $('#sel_kb').autoNumeric('set',sel_kb);
            }
        }
    }

    function autoNum() {
      $('#so_awal').autoNumeric();
      $('#so_akhir').autoNumeric();
      $('#so_fisik').autoNumeric();
      $('#so_kasbon').autoNumeric();
      $('#tot_debet').autoNumeric();
      $('#tot_kredit').autoNumeric();
      $('#sel_kb').autoNumeric();
    }

    function clear() {
      $('.btn-proses').attr('disabled',false);
      $('#so_awal').val('');
      $('#so_akhir').val('');
      $('#so_fisik').val('');
      $('#so_kasbon').val('');
      $('#tot_debet').val('');
      $('#tot_kredit').val('');
      $('#sel_kb').val('');
    }

    function setNilai() {
      var kdkb = $("#kdkb").val();

      $.ajax({
          type: "POST",
          url: "<?=site_url("closekbsh/setNilai");?>",
          data: {"kdkb":kdkb },
          success: function(resp){
            var obj = JSON.parse(resp);
            $.each(obj, function(key, data){
                $('#so_awal').autoNumeric('set',data.so_awal);
                $('#tot_debet').autoNumeric('set',data.debet);
                $('#tot_kredit').autoNumeric('set',data.kredit);
                $('#so_akhir').autoNumeric('set',data.so_akhir);
                $('#so_fisik').autoNumeric('set','0');
                $('#so_kasbon').autoNumeric('set','0');
                $('#sel_kb').autoNumeric('set','0');
            });
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function tglkb(){
      var kdkb = $("#kdkb").val();

      $.ajax({
          type: "POST",
          url: "<?=site_url("closekbsh/tglkb");?>",
          data: {"kdkb":kdkb },
          success: function(resp){
            var obj = JSON.parse(resp);
            $.each(obj, function(key, data){
                $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb)));
            });
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function proses(){
        swal({
            title: "Konfirmasi Tutup Kas/Bank Bengkel!",
            text: "Data yang akan ditutup !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var kdkb = $("#kdkb").val();
            var so_fisik = $("#so_fisik").autoNumeric('get');
            $.ajax({
                type: "POST",
                url: "<?=site_url("closekbsh/process");?>",
                data: {"kdkb":kdkb , "so_fisik":so_fisik},
                beforeSend: function() {
                    $('.btn-proses').attr('disabled',true);
                },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                    clear();
                    setNilai();
                    tglkb();
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }
</script>

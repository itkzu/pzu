<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Blokbpkb
 *
 * @author adi
 */
class Blokbpkb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('blokbpkb/submit'),
            'add' => site_url('blokbpkb/add'),
            'edit' => site_url('blokbpkb/edit'),
            'reload' => site_url('blokbpkb'),
        );
        $this->load->model('blokbpkb_qry');  
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 

    public function set_do() {
        echo $this->blokbpkb_qry->set_do();
    } 

    public function submit() {
        echo $this->blokbpkb_qry->submit();
    }

    public function getNoID() {
        echo $this->blokbpkb_qry->getNoID();
    }

    private function _init_add(){

        if(isset($_POST['blokbpkb_nama']) && strtoupper($_POST['blokbpkb_nama']) == ''){
          $faktif = FALSE;
        } else{
          $faktif = TRUE;
        }

        $this->data['form'] = array(
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tgl DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ), 
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tgl SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ), 
            'nama'=> array(
                    'placeholder' => 'Nama Pemesan',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nama_s'=> array(
                    'placeholder' => 'Nama BPKB',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'alamatktp_s'=> array(
                    'placeholder' => 'Alamat KTP',
                    'value'       => set_value('alamatktp_s'),
                    'id'          => 'alamatktp_s',
                    'name'        => 'alamatktp_s',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nosin'=> array(
                    'placeholder' => 'No. Mesin/RK',
                    'value'       => set_value('nosin'),
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'value'       => set_value('nora'),
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'value'       => set_value('kdtipe'),
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => set_value('warna'),
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'jnsbayar'=> array(
                    'placeholder' => 'Jenis Penjualan',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('jnsbayar'),
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'harga_otr'=> array(
                    'placeholder' => 'Harga OTR',
                    'value'       => set_value('harga_otr'),
                    'id'          => 'harga_otr',
                    'name'        => 'harga_otr',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'type'        => 'hidden',
                    'readonly'    => '',
            ),
            'um'=> array(
                    'placeholder' => 'Uang Muka',
                    'value'       => set_value('um'),
                    'id'          => 'um',
                    'name'        => 'um',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'type'        => 'hidden',
                    'readonly'    => '',
            ),

            //blok 
            'blok'=> array(
                    'placeholder' => 'Blokir BPKB',
                    'id'          => 'blok',
                    // 'value'       => 't',
                    'checked'     => $faktif,
                    'class'       => 'custom-control-input',
                    'name'        => 'blok',
                    'type'        => 'checkbox',
            ),
            'blokbpkb_nama'=> array(
                    'placeholder' => 'Atas Nama',
                    'value'       => set_value('blokbpkb_nama'),
                    'id'          => 'blokbpkb_nama',
                    'name'        => 'blokbpkb_nama',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'type'        => 'hidden',
                    'readonly'    => '',
            ), 
            'blokbpkb_alamat'=> array(
                     'placeholder' => 'Alamat KTP',
                     'id'          => 'blokbpkb_alamat',
                     'name'        => 'blokbpkb_alamat',
                     'value'       => set_value('blokbpkb_alamat'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'blokbpkb_noktp'=> array(
                    'placeholder' => 'No. KTP',
                    'value'       => set_value('blokbpkb_noktp'),
                    'id'          => 'blokbpkb_noktp',
                    'name'        => 'blokbpkb_noktp',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'blokbpkb_nohp'=> array(
                    'placeholder' => 'No. HP',
                    'value'       => set_value('blokbpkb_nohp'),
                    'id'          => 'blokbpkb_nohp',
                    'name'        => 'blokbpkb_nohp',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'blokbpkb_ket'=> array(
                    'placeholder' => 'Keterangan',
                    'value'       => set_value('blokbpkb_ket'),
                    'id'          => 'blokbpkb_ket',
                    'name'        => 'blokbpkb_ket',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3); 
        }
        $this->_check_id($no);

        if($this->val[0]['fblokbpkb'] == 't'){
                    $faktifx = true;
                } else {
                        $faktifx = false;
            }
        $this->data['form'] = array(
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => $this->val[0]['nodo'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tgl DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => $this->apps->dateConvert($this->val[0]['tgldo']),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ), 
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => $this->val[0]['noso'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tgl SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => $this->apps->dateConvert($this->val[0]['tglso']),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ), 
            'nama'=> array(
                    'placeholder' => 'Nama Pemesan',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => $this->val[0]['nama'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nama_s'=> array(
                    'placeholder' => 'Nama BPKB',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => $this->val[0]['nama_s'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'alamatktp_s'=> array(
                    'placeholder' => 'Alamat KTP',
                    'value'       => $this->val[0]['alamatktp_s'],
                    'id'          => 'alamatktp_s',
                    'name'        => 'alamatktp_s',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nosin'=> array(
                    'placeholder' => 'No. Mesin/RK',
                    'value'       => $this->val[0]['nosin2'],
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'value'       => $this->val[0]['nora2'],
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'value'       => $this->val[0]['kdtipex'],
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'value'       => $this->val[0]['nmtipe'],
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => $this->val[0]['warna'],
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => $this->val[0]['tahun'],
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'jnsbayar'=> array(
                    'placeholder' => 'Jenis Penjualan',
                    // 'type'        =>  'hidden',
                    'value'       => $this->val[0]['jnsbayar'],
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'harga_otr'=> array(
                    'placeholder' => 'Harga OTR',
                    'value'       => $this->val[0]['harga_otr'],
                    'id'          => 'harga_otr',
                    'name'        => 'harga_otr',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'type'        => 'hidden',
                    'readonly'    => '',
            ),
            'um'=> array(
                    'placeholder' => 'Uang Muka',
                    'value'       => $this->val[0]['um'],
                    'id'          => 'um',
                    'name'        => 'um',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'type'        => 'hidden',
                    'readonly'    => '',
            ),

            //blok 
            'blok'=> array(
                    'placeholder' => 'Blokir BPKB',
                    'id'          => 'blok',
                    // 'value'       => 't',
                    'checked'     => $faktifx,
                    'class'       => 'custom-control-input',
                    'name'        => 'blok',
                    'type'        => 'checkbox',
            ),
            'blokbpkb_nama'=> array(
                    'placeholder' => 'Atas Nama',
                    'value'       => $this->val[0]['blokbpkb_nama'],
                    'id'          => 'blokbpkb_nama',
                    'name'        => 'blokbpkb_nama',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'type'        => 'hidden',
                    // 'readonly'    => '',
            ), 
            'blokbpkb_alamat'=> array(
                     'placeholder' => 'Alamat KTP',
                     'id'          => 'blokbpkb_alamat',
                     'name'        => 'blokbpkb_alamat',
                    'value'       => $this->val[0]['blokbpkb_alamat'],
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'blokbpkb_noktp'=> array(
                    'placeholder' => 'No. KTP',
                    'value'       => $this->val[0]['blokbpkb_noktp'],
                    'id'          => 'blokbpkb_noktp',
                    'name'        => 'blokbpkb_noktp',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'blokbpkb_nohp'=> array(
                    'placeholder' => 'No. HP',
                    'value'       => $this->val[0]['blokbpkb_nohp'],
                    'id'          => 'blokbpkb_nohp',
                    'name'        => 'blokbpkb_nohp',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'blokbpkb_ket'=> array(
                    'placeholder' => 'Keterangan',
                    'value'       => $this->val[0]['blokbpkb_ket'],
                    'id'          => 'blokbpkb_ket',
                    'name'        => 'blokbpkb_ket',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->blokbpkb_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes); 
        ?> 
          <div class="box-body">
                <div class="form-group">
                    <?php
                        echo form_input($form['kdsales']);
                        echo form_input($form['kdsaleshd']);

                        echo form_label($form['nik']['placeholder']);
                        echo form_input($form['nik']);
                        echo form_error('nik','<div class="note">','</div>'); 
                    ?>
                </div>
                <div class="form-group">
                    <?php 
                        echo form_label($form['nmlengkap']['placeholder']);
                        echo form_input($form['nmlengkap']);
                        echo form_error('nmlengkap','<div class="note">','</div>'); 
                    ?>
                </div>    
                <div class="form-group">
                    <?php 
                        echo form_label($form['nmsales']['placeholder']);
                        echo form_input($form['nmsales']);
                        echo form_error('nmsales','<div class="note">','</div>'); 
                    ?>
                </div>    
                <div class="form-group">
                    <?php
                        echo form_label('Jabatan');
                        echo form_dropdown($form['status']['name'],$form['status']['data'] ,$form['status']['value'] ,$form['status']['attr']);
                        echo form_error('status','<div class="note">','</div>'); 
                    ?>
                </div>     
                <div class="form-group">
                    <?php
                        echo form_label('Nama Atasan/SPV');
                        echo form_dropdown($form['kdsales_header']['name'],$form['kdsales_header']['data'] ,$form['kdsales_header']['value'] ,$form['kdsales_header']['attr']);
                        echo form_error('kdsales_header','<div class="note">','</div>'); 
                    ?>
                </div>   
                <div class="form-group">
                    <?php
                        echo form_label('POS/Cabang');
                        echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                        echo form_error('kddiv','<div class="note">','</div>'); 
                    ?>
                </div>    
                <div class="form-group">
                    <?php 
                        echo form_label($form['tgl_in']['placeholder']);
                        echo form_input($form['tgl_in']);
                        echo form_error('tgl_in','<div class="note">','</div>'); 
                    ?>
                </div>  
                <div class="form-group" style="display: none;">
                    <?php
                        echo form_label('Status Sales');
                        echo form_dropdown($form['faktif']['name'],$form['faktif']['data'] ,$form['faktif']['value'] ,$form['faktif']['attr']);
                        echo form_error('faktif','<div class="note">','</div>'); 
                    ?>
                </div>            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>    
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        /*
        $(".form-control").keyup(function(){
            var val = $(this).val();
            $(this).val(val.toUpperCase());
        });        
        */
        
        $(".btn-submit").click(function(){
            $("#kddiv").prop("disabled", false);
        });
        if($("#kdsales").val()){
            getSalesheader();
        }
        $('#kddiv').select2({
            placeholder: '-- Pilih POS/Cabang --',
            dropdownAutoWidth : true,
            width: 'resolve',
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            templateResult: formatKdDiv, // omitted for brevity, see the source of this page
            templateSelection: formatKdDivSelection // omitted for brevity, see the source of this page
        }); 
        
        $("#kdsales_header").change(function(){
            getSalesHeaderDivisi();
        });
        
        $("#kddiv").prop("disabled", true);
        
        $("#nmlengkap").keyup(function(){
            var nmsales = $(this).val();
            var spc = nmsales.indexOf(" ");
            if(spc===-1){
                $("#nmsales").val(nmsales);
            }else{
                $("#nmsales").val(nmsales.substring(0,spc));
            }
        });
        
        $("#status").change(function(){
            getSalesheader();
        });
        function formatKdDiv (repo) {
            if (repo.loading) return "Mencari data ... ";
            var separatora = repo.text.indexOf("[");
            var separatorb = repo.text.indexOf("]");
            var text = repo.text.substring(0,separatora);
            var alamat = repo.text.substring(separatora+1,separatorb);
            var markup = "<div class='select2-result-repository clearfix'>" +
              "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + text + "</b></div>";

            markup += "<div class='select2-result-repository__statistics'>" +
              "<div class='select2-result-repository__stargazers' style='font-size: 12px;'> " + alamat + " </div>" +
            "</div>" +
            "</div></div>";
            return markup;
          }

        function formatKdDivSelection (repo) {
            var separatora = repo.text.indexOf("[");
            var text = repo.text.substring(0,separatora);
            return text;
        }  
        
        function getSalesheader(){
           var status = $("#status").val();
           if(status==="KP"){
               $("#kddiv").prop("disabled", false);
           }else{
               $('#kddiv').val(null).trigger('change');
               $("#kddiv").prop("disabled", true);
           }
            $.ajax({
                type: "POST",
                url: "<?=site_url("transalesin/getSalesheader");?>",
                data: {"status":status},
                beforeSend: function() {
                    $('#kdsales_header').html("")
                                    .append($('<option>', { value : '' })
                                    .text('-- Pilih Atasan/SPV --'));
                    $("#kdsales_header").trigger("change.select2");
                    if ($('#kdsales_header').hasClass("select2-hidden-accessible")) {
                        $('#kdsales_header').select2('destroy');
                    }
                }, 
                success: function(resp){   
                    var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, value){
                        $('#kdsales_header')
                            .append($('<option>', { value : value.kdsales })
                            .text(value.nmlengkap+' ['+statussales(value.status)+' | ' +value.nmdiv+ ']'));
                    });
                    
                    $('#kdsales_header').select2({
                        placeholder: '-- Pilih Atasan/SPV --',
                        dropdownAutoWidth : true,
                        width: 'resolve',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
                        templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
                    }); 
                    
                    function formatSalesHeader (repo) {
                        if (repo.loading) return "Mencari data ... ";
                        var separatora = repo.text.indexOf("[");
                        var separatorb = repo.text.indexOf("]");
                        var text = repo.text.substring(0,separatora);
                        var status = repo.text.substring(separatora+1,separatorb);
                        var markup = "<div class='select2-result-repository clearfix'>" +
                          "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + text + "</b></div>";

                        markup += "<div class='select2-result-repository__statistics'>" +
                          "<div class='select2-result-repository__stargazers' style='font-size: 12px;'> " + status + " </div>" +
                        "</div>" +
                        "</div></div>";
                        return markup;
                      }

                    function formatSalesHeaderSelection (repo) {
                        var separatora = repo.text.indexOf("[");
                        var text = repo.text.substring(0,separatora);
                        return text;
                    }  
                    $('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }
        
        function getSalesHeaderDivisi(){
            var kdsales_header = $("#kdsales_header").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("transalesin/getSalesHeaderDivisi");?>",
                data: {"kdsales_header":kdsales_header},
                beforeSend: function() {
                }, 
                success: function(resp){   
                    var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, value){
                        $('#kddiv').val(value.kddiv).trigger('change');
                    });
                    
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }
    });
</script>
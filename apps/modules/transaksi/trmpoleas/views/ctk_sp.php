  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style>
      caption {
          padding-top: 8px;
          padding-bottom: 8px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto;
          font-family: verdana;
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }
  </style>
  <?php 
      function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 

      function tgl($date) { 
        $day = substr($date,0,2);
        $month = substr($date,3,2); 
        $year = substr($date,6,4);
        $bulan = array("", "January", "February", "Maret", "April", "May", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        // $temp = "";
        $temp = $day."&nbsp;".$bulan[(Int)$month]."&nbsp;".$year; 
        return $temp;
      } 

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $namaheader = array(
      // array('data' => '&nbsp;'
      //                     , 'colspan' => 24
      //                     , 'style' => 'text-align: center; width: 100%; font-size: 15px;'),
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;'
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);   
  $col1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; width: 83%; font-size: 70px;'),  
  );
  $judul_header = array(
      array('data' => '<b> SURAT PERNYATAAN </b>'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 18px;'),  
  );
  $this->table->add_row($judul_header);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 20px;'),   
  ); 
  $this->table->add_row($br);

  $header_isi = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => 'Bahwa yang bertandatangan dibawah ini :'
                          , 'colspan' => 23
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($header_isi);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  ); 
  $this->table->add_row($br);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Nama '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.ucwords(strtolower($this->session->userdata('data')['adh']))
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Jabatan '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : Kepala Administrasi PT. '.$this->apps->title.'&nbsp;'.ucwords(strtolower($this->session->userdata('data')['cabang']))
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Alamat '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.ucwords(strtolower($this->session->userdata('data')['alamat'])).' - '.ucwords(strtolower($this->session->userdata('data')['kota']))
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  ); 
  $this->table->add_row($br);

  $header_isi = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => 'dengan ini menyatakan bahwa BPKB dengan spesifikasi sebagai berikut :'
                          , 'colspan' => 23
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($header_isi);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  ); 
  $this->table->add_row($br);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Nama '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.$data[0]['nama_stnk']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Alamat '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.$data[0]['alamat'].' KEL. '.$data[0]['kel']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; font-size: 14px;'),  
      array('data' => ' &nbsp; KEC. '.$data[0]['kec'].' - '.$data[0]['kota']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Merk / Tipe '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : SPD MOTOR HONDA /'.$data[0]['nmtipe'].' ('.$data[0]['kode'].')'
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' Warna/Tahun'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.$data[0]['nmwarna'].' / '.$data[0]['tahun']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian); 

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' No. Rangka '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.$data[0]['nora']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' No. Mesin '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.$data[0]['nosin']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $isian = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ' No. PO '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; font-size: 14px;'),  
      array('data' => ' : '.$data[0]['nopoleasing']
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($isian);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  ); 
  $this->table->add_row($br);

  $header_isi = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => 'masih dalam proses penyelesaian. Apabila proses tersebut telah selesai, maka akan kami serahkan kepada '.$data[0]['nmleasing2'].'.'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($header_isi);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  ); 
  $this->table->add_row($br);

  $header_isi = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => 'Demikian surat pernyatan ini saya buat untuk digunakan sebagaimana mestinya.'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; font-size: 14px;'),  
  ); 
  $this->table->add_row($header_isi);

  $br = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  ); 
  $this->table->add_row($br);

  $ttd = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ucwords(strtolower($this->session->userdata('data')['kota'])).", ".tgl($this->apps->dateConvert(date("Y-m-d")))
                          , 'colspan' => 17
                          , 'style' => 'text-align: center; font-size: 14px;'),  
  ); 
  $this->table->add_row($ttd);

  $ttd = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => "Yang Menyatakan,"
                          , 'colspan' => 17
                          , 'style' => 'text-align: center; font-size: 14px;'),  
  ); 
  $this->table->add_row($ttd);

  $ttd = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 40px;'),  
  ); 
  $this->table->add_row($ttd);

  $ttd = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; font-size: 14px;'), 
      array('data' => ucwords(strtolower($this->session->userdata('data')['adh']))
                          , 'colspan' => 17
                          , 'style' => 'text-align: center; font-size: 14px;'),  
  ); 
  $this->table->add_row($ttd);

  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();    
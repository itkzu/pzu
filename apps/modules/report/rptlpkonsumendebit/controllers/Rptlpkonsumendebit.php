<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptlpkonsumendebit
 *
 * @author adi
 */
class Rptlpkonsumendebit extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptlpkonsumendebit/submit'),
            'add' => site_url('rptlpkonsumendebit/add'),
            'edit' => site_url('rptlpkonsumendebit/edit'),
            'reload' => site_url('rptlpkonsumendebit'),
        );
        $this->load->model('rptlpkonsumendebit_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->rptlpkonsumendebit_qry->json_dgview();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptlpkonsumendebit_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_awal',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'periode_akhir',
                    'label' => 'Periode Akhir',
                    'rules' => 'required|max_length[20]',
                    ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

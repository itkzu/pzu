<?php

/* 
 * ***************************************************************
 * Script : excel.php
 * Version : 
 * Date Created : Sep 25, 2017 10:11:40 AM 
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */


require_once APPPATH.'libraries/PHPExcel.php';

function month_name($param){
    $date = explode("-", $param);
    $mm = strtoupper(month($date[1]));
    $yy = $date[0];
    $dm = $param."-01";
    $dd = date("t", strtotime($dm));
    return $dd." ".$mm." ".$yy;
}

function spacing($level){
    $res = " ";
    for ($x = 1; $x <= $level; $x++) {
        $res.=$res;
    } 
    return $res;
}


$objPHPExcel = new PHPExcel(); 
$idxsheet = 0;   
define('FORMAT_INDONESIA_CURRENCY', '#,##0.00');
define('FORMAT_INDONESIA_NUMBER', '#,##');

$array = $this->input->post();
$array['periode_sebelumnya'] = month_before($array['periode_awal']);

$namafile = "PERIODE " .month_name($array['periode_awal']);

$objWorkSheet = $objPHPExcel->createSheet($idxsheet);
$objWorkSheet->setTitle(strtoupper(month(date('m')))." ".date('Y'));      
if(count($rptakun['mtd'])>0){  
    // Kolom Header Start
    $objWorkSheet->setCellValueByColumnAndRow(0, 1, "LAPORAN NERACA")
            ->mergeCellsByColumnAndRow(0, 1, 5,1);
    $objWorkSheet->getStyleByColumnAndRow(0, 1)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
    $objWorkSheet->getRowDimension(1)->setRowHeight(18);
    
    $objWorkSheet->setCellValueByColumnAndRow(0, 2, strtoupper($this->apps->title)." - ". strtoupper($rptakun['div'][0]['nmdiv']))
            ->mergeCellsByColumnAndRow(0, 2, 5,2);
    $objWorkSheet->getStyleByColumnAndRow(0, 2)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
    $objWorkSheet->getRowDimension(2)->setRowHeight(18);
    
    $objWorkSheet->setCellValueByColumnAndRow(0, 3, $namafile)
            ->mergeCellsByColumnAndRow(0, 3, 5,3);
    $objWorkSheet->getStyleByColumnAndRow(0, 3)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
    $objWorkSheet->getRowDimension(3)->setRowHeight(18);
 

    // Generate Header Laporan Start
    $colHeader = 0;            
    $namaheader = array(
        'AKTIVA',
        'PASIVA',
    );

    $rowheader = 5;
    foreach ($namaheader as $val) {
        $objWorkSheet->getRowDimension($rowheader)->setRowHeight(18);
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow($colHeader, $rowheader, $val);
        $objWorkSheet->mergeCellsByColumnAndRow($colHeader, $rowheader, $colHeader+2,$rowheader);            
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader, $colHeader+2,$rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $colHeader = $colHeader + 2;    
        $colHeader++;
    }
    $rowheader += 1;
    $objWorkSheet->getRowDimension($rowheader)->setRowHeight(18);
    if(isset($array['compare_myd'])){
        $objWorkSheet->getStyleByColumnAndRow(0, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(0, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowheader, "  PERIODE AKTIVA");
        $objWorkSheet->getStyleByColumnAndRow(0, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(1, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(1, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(1, $rowheader, month_name($array['periode_awal']));
        $objWorkSheet->getStyleByColumnAndRow(1, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(2, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(2, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(2, $rowheader, month_name($array['periode_sebelumnya']));
        $objWorkSheet->getStyleByColumnAndRow(2, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(3, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(3, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(3, $rowheader, "  PERIODE PASIVA");
        $objWorkSheet->getStyleByColumnAndRow(3, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(4, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(4, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(4, $rowheader, month_name($array['periode_awal']));
        $objWorkSheet->getStyleByColumnAndRow(4, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(5, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(5, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(5, $rowheader, month_name($array['periode_sebelumnya']));
        $objWorkSheet->getStyleByColumnAndRow(5, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
    }
    else{
        $objWorkSheet->getStyleByColumnAndRow(0, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(0, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowheader, "  PERIODE AKTIVA")
            ->mergeCellsByColumnAndRow(0, $rowheader, 1,$rowheader);
        $objWorkSheet->getStyleByColumnAndRow(0, $rowheader,1,$rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(2, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(2, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(2, $rowheader, month_name($array['periode_awal']));
        $objWorkSheet->getStyleByColumnAndRow(2, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(3, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(3, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(3, $rowheader, "  PERIODE PASIVA")
                    ->mergeCellsByColumnAndRow(3, $rowheader, 4,$rowheader);
        $objWorkSheet->getStyleByColumnAndRow(3, $rowheader, 4,$rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(5, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(5, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(5, $rowheader, month_name($array['periode_awal']));
        $objWorkSheet->getStyleByColumnAndRow(5, $rowheader)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
    }
        
    $row_a_myd = $rowheader+1;
    $row_p_myd = $rowheader+1;
    $row_a_mtd = $rowheader+1;
    $row_p_mtd = $rowheader+1;
    
    $sum_a_myd = 0;
    $sum_b_myd = 0;

    $sum_a_mtd = 0;
    $sum_b_mtd = 0;    
    if(isset($array['compare_myd'])){
        
        $objWorkSheet->getColumnDimensionByColumn(0)->setWidth(70);
        $objWorkSheet->getColumnDimensionByColumn(1)->setWidth(16);
        $objWorkSheet->getColumnDimensionByColumn(2)->setWidth(16);
        
        $objWorkSheet->getColumnDimensionByColumn(3)->setWidth(70);
        $objWorkSheet->getColumnDimensionByColumn(4)->setWidth(16);
        $objWorkSheet->getColumnDimensionByColumn(5)->setWidth(16);
        foreach ($rptakun['myd'] as $v_mtd) {
            $objWorkSheet->getRowDimension($row_a_myd)->setRowHeight(18);    
            if($v_mtd['jenis']==="A"){
                if($v_mtd['level']==="1"){
                    $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd)
                            ->getFont()
                                ->setBold(true);
                    $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                            ->getFont()
                                ->setBold(true);
                    $sum_a_myd = $sum_a_myd + $v_mtd['nominal'];
                }
                $objWorkSheet->setCellValueByColumnAndRow(0, $row_a_myd, spacing($v_mtd['level']).$v_mtd['nmheader']);
                $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  
                
                $objWorkSheet->setCellValueByColumnAndRow(2, $row_a_myd, $v_mtd['nominal']);
                    $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                                    ->getNumberFormat()
                                        ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
                $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);    
                
                
                $objWorkSheet->getStyleByColumnAndRow(3, $row_a_myd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  
                $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
                $row_a_myd++;
            }
            else{
                if($v_mtd['level']==="1"){
                    $objWorkSheet->getStyleByColumnAndRow(3, $row_p_myd)
                            ->getFont()
                                ->setBold(true);
                    $objWorkSheet->getStyleByColumnAndRow(4, $row_p_myd)
                            ->getFont()
                                ->setBold(true);
                    $sum_b_myd = $sum_b_myd + $v_mtd['nominal'];
                }
                $objWorkSheet->setCellValueByColumnAndRow(3, $row_p_myd, spacing($v_mtd['level']).$v_mtd['nmheader']);
                
                $objWorkSheet->setCellValueByColumnAndRow(5, $row_p_myd, $v_mtd['nominal']);
                    $objWorkSheet->getStyleByColumnAndRow(5, $row_p_myd)
                                    ->getNumberFormat()
                                        ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
                $row_p_myd++;
            }
        }
        foreach ($rptakun['mtd'] as $v_mtd){
            if($v_mtd['jenis']==="A"){
                if($v_mtd['level']==="1"){
                    $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                            ->getFont()
                                ->setBold(true);
                    $sum_a_mtd = $sum_a_mtd + $v_mtd['nominal'];
                }
                $objWorkSheet->setCellValueByColumnAndRow(1, $row_a_mtd, $v_mtd['nominal']);
                    $objWorkSheet->getStyleByColumnAndRow(1, $row_a_mtd)
                                    ->getNumberFormat()
                                        ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
                $objWorkSheet->getStyleByColumnAndRow(1, $row_a_mtd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                $objWorkSheet->getStyleByColumnAndRow(5, $row_a_mtd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                $row_a_mtd++;
            }
            else{
                if($v_mtd['level']==="1"){
                    $objWorkSheet->getStyleByColumnAndRow(5, $row_p_mtd)
                            ->getFont()
                                ->setBold(true);
                    $sum_b_mtd = $sum_b_mtd + $v_mtd['nominal'];
                }
                $objWorkSheet->setCellValueByColumnAndRow(4, $row_p_mtd, $v_mtd['nominal']);
                    $objWorkSheet->getStyleByColumnAndRow(4, $row_p_mtd)
                                    ->getNumberFormat()
                                        ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
                $row_p_mtd++;
            }

        }
        
        // Row Total Aktiva dan Pasiva
        
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd,5, $row_a_myd)
                ->getFont()
                    ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(0, $row_a_myd, "TOTAL AKTIVA");
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);   
        
        $objWorkSheet->setCellValueByColumnAndRow(2, $row_a_myd, $sum_a_myd);
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                        ->getNumberFormat()
                            ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->setCellValueByColumnAndRow(1, $row_a_myd, $sum_a_mtd);
        $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                        ->getNumberFormat()
                            ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        
        
        $objWorkSheet->setCellValueByColumnAndRow(3, $row_a_myd, "TOTAL PASIVA");
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);   
        
        $objWorkSheet->setCellValueByColumnAndRow(5, $row_a_myd, $sum_b_myd);
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_myd)
                        ->getNumberFormat()
                            ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->setCellValueByColumnAndRow(4, $row_a_myd, $sum_b_mtd);
        $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                        ->getNumberFormat()
                            ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        // ./Row Total Aktiva dan Pasiva
    }
    else{
        $objWorkSheet->getColumnDimensionByColumn(0)->setWidth(70);
        $objWorkSheet->getColumnDimensionByColumn(3)->setWidth(70);
        
        $objWorkSheet->getColumnDimensionByColumn(2)->setWidth(16);
        $objWorkSheet->getColumnDimensionByColumn(5)->setWidth(16);
        foreach ($rptakun['mtd'] as $v_mtd){
            if($v_mtd['jenis']==="A"){
                if($v_mtd['level']==="1"){
                    $objWorkSheet->getStyleByColumnAndRow(0, $row_a_mtd,2,$row_a_mtd)
                            ->getFont()
                                ->setBold(true);
                    $sum_a_mtd = $sum_a_mtd + $v_mtd['nominal'];
                }
                $objWorkSheet->setCellValueByColumnAndRow(0, $row_a_mtd, spacing($v_mtd['level']).$v_mtd['nmheader'])
                        ->mergeCellsByColumnAndRow(0, $row_a_mtd, 1,$row_a_mtd);
                $objWorkSheet->setCellValueByColumnAndRow(2, $row_a_mtd, $v_mtd['nominal']);
                    $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                                    ->getNumberFormat()
                                        ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
                    
                $objWorkSheet->getStyleByColumnAndRow(0, $row_a_mtd, 1,$row_a_mtd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                    
                $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                
                $objWorkSheet->mergeCellsByColumnAndRow(3, $row_a_mtd, 4,$row_a_mtd);    
                $objWorkSheet->getStyleByColumnAndRow(3, $row_a_mtd, 4,$row_a_mtd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                $objWorkSheet->getStyleByColumnAndRow(5, $row_a_mtd)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                $row_a_mtd++;
            }
            else{
                if($v_mtd['level']==="1"){
                    $objWorkSheet->getStyleByColumnAndRow(3, $row_p_mtd,5, $row_p_mtd)
                            ->getFont()
                                ->setBold(true);
                    $sum_b_mtd = $sum_b_mtd + $v_mtd['nominal'];
                }
                $objWorkSheet->setCellValueByColumnAndRow(3, $row_p_mtd, spacing($v_mtd['level']).$v_mtd['nmheader'])
                        ->mergeCellsByColumnAndRow(3, $row_p_mtd, 4,$row_p_mtd);
                $objWorkSheet->setCellValueByColumnAndRow(5, $row_p_mtd, $v_mtd['nominal']);
                    $objWorkSheet->getStyleByColumnAndRow(5, $row_p_mtd)
                                    ->getNumberFormat()
                                        ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
                $row_p_mtd++;
            }

        }
        
    }
    
    
    if(isset($array['compare_myd'])){
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(0, $row_a_myd, "  TOTAL AKTIVA");
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                ->getNumberFormat()
                    ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->setCellValueByColumnAndRow(1, $row_a_myd, $sum_a_mtd);
        $objWorkSheet->getStyleByColumnAndRow(1, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                ->getNumberFormat()
                    ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->setCellValueByColumnAndRow(2, $row_a_myd, $sum_a_myd);
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_myd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_myd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(3, $row_a_myd, "  TOTAL PASIVA");
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                ->getNumberFormat()
                    ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->setCellValueByColumnAndRow(4, $row_a_myd, $sum_b_mtd);
        $objWorkSheet->getStyleByColumnAndRow(4, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_myd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_myd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_myd)
                ->getNumberFormat()
                    ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->setCellValueByColumnAndRow(5, $row_a_myd, $sum_b_myd);
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_myd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
    }
    else{
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_mtd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_mtd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(0, $row_a_mtd, "  TOTAL AKTIVA")
            ->mergeCellsByColumnAndRow(0, $row_a_mtd, 1,$row_a_mtd);
        $objWorkSheet->getStyleByColumnAndRow(0, $row_a_mtd,1,$row_a_mtd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                ->getNumberFormat()
                    ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->setCellValueByColumnAndRow(2, $row_a_mtd, $sum_a_mtd);
        $objWorkSheet->getStyleByColumnAndRow(2, $row_a_mtd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_mtd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);            
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_mtd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow(3, $row_a_mtd, "  TOTAL PASIVA")
                    ->mergeCellsByColumnAndRow(3, $row_a_mtd, 4,$row_a_mtd);
        $objWorkSheet->getStyleByColumnAndRow(3, $row_a_mtd, 4,$row_a_mtd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_mtd)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);            
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_mtd)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_mtd)
                ->getNumberFormat()
                    ->setFormatCode(FORMAT_INDONESIA_CURRENCY);
        $objWorkSheet->setCellValueByColumnAndRow(5, $row_a_mtd, $sum_b_mtd);
        $objWorkSheet->getStyleByColumnAndRow(5, $row_a_mtd)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        
    }
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$namafile.'.xls"'); 
        header('Cache-Control: max-age=0'); 
$objWriter->save('php://output'); 
exit(); 
<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
			<!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
			-->
            <!-- /.box-header -->
            
            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes); 
            ?> 
            
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-6">

                    	<div class="form-group">
                            <label for="exampleInputFile">Data Harga Beli</label>
                          	
							<div class="fileinputBeli fileinput-new input-group" data-provides="fileinput">
                            	<div class="form-control" data-trigger="fileinput">
                                	<i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                	<span class="fileinput-filename"></span>
                            	</div>
                            
								<span class="input-group-addon btn btn-default btn-file">
                                	<span class="fileinput-new">Cari...</span>
                                	<span class="fileinput-exists">Ubah</span>
                                	<input type="file" name="fileBeli" id="fileBeli" required accept=".xls, .xlsx">
                            	</span>

								<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
                            
								<span class="input-group-btn fileinput-exists">
									<button class="btn btn-primary" name="importBeli" id="importBeli"><i class="fa fa-cloud-upload"></i> Import</button>
                            	</span>
                          	</div>
					  	</div>


						<div class="form-group">
                            <label for="exampleInputFile">Data Harga Jual ON-The Road</label>
							<div class="fileinputON fileinput-new input-group" data-provides="fileinput">
                            	<div class="form-control" data-trigger="fileinput">
                                	<i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                	<span class="fileinput-filename"></span>
                            	</div>
                            
								<span class="input-group-addon btn btn-default btn-file">
                                	<span class="fileinput-new">Cari...</span>
                                	<span class="fileinput-exists">Ubah</span>
                                	<input type="file" name="fileON" id="fileON" required accept=".xls, .xlsx">
                            	</span>

								<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
                            
								<span class="input-group-btn fileinput-exists">
									<button class="btn btn-primary" name="importON" id="importON"><i class="fa fa-cloud-upload"></i> Import</button>
                            	</span>
                          	</div>
                        </div>


						<div class="form-group">
                            <label for="exampleInputFile">Data Harga Jual OFF-The Road</label>
							<div class="fileinputOFF fileinput-new input-group" data-provides="fileinput">
                            	<div class="form-control" data-trigger="fileinput">
                                	<i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                	<span class="fileinput-filename"></span>
                            	</div>
                            
								<span class="input-group-addon btn btn-default btn-file">
                                	<span class="fileinput-new">Cari...</span>
                                	<span class="fileinput-exists">Ubah</span>
                                	<input type="file" name="fileOFF" id="fileOFF" required accept=".xls, .xlsx">
                            	</span>

								<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
                            
								<span class="input-group-btn fileinput-exists">
									<button class="btn btn-primary" name="importOFF" id="importOFF"><i class="fa fa-cloud-upload"></i> Import</button>
                            	</span>
                          	</div>
                        </div>


                  	</div>


				</div>
			</div>
	          	<!-- /.box-body -->

			<div class="box-footer">
				<p><b>Syarat & Ketentuan Import data :</b></p>
				<p>- Harga Jual ON The Road harus lebih dari Harga Jual OFF The Road.</p>
				<p>- Diharuskan untuk import data Harga Jual ON The Road dahulu kemudian import data Harga Jual OFF The Road.</p>
				<p>- Untuk Proses Import Data Harga Jual OFF The Road, jika Harga Jual ON The Road yang tersimpan di sistem nilainya belum terisi (bernilai 0) atau nilainya lebih kecil dari Harga Jual OFF The Road yang akan di import, maka data Harga Jual OFF The Road yang di import tidak akan terupdate setelah proses import Harga Jual OFF The Road.</p>
			</div>

        	<?php echo form_close(); ?>
      	</div>
      	<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

		//import data beli
		$('#importBeli').click(function(event){
  			event.preventDefault();

			var formData = new FormData();
			formData.append('file', $('input[type=file]')[0].files[0]);

  			$.ajax({
   				url:"<?php echo base_url(); ?>impunit/importBeli",
   				method:"POST",
   				//data:new FormData(this),
				data:formData,
   				contentType:false,
   				cache:false,
   				processData:false,
   				success:function(data){
					$('.fileinputBeli').fileinput('clear')
    				alert(data);
   				}
  			})
 		});


		//import data ON TR
		$('#importON').click(function(event){
  			event.preventDefault();

			var formData = new FormData();
			formData.append('file', $('input[type=file]')[1].files[0]);
			//formData.append('file', $('.fileinputON').files[0]);

  			$.ajax({
   				url:"<?php echo base_url(); ?>impunit/importONTR",
   				method:"POST",
   				//data:new FormData(this),
				data:formData,
   				contentType:false,
   				cache:false,
   				processData:false,
   				success:function(data){
					$('.fileinputON').fileinput('clear')
    				alert(data);
   				}
  			})
 		});

		//import data OFF TR
		$('#importOFF').click(function(event){
  			event.preventDefault();

			var formData = new FormData();
			formData.append('file', $('input[type=file]')[2].files[0]);
			//formData.append('file', $('#fileinputOFF').files[0]);

  			$.ajax({
   				url:"<?php echo base_url(); ?>impunit/importOFFTR",
   				method:"POST",
   				//data:new FormData(this),
				data:formData,
   				contentType:false,
   				cache:false,
   				processData:false,
   				success:function(data){
					$('.fileinputOFF').fileinput('clear')
    				alert(data);
   				}
  			})
 		});		 
    });
</script>
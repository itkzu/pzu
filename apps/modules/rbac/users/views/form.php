<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>
<style>
	.pass-control {
		display: block;
		width: 100%;
		height: 34px;
		padding: 6px 12px;
		font-size: 14px;
		line-height: 1.42857143;
		color: #555;
		background-color: #fff;
		background-image: none;
		border: 1px solid #ccc;
		border-radius: 0px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
		-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>

<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!-- 
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
						'role='  => 'form'
						, 'id' 	 => 'form_add'
						, 'name' => 'form_add');
				echo form_open($submit,$attributes);
			?>

			<div class="box-body">
				<div class="form-group">
					<?php 
						echo form_input($form['id']);
						echo form_label($form['kduser']['placeholder']);
						echo form_input($form['kduser']);
						echo form_error('kduser','<div class="note">','</div>');
					?>
				</div>

				<div class="form-group">
					<?php 
						echo form_label($form['nmuser']['placeholder']);
						echo form_input($form['nmuser']);
						echo form_error('nmuser','<div class="note">','</div>');
					?>
				</div>

				<div class="form-group">
					<?php 
						echo form_label($form['pwd']['placeholder']);
						echo form_input($form['pwd']);
						echo form_error('pwd','<div class="note">','</div>');
					?>
				</div>

				<div class="form-group">
					<?php 
						echo form_label($form['kddiv']['placeholder']);
						echo form_dropdown($form['kddiv']['name']
															,$form['kddiv']['data'] 
															,$form['kddiv']['value'] 
															,$form['kddiv']['attr']);
						echo form_error('kddiv','<div class="note">','</div>');
					?>
				</div>

				<div class="form-group">
					<?php
						echo form_label($form['group_id']['placeholder']);
					?>

					<div class="checkbox">
						<label>    
							<?=form_checkbox($form['su']) . $form['su']['placeholder'];?>
						</label>
						<?=form_error('su','<div class="note">','</div>');?>
					</div>

					<?php
						echo form_dropdown($form['group_id']['name']
															,$form['group_id']['data']
															,$form['group_id']['value']
															,$form['group_id']['attr']);
						echo form_error('kdgroups','<div class="note">','</div>');
					?>
				</div>


				<div class="form-group">
					<div class="checkbox">
						<label>
							<?=form_checkbox($form['faktif']) . $form['faktif']['placeholder'];?>
						</label>
					</div>
					<?=form_error('faktif','<div class="note">','</div>');?>
				</div>

			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="auto" title="Simpan Data"><span class="fa fa-check"></span>
					Simpan
				</button>
				<a href="<?php echo $reload;?>" class="btn btn-danger" data-toggle="tooltip" data-placement="auto" title="Batal Simpan"><span class="fa fa-remove"></span>
					Batal
				</a>
			</div>
			<?php echo form_close(); ?>
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {


		if ($("#su").prop("checked")) {
			$("#group_id").hide();
		} else{
			$("#group_id").show();
		}


		$("#su").click(function(){
			if($("#su").prop("checked")){
				$("#group_id").hide();
			} else{
				$("#group_id").show();
			}
		});


		function format_nama_terpilih(repo) {
			return repo.full_name || repo.id;
		}

		function format_nama (repo) {
			if (repo.loading)
				return "Mencari data ... ";


			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'>" + repo.id +" | "+ repo.text + " </div>" +
			/*
			"<div class='select2-result-repository__title'>" + repo.id + "</div>";

			markup += "<div class='select2-result-repository__statistics'>" +
			
			//"<div class='select2-result-repository__forks'>" + 
			
			"<i class=\"fa fa-book\"></i> " + repo.text + " </div>" +
			*/			
			//"</div>" +
			"</div>" + 
			"</div>";
			return markup;
		}

		/*
		function format_nama1 (repo) {
			if (repo.loading)
				return "Mencari data ... ";


			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'>" + repo.text + "</div>";

			markup += "</div>" +
			"</div>";
			return markup;
		}

		function format_nama2 (repo) {
			if (repo.loading) return "Mencari data ... ";


			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

			if (repo.alamat) {
				markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.alamat + "</div>";
			}

			markup += "<div class='select2-result-repository__statistics'>" +
			"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nobpkb + " | <i class=\"fa fa-tag\"></i> " + repo.nopol + " | <i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " </div>" +
			"<div class='select2-result-repository__stargazers'> <i class=\"fa fa-file-o\"></i> " + repo.nodo + " | <i class=\"fa fa-calendar\"></i> " + repo.tgldo + "</div>" +
			"</div>" +
			"</div>" + 
			"</div>";
			return markup;
		}
		*/



		$("#kddiv").select2({
			ajax: {
				url: "<?= site_url('users/get_divisi'); ?>",
				type: 'post',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
						page: params.page
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 30) < data.total_count
						}
					};
				},
				cache: true
			},
			placeholder: 'Masukkan Divisi ...',
			dropdownAutoWidth: false,
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			templateResult: format_nama, // omitted for brevity, see the source of this page
			templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
		});


		var kddiv = "<?= @$val['kddiv']; ?>";
		if (kddiv) {
			var kddivSelect = $('#kddiv');
			$.ajax({
				url: "<?= site_url('users/get_divisi'); ?>",
				type: 'post',
				dataType: 'json',
				delay: 250,
				data: {
					kddiv: kddiv
				}
			}).then(function (data) {
				// create the option and append to Select2
				var option = new Option(data.items[0].text, data.items[0].id, true, true);
				kddivSelect.append(option).trigger('change');

				// manually trigger the `select2:select` event
				kddivSelect.trigger({
					type: 'change.select2',
					params: {
						data: data
					}
				});
			});
		}
	});
</script>
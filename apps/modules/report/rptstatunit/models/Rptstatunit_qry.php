<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptstatunit_qry
 *
 * @author adi
 */
class Rptstatunit_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getDataUnit() {
        $nama = $this->input->post('nama');
        $this->db->where('LOWER(nodo)', strtolower($nama));
        $this->db->select(" nodo,tgldo,noso,tglso,nosin,nora,

                                kdtipex,nmtipe,nopo,tglpo,nmwarna,tahun,nmsup,nama,alamat,kel,kec,kota,nohp,nmsales,nmsales_header,ket,
                                status_otr,harga_otr,um,disc,disc_leasing,um_riel,nmleasingx,angsuran,kirim_tagih,nmprogleas,tenor,
                                ar_konsumen,ar_fincoy,ar_jp,ar_matriks");
        $query = $this->db->get('pzu.vl_jual');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataBayar() {
        $nama = $this->input->post('nama');
        $query = $this->db->query("select a.noso ,  (select sum(nilai) from pzu.v_ar_bayar where noso=a.noso and jenis in ('PC','DP','BT')) as pc ,
                                                    (select nilai from pzu.v_ar_bayar where noso=a.noso and jenis='PL') as pl ,
                                                    (select nilai from pzu.v_ar_bayar where noso=a.noso and jenis='JP') as jp ,
                                                    (select nilai from pzu.v_ar_bayar where noso=a.noso and jenis='MX') as mx
                                    from pzu.t_so a join pzu.vl_jual b on a.noso=b.noso where b.nodo ='".$nama."'");

        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataBBN() {
        $nodo = $this->input->post('nodo');
        $this->db->where('LOWER(nodo)', strtolower($nodo));
        $this->db->select(" nopol, nobpkb,  tgl_aju_fa, tgl_buat_fa, tgl_trm_fa, tgl_kps, tgl_aju_bbn, tgl_trm_bbn,
                                            tgl_trm_notis, tgl_trm_stnk, tgl_trm_tnkb, tgl_trm_bpkb,
                                            tgl_srh_notis, tgl_srh_stnk, tgl_srh_tnkb, tgl_srh_bpkb");
        $query = $this->db->get('pzu.t_do');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataTFA() {
        $nodo = $this->input->post('nodo');
        $this->db->where('LOWER(nodo)', strtolower($nodo));
        $this->db->select("notfa");
        $query = $this->db->get('pzu.t_tfa_d');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataKPS() {
        $nodo = $this->input->post('nodo');
        $this->db->where('LOWER(nodo)', strtolower($nodo));
        $this->db->select("nokps");
        $query = $this->db->get('pzu.t_kps');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataBBN_aju() {
        $nodo = $this->input->post('nodo');
        $this->db->where('LOWER(nodo)', strtolower($nodo));
        $this->db->select("nobbn_aju ");
        $query = $this->db->get('pzu.t_bbn_aju_d ');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataBBN_trm() {
        $nodo = $this->input->post('nodo');
        $this->db->where('LOWER(nodo)', strtolower($nodo));
        $this->db->select("nobbn_trm ");
        $query = $this->db->get('pzu.t_bbn_trm_d');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataKBBN() {
        $nodo = $this->input->post('nodo');
        $this->db->where('LOWER(nodo)', strtolower($nodo));
        $this->db->select("noskbbn_leas");
        $query = $this->db->get('pzu.t_skbbn_leas_d');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getNama() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nama_s)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(noso)', strtolower($q));
        $this->db->or_like('LOWER(nosin)', strtolower($q));
        $this->db->or_like('LOWER(nora)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama_s) like '".strtolower($q)."' then 1
                                when LOWER(nama_s) like '".strtolower($q)."%' then 2
                                when LOWER(nama_s) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama_s");
        $query = $this->db->get('pzu.vl_jual');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['nodo'],
                    'text' => $value['nama_s'],
                    'alamat' => $value['alamatktp_s'],
                    'noso' => $value['noso'],
                    'nodo' => $value['nodo'],
                    'tgldo' => $value['tgldo'],
                    'nmtipe' => $value['nmtipe'],
                    'nosin' => $value['nosin'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'noso' => '',
                'nodo' => '',
                'tgldo' => '',
                'nmtipe' => '',
                'nosin' => 'nosin',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nama']) ){
            if($_GET['nama']){
                $nama = $_GET['nama'];
            }else{
                $nama = '';
            }
        }else{
            $nama = '';
        }

        $aColumns = array('no',
                          'jenis',
                          'nmkb',
                          'nocetak',
                          'notrx',
                          'tgltrx',
                          'drkpd',
                          'ket',
                          'nilai',
                          'noso');
	    $sIndexColumn = "noso";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select '' as no, a.noso, a.jenis, a.nmkb, a.nocetak, a.notrx, a.tgltrx, a.drkpd, a.ket, a.nilai
                      from pzu.v_ar_bayar_det a join pzu.vl_jual b on a.noso=b.noso where b.nodo = '".$nama."' ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }
}

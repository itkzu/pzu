<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptbkfakturshowroom_qry
 *
 * @author adi
 */
class Rptbkfakturshowroom_qry extends CI_Model {

    //put your code here
    protected $res = "";
    protected $delete = "";
    protected $state = "";

    public function __construct() {
        parent::__construct();
    }

    public function json_dgview() {
        error_reporting(-1);
        if (isset($_GET['periode_awal'])) {
            if ($_GET['periode_awal']) {
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode_awal = $this->apps->dateConvert($_GET['periode_awal']); //$tgl1[1].$tgl1[0];
            } else {
                $periode_awal = '';
            }
        } else {
            $periode_awal = '';
        }

        if (isset($_GET['periode_akhir'])) {
            if ($_GET['periode_akhir']) {
                //$tgl2 = explode('-', $_GET['periode_akhir']);
                $periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']); //$tgl2[1].$tgl2[0];
            } else {
                $periode_akhir = '';
            }
        } else {
            $periode_akhir = '';
        }
        $aColumns = array('nodo',
                            'tgldo',
                            'nama',
                            'alamat',
                            'noktp',
                            'npwp',
                            'harga_oftr',
                            'harga_net_oftr',
                            'tot_disc',
                            'dpp_unit',
                            'ppn_unit',
                            'ket');
	$sIndexColumn = "nodo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT   nodo,
                        tgldo,
                        nama,
                        alamat,
                        noktp,
                        npwp,
                        nmbarang,
                        harga_oftr,
                        harga_net_oftr,
                        tot_disc,
                        dpp_unit,
                        ppn_unit,
                        ket
                      FROM pzu.v_faktur_ppn_jual 
                      WHERE (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$periode_awal."' AND '".$periode_akhir."')
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "ORDER BY tglkasbon,nokasbon";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
//        echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '-';
                    }
                    if(is_numeric($aRow[ $aColumns[$i] ])){
                        $row[] = (float) $aRow[ $aColumns[$i] ];
                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                //23 - 28
               $output['aaData'][] = $row;
	}
	echo  json_encode( $output );  
    }

    public function submit() {
        $input = $this->input->post();
        error_reporting(-1);
        if (isset($input['periode_awal'])) {
            if ($input['periode_awal']) {
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode_awal = $this->apps->dateConvert($input['periode_awal']); //$tgl1[1].$tgl1[0];
            } else {
                $periode_awal = '';
            }
        } else {
            $periode_awal = '';
        }

        if (isset($input['periode_akhir'])) {
            if ($input['periode_akhir']) {
                //$tgl2 = explode('-', $_GET['periode_akhir']);
                $periode_akhir = $this->apps->dateConvert($input['periode_akhir']); //$tgl2[1].$tgl2[0];
            } else {
                $periode_akhir = '';
            }
        } else {
            $periode_akhir = '';
        }

        $str = "SELECT nodo,
                    to_char(tgldo,'DD/MM/YYYY') AS tgldo,
                    nama,
                    alamat,
                    noktp,
                    npwp,
                    nmbarang,
                    harga_oftr,
                    harga_net_oftr,
                    tot_disc,
                    dpp_unit,
                    ppn_unit,
                    ket,
                    to_char(tgldo,'mm') as mm,
                    to_char(tgldo,'yyyy') as yyyy
                      FROM pzu.v_faktur_ppn_jual 
                WHERE (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$periode_awal."' AND '".$periode_akhir."')";
        $query = $this->db->query($str);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        } else {
            return false;
        }
    }

}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Closegl
 *
 * @author adi
 */
class Closegl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('closegl/submit'),
            'add' => site_url('closegl/add'),
            'edit' => site_url('closegl/edit'),
            'reload' => site_url('closegl'),
        );
        $this->load->model('closegl_qry');

        $kddiv = $this->closegl_qry->getDataCabang();
        // $this->data['kddiv'] = array(
        //     "" => "-- Pilih Kode Akun --",
        //   );
        foreach ($kddiv as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->closegl_qry->json_dgview();
    }

    public function tglkb() {
        echo $this->closegl_qry->tglkb();
    }

    public function cekpost() {
        echo $this->closegl_qry->cekpost();
    }

    public function setAktiva() {
        echo $this->closegl_qry->setAktiva();
    }

    public function setPasiva() {
        echo $this->closegl_qry->setPasiva();
    }

    public function setLR() {
        echo $this->closegl_qry->setLR();
    }

    public function process() {
        echo $this->closegl_qry->process();
    }

    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->closegl_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kddiv'],
                    'value'    => '',
                    'name'     => 'kddiv',
                    'autofocus'   => ''
            ),
           'tglkb'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => date('Y-m'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),

            //detail
           'aktiva'=> array(
                    'placeholder' => 'Nilai Aktiva',
                    'id'          => 'aktiva',
                    'name'        => 'aktiva',
                    'value'       => set_value('aktiva'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'pasiva'=> array(
                    'placeholder' => 'Nilai Pasiva',
                    'id'          => 'pasiva',
                    'name'        => 'pasiva',
                    'value'       => set_value('pasiva'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'lr'=> array(
                    'placeholder' => 'Laba / (Rugi)',
                    'id'          => 'lr',
                    'name'        => 'lr',
                    'value'       => set_value('lr'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
        );
    }

    private function validate() {
        $config = array(
            array(
                    'field' => 'Periode',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <div class="table-responsive">
                    <table style="width: 4000px;" class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;width: 250px;">NAMA</th>
                            <th style="text-align: center;">ALAMAT</th>
                            <th style="text-align: center;width: 150px;">KELURAHAN</th>
                            <th style="text-align: center;width: 150px;">KECAMATAN</th>
                            <th style="text-align: center;width: 150px;">KOTA</th>
                            <th style="text-align: center;width: 100px;">NO. HP</th>
                            <th style="text-align: center;width: 170px;">TIPE UNIT</th>
                            <th style="text-align: center;width: 100px;">NO. MESIN</th>
                            <th style="text-align: center;width: 100px;">NO. RANGKA</th>
                            <th style="text-align: center;width: 70px;">LEASING</th>
                            <th style="text-align: center;width: 170px;">SALES</th>
                            <th style="text-align: center;width: 70px;">NO. DO</th>
                            <th style="text-align: center;width: 100px;">TANGGAL DO</th>
                            <th style="text-align: center;width: 100px;">TGL AJU FA</th>
                            <th style="text-align: center;width: 100px;">TGL TERBIT FA</th>
                            <th style="text-align: center;width: 100px;">TGL TERIMA FA</th>
                            <th style="text-align: center;width: 100px;">AJU BBN BJ</th>
                            <th style="text-align: center;width: 110px;">TERIMA TAGIHAN</th>
                            <th style="text-align: center;width: 100px;">NO. POLISI</th>
                            <th style="text-align: center;width: 100px;">TRM NOTICE</th>
                            <th style="text-align: center;width: 100px;">TRM STNK</th>
                            <th style="text-align: center;width: 100px;">TRM TNKB</th>
                            <th style="text-align: center;width: 100px;">TRM BPKB</th>
                            <th style="text-align: center;width: 100px;">NO. BPKB</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">NAMA</th>
                            <th style="text-align: center;">ALAMAT</th>
                            <th style="text-align: center;">KELURAHAN</th>
                            <th style="text-align: center;">KECAMATAN</th>
                            <th style="text-align: center;">KOTA</th>
                            <th style="text-align: center;">NO. HP</th>
                            <th style="text-align: center;">TIPE UNIT</th>
                            <th style="text-align: center;">NO. MESIN</th>
                            <th style="text-align: center;">NO. RANGKA</th>
                            <th style="text-align: center;">LEASING</th>
                            <th style="text-align: center;">SALES</th>
                            <th style="text-align: center;">NO. DO</th>
                            <th style="text-align: center;">TANGGAL DO</th>
                            <th style="text-align: center;">TGL AJU FA</th>
                            <th style="text-align: center;">TGL TERBIT FA</th>
                            <th style="text-align: center;">TGL TERIMA FA</th>
                            <th style="text-align: center;">AJU BBN BJ</th>
                            <th style="text-align: center;">TERIMA TAGIHAN</th>
                            <th style="text-align: center;">NO. POLISI</th>
                            <th style="text-align: center;">TRM NOTICE</th>
                            <th style="text-align: center;">TRM STNK</th>
                            <th style="text-align: center;">TRM TNKB</th>
                            <th style="text-align: center;">TRM BPKB</th>
                            <th style="text-align: center;">NO. BPKB</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });

        var column = [];

        /*
        for (i = 4; i <= 7; i++) { 
            column.push({ 
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    // return formmatedvalue;
                    } 
                });
        }
        */

        column.push({ 
            "aTargets": [ 12,13,14,15,16,17,19,20,21,22 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                //return type === 'export' ? data : moment(data).format('L');
            },
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({     
            "aoColumnDefs": column,
            "order": [[ 11, 'asc' ]],
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( 
                            //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            //,{ "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(0)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptbbnblmtrmsk/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'potrait',
                    pageSize: 'legal'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>
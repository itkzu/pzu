<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptaksi_qry
 *
 * @author adi
 */
class Rptaksi_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();
	}

	public function json_dgview() {
			error_reporting(-1);
			if (isset($_GET['ket'])) {
					if ($_GET['ket']) {
							//$tgl1 = explode('-', $_GET['periode_awal']);
							$ket = $_GET['ket']; //$tgl1[1].$tgl1[0];
					} else {
							$ket = '';
					}
			} else {
					$ket = '';
			}

			if( isset($_GET['periode']) ){
					if($_GET['periode']){
							$tgl2 = explode('-',$_GET['periode']);
							$periode = $tgl2[0].$tgl2[1]; //$this->apps->dateConvert($_GET['periode_akhir']);//
					}else{
							$periode = '';
					}
			}else{
					$periode = '';
			}

			if (isset($_GET['periode_awal'])) {
					if ($_GET['periode_awal']) {
						$periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
					} else {
							$periode_awal = '';
					}
			} else {
					$periode_awal = '';
			}

			if (isset($_GET['periode_akhir'])) {
					if ($_GET['periode_akhir']) {
						$periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl1[1].$tgl1[0];
					} else {
							$periode_akhir = '';
					}
			} else {
					$periode_akhir = '';
			}
			$aColumns = array('noaksi', 'tglaksi', 'nmsup', 'ket', 'total', 'disc', 'bylain', 'totalnet', 'sppnx', 'dpp', 'ppn', 'grandtot');
			$sIndexColumn = "noaksi";
			$sLimit = "";
			if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
					$sLimit = " LIMIT " . $_GET['iDisplayLength'];
			}
			if($ket=='periode_between'){
				$sTable=" ( SELECT noaksi, tglaksi, nmsup, ket, total, disc, bylain, totalnet, sppnx, dpp, ppn, grandtot
												FROM pzu.vl_aksi where tglaksi between '".$periode_awal."' AND '".$periode_akhir."') AS a";
			} else {
				$sTable=" ( SELECT noaksi, tglaksi, nmsup, ket, total, disc, bylain, totalnet, sppnx, dpp, ppn, grandtot
												FROM pzu.vl_aksi where periode = '".$periode."') AS a";
			}
								        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
								        {
								            if($_GET['iDisplayStart']>0){
								                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
								                        intval( $_GET['iDisplayStart'] );
								            }
								        }

								        $sOrder = "";
								        if ( isset( $_GET['iSortCol_0'] ) )
								        {
								                $sOrder = " ORDER BY  ";
								                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
								                {
								                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
								                        {
								                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
								                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
								                        }
								                }

								                $sOrder = substr_replace( $sOrder, "", -2 );
								                if ( $sOrder == " ORDER BY" )
								                {
								                        $sOrder = "";
								                }
								        }
								        $sWhere = "";

								        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
								        {
										$sWhere = " WHERE (";
										for ( $i=0 ; $i<count($aColumns) ; $i++ )
										{
											$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
										}
										$sWhere = substr_replace( $sWhere, "", -3 );
										$sWhere .= ')';
								        }

								        for ( $i=0 ; $i<count($aColumns) ; $i++ )
								        {
								            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
								            {
								                $ix = $i - 1;
								                if ( $sWhere == "" )
								                {
								                    $sWhere = " WHERE ";
								                }
								                else
								                {
								                    $sWhere .= " AND ";
								                }
								                //
								                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
								                //echo $sWhere;
								            }
								        }


								        /*
								         * SQL queries
								         */
								        if(empty($sOrder)){
								            $sOrder = "order by tglaksi";
								        }
								        $sQuery = "
								                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
								                FROM   $sTable
								                $sWhere
								                $sOrder
								                $sLimit
								                ";

								        //echo $sQuery;

								        $rResult = $this->db->query( $sQuery);

								        $sQuery = "
								                SELECT COUNT(".$sIndexColumn.") AS jml
								                FROM $sTable
								                $sWhere";    //SELECT FOUND_ROWS()

								        $rResultFilterTotal = $this->db->query( $sQuery);
								        $aResultFilterTotal = $rResultFilterTotal->result_array();
								        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

								        $sQuery = "
								                SELECT COUNT(".$sIndexColumn.") AS jml
								                FROM $sTable
								                $sWhere";
								        $rResultTotal = $this->db->query( $sQuery);
								        $aResultTotal = $rResultTotal->result_array();
								        $iTotal = $aResultTotal[0]['jml'];

								        $output = array(
								                "sEcho" => intval($_GET['sEcho']),
								                "iTotalRecords" => $iTotal,
								                "iTotalDisplayRecords" => $iFilteredTotal,
								                "data" => array()
								        );

												$detail = $this->getdetail($periode_awal, $periode_akhir,$periode,$ket);
												foreach ($rResult->result_array() as $aRow) {
														foreach ($aRow as $key => $value) {
																if (is_numeric($value)) {
																		$aRow[$key] = (float) $value;
																} else {
																		$aRow[$key] = $value;
																}
														}
														$aRow['detail'] = array();
														foreach ($detail as $value) {
																if ($aRow['noaksi'] == $value['noaksi']) {
																		$aRow['detail'][] = $value;
																}
														}
														$output['data'][] = $aRow;
												}
								        echo  json_encode( $output );
	}

	private function getdetail($periode_awal, $periode_akhir,$periode, $ket) {
			$output = array();
			//echo $ket;
			//echo $periode;
			if($ket=='periode_between'){
				$str = "SELECT noaksi,kdaks,nmaks,qty,harga,subtot
								FROM pzu.vl_aksi_d where tglaksi between '".$periode_awal."' AND '".$periode_akhir."' ";
			} else {
				$str = "SELECT noaksi,kdaks,nmaks,qty,harga,subtot
								FROM pzu.vl_aksi_d where periode = '".$periode."'";
			}

			$q = $this->db->query($str);
			//echo $this->db->last_query();
			$res = $q->result_array();

			foreach ($res as $aRow) {
					foreach ($aRow as $key => $value) {
							if (is_numeric($value)) {
									$aRow[$key] = (float) $value;
							} else {
									$aRow[$key] = $value;
							}
					}
					$output[] = $aRow;
			}
			return $output;
	}

	public function json_dgview_2() {
			error_reporting(-1);
			if (isset($_GET['ket'])) {
					if ($_GET['ket']) {
							//$tgl1 = explode('-', $_GET['periode_awal']);
							$ket = $_GET['ket']; //$tgl1[1].$tgl1[0];
					} else {
							$ket = '';
					}
			} else {
					$ket = '';
			}

			if( isset($_GET['periode']) ){
					if($_GET['periode']){
							$tgl2 = explode('-',$_GET['periode']);
							$periode = $tgl2[0].$tgl2[1]; //$this->apps->dateConvert($_GET['periode_akhir']);//
					}else{
							$periode = '';
					}
			}else{
					$periode = '';
			}

			if (isset($_GET['periode_awal'])) {
					if ($_GET['periode_awal']) {
						$periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
					} else {
							$periode_awal = '';
					}
			} else {
					$periode_awal = '';
			}

			if (isset($_GET['periode_akhir'])) {
					if ($_GET['periode_akhir']) {
						$periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl1[1].$tgl1[0];
					} else {
							$periode_akhir = '';
					}
			} else {
					$periode_akhir = '';
			}
		$aColumns = array('noaksi', 'tglaksi', 'nmsup', 'ket', 'total', 'disc', 'bylain', 'totalnet', 'sppnx', 'dpp', 'ppn', 'grandtot', 'nmaks', 'qty', 'harga', 'subtot');
$sIndexColumn = "noaksi";
			$sLimit = "";
			if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
					$sLimit = " LIMIT " . $_GET['iDisplayLength'];
			}
			if($ket=='periode_between'){
				$sTable=" ( SELECT noaksi, tglaksi, nmsup, ket, total, disc, bylain, totalnet, sppnx, dpp, ppn, grandtot, nmaks, qty, harga, subtot
											FROM pzu.vl_aksi_d where tglaksi between '".$periode_awal."' AND '".$periode_akhir."') AS a";
			} else {
				$sTable=" ( SELECT noaksi, tglaksi, nmsup, ket, total, disc, bylain, totalnet, sppnx, dpp, ppn, grandtot, nmaks, qty, harga, subtot
											FROM pzu.vl_aksi_d where periode = '".$periode."') AS a";
			}

			if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
			{
					if($_GET['iDisplayStart']>0){
							$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
											intval( $_GET['iDisplayStart'] );
					}
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
							$sOrder = " ORDER BY  ";
							for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
							{
											if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
											{
															$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
																			($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
											}
							}

							$sOrder = substr_replace( $sOrder, "", -2 );
							if ( $sOrder == " ORDER BY" )
							{
											$sOrder = "";
							}
			}
			$sWhere = "";

			if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
			{
	$sWhere = " WHERE (";
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
	}
	$sWhere = substr_replace( $sWhere, "", -3 );
	$sWhere .= ')';
			}

			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{

					if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
					{
							if ( $sWhere == "" )
							{
									$sWhere = " WHERE ";
							}
							else
							{
									$sWhere .= " AND ";
							}
							//echo $sWhere."<br>";
							$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
					}
			}


			        if(empty($sOrder)){
			            $sOrder = "order by tglaksi";
			        }
			/*
			 * SQL queries
			 */
			$sQuery = "
							SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
							FROM   $sTable
							$sWhere
							$sOrder
							$sLimit
							";

//        echo $sQuery;

			$rResult = $this->db->query( $sQuery);

			$sQuery = "
							SELECT COUNT(".$sIndexColumn.") AS jml
							FROM $sTable
							$sWhere";    //SELECT FOUND_ROWS()

			$rResultFilterTotal = $this->db->query( $sQuery);
			$aResultFilterTotal = $rResultFilterTotal->result_array();
			$iFilteredTotal = $aResultFilterTotal[0]['jml'];

			$sQuery = "
							SELECT COUNT(".$sIndexColumn.") AS jml
							FROM $sTable
							$sWhere";
			$rResultTotal = $this->db->query( $sQuery);
			$aResultTotal = $rResultTotal->result_array();
			$iTotal = $aResultTotal[0]['jml'];

			$output = array(
							"sEcho" => intval($_GET['sEcho']),
							"iTotalRecords" => $iTotal,
							"iTotalDisplayRecords" => $iFilteredTotal,
							"aaData" => array()
			);


			foreach ( $rResult->result_array() as $aRow )
			{
							$row = array();
							for ( $i=0 ; $i<count($aColumns) ; $i++ )
							{
									if($aRow[ $aColumns[$i] ]===null){
											$aRow[ $aColumns[$i] ] = '';
									}
									if(($i>=9 && $i<=11)){
											$row[] = (float) $aRow[ $aColumns[$i] ];

									}else{
											$row[] = $aRow[ $aColumns[$i] ];
									}
							}
							//23 - 28
							//$row[23] = "<label>".$aRow['disc']."</label>";
						 $output['aaData'][] = $row;
}
echo  json_encode( $output );
	}
	}

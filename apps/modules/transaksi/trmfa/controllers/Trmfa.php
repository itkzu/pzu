<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Trmfa
 *
 * @author
 */
class Trmfa extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(    
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('trmfa/submit'),
            'add' => site_url('trmfa/add'),
            'edit' => site_url('trmfa/edit'),
            'reload' => site_url('trmfa'),
        );
        $this->load->model('trmfa_qry');
        $supplier = $this->trmfa_qry->getkota();
        foreach ($supplier as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 

    public function json_dgview() {
        echo $this->trmfa_qry->json_dgview();
    }

    public function set_nosin() {
        echo $this->trmfa_qry->set_nosin();
    }

    public function set_ket() {
        echo $this->trmfa_qry->set_ket();
    }

    public function getdetPO() {
        echo $this->trmfa_qry->getdetPO();
    }   

    public function json_dgview_detail() {
        echo $this->trmfa_qry->json_dgview_detail();
    }   

    public function addDetail() {
        echo $this->trmfa_qry->addDetail();
    }   

    public function submit() {
        echo $this->trmfa_qry->submit();
    }

    public function batal() {
        echo $this->trmfa_qry->batal();
    }
    private function _init_add(){ 

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
            'nosin'     => array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),
            'tgltrmfa'     => array(
                    'placeholder' => 'Tgl Terima',
                    //'type'        => 'hidden',
                    'id'          => 'tgltrmfa',
                    'name'        => 'tgltrmfa',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nama'     => array(
                    'placeholder' => 'Nama Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'alamat'     => array(
                    'placeholder' => 'Alamat Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'kel'     => array(
                    'placeholder' => 'Kel.',
                    //'type'        => 'hidden',
                    'id'          => 'kel',
                    'name'        => 'kel',
                    'value'       => set_value('kel'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'kec'     => array(
                    'placeholder' => 'Kec.',
                    //'type'        => 'hidden',
                    'id'          => 'kec',
                    'name'        => 'kec',
                    'value'       => set_value('kec'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'kota'     => array(
                    'placeholder' => 'Kota.',
                    //'type'        => 'hidden',
                    'attr'        => array(
                       'id'    => 'kota',
                       'class' => 'form-control',
                     ),
                    'data'        => $this->data['kota'],
                    'name'        => 'kota',
                    'value'       => set_value('kota'), 
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'noktp'     => array(
                    'placeholder' => 'No. KTP/TDP',
                    //'type'        => 'hidden',
                    'id'          => 'noktp',
                    'name'        => 'noktp',
                    'value'       => set_value('noktp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'kdtipe'     => array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tahun'     => array(
                    'placeholder' => 'Tahun',
                    //'type'        => 'hidden',
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'value'       => set_value('tahun'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'cc'     => array(
                    'placeholder' => 'Isi Silinder',
                    //'type'        => 'hidden',
                    'id'          => 'cc',
                    'name'        => 'cc',
                    'value'       => set_value('cc'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'warna'     => array(
                    'placeholder' => 'Warna',
                    //'type'        => 'hidden',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => set_value('warna'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nora2'     => array(
                    'placeholder' => 'No. Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora2',
                    'name'        => 'nora2',
                    'value'       => set_value('nora2'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nosin2'     => array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin2',
                    'name'        => 'nosin2',
                    'value'       => set_value('nosin2'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nofaktur'     => array(
                    'placeholder' => 'Nomor Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nofaktur',
                    'name'        => 'nofaktur',
                    'value'       => set_value('nofaktur'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tglfaktur'     => array(
                    'placeholder' => 'Tgl Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'tglfaktur',
                    'name'        => 'tglfaktur',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'harga'     => array(
                    'placeholder' => 'Harga',
                    //'type'        => 'hidden',
                    'id'          => 'harga',
                    'name'        => 'harga',
                    'value'       => set_value('harga'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'nodo'     => array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tgldo'     => array(
                    'placeholder' => 'Tgl DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'notpt'     => array(
                    'placeholder' => 'No. TPT',
                    //'type'        => 'hidden',
                    'id'          => 'notpt',
                    'name'        => 'notpt',
                    'value'       => set_value('notpt'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nosut'     => array(
                    'placeholder' => 'No. SUT',
                    //'type'        => 'hidden',
                    'id'          => 'nosut',
                    'name'        => 'nosut',
                    'value'       => set_value('nosut'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            // 'banner'     => array( 
            //         //'type'        => 'hidden',
            //         'id'          => 'banner',
            //         'name'        => 'banner',
            //         'value'       => set_value('banner'),
            //         'class'       => 'form-control',
            //         'style'       => 'text-transform: uppercase;',
            //         'readonly' => '',
            // ),  
            // 'ket'     => array( 
            //         // 'type'        => 'hidden',
            //         'id'          => 'ket',
            //         'name'        => 'ket',
            //         'value'       => set_value('ket'),
            //         'class'       => 'form-control',
            //         'style'       => 'text-transform: uppercase;',
            //         'readonly' => '',
            // ),  
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nodo = $this->uri->segment(3);
            // echo $nodo;
        }
        $this->_check_id($nodo);
        $this->data['form'] = array(    
            'nosin'     => array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => $this->val[0]['nosin'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
                    // 'readonly' => '',
            ),
            'tgltrmfa'     => array(
                    'placeholder' => 'Tgl Terima',
                    //'type'        => 'hidden',
                    'id'          => 'tgltrmfa',
                    'name'        => 'tgltrmfa', 
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
                    // 'readonly' => '',
            ),
            'nama'     => array(
                    'placeholder' => 'Nama Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => $this->val[0]['nama'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),
            'alamat'     => array(
                    'placeholder' => 'Alamat Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => $this->val[0]['alamat_s'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'kel'     => array(
                    'placeholder' => 'Kel.',
                    //'type'        => 'hidden',
                    'id'          => 'kel',
                    'name'        => 'kel',
                    'value'       => $this->val[0]['kel_s'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'kec'     => array(
                    'placeholder' => 'Kec.',
                    //'type'        => 'hidden',
                    'id'          => 'kec',
                    'name'        => 'kec',
                    'value'       => $this->val[0]['kec_s'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'kota'     => array(
                    'placeholder' => 'Kota.',
                    //'type'        => 'hidden',
                    'attr'        => array(
                       'id'    => 'kota',
                       'class' => 'form-control',
                     ),
                    'name'        => 'kota',
                    'data'        => $this->data['kota'],
                    'value'       => $this->val[0]['kota_s'],
                    'style'       => 'text-transform: uppercase;', 
            ),  
            'noktp'     => array(
                    'placeholder' => 'No. KTP/TDP',
                    //'type'        => 'hidden',
                    'id'          => 'noktp',
                    'name'        => 'noktp',
                    'value'       => $this->val[0]['noktp'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'kdtipe'     => array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => $this->val[0]['kdtipe2'],
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly' => '',
            ), 
            'tahun'     => array(
                    'placeholder' => 'Tahun',
                    //'type'        => 'hidden',
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'value'       => $this->val[0]['tahun'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'cc'     => array(
                    'placeholder' => 'Isi Silinder',
                    //'type'        => 'hidden',
                    'id'          => 'cc',
                    'name'        => 'cc',
                    'value'       => $this->val[0]['cc'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'warna'     => array(
                    'placeholder' => 'Warna',
                    //'type'        => 'hidden',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => $this->val[0]['nmwarna'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nora2'     => array(
                    'placeholder' => 'No. Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora2',
                    'name'        => 'nora2',
                    'value'       => $this->val[0]['nora'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nosin2'     => array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin2',
                    'name'        => 'nosin2',
                    'value'       => $this->val[0]['nosin'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nofaktur'     => array(
                    'placeholder' => 'Nomor Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nofaktur',
                    'name'        => 'nofaktur',
                    'value'       => $this->val[0]['nofaktur'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'tglfaktur'     => array(
                    'placeholder' => 'Tgl Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'tglfaktur',
                    'name'        => 'tglfaktur',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
                    // 'readonly' => '',
            ),
            'harga'     => array(
                    'placeholder' => 'Harga',
                    //'type'        => 'hidden',
                    'id'          => 'harga',
                    'name'        => 'harga',
                    'value'       => $this->val[0]['harga_fa'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    // 'readonly' => '',
            ), 
            'nodo'     => array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => $this->val[0]['nodo'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly' => '',
            ), 
            'tgldo'     => array(
                    'placeholder' => 'Tgl DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => $this->val[0]['tgldo'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'notpt'     => array(
                    'placeholder' => 'No. TPT',
                    //'type'        => 'hidden',
                    'id'          => 'notpt',
                    'name'        => 'notpt',
                    'value'       => $this->val[0]['notpt'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'nosut'     => array(
                    'placeholder' => 'No. SUT',
                    //'type'        => 'hidden',
                    'id'          => 'nosut',
                    'name'        => 'nosut',
                    'value'       => $this->val[0]['nosut'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->trmfa_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

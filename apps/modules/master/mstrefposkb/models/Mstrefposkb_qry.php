<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstrefposkb_qry
 *
 * @author adi
 */
class Mstrefposkb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kdrefkb="";
    public function __construct() {
        parent::__construct();
    }

    public function select_data() {
        $kdrefkb = $this->uri->segment(3);
        $this->db->where('kdrefkb',$kdrefkb);
        $q = $this->db->get("pzu.refkb_m");
        $res = $q->result_array();
        return $res;
    }

    public function getKodeAkun() {
        $this->db->where("jnsakun NOT IN ('LRD','LRT','LRB')");
        $this->db->where("faktif","true");
        $this->db->order_by("kdakun");
        $q = $this->db->get("glr.v_akun_div");
        $res = $q->result_array();
        return $res;
    }

    public function printAll() {
        $this->db->select("kdrefkb
                            , nmrefkb
                            , CASE WHEN dk = 'D' THEN 'Debit'
                                ELSE 'Kredit'
                              END AS dk
                            , CASE WHEN faktif = 't' THEN 'Ya'
                                ELSE 'Tidak'
                              END AS faktif
                            , ket");
        $this->db->order_by('faktif','desc');
        $this->db->order_by('dk','asc');
        $this->db->order_by('nmrefkb','asc');
        $q = $this->db->get("pzu.refkb_m");
        $res = $q->result_array();
        $detail = $this->getdetail();
        $data = array();
        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['kdrefkb']==$value['kdrefkb']){
                    $aRow['detail'][]= $value;
                }
            }
            $data[] = $aRow;
        }
        return $data;
    }

    private function getdetail(){
        $output = array();
        $str = "SELECT refkb_d.kdrefkb
                    , refkb_d.nourut
                    , refkb_d.kddiv
                    , refkb_d.kdakun
                    , v_akun_div.nmakun
                    , CASE WHEN v_akun_div.dk = 'D' THEN 'Debit'
                        ELSE 'Kredit'
                      END AS dk
                    , refkb_d.mpl * 100 as mpl
                      FROM pzu.refkb_d
                      JOIN glr.v_akun_div ON refkb_d.kdakun = v_akun_div.kdakun  and refkb_d.kddiv = v_akun_div.kddiv
                    ORDER BY refkb_d.nourut";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
	    }
        return $output;
    }

    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('nmrefkb', 'dk', 'faktif', 'ket','kdrefkb', );
    	$sIndexColumn = "kdrefkb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT kdrefkb
                        , nmrefkb
                        , CASE WHEN dk = 'D' THEN 'Debit'
                            ELSE 'Kredit'
                          END AS dk
                        , CASE WHEN faktif = 't' THEN 'Ya'
                            ELSE 'Tidak'
                          END AS faktif
                        , ket
                        FROM pzu.refkb_m) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by nmrefkb ";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['kdrefkb']==$value['kdrefkb']){
                    $aRow['detail'][]= $value;
                }
            }
            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('mstrefposkb/edit/'.$aRow['kdrefkb'])."\">Edit</a>";
            $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdrefkb']."');\">Hapus</button>";

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['kdrefkb']) ){
            if($_GET['kdrefkb']){
                $kdrefkb = $_GET['kdrefkb'];
            }else{
                $kdrefkb = '';
            }
        }else{
            $kdrefkb = '';
        }

        $aColumns = array('nourut', 'kdakun', 'nmakun', 'dk', 'mpl', 'kdrefkb');
	    $sIndexColumn = "kdrefkb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT refkb_d.kdrefkb
                        , refkb_d.nourut
                        , refkb_d.kddiv
                        , refkb_d.kdakun
                        , v_akun_div.nmakun
                        , v_akun_div.dk
                        , refkb_d.mpl * 100 as mpl
                      FROM pzu.refkb_d
                        JOIN glr.v_akun_div ON refkb_d.kdakun = v_akun_div.kdakun and refkb_d.kddiv = v_akun_div.kddiv
                      WHERE refkb_d.kdrefkb::text = '".$kdrefkb."'
                          AND refkb_d.kddiv = '".$this->session->userdata('data')['kddiv']."'
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['kdrefkb']==$value['kdrefkb']){
                    $aRow['detail'][]= $value;
                }
            }
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['kdrefkb']."','".$aRow['nourut']."','".$aRow['kdakun']."','".$aRow['mpl']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['kdrefkb']."','".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );

    }

    public function addDetail() {
        try {
            $array = $this->input->post();
            $this->db->trans_begin();
            // Insert Edit Delete Table Master Start
            if(empty($array['kdrefkb'])){
                unset($array['kdrefkb']);
                $data = array(
                    'nmrefkb' => $array['nmrefkb']
                    , 'dk' => $array['dk']
                    , 'faktif' => $array['faktif']
                    , 'ket' => $array['ket']
                );
                $resl = $this->db->insert('pzu.refkb_m',$data);
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }else{
                    $array['kdrefkb'] = $this->db->insert_id();
                }
            }elseif(!empty($array['kdrefkb']) && empty($array['stat'])){
                $data = array(
                    'kdrefkb' => $array['kdrefkb']
                    , 'nmrefkb' => $array['nmrefkb']
                    , 'dk' => $array['dk']
                    , 'faktif' => $array['faktif']
                    , 'ket' => $array['ket']
                );
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $resl = $this->db->update('pzu.refkb_m', $data);
                // echo $this->db->last_query();
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }elseif(!empty($array['kdrefkb']) && !empty($array['stat'])){
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $this->db->set("faktif","false");
                $resl = $this->db->update('pzu.refkb_m');
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }else{
                $this->db->trans_rollback();
                throw new Exception(" Error : ". $this->apps->err_code("Variabel tidak sesuai"));
            }
            // Insert Edit Delete Table Master End

            if(empty($array['kdrefkb'])){
                $this->db->trans_rollback();
                $err = "Kode Referensi Tidak Ada!, Transaksi Dibatalkan";
                throw new Exception(" Error : ". $this->apps->err_code($err));
            }

            if(empty($array['nourut'])){
                $data = array(
                    'nourut' => $this->getNourut($array['kdrefkb'])
                    , 'kdakun' => $array['kdakun']
                    , 'mpl' => ((int)$array['mpl'] / 100)
                    , 'kdrefkb' => $array['kdrefkb']
                    , 'kddiv' => $this->session->userdata('data')['kddiv']
                );
                $resl = $this->db->insert('pzu.refkb_d',$data);
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }elseif(!empty($array['nourut']) && empty($array['stat'])){
                $data = array(
                    'nourut' => $array['nourut']
                    , 'kdakun' => $array['kdakun']
                    , 'mpl' => ((int)$array['mpl'] / 100)
                    , 'kdrefkb' => $array['kdrefkb']
                    , 'kddiv' => $this->session->userdata('data')['kddiv']
                );
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $this->db->where('nourut', $array['nourut']);
                $resl = $this->db->update('pzu.refkb_d', $data);
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }elseif(!empty($array['nourut']) && !empty($array['stat'])){
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $this->db->where('nourut', $array['nourut']);
                $resl = $this->db->delete('pzu.refkb_d');
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }else{
                $this->db->trans_rollback();
                throw new Exception(" Error : ". $this->apps->err_code("Variabel tidak sesuai"));
            }
        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

		$this->db->trans_commit();
		$this->res = "Data Terproses";
		$this->state = "1";
		$this->kdrefkb = $array['kdrefkb'];

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            'kdrefkb' => $this->kdrefkb,
            );
        return $arr;

    }

    public function submit() {
        try {
            $array = $this->input->post();
            $this->db->trans_begin();
            // Insert Edit Delete Table Master Start
            if(empty($array['kdrefkb'])){
                unset($array['kdrefkb']);
                $data = array(
                    'nmrefkb' => $array['nmrefkb']
                    , 'dk' => $array['dk']
                    , 'faktif' => $array['faktif']
                    , 'ket' => $array['ket']
                );
                $resl = $this->db->insert('pzu.refkb_m',$data);
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }else{
                    $array['kdrefkb'] = $this->db->insert_id();
                }
            }elseif(!empty($array['kdrefkb']) && empty($array['stat'])){
                $data = array(
                    'kdrefkb' => $array['kdrefkb']
                    , 'nmrefkb' => $array['nmrefkb']
                    , 'dk' => $array['dk']
                    , 'faktif' => $array['faktif']
                    , 'ket' => $array['ket']
                );
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $resl = $this->db->update('pzu.refkb_m', $data);
                // echo $this->db->last_query();
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }elseif(!empty($array['kdrefkb']) && !empty($array['stat'])){
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $this->db->delete('pzu.refkb_d');
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $resl = $this->db->delete('pzu.refkb_m');
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }else{
                $this->db->trans_rollback();
                throw new Exception(" Error : ". $this->apps->err_code("Variabel tidak sesuai"));
            }
            // Insert Edit Delete Table Master End

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }
		$this->db->trans_commit();
		$this->res = "Data Terproses";
		$this->state = "1";
		$this->kdrefkb = $array['kdrefkb'];

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            'kdrefkb' => $this->kdrefkb,
            );
        return $arr;

    }

    public function validasi() {
        try {
            $array = $this->input->post();
            $this->db->trans_begin();
            // Insert Edit Delete Table Master Start
            if(!empty($array['kdrefkb']) && empty($array['stat'])){
                $data = array(
                    'kdrefkb' => $array['kdrefkb']
                    , 'nmrefkb' => $array['nmrefkb']
                    , 'dk' => $array['dk']
                    , 'faktif' => $array['faktif']
                    , 'ket' => $array['ket']
                );
                $this->db->where('kdrefkb', $array['kdrefkb']);
                $resl = $this->db->update('pzu.refkb_m', $data);
                // echo $this->db->last_query();
                if( ! $resl){
                    $this->db->trans_rollback();
                    $err = $this->db->error();
                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                }
            }else{
                $this->db->trans_rollback();
                throw new Exception(" Error : ". $this->apps->err_code("Variabel tidak sesuai"));
            }
            // Insert Edit Delete Table Master End

            $kdrefkb = $this->input->post('kdrefkb');
            $this->db->select('CASE WHEN SUM(mpl) IS NULL THEN 0 ELSE SUM(mpl) END as mpl');
            $this->db->where('kdrefkb',$kdrefkb);
            $q = $this->db->get("pzu.refkb_d");
            if($q->num_rows()>0){
                $jml = $q->result_array();
                $res = $jml[0]['mpl'];
            }else{
                $res = 0;
            }
            $this->db->trans_commit();
        }catch (Exception $e) {
            $res = 0;
        }
        return $res;
    }

    private function getNourut($kdrefkb) {
        $str = "SELECT CASE WHEN max(nourut) IS NULL
                        THEN 1 ELSE max(nourut)+1
                    END AS nourut
                    FROM pzu.refkb_d
                  WHERE kdrefkb='".$kdrefkb."'";
        $q = $this->db->query($str);
        $res = $q->result_array();
        return $res[0]['nourut'];
    }

}

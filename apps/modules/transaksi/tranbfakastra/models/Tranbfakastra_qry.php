<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Tranbfakastra_qry
 *
 * @author adi
 */
class Tranbfakastra_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getData() {
        $this->db->select("nodo, to_char(tgldo,'DD-MM-YYYY') AS tgldo, nama"
                . " , alamat, kel, kec, kota, nohp, kode, kdtipe"
                . " , nmtipe, nosin, nora, to_char(tgl_trm_fa,'DD-MM-YYYY') AS tgl_trm_fa");
        $nodo = $this->input->post('nodo');
        $this->db->where("nodo",$nodo);
        $this->db->where("tgl_aju_fa IS NOT NULL");
        $q = $this->db->get("pzu.vl_bbn_status");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
    
    public function submit() {
        try {
            $array = $this->input->post();
            if(!empty($array['nodo']) && !empty($array['stat'])){
                $this->db->where('nodo', $array['nodo']);
                $this->db->set('tgl_aju_fa',NULL);
                $resl = $this->db->update('pzu.t_do');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

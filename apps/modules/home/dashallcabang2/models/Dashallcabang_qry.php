<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashallcabang_qry
 *
 * @author adi
 */
class Dashallcabang_qry extends CI_Model {

    //put your code here
    protected $res = "";
    protected $delete = "";
    protected $state = "";

    public function __construct() {
        parent::__construct();
    }

    public function get_penjualan() {
        error_reporting(-1);

        if (isset($_GET['periode_awal'])) {
            if ($_GET['periode_awal']) {
                //$dt_awal = explode("-", $_GET['periode_awal']);
                //$periode_awal = $dt_awal[2].'-'.$dt_awal[1].'-'.$dt_awal[0];
                $periode_awal = $this->apps->dateConvert($_GET['periode_awal']);
            } else {
                $periode_awal = '';
            }
        } else {
            $periode_awal = '';
        }

        $aColumns = array('no',
            'kddiv',
            'mtd',
            'mtdx',
            'mtddiff',
            'ytd',
            'ytdx',
            'ytddiff',);
        $sIndexColumn = "kddiv";
        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() OVER () AS no,
                        kddiv,
                        mtd,
                        mtdx,
                        mtd - mtdx AS mtddiff,
                        ytd,
                        ytdx,
                        ytd - ytdx AS ytddiff
                      FROM ( select * from imp.pl_do_total('{$periode_awal}') ) jual ) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " AND (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        $no = 1;
        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aRow[$aColumns[$i]] === null) {
                    $aRow[$aColumns[$i]] = '-';
                }
                if (is_numeric($aRow[$aColumns[$i]])) {
                    $row[] = (float) $aRow[$aColumns[$i]];
                } else {
                    $row[] = $aRow[$aColumns[$i]];
                }
            }
            $row[8] = $this->get_target_cabang($aRow['kddiv'], $periode_awal);
            //23 - 28
            $output['aaData'][] = $row;
            $no++;
        }
        echo json_encode($output);
    }

    public function get_target_penjualan() {
        error_reporting(-1);

        if (isset($_GET['periode_awal'])) {
            if ($_GET['periode_awal']) {
                //$dt_awal = explode("-", $_GET['periode_awal']);
                //$periode_awal = $dt_awal[2].'-'.$dt_awal[1].'-'.$dt_awal[0];
                $periode_awal = $this->apps->dateConvert($_GET['periode_awal']);
            } else {
                $periode_awal = '';
            }
        } else {
            $periode_awal = '';
        }

        $aColumns = array('no',
            'kddiv',
            'mtd',);
        $sIndexColumn = "kddiv";
        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() OVER () AS no,
                        kddiv,
                        mtd,
                        mtdx,
                        mtd - mtdx AS mtddiff,
                        ytd,
                        ytdx,
                        ytd - ytdx AS ytddiff
                      FROM ( select * from imp.pl_do_total('{$periode_awal}') ) jual ) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " AND (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        $no = 1;
        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aRow[$aColumns[$i]] === null) {
                    $aRow[$aColumns[$i]] = '-';
                }
                if (is_numeric($aRow[$aColumns[$i]])) {
                    $row[] = (float) $aRow[$aColumns[$i]];
                } else {
                    $row[] = $aRow[$aColumns[$i]];
                }
            }
            $row[2] = $this->get_target_cabang($aRow['kddiv'], $periode_awal);
            $row[3] = $aRow['mtd'];
            $row[4] = (int) $row[3] - (int) $row[2];
            //23 - 28
            $output['aaData'][] = $row;
            $no++;
        }
        echo json_encode($output);
    }

    public function get_target_cabang($nama_cabang, $periode) {
        try {

            switch ($nama_cabang) {
                case "PZU A.YANI":
                    $url = "pzusmg1.ip-dynamic.com";
                    break;
                case "PZU BREBES":
                    $url = "pzubrebes1.ip-dynamic.com";
                    break;
                case "PZU KUDUS":
                    $url = "pzukudus1.ip-dynamic.com";
                    break;
                case "PZU PATI":
                    $url = "pzupati1.ip-dynamic.com";
                    break;
                case "PZU PURWODADI":
                    $url = "pzupwd1.ip-dynamic.com";
                    break;
                case "PZU SETIABUDI":
                    $url = "pzusmg2.ip-dynamic.com";
                    break;

                default:
                    break;
            }

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/gettargetbulan",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . substr($periode, 0, 7),
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if (is_numeric($response)) {
                    return (int) $response;
                } else {
                    return 0;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }

    public function get_blm_trm_po_leasing($nama_cabang, $periode) {
        try {

            switch ($nama_cabang) {
                case "PZU A.YANI":
                    $url = "pzusmg1.ip-dynamic.com";
                    break;
                case "PZU BREBES":
                    $url = "pzubrebes1.ip-dynamic.com";
                    break;
                case "PZU KUDUS":
                    $url = "pzukudus1.ip-dynamic.com";
                    break;
                case "PZU PATI":
                    $url = "pzupati1.ip-dynamic.com";
                    break;
                case "PZU PURWODADI":
                    $url = "pzupwd1.ip-dynamic.com";
                    break;
                case "PZU SETIABUDI":
                    $url = "pzusmg2.ip-dynamic.com";
                    break;

                default:
                    break;
            }

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getblmtrmpoleasing",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . substr($periode, 0, 7),
                    "nm_cabang:" . $nama_cabang,
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }

    public function get_blm_trm_po_leasing_detail() {
        $cabang = $this->input->post('cabang');
        $kdleasing = $this->input->post('kdleasing');
        $periode = date('Y-m-d');
        try {

            switch ($cabang) {
                case "PZU A.YANI":
                    $url = "pzusmg1.ip-dynamic.com";
                    break;
                case "PZU BREBES":
                    $url = "pzubrebes1.ip-dynamic.com";
                    break;
                case "PZU KUDUS":
                    $url = "pzukudus1.ip-dynamic.com";
                    break;
                case "PZU PATI":
                    $url = "pzupati1.ip-dynamic.com";
                    break;
                case "PZU PURWODADI":
                    $url = "pzupwd1.ip-dynamic.com";
                    break;
                case "PZU SETIABUDI":
                    $url = "pzusmg2.ip-dynamic.com";
                    break;

                default:
                    break;
            }

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getblmtrmpoleasingdetail",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . substr($periode, 0, 7),
                    "nm_cabang: " . $cabang,
                    "kdleasing: ".$kdleasing
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }

    function is_json($string, $return_data = false) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }




    public function get_penjualan_curl($url_cabang) {
        error_reporting(-1);

        if (isset($_GET['periode_awal'])) {
            if ($_GET['periode_awal']) {
                //$dt_awal = explode("-", $_GET['periode_awal']);
                //$periode_awal = $dt_awal[2].'-'.$dt_awal[1].'-'.$dt_awal[0];
                $periode_awal = $this->apps->dateConvert($_GET['periode_awal']);
            } else {
                $periode_awal = '';
            }
        } else {
            $periode_awal = '';
        }

        $aColumns = array('no',
            'kddiv',
            'mtd',
            'mtdx',
            'mtddiff',
            'ytd',
            'ytdx',
            'ytddiff',);
        $sIndexColumn = "kddiv";
        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() OVER () AS no,
                        kddiv,
                        mtd,
                        mtdx,
                        mtd - mtdx AS mtddiff,
                        ytd,
                        ytdx,
                        ytd - ytdx AS ytddiff
                      FROM ( select * from imp.pl_do_total('{$periode_awal}') ) jual ) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " AND (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        $no = 1;
        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aRow[$aColumns[$i]] === null) {
                    $aRow[$aColumns[$i]] = '-';
                }
                if (is_numeric($aRow[$aColumns[$i]])) {
                    $row[] = (float) $aRow[$aColumns[$i]];
                } else {
                    $row[] = $aRow[$aColumns[$i]];
                }
            }
            $row[8] = $this->get_target_cabang($aRow['kddiv'], $periode_awal);
            //23 - 28
            $output['aaData'][] = $row;
            $no++;
        }
        echo json_encode($output);
    }

}

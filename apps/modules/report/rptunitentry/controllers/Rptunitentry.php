<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptunitentry
 *
 * @author adi
 */
class Rptunitentry extends MY_Controller {

	protected $data = '';
	protected $val = '';

	public function __construct() {
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			'submit' => site_url('rptunitentry/submit'),
			'add' => site_url('rptunitentry/add'),
			'edit' => site_url('rptunitentry/edit'),
			'reload' => site_url('rptunitentry'),
		);
		$this->load->model('rptunitentry_qry');

		//$this->load->database('bengkel', true);

        for ($x = date('Y'); $x >= 2000; $x--) {
            $this->data['tahun'][$x] = $x;
        } 
	}

	public function index() {
		$this->_init_add();
		$this->template
				->title($this->data['msg_main'], $this->apps->name)
				->set_layout('main')
				->build('index', $this->data);
	}

	public function json_dgview() {
		  echo $this->rptunitentry_qry->json_dgview();
/*
		if ($this->validate() == TRUE) {
		  echo $this->rptunitentry_qry->json_dgview();
		} else {
			$this->_init_add();
			$this->template
					->title($this->data['msg_main'], $this->apps->name)
					->set_layout('main')
					->build('index', $this->data);
		}
*/
	}

	public function submit() {
		if ($this->validate() == TRUE) {

		} else {
			$this->_init_add();
			$this->template
					->title($this->data['msg_main'], $this->apps->name)
					->set_layout('main')
					->build('index', $this->data);
		}
	}

	private function _init_add() {
		$this->data['form'] = array(
            'tahun'=> array(
                    'attr'        => array(
                        'id'    => 'tahun',
                        'class' => 'form-control',
                    ),
					'placeholder' => 'Tahun',
                    'data'     => $this->data['tahun'],
                    'value'    => '',
                    'name'     => 'tahun',
            ),
		);
	}

	private function validate() {
		$config = array(
			array(
				'field' => 'tahun',
				'label' => 'tahun Awal',
				'rules' => 'required',
			),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			return false;
		} else {
			return true;
		}
	}
}
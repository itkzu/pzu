<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Deldoin
 *
 * @author
 */

class Deldoin extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('deldoin/submit'),
            'add' => site_url('deldoin/add'),
            'edit' => site_url('deldoin/edit'),
            'reload' => site_url('deldoin'),
        );
        $this->load->model('deldoin_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function getData() {
        echo $this->deldoin_qry->getData();
    }
    
    public function proses(){
        echo $this->deldoin_qry->proses();
    }

    public function prosesHead(){
        echo $this->deldoin_qry->prosesHead();
    }
 

    /*
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->deldoin_qry->submit();
            echo $res;
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    */
    
    private function _init_add(){
        $this->data['form'] = array(
           'nodoin'=> array(
                    'placeholder' => 'Nomor DO Intern',
                    'id'          => 'nodoin',
                    'name'        => 'nodoin',
                    'value'       => set_value('nodoin'),
                    'class'       => 'form-control',
                    //'style'       => 'margin-left: 5px;',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
        );
    }
    
    /*
    private function validate() {
        $config = array(
            array(
                    'field' => 'nodo',
                    'label' => 'No. DO',
                    'rules' => 'required|max_length[10]',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
    */
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover js-basic-example dataTable" class="display select">
						<thead>
							<tr>
			                  	<th style="width: 50px;text-align: center;">No. Faktur</th>
			                  	<th style="width: 50px;text-align: center;">Tgl. Faktur</th>
								<th style="width: 50px;text-align: center;">Hutang</th>
								<th style="width: 10px;text-align: center;">Proses</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th>Proses</th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		//var rows_selected = [];
		var column = [];

		column.push({
			"aTargets": [ 0,1,2 ],
			"orderable": true,
			"searchable": true,
		});

		column.push({
			"aTargets": [ 3 ],
			"orderable": false,
			"searchable": false,
      		"sClass":"center"
		});

	    column.push({
	        "aTargets": [ 1 ],
	        "mRender": function (data, type, full) {
	            return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
	        },
	        "sClass": "center"
	    });

	    column.push({
	        "aTargets": [ 2 ],
	        "mRender": function (data, type, full) {
	            return type === 'export' ? data : numeral(data).format('0,0.00');
	          },
	        "sClass": "right"
	    });


		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"bProcessing": true,
			"select":{
				style: 'multi',
			},
			"fixedColumns": {
				leftColumns: 2
			},
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Semua Data"]],
			"bPaginate": true,
			"bDestroy": true,
			"bServerSide": true,
			"order": [[ 1, 'asc' ]],
			"rowCallback": function(row, data, dataIndex){
			/*
		       	// Get row ID
		        var rowId = data[0];

		        // If row ID is in the list of selected row IDs
		        if($.inArray(rowId, rows_selected) !== -1){
					$(row).find('input[type="checkbox"]').prop('checked', true);
					$(row).addClass('selected');
				}
			*/
			},

			// "pagingType": "simple",
			"sAjaxSource": "<?=site_url('acrdf/getData');?>",
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
			}
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Proses" && title!=="No."){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
	});



	function proses(nopo){
      	swal({
          	title: "Konfirmasi",
          	text: "Proses Pengakuan Hutang DF!",
          	type: "warning",
          	showCancelButton: true,
          	confirmButtonColor: "#c9302c",
          	confirmButtonText: "Ya, Lanjutkan!",
          	cancelButtonText: "Batalkan!",
          	closeOnConfirm: true
          	},

          	function () {
              	$.ajax({
					type: "POST",
					url: "<?=site_url('acrdf/proses');?>",
					data: {"nopo":nopo},
					success: function(resp){
                     	var obj = JSON.parse(resp);
                      	//alert(JSON.stringify(resp));
                      	$.each(obj, function(key, data){
                          	swal({
                              	title: data.title,
                              	text: data.msg,
                              	type: data.tipe
                          	}, function(){
                             	table.ajax.reload();
                          	});
                      	});
                  	},
                  	error: function(event, textStatus, errorThrown) {
                      	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  	}
              	});
          	}
      	);
  	}



</script>

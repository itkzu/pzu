<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Unclosekb
 *
 * @author adi
 */
class Unclosekb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('unclosekb/submit'),
            'add' => site_url('unclosekb/add'),
            'edit' => site_url('unclosekb/edit'),
            'reload' => site_url('unclosekb'),
        );
        $this->load->model('unclosekb_qry');

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->unclosekb_qry->json_dgview();
    }

    public function unclosing() {
        echo $this->unclosekb_qry->unclosing();
    }

}

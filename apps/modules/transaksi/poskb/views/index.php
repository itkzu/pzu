<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>

<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdkb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-6">
                                  <div class="form-group">
                                      <?php
                                          echo form_dropdown($form['kdkb']['name'],
                                                              $form['kdkb']['data'],
                                                              $form['kdkb']['value'],
                                                              $form['kdkb']['attr']);
                                          echo form_error('kdkb','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                                </div>
                            </div> 

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="table-responsive">
                            <table class="dataTable table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;text-align: center;">No.</th>
                                        <th style="text-align: center;">Jenis Kas/Bank</th>
                                        <th style="width: 20px;text-align: center;">No. Bukti</th>
                                        <th style="width: 20px;text-align: center;">Tanggal</th>
                                        <th style="text-align: center;">Jenis Transaksi</th>
                                        <th style="text-align: center;">Dari/Kepada</th>
                                        <th style="text-align: center;">Keterangan</th>
                                        <th style="width: 50px;text-align: center;">Debet</th>
                                        <th style="width: 50px;text-align: center;">Kredit</th>
                                        <th style="width: 50px;text-align: center;"><i class="fa fa-th-large"></i></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <!--
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                -->
                            </table>
                        </div>
                    </div>
                  </div> 
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<!-- modal dialog -->
<div id="modal_posting" class="modal fade">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Posting Transaksi Kas/Bank</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['nmkb']['placeholder']);  
                                    ?>
                                </div> 
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nmkb']);
                                        echo form_error('nmkb','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nokb']);
                                        echo form_error('nokb','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nourut']);
                                        echo form_error('nourut','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nocetak']['placeholder']);  
                                        ?>
                                    </div> 
                            </div>
                            <div class="col-xs-2"> 
                                    <div class="form-group">
                                        <?php
                                            echo form_input($form['nocetak']);
                                            echo form_error('nocetak','<div class="note">','</div>');
                                        ?>
                                    </div> 
                            </div>

                            <div class="col-xs-2"> 
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglkb']['placeholder']);  
                                        ?>
                                    </div> 
                            </div>
                            <div class="col-xs-2"> 
                                    <div class="form-group">
                                        <?php
                                            echo form_input($form['tglkb']);
                                            echo form_error('tglkb','<div class="note">','</div>');
                                        ?>
                                    </div> 
                            </div>

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <div class="checkbox">
                                      <label>
                                        <?php
                                            echo form_checkbox($form['jmemo']);
                                            echo "<b> Jurnal Memo </b>";
                                        ?>
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['drkpd']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-8"> 
                                <div class="form-group">
                                  <?php
                                      echo form_input($form['drkpd']);
                                      echo form_error('drkpd','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div>   
                        </div>
                    </div>   

                    <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['ket']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-8"> 
                                <div class="form-group">
                                  <?php
                                      echo form_input($form['ket']);
                                      echo form_error('ket','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div>  

                            <div class="col-xs-2"> 
                                <div class="form-group">
                                  <?php
                                      echo form_input($form['stat']);
                                      echo form_error('stat','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div>  
                        </div>
                    </div>   

                    <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['jenis']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-2"> 
                                <div class="form-group">
                                  <?php
                                      echo form_dropdown($form['jenis']['name'],$form['jenis']['data'] ,$form['jenis']['value'] ,$form['jenis']['attr']);
                                      echo form_error('jenis','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div>  
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['nilai']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-2"> 
                                <div class="form-group">
                                  <?php
                                      echo form_input($form['nilai']);
                                      echo form_error('nilai','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div> 
                        </div>
                    </div>  

                    <div class="col-xs-12 umum">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['kdrefkb']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-8"> 
                                <div class="form-group">
                                  <?php
                                      echo form_dropdown($form['kdrefkb']['name'],$form['kdrefkb']['data'] ,$form['kdrefkb']['value'] ,$form['kdrefkb']['attr']);
                                      echo form_error('kdrefkb','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div>   
                        </div>
                    </div>   

                    <div class="col-xs-12 khusus">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['nmrefkb']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-8"> 
                                <div class="form-group">
                                  <?php
                                      echo form_input($form['nmrefkb']);
                                      echo form_error('nmrefkb','<div class="note">','</div>');
                                  ?>
                                </div> 
                            </div>   
                        </div>
                    </div>      

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="table-responsive">
                            <table class="dataTable_det table table_det table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;text-align: center;">No.</th>
                                        <th style="text-align: center;">Akun/Rekening</th>
                                        <th style="width: 20px;text-align: center;">Debet</th>
                                        <th style="width: 20px;text-align: center;">Kredit</th>  
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <!--
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                -->
                            </table>
                        </div>
                    </div>
                  </div> 

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Posting</button>
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
       $('.umum').show();
       $('.khusus').hide();

      $('.btn-tampil').click(function() {
        tbl_json();
        getkdrefkb();
      });

      $('#kdrefkb').change(function() {
        tbl_det_umum(); 
      });

      $('.btn-simpan').click(function() {
        submit(); 
      });
 
      var column = [];
      for (i = 7; i <= 7; i++) {
          if(i===7){
              column.push({
                  "bSortable": false,
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }else{
              column.push({
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }
      }
        column.push({
            "aTargets": [ 8 ],
            "bSortable": false,
        });

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
          "aoColumnDefs": column,
              "bProcessing": true,
              "bServerSide": true,
              "bDestroy": true,
              "bAutoWidth": false,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('poskb/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            // "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                // { "name": "kddiv", "value": kddiv }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
        });


      // $('#kddiv').change(function(){
      //     table.ajax.reload();
      // });

    });

    function tbl_json(){ 
      var kdkb = $('#kdkb').val();
      var column = [];
      for (i = 7; i <= 7; i++) {
          if(i===7){
              column.push({
                  "bSortable": false,
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }else{
              column.push({
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }
      }
        column.push({
            "aTargets": [ 8 ],
            "bSortable": false,
        });

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
          "aoColumnDefs": column,
              "bProcessing": true,
              "bServerSide": true,
              "bDestroy": true,
              "bAutoWidth": false,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('poskb/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            // "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "kdkb", "value": kdkb }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
        }); 

        //row number
        table.on( 'draw.dt', function () {
            var PageInfo = $('.dataTable').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }

    function posting(nokb,nourut,kdrefkb){
      $('#modal_posting').modal('toggle'); 
      // $('#nilai').autoNumeric();
      $('#nokb').val(nokb);
      $('#kdrefkb').val(kdrefkb); 
      $("#kdrefkb").trigger("chosen:updated");
      $('#kdrefkb').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
      }); 
          $.ajax({
              type: "POST",
              url: "<?=site_url('poskb/det_kb');?>",
              data: {"nokb":nokb,
                     "nourut":nourut,
                     "kdrefkb":kdrefkb},
              success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                        $('#nocetak').val(data.nocetak);
                        $('#nmkb').val(data.nmkb);
                        $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb)));
                        $('#drkpd').val(data.drkpd);
                        $('#ket').val(data.ket);
                        $('#jenis').val(data.dk);
                        $('#jenis').attr('disabled',true);
                        if(data.dk==='D'){
                          $('#nilai').val(data.debet); 
                        }else{
                          $('#nilai').val(data.kredit); 
                        }
                        if(data.jenis==='UM'){
                          // alert(data.nourut);
                          $('#nourut').val(nourut); 
                          $('#stat').val(data.jenis); 
                          $('#nmrefkb').val(data.nmrefkb);
                          $('.umum').show();
                          $('.khusus').hide();
                          tbl_det_umum();
                        } else if(data.nourut2>0) {
                          // alert(data.nourut2);
                          $('#stat').val(data.jenis); 
                          $('#nourut').val(data.nourut2); 
                          $('#nmrefkb').val(data.nmrefkb);
                          $('.umum').hide();
                          $('.khusus').show();
                          tbl_det_khusus();
                        } else {
                          // alert(data.nourut2);
                          $('#stat').val(data.jenis); 
                          $('#nourut').val(data.nourut); 
                          $('#nmrefkb').val(data.nmrefkb);
                          $('.umum').hide();
                          $('.khusus').show();
                          tbl_det_khusus();
                        }
                        
                    });
              },
              error: function(event, textStatus, errorThrown) {
                  swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
              }
          });
    }

    function getkdrefkb(){ 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("poskb/getkdrefkb");?>",
            data: {},
            beforeSend: function() {
                $('#kdrefkb').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis Transaksi --'));
                $("#kdrefkb").trigger("change.chosen");
                if ($('#kdrefkb').hasClass("chosen-hidden-accessible")) {
                    $('#kdrefkb').select2('destroy');
                    $("#kdrefkb").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdrefkb')
                      .append($('<option>', { value : value.kdrefkb })
                      .html("<b style='font-size: 14px;'>" + value.nmrefkb + " </b>"));
                });

                $('#kdrefkb').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function tbl_det_umum(){ 
      var nokb = $('#nokb').val(); 
      var nourut = $('#nourut').val();
      var kdrefkb = $('#kdrefkb').val();
      var jenis = $('#stat').val();
      var column = []; 
      
      column.push({
          "bSortable": false,
          "aTargets": [ 2,3 ],
          "mRender": function (data, type, full) {
              return type === 'export' ? data : numeral(data).format('0,0.00');
                // return type === 'export' ? data : numeral(data).format('0,0.00');
                // return formmatedvalue;
              },
          "sClass": "right"
      });   
        column.push({
            "aTargets": [ 0 ],
            "bSortable": false,
        });

        // column.push({
        //     "aTargets": [ 2 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });


        table_det = $('.table_det').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nmakun" },
                { "data": "debet" },
                { "data": "kredit"}
            ],
              "bProcessing": true,
              "bServerSide": true,
              "bDestroy": true,
              "bAutoWidth": false,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('poskb/det_json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            // "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nokb", "value": nokb },
                                { "name": "nourut", "value": nourut },
                                { "name": "kdrefkb", "value": kdrefkb },
                                { "name": "jenis", "value": jenis }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
        }); 

        //row number
        table_det.on( 'draw.dt', function () {
            var PageInfo = $('.table_det').DataTable().page.info();
            table_det.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }

    function tbl_det_khusus(){ 
      var nokb = $('#nokb').val(); 
      var nourut = $('#nourut').val();
      var kdrefkb = $('#kdrefkb').val();
      var jenis = $('#stat').val();
      var column = []; 
      
      column.push({
          "bSortable": false,
          "aTargets": [ 2,3 ],
          "mRender": function (data, type, full) {
              return type === 'export' ? data : numeral(data).format('0,0.00');
                // return type === 'export' ? data : numeral(data).format('0,0.00');
                // return formmatedvalue;
              },
          "sClass": "right"
      });   
        column.push({
            "aTargets": [ 0 ],
            "bSortable": false,
        });

        // column.push({
        //     "aTargets": [ 2 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });


        table_det = $('.table_det').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nmakun" },
                { "data": "debet" },
                { "data": "kredit"}
            ],
              "bProcessing": true,
              "bServerSide": true,
              "bDestroy": true,
              "bAutoWidth": false,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('poskb/det_k_json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            // "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nokb", "value": nokb },
                                { "name": "nourut", "value": nourut },
                                { "name": "kdrefkb", "value": kdrefkb },
                                { "name": "jenis", "value": jenis }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
        }); 

        //row number
        table_det.on( 'draw.dt', function () {
            var PageInfo = $('.table_det').DataTable().page.info();
            table_det.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }
      
    function submit(){ 
        var nokb = $('#nokb').val();
        var jenis = $('#stat').val();
        var nourut = $('#nourut').val();
        var kdrefkb = $('#kdrefkb').val();
        swal({
            title: "Konfirmasi",
            text: "Posting Kas/Bank "+nokb+" akan diproses.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            },

            function () {
                $.ajax({
                    type: "POST",
                      url: "<?=site_url('poskb/proses');?>",
                      data: {"nokb":nokb,
                            "jenis":jenis,
                            "nourut":nourut,
                            "kdrefkb":kdrefkb},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        $.each(obj, function(key, data){
                            $("#modal_posting").modal("hide");
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                table.ajax.reload();
                                // table_det.ajax.reload();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }
    

</script>

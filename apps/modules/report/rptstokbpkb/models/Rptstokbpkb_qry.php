<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptstokbpkb_qry
 *
 * @author adi
 */
class Rptstokbpkb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function json_dgview() {
        error_reporting(-1);

        if( isset($_GET['periode']) ){
            if($_GET['periode']){
                //$tgl2 = explode('-', $_GET['periode_akhir']);
                $periode = $this->apps->dateConvert($_GET['periode']);//$tgl2[1].$tgl2[0];
            }else{
                $periode = '';
            } 
        }else{
            $periode = '';
        } 

        
        $aColumns = array(
            'tgl_trm_bpkb',
            'nobpkb',
            'nopol',
            'nmtipe',
            'jnsbayarx',
            'nodo',
            'tgldo',
            'nama',
            'alamat',
            'kel',
            'kec',
            'kota',
            'nohp',
            'nosin',
            'nora',
            'nmsales'
        );
	    $sIndexColumn = "nodo";
                            
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //extract(day from (now() - tglterima)) - 14 as umur
        $sTable = " (SELECT
                        nama,
                        alamat,
                        kel,
                        kec,
                        kota,
                        nohp,
                        nmtipe,
                        nosin,
                        nora,
                        jnsbayarx,
                        nmsales,
                        nodo,
                        tgldo,
                        nopol,
                        tgl_trm_bpkb,
                        tgl_srh_bpkb,
                        nobpkb
                      FROM pzu.vl_bbn_status
                    ) AS a";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                    intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                $sOrder = "";
            }
        }
        //$sWhere = "WHERE (to_char(tglpo,'YYYY-MM-DD') <= '".$periode_awal."') AND (tgldo is null OR to_char(tgldo,'YYYY-MM-DD') > '".$periode_awal."')";
        $sWhere = "WHERE (to_char(tgl_trm_bpkb,'YYYY-MM-DD') <= '".$periode."') AND (tgl_srh_bpkb is null OR to_char(tgl_srh_bpkb,'YYYY-MM-DD') > '".$periode."')";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
        //echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        
        foreach ( $rResult->result_array() as $aRow )
        {
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if($aRow[ $aColumns[$i] ]===null){
					$aRow[ $aColumns[$i] ] = '';
                /*
				}
                if(is_numeric($aRow[ $aColumns[$i] ])){
                    $row[] = floatval($aRow[ $aColumns[$i] ]);
                */
				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$output['aaData'][] = $row;

            /*
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if($aRow[ $aColumns[$i] ]===null){
                    $aRow[ $aColumns[$i] ] = '-';
                }
                if($i>=11 && $i<=13){
                    $row[] = (float) $aRow[ $aColumns[$i] ];
                }else{
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
            //23 - 28
            //$row[23] = "<label>".$aRow['disc']."</label>";
            
            if($aRow['umur']===0 || $aRow['umur']<0){
                $row[10] = 0;
            }
            $output['aaData'][] = $row;
            */
	    }
	    echo  json_encode( $output );  
    }
    
 

}

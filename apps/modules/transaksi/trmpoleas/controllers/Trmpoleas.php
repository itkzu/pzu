<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Trmpoleas
 *
 * @author adi
 */
class Trmpoleas extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main'    => $this->msg_main,
            'msg_detail'  => $this->msg_detail, 

            'ctk_um'      => site_url('trmpoleas/ctk_um'),
            'ctk_pl'      => site_url('trmpoleas/ctk_pl'),
            'ctk_jp'      => site_url('trmpoleas/ctk_jp'),
            'ctk_sp'      => site_url('trmpoleas/ctk_sp'), 
            'submit'      => site_url('trmpoleas/submit'),
            'add'         => site_url('trmpoleas/add'),
            'edit'        => site_url('trmpoleas/edit'),
            'reload'      => site_url('trmpoleas'),
        );
        $this->load->model('trmpoleas_qry'); 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk_um() {
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->trmpoleas_qry->ctk_um($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_um',$this->data);
    }

    public function ctk_pl() {
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->trmpoleas_qry->ctk_pl($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_pl',$this->data);
    }

    public function ctk_jp() {
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->trmpoleas_qry->ctk_jp($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_jp',$this->data);
    }

    public function ctk_sp() {
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->trmpoleas_qry->ctk_sp($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_sp',$this->data);
    }  
    // tampil data belum do
    public function json_dgview() {
        echo $this->trmpoleas_qry->json_dgview();
    }
    // kode no do
    public function getNoID() {
        echo $this->trmpoleas_qry->getNoID();
    } 
    public function set_nodo() {
        echo $this->trmpoleas_qry->set_nodo();
    } 
    public function save() {
        echo $this->trmpoleas_qry->save();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
          //baris 1
          'nodo'=> array(
                  'placeholder' => 'No. DO',
                  //'type'        => 'hidden',
                  'id'          => 'nodo',
                  'name'        => 'nodo',
                  'value'       => set_value('nodo'),
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase;',
          ),
          'tgldo'=> array(
                  'placeholder' => 'Tanggal DO',
                  'id'          => 'tgldo',
                  'name'        => 'tgldo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'noso'=> array(
                  'placeholder' => 'No. SPK',
                  //'type'        => 'hidden',
                  'id'          => 'noso',
                  'name'        => 'noso',
                  'value'       => set_value('noso'),
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase;',
                  'readonly'    => '',
          ),
          'tglso'=> array(
                  'placeholder' => 'Tanggal SPK',
                  'id'          => 'tglso',
                  'name'        => 'tglso',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'nopoleasing'=> array(
                  'placeholder' => 'No. PO Leasing',
                  'value'       => set_value('nopoleasing'),
                  'id'          => 'nopoleasing',
                  'name'        => 'nopoleasing',
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase;',
                  'readonly'    => '',
          ),
          'nama_p'=> array(
                  'placeholder' => 'Atas Nama PO',
                  'value'       => set_value('nama_p'),
                  'id'          => 'nama_p',
                  'name'        => 'nama_p',
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => ''
          ),
          'tgltrmpo'=> array(
                  'placeholder' => 'Tgl Terima PO',
                  'id'          => 'tgltrmpo',
                  'name'        => 'tgltrmpo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'tglterbitpo'=> array(
                  'placeholder' => 'Tgl Terbit PO',
                  'id'          => 'tglterbitpo',
                  'name'        => 'tglterbitpo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'tgltagihleas'=> array(
                  'placeholder' => 'Tgl Tagih ke Leasing',
                  'id'          => 'tgltagihleas',
                  'name'        => 'tgltagihleas',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'ket_ar_leas'=> array(
                  'placeholder' => 'Keterangan',
                  'id'          => 'ket_ar_leas',
                  'name'        => 'ket_ar_leas',
                  'value'       => set_value('ket_ar_leas'),
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase;',
                  'readonly'    => '',
          ),
          'nama'=> array(
                  'placeholder' => 'Nama Pemesan',
                  'id'          => 'nama',
                  'class'       => 'form-control',
                  'value'       => set_value('nama'),
                  'name'        => 'nama',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'nama_s'=> array(
                  'placeholder' => 'Nama STNK',
                  'value'       => set_value('nama_s'),
                  'id'          => 'nama_s',
                  'name'        => 'nama_s',
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'alamat'=> array(
                  'placeholder' => 'Alamat STNK',
                  // 'type'        => 'hidden',
                  'value'       => set_value('alamat'),
                  'id'          => 'alamat',
                  'name'        => 'alamat',
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'nmleasing'=> array(
                  'placeholder' => 'Nama Leasing',
                  // 'type'        => 'hidden',
                  'value'       => set_value('nmleasing'),
                  'id'          => 'nmleasing',
                  'name'        => 'nmleasing',
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'nmprogleas'=> array(
                  'placeholder' => 'Program Leasing',
                  // 'type'        => 'hidden',
                  'value'       => set_value('nmprogleas'),
                  'id'          => 'nmprogleas',
                  'name'        => 'nmprogleas',
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
          'pl'=> array(
                  'placeholder' => 'Pelunasan',
                  // 'type'        => 'hidden',
                  'value'       => set_value('pl'),
                  'id'          => 'pl',
                  'name'        => 'pl',
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase; text-align: right;',
                  'readonly'    => '',
          ),
          'jp'=> array(
                  'placeholder' => 'Joint Promo (JP)',
                  // 'type'        => 'hidden',
                  'value'       => set_value('jp'),
                  'id'          => 'jp',
                  'name'        => 'jp',
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase; text-align: right;',
                  'readonly'    => '',
          ),
          'matriks'=> array(
                  'placeholder' => 'Matriks',
                  'id'          => 'matriks',
                  'class'       => 'form-control',
                  'value'       => set_value('matriks'),
                  'name'        => 'matriks',
                  'style'       => 'text-transform: uppercase; text-align: right;',
                  'readonly'    => '',
          ),
          'total'=> array(
                  'placeholder' => 'Total',
                  'id'          => 'total',
                  'class'       => 'form-control',
                  'value'       => set_value('total'),
                  'name'        => 'total',
                  'style'       => 'text-transform: uppercase; text-align: right;', 
                  'readonly'    => '',
          ),
          'ket'=> array(
                  'placeholder' => '',
                  'type'        => 'hidden',
                  'id'          => 'ket',
                  'class'       => 'form-control',
                  'value'       => set_value('ket'),
                  'name'        => 'ket',
                  'style'       => 'text-transform: uppercase; text-align: right;', 
                  'readonly'    => '',
          ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);

        if($this->val[0]['tgltrmpo']===null){
            $this->data['form'] = array(
              'nodo'=> array(
                      'placeholder' => 'No. DO',
                      //'type'        => 'hidden',
                      'id'          => 'nodo',
                      'name'        => 'nodo',
                      'value'        => $this->val[0]['nodo'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
              ),
              'tgldo'=> array(
                      'placeholder' => 'Tanggal DO',
                      'id'          => 'tgldo',
                      'name'        => 'tgldo',
                      'value'         => $this->apps->dateConvert($this->val[0]['tgldo']),
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'noso'=> array(
                      'placeholder' => 'No. SPK',
                      //'type'        => 'hidden',
                      'id'          => 'noso',
                      'name'        => 'noso',
                      'value'       => $this->val[0]['noso'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
              ),
              'tglso'=> array(
                      'placeholder' => 'Tanggal SPK',
                      'id'          => 'tglso',
                      'name'        => 'tglso',
                      'value'       => $this->apps->dateConvert($this->val[0]['tglso']),
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'nopoleasing'=> array(
                      'placeholder' => 'No. PO Leasing',
                      'value'       => $this->val[0]['nopoleasing'],
                      'id'          => 'nopoleasing',
                      'name'        => 'nopoleasing',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;', 
              ),
              'nama_p'=> array(
                      'placeholder' => 'Atas Nama PO',
                      'value'       => $this->val[0]['nama_p'],
                      'id'          => 'nama_p',
                      'name'        => 'nama_p',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
              ),
              'tgltrmpo'=> array(
                      'placeholder' => 'Tgl Terima PO',
                      'id'          => 'tgltrmpo',
                      'name'        => 'tgltrmpo',
                      'value'       => date('d-m-Y'),
                      'class'       => 'form-control calendar',
                      'style'       => '', 
              ),
              'tglterbitpo'=> array(
                      'placeholder' => 'Tgl Terbit PO',
                      'id'          => 'tglterbitpo',
                      'name'        => 'tglterbitpo',
                      'value'       => date('d-m-Y'),
                      'class'       => 'form-control calendar',
                      'style'       => '', 
              ),
              'tgltagihleas'=> array(
                      'placeholder' => 'Tgl Tagih ke Leasing',
                      'id'          => 'tgltagihleas',
                      'name'        => 'tgltagihleas',
                      'value'       => date('d-m-Y'),
                      'class'       => 'form-control calendar',
                      'style'       => '', 
              ),
              'ket_ar_leas'=> array(
                      'placeholder' => 'Keterangan',
                      'id'          => 'ket_ar_leas',
                      'name'        => 'ket_ar_leas',
                      'value'       => $this->val[0]['ket_ar_leas'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;', 
              ),
              'nama'=> array(
                      'placeholder' => 'Nama Pemesan',
                      'id'          => 'nama',
                      'class'       => 'form-control',
                      'value'       => $this->val[0]['nama'],
                      'name'        => 'nama',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'nama_s'=> array(
                      'placeholder' => 'Nama STNK',
                      'value'       => $this->val[0]['nama_s'],
                      'id'          => 'nama_s',
                      'name'        => 'nama_s',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'alamat'=> array(
                      'placeholder' => 'Alamat STNK',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['alamat'],
                      'id'          => 'alamat',
                      'name'        => 'alamat',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ), 
              'nmleasing'=> array(
                      'placeholder' => 'Nama Leasing',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['nmleasing'],
                      'id'          => 'nmleasing',
                      'name'        => 'nmleasing',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'nmprogleas'=> array(
                      'placeholder' => 'Program Leasing',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['nmprogleas'],
                      'id'          => 'nmprogleas',
                      'name'        => 'nmprogleas',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'pl'=> array(
                      'placeholder' => 'Pelunasan',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['pl'],
                      'id'          => 'pl',
                      'name'        => 'pl',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase; text-align: right;',
                      'readonly'    => '',
              ),
              'jp'=> array(
                      'placeholder' => 'Joint Promo (JP)',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['jp'],
                      'id'          => 'jp',
                      'name'        => 'jp',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase; text-align: right;',
                      'readonly'    => '',
              ),
              'matriks'=> array(
                      'placeholder' => 'Matriks',
                      'id'          => 'matriks',
                      'class'       => 'form-control',
                      'value'       => $this->val[0]['matriks'],
                      'name'        => 'matriks',
                      'style'       => 'text-transform: uppercase; text-align: right;',
                      'readonly'    => '',
              ),
              'total'=> array(
                      'placeholder' => 'Total',
                      'id'          => 'total',
                      'class'       => 'form-control',
                      'value'       => set_value('total'),
                      'name'        => 'total',
                      'style'       => 'text-transform: uppercase; text-align: right;', 
                      'readonly'    => '',
              ),
              'ket'=> array(
                      'placeholder' => '',
                      'type'        => 'hidden',
                      'id'          => 'ket',
                      'class'       => 'form-control',
                      'value'       => set_value('ket'),
                      'name'        => 'ket',
                      'style'       => 'text-transform: uppercase; text-align: right;', 
                      'readonly'    => '',
              ),
            ); 
        } else {
            $this->data['form'] = array(
              'nodo'=> array(
                      'placeholder' => 'No. DO',
                      //'type'        => 'hidden',
                      'id'          => 'nodo',
                      'name'        => 'nodo',
                      'value'        => $this->val[0]['nodo'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
              ),
              'tgldo'=> array(
                      'placeholder' => 'Tanggal DO',
                      'id'          => 'tgldo',
                      'name'        => 'tgldo',
                      'value'         => $this->apps->dateConvert($this->val[0]['tgldo']),
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'noso'=> array(
                      'placeholder' => 'No. SPK',
                      //'type'        => 'hidden',
                      'id'          => 'noso',
                      'name'        => 'noso',
                      'value'       => $this->val[0]['noso'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
              ),
              'tglso'=> array(
                      'placeholder' => 'Tanggal SPK',
                      'id'          => 'tglso',
                      'name'        => 'tglso',
                      'value'       => $this->apps->dateConvert($this->val[0]['tglso']),
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'nopoleasing'=> array(
                      'placeholder' => 'No. PO Leasing',
                      'value'       => $this->val[0]['nopoleasing'],
                      'id'          => 'nopoleasing',
                      'name'        => 'nopoleasing',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;', 
              ),
              'nama_p'=> array(
                      'placeholder' => 'Atas Nama PO',
                      'value'       => $this->val[0]['nama_p'],
                      'id'          => 'nama_p',
                      'name'        => 'nama_p',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
              ),
              'tgltrmpo'=> array(
                      'placeholder' => 'Tgl Terima PO',
                      'id'          => 'tgltrmpo',
                      'name'        => 'tgltrmpo',
                      'value'       => $this->apps->dateConvert($this->val[0]['tgltrmpo']),
                      'class'       => 'form-control calendar',
                      'style'       => '', 
              ),
              'tglterbitpo'=> array(
                      'placeholder' => 'Tgl Terbit PO',
                      'id'          => 'tglterbitpo',
                      'name'        => 'tglterbitpo',
                      'value'       => $this->apps->dateConvert($this->val[0]['tglterbitpo']),
                      'class'       => 'form-control calendar',
                      'style'       => '', 
              ),
              'tgltagihleas'=> array(
                      'placeholder' => 'Tgl Tagih ke Leasing',
                      'id'          => 'tgltagihleas',
                      'name'        => 'tgltagihleas',
                      'value'       => $this->apps->dateConvert($this->val[0]['tgltagihleas']),
                      'class'       => 'form-control calendar',
                      'style'       => '', 
              ),
              'ket_ar_leas'=> array(
                      'placeholder' => 'Keterangan',
                      'id'          => 'ket_ar_leas',
                      'name'        => 'ket_ar_leas',
                      'value'       => $this->val[0]['ket_ar_leas'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;', 
              ),
              'nama'=> array(
                      'placeholder' => 'Nama Pemesan',
                      'id'          => 'nama',
                      'class'       => 'form-control',
                      'value'       => $this->val[0]['nama'],
                      'name'        => 'nama',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'nama_s'=> array(
                      'placeholder' => 'Nama STNK',
                      'value'       => $this->val[0]['nama_s'],
                      'id'          => 'nama_s',
                      'name'        => 'nama_s',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'alamat'=> array(
                      'placeholder' => 'Alamat STNK',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['alamat'],
                      'id'          => 'alamat',
                      'name'        => 'alamat',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ), 
              'nmleasing'=> array(
                      'placeholder' => 'Nama Leasing',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['nmleasing'],
                      'id'          => 'nmleasing',
                      'name'        => 'nmleasing',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'nmprogleas'=> array(
                      'placeholder' => 'Program Leasing',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['nmprogleas'],
                      'id'          => 'nmprogleas',
                      'name'        => 'nmprogleas',
                      'class'       => 'form-control',
                      'style'       => '',
                      'readonly'    => '',
              ),
              'pl'=> array(
                      'placeholder' => 'Pelunasan',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['pl'],
                      'id'          => 'pl',
                      'name'        => 'pl',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase; text-align: right;',
                      'readonly'    => '',
              ),
              'jp'=> array(
                      'placeholder' => 'Joint Promo (JP)',
                      // 'type'        => 'hidden',
                      'value'       => $this->val[0]['jp'],
                      'id'          => 'jp',
                      'name'        => 'jp',
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase; text-align: right;',
                      'readonly'    => '',
              ),
              'matriks'=> array(
                      'placeholder' => 'Matriks',
                      'id'          => 'matriks',
                      'class'       => 'form-control',
                      'value'       => $this->val[0]['matriks'],
                      'name'        => 'matriks',
                      'style'       => 'text-transform: uppercase; text-align: right;',
                      'readonly'    => '',
              ),
              'total'=> array(
                      'placeholder' => 'Total',
                      'id'          => 'total',
                      'class'       => 'form-control',
                      'value'       => set_value('total'),
                      'name'        => 'total',
                      'style'       => 'text-transform: uppercase; text-align: right;', 
                      'readonly'    => '',
              ),
              'ket'=> array(
                      'placeholder' => '',
                      'type'        => 'hidden',
                      'id'          => 'ket',
                      'class'       => 'form-control',
                      'value'       => set_value('ket'),
                      'name'        => 'ket',
                      'style'       => 'text-transform: uppercase; text-align: right;', 
                      'readonly'    => '',
              ),
            ); 
        }
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->trmpoleas_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

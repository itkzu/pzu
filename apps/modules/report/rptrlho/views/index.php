<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
			  <h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?> 
			<div class="box-body">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_label('Pilih Bulan Awal');
								echo form_dropdown($form['bulan_awal']['name'],$form['bulan_awal']['data'] ,$form['bulan_awal']['value'] ,$form['bulan_awal']['attr']);
								echo form_error('bulan_awal','<div class="note">','</div>'); 
							?>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_label('Pilih Bulan Akhir');
								echo form_dropdown($form['bulan_akhir']['name'],$form['bulan_akhir']['data'] ,$form['bulan_akhir']['value'] ,$form['bulan_akhir']['attr']);
								echo form_error('bulan_akhir','<div class="note">','</div>'); 
							?>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_label('Pilih Tahun');
								echo form_dropdown($form['tahun']['name'],$form['tahun']['data'] ,$form['tahun']['value'] ,$form['tahun']['attr']);
								echo form_error('tahun','<div class="note">','</div>'); 
							?>
						</div>
					</div>
					
					<div class="col-lg-3">
						<button type="button" class="btn btn-primary btn-tampil" style="margin-top: 25px;" name="submit" value="html">
							Tampil
						</button>
					</div>
					
					<div class="col-lg-12 table-rl-ho">
					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
				</div>
			<?php echo form_close(); ?>
			</div>
		<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".btn-tampil").click(function(){

			$(this).attr("disabled",true)
					.html('<i class="fa fa-cog fa-spin"></i> Mengambil Data');

			if ( $.fn.DataTable.isDataTable('.dataTable') ) {
				$('.dataTable').DataTable().destroy();
			}

			var bulan = ['','Januari','Februari','Maret','April','Mei','Juni'
							,'Juli','Agustus','September','Oktober','November'
							,'Desember','Penutupan'];
			var bulan_awal = $("#bulan_awal").val();
			var bulan_akhir = $("#bulan_akhir").val();
			var tahun = $("#tahun").val();
			var jml_kolom = 0;
			$(".table-rl-ho").html('');
			var table = '<div class="table-responsive">'+
						'<table class="table table-hover table-bordered dataTable">'+
							'<caption>Laporan Laba Rugi Semua Cabang Periode '+bulan[bulan_awal]+' '+tahun+' s/d '+bulan[bulan_akhir]+' '+tahun+'</caption>'+
							'<thead>'+
								'<tr class="periode">'+
									'<th style="text-align: center;">Cabang</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>'+
								'<tr class="pzusmg1">'+
									'<td>'+
										'<b>A. Yani</b>'+
									'</td>'+
								'</tr>'+
								'<tr class="pzubrebes1">'+
									'<td>'+
										'<b>Brebes</b>'+
									'</td>'+
								'</tr>'+
								'<tr class="pzukudus1">'+
									'<td>'+
										'<b>Kudus</b>'+
									'</td>'+
								'</tr>'+
								'<tr class="pzupati1">'+
									'<td>'+
										'<b>Pati</b>'+
									'</td>'+
								'</tr>'+
								'<tr class="pzupwd1">'+
									'<td>'+
										'<b>Purwodadi</b>'+
									'</td>'+
								'</tr>'+
								'<tr class="pzusmg2">'+
									'<td>'+
										'<b>Setiabudi</b>'+
									'</td>'+
								'</tr>'+
							'</tbody>'+
							'<tfoot>'+
								'<tr class="total-periode" style="background: #dadada;">'+
									'<th style="text-align: left;">Total</th>'+
								'</tr>'+
							'</tfoot>'+
						'</table>'+
						'<label style="color: #00a65a;">*Warna hijau -> periode aktif akunting</label>'+
						'<br><label style="color: #DC143C;">*Warna pink -> internet cabang gangguan</label></div>';
				$(".table-rl-ho").html(table);
			for (i = Number(bulan_awal); i <= Number(bulan_akhir); i++) { 
				if(i<=9){
					mm = "0"+i;
				}else{
					mm = i;
				}
				$(".periode").append('<th style="text-align: center;width: 90px;">'+bulan[i]+' '+tahun+'</th>');
				$(".total-periode").append('<th style="text-align: right;width: 90px;"></th>');
				$(".pzusmg1").append('<td style="text-align: right;" id="pzusmg1-'+tahun+mm+'">-</td>');
				$(".pzusmg2").append('<td style="text-align: right;" id="pzusmg2-'+tahun+mm+'">-</td>');
				$(".pzubrebes1").append('<td style="text-align: right;" id="pzubrebes1-'+tahun+mm+'">-</td>');
				$(".pzukudus1").append('<td style="text-align: right;" id="pzukudus1-'+tahun+mm+'">-</td>');
				$(".pzupati1").append('<td style="text-align: right;" id="pzupati1-'+tahun+mm+'">-</td>');
				$(".pzupwd1").append('<td style="text-align: right;" id="pzupwd1-'+tahun+mm+'">-</td>');
				jml_kolom++;
			}
			
			$(".periode").append('<th style="text-align: center;width: 90px; background: #dadada; font-weight: bold;">Total</th>');
			$(".total-periode").append('<th style="text-align: right;width: 90px;"></th>');
			$(".pzusmg1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzusmg1-total">-</td>');
			$(".pzusmg2").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzusmg2-total">-</td>');
			$(".pzubrebes1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzubrebes1-total">-</td>');
			$(".pzukudus1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzukudus1-total">-</td>');
			$(".pzupati1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzupati1-total">-</td>');
			$(".pzupwd1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzupwd1-total">-</td>');
			
			var cbg1 = $.post("<?=site_url('android/getRugiLaba');?>",{
				"url":"http://pzusmg1.ddns.net"
				,"bulan_awal":bulan_awal
				,"bulan_akhir":bulan_akhir
				,"tahun":tahun
				,"rdjenis":"konsol"
				,"idcabang":"pzusmg1"
				,"kddiv":"ZPH01.01S"
			},
			function(data, status){
				if(status==="success"){
					setValue(data,"pzusmg1");
				}else{
					alert("Data: " + data + "\nStatus: " + status);
					$("#pzusmg1").css("background-color", "#ef9a9a");
				}
			});
			
			var cbg2 = $.post("<?=site_url('android/getRugiLaba');?>",{
				//"url":"http://pzusmg2.ddns.net"
				"url":"http://zirang.ddns.net:8080"
				,"bulan_awal":bulan_awal
				,"bulan_akhir":bulan_akhir
				,"tahun":tahun
				,"rdjenis":"konsol"
				,"idcabang":"pzusmg2"
				,"kddiv":"ZPH04.01S"
			},
			function(data, status){
				if(status==="success"){
					setValue(data,"pzusmg2");
				}else{
					alert("Data: " + data + "\nStatus: " + status);
					$("#pzusmg2").css("background-color", "#ef9a9a");
				}
			});
			
			var cbg3 = $.post("<?=site_url('android/getRugiLaba');?>",{
				"url":"http://zirang.ddns.net:8085"
				,"bulan_awal":bulan_awal
				,"bulan_akhir":bulan_akhir
				,"tahun":tahun
				,"rdjenis":"konsol"
				,"idcabang":"pzubrebes1"
				,"kddiv":"ZPH03.01S"
			},
			function(data, status){
				if(status==="success"){
					setValue(data,"pzubrebes1");
				}else{
					alert("Data: " + data + "\nStatus: " + status);
					$("#pzubrebes1").css("background-color", "#ef9a9a");
				}
			});
			
			var cbg4 = $.post("<?=site_url('android/getRugiLaba');?>",{
				"url":"http://zirang.ddns.net:8082"
				,"bulan_awal":bulan_awal
				,"bulan_akhir":bulan_akhir
				,"tahun":tahun
				,"rdjenis":"konsol"
				,"idcabang":"pzukudus1"
				,"kddiv":"ZPH02.01S"
			},
			function(data, status){
				if(status==="success"){
					setValue(data,"pzukudus1");
				}else{
					alert("Data: " + data + "\nStatus: " + status);
					$("#pzukudus1").css("background-color", "#ef9a9a");
				}
			});
			
			var cbg5 = $.post("<?=site_url('android/getRugiLaba');?>",{
				"url":"http://zirang.ddns.net:8081"
				,"bulan_awal":bulan_awal
				,"bulan_akhir":bulan_akhir
				,"tahun":tahun
				,"rdjenis":"konsol"
				,"idcabang":"pzupati1"
				,"kddiv":"ZPH01.02S"
			},
			function(data, status){
				if(status==="success"){
					setValue(data,"pzupati1");
				}else{
					alert("Data: " + data + "\nStatus: " + status);
					$("#pzupati1").css("background-color", "#ef9a9a");
				}
			});
			
			var cbg6 = $.post("<?=site_url('android/getRugiLaba');?>",{
				"url":"http://zirang.ddns.net:8083"
				,"bulan_awal":bulan_awal
				,"bulan_akhir":bulan_akhir
				,"tahun":tahun
				,"rdjenis":"konsol"
				,"idcabang":"pzupwd1"
				,"kddiv":"ZPH02.02S"
			},
			function(data, status){
				if(status==="success"){
					setValue(data,"pzupwd1");
				}else{
					alert("Data: " + data + "\nStatus: " + status);
					$("#pzupwd1").css("background-color", "#ef9a9a");
				}
			});
			
			$.when(cbg1, cbg2, cbg3, cbg4, cbg5, cbg6).done(function(e) {
				swal({
					title: "Informasi",
					text: "Pengambilan data selesai, Status : " + e[1],
					type: "info"
				}, function(){
					
					var column = [];
					for (i = 1; i <= jml_kolom + 1; i++) { 
						column.push({ 
							"aType":"numeric-comma",
							"aTargets": [ i ],
							"mRender": function (data, type, full) {
								return type === 'export' ? data : numeral(data).format('0,0');
								// return formmatedvalue;
								} 
							});
					} 
					$(".dataTable").DataTable({
						"aoColumnDefs": column,
						"lengthMenu": [[-1], ["Semua Data"]],
						"bProcessing": false,
						"bServerSide": false,
						//"searching": false,
						"bDestroy": true,
						"bAutoWidth": false,
							buttons: [
								{extend: 'copy',
									exportOptions: {orthogonal: 'export'}},
								{extend: 'csv',
									exportOptions: {orthogonal: 'export'}},
								{extend: 'excel',
									exportOptions: {orthogonal: 'export'}},
								{extend: 'pdf', 
									orientation: 'landscape',
								},
								{extend: 'print',
									customize: function (win){
											$(win.document.body).addClass('white-bg');
											$(win.document.body).css('font-size', '10px');
											$(win.document.body).find('table')
													.addClass('compact')
													.css('font-size', 'inherit');
									}
								}
							],
						"sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> ",
						"footerCallback": function ( row, data, start, end, display ) {
							var api = this.api(), data;

							// Remove the formatting to get integer data for summation
							var intVal = function ( i ) {
								var myNumeral = numeral(i);
								var value = myNumeral.value();
								
								return typeof value === 'string' ?
									value.replace(/[\$,]/g, '')*1 :
									typeof value === 'number' ?
										value : 0;
							};
							
							for (i = 1; i <= jml_kolom + 1; i++) { 
								// Total over all pages
								total = api
									.column( i )
									.data()
									.reduce( function (a, b) {
										return intVal(a) + intVal(b);
									}, 0 );

								// Update footer
								$( api.column( i ).footer() ).html(
									 numeral(total).format('0,0')
								);
							}
						}
					}); 
				});
				$(".btn-tampil").attr("disabled",false)
								.html('Tampil');
			});
		});
		
		$(".btn-cetak").click(function(){
			print_data("table-rl-ho");
		});
		
	});
	
	function setValue(resp,idcabang){
		if(resp===""){
			$("."+idcabang).css("background-color", "#ffcdd2");
		}else{
			var obj = jQuery.parseJSON(resp);
			if(obj.stat===0){
				//alert(obj.msg);
				$("."+idcabang).css("background-color", "#ffcdd2");
			}else{
				console.log(obj);
				var total = 0;
				$.each(obj, function(key, head){
					// alert(head);
					if(!isNaN(head)){
						$("#"+idcabang+"-"+key).html(head.toFixed(0));
						if(key==="p_aktif"){
							$("#"+idcabang+"-"+head).css("background-color", "#c8e6c9");
							//$("#"+idcabang+"-"+head).css("text-decoration", "underline");
						}else{
							total = Number(total) + Number(head.toFixed(0));
						}
					}
				});
				$("#"+idcabang+"-total").html(total);
			}
		}
	}
</script>
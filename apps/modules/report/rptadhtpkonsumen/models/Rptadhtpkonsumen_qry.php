<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptadhtpkonsumen_qry
 *
 * @author adi
 */
class Rptadhtpkonsumen_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    public function get_data() {
        $periode_akhir = $this->input->post('periode_akhir');
        $str = "select * from pzu.pl_ar_tipkons('".$this->apps->dateConvert($periode_akhir)."')";
        $q = $this->db->query($str);
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = null;
        }
        return json_encode($res);        
    }

}

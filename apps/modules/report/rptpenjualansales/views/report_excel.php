<?php

/* 
 * ***************************************************************
 * Script : pungutan_bank_excel
 * Version : 
 * Date : May 5, 2017 | 9:23:07 PM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

require_once APPPATH.'libraries/PHPExcel.php';
$objPHPExcel = new PHPExcel(); 
$idxsheet = 0;   
define('FORMAT_INDONESIA_CURRENCY', '#,##0.00');
define('FORMAT_INDONESIA_NUMBER', '#,##');
$array = $this->input->post();

$namafile = "REKAP PENJUALAN PER SALESMAN ".  strtoupper(month(date('m'))) ." ".date('Y');
$objWorkSheet = $objPHPExcel->createSheet($idxsheet);
$objWorkSheet->setTitle(strtoupper(month(date('m')))." ".date('Y'));      
if(count($rpt['header'])>0){  
    // Kolom Header Start
    $objWorkSheet->setCellValueByColumnAndRow(0, 1, strtoupper($this->apps->title) . $this->session->userdata('data')['cabang'])
            ->mergeCellsByColumnAndRow(0, 1, 8,1);
    $objWorkSheet->getStyleByColumnAndRow(0, 1)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
    $objWorkSheet->getRowDimension(1)->setRowHeight(18);
    
    $objWorkSheet->setCellValueByColumnAndRow(0, 2, 'REKAPITULASI PENJUALAN PERIODE' 
                                                    .' ' . $this->apps->dateConvert($array['periode_awal'])
                                                    .' s/d ' . $this->apps->dateConvert($array['periode_akhir'])
                                             )
                ->mergeCellsByColumnAndRow(0, 2, 8,2);
    $objWorkSheet->getStyleByColumnAndRow(0, 2)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
    $objWorkSheet->getRowDimension(2)->setRowHeight(18);
 

    // Generate Header Laporan Start
    $colHeader = 0;            
    $namaheader = array(
        'NO',
        'NAMA SALES',
        'TOTAL'
    );

    $rowheader = 4;
    foreach($rpt['header'] as $key => $val){
        array_push($namaheader, $val['nmtipe']);
    }
    foreach ($namaheader as $val) {
        $objWorkSheet->getRowDimension($rowheader)->setRowHeight(70);
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);        
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                    ->getAlignment()->setWrapText(true);
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow($colHeader, $rowheader, $val);  
        if($val==="NO" || $val==="NAMA SALES"|| $val==="TOTAL"){
            $objWorkSheet->mergeCellsByColumnAndRow($colHeader, $rowheader, $colHeader,$rowheader+1);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader, $colHeader,$rowheader+1)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setAutoSize(true); 
        }else{          
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);  
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader, $colHeader+1,$rowheader)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            $objWorkSheet->mergeCellsByColumnAndRow($colHeader, $rowheader, $colHeader+1,$rowheader);
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader+1)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader+1)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader+1)
                            ->getFont()
                                ->setBold(true);
            $objWorkSheet->setCellValueByColumnAndRow($colHeader, $rowheader+1, 'K');  
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(7); 
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+1, $rowheader+1)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+1, $rowheader+1)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
            $objWorkSheet->getStyleByColumnAndRow($colHeader+1, $rowheader+1)
                            ->getFont()
                                ->setBold(true);
            $objWorkSheet->setCellValueByColumnAndRow($colHeader+1, $rowheader+1, 'T'); 
            $objWorkSheet->getColumnDimensionByColumn($colHeader+1)->setWidth(7); 
            
            $colHeader = $colHeader + 1;
        }
        
        if($colHeader===0){
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(5);    
        }elseif($colHeader===1){
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(60);    
        }else{
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(7);    
        }
        $colHeader++;
    }
    $rowDetail = $rowheader + 2;
    $nomor = 1;
    foreach($rpt['detail'] as $val){
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowDetail, $nomor); 
        
        $subtotal = 0;
        $colDetail_sales = 1;
        foreach ($val as $key => $v_kolom) {
            if(!is_numeric($v_kolom)){
                $objWorkSheet->getStyleByColumnAndRow($colDetail_sales, $rowDetail)
                                    ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
                $objWorkSheet->setCellValueByColumnAndRow($colDetail_sales, $rowDetail, $v_kolom);          
                $objWorkSheet->getStyleByColumnAndRow($colDetail_sales, $rowDetail)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);     
            }else{
                $subtotal = $subtotal + $v_kolom;
            }
            $colDetail_sales++;
        }
        $colDetail_total = 2;
        $objWorkSheet->getStyleByColumnAndRow($colDetail_total, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow($colDetail_total, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow($colDetail_total, $rowDetail, $subtotal);   
                
        $colDetail_b = 2;
        foreach ($val as $key => $v_kolom) {           
            if(is_numeric($v_kolom)){
                $objWorkSheet->getStyleByColumnAndRow($colDetail_b, $rowDetail)
                                    ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
                $objWorkSheet->setCellValueByColumnAndRow($colDetail_b, $rowDetail, $v_kolom);          
                $objWorkSheet->getStyleByColumnAndRow($colDetail_b, $rowDetail)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);     
            }
            $colDetail_b++;
        }
        
        $rowDetail++;
        $nomor++;
    }
    
    foreach($rpt['footer'] as $val){
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail,1,$rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail,1,$rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowDetail, 'TOTAL')
                    ->mergeCellsByColumnAndRow(0, $rowDetail,1,$rowDetail); 
        
        $subtotal = 0;
        $colDetail = 2;
        
        foreach ($val as $key => $v_kolom) {
            if($colDetail>1){
                $subtotal = $subtotal + $v_kolom;
            }
        }
        
        
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $subtotal); 
        
        $colDetail_total_b = 3;
        foreach ($val as $key => $v_kolom) {
            $objWorkSheet->getStyleByColumnAndRow($colDetail_total_b, $rowDetail)
                                ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
            $objWorkSheet->setCellValueByColumnAndRow($colDetail_total_b, $rowDetail, $v_kolom);          
            $objWorkSheet->getStyleByColumnAndRow($colDetail_total_b, $rowDetail)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            $colDetail_total_b++;
        }
        
        $rowDetail++;
    }
    $row  = $rowDetail + 1;
    
    // Kepala Administrasi Start
    $objWorkSheet->setCellValueByColumnAndRow(0, $row, "Diperiksa Oleh :"); 
    $objWorkSheet->mergeCellsByColumnAndRow(0, $row, 1, $row);  

    $objWorkSheet->setCellValueByColumnAndRow(0, $row + 1, "Kepala Administrasi"); 
    $objWorkSheet->mergeCellsByColumnAndRow(0, $row + 1, 1, $row + 1);   
    

    $objWorkSheet->setCellValueByColumnAndRow(0, $row + 9, $this->apps->kepala_administrasi); 
    $objWorkSheet->getStyleByColumnAndRow(0, $row + 9)
                    ->getFont()
                        ->setBold(true)
                        ->setUnderline(true);    
    $objWorkSheet->mergeCellsByColumnAndRow(0, $row + 9, 1, $row + 9); 
    // Kepala Administrasi End
    
    
    
    // Kepala Administrasi Start
    $objWorkSheet->setCellValueByColumnAndRow(18, $row, "Mengetahui,"); 
    $objWorkSheet->mergeCellsByColumnAndRow(18, $row, 25, $row);  

    $objWorkSheet->setCellValueByColumnAndRow(18, $row + 1, "Kepala Cabang"); 
    $objWorkSheet->mergeCellsByColumnAndRow(18, $row + 1, 25, $row + 1);   
    

    $objWorkSheet->setCellValueByColumnAndRow(18, $row + 9, $this->apps->kepala_cabang); 
    $objWorkSheet->getStyleByColumnAndRow(18, $row + 9)
                    ->getFont()
                        ->setBold(true)
                        ->setUnderline(true);    
    $objWorkSheet->mergeCellsByColumnAndRow(18, $row + 9, 25, $row + 9); 
    // Kepala Administrasi End
    
    // Admin Penjualan Start
    $objWorkSheet->setCellValueByColumnAndRow(42, $row, "Semarang, ".date('d')." ".month(date('m')) ." ".date('Y')); 
    $objWorkSheet->mergeCellsByColumnAndRow(42, $row, 51, $row);  

    $objWorkSheet->setCellValueByColumnAndRow(42, $row + 1, "Admin Penjualan"); 
    $objWorkSheet->mergeCellsByColumnAndRow(42, $row + 1, 51, $row + 1);   
    

    $objWorkSheet->setCellValueByColumnAndRow(42, $row + 9, $this->apps->admin_penjualan); 
    $objWorkSheet->getStyleByColumnAndRow(42, $row + 9)
                    ->getFont()
                        ->setBold(true)
                        ->setUnderline(true);    
    $objWorkSheet->mergeCellsByColumnAndRow(42, $row + 9, 51, $row + 9); 
    // Admin Penjualan End
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$namafile.'.xls"'); 
        header('Cache-Control: max-age=0'); 
$objWriter->save('php://output'); 
exit();   
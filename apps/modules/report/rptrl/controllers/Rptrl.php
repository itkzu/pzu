<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptrl
 *
 * @author adi
 */
class Rptrl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptrl/submit'),
            'add' => site_url('rptrl/add'),
            'edit' => site_url('rptrl/edit'),
            'reload' => site_url('rptrl'),
        );
        $this->load->model('rptrl_qry');
        $kddiv = $this->rptrl_qry->getDivisiPos();
        $this->data['kddiv'] = array();
        if($kddiv){
            foreach ($kddiv as $value) {
                $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
            }
        }
        for ($x = 1; $x <= 13; $x++) {
            $this->data['bulan'][$x] = strmonth($x).". ".month($x);
        } 
        
        for ($x = date('Y'); $x >= 2015; $x--) {
            $this->data['tahun'][$x] = $x;
        } 
    }
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $this->data['rptakun'] = $this->rptrl_qry->submit();

        if(empty($this->data['rptakun'])){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('debug/err_000',$this->data);
        }else{
            $array = $this->input->post();
            if($array['submit']){
                $this->template
                    ->title($this->data['msg_main'],$this->apps->name)
                    ->set_layout('print-layout')
                    ->build('html',$this->data);       
            }else{
                redirect("rptrl");            
            }    
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kddiv'],
                    'value'    => '',
                    'name'     => 'kddiv',
            ),
            'bulan_awal'=> array(
                    'attr'        => array(
                        'id'    => 'bulan_awal',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['bulan'],
                    'value'    => $this->mm,
                    'name'     => 'bulan_awal',
            ),
            'bulan_akhir'=> array(
                    'attr'        => array(
                        'id'    => 'bulan_akhir',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['bulan'],
                    'value'    => $this->mm,
                    'name'     => 'bulan_akhir',
            ),
            'tahun'=> array(
                    'attr'        => array(
                        'id'    => 'tahun',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['tahun'],
                    'value'    => '',
                    'name'     => 'tahun',
            ),
        );
    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of 
 *
 * @author 
 */

class Unpostdo_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getData() {
        $this->db->select("nodo, to_char(tgldo,'DD-MM-YYYY') AS tgldo, nama"
                . " , alamat, kel, kec, kota, nohp, noso, to_char(tglso,'DD-MM-YYYY') AS tglso, kode"
                . " , kdtipe, nmtipe, nosin, nora, kdwarna, nmwarna, tahun");
        $nodo = $this->input->post('nodo');
        $this->db->where("nodo",$nodo);
        $q = $this->db->get("pzu.vl_jual");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
    
    public function proses() {
        $nodo = $this->input->post('nodo');
        $q = $this->db->query("select code,title,msg,tipe from pzu.do_unpost('". $nodo ."')");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    /*
    public function submit() {
        try {
            $array = $this->input->post();
            if(!empty($array['nodo']) && !empty($array['stat'])){
                $this->db->where('nodo', $array['nodo']);
                $this->db->set('tgl_aju_fa',NULL);
                $resl = $this->db->update('pzu.t_do');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }
    */
}

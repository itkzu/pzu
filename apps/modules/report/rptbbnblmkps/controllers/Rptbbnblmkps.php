<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptbbnblmkps
 *
 * @author adi
 */
class Rptbbnblmkps extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptbbnblmkps/submit'),
            'add' => site_url('rptbbnblmkps/add'),
            'edit' => site_url('rptbbnblmkps/edit'),
            'reload' => site_url('rptbbnblmkps'),
        );
        $this->load->model('rptbbnblmkps_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->rptbbnblmkps_qry->json_dgview();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptbbnblmkps_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode DO Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => set_value('periode_awal'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode DO Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => set_value('periode_akhir'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_awal',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'periode_akhir',
                    'label' => 'Periode Akhir',
                    'rules' => 'required|max_length[20]',
                    ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

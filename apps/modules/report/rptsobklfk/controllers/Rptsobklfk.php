<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptsobklfk
 *
 * @author adi
 */
class Rptsobklfk extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptsobklfk/submit'),
            'add' => site_url('rptsobklfk/add'),
            'edit' => site_url('rptsobklfk/edit'),
            'reload' => site_url('rptsobklfk'),
        );
        $this->load->model('rptsobklfk_qry');
        $cabang = $this->rptsobklfk_qry->getDataCabang();
        foreach ($cabang as $value) {
                $this->data['kdkb'][$value['kdkb']] = $value['nmkb'];
        }
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->rptsobklfk_qry->json_dgview();
    }
    
    public function gettgl() {
        echo $this->rptsobklfk_qry->gettgl();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptsobklfk_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Tanggal Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    //'value'       => set_value('periode_awal'),
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ), 
           'periode_akhir'=> array(
                    'placeholder' => 'Tanggal Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    //'value'       => set_value('periode_awal'),
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ), 
            'kdkb'=> array(
                    'attr'        => array(
                        'id'    => 'kdkb',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kdkb'],
                    'value'    => set_value('kdkb'),
                    'name'     => 'kdkb',
                    'required'    => '',
                    'placeholder' => 'Kas/Bank ',
            ),
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'periode',
                    'label' => 'Periode',
                    'rules' => 'required|max_length[20]',
                ), 
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

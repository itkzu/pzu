<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['noakso']['placeholder']);
                                            echo form_input($form['noakso']);
                                            echo form_error('noakso','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglakso']['placeholder']);
                                            echo form_input($form['tglakso']);
                                            echo form_error('tglakso','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="checkbox">
                        						<label>
                        							<?php
                                          echo form_checkbox($form['ketnondo']);
                                          echo "<b>Pengeluaran Non DO</b>";
                                          echo form_error('ketnondo', '<div class="note">', '</div>');
                        							?>
                        						</label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['ket']['placeholder']);
                                            echo form_input($form['ket']);
                                            echo form_error('ket','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php
                                                echo form_input($form['ketnodo']);
                                                echo form_input($form['nourut']);
                                                echo form_error('nourut', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <?php
                                                echo form_label($form['nodo']['placeholder']);
                                                echo form_dropdown( $form['nodo']['name'],
                                                                    $form['nodo']['value'],
                                                                    $form['nodo']['data'],
                                                                    $form['nodo']['attr']);
                                                echo form_error('nodo', '<div class="note">', '</div>');
                                            ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tgldo']['placeholder']);
                                            echo form_input($form['tgldo']);
                                            echo form_error('tgldo','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['kdunit']['placeholder']);
                                            echo form_input($form['kdunit']);
                                            echo form_error('kdunit','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?php
                                                echo form_label($form['tahun']['placeholder']);
                                                echo form_input($form['tahun']);
                                                echo form_error('tahun','<div class="note">','</div>');
                                            ?>
                                        </div>
                                    </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nospk']['placeholder']);
                                            echo form_input($form['nospk']);
                                            echo form_error('nospk','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nmunit']['placeholder']);
                                            echo form_input($form['nmunit']);
                                            echo form_error('nmunit','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nmcust']['placeholder']);
                                            echo form_input($form['nmcust']);
                                            echo form_error('nmcust','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?php
                                                echo form_label($form['warna']['placeholder']);
                                                echo form_input($form['warna']);
                                                echo form_error('warna','<div class="note">','</div>');
                                            ?>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-add">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">Nama Barang</th>
                                            <th style="width: 100px;text-align: center;">Quantity</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                            <th style="width: 10px;text-align: center;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center;"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-submit">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Transaksi</h4>
                </div>

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label('Pilih Nama Barang');
                                    echo form_dropdown( $form['kdaks']['name'],
                                                        $form['kdaks']['data'] ,
                                                        $form['kdaks']['value'] ,
                                                        $form['kdaks']['attr']);
                                    echo form_error('kdaks','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['qty']['placeholder']);
                                    echo form_input($form['qty']);
                                    echo form_error('qty','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {
        var nodo = $("#ketnodo").val();
        $('#nodo')
                .append($('<option>', { value : nodo })
                .html("<b style='font-size: 14px;'>" + nodo + " </b>")
                .trigger("change"));

        $("#nodo").val(nodo).text();
        $('#nodo option:nth-child(2)').val();
        if($("#ketnodo").val()){
            $("#nodo").click(function(){
                init_nama();
                getData();
            });
        } else {
            init_nama();
            $("#nodo").change(function(){
                getData();
            });        
        }
        check();
        getKodeAkso();
        empty();

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdakun  : "required",
                nominal : "required"
            },
            messages : {
                kdakun  : "Masukkan Kode Akun",
                nominal : "Masukkan Nominal"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });
        $('#qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});


        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });


        column.push({
            "aTargets": [ 1,2,3 ],
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nmaks"  },
                { "data": "qty" },
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noakso", "value": $("#noakso").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('akso/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 0 ).footer() ).html('Total');
                $( api.column( 1 ).footer() ).html(numeral(total).format('0,0'));
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });


        $('.btn-add').click(function(){
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kdaks').select2({
                placeholder: '-- Pilih Nama Barang --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            //alert($('nodo').val());
            clear();
        });

        $('#ketnondo').click(function(){
            check();
        });

        $('.btn-simpan').click(function(){
            if ($("#modalform").valid()) {
                addDetail();
            }
        });

        $('.btn-update').click(function(){
            if ($("#modalform").valid()) {
                UpdateDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){
            if ($("#myform").valid()) {
              submit();
            }
        });
    });

    function check(){
          if ($("#ketnondo").prop("checked")){
              $("#nodo").attr("disabled",true);
              $("#nodo").empty();
              empty();
          } else {
              $("#nodo").attr("disabled",false);
          }
    }

    function empty(){
      $('#tgldo').val('');
      $('#kdunit').val('');
      $('#nmunit').val('');
      $('#ppn').val('');
      $('#tahun').val('');
      $('#warna').val('');
      $('#nmcust').val('');
      $('#nospk').val('');
    }

    function getKodeAkso(){
       //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("akso/getKodeAkso");?>",
            data: {},
            beforeSend: function() {
                $('#kdaks').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Nama Barang --'));
                $("#kdaks").trigger("change.chosen");
                if ($('#kdaks').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks').select2('destroy');
                    $("#kdaks").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdaks')
                        .append($('<option>', { value : value.kdaks })
                        .html("<b style='font-size: 14px;'>" + value.nmaks + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmaks + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getData(){
    	     var nodo = $("#nodo").val();
    	$.ajax({
    		type: "POST",
    		url: "<?=site_url("akso/getData");?>",
    		data: {"nodo":nodo},
    		beforeSend: function() {

    		},
    		success: function(resp){
    			var obj = jQuery.parseJSON(resp);
    			$.each(obj, function(key, value){
    				$("#nospk").val(value.noso);
    				$("#tgldo").val(value.tgldo);
    				$("#nmunit").val(value.nmtipe);
    				$("#tahun").val(value.tahun);
    				$("#kdunit").val(value.kdtipex);
            $("#nmcust").val(value.nama);
    				$("#warna").val(value.nmwarna);
				});
    },
    error:function(event, textStatus, errorThrown) {
    	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    }
    });
    }

    function setBgColour(val,object){
    	if(val){
    		$("#"+object).css("background-color", "#fff");
    	}else{
    		$("#"+object).css("background-color", "#eee");
    	}
    }

    function init_nama(){
    	$("#nodo").select2({
    		ajax: {
    			url: "<?=site_url('akso/getNama');?>",
    			type: 'post',
    			dataType: 'json',
    			delay: 250,
    			data: function (params) {
    				return {
    					q: params.term,
    					page: params.page
    				};
    			},
    			processResults: function (data, params) {
    				params.page = params.page || 1;

    				return {
    					results: data.items,
    					pagination: {
    						more: (params.page * 30) < data.total_count
    					}
    				};
    			},
    			cache: true
    		},
    		placeholder: 'Masukkan No DO ...',
    		dropdownAutoWidth : false,
        width: '100%',
    					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    					minimumInputLength: 3,
    					templateResult: format_nama, // omitted for brevity, see the source of this page
    					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
    				});
    }

    function format_nama_terpilih (repo) {
    	return repo.full_name || repo.id;
    }

    function format_nama (repo) {
    	if (repo.loading) return "Mencari data ... ";


    	var markup = "<div class='select2-result-repository clearfix'>" +
    	"<div class='select2-result-repository__meta'>" +
    	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.id + "</b></div>";

    	if (repo.nama) {
    		markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.nama + "</div>";
    	}

    	markup += "<div class='select2-result-repository__statistics'>" +
    	"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nama + " | <i class=\"fa fa-tag\"></i> " + repo.nmwarna + " | <i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " </div>" +
    	"<div class='select2-result-repository__stargazers'> <i class=\"fa fa-file-o\"></i> " + repo.kdtipe + " | <i class=\"fa fa-calendar\"></i> " + repo.kdtipe + "</div>" +
    	"</div>" +
    	"</div>" +
    	"</div>";
    	return markup;
    }

    function clear(){
        $("#kdaks").val('');
        $("#qty").val('');
    }

    function edit(nourut,kdaks,qty){
        $("#nourut").val(nourut);

        $("#kdaks").val(kdaks);
        //$("#kdakun").val(nmakun);
        $("#kdaks").trigger("change");
        $('#kdaks').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });

        $("#qty").val(qty);

        $('#modal_transaksi').modal('toggle');
    }

    function addDetail(){

        var noakso = $("#noakso").val();
        var tglakso = $("#tglakso").val();
        var nodo = $("#nodo").val();
        //alert(nodo);
        var ket = $("#ket").val();
        var nourut = $("#nourut").val();
        var kdaks = $("#kdaks").val();
        var qty = $("#qty").autoNumeric('get');

        $.ajax({
            type: "POST",
            url: "<?=site_url("akso/addDetail");?>",
            data: {"noakso":noakso
                    ,"tglakso":tglakso
                    ,"nodo":nodo
                    ,"ket":ket
                    ,"nourut":nourut
                    ,"kdaks":kdaks
                    ,"qty":qty  },

            success: function(resp){

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    });

                    if (data.tipe==="success"){
                        $("#nourut").val('');
                        $("#noakso").val(data.noakso);
                        refresh();
                        clear();
                        $("#modal_transaksi").modal("hide");
                    }else{
                        
                    }
                });
            },

            /*
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                refresh();
                clear();

                var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                    $("#noakso").val(data.noakso);
                    if (data.tipe==="success"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                          $("#nourut").val('');
                            refresh();
                        });
                    }else{
                        //refresh();
                    }
                });
            },
            */
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(noakso,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("akso/detaildeleted");?>",
                data: {"noakso":noakso,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('akso');?>';
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nodo = $("#nodo").val();
            var noakso = $("#noakso").val();
            var tglakso = $("#tglakso").val();
            var ket = $("#ket").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("akso/submit");?>",
                data: {"noakso":noakso
                    ,"tglakso":tglakso
                    ,"nodo":nodo
                    ,"ket":ket },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('akso');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

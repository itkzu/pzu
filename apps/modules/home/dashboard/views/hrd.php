<style type="text/css">
    .nopadding {
        padding: 0 !important;
        margin: 0 !important;
        border: none;
    }
    .content-header{
        display: none;
    }
    
    .content {
        min-height: 250px;
        padding: 0px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
    .input-sm{
        height: 20px;
    }
    
    .table>tbody>tr>td
    , .table>tfoot>tr>td
    , .table>thead>tr>td
    {
        padding: 3px 8px;
    }
</style>
<!-- Main row -->
<div class="row">
    <div class="box" style="border-top: 0px">
        <div class="box-body">
            <div class="col-lg-3">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i> <?=$form['periode_akhir']['placeholder'];?>
                    </span>
                    <?php 
                        echo form_input($form['periode_akhir']);
                        echo form_error('periode_akhir','<div class="note">','</div>'); 
                    ?>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-preview">
                            Proses
                        </button>
                    </span>
                </div>      
            </div>
            <div class="col-lg-10">
                
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="row"> 
                    <!-- Split -->
                    <div class="col-lg-12">
                        <div id="" style="" align="center">
                            <table class="table table-hover table-bordered table-striped table-top-sales" style="margin-top: 0px !important;">
                                <caption style="text-align: center; font-weight: 700;color: #ff6363;padding-top: 0px;" class="cap-sales-mtd">
                                    <div>
                                        <label class="label label-primary">Sales Productivity</label>
                                        <label class="label label-success">Counter Productivity</label>
                                    </div>
                                    Penjualan Per Sales MTD
                                </caption>
                                <thead>
                                    <tr>
                                        <th  style="width:10px;">No.</th>
                                        <th>Nama Sales</th>
                                        <th style="width: 100px;">Jabatan</th>
                                        <th style="width: 25px;">Jml</th>
                                    </tr>
                                </thead>
                                <tfoot class="footSalesMTD"></tfoot>
                                <tbody class="bodySalesMTD"></tbody>
                                <tfoot class="footSalesMTD"></tfoot>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row (main row) -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<script>
    $(function () {
        setInterval(function(){  
            getChart();
        }, 1800000);
        $('#periode_akhir').datepicker({
            startView: "year", 
            minViewMode: "months",
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm"      
        });
        getChart();
        $('.btn-preview').click(function(){
            getChart();
        });

        function getChart(){
            var periode = $('#periode_akhir').val();
            getJualPerSalesAktual(periode);
        }

        $('#periode_akhir').on("changeDate", function(e) {
            getChart();
        });
        $('.overlay').hide();
    });

    
    // Tabel untuk penjualan per sales aktual start
    function getJualPerSalesAktual(tahun){
          if ( $.fn.DataTable.isDataTable('.table-top-sales') ) {
            $('.table-top-sales').DataTable().destroy();
          } 
        $.ajax({
            type: "POST",
            url: "<?=site_url('dashboard/getJualPerSalesAktual');?>",
            data: {"tanggal":tahun},
            success: function(resp){  
                var obj = jQuery.parseJSON(resp);
                var no = 1;
                var totalJualSL = 0;
                var totalJualCS = 0;
                
                var totalSL = 0;
                var totalCS = 0;
                var totalAll = 0;
                $(".bodySalesMTD").html('');
                $.each(obj.jual, function(key, data){
                      $(".bodySalesMTD").append('<tr>' + 
                                                  '<td style="text-align: center;">'+no+'</td>' + 
                                                  '<td style="text-align: left;">'+data.nmsales+'</td>' + 
                                                  '<td style="text-align: left;">'+data.status+'</td>' + 
                                                  '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
                                              '</tr>');
                        if(data.status==="CS"){
                            totalJualCS = Number(totalJualCS) + Number(data.jml);                            
                        }else{
                            totalJualSL = Number(totalJualSL) + Number(data.jml);
                        }
                        totalAll = Number(totalAll) + Number(data.jml);     
                      no++;
                });     
                $(".footSalesMTD").html('<tr>' + 
                                                '<td style="text-align: center;"></td>' + 
                                                '<td style="text-align: center;" colspan="2"><b>TOTAL</b></td>'+
                                                '<td style="text-align: right;"><b>'+numeral(totalAll).format('0,0')+'</b></td>' + 
                                            '</tr>');
                $(".table-top-sales").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
                });
                
                $.each(obj.jmlsales, function(key, data){
                    if(data.status==="CS"){
                        totalCS = Number(totalCS) + Number(data.jml);                            
                    }else{
                        totalSL = Number(totalSL) + Number(data.jml);
                    }
                });
                
                var avgCS = (Number(totalJualCS)/Number(totalCS));
                var avgSL = (Number(totalJualSL)/Number(totalSL));
                
                $(".cap-sales-mtd").html('<div style="font-size:13px;">' +
                                                ' <label class="label label-primary">Sales Productivity '+avgSL.toFixed(1)+'</label> ' +
                                                ' | <label class="label label-success">Counter Productivity '+avgCS.toFixed(1)+'</label> ' +
                                            '</div>' +
                                            'Penjualan Per Sales MTD : ' + tahun);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
    // Tabel untuk penjualan per sales aktual end
</script>
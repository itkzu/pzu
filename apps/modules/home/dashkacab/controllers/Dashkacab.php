<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashkacab
 *
 * @author adi
 */
class Dashkacab extends MY_Controller {
	protected $data = '';
	protected $val = '';
	public function __construct()
	{
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			
			'submit' => site_url('dashkacab/submit'),
			'add' => site_url('dashkacab/add'),
			'edit' => site_url('dashkacab/edit'),
			'reload' => site_url('dashkacab'),
		);
		$this->load->model('dashkacab_qry');
	}

	//redirect if needed, otherwise display the user list
	
	public function index(){
		$this->_init_add();
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('index',$this->data);
	}
	
	private function _init_add(){
		$this->data['form'] = array(
		   'periode_awal'=> array(
					'placeholder' => 'Periode',
					'id'          => 'periode_awal',
					'name'        => 'periode_awal',
					'value'       => date('m-Y'),
                    'class'       => 'form-control calendar',
					'required'    => '',
			),
		);
	}
	
	public function get_target_penjualan() {
		echo $this->dashkacab_qry->get_target_penjualan();
	}
	
	public function get_blm_trm_po_leasing() {
		$res = $this->dashadh_qry->get_blm_trm_po_leasing();
		echo json_encode($res);
	}
	
	public function get_blm_trm_po_leasing_detail() {
		$data = $this->dashadh_qry->get_blm_trm_po_leasing_detail();
		$res = array();
		$no = 0;
		foreach ($data as $dt) {
			foreach ($dt as $k => $val) {
				if(is_numeric($val)){
					$res[$no][$k] = (float) $val;
				}else{
					$res[$no][$k] = $val;
				}   
			}
			$no++;
		}
		echo json_encode($res);
	}

}

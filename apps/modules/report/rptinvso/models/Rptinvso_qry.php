<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptinvso_qry
 *
 * @author adi
 */
class Rptinvso_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getData() {
        $nama = $this->input->post('nama');
        $this->db->where('LOWER(nodo)', strtolower($nama));
        $this->db->select("nodo, tgldo, noso, tglso, kdtipex, nmtipe, jnsbayar, tot_disc, dpp_unit, ppn_unit, nama, alamat, kel, kec, kota, nohp, kode, notelp, noktp, npwp, nosin, nora");
        $query = $this->db->get('pzu.vb_invoice_so');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getid() {
      $ayani  = 'ZPH01.01S';
      $pati   = 'ZPH01.02S';
      $kudus  = 'ZPH02.01S';
      $pwd    = 'ZPH02.02S';
      $brebes = 'ZPH03.01S';
      $stbd   = 'ZPH04.01S'; 
      if($this->session->userdata('data')['kddiv']===$stbd){
        $kode = 'S';
      } else if ($this->session->userdata('data')['kddiv']===$brebes) {
        $kode = 'B';
      } else if ($this->session->userdata('data')['kddiv']===$pwd) {
        $kode = 'P';
      } else if ($this->session->userdata('data')['kddiv']===$kudus) {
        $kode = 'K';
      } else if ($this->session->userdata('data')['kddiv']===$pati) {
        $kode = 'P';
      } else if ($this->session->userdata('data')['kddiv']===$ayani) {
        $kode = 'S';
      }

      $query = $this->db->query("select '".$kode."' as kode"); 
      $res = $query->result_array();
      return json_encode($res);
    }

    public function ctk_inv($nodo) { 
        $q = $this->db->query("select * from pzu.vb_invoice_so where nodo = upper('".$nodo."')");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function mlt_ctk_inv($nodo_awal,$nodo_akhir) { 
        $q = $this->db->query("select * from pzu.vb_invoice_so where nodo between upper('".$nodo_awal."') and upper('".$nodo_akhir."')  order by nodo");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    // public function get_jml($nodo_awal,$nodo_akhir) { 
    //     $str = "select sum(debet) as debet, sum(kredit) as kredit, sum(so_awal) as saldo from bkl.vl_kas_harian where kdkb = '".$kdkb."' and tglkb = '".$this->apps->dateConvert($periode_awal)."'";
    //     $q = $this->db->query($str);
    //     // echo $this->db->last_query();
    //     $jml = $q->result_array();
    //     return $jml;
    // }

    public function getNama() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nama)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(noso)', strtolower($q)); 
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                when LOWER(nama) like '".strtolower($q)."%' then 4
                                else 5 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vb_invoice_so');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['nodo'],
                    'text' => $value['nama'],
                    'alamat' => $value['alamat'], 
                    'nodo' => $value['nodo'],
                    'tgldo' => $value['tgldo'],
                    'nmtipe' => $value['nmtipe'],
                    'kel' => $value['kel'], 
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '', 
                'nodo' => 'nodo',
                'tgldo' => '',
                'nmtipe' => '', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }
    
    public function json_dgview() {
        error_reporting(-1); 
        
        if( isset($_GET['nodo_awal']) ){
            if($_GET['nodo_awal']){
                $nodo_awal = $_GET['nodo_awal'];
            }else{
                $nodo_awal = '';
            } 
        }else{
            $nodo_awal = '';
        }
        
        
        if( isset($_GET['nodo_akhir']) ){
            if($_GET['nodo_akhir']){
                $nodo_akhir = $_GET['nodo_akhir'];
            }else{
                $nodo_akhir = '';
            } 
        }else{
            $nodo_akhir = '';
        }
        
        $aColumns = array('nodo',
                        'tgldo',
                        'noso',
                        'nmtipe',
                        'nama',
                        'alamat',
                        'jnsbayar',
                        'harga',
                        'tot_disc',
                        'dpp_unit',
                        'ppn_unit',
                    );
    $sIndexColumn = "nodo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT
                        nodo,
                        tgldo,
                        noso,
                        nmtipe,
                        nama,
                        alamat,
                        jnsbayar::varchar,
                        tot_disc + dpp_unit as harga,
                        tot_disc,
                        dpp_unit,
                        ppn_unit
                      FROM pzu.vb_invoice_so
                      WHERE nodo between '".$nodo_awal."' and '".$nodo_akhir."' ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "ORDER BY nodo ";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = " ";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " AND (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
       // echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        $saldo = 0;
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '';
                    }
                    if(($i>=7 && $i<=10)){
                        $row[] = (float) $aRow[ $aColumns[$i] ];
                        
                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
               $output['aaData'][] = $row;
    }
    echo  json_encode( $output );  
    }
}

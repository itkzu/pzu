<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptbklsaleday
 *
 * @author adi
 */
class Rptbklsaleday extends MY_Controller {

	protected $data = '';
	protected $val = '';

	public function __construct() {
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			'submit' => site_url('rptbklsaleday/submit'),
			'add' => site_url('rptbklsaleday/add'),
			'edit' => site_url('rptbklsaleday/edit'),
			'reload' => site_url('rptbklsaleday'),
		);
		$this->load->model('rptbklsaleday_qry');

		//$this->load->database('bengkel', true);
	}

	public function index() {
		$this->_init_add();
		$this->template
				->title($this->data['msg_main'], $this->apps->name)
				->set_layout('main')
				->build('index', $this->data);
	}

	public function json_dgview() {
		  echo $this->rptbklsaleday_qry->json_dgview();
/*
		if ($this->validate() == TRUE) {
		  echo $this->rptunitentry_qry->json_dgview();
		} else {
			$this->_init_add();
			$this->template
					->title($this->data['msg_main'], $this->apps->name)
					->set_layout('main')
					->build('index', $this->data);
		}
*/
	}

	public function submit() {
		if ($this->validate() == TRUE) {

		} else {
			$this->_init_add();
			$this->template
					->title($this->data['msg_main'], $this->apps->name)
					->set_layout('main')
					->build('index', $this->data);
		}
	}

	private function _init_add() {
		$this->data['form'] = array(
			'periode_awal' => array(
				'placeholder' => 'Periode',
				'id' 		  => 'periode_awal',
				'name' 		  => 'periode_awal',
				'value' 	  => date('m-Y'),
				'class' 	  => 'form-control calendar',
				'style' 	  => 'margin-left: 5px;',
				'required' 	  => '',
			),
		);
	}

	private function validate() {
		$config = array(
			array(
				'field' => 'periode_awal',
				'label' => 'Periode Awal',
				'rules' => 'required',
			),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			return false;
		} else {
			return true;
		}
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstaks
 *
 * @author adi
 */
class Mstaks extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstaks/submit'),
            'add' => site_url('mstaks/add'),
            'edit' => site_url('mstaks/edit'),
            'reload' => site_url('mstaks'),
        );
        $this->load->model('mstaks_qry');
        $kategori = $this->mstaks_qry->getKategori();
        foreach ($kategori as $value) {
            $this->data['kdaksgr'][$value['kdaksgr']] = $value['nmaksgr'];
        }

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstaks_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->mstaks_qry->getKategori();
    }*/

    public function submit() {
        echo $this->mstaks_qry->submit();
    }

    public function update() {
        echo $this->mstaks_qry->update();
    }

    public function delete() {
        echo $this->mstaks_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdaks'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Aksesoris',
                    'id'          => 'kdaks',
                    'name'        => 'kdaks',
                    'value'       => set_value('kdaks'),
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmaks'=> array(
                    'placeholder' => 'Nama Aksesoris',
                    'id'      => 'nmaks',
                    'name'        => 'nmaks',
                    'value'       => set_value('nmaks'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdaksgr'=> array(
                    'attr'        => array(
                        'id'    => 'kdaksgr',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdaksgr'],
                    'value'    => set_value('kdaksgr'),
                    'name'     => 'kdaksgr',
                    'placeholder' => 'Pilih Kategori',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => '',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktif,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdaks'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Aksesoris',
                    'id'          => 'kdaks',
                    'name'        => 'kdaks',
                    'value'       => $this->val[0]['kdaks'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmaks'=> array(
                    'placeholder' => 'Nama Aksesoris',
                    'id'          => 'nmaks',
                    'name'        => 'nmaks',
                    'value'       => $this->val[0]['nmaks'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'kdaksgr'=> array(
                    'attr'        => array(
                        'id'    => 'kdaksgr',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdaksgr'],
                    'value'       => $this->val[0]['kdaksgr'],
                    'name'     => 'kdaksgr',
                    'placeholder' => 'Pilih Kategori',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => 'Status Aksesoris',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktifx,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstaks_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'Kode Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmaks',
                    'label' => 'Nama Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

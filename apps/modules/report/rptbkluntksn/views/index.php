<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>

			<div class="box-body">
 
                <div class="form-group">
                    <?php 
                        echo form_label($form['kddiv']['placeholder']);
                        echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                        echo form_error('kddiv','<div class="note">','</div>');  
                    ?>
                </div>
				<div class="form-group">
					<?php
						echo form_label($form['kdpart']['placeholder']);
						echo form_dropdown( $form['kdpart']['name'],
											$form['kdpart']['data'] ,
											$form['kdpart']['value'] ,
											$form['kdpart']['attr']);
						echo form_error('kdpart','<div class="note">','</div>');
					?>
				</div>

				<div class="form-group">
					<?php
						echo '<div class="radio">';
						echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Periode Bulan <small>per bulan</small>');
						echo '</div>';
						echo form_input($form['periode']);
						echo form_error('akun_multi','<div class="note">','</div>');
					?>
				</div>

				<div class="row">

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Periode Awal');
								echo '</div>';
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Pilih Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							?>
						</div>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				</div>

				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 20%;text-align: center;" rowspan="2">Keterangan</th>
								<th style="text-align: center;" rowspan="2">No. Ref</th>
								<th style="text-align: center;" rowspan="2">Tanggal</th>
								<th style="text-align: center;" colspan="3">Saldo Awal</th>
								<th style="text-align: center;" colspan="3">Masuk</th>
								<th style="text-align: center;" colspan="3">Keluar</th>
								<th style="text-align: center;" colspan="3">Adjustment</th>
								<th style="text-align: center;" colspan="3">Saldo Akhir</th>
							</tr>
							<tr>
								<td style="text-align: center; font-weight:bold">Qty</td>
								<td style="text-align: center; font-weight:bold">Harga</td>
								<td style="text-align: center; font-weight:bold">Total</td>
								<td style="text-align: center; font-weight:bold">Qty</td>
								<td style="text-align: center; font-weight:bold">Harga</td>
								<td style="text-align: center; font-weight:bold">Total</td>
								<td style="text-align: center; font-weight:bold">Qty</td>
								<td style="text-align: center; font-weight:bold">Harga</td>
								<td style="text-align: center; font-weight:bold">Total</td>
								<td style="text-align: center; font-weight:bold">Qty</td>
								<td style="text-align: center; font-weight:bold">Harga</td>
								<td style="text-align: center; font-weight:bold">Total</td>
								<td style="text-align: center; font-weight:bold">Qty</td>
								<td style="text-align: center; font-weight:bold">Total</td>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
						<tr>
							<td style="text-align: center;" colspan="3"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
						</tr>
						</tfoot>
					</table>
				</div>

				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		disable();
		init_nama();
		$("#kdpart").change(function(){
			 getData();
		});
		$("#rdakun_multi").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});
		$("#rdakun_between").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});

		$('#periode').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$(".btn-tampil").click(function(){
			$(".dataTable").show();
			tampilData();
			// alert($('#periode').val());
		});
	});

	function tampilData(){

		var kddiv = $("#kddiv").val();
		var kdpart = $("#kdpart").val();
		var periode = $("#periode").val();
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val();
		var periode_d = $('#rdakun_multi').is(':checked');
		if(periode_d){
			var ket = "periode_multi";
		} else {
			var ket = "periode_between";
		}

		var column = [];

		column.push({
			"aTargets": [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ],
			"orderable": false
		});

		column.push({
			"aTargets": [ 2 ],
			"mRender": function (data, type, full) {
				return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});

		column.push({
			"aTargets": [ 4,5,7,8,10,11,13,14,16 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
			},
			"sClass": "right"
		});

		column.push({
			"aTargets": [ 3,6,9,12,15 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "ket" },
				{ "data": "noref" },
				{ "data": "tgl" },
				{ "data": "saw_qty" },
				{ "data": "saw_harga" },
				{ "data": "saw_total" },
				{ "data": "in_qty" },
				{ "data": "in_harga" },
				{ "data": "in_total" },
				{ "data": "out_qty" },
				{ "data": "out_harga" },
				{ "data": "out_total" },
				{ "data": "adj_qty" },
				{ "data": "adj_harga" },
				{ "data": "adj_total" },
				{ "data": "sak_qty" },
				{ "data": "sak_total" }
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"order": false,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
							{ "name": "kddiv", "value": kddiv },
							{ "name": "kdpart", "value": kdpart },
							{ "name": "ket", "value": ket },
							{ "name": "periode", "value": periode },
							{ "name": "periode_awal", "value": periode_awal },
							{ "name": "periode_akhir", "value": periode_akhir }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptbkluntksn/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			// jumlah TOTAL
			'footerCallback': function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// computing column Total of the complete result
				var saw_qty = api
					.column( 3 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var saw_tot = api
					.column( 5 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var in_qty = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var in_tot = api
					.column( 8 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var out_qty = api
					.column( 9 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var out_tot = api
					.column( 11 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var adj_qty = api
					.column( 12 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var adj_tot = api
					.column( 14 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var sak_qty =  parseInt(saw_qty+in_qty-out_qty+adj_qty);
				var sak_tot =  parseInt(saw_tot+in_tot-out_tot+adj_tot);

				// Update footer by showing the total with the reference of the column index
				$( api.column( 1 ).footer() ).html('<b>Total</b>');
				$( api.column( 3 ).footer() ).html('<b>' + numeral(saw_qty).format('0,0') + '</b>');
				$( api.column( 5 ).footer() ).html('<b>' + numeral(saw_tot).format('0,0.00') + '</b>');
				$( api.column( 6 ).footer() ).html('<b>' + numeral(in_qty).format('0,0') + '</b>');
				$( api.column( 8 ).footer() ).html('<b>' + numeral(in_tot).format('0,0.00') + '</b>');
				$( api.column( 9 ).footer() ).html('<b>' + numeral(out_qty).format('0,0') + '</b>');
				$( api.column( 11 ).footer() ).html('<b>' + numeral(out_tot).format('0,0.00') + '</b>');
				$( api.column( 12 ).footer() ).html('<b>' + numeral(adj_qty).format('0,0') + '</b>');
				$( api.column( 14 ).footer() ).html('<b>' + numeral(adj_tot).format('0,0.00') + '</b>');
				$( api.column( 15 ).footer() ).html('<b>' + numeral(sak_qty).format('0,0') + '</b>');
				$( api.column( 16 ).footer() ).html('<b>' + numeral(sak_tot).format('0,0.00') + '</b>');
			},

			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print', footer: true,
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {

		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
	}

	function disable(){
		$(".dataTable").hide();
		$("#periode").prop("disabled", true);
		$(".btn-tampil").prop("disabled", true);
		$("#periode_awal").prop("disabled", true);
		$("#periode_akhir").prop("disabled", true);
	}

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#periode").prop("disabled", false);
			$("#periode_awal").prop("disabled", true);
			$("#periode_akhir").prop("disabled", true);
		}else{
			$("#periode").prop("disabled", true);
			$("#periode_awal").prop("disabled", false);
			$("#periode_akhir").prop("disabled", false);

		}
	}

	function getData(){
		var kdpart = $("#kdpart").val();
		$.ajax({
			type: "POST",
			url: "<?=site_url("rptbkluntksn/getData");?>",
			data: {"kdpart":kdpart},
			beforeSend: function() {

			},
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$.each(obj, function(key, value){});
			},
			error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		});
	}

	function setBgColour(val,object){
		if(val){
			$("#"+object).css("background-color", "#fff");
		}
		else{
			$("#"+object).css("background-color", "#eee");
		}
	}

	function init_nama(){
		$("#kdpart").select2({
			ajax: {
				url: "<?=site_url('rptbkluntksn/getNama');?>",
				type: 'post',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
						page: params.page
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 30) < data.total_count
						}
					};
				},
				cache: true
			},
			placeholder: 'Masukkan Nama Aksesoris ...',
			dropdownAutoWidth : false,
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 2,
			templateResult: format_nama, // omitted for brevity, see the source of this page
			templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
		});
	}

	function format_nama_terpilih (repo) {
		return repo.full_name || repo.text;
	}

	function format_nama (repo) {
		if (repo.loading) return "Mencari data ... ";

		var markup = "<div class='select2-result-repository clearfix'>" +
		"<div class='select2-result-repository__meta'>" +
		"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>" +
		"</div>" +
		"</div>";
		return markup;
	}

</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstaddunit
 *
 * @author adi
 */
class Mstaddunit extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstaddunit/submit'),
            'add' => site_url('mstaddunit/add'),
            'edit' => site_url('mstaddunit/edit'),
            'reload' => site_url('mstaddunit'),
        );
        $this->load->model('mstaddunit_qry');
        $kategori = $this->mstaddunit_qry->getKdSeries();
        foreach ($kategori as $value) {
            $this->data['kdtipesr'][$value['kdtipesr']] = $value['nmtipesr'];
        }

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstaddunit_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->mstaddunit_qry->getKategori();
    }*/

    public function submit() {
        echo $this->mstaddunit_qry->submit();
    }

    public function update() {
        echo $this->mstaddunit_qry->update();
    }

    public function gettipegrp() {
        echo $this->mstaddunit_qry->gettipegrp();
    }

    public function delete() {
        echo $this->mstaddunit_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kode'=> array( 
                    'placeholder' => 'Kode',
                    'id'          => 'kode',
                    'name'        => 'kode',
                    'value'       => set_value('kode'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'kdtipe'=> array(
                    'placeholder' => 'Tipe',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'nmtipe'=> array(
                    'placeholder' => 'Nama',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdtipesr'=> array(
                    'attr'        => array(
                        'id'    => 'kdtipesr',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdtipesr'],
                    'value'    => set_value('kdtipesr'),
                    'name'     => 'kdtipesr',
                    'placeholder' => 'Series',
                    'required' => ''
            ),
           'nmtipegrp'=> array(
                    'placeholder' => 'Grup',
                    'id'          => 'nmtipegrp',
                    'name'        => 'nmtipegrp',
                    'value'       => set_value('nmtipegrp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'beli'=> array(
                    'placeholder' => 'Harga Beli',
                    'id'          => 'beli',
                    'name'        => 'beli',
                    'value'       => set_value('beli'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
           'jual'=> array(
                    'placeholder' => 'Harga Jual',
                    'id'          => 'jual',
                    'name'        => 'jual',
                    'value'       => set_value('jual'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
           'jual_off'=> array(
                    'placeholder' => 'Harga Jual OFF TR',
                    'id'          => 'jual_off',
                    'name'        => 'jual_off',
                    'value'       => set_value('jual_off'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
           'bbn'=> array(
                    'placeholder' => 'Cadangan BBN',
                    'id'          => 'bbn',
                    'name'        => 'bbn',
                    'value'       => set_value('bbn'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    'style'       => '',
            ),  
           'kode2'=> array( 
                    'id'          => 'kode2',
                    'name'        => 'kode2',
                    'value'       => set_value('kode2'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    'type'       => 'hidden',
            ), 
            'faktif'=> array(
                    'placeholder' => '',
                    'id'          => 'faktif',
                    'value'       => 't',
                    'checked'     => $faktif,
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif',
                    'type'        => 'checkbox',
            ),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks); 

        if($this->val[0]['faktif'] == 't'){
                    $faktifx = true;
                } else {
                    $faktifx = false;
        }
        $this->data['form'] = array(
           'kode'=> array( 
                    'placeholder' => 'Kode',
                    'id'          => 'kode',
                    'name'        => 'kode',
                    'value'       => $this->val[0]['kode'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'kdtipe'=> array(
                    'placeholder' => 'Tipe',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => $this->val[0]['kdtipe'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'nmtipe'=> array(
                    'placeholder' => 'Nama',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => $this->val[0]['nmtipe'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdtipesr'=> array(
                    'attr'        => array(
                        'id'    => 'kdtipesr',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdtipesr'],
                    'value'       => $this->val[0]['kdtipesr'],
                    'name'     => 'kdtipesr',
                    'placeholder' => 'Series',
                    'required' => ''
            ),
           'nmtipegrp'=> array(
                    'placeholder' => 'Grup',
                    'id'          => 'nmtipegrp',
                    'name'        => 'nmtipegrp',
                    'value'       => set_value('nmtipegrp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'beli'=> array(
                    'placeholder' => 'Harga Beli',
                    'id'          => 'beli',
                    'name'        => 'beli',
                    'value'       => $this->val[0]['beli'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
           'jual'=> array(
                    'placeholder' => 'Harga Jual',
                    'id'          => 'jual',
                    'name'        => 'jual',
                    'value'       => $this->val[0]['jual'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
           'jual_off'=> array(
                    'placeholder' => 'Harga Jual OFF TR',
                    'id'          => 'jual_off',
                    'name'        => 'jual_off',
                    'value'       => $this->val[0]['jual_off'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
           'bbn'=> array(
                    'placeholder' => 'Cadangan BBN',
                    'id'          => 'bbn',
                    'name'        => 'bbn',
                    'value'       => $this->val[0]['bbn'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => '',
                    'readonly'    => '',
            ),   
           'kode2'=> array( 
                    'id'          => 'kode2',
                    'name'        => 'kode2',
                    'value'       => $this->val[0]['kode'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    'type'       => 'hidden',
            ), 
            'faktif'=> array(
                    'placeholder' => '',
                    'id'          => 'faktif',
                    'value'       => 't',
                    'checked'     => $faktifx,
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif',
                    'type'        => 'checkbox',
            ),
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstaddunit_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'Kode Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmaks',
                    'label' => 'Nama Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

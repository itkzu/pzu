<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_akhir']['placeholder']);
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                <table style="width: 1200px;"  class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;">NAMA PEMBELI</th>
                            <th style="text-align: center;">NAMA STNK</th>
                            <th style="text-align: center;">NO. SPK</th>
                            <th style="text-align: center;">TGL SPK</th>
                            <th style="text-align: center;">LEASING</th>
                            <th style="text-align: center;">SALESMAN</th>
                            <th style="text-align: center;">SALDO TITIPAN</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">NAMA PEMBELI</th>
                            <th style="text-align: center;">NAMA STNK</th>
                            <th style="text-align: center;">NO. SPK</th>
                            <th style="text-align: center;">TGL SPK</th>
                            <th style="text-align: center;">LEASING</th>
                            <th style="text-align: center;">SALESMAN</th>
                            <th style="text-align: center;">SALDO TITIPAN</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        
        var column = [];
        for (i = 6; i <= 6; i++) { 
            column.push({ 
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    // return formmatedvalue;
                    } 
                });
        }
        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( 
                            { "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(0)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptltpkonsumen/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A0'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>
<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstdatacust_qry
 *
 * @author adi
 */
class Mstdatacust_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getData() {
        $nama = $this->input->post('nama');
        $this->db->where('LOWER(noso)', strtolower($nama));
        $this->db->select("nodo, nama, alamat,alamat2, kel, kec, kota, nohp, kode, kdtipe, 
                            nmtipe, nosin, nora, nopo, tglpo, nmsup, status, jnsbayar, jnsbayarx, 
                            status_otr, status_otrx, nmsales, tgl_aju_fa, tgl_buat_fa, tgl_trm_fa, 
                            harga_fa, tgl_kps, tgl_aju_bbn, tgl_trm_bbn, nopol, nobpkb
                            , to_char(tgldo,'DD FMMonth YYYY') tgldo
                            , to_char(tgl_trm_notis,'DD FMMonth YYYY') tgl_trm_notis
                            , to_char(tgl_trm_stnk,'DD FMMonth YYYY') tgl_trm_stnk
                            , to_char(tgl_trm_tnkb,'DD FMMonth YYYY') tgl_trm_tnkb
                            , to_char(tgl_trm_bpkb,'DD FMMonth YYYY') tgl_trm_bpkb
                            , to_char(tgl_srh_notis,'DD FMMonth YYYY') tgl_srh_notis
                            , to_char(tgl_srh_stnk,'DD FMMonth YYYY') tgl_srh_stnk
                            , to_char(tgl_srh_tnkb,'DD FMMonth YYYY') tgl_srh_tnkb
                            , to_char(tgl_srh_bpkb,'DD FMMonth YYYY') tgl_srh_bpkb
                            , nosin2
                            , nora2");
        $query = $this->db->get('pzu.vl_bbn_status');        
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
    
    public function getNama() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nama)', strtolower($q));
        $this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(noso)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vl_bbn_status');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array(); 
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'noso' => $value['noso'],
                    'nodo' => $value['nodo'],
                    'tgldo' => $value['tgldo'],
                    'nama' => $value['nama'],
                    'alamat' => $value['alamat2'],
                    'nohp' => $value['nohp'],
                    'nmtipe' => $value['nmtipe'],
                    'jnsbayarx' => $value['jnsbayarx'],
                );
                
            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );            
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'noso' => '',
                'nodo' => '',
                'tgldo' => '',
                'nama' => '',
                'alamat' => '',
                'nohp' => '',
                'nmtipe' => '',
                'jnsbayarx' => '',
            );
                
            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );            
            return json_encode($data);
        }
    }
    
    public function updateData() {
        try {
            $array = $this->input->post();  
            if(!empty($array['noso'])){
                $this->db->where('noso', $array['noso']);
                $this->db->set('nohp_s',$array['nohp_s']);
                $resl = $this->db->update('t_so');
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        return json_encode($arr);
    }
}

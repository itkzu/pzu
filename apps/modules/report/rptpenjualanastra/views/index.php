<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes);
                ?>
                <div class="form-group">
                    <?php
                        echo form_label($form['periode_awal']['placeholder']);
                        echo form_input($form['periode_awal']);
                        echo form_error('periode_awal','<div class="note">','</div>');
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        echo form_label($form['periode_akhir']['placeholder']);
                        echo form_input($form['periode_akhir']);
                        echo form_error('periode_akhir','<div class="note">','</div>');
                    ?>
                </div>
                <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>

                <?php $clr_1 = '#f7e594'; $clr_2 = '#bddaef'; ?>
                <div class="table-responsive">
                    <table style="width: 10000px;" class="table table-bordered table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th style="text-align: center;" colspan="57">&nbsp;</th>
                                <th style="text-align: center;background-color:<?php echo $clr_2 ?>;" colspan="4">REFUND LEASING</th>
                            </tr>
                            <tr>
                                <th style="width: 80px;text-align: center;">No. DO</th>
                                <th style="width: 60px;text-align: center;">Tanggal</th>
                                <th style="width: 80px;text-align: center;">No. SPK</th>
                                <th style="text-align: center;">Nama Konsumen</th>
                                <th style="width: 5px;text-align: center;">T/K</th>
                                <th style="width: 5px;text-align: center;">Leasing</th>
                                <th style="text-align: center;">Program</th>
                                <th style="width: 5px;text-align: center;">Tenor</th>
                                <th style="width: 80px;text-align: center;">Angsuran</th>
                                <th style="width: 80px;text-align: center;">Status</th>
                                <th style="text-align: center;">Nama STNK</th>
                                <th style="text-align: center;">Alamat KTP</th>
                                <th style="text-align: center;">Kelurahan</th>
                                <th style="text-align: center;">Kecamatan</th>
                                <th style="text-align: center;">Kota</th>
                                <th style="width: 10px;text-align: center;">No. Telp</th>
                                <th style="width: 10px;text-align: center;">Kode</th>
                                <th style="text-align: center;">Tipe</th>
                                <th style="text-align: center;">Jenis</th>
                                <th style="text-align: center;">Warna</th>
                                <th style="width: 80px;text-align: center;">No. Mesin</th>
                                <th style="width: 80px;text-align: center;">No. Rangka</th>
                                <th style="width: 80px;text-align: center;">Harga Bruto</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>;">Disc. Unit</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>;">Sub. Dealer</th>
                                <th style="width: 80px;width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>;">Sub. HSO</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>;">Sub. AHM</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">Sub. Leasing</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">Tot. Diskon</th>
                                <th style="width: 80px;text-align: center;">Beban Dealer</th>
                                <th style="width: 80px;text-align: center;">Harga  ONTR</th>
                                <th style="width: 80px;text-align: center;">Cad. BBN</th>
                                <th style="width: 80px;text-align: center;">Harga OFFTR</th>
                                <th style="width: 80px;text-align: center;">DPP</th>
                                <th style="width: 80px;text-align: center;">PPN</th>
                                <th style="width: 80px;text-align: center;">UM</th>
                                <th style="width: 80px;text-align: center;">UM Riel</th>
                                <th style="width: 80px;text-align: center;">AR Konsumen</th>
                                <th style="width: 80px;text-align: center;">AR Leasing</th>
                                <th style="width: 80px;text-align: center;">Sub. HSO</th>
                                <th style="width: 80px;text-align: center;">Ins. Leasing</th>
                                <th style="width: 80px;text-align: center;">Harga Beli</th>
                                <th style="width: 80px;text-align: center;">Komisi</th>
                                <th style="width: 80px;text-align: center;">Jaket</th>
                                <th style="width: 80px;text-align: center;">Lain-lain</th>
                                <th style="width: 80px;text-align: center;">Ins. Sales</th>
                                <th style="width: 80px;text-align: center;">By. Langsung</th>
                                <th style="width: 80px;text-align: center;">JP (DPP)</th>
                                <th style="width: 80px;text-align: center;">Gross Profit</th>
                                <th style="width: 80px;text-align: center;">Biops</th>
                                <th style="width: 80px;text-align: center;">Matriks (DPP)</th>
                                <th style="width: 80px;text-align: center;">Laba/Rugi</th>
                                <th style="text-align: center;">Sales</th>
                                <th style="text-align: center;">Supervisor</th>
                                <th style="text-align: center;">Showroom/Pos</th>
                                <th style="text-align: center;">Keterangan</th>
                                <th style="text-align: center;">Keterangan 2</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">DPP Total</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">PPN Total</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">PPH Total</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">AR Total</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 80px;text-align: center;">NO. DO</th>
                                <th style="width: 80px;text-align: center;">TANGGAL</th>
                                <th style="width: 80px;text-align: center;">NO. SPK</th>
                                <th style="text-align: center;">NAMA KONSUMEN</th>
                                <th style="width: 5px;text-align: center;">T/K</th>
                                <th style="width: 5px;text-align: center;">LEASING</th>
                                <th style="text-align: center;">PROGRAM</th>
                                <th style="width: 5px;text-align: center;">TENOR</th>
                                <th style="width: 80px;text-align: center;">ANGSURAN</th>
                                <th style="width: 80px;text-align: center;">STATUS</th>
                                <th style="text-align: center;">NAMA STNK</th>
                                <th style="text-align: center;">ALAMAT KTP</th>
                                <th style="text-align: center;">KELURAHAN</th>
                                <th style="width: 80px;text-align: center;">KECAMATAN</th>
                                <th style="text-align: center;">KOTA</th>
                                <th style="width: 10px;text-align: center;">NO. TELP</th>
                                <th style="width: 10px;text-align: center;">KODE</th>
                                <th style="text-align: center;">TIPE</th>
                                <th style="text-align: center;">JENIS</th>
                                <th style="text-align: center;">WARNA</th>
                                <th style="width: 80px;text-align: center;">NO. MESIN</th>
                                <th style="width: 80px;text-align: center;">NO. RANGKA</th>
                                <th style="width: 80px;text-align: center;">HARGA BRUTO</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">DISC. UNIT</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">SUB. DEALER</th>
                                <th style="width: 80px;width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">SUB. HSO</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">SUB. AHM</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">SUB. LEASING</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_1 ?>">TOT. DISKON</th>
                                <th style="width: 80px;text-align: center;">BEBAN DEALER</th>
                                <th style="width: 80px;text-align: center;">HARGA  ONTR</th>
                                <th style="width: 80px;text-align: center;">CAD. BBN</th>
                                <th style="width: 80px;text-align: center;">HARGA OFFTR</th>
                                <th style="width: 80px;text-align: center;">DPP</th>
                                <th style="width: 80px;text-align: center;">PPN</th>
                                <th style="width: 80px;text-align: center;">UM</th>
                                <th style="width: 80px;text-align: center;">UM RIEL</th>
                                <th style="width: 80px;text-align: center;">AR KONSUMEN</th>
                                <th style="width: 80px;text-align: center;">AR LEASING</th>
                                <th style="width: 80px;text-align: center;">SUB. HSO</th>
                                <th style="width: 80px;text-align: center;">INS. LEASING</th>
                                <th style="width: 80px;text-align: center;">HARGA BELI</th>
                                <th style="width: 80px;text-align: center;">KOMISI</th>
                                <th style="width: 80px;text-align: center;">JAKET</th>
                                <th style="width: 80px;text-align: center;">LAIN-LAIN</th>
                                <th style="width: 80px;text-align: center;">INS. SALES</th>
                                <th style="width: 80px;text-align: center;">BY. LANGSUNG</th>
                                <th style="width: 80px;text-align: center;">JP (DPP)</th>
                                <th style="width: 80px;text-align: center;">GROSS PROFIT</th>
                                <th style="width: 80px;text-align: center;">BIOPS</th>
                                <th style="width: 80px;text-align: center;">MATRIKS (DPP)</th>
                                <th style="width: 80px;text-align: center;">LABA/RUGI</th>
                                <th style="text-align: center;">SALES</th>
                                <th style="text-align: center;">SUPERVISOR</th>
                                <th style="text-align: center;">SHOWROOM/POS</th>
                                <th style="text-align: center;">KETERANGAN</th>
                                <th style="text-align: center;">KETERANGAN 2</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">DPP TOTAL</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">PPN TOTAL</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">PPH TOTAL</th>
                                <th style="width: 80px;text-align: center;background-color: <?php echo $clr_2 ?>;">AR TOTAL</th>
                            </tr>
                        </tfoot>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });

        var column = [];
        column.push({
            "aTargets": [  ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
                // return type === 'export' ? data : numeral(data).format('0,0.00');
                // return formmatedvalue;
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 8 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
                // return type === 'export' ? data : numeral(data).format('0,0.00');
                // return formmatedvalue;
            },
            "sClass": "right"
        });

        for (i = 22; i <= 51; i++) {
            column.push({
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                            // return type === 'export' ? data : numeral(data).format('0,0.00');
                            // return formmatedvalue;
                    },
                    "sClass": "right"
                });
        }
        for (i = 57; i <= 60; i++) {
            column.push({
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                            // return type === 'export' ? data : numeral(data).format('0,0.00');
                            // return formmatedvalue;
                    },
                    "sClass": "right"
                });
        }
        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
                            ,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    var clr_1 = '#f7e594';
                    var clr_2 = '#bddaef';

                    $(row).find('td:eq(23)').css('background-color', clr_1);
                    $(row).find('td:eq(24)').css('background-color', clr_1);
                    $(row).find('td:eq(25)').css('background-color', clr_1);
                    $(row).find('td:eq(26)').css('background-color', clr_1);
                    $(row).find('td:eq(27)').css('background-color', clr_1);
                    $(row).find('td:eq(28)').css('background-color', clr_1);

                    $(row).find('td:eq(57)').css('background-color', clr_2);
                    $(row).find('td:eq(58)').css('background-color', clr_2);
                    $(row).find('td:eq(59)').css('background-color', clr_2);
                    $(row).find('td:eq(60)').css('background-color', clr_2);
                //}
            },
            "sAjaxSource": "<?=site_url('rptpenjualanastra/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A0',
                    exportOptions: {orthogonal: 'export'}
                },
                {
                    extend: 'excel',
                    exportOptions: {orthogonal: 'export'}
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>

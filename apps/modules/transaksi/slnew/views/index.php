<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    .pass-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    } 
    #load{
        width: 80%;
        height: 80%;
        position: fixed;
        text-indent: 100%;
        background: #ebebeb url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <div class="col-md-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">

          <div class="col-md-6">
              <div class="box-header box-view">
                  <button type="button" class="btn btn-primary btn-add">Tambah</button>
                  <button type="button" class="btn btn-warning btn-edit">Ubah</button>
                  <button type="button" class="btn btn-danger btn-del">Hapus</button>
              <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
              </div>
              <div class="box-header box-new">
                  <button type="button" class="btn btn-primary btn-save">Simpan</button>
                  <button type="button" class="btn btn-danger btn-cancel">Batal</button>
              </div>
          </div> 

          <div class="col-md-3"> 
              <div class="box-header">
                  <button type="button" class="btn btn-primary btn-imp">Import Assist</button> 
              </div>
          </div>
          
          <div class="col-md-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-md-2">
                          <label>
                            <?php echo form_label($form['kdsup']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                      echo form_dropdown($form['kdsup']['name'],$form['kdsup']['data'] ,$form['kdsup']['value'] ,$form['kdsup']['attr']);
                                      echo form_error('kdsup','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                          <label>
                            <?php echo form_label($form['nopo']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nopo']);
                                    echo form_error('nopo','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['statpo']);
                                    echo form_error('statpo','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-md-2">
                          <label>
                            <?php echo form_label($form['noslhso']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                      echo form_dropdown($form['noslhso']['name'],$form['noslhso']['data'] ,$form['noslhso']['value'] ,$form['noslhso']['attr']);
                                      echo form_error('noslhso','<div class="note">','</div>');
                                    // echo form_label($form['nospk']['placeholder']);
                                    // echo form_input($form['nospk']);
                                    // echo form_error('nospk','<div class="note">','</div>');
                                ?>
                             </div>
                        </div>

                        <div class="col-md-2">
                          <label>
                            <?php echo form_label($form['tglpo']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['tglpo']);
                                    echo form_error('tglpo','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['kdlokasi']);
                                    echo form_error('kdlokasi','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12 nosl2">
                      <div class="row">

                        <div class="col-md-2"> 
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                      echo form_dropdown($form['noslhso2']['name'],$form['noslhso2']['data'] ,$form['noslhso2']['value'] ,$form['noslhso2']['attr']);
                                      echo form_error('noslhso2','<div class="note">','</div>');
                                    // echo form_label($form['nospk']['placeholder']);
                                    // echo form_input($form['nospk']);
                                    // echo form_error('nospk','<div class="note">','</div>');
                                ?>
                             </div>
                        </div> 

                        <div class="col-md-1 btn-del1">
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-del1"> - </button>
                             </div>
                        </div>  
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12 nosl3">
                      <div class="row">

                        <div class="col-md-2"> 
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                      echo form_dropdown($form['noslhso3']['name'],$form['noslhso3']['data'] ,$form['noslhso3']['value'] ,$form['noslhso3']['attr']);
                                      echo form_error('noslhso3','<div class="note">','</div>');
                                    // echo form_label($form['nospk']['placeholder']);
                                    // echo form_input($form['nospk']);
                                    // echo form_error('nospk','<div class="note">','</div>');
                                ?>
                             </div>
                        </div> 

                        <div class="col-md-2 btn-del2">
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-del2"> - </button>
                             </div>
                        </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-md-2">
                          <label>
                            <?php echo form_label($form['tipebayar']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php
                                      echo form_dropdown($form['tipebayar']['name'],$form['tipebayar']['data'] ,$form['tipebayar']['value'] ,$form['tipebayar']['attr']);
                                      echo form_error('tipebayar','<div class="note">','</div>');
                                    // echo form_label($form['nospk']['placeholder']);
                                    // echo form_input($form['nospk']);
                                    // echo form_error('nospk','<div class="note">','</div>');
                                ?>
                             </div>
                        </div>

                          <div class="col-md-2">
                            <label>
                              <?php echo form_label($form['tgltrm']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-md-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['tgltrm']);
                                      echo form_error('tgltrm','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                      <div class="kredit">
                          <div class="col-md-2">
                            <label>
                              <?php echo form_label($form['jth_tmp']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-md-1">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['jth_tmp']);
                                      echo form_error('jth_tmp','<div class="note">','</div>');

                                  ?>
                              </div>
                          </div>

                          <div class="col-md-1">
                            <label>
                              <?php echo " Hari"; ?>
                            </label>
                          </div>

                          <div class="col-md-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['tgltmp']);
                                      echo form_error('tgltmp','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                      <div class="tunai">
                          <div class="col-md-5">
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-md-12">
                          <label> Detail Shipping List </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-md-3 detail_add">
                        <button type="button" class="btn btn-success btn-addDetail">Tambah</button>
                        <button type="button" class="btn btn-primary btn-updDetail">Ubah</button>
                        <button type="button" class="btn btn-danger btn-delDetail">Hapus</button>
                      </div>

                      <div class="col-md-1">
                          <div class="form-group">
                              <?php
                                  echo form_input($form['ket']);
                                  echo form_error('ket','<div class="note">','</div>');
                              ?>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-hover table-bordered dataTable">
                              <thead>
                                  <tr>
                                      <th style="width: 1%;text-align: center;">No</th>
                                      <th style="text-align: center;">Kode</th>
                                      <th style="text-align: center;">Tipe SMH</th>
                                      <th style="text-align: center;">Nama SMH</th>
                                      <th style="text-align: center;">Warna</th>
                                      <th style="text-align: center;">No. Mesin</th>
                                      <th style="text-align: center;">No. Rangka</th>
                                      <th style="text-align: center;">Tahun</th>
                                      <th style="text-align: center;">Status DO</th>
                                  </tr>
                              </thead>
                              <tbody></tbody>
                              <tfoot>
                                  <tr>
                                      <th></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                      <th style="text-align: center;"></th>
                                  </tr>
                              </tfoot>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>
<div id="modal_detail" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Transaksi</h4>
                </div>

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kdaks']['placeholder']);
                                    echo form_dropdown( $form['kdaks']['name'],
                                                        $form['kdaks']['data'] ,
                                                        $form['kdaks']['value'] ,
                                                        $form['kdaks']['attr']);
                                    echo form_error('kdaks','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['qty']['placeholder']);
                                    echo form_input($form['qty']);
                                    echo form_error('qty','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo "<b>Harga @</b>";
                                    echo form_input($form['harga']);
                                    echo form_error('harga','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
        $("#nopo").mask("999-9999-999");
        // $("#noso").mask("s99-999999");
        // $("#noktp").mask("99.99.99.999999.9999");
        $(".detail_add").hide();
        $("#ket").val('0');
        $("#nopo").val('');
        $("#jth_tmp").val('15');
        $('#tgltmp').val(dateback($("#tglpo").val()));
        $("#statpo").hide();
        $(".kredit").hide();
        $(".tunai").show();
        $('.nosl2').hide();
        $('.nosl3').hide();
        // $("#npwp").mask("s99-999999");
        clear();
        nonaktif();
        kdlokasi();
        disabled();
        getKdSup();
        // getTipeUnit();
        // getKdWarna();
        getSLHSO();

        $('#tipebayar').change(function () {
            if ($('#tipebayar').val()==='T'){
              $(".kredit").hide();
              $(".tunai").show();
            } else {
                $(".kredit").show();
                $(".tunai").hide();
            }
        });

        $('#noslhso').change(function () {
            if($('#noslhso').val()===''){
              $('.nosl2').hide();
              $('.nosl3').hide();
            } else { 
              getSLHSO2();
              $('.nosl2').show();
              $('.btn-del1').hide(); 
              $('.nosl3').hide();
            }
        });

        $('#noslhso2').change(function () {
            if($('#noslhso2').val()===''){ 
              $('.nosl3').hide();
            } else {  
              getSLHSO3();
              $('.nosl3').show();
              $('.btn-del1').show();
              $('.btn-del2').hide();
            }
        });

        $('#noslhso3').change(function () {
            if($('#noslhso3').val()===''){ 
              $('.btn-del1').show();
              $('.btn-del2').hide();
            } else {  
              $('.btn-del1').show();
              $('.btn-del2').show();
            }
        });

        $('.btn-del1').click(function () {
            $('#noslhso2').val(''); 
            $("#noslhso2").trigger("chosen:updated");
            $('#noslhso2').select2({
                          dropdownAutoWidth : true,
                          width: '100%'
                        });
            $('.btn-del1').hide();
            getdetHSO();
            $('.nosl2').show();
            $('.nosl3').hide();
        });

        $('.btn-del2').click(function () {
            $('#noslhso3').val('');
            $("#noslhso3").trigger("chosen:updated");
            $('#noslhso3').select2({
                          dropdownAutoWidth : true,
                          width: '100%'
                        });
            getdetHSO2();
            $('.btn-del2').hide();
            $('.nosl3').show();
        });

        $('#tglpo').change(function () {
            $('#tgltmp').val(dateback($("#tglpo").val()));
        });

        $('#jth_tmp').keyup(function () {
            $('#tgltmp').val(dateback($("#tglpo").val()));
        });

        $('.btn-batal').click(function () {
            clear();
            disabled();
        });

        $('.btn-add').click(function () {
            clear();
          $("#kdsup").val('');
          $("#kdsup").trigger("chosen:updated");
          $('#kdsup').select2({
                        dropdownAutoWidth : true,
                        width: '100%'
                      });
          $("#nopo").val('');
          $("#statpo").hide();
          getdetPO();
          $("#ket").val('1');
            enabled();
        });

        $('.btn-edit').click(function () {
          $("#ket").val('2');
            enabled();
            $("#tglso").attr('disabled',true);
            $("#noso").attr('disabled',true);
            $("#kdtipe").attr('disabled',true);
            $("#kdwarna").attr('disabled',true);

            if($('#noslhso').val()===''){
              $('.nosl2').hide();
              $('.nosl3').hide();
            } else { 
              getSLHSO2();
              $('.nosl2').show();
              $('.btn-del1').hide(); 
              $('.nosl3').hide();
            }
        });

        $('#nopo').change(function () {
          if($('#ket').val()==='0'){
            getSLHSO();
            set_faktur();
            // alert($('#ket').val());
          } else {
            // alert($('#ket').val());
            caripo();
          }
            // set_faktur();
        });

        $('#noslhso').change(function () {
          getdetHSO();
        });

        $('#noslhso2').change(function () {
          getdetHSO2();
        });

        $('#noslhso3').change(function () {
          getdetHSO3();
        });

        $('.btn-updDetail').click(function () {

              //alert( table.rows('.selected').data().length +' row(s) selected' );
              if (table.rows('.selected').data().length > 0) {
                	//$('#modal_detail').modal('toggle');
                  var row_nosin = table.column(0).checkboxes.selected();
                  var row_kode = table.columns(1).data();
                  var row_kdtipe = table.column(2).checkboxes.selected();
                  var row_nmtipe = table.column(3).checkboxes.selected();
                  var row_kdwarna = table.column(4).checkboxes.selected();
                  // var row_nosin2 = table.column(5).checkboxes.selected();
                  var row_nora = table.column(6).checkboxes.selected();
                  var row_thn = table.column(7).checkboxes.selected();

                  var nosin = row_nosin.join(",");
                  var kode = row_kode.join(",");
                  var kdtipe = row_kdtipe.join(",");
                  var nmtipe = row_nmtipe.join(",");
                  var kdwarna = row_kdwarna.join(",");
                  var nora = row_nora.join(",");
                  var thn = row_thn.join(",");

                  alert(kode + kdtipe + nmtipe + kdwarna + nora + thn)
              } else {
                  swal({
                      title: 'Gagal',
                      text: 'Pilih data terlebih dahulu !',
                      type: 'error'
                  });
              }
        });

        $('.btn-delDetail').click(function () {

              //alert( table.rows('.selected').data().length +' row(s) selected' );
              if (table.rows('.selected').data().length > 0) {

                  var rows_selected = table.column(0).checkboxes.selected();


                  /*
                  $.each(rows_selected, function(index, rowId){
                      alert(rowId);
                  })
                  */

                  //alert(rows_selected.join(","));

                  var nopo = '';
                  var nosin = rows_selected.join(",");
                  // alert(nosin);
                  deleted(nosin);

              } else {
                  swal({
                      title: 'Gagal',
                      text: 'Pilih data terlebih dahulu !',
                      type: 'error'
                  });
              }
        });

        $('.btn-save').click(function () {
          if($('#ket').val()==='1'){
            if($('#noslhso2').val() === ''){
              addpo();
            } else {
              if($('#noslhso3').val() === ''){
                addpo2();
              } else {
                addpo3();
              }
            }
          } else {
            if($('#noslhso2').val() === ''){
              updatepo();
            } else {
              if($('#noslhso3').val() === ''){
                updatepo2();
              } else {
                updatepo3();
              }
            }
            
          };
        });

        $('.btn-del').click(function () {
          deletepo();
        });

        $('.btn-cancel').click(function () {
          swal({
              title: "Input Transaki Faktur akan dibatalkan!",
              text: "Data tidak akan Tersimpan!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#c9302c",
              confirmButtonText: "Ya, Lanjutkan!",
              cancelButtonText: "Batalkan!",
              closeOnConfirm: false
          }, function () {
            window.location.reload();
          });
        });

        $('.btn-imp').click(function () {
            var date7 = new Date(Date.now() - 7*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            get_UINB(secret_key,api_key,req_time,rdate1,rdate7);
        });
    });

    // UINB
    function get_UINB(secret_key,api_key,req_time,rdate1,rdate7){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_UINB'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7 },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                // console.log(resp);
                ins_UINB(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_UINB(resp){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_UINB'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                save_UINB();
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_UINB(){ 
        // var periode = $("#periode_awal").val(); 
        $("#load").fadeOut();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_UINB'); ?>",
            data: {}, 
            success: function (resp) {  
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    },function(){ 
                      getSLHSO();
                    });
                    // window.location.reload();
                });
            }, 
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function disabled(){
      $(".box-new").hide();
      $(".box-view").show();
      $(".btn-addDetail").hide();
      $(".btn-updDetail").hide();
      $(".btn-delDetail").hide();
      $("#tipebayar").attr('disabled',true);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").hide();
    }

    function clear(){
      $("#kdsup").val('');
      $("#nopo").val('');
      $("#noslhso").val('');
      $("#tipebayar").val('');
      $("#tgltrm").val(moment().format('DD-MM-YYYY'));
      $("#tglpo").val(moment().format('DD-MM-YYYY'));
      $("#jth_tmp").val('15');
    }

    function dateback(tgl) {
      var less = $("#jth_tmp").val();
      var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var res = day + '-' + month + '-' + year;
    	return res;
    }

    function kdlokasi(){
      // var kdsales = $('#kdsales').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/kdlokasi");?>",
            data: { },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#kdlokasi").val(data.kdlokasi);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getTipeUnit(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getTipeUnit");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdtipe').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Tipe Unit --'));
                $("#kdtipe").trigger("change.chosen");
                if ($('#kdtipe').hasClass("chosen-hidden-accessible")) {
                    $('#kdtipe').select2('destroy');
                    $("#kdtipe").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdtipe')
                      .append($('<option>', { value : value.kode })
                      .html("<b style='font-size: 14px;'>" + value.kode + " </b>"));
                });

                $('#kdtipe').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getdetPO(){
        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();
        //alert(kddiv);
        var column1 = [];


        column1.push({
                "aTargets": [ 0 ],
                "searchable": false,
                "orderable": false,

//              "className": 'select-checkbox',

                "checkboxes": {
                    'selectRow': true
                }

        });

        var column = [];

        table1 = $('.dataTable').DataTable({
            "aoColumnDefs": column1,
            "columns": [
                { "data": "nosin"},
                { "data": "kode" },
                { "data": "kdtipe" },
                { "data": "nmtipe"},
                { "data": "warna" },
                { "data": "nosin" },
                { "data": "nora" },
                { "data": "tahun" },
                { "data": "status_do" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "fixedColumns": {
                leftColumns: 2
            },
            "select": {
                style: 'single'
            },
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nopo", "value": nopo});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){

            },
            "sAjaxSource": "<?=site_url('slnew/getdetPO');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        // //row number
        // table.on( 'draw.dt', function () {
        // var PageInfo = $('.dataTable').DataTable().page.info();
        //     table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
        //         cell.innerHTML = i + 1 + PageInfo.start;
        //     } );
        // } );

        table1.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    }

    function getdetHSO(){

        var column2 = [];

        column2.push({
                "aTargets": [ 0 ],
                "searchable": false,
                "orderable": false,

//              "className": 'select-checkbox',

                "checkboxes": {
                    'selectRow': true
                }
        });

        table2 = $('.dataTable').DataTable({
            "aoColumnDefs": column2,
            "columns": [
                { "data": "nomesin"},
                { "data": "kodetipeunit" },
                { "data": "kdtipe" },
                { "data": "nmtipe"},
                { "data": "nmwarna" },
                { "data": "nosin" },
                { "data": "nora" },
                { "data": "tahun" },
                { "data": "statdo" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "fixedColumns": {
                leftColumns: 2
            },
            "select": {
                style: 'single'
            },
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noslhso", "value": $("#noslhso").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('slnew/getdetHSO');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        // //row number
        // table.on( 'draw.dt', function () {
        // var PageInfo = $('.dataTable').DataTable().page.info();
        //     table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
        //         cell.innerHTML = i + 1 + PageInfo.start;
        //     } );
        // } );

        table2.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    }

    function getdetHSO2(){

        var column2 = [];

        column2.push({
                "aTargets": [ 0 ],
                "searchable": false,
                "orderable": false,

//              "className": 'select-checkbox',

                "checkboxes": {
                    'selectRow': true
                }
        });

        table2 = $('.dataTable').DataTable({
            "aoColumnDefs": column2,
            "columns": [
                { "data": "nomesin"},
                { "data": "kodetipeunit" },
                { "data": "kdtipe" },
                { "data": "nmtipe"},
                { "data": "nmwarna" },
                { "data": "nosin" },
                { "data": "nora" },
                { "data": "tahun" },
                { "data": "statdo" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "fixedColumns": {
                leftColumns: 2
            },
            "select": {
                style: 'single'
            },
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noslhso", "value": $("#noslhso").val()},
                            { "name": "noslhso2", "value": $("#noslhso2").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('slnew/getdetHSO2');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        // //row number
        // table.on( 'draw.dt', function () {
        // var PageInfo = $('.dataTable').DataTable().page.info();
        //     table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
        //         cell.innerHTML = i + 1 + PageInfo.start;
        //     } );
        // } );

        table2.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    }

    function getdetHSO3(){

        var column2 = [];

        column2.push({
                "aTargets": [ 0 ],
                "searchable": false,
                "orderable": false,

//              "className": 'select-checkbox',

                "checkboxes": {
                    'selectRow': true
                }
        });

        table2 = $('.dataTable').DataTable({
            "aoColumnDefs": column2,
            "columns": [
                { "data": "nomesin"},
                { "data": "kodetipeunit" },
                { "data": "kdtipe" },
                { "data": "nmtipe"},
                { "data": "nmwarna" },
                { "data": "nosin" },
                { "data": "nora" },
                { "data": "tahun" },
                { "data": "statdo" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "fixedColumns": {
                leftColumns: 2
            },
            "select": {
                style: 'single'
            },
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noslhso", "value": $("#noslhso").val()},
                            { "name": "noslhso2", "value": $("#noslhso2").val()},
                            { "name": "noslhso3", "value": $("#noslhso3").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('slnew/getdetHSO3');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        // //row number
        // table.on( 'draw.dt', function () {
        // var PageInfo = $('.dataTable').DataTable().page.info();
        //     table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
        //         cell.innerHTML = i + 1 + PageInfo.start;
        //     } );
        // } );

        table2.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    }

    function getSLHSO(){
        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();
        var noslhso = $("#noslhso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getSLHSO");?>",
            data: {"noslhso":noslhso,"nopo":nopo},
            beforeSend: function() {
                $('#noslhso').html("")
                            .append($('<option>', { value :  ''  }));
                            // .text('-- Pilih SPK --'));
                $("#noslhso").trigger("change.chosen");
                if ($('#noslhso').hasClass("chosen-hidden-accessible")) {
                    $('#noslhso').select2('destroy');
                    $("#noslhso").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#noslhso')
                      .append($('<option>', { value : value.noshippinglist })
                      .html("<b style='font-size: 14px;'>" + value.noshippinglist + " </b>"));
                });

                $('#noslhso').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getSLHSO2(){
        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();
        var noslhso = $("#noslhso").val(); 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getSLHSO2");?>",
            data: {"noslhso":noslhso,"nopo":nopo},
            beforeSend: function() {
                $('#noslhso2').html("")
                            .append($('<option>', { value :  ''  }));
                            // .text('-- Pilih SPK --'));
                $("#noslhso2").trigger("change.chosen");
                if ($('#noslhso2').hasClass("chosen-hidden-accessible")) {
                    $('#noslhso2').select2('destroy');
                    $("#noslhso2").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#noslhso2')
                      .append($('<option>', { value : value.noshippinglist })
                      .html("<b style='font-size: 14px;'>" + value.noshippinglist + " </b>"));
                });

                $('#noslhso2').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getSLHSO3(){
        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase(); 
        var noslhso2 = $("#noslhso2").val();
        var noslhso = $("#noslhso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getSLHSO3");?>",
            data: {"noslhso":noslhso,"nosl":noslhso2,"nopo":nopo},
            beforeSend: function() {
                $('#noslhso3').html("")
                            .append($('<option>', { value :  ''  }));
                            // .text('-- Pilih SPK --'));
                $("#noslhso3").trigger("change.chosen");
                if ($('#noslhso3').hasClass("chosen-hidden-accessible")) {
                    $('#noslhso3').select2('destroy');
                    $("#noslhso3").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#noslhso3')
                      .append($('<option>', { value : value.noshippinglist })
                      .html("<b style='font-size: 14px;'>" + value.noshippinglist + " </b>"));
                });

                $('#noslhso3').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeSalesHeader(){
      var kdsales = $('#kdsales').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getKodeSalesHeader");?>",
            data: {"kdsales":kdsales },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmspv").val(data.nmsales_header);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeTipeHeader(){
      var kdtipe = $('#kdtipe').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getKodeTipeHeader");?>",
            data: {"kdtipe":kdtipe },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmtipe").val(data.nmtipe);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdWarnaHeader(){
      var kdwarna = $('#kdwarna').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getKdWarnaHeader");?>",
            data: {"kdwarna":kdwarna },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmwarna").val(data.nmwarna);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function enabled(){
      $(".box-view").hide();
      $(".box-new").show();
      $("#kdsup").prop('disabled',false);
      $("#noslhso").prop('disabled',false);
      $("#tglpo").prop('disabled',false);
      $("#tgltrm").prop('disabled',false);
      $("#jth_tmp").prop('disabled',false);
      $("#tipebayar").prop('disabled',false);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").show();
    }

    function aktif(){
      $(".box-view").hide();
      $(".box-new").show();
      $('#kdsup').attr('disabled',false);
      $('#noslhso').attr('disabled',false);
      $('#tipebayar').attr('disabled',false);
      $('#jth_tmp').attr('disabled',false);
      $('#tglpo').attr('disabled',false);
      $('#tgltrm').attr('disabled',false);

    }

    function nonaktif(){
      $(".box-view").show();
      $(".box-new").hide();
      $('#kdsup').attr('disabled',true);
      $('#noslhso').attr('disabled',true);
      $('#tipebayar').attr('disabled',true);
      $('#jth_tmp').attr('disabled',true);
      $('#tglpo').attr('disabled',true);
      $('#tgltrm').attr('disabled',true);

    }

    function set_faktur(){

      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      // alert(nopo);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/set_faktur");?>",
            data: {"nopo":nopo },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      nonaktif();
                      $("#tglpo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpo)));
                      $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglterima)));
                      $("#noslhso").val(data.noslhso);
                      // alert(data.idspk);
                      $("#noslhso").trigger("chosen:updated");
                      $('#noslhso').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      if(data.jnsbayar==='TUNAI'){
                        $(".tunai").show();
                        $(".kredit").hide();
                        $("#tipebayar").val('T');
                        $("#tipebayar").trigger("change");
                      } else {
                        $(".tunai").hide();
                        $(".kredit").show();
                        $("#tipebayar").val('K');
                        $("#tipebayar").trigger("change");
                      }
                      if(data.progres==1){
                        // alert('1');
                        $(".detail_add").hide();
                        $("#statpo").show();
                        $("#statpo").val(data.ket);
                        $(".btn-edit").attr('disabled',true);
                        $(".btn-del").attr('disabled',true);
                      } else {
                        // alert('2');
                        $(".detail_add").show();
                        $("#statpo").hide();
                        $(".btn-edit").attr('disabled',false);
                        $(".btn-del").attr('disabled',false);
                      }
                      $("#kdsup").val(data.kdsup);
                      $("#kdsup").trigger("chosen:updated");
                      $('#kdsup').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#jth_tmp").val(data.njtempo);
                      $("#tgltmp").val(dateback($("#tglpo").val()));
                      $(".btn-add").attr('disabled',false);
                      $(".btn-batal").show();
                      getdetPO();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function caripo(){

      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      //alert(nopo);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/caripo");?>",
            data: {"nopo":nopo },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                    // if(data.noso=$('#noso').val()){
                        swal({
                            title: "Proses Gagal",
                            text: "No Faktur Sudah Ada",
                            type: "error"
                        }, function(){
                            clear();
                            $('#noso').val('');
                        });
                    // }
                });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function getKdSup(){
        // var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/getKdSup");?>",
            data: {},
            beforeSend: function() {
                $('#kdsup').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Supplier --'));
                $("#kdsup").trigger("change.chosen");
                if ($('#kdsup').hasClass("chosen-hidden-accessible")) {
                    $('#kdsup').select2('destroy');
                    $("#kdsup").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdsup')
                      .append($('<option>', { value : value.kdsup })
                      .html("<b style='font-size: 14px;'>" + value.nmsup + " </b>"));
                });

                $('#kdsup').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function addpo(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      var kdsup = $("#kdsup").val();
      var noslhso = $("#noslhso").val();
      var tglpo = $("#tglpo").val();
      var tgltrm = $("#tgltrm").val();
      var tpbayar = $("#tipebayar").val();
      var kdlokasi = $("#kdlokasi").val();
      if(tpbayar==='K'){
        var jth_tmp =  $("#jth_tmp").val();
      } else {
        var jth_tmp =  '15';
      }

        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/addpo");?>",
            data: {"nopo":nopo
                    ,"kdsup":kdsup
                    ,"noslhso":noslhso
                    ,"tglpo":tglpo
                    ,"tgltrm":tgltrm
                    ,"tpbayar":tpbayar
                    ,"jth_tmp":jth_tmp
                    ,"kdlokasi":kdlokasi },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function addpo2(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      var kdsup = $("#kdsup").val();
      var noslhso = $("#noslhso").val();
      var noslhso2 = $("#noslhso2").val();
      var tglpo = $("#tglpo").val();
      var tgltrm = $("#tgltrm").val();
      var tpbayar = $("#tipebayar").val();
      var kdlokasi = $("#kdlokasi").val();
      if(tpbayar==='K'){
        var jth_tmp =  $("#jth_tmp").val();
      } else {
        var jth_tmp =  '15';
      }

        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/addpo2");?>",
            data: {"nopo":nopo
                    ,"kdsup":kdsup
                    ,"noslhso":noslhso
                    ,"noslhso2":noslhso2
                    ,"tglpo":tglpo
                    ,"tgltrm":tgltrm
                    ,"tpbayar":tpbayar
                    ,"jth_tmp":jth_tmp
                    ,"kdlokasi":kdlokasi },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function addpo3(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      var kdsup = $("#kdsup").val();
      var noslhso = $("#noslhso").val();
      var noslhso2 = $("#noslhso2").val();
      var noslhso3 = $("#noslhso3").val();
      var tglpo = $("#tglpo").val();
      var tgltrm = $("#tgltrm").val();
      var tpbayar = $("#tipebayar").val();
      var kdlokasi = $("#kdlokasi").val();
      if(tpbayar==='K'){
        var jth_tmp =  $("#jth_tmp").val();
      } else {
        var jth_tmp =  '15';
      }

        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/addpo3");?>",
            data: {"nopo":nopo
                    ,"kdsup":kdsup
                    ,"noslhso":noslhso
                    ,"noslhso2":noslhso2
                    ,"noslhso3":noslhso3
                    ,"tglpo":tglpo
                    ,"tgltrm":tgltrm
                    ,"tpbayar":tpbayar
                    ,"jth_tmp":jth_tmp
                    ,"kdlokasi":kdlokasi },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function updatepo(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      var tglpo = $("#tglpo").val();
      var tgltrm = $("#tgltrm").val();
      var noslhso = $("#noslhso").val();

      var tpbayar = $("#tipebayar").val();
      var kdsup = $("#kdsup").val();
      var kdlokasi = $("#kdlokasi").val();
      if(tpbayar==='K'){
        var jth_tmp =  $("#jth_tmp").val();
      } else {
        var jth_tmp =  '15';
      }
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/updatepo");?>",
            data: {"nopo":nopo
                    ,"tglpo":tglpo
                    ,"tgltrm":tgltrm
                    ,"noslhso":noslhso
                    ,"tpbayar":tpbayar
                    ,"kdsup":kdsup
                    ,"kdlokasi":kdlokasi
                    ,"jth_tmp":jth_tmp },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function updatepo2(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      var tglpo = $("#tglpo").val();
      var tgltrm = $("#tgltrm").val();
      var noslhso = $("#noslhso").val();
      var noslhso2 = $("#noslhso2").val();

      var tpbayar = $("#tipebayar").val();
      var kdsup = $("#kdsup").val();
      var kdlokasi = $("#kdlokasi").val();
      if(tpbayar==='K'){
        var jth_tmp =  $("#jth_tmp").val();
      } else {
        var jth_tmp =  '15';
      }
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/updatepo2");?>",
            data: {"nopo":nopo
                    ,"tglpo":tglpo
                    ,"tgltrm":tgltrm
                    ,"noslhso":noslhso
                    ,"noslhso2":noslhso2
                    ,"tpbayar":tpbayar
                    ,"kdsup":kdsup
                    ,"kdlokasi":kdlokasi
                    ,"jth_tmp":jth_tmp },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function updatepo3(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
      var tglpo = $("#tglpo").val();
      var tgltrm = $("#tgltrm").val();
      var noslhso = $("#noslhso").val();
      var noslhso2 = $("#noslhso2").val();
      var noslhso3 = $("#noslhso3").val();

      var tpbayar = $("#tipebayar").val();
      var kdsup = $("#kdsup").val();
      var kdlokasi = $("#kdlokasi").val();
      if(tpbayar==='K'){
        var jth_tmp =  $("#jth_tmp").val();
      } else {
        var jth_tmp =  '15';
      }
        $.ajax({
            type: "POST",
            url: "<?=site_url("slnew/updatepo3");?>",
            data: {"nopo":nopo
                    ,"tglpo":tglpo
                    ,"tgltrm":tgltrm
                    ,"noslhso":noslhso
                    ,"noslhso2":noslhso2
                    ,"noslhso3":noslhso3
                    ,"tpbayar":tpbayar
                    ,"kdsup":kdsup
                    ,"kdlokasi":kdlokasi
                    ,"jth_tmp":jth_tmp },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function deleted(nosin){

        var nosin = nosin.replace("-","");
        var nosin = nosin.replace("-","");
        var nosin = nosin.replace("-","");
        var nosin = nosin.toUpperCase();
        // alert(nosin);
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("slnew/deleted");?>",
                    data: {"nosin":nosin },
                    success: function(resp){
                      var obj = JSON.parse(resp);
                      $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            table.ajax.reload();
                        });
                    });
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }

    function deletepo(){
      var po = $("#nopo").val();
      var nopo = po.replace("-","");
      var nopo = nopo.replace("-","");
      var nopo = nopo.toUpperCase();
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("slnew/deletepo");?>",
                    data: {"nopo":nopo },
                    success: function(resp){
                      var obj = JSON.parse(resp);
                      $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            window.location.reload();
                        });
                    });
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
</script>

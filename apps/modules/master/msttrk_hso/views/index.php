<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    } 
    .pass-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    } 
    #load{
        width: 80%;
        height: 80%;
        position: fixed;
        text-indent: 100%;
        background: #ebebeb url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>


<div id="load">Loading...</div> 
<div class="profile">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active">
                <a href="#tab_1_1" class="tab_uinb" data-toggle="tab"> Unit Assist </a>
            </li>
            <li>
                <a href="#tab_1_2" class="tab_spk" data-toggle="tab"> SPK Assist </a>
            </li>
            <li>
                <a href="#tab_1_3" class="tab_doch" data-toggle="tab"> Faktur STNK Assist </a>
            </li>
            <li>
                <a href="#tab_1_4" class="tab_pkb" data-toggle="tab"> Service PKB </a>
            </li>
            <li>
                <a href="#tab_1_5" class="tab_po" data-toggle="tab"> Purchace Order Sparepart </a>
            </li>
            <li>
                <a href="#tab_1_6" class="tab_so" data-toggle="tab"> Penjualan Sparepart </a>
            </li>
            <li>
                <a href="#tab_1_6" class="tab_so" data-toggle="tab"> Penjualan Bahan </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-12"> 
                        <span>
                            <button type="button" class="btn btn-primary btn-add-uinb"><i class="fa fa-plus"> Import Data</i></button>
                        </span>
                        <span>
                            <button type="button" class="btn btn-success btn-upload-uinb"><i class="fa fa-upload"> Upload Data</i></button>
                        </span> 
                    </div> 
                    <div class="col-md-12"> 
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example tbl_uinb" class="display select">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;text-align: center;">DETAIL</th>
                                        <th style="text-align: center;">No. Shipping List</th>
                                        <th style="text-align: center;">Tanggal Terima</th>
                                        <th style="width: 20px;text-align: center;">ID Dealer</th>
                                        <th style="text-align: center;">No Invoice</th>
                                        <th style="text-align: center;">Status</th>
                                        <th style="text-align: center;">No. Goods Receipt</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="width: 10px;text-align: center;">DETAIL</th>
                                        <th style="text-align: center;">No. Shipping List</th>
                                        <th style="text-align: center;">Tanggal Terima</th>
                                        <th style="width: 20px;text-align: center;">ID Dealer</th>
                                        <th style="text-align: center;">No Invoice</th>
                                        <th style="text-align: center;">Status</th>
                                        <th style="text-align: center;">No. Goods Receipt</th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>  
                </div>
            </div>
            <!--tab_1_2-->
            <div class="tab-pane" id="tab_1_2">
                <div class="row">
                    <div class="col-md-12"> 
                        <span>
                            <button type="button" class="btn btn-primary btn-add-spk"><i class="fa fa-plus"> Import Data</i></button>
                        </span>
                        <span>
                            <button type="button" class="btn btn-success btn-upload-spk"><i class="fa fa-upload"> Upload Data</i></button>
                        </span> 
                    </div> 
                    <div class="col-md-12"> 
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example tbl_spk" class="display select" style="width:3000px">
                                <thead>
                                    <tr>
                                        <!-- <th style="width: 1%;text-align: center;">Detail</th> -->
                                        <th style="width: 19%;text-align: center;">ID SPK</th>
                                        <th style="width: 19%;text-align: center;">ID Prospect</th>
                                        <th style="width: 19%;text-align: center;">Nama Customer</th>
                                        <th style="width: 30%;text-align: center;">No KTP</th>
                                        <th style="width: 19%;text-align: center;">Alamat</th>
                                        <th style="width: 19%;text-align: center;">Nama Provinsi</th>
                                        <th style="width: 19%;text-align: center;">Nama Kota</th>
                                        <th style="width: 19%;text-align: center;">Nama Kecamatan</th>
                                        <th style="width: 19%;text-align: center;">Nama Kelurahan</th>
                                        <th style="width: 19%;text-align: center;">Kode Pos</th>
                                        <th style="width: 19%;text-align: center;">No. Kontak</th>
                                        <th style="width: 19%;text-align: center;">Nama BPKB</th>
                                        <th style="width: 19%;text-align: center;">No. KTP BPKB</th>
                                        <th style="width: 19%;text-align: center;">Latitude</th>
                                        <th style="width: 19%;text-align: center;">Longitude</th>
                                        <th style="width: 19%;text-align: center;">NPWP</th>
                                        <th style="width: 19%;text-align: center;">No. KK</th>
                                        <th style="width: 19%;text-align: center;">Fax</th>
                                        <th style="width: 19%;text-align: center;">Email</th>
                                        <th style="width: 19%;text-align: center;">Id Sales</th>
                                        <th style="width: 19%;text-align: center;">Id Event</th>
                                        <th style="width: 19%;text-align: center;">Tgl Psn</th>
                                        <th style="width: 19%;text-align: center;">Status SPK</th>
                                        <th style="width: 19%;text-align: center;">Kode Tipe Unit</th>
                                        <th style="width: 19%;text-align: center;">Anggota KK</th>
                                        <th style="width: 19%;text-align: center;">Kode Warna</th>
                                        <th style="width: 19%;text-align: center;">Quantity</th>
                                        <th style="width: 19%;text-align: center;">Harga Jual</th>
                                        <th style="width: 19%;text-align: center;">Diskon</th>
                                        <th style="width: 19%;text-align: center;">Kode PPN</th>
                                        <th style="width: 19%;text-align: center;">DP</th>
                                        <th style="width: 19%;text-align: center;">FP</th>
                                        <th style="width: 19%;text-align: center;">Tipe Pembayaran</th>
                                        <th style="width: 19%;text-align: center;">Jumlah Tanda Jadi</th>
                                        <th style="width: 19%;text-align: center;">Tgl Pengiriman</th>
                                        <th style="width: 19%;text-align: center;">Id Sales Prog</th>
                                        <th style="width: 19%;text-align: center;">Id Appreal</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <!-- <th style="width: 1%;text-align: center;">Detail</th> -->
                                        <th style="width: 19%;text-align: center;">ID SPK</th>
                                        <th style="width: 19%;text-align: center;">ID Prospect</th>
                                        <th style="width: 19%;text-align: center;">Nama Customer</th>
                                        <th style="width: 30%;text-align: center;">No KTP</th>
                                        <th style="width: 19%;text-align: center;">Alamat</th>
                                        <th style="width: 19%;text-align: center;">Kode Provinsi</th>
                                        <th style="width: 19%;text-align: center;">Kode Kota</th>
                                        <th style="width: 19%;text-align: center;">Kode Kecamatan</th>
                                        <th style="width: 19%;text-align: center;">Kode Kelurahan</th>
                                        <th style="width: 19%;text-align: center;">Kode Pos</th>
                                        <th style="width: 19%;text-align: center;">No. Kontak</th>
                                        <th style="width: 19%;text-align: center;">Nama BPKB</th>
                                        <th style="width: 19%;text-align: center;">No. KTP BPKB</th>
                                        <th style="width: 19%;text-align: center;">Latitude</th>
                                        <th style="width: 19%;text-align: center;">Longitude</th>
                                        <th style="width: 19%;text-align: center;">NPWP</th>
                                        <th style="width: 19%;text-align: center;">No. KK</th>
                                        <th style="width: 19%;text-align: center;">Fax</th>
                                        <th style="width: 19%;text-align: center;">Email</th>
                                        <th style="width: 19%;text-align: center;">Id Sales</th>
                                        <th style="width: 19%;text-align: center;">Id Event</th>
                                        <th style="width: 19%;text-align: center;">Tgl Psn</th>
                                        <th style="width: 19%;text-align: center;">Status SPK</th>
                                        <th style="width: 19%;text-align: center;">Kode Tipe Unit</th>
                                        <th style="width: 19%;text-align: center;">Anggota KK</th>
                                        <th style="width: 19%;text-align: center;">Kode Warna</th>
                                        <th style="width: 19%;text-align: center;">Quantity</th>
                                        <th style="width: 19%;text-align: center;">Harga Jual</th>
                                        <th style="width: 19%;text-align: center;">Diskon</th>
                                        <th style="width: 19%;text-align: center;">Kode PPN</th>
                                        <th style="width: 19%;text-align: center;">DP</th>
                                        <th style="width: 19%;text-align: center;">FP</th>
                                        <th style="width: 19%;text-align: center;">Tipe Pembayaran</th>
                                        <th style="width: 19%;text-align: center;">Jumlah Tanda Jadi</th>
                                        <th style="width: 19%;text-align: center;">Tgl Pengiriman</th>
                                        <th style="width: 19%;text-align: center;">Id Sales Prog</th>
                                        <th style="width: 19%;text-align: center;">Id Appreal</th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>  
                </div>
            </div>
            <!--tab_1_3-->
            <div class="tab-pane" id="tab_1_3">
                <div class="row">
                    <div class="col-md-12"> 
                        <span>
                            <button type="button" class="btn btn-primary btn-add-doch"><i class="fa fa-plus"> Import Data</i></button>
                        </span>
                        <span>
                            <button type="button" class="btn btn-success btn-upload-doch"><i class="fa fa-upload"> Upload Data</i></button>
                        </span> 
                    </div> 
                    <div class="col-md-12"> 
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example tbl_doch" class="display select">
                                <thead>
                                    <tr>
                                        <th style="width: 1%;text-align: center;">Detail</th>
                                        <th style="text-align: center;">ID SO</th>
                                        <th style="text-align: center;">ID SPK</th>
                                        <th style="text-align: center;">No. Rangka</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="width: 1%;text-align: center;">Detail</th>
                                        <th style="text-align: center;">ID SO</th>
                                        <th style="text-align: center;">ID SPK</th>
                                        <th style="text-align: center;">No. Rangka</th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>  
                </div>
            </div>
            <!--tab_1_4-->
            <div class="tab-pane" id="tab_1_4">
                <div class="row">
                    <div class="col-md-12"> 
                        <span>
                            <button type="button" class="btn btn-primary btn-add-pkb"><i class="fa fa-plus"> Import Data</i></button>
                        </span>
                        <span>
                            <button type="button" class="btn btn-success btn-upload-pkb"><i class="fa fa-upload"> Upload Data</i></button>
                        </span> 
                    </div> 
                    <div class="col-md-12"> 
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example tbl_doch" class="display select">
                                <thead>
                                    <tr>
                                        <th style="width: 1%;text-align: center;">Detail</th>
                                        <th style="text-align: center;">ID SO</th>
                                        <th style="text-align: center;">ID SPK</th>
                                        <th style="text-align: center;">No. Rangka</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="width: 1%;text-align: center;">Detail</th>
                                        <th style="text-align: center;">ID SO</th>
                                        <th style="text-align: center;">ID SPK</th>
                                        <th style="text-align: center;">No. Rangka</th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>  
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
        
        // datatable UINB
        getTable_UINB();

        $('.tab_uinb').click(function () {  
            getTable_UINB();
        })

        $('.btn-add-uinb').click(function () {  
            var date7 = new Date(Date.now() - 2*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1 = new Date(Date.now() + 2*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            get_UINB(secret_key,api_key,req_time,rdate1,rdate7);
            // get_UINB(rdate1,rdate7);
        })

        $('.btn-upload-uinb').click(function () {   
            save_UINB();
        })
        
        // datatable SPK 
        $('.tab_spk').click(function () {  
            getTable_SPK();
        })

        $('.btn-add-spk').click(function () {  
            var date7       = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7      = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1       = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1      = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            get_SPK(secret_key,api_key,req_time,rdate1,rdate7);
        })

        $('.btn-upload-spk').click(function () {   
            save_SPK();
        })
        
        // datatable DOCH 
        $('.tab_doch').click(function () {  
            getTable_DOCH();
        })

        $('.btn-add-doch').click(function () {  
            var date7 = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            get_DOCH(secret_key,api_key,req_time,rdate1,rdate7);
            // get_DOCH(rdate1,rdate7);
        })

        $('.btn-upload-doch').click(function () {   
            save_DOCH();
        })

    });

    // UINB
    function get_UINB(secret_key,api_key,req_time,rdate1,rdate7){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_UINB'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7 },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                // console.log(resp);
                ins_UINB(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_UINB(resp){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_UINB'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    },function () {
                        tbl_uinb.ajax.reload();
                    });
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_UINB(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_UINB'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        tbl_uinb.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function getTable_UINB(){
        var column_uinb = [];

        column_uinb.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        // column.push({
        //     "aTargets": [ 27,28,29,30,31,33 ],
        //     "mRender": function (data, type, full) {
        //          return type === 'export' ? data : numeral(data).format('0,0.00');
        //     },
        //     "sClass": "center"
        // });

        // column.push({
        //     "aTargets": [ 0,5 ],
        //  "searchable": false,
        //     "bSortable": false,
        //     "sClass": "center"
        // });

        $('.tbl_uinb_detail').DataTable({});
        tbl_uinb = $('.tbl_uinb').DataTable({
            "aoColumnDefs": column_uinb,
            "order": [[ 1, "asc" ]],
            "columns": [
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "noshippinglist" },
                { "data": "tanggalterima" },
                { "data": "dealerid" },
                { "data": "noinvoice" },
                { "data": "statusshippinglist" },
                { "data": "nogoodsreceipt" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('msttrk_hso/uinb_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                    },
                    exportOptions: {orthogonal: 'export'}
                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                    .addClass('compact')
                                   .css('font-size', 'inherit');
                    }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.tbl_uinb').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        // Add event listener for opening and closing details
        $('.tbl_uinb tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tbl_uinb.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );

        $('.tbl_uinb tfoot th').each( function () {
            var title = $('.tbl_uinb thead th').eq( $(this).index() ).text();
            if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        tbl_uinb.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


                download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover tbl_uinb_detail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
                                                '<th style="text-align: center;">Kode Tipe Unit</th>'+
                                                '<th style="text-align: center;">Kode Warna</th>'+
                                                '<th style="text-align: center;">Kuantitas Terkirim</th>'+
                                                '<th style="text-align: center;">Kuantitas Diterima</th>'+
                                                '<th style="text-align: center;">No. Mesin</th>'+
                                                '<th style="text-align: center;">No. Rangka</th>'+
                                                '<th style="text-align: center;">Status RFS</th>'+
                                                '<th style="text-align: center;">PO ID</th>'+
                                                '<th style="text-align: center;">Kelengkapan Unit</th>'+
                                                '<th style="text-align: center;">No Goods Receipt</th>'+
                        '<th style="text-align: center;">Doc NRFS ID</th>'+
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
                                                '<td style="text-align: ">'+data.kodetipeunit+'</td>'+
                                                '<td style="text-align: ">'+data.kodewarna+'</td>'+
                                                '<td style="text-align: ">'+data.kuantitasterkirim+'</td>'+
                                                '<td style="text-align: ">'+data.kuantitasditerima+'</td>'+
                                                '<td style="text-align: ">'+data.nomesin+'</td>'+
                                                '<td style="text-align: ">'+data.norangka+'</td>'+
                                                '<td style="text-align: ">'+data.statusrfs+'</td>'+
                                                '<td style="text-align: ">'+data.poid+'</td>'+
                                                '<td style="text-align: ">'+data.kelengkapanunit+'</td>'+
                                                '<td style="text-align: ">'+data.nogoodsreceipt+'</td>'+
                                                '<td style="text-align: ">'+data.docnrfsid+'</td>'+
                    '</tr>';
                    no++;
                });
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    }

    // SPK
    function get_SPK(secret_key,api_key,req_time,rdate1,rdate7){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SPK'); ?>",
            data: {"secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7 },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                // console.log(resp);
                ins_SPK(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_SPK(resp){ 
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_SPK'); ?>",
            data: {"info" : resp}, 
            success: function (resp) {
                // console.log(resp);
                if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    },function () {
                        tbl_spk.ajax.reload();
                    });
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_SPK(){ 
        $("#load").show();
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_SPK'); ?>",
            data: {}, 
            success: function (resp) {
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        tbl_spk.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function getTable_SPK(){
        var column_spk = [];

        column_spk.push({
            "aTargets": [ 21,34 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        column_spk.push({
            "aTargets": [ 27,28,29,30,31,33 ],
            "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "center"
        });

        column_spk.push({
            "aTargets": [ 0,5 ],
            "searchable": false,
            "bSortable": false,
            "sClass": "center"
        });

        tbl_spk = $('.tbl_spk').DataTable({
            "aoColumnDefs": column_spk,
            "order": [[ 1, "asc" ]],
            "columns": [
                { "data": "idspk" },
                { "data": "idprospect" },
                { "data": "nmcustomer" },
                { "data": "noktp" },
                { "data": "alamat" },
                { "data": "kodepropinsi" },
                { "data": "kdkota" },
                { "data": "kdkec" },
                { "data": "kdlurah"},
                { "data": "kdpos" },
                { "data": "nokontak" },
                { "data": "nmbpkb" },
                { "data": "noktpbpkb" },
                { "data": "lat" },
                { "data": "longi" },
                { "data": "npwp" },
                { "data": "nokk" },
                { "data": "fax" },
                { "data": "email" },
                { "data": "idsalespeople" },
                { "data": "idevent" },
                { "data": "tglpsn" },
                { "data": "statusspk" },
                { "data": "kdtipeunit" },
                { "data": "anggotakk" },
                { "data": "kodewarna" },
                { "data": "qty" },
                { "data": "hrgjual" },
                { "data": "diskon" },
               { "data": "ppnamount" },
                { "data": "downpayment" },
                { "data": "fakturpajak" },
                { "data": "tppembayaran" },
                { "data": "jmltandajadi" },
                { "data": "tglpengiriman" },
                { "data": "idsalesprog" },
                { "data": "idappreal" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('msttrk_hso/spk_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.tbl_spk').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });  

        $('.tbl_spk tfoot th').each( function () {
            var title = $('.tbl_uinb thead th').eq( $(this).index() ).text();
            if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        tbl_spk.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


                download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }
    }

    // DOCH
    function get_DOCH(secret_key,api_key,req_time,rdate1,rdate7){ 
        $("#load").show();
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_DOCH'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7 },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                // alert(resp);
                ins_DOCH(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_DOCH(resp){ 
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_DOCH'); ?>",
            data: {"info" : resp}, 
            success: function (resp) {
                // console.log(resp);
                if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    },function () {
                        tbl_doch.ajax.reload();
                    });
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_DOCH(){ 
        $("#load").show();
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_DOCH'); ?>",
            data: {}, 
            success: function (resp) {
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        tbl_doch.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function getTable_DOCH(){
        var column_doch = [];

        column_doch.push({
            "aTargets": [  ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        column_doch.push({
                "aTargets": [  ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    // return formmatedvalue;
                },
          "sClass": "right"
            });

        $('.tbl_doch_detail').DataTable({});
        tbl_doch = $('.tbl_doch').DataTable({
            "aoColumnDefs": column_doch,
            "order": [[ 1, "asc" ]],
            "columns": [ 
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "idso" },
                { "data": "idspk" },
                { "data": "nomorrangka" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('msttrk_hso/doch_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.tbl_doch').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // Add event listener for opening and closing details
        $('.tbl_doch tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tbl_doch.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );  

        $('.tbl_doch tfoot th').each( function () {
            var title = $('.tbl_uinb thead th').eq( $(this).index() ).text();
            if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        tbl_doch.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


                download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover tbl_doch_detail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
                        '<th style="text-align: center;">No. Rangka</th>'+
                        '<th style="text-align: center;">No. Faktur STNK</th>'+
                                                '<th style="text-align: center;">Status Faktur STNK</th>'+
                                                '<th style="text-align: center;">No STNK</th>'+
                                                '<th style="text-align: center;">Plat Nomor</th>'+
                        '<th style="text-align: center;">Nomor BPKB</th>'+
                                                '<th style="text-align: center;">Nama Penerima</th>'+
                                                '<th style="text-align: center;">Jenis ID Penerima</th>'+
                                                '<th style="text-align: center;">No ID Penerima</th>'+
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
                                                '<td style="text-align: ">'+data.nomorrangka+'</td>'+
                                                '<td style="text-align: ">'+data.nomorfakturstnk+'</td>'+
                                                '<td style="text-align: ">'+data.statusfakturstnk+'</td>'+
                                                '<td style="text-align: ">'+data.nomorstnk+'</td>'+
                                                '<td style="text-align: ">'+data.platnomor+'</td>'+
                                                '<td style="text-align: ">'+data.nomorbpkb+'</td>'+
                                                '<td style="text-align: ">'+data.namapenerimastnk+'</td>'+
                                                '<td style="text-align: ">'+data.jenisidpenerimastnk+'</td>'+
                                                '<td style="text-align: ">'+data.noidpenerimastnk+'</td>'+
                    '</tr>';
                    no++;
                });
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    }
 

</script>

<?php

/* 
 * ***************************************************************
 * Script : pdf
 * Version : 
 * Date : 
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);

function month_name($param){
    $date = explode("-", $param);
    $mm = strtoupper(month($date[1]));
    $yy = $date[0];
    $dm = $param."-01";
    $dd = '';//date("t", strtotime($dm));
    return $dd." ".$mm." ".$yy;
}

function spacing($level){
    $res = "&nbsp;";
    for ($x = 1; $x <= $level; $x++) {
        $res.=$res;
    } 
    return $res;
}


$array = $this->input->post();
$array['periode_akhir'] = $array['tahun']."-".strmonth($array['bulan_akhir']);
$array['periode_awal'] = $array['tahun']."-".strmonth($array['bulan_awal']);
if($array['submit']==="excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAP_RL_".date('Ymd_His').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$array['periode_sebelumnya'] = month_before($array['periode_awal']);

$template = array(
        'table_open'            => '<table style="padding: 0px 2px;min-width: 850px;border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
$this->load->library('table');
if($array['rdjenis']==="konsol"){
    $caption = "<b>LAPORAN LABA (RUGI) KONSOLIDASI</b>";
}else{
    $caption = "<b>LAPORAN LABA (RUGI)</b>";
}
if($array['periode_awal']===$array['periode_akhir']){
    $caption .= "<br>"
            . "<b>". strtoupper($this->apps->title)." <br> ". strtoupper($rptakun['div'][0]['nmdiv'])."</b>"
            . "<br>"
            . "<b> PERIODE " .month_name($array['periode_awal']) ."</b>"
            . "<br><br>";
    
}else{
    $caption .= "<br>"
            . "<b>". strtoupper($this->apps->title)." <br> ". strtoupper($rptakun['div'][0]['nmdiv'])."</b>"
            . "<br>"
            . "<b> PERIODE " .month_name($array['periode_awal']) .' S/D '.month_name($array['periode_akhir']) ."</b>"
            . "<br><br>";
    
}

// Caption text
$this->table->set_caption($caption);
$header = array();
$sum_periode = array();
foreach ($rptakun['data'][0] as $k=>$v) {
    $sum_periode[$k] = '';
    if($k!=="level" && $k!=="kdakun"){
        if($k=="nmakun"){
            $h = array('data' => "PERIODE"
                            , 'style' => 'font-weight: bold;font-size: 12px;text-align: left;background-color:#dadada;');
        }else{
            $yyyy = substr($k, 0,4);
            $mm = substr($k, 4,2);
            if((int) $rptakun['periode_aktif']>(int) ($yyyy.$mm)){
                $h = array('data' => month_name($yyyy."-".$mm)
                                , 'style' => 'font-weight: bold;font-size: 12px;text-align: right;background-color:#dadada;');   
            }else{
                $h = array('data' => "*".month_name($yyyy."-".$mm)
                                , 'style' => 'font-weight: bold;font-size: 12px;text-align: right;background-color:#dadada;');
            }
        }
        $header[]=$h;    
    }
}
$this->table->add_row($header);   

foreach ($rptakun['data'] as $k=>$v) {
    $detail = array();
    foreach ($v as $vk=>$d){
        if($v['level']==1){
             $bold = "font-weight: bold;";
        }else{
             $bold = "";
        }     
        //echo strlen($v['kdakun'])."|".$v['kdakun']."<br>";
        if($array['rdjenis']==="pos"){
            if($v['level']==="1"){
                if(is_numeric($d) && is_numeric($vk)){
                    $sum_periode[$vk] = $sum_periode[$vk] + $d;
                }
            }   
        }else{
            if(strlen($v['kdakun'])===3){
                if(is_numeric($d) && is_numeric($vk)){
                    $sum_periode[$vk] = $sum_periode[$vk] + $d;
                    //echo $v['kdakun']." | ".$vk."<br>";
                }
            }   
        }        
        if($vk!=="level" && $vk!=="kdakun"){
            if(is_numeric($d)){
                if($array['submit']==="excel"){
                    $n = array('data' => (float) $d
                        , 'style' => $bold. 'text-align: right;'); 
                }else{
                    $n = array('data' => number_format($d, 2,",",".")
                        , 'style' => $bold. 'text-align: right;'); 
                }
            }else{
                if($array['submit']==="excel"){
                    $n = array('data' => $d
                        , 'style' => $bold. 'text-align: left;'); 
                }else{
                    $n = array('data' => str_replace(" ", "&nbsp;", $d)
                        , 'style' => $bold. 'text-align: left;'); 
                }
            }
            $detail[]=$n;    
        }
    }
    $this->table->add_row($detail);
   
}
unset($sum_periode['kdakun']);
unset($sum_periode['level']);
$sum_periode['nmakun'] = 'TOTAL LABA/(RUGI)';
$footer = array();
$info_colspan = 1;
foreach ($sum_periode as $f) {
    //var_dump($f);
    if(is_numeric($f)){
        if($array['submit']==="excel"){
            $ft = array('data' => (float) $f
                , 'style' => 'font-weight: bold;text-align: right;background-color:#dadada;'); 
        }else{
            $ft = array('data' =>number_format($f, 2,",",".")
                , 'style' => 'font-weight: bold;text-align: right;background-color:#dadada;'); 
        }
    }else{
        $ft = array('data' => $f
                , 'style' => 'font-weight: bold;text-align: left;background-color:#dadada;'); 
    }    
    $footer[] = $ft;     
    $info_colspan++;
}
$this->table->add_row($footer);
$info = array('data' => "* Belum Final"
                , 'colspan' => $info_colspan
                , 'style' => 'font-weight: bold;text-align: left;'); 
$this->table->add_row($info);
$this->table->set_template($template);
echo $this->table->generate();

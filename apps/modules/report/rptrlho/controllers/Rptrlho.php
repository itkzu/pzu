<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptrlho
 *
 * @author adi
 */
class Rptrlho extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptrlho/submit'),
            'add' => site_url('rptrlho/add'),
            'edit' => site_url('rptrlho/edit'),
            'reload' => site_url('rptrlho'),
        );
        $this->load->model('rptrlho_qry');
        for ($x = 1; $x <= 13; $x++) {
            $this->data['bulan'][$x] = strmonth($x).". ".month($x);
        } 
        
        for ($x = date('Y'); $x >= 2015; $x--) {
            $this->data['tahun'][$x] = $x;
        } 
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $this->data['rptakun'] = $this->rptrlho_qry->submit();
        if(empty($this->data['rptakun'])){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('debug/err_000',$this->data);
        }else{
            $array = $this->input->post();
            if($array['submit']){
                $this->template
                    ->title($this->data['msg_main'],$this->apps->name)
                    ->set_layout('print-layout')
                    ->build('html',$this->data);       
            }else{
                redirect("rptrlho");            
            }    
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            'bulan_awal'=> array(
                    'attr'        => array(
                        'id'    => 'bulan_awal',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['bulan'],
                    'value'    => $this->mm,
                    'name'     => 'bulan_awal',
            ),
            'bulan_akhir'=> array(
                    'attr'        => array(
                        'id'    => 'bulan_akhir',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['bulan'],
                    'value'    => $this->mm,
                    'name'     => 'bulan_akhir',
            ),
            'tahun'=> array(
                    'attr'        => array(
                        'id'    => 'tahun',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['tahun'],
                    'value'    => '',
                    'name'     => 'tahun',
            ),
        );
    }
}

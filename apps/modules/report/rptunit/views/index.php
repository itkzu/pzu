<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <div class="table-responsive">
                <table class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Kode</th>
                            <th style="text-align: center;">Tipe</th>
                            <th style="text-align: center;">Jenis</th>
                            <th style="text-align: center;">Series</th>
                            <th style="text-align: center;">Grup</th>
                            <th style="text-align: center;">CC</th>
                            <th style="text-align: center;">Harga Beli</th>
                            <th style="text-align: center;">H. Jual ON-TR</th>
                            <th style="text-align: center;">H. Jual OFF-TR</th>
                            <th style="text-align: center;">Cadangan BBN</th>
                            <th style="text-align: center;">No. SUT</th>
                            <th style="text-align: center;">No. TPT</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">Kode</th>
                            <th style="text-align: center;">Tipe</th>
                            <th style="text-align: center;">Jenis</th>
                            <th style="text-align: center;">Series</th>
                            <th style="text-align: center;">Grup</th>
                            <th style="text-align: center;">CC</th>
                            <th style="text-align: center;">Harga Beli</th>
                            <th style="text-align: center;">H. Jual ON-TR</th>
                            <th style="text-align: center;">H. Jual OFF-TR</th>
                            <th style="text-align: center;">Cadangan BBN</th>
                            <th style="text-align: center;">No. SUT</th>
                            <th style="text-align: center;">No. TPT</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        var column = [];

        column.push({ 
            "aTargets": [ 6,7,8,9 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
                // return formmatedvalue;
                },
            "sClass": "right"
        });

        table = $('.dataTable').DataTable({     
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( 
                            //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            //,{ "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(0)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptunit/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'potrait',
                    pageSize: 'legal'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>
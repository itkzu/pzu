<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbkluntns
 *
 * @author adi
 */
class Rptbkluntns extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptbkluntns/submit'),
            'add' => site_url('rptbkluntns/add'),
            'edit' => site_url('rptbkluntns/edit'),
            'reload' => site_url('rptbkluntns'),
        );
        $this->load->model('rptbkluntns_qry');
        
        $this->data['kddiv'] = array(
            "ZPH01.01B" => "AHMAD YANI",
            "ZPH01.02B.01" => "PATI",
            "ZPH02.01B" => "KUDUS",
            "ZPH03.01B" => "BREBES",
            "ZPH04.01B" => "SETIABUDI"
          );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {
        if($this->validate() == TRUE){ 
            $res = $this->rptbkluntns_qry->getdata_table();
            $this->data['decode'] = $res;
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('html',$this->data);  
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    public function json_dgview() {
        echo $this->rptbkluntns_qry->json_dgview();
    }

    public function getdata_table() {
        echo $this->rptbkluntns_qry->getdata_table();
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                    ),
                    'placeholder' => 'Cabang Bengkel',
                    'data'     => $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '', 
            ), 
            'periode'=> array(
                     'placeholder' => 'Periode',
                     'id'          => 'periode',
                     'name'        => 'periode',
                     'value'       => date('Y-m'),
                     'class'       => 'form-control',
                     'style'       => 'margin-left: 5px;',
                     'required'    => '',
            ),
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
           'ket'=> array( 
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'type'        => 'hidden',
            ),
        );
    }    

    private function validate() {
        $config = array(
            array(
                    'field' => 'kddiv',
                    'label' => 'Kddiv',
                    'rules' => 'required|max_length[20]',
                    ), 
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

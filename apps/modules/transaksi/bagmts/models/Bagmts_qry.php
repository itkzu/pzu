<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Bagmts_qry
 *
 * @author
 */

class Bagmts_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    } 

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_div_unit_bkl()");
        return $q->result_array();
    }

    public function getJenispekerjaan() {
        // if($this->input->post('jenis')==='JASA'){
            // $q = $this->db->query("select kdpart, nmpart from bkl.part where kdgrup in ('ASS1','ASS2','ASS4','CS','HR','JR','LR','LS','GANTI OLI PLUS','HEAVY REPAIR',
                                    // 'LIGHT REPAIR','PAKET LAIN','PAKET LENGKAP','PAKET RINGAN','PEKERJAAN LUAR','ASS 3','ASS 4','ASS 1','Paket Lengkap','Light Repair')");
        // } else {
            // $q = $this->db->query("select kdpart, nmpart from bkl.part where kdgrup not in ('ASS1','ASS2','ASS4','CS','HR','JR','LR','LS','GANTI OLI PLUS','HEAVY REPAIR',
                                    // 'LIGHT REPAIR','PAKET LAIN','PAKET LENGKAP','PAKET RINGAN','PEKERJAAN LUAR','ASS 3','ASS 4','ASS 1','Paket Lengkap','Light Repair')");
        // } 
         $q = $this->db->query("select kdpart, nmpart from bkl.part limit 1000");
        
                // echo $this->db->last_query();
        $res = $q->result_array();
        return json_encode($res);
    }

    //import
    public function get_SV() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }  
        }

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/pkb/read',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>'{
                "fromTime": "'.$rdate7.'",
                "toTime": "'.$rdate1.'",
                "dealerId":"'.$dealerId.'", 
                "noWorkOrder": ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=2xIeUBOQd_rxsx1i5cGlrYf8BRClAtK5gY9e9qZWweE-1699068563-0-AVdINXH+q+Y+VpHc9rH8Erur9TqjzMO72vhhwjU2A4wSqZET1PsUFa5zIynTRdHHHSNFdGh5TavTC6X7IGuGDZ8='
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response; 
        // echo $dealerId; 
    } 

    public function ins_SV() {    
        $info   = $this->input->post('info'); 
        $info   = str_replace("'","''",$info);
        $q = $this->db->query("insert into api.pkb_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        // echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function save_SV() { 
        $q = $this->db->query("select title,msg,tipe from api.in_pkb()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function select_data($nopkb){
        $query = $this->db->query("select * from api.v_pkb where noworkorder =  '".$nopkb."'");
        // echo $this->db->last_query();
        $res = $query->result_array();
        return $res;

    } 

    public function cekdata(){
        $nopkb = $this->input->post('nopkb');

        $query = $this->db->query("select nopkb from api.v_pkb where noworkorder =  '".$nopkb."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);

    } 

    public function set_SV(){
        $nopkb = $this->input->post('nopkb');

        $query = $this->db->query("select * from api.v_pkb where noworkorder =  '".$nopkb."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);

    } 

    public function set_nmpart(){
        $kdpart = $this->input->post('kdpart');

        $query = $this->db->query("select * from bkl.part where kdpart =  '".$kdpart."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);

    } 

    public function set_NT(){
        $nopkb = $this->input->post('nopkb');

        $query = $this->db->query("select * from bkl.tb_sv where nopkb =  '".$nopkb."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);

    } 

    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('noworkorder', 'tanggalservis', 'nopolisi', 'nomesin', 'norangka', 'namapembawa', 'dealerid', 'no');
        $sIndexColumn = "noworkorder";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
            
        if($this->session->userdata('data')['kddiv']==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.01B.01'){
            $dealerId = '10091'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }
        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, a.noworkorder, a.tanggalservis, a.nopolisi, a.nomesin, a.norangka, a.namapembawa, a.dealerid, a.nopkb
                              FROM api.v_pkb a JOIN api.v_pkb_d b on a.noworkorder = b.noworkorder where a.tanggalservis::date between (now() - interval '1 day')::date and now()::date and statusworkorder = '4' and (a.nopkb IS NULL OR a.nopkb::text = ''::text) group by a.noworkorder, a.tanggalservis, a.nopolisi, a.nomesin, a.norangka, a.namapembawa, a.dealerid, a.nopkb) AS a";
        } else {
            // echo $this->session->userdata('data')['kddiv'];
            $sTable = " ( SELECT '' as no, a.noworkorder, a.tanggalservis, a.nopolisi, a.nomesin, a.norangka, a.namapembawa, a.dealerid, a.nopkb
                            FROM api.v_pkb a JOIN api.v_pkb_d b on a.noworkorder = b.noworkorder where a.tanggalservis::date between (now() - interval '1 day')::date and now()::date and statusworkorder = '4' and (a.nopkb IS NULL OR a.nopkb::text = ''::text) and dealerid = '".$dealerId."' group by a.noworkorder, a.tanggalservis, a.nopolisi, a.nomesin, a.norangka, a.namapembawa, a.dealerid, a.nopkb) AS a";
        }

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noworkorder ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                }


                $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('bagmts/edit/'.$aRow['noworkorder'])."\">Edit</a>";

                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    }

    public function json_dgview_history() {
        error_reporting(-1);
        $aColumns = array('nopkb', 'nomor_nota_servis', 'tgl_nota_servis', 'no_polisi', 'nomor_mesin', 'nomor_rangka', 'nama_pembawa', 'no_ahass', 'no');
        $sIndexColumn = "nomor_nota_servis";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
            
        if($this->session->userdata('data')['kddiv']==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.01B.01'){
            $dealerId = '10091'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }
        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, a.nomor_nota_servis, a.tgl_nota_servis, a.no_polisi, a.nomor_mesin, a.nomor_rangka, a.nama_pembawa, a.no_ahass, a.nopkb
                            FROM bkl.tb_sv a JOIN bkl.tb_sv b on a.nomor_nota_servis = b.nomor_nota_servis where a.tgl_nota_servis::date between (now() - interval '1 day')::date and now()::date group by a.nomor_nota_servis, a.tgl_nota_servis, a.no_polisi, a.nomor_mesin, a.nomor_rangka, a.nama_pembawa, a.no_ahass, a.nopkb) AS a";
        } else {
            // echo $this->session->userdata('data')['kddiv'];
            $sTable = " ( SELECT '' as no, a.nomor_nota_servis, a.tgl_nota_servis, a.no_polisi, a.nomor_mesin, a.nomor_rangka, a.nama_pembawa, a.no_ahass, a.nopkb
                            FROM bkl.tb_sv a JOIN bkl.tb_sv b on a.nomor_nota_servis = b.nomor_nota_servis where a.tgl_nota_servis::date between (now() - interval '1 day')::date and now()::date and a.no_ahass = '".$dealerId."' group by a.nomor_nota_servis, a.tgl_nota_servis, a.no_polisi, a.nomor_mesin, a.nomor_rangka, a.nama_pembawa, a.no_ahass, a.nopkb) AS a";
        }

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by nomor_nota_servis desc";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                }

                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['nopkb']) ){
            $nopkb = $_GET['nopkb'];
        }else{
            $nopkb = '';
        }
        $aColumns = array('no', 'jenis', 'namapekerjaan', 'jenispekerjaan', 'partsnumber', 'kuantitas', 'harga', 'diskon', 'totalharga', 'noworkorder', 'idjob', 'uangmuka');
        $sIndexColumn = "noworkorder";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 
        $sTable = " ( SELECT '' as no, a.*
                              FROM api.in_sv('".$nopkb."','".$this->session->userdata('data')['kddiv']."') a) AS a"; 
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noworkorder ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                }

                $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['noworkorder']."','".$aRow['jenis']."','".$aRow['idjob']."','".$aRow['partsnumber']."','".$aRow['diskon']."','".$aRow['harga']."','".$aRow['totalharga']."','".$aRow['namapekerjaan']."','".$aRow['jenispekerjaan']."','".$aRow['kuantitas']."');\">Edit</button>";

                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    }

    public function batal() {     
        $nopkb = $this->input->post('nopkb');
        $q = $this->db->query("select * from api.btl_sv('".$nopkb."','".$this->session->userdata('data')['kddiv']."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function update() {      
        $nopkb  = $this->input->post('nopkb');  
        $disk   = $this->input->post('disk'); 
        $total  = $this->input->post('total');
        $harga  = $this->input->post('harga');  
        $jenis  = $this->input->post('jenis');  
        $idjob  = $this->input->post('idjob');  
        $partsnumber  = $this->input->post('partsnumber');  
        $nmkerja  = $this->input->post('nmkerja');   

            $q = $this->db->query("update api.pkb_d set diskon = ".$disk.", harga = ".$harga." , totalharga = ".$total." where noworkorder = '".$nopkb."' and kddiv = '".$this->session->userdata('data')['kddiv']."' and idjob = '".$idjob."' and jenis = '".$jenis."' and partsnumber = '".$partsnumber."'");
            if (!$q) { 
                $this->state = "0";
            } else { 
                $this->state = "1";
            }   
        return json_encode($this->state);  
    }

    public function submit() {      
        $nopkb  = $this->input->post('nopkb');  
        $disk   = $this->input->post('disk'); 
        $total  = $this->input->post('total');  
        $jenis  = $this->input->post('jenis');  
        $idjob  = $this->input->post('idjob');  

        $q = $this->db->query("select * from api.sv_ins('".$nopkb."','".$this->session->userdata('data')['kddiv']."','".$this->session->userdata('username')."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    //cetak
    public function ctk_nt($nopkb) {
        $q = $this->db->query("SELECT * FROM bkl.vb_ntsv_jasa where nopkb = upper('".$nopkb."') ");
        // $this->db->select("to_char(now(),'HH:MI:SS') as wkt, *");
        // $this->db->where('nodo',upper('.$nodo.'));
        // $q = $this->db->get("pzu.vb_do");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function ctk_nt2($nopkb) {
        $q = $this->db->query("SELECT * FROM bkl.vb_ntsv_parts where nopkb = upper('".$nopkb."') ");
        // $this->db->select("to_char(now(),'HH:MI:SS') as wkt, *");
        // $this->db->where('nodo',upper('.$nodo.'));
        // $q = $this->db->get("pzu.vb_do");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }
}

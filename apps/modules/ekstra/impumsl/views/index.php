<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data');
            echo form_open($submit,$attributes); 
        ?> 
          <div class="box-body">
              <div class="row">
                  <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputFile">Pilih File UMSL <small>Bisa pilih file lebih dari satu</small></label>
                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="csvfile[]" id="csvfile" multiple="">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                          </div>
                        </div>   
                  </div>
                  <div class="col-lg-12">
                      <?php                    
                        $tbumsl = $this->session->userdata('tbumsl');
                        $tbumsl_d = $this->session->userdata('tbumsl_d');
                        $this->session->unset_userdata('tbumsl');
                        $this->session->unset_userdata('tbumsl_d');
//                        var_dump($tbumsl_d);
                        if($tbumsl){
                            $template = array(
                                    'table_open'            => '<table class="table table-bordered table-hover">',

                                    'thead_open'            => '<thead>',
                                    'thead_close'           => '</thead>',

                                    'heading_row_start'     => '<tr>',
                                    'heading_row_end'       => '</tr>',
                                    'heading_cell_start'    => '<th>',
                                    'heading_cell_end'      => '</th>',

                                    'tbody_open'            => '<tbody>',
                                    'tbody_close'           => '</tbody>',

                                    'row_start'             => '<tr>',
                                    'row_end'               => '</tr>',
                                    'cell_start'            => '<td>',
                                    'cell_end'              => '</td>',

                                    'row_alt_start'         => '<tr>',
                                    'row_alt_end'           => '</tr>',
                                    'cell_alt_start'        => '<td>',
                                    'cell_alt_end'          => '</td>',

                                    'table_close'           => '</table>'
                            );                            
                            $this->load->library('table');
                            $caption = "<b>Import Data UMSL</b>";

                            // Caption text
                            $this->table->set_caption($caption);    
                            foreach ($tbumsl as $rows) {
                                $namaheader = array(
                                    array('data' => '<div class="col-lg-2">No. UMSL : '.$rows['noumsl'].'</div>'
                                                    . '<div class="col-lg-2">Tgl. SL : '.$this->apps->dateConvert($rows['tglumsl']).'</div>'
                                                    . '<div class="col-lg-2">No. UMDO : '.$rows['noumdo'].'</div>'
                                                    . '<div class="col-lg-6">File Name : '.$rows['nmfile'].'</div>'
                                                        , 'colspan' => 6
                                                        , 'style' => ''), 
                                );
                                // Header detail
                                $this->table->add_row($namaheader);                                 
                                $detail_h = array(
                                    array('data' => '<b>No.</b>'
                                                        , 'style' => 'text-align: center; width: 100px;'), 
                                    array('data' => '<b>Kode</b>'
                                                        , 'style' => 'text-align: center; width: 10px;'), 
                                    array('data' => '<b>Kd. Warna</b>'
                                                        , 'style' => 'text-align: center; width: 80px;'),  
                                    array('data' => '<b>No. Mesin</b>'
                                                        , 'style' => 'text-align: center;'),
                                    array('data' => '<b>No. Rangka</b>'
                                                        , 'style' => 'text-align: center;'), 
                                    array('data' => '<b>Tahun</b>'
                                                        , 'style' => 'text-align: center; width: 10px;'), 
                                );
                                $this->table->add_row($detail_h);
                                foreach ($tbumsl_d as $row_d) {
                                    if($row_d['noumsl']===$rows['noumsl']){
                                        $detail = array(
                                            array('data' => $row_d['nourut']
                                                                , 'style' => 'text-align: center; width: 100px;'), 
                                            array('data' => $row_d['kode']
                                                                , 'style' => 'text-align: center; width: 10px;'), 
                                            array('data' => $row_d['kdwarna']
                                                                , 'style' => 'text-align: center; width: 80px;'),  
                                            array('data' => $row_d['nosin']
                                                                , 'style' => 'text-align: center;'),
                                            array('data' => $row_d['nora']
                                                                , 'style' => 'text-align: center;'), 
                                            array('data' => $row_d['tahun']
                                                                , 'style' => 'text-align: center; width: 10px;'), 
                                        );
                                        $this->table->add_row($detail);
                                        
                                    }
                                }
                            }
                            $this->table->set_template($template);
                            echo $this->table->generate();    
                        }
                      ?>
                  </div>
              </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" name="submit" value="html">
                <i class="fa fa-cloud-upload"></i> Upload
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>    
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        
    });
</script>
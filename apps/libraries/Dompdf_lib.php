<?php

class Dompdf_lib {

	var $_dompdf = NULL;

	function __construct()
	{
		require_once APPPATH.'third_party/dompdf/dompdf_config.inc.php';
		if(is_null($this->_dompdf)){
			$this->_dompdf = new DOMPDF();
		}
	}
  /**
   * Get an instance of CodeIgniter
   *
   * @access    protected
   * @return    void
   */
  protected function ci()
  {
      return get_instance();
  }
  /**
   * Load a CodeIgniter view into domPDF
   *
   * @access    public
   * @param    string    $view The view to load
   * @param    array    $data The view data
   * @return    void
   */

	function convert_html_to_pdf($html, $filename ='', $stream = TRUE)
	{
		$this->_dompdf->load_html($html);
		$this->_dompdf->render();
		if ($stream) {
			$this->_dompdf->stream($filename);
		} else {
			return $this->_dompdf->output();
		}
	}
 public function load_view($view, $data = array()){
     $html = $this->ci()->load->view($view, $data, TRUE);
     $this->_dompdf->load_html($html);
     // Render the PDF
     $this->_dompdf->render();
         // Output the generated PDF to Browser
            $this->_dompdf->stream($this->filename, array("Attachment" => false));
 }

}
?>

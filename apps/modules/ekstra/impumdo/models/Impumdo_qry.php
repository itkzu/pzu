<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Impumdo_qry
 *
 * @author adi
 */
class Impumdo_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }
    
    public function submit() {
        try {
            $this->load->library('CSVReader');
            if( isset( $_FILES['csvfile'] ) == TRUE && !empty($_FILES['csvfile']['name'][0])){
                //$files = array_filter($_FILES['upload']['name']); something like that to be used before processing files.
                // Count # of uploaded files in array
                $total = count($_FILES['csvfile']['name']);

                // Loop through each file
                $rows = array();
                $sess_umdo = array();
                $this->db->trans_begin();
                for($i=0; $i<$total; $i++) {
                  //Get the temp file path
                  $tmpFilePath = $_FILES['csvfile']['tmp_name'][$i];
                  //Make sure we have a filepath
                  if ($tmpFilePath != ""){
                    //Setup our new file path
                    $newFilePath = './files/hso/' . $_FILES['csvfile']['name'][$i];
                    //Upload the file into the temp dir
                    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                        //Handle other code here
                        $filename = $_FILES['csvfile']['name'][$i];
                        $csvData = $this->csvreader->parse_file($newFilePath,false); //path to csv file
                        $path_parts = pathinfo($newFilePath);
                        if(strpos($filename, "(")===false && $path_parts['extension']==="UMDO"){
                            // Jika dalam file ada tanda kurung maka dianggap file ganda dan tidak akan di proses
                            if($csvData){
                                $rows = $this->pharseData($filename, $csvData);
                                if($rows){
                                    $res = $this->db->insert('t_umdo',$rows);
                                    if($res==FALSE){
                                        $this->db->trans_rollback();
                                        $err = $this->db->error();
                                        unlink($newFilePath);
                                        throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                                    }  
                                    $sess_umdo[] = $rows;
                                }
                                else{
                                    $res = false;
                                    $sess_umdo = null;
                                    $this->db->trans_rollback();
                                    unlink($newFilePath);
                                    throw new Exception(" Error : ". "Data file CSV Sudah ada didatabase"
                                            . " , transaksi dibatalkan."
                                            . " <br>Reff File : " . $filename);
                                }
                            }
                            else{
                                unlink($newFilePath);
                                throw new Exception("Isi File CSV Kosong, Silahkan Cek Kembali");
                            }    
                        }else{
                            unlink($newFilePath);
                            throw new Exception("Error : Tidak ada data yang di simpan, karena file salah atau gagal di simpan ke database"
                                    . " <br>Reff File : Wrong File!");

                        }
                    }
                    unlink($newFilePath);
                  }
                }     
                if($res!==FALSE){
                    $this->db->trans_commit();
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                    $this->session->set_userdata('tbumdo', $sess_umdo);
                }
            }else{
                $this->res = " Error : ". "Data file CSV masih kosong"
                                . " , transaksi dibatalkan."
                                . " Reff File : Empty File!";
                $this->state = "0";
            }
        }catch (Exception $e) {     
            $this->db->trans_rollback();
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_userdata('statsubmit', json_encode($arr));
        return $arr;
    }
    
    private function pharseData($filename, $array) {
        $data = array();
        foreach ($array as $value) {
            $dd = substr($value[1],0,2);
            $mm = substr($value[1],2,2);
            $yyyy = substr($value[1],4,4);
            $date = $yyyy."-".$mm."-".$dd;
            $data['noumdo'] = $value[0];
            $data['nopo'] = $value[8];
            $data['nmfile'] = $filename;
            $data['userentry'] = $this->session->userdata("username");
            $data['tglentry'] = date("Y-m-d H:i:s");
            $data['tglpo'] = $date;
        }
        $query = $this->db->get_where('t_umdo', array('noumdo' => $data['noumdo']));
        if($query->num_rows()>0){
            return false;
        }else{
            return $data;   
        }         
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
</style>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->
                <div class="box-header">
                    <button type="button" class="btn btn-primary btn-submit">
                       Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                       Batal
                    </button>
                </div>
                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <?php echo form_label($form['nopo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-2">
                                    <?php
                                        echo form_input($form['nopo']);
                                        echo form_error('nopo','<div class="note">','</div>');
                                    ?>
                                </div>

                                <div class="col-xs-2 ">
                                    <?php echo form_label($form['nmsup']['placeholder']); ?>
                                </div>

                                <div class="col-xs-4 ">
                                    <?php
                                        echo form_input($form['nmsup']);
                                        echo form_error('nmsup','<div class="note">','</div>');
                                    ?>
                                </div>  
                            </div>
                        </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tglpo']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <?php
                                                echo form_input($form['tglpo']);
                                                echo form_error('tglpo','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php echo form_label($form['tipebayar']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-3 ">
                                            <?php
                                                echo form_input($form['tipebayar']);
                                                echo form_error('tipebayar','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tgltrm']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <?php
                                                echo form_input($form['tgltrm']);
                                                echo form_error('tgltrm','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php echo form_label($form['jth_tmp']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-1 ">
                                            <?php
                                                echo form_input($form['jth_tmp']);
                                                echo form_error('jth_tmp','<div class="note">','</div>');
                                            ?>
                                        </div> 

                                        <div class="col-xs-1 ">
                                            <?php echo form_label($form['tgltmp']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['tgltmp']);
                                                echo form_error('tgltmp','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php
                                                echo form_input($form['nourut']);
                                                echo form_error('nourut', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label> Detail Shipping List </label>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">Qty</th>
                                                    <th style="text-align: center;">Kode</th>
                                                    <th style="text-align: center;">Tipe Unit</th> 
                                                    <th style="text-align: center;">Nama Unit</th> 
                                                    <th style="text-align: center;">Warna</th> 
                                                    <th style="text-align: center;">Harga Per Unit</th> 
                                                    <th style="text-align: center;">Total Harga</th> 
                                                    <th style="text-align: center;">Edit</th> 
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;">Edit</th>  
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-1">
                                            <div class="form-group"> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-1 ">
                                            <div class="form-group"> 
                                                <?php echo form_label($form['sub_tot']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['sub_tot']);
                                                echo form_error('sub_tot','<div class="note">','</div>');
                                            ?>
                                        </div> 

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['potongan']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['potongan']);
                                                echo form_error('potongan','<div class="note">','</div>');
                                            ?>
                                        </div>  

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['dpp']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['dpp']);
                                                echo form_error('dpp','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-4">
                                            <div class="form-group"> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-2">
                                            <div class="form-group"> 
                                                <?php echo form_label($form['disc_top']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['disc_top']);
                                                echo form_error('disc_top','<div class="note">','</div>');
                                            ?>
                                        </div>  

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['ppn']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['ppn']);
                                                echo form_error('ppn','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-6">
                                            <div class="form-group"> 
                                                <?php
                                                    echo form_input($form['banner']);
                                                    echo form_error('banner','<div class="note">','</div>');
                                                ?>
                                            </div>
                                        </div>  

                                        <div class="col-xs-2">
                                            <div class="form-group"> 
                                                <?php echo form_label($form['total']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <?php
                                                echo form_input($form['total']);
                                                echo form_error('total','<div class="note">','</div>');
                                            ?>
                                        </div>    
                                    </div>
                                </div>  
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
        </form>
        <!-- /.box -->
    </div>
</div> 

<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Kas/Bank Bengkel</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6"> 
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['kode']['placeholder']);
                                            echo form_input($form['kode']);
                                            echo form_error('kode','<div class="note">','</div>');
                                          ?>
                                    </div> 
                            </div>
                            <div class="col-xs-6"> 
                                    <div class="form-group">
                                        <?php 
                                            echo form_input($form['kdwarna']);
                                            echo form_error('kdwarna','<div class="note">','</div>');
                                          ?>
                                    </div> 
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kdtipe']['placeholder']);
                                    echo form_input($form['kdtipe']);
                                    echo form_error('kdtipe','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                      echo form_label($form['nmtipe']['placeholder']);
                                    echo form_input($form['nmtipe']);
                                    echo form_error('nmtipe','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['qty']['placeholder']);
                                    echo form_input($form['qty']);
                                    echo form_error('qty','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['harga']['placeholder']);
                                    echo form_input($form['harga']);
                                    echo form_error('harga','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div>
    <?php echo form_close(); ?>
</div>
 

<script type="text/javascript">
    $(document).ready(function () { 
        reset();
        autoNum();  
        sum();  

        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 6,7 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "qty" },
                { "data": "kode" },
                { "data": "kdtipe"},
                { "data": "nmtipe" },
                { "data": "warna"},
                { "data": "harga"},
                { "data": "totharga"},
                { "data": "edit"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nopo", "value": nopo});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('fkpo/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                $('#sub_tot').autoNumeric('set',total); 
                sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });  

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });

        $('.btn-set').click(function(){
            $('.btn-set').hide();
            $('.btn-add').attr('disabled',false);
            $('#stathrg').attr('disabled',true);
            if ($("#stathrg").prop("checked")){
              $('#harga').prop('readonly',false);
            } else {
              $('#harga').prop('readonly',true);
            }

        });


        $('.btn-add').click(function(){

        	  getKodefkpo();
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kdaks').select2({
                placeholder: '-- Pilih Barang --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            clear();
        });

        $('.btn-simpan').click(function(){
            addDetail(); 
        }); 

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){ 
              submit(); 
        });
    });

    function autoNum(){
        $('#sub_tot').autoNumeric('init',{ 	aSep: '.' , aDec: ',' });
        $("#potongan").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#disc_top").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#dpp").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#ppn").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#total").autoNumeric('init',{    aSep: '.' , aDec: ',' }); 
        $("#harga").autoNumeric('init',{ 	aSep: '.' , aDec: ',' }); 
    }

    function reset(){
        $('#sub_tot').val('');
        $('#potongan').val('');
        $('#disc_top').val('');
        $('#dpp').val('');
        $('#ppn').val('');
        $('#total').val('');
        $('#kode').val('');
        $('#kdtipe').val('');
        $('#nmtipe').val(''); 
        $('#qty').val(''); 
        $('#harga').val(''); 
        $('#nourut').val(''); 
    }


    function dateback(tgl) {
        var less = $("#jth_tmp").val();
        var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
        var day = new_date.format('DD');
        var month = new_date.format('MM');
        var year = new_date.format('YYYY');
        var res = day + '-' + month + '-' + year;
        return res;
    }

    function sum(){
        var sub_tot = $('#sub_tot').autoNumeric('get');
        var potongan = $('#potongan').autoNumeric('get');
        var disc_top = $('#disc_top').autoNumeric('get');
        var tglpo = $('#tglpo').val();
        // var tglpo = tglpo.substring(6,10)+tglpo.substring(3,5)+tglpo.substring(0,2); 
        // replaceAll('-','')); 
        if(sub_tot===''){
            $('#sub_tot').autoNumeric('set',0);
            var sub_tot = 0;
        }
        if(potongan===''){
            $('#potongan').autoNumeric('set',0);
            var potongan = 0;
        }
        if(disc_top===''){
            $('#disc_top').autoNumeric('set',0);
            var disc_top = 0;
        }
        var dpp = parseFloat(sub_tot) - parseFloat(disc_top) - parseFloat(potongan);
        if(!isNaN(dpp)){
            $('#dpp').autoNumeric('set',dpp);
        } else {
            $('#dpp').autoNumeric('set',0);
            var dpp = 0;
        }
        if(tglpo > '2022-03-31'){
            var ppn = parseFloat(dpp) * 0.11;
            if(!isNaN(ppn)){
                $('#ppn').autoNumeric('set',ppn);
            } else {
                $('#ppn').autoNumeric('set',0);
                var ppn = 0;
            }
        } else {
            var ppn = parseFloat(dpp) * 0.1;
            if(!isNaN(ppn)){
                $('#ppn').autoNumeric('set',ppn);
            } else {
                $('#ppn').autoNumeric('set',0);
                var ppn = 0;
            }
        }

        var total = parseFloat(dpp) + parseFloat(ppn);
        if(!isNaN(total)){
            $('#total').autoNumeric('set',total);
        } else {
            $('#total').autoNumeric('set',0);
            var total = 0;
        }

        var terbilang = penyebut(total);
        $('#banner').val(terbilang);
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 

    function clear(){
        $("#nourut").val('');
    } 

    function edit(nopo,kode,kdtipe,nmtipe,qty,harga,kdwarna){

        $('#modal_transaksi').modal('toggle');
        $("#kdwarna").val(kdwarna);
        // $("#nourut").val(nourut);

        $("#kode").val(kode);
        $("#kdtipe").val(kdtipe); 
        $("#nmtipe").val(nmtipe); 
        $("#qty").val(qty);  
        $("#harga").autoNumeric('set',harga);
//         $("#harga").autoNumeric('set',harga);

//         $('#modal_transaksi').modal('toggle');
    }


    function addDetail(){

        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();  

        var nourut = $("#nourut").val();
        var kode = $("#kode").val();
        var kdwarna = $("#kdwarna").val();
        var harga = $("#harga").autoNumeric('get'); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("fkpo/addDetail");?>",
            data: {"nopo":nopo
                    ,"kode":kode 
                    ,"kdwarna":kdwarna 
                    ,"harga":harga },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                var obj = jQuery.parseJSON(resp); 
                        refresh();  
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(nofkpo,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("fkpo/detaildeleted");?>",
                data: {"nofkpo":nofkpo ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){

        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();   
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("fkpo/batal");?>",
                data: {"nopo":nopo },
                success: function(resp){ 
                    var obj = jQuery.parseJSON(resp);
                    if(obj.state==='1'){
                        swal({
                            title: 'Faktur Berhasil dibatalkan',
                            text: obj.msg,
                            type: 'success'
                        }, function(){
                            window.location.href = '<?=site_url('fkpo');?>';
                        }); 
                    }else{ 
                        swal({
                            title: 'Faktur Gagal dibatalkan',
                            text: obj.msg,
                            type: 'error'
                        }); 
                    } 
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    refresh();
                }
            });
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () { 
            var po = $("#nopo").val();
            var nopo = po.replace("-","");
            var nopo = nopo.replace("-","");
            var nopo = nopo.toUpperCase();   

            var sub_tot = $("#sub_tot").autoNumeric('get');
            var potongan = $("#potongan").autoNumeric('get');
            var disc_top = $("#disc_top").autoNumeric('get'); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("fkpo/submit");?>",
                data: {"nopo":nopo
                    ,"sub_tot":sub_tot
                    ,"potongan":potongan
                    ,"disc_top":disc_top },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('fkpo');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

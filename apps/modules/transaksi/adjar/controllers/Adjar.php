<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Adjar
 *
 * @author adi
 */
class Adjar extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('adjar/submit'),
            'add' => site_url('adjar/add'),
            'edit' => site_url('adjar/edit'),
            'reload' => site_url('adjar'),
        );
        $this->load->model('adjar_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function getNoID() {
        echo $this->adjar_qry->getNoID();
    }

    public function set_nodo() {
        echo $this->adjar_qry->set_nodo();
    }

    public function set_noaradj() {
        echo $this->adjar_qry->set_noaradj();
    }

    public function set_harga() {
        echo $this->adjar_qry->set_harga();
    }

    public function getjenis() {
        echo $this->adjar_qry->getjenis();
    }

    public function simpan() {
        echo $this->adjar_qry->simpan();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'noaradj'=> array(
                    'placeholder' => 'No. Adjustment',
                    //'type'        => 'hidden',
                    'id'          => 'noaradj',
                    'name'        => 'noaradj',
                    'value'       => set_value('noaradj'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglaradj'=> array(
                    'placeholder' => 'Tanggal Adjustment',
                    'id'          => 'tglaradj',
                    'name'        => 'tglaradj',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Nama Pemesan',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nama_s'=> array(
                    'placeholder' => 'Nama. STNK',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'alamat'=> array(
                     'placeholder' => 'Alamat STNK',
                     'id'          => 'alamat',
                     'name'        => 'alamat',
                     'value'       => set_value('alamat'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'jnsbayar'=> array(
                    'placeholder' => 'Tunai/Kredit',
                    'value'       => set_value('jnsbayar'),
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'kdjns'=> array( 
                    'value'       => set_value('kdjns'),
                    'id'          => 'kdjns',
                    'name'        => 'kdjns',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'jenis'=> array(
                    'attr'        => array(
                        'id'    => 'jenis',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('jenis'),
                    'name'     => 'jenis',
                    'placeholder' => 'Jenis AR',
                    'required' => ''
            ), 
            'nilai_ar'=> array(
                    'placeholder' => 'Total AR',
                    'value'       => set_value('nilai_ar'),
                    'id'          => 'nilai_ar',
                    'name'        => 'nilai_ar',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
            'nilai_byr'=> array(
                    'placeholder' => 'Total Pembayaran',
                    'value'       => set_value('nilai_byr'),
                    'id'          => 'nilai_byr',
                    'name'        => 'nilai_byr',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
            'nilai_adj'=> array(
                    'placeholder' => 'Nilai Penyesuaian',
                    'value'       => set_value('nilai_adj'),
                    'id'          => 'nilai_adj',
                    'name'        => 'nilai_adj',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-align: right;',
                    // 'readonly'    => '',
            ),
            'saldo'=> array(
                    'placeholder' => 'Saldo AR',
                    'value'       => set_value('saldo'),
                    'id'          => 'saldo',
                    'name'        => 'saldo',
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'status'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('status'),
                    'id'          => 'status',
                    'name'        => 'status',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'type'        => 'hidden',
            ),
        );
    }
    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Transalesin
 *
 * @author adi
 */
class Transalesin extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('transalesin/submit'),
            'add' => site_url('transalesin/add'),
            'edit' => site_url('transalesin/edit'),
            'reload' => site_url('transalesin'),
        );
        $this->load->model('transalesin_qry');
        $kddiv = $this->transalesin_qry->getKddiv();
        require_once APPPATH.'libraries/PHPExcel.php';
        $this->data['kddiv'][''] = '-- PILIH POS/CABANG --';
        if($kddiv){
            foreach ($kddiv as $value) {
                $this->data['kddiv'][$value['kddiv']] = $value['nmdiv']." [".$value['alamat']."]";
            }
        }
        $this->data['statsales'] = array(
            "" => "-- PILIH JABATAN --",
            "KP" => "KEPALA POS",
            "SV" => "SUPERVISOR",
            "CO" => "KOORDINATOR",
            "CS" => "SALES COUNTER",
            "SL" => "SALES",
//            "KC" => "KEPALA CABANG",
//            "KW" => "KEPALA WILAYAH",
        );
        $this->data['faktif'] = array(
            "t" => "YA",
            "f" => "TIDAK",
        );
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){  
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function importnik() {
        if(isset($_FILES["file"]["name"])){
            $data = $this->Pushnik();

            $kddiv = $this->input->post('kddiv');
            // $res = $this->imptrxbkl_qry->insertBeli($data,$kddiv);
            echo $this->transalesin_qry->insertNik(json_decode($data),$kddiv);  
        }
    }

    public function Pushnik() {
        //if(isset($_FILES["file"]["name"])){
        $path = $_FILES["file"]["tmp_name"];
        $object = PHPExcel_IOFactory::load($path);
        foreach($object->getWorksheetIterator() as $worksheet){
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();

            for($row=2; $row<=$highestRow; $row++){
                $nik               = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                $nama             = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $cabang         = $worksheet->getCellByColumnAndRow(2, $row)->getValue(); 

                if(empty($nama)){  //jika kode tidak null -> simpan

                    $data = 'empty';
                } else {
                    //jika deskripsi, tipe, harga = null
                    if(empty($nik)){$nik = '';}
                    if(empty($nama)){$nama = '';}
                    if(empty($cabang)){$cabang = '';}  

                    $data[] = array(
                        'nik'               => $nik, 
                        'nama'              => $nama,
                        'cabang'            => $cabang,
                    );
                }
            }
            return json_encode($data);
        }
    }
    
    public function json_dgview() {
        echo $this->transalesin_qry->json_dgview();
    }
    
    public function getSalesheader() {
        echo $this->transalesin_qry->getsalesheader();
    }
    
    public function getSalesHeaderDivisi() {
        echo $this->transalesin_qry->getSalesHeaderDivisi();
    }
    
    public function submit() {  
        $kdsales = $this->input->post('kdsales');
        $stat = $this->input->post('stat');
        if($this->validate($kdsales,$stat) == TRUE){
            $res = $this->transalesin_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($kdsales)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($kdsales);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($kdsales)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_init_edit();
                $this->_check_id($kdsales);
                $this->template->build('form', $this->data);
            }
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'kdsales'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kdsales',
                    'name'        => 'kdsales',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'kdsaleshd'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Sales Header',
                    'id'      => 'kdsaleshd',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nik'=> array(
                    'placeholder' => 'Nomor Induk Karyawan',
                    'id'          => 'nik',
                    'name'        => 'nik',
                    'value'       => set_value('nik'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;', 
            ),
           'nmlengkap'=> array(
                    'placeholder' => 'Nama Lengkap Sales',
                    'id'          => 'nmlengkap',
                    'name'        => 'nmlengkap',
                    'value'       => set_value('nmlengkap'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'nmsales'=> array(
                    'placeholder' => 'Nama Cetak Sales',
                    'id'      => 'nmsales',
                    'name'        => 'nmsales',
                    'value'       => set_value('nmsales'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'status'=> array(
                    'attr'        => array(
                        'id'    => 'status',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['statsales'],
                    'value'    => set_value('status'),
                    'name'     => 'status',
                    'required'    => '',
            ),
            'kdsales_header'=> array(
                    'attr'        => array(
                        'id'    => 'kdsales_header',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdsales_header'),
                    'name'     => 'kdsales_header',
                    'required'    => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                        'style' => 'width:100%;'
                    ),
                    'data'     => $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '',
            ),
            'faktif'=> array(
                    'attr'        => array(
                        'id'    => 'faktif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['faktif'],
                    'value'    => set_value('faktif'),
                    'name'     => 'faktif',
                    'required'    => '',
            ),
           'tgl_in'=> array(
                    'placeholder' => 'Tanggal Mulai Masuk Sales',
                    'id'      => 'tgl_in',
                    'name'        => 'tgl_in',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
        );
    }
    
    private function _init_edit($kdsales = null){
        if(!$kdsales){
            $kdsales = $this->uri->segment(3);
        }
        $this->_check_id($kdsales);
        $this->data['form'] = array(
           'kdsales'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kdsales',
                    'name'        => 'kdsales',
                    'value'       => $this->val[0]['kdsales'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
           'kdsaleshd'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Sales Header',
                    'id'      => 'kdsaleshd',
                    'value'       => $this->val[0]['kdsales_header'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nik'=> array(
                    'placeholder' => 'Nomor Induk Karyawan',
                    'id'          => 'nik',
                    'name'        => 'nik',
                    'value'       => $this->val[0]['nik'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;', 
            ),
           'nmlengkap'=> array(
                    'placeholder' => 'Nama Lengkap Sales',
                    'id'          => 'nmlengkap',
                    'name'        => 'nmlengkap',
                    'value'       => $this->val[0]['nmlengkap'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'nmsales'=> array(
                    'placeholder' => 'Nama Cetak Sales',
                    'id'      => 'nmsales',
                    'name'        => 'nmsales',
                    'value'       => $this->val[0]['nmsales'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'status'=> array(
                    'attr'        => array(
                        'id'    => 'status',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['statsales'],
                    'value'    => $this->val[0]['status'],
                    'name'     => 'status',
                    'required'    => '',
            ),
            'kdsales_header'=> array(
                    'attr'        => array(
                        'id'    => 'kdsales_header',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => $this->val[0]['kdsales_header'],
                    'name'     => 'kdsales_header',
                    'required'    => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kddiv'],
                    'value'    => $this->val[0]['kddiv'],
                    'name'     => 'kddiv',
                    'required'    => '',
            ),
            'faktif'=> array(
                    'attr'        => array(
                        'id'    => 'faktif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['faktif'],
                    'value'    => $this->val[0]['faktif'],
                    'name'     => 'faktif',
                    'required'    => '',
            ),
           'tgl_in'=> array(
                    'placeholder' => 'Tanggal Mulai Masuk Sales',
                    'id'      => 'tgl_in',
                    'name'        => 'tgl_in',
                    'value'       => $this->val[0]['tgl_in'],
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
        );
    }
    
    private function _check_id($kdsales){
        if(empty($kdsales)){
            redirect($this->data['add']);
        }
        
        $this->val= $this->transalesin_qry->select_data($kdsales);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($kdsales,$stat) {
        if(!empty($kdsales) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nmlengkap',
                    'label' => 'Nama Lengkap Sales',
                    'rules' => 'required|max_length[100]',
                ),
            array(
                    'field' => 'nmsales',
                    'label' => 'Nama Cetak Sales',
                    'rules' => 'required|max_length[100]',
                ),
            array(
                    'field' => 'status',
                    'label' => 'Jabatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kdsales_header',
                    'label' => 'Nama Atasan/SPV',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kddiv',
                    'label' => 'Nama POS/Cabang',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgl_in',
                    'label' => 'Tanggal Masuk Sales',
                    'rules' => 'required',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

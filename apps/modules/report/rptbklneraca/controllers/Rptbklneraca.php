<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbklneraca
 *
 * @author adi
 */
class Rptbklneraca extends MY_Controller {
	protected $data = '';
	protected $val = '';
	public function __construct()
	{
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,

			'submit' => site_url('rptbklneraca/submit'),
			'add' => site_url('rptbklneraca/add'),
			'edit' => site_url('rptbklneraca/edit'),
			'reload' => site_url('rptbklneraca'),
		);
		$this->load->model('rptbklneraca_qry');
		$cabang = $this->rptbklneraca_qry->getDataCabang();
		foreach ($cabang as $value) {
			$this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
		}
        for ($x = 1; $x <= 13; $x++) {
          $this->data['bulan'][$x] = strmonth($x).". ".month($x);
        }

        for ($x = date('Y'); $x >= 2015; $x--) {
          $this->data['tahun'][$x] = $x;
        }
	}

	//redirect if needed, otherwise display the user list

	public function index(){
		$this->_init_add();
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('index',$this->data);
	}

	public function submit() {
		$this->data['rptakun'] = $this->rptbklneraca_qry->submit();

		// echo "<script> console.log('PHP: ". json_encode($this->data) ."');</script>";


		if(empty($this->data['rptakun']['mtd'])){
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('debug/err_000',$this->data);
		}else{
			$array = $this->input->post();
			if($array['submit']==="html"){
				$this->template
					->title($this->data['msg_main'],$this->apps->name)
					->set_layout('print-layout')
					->build('html',$this->data);
			}elseif($array['submit']==="excel"){
				$this->load->view('excel',$this->data);
			}else{
				redirect("rptbklneraca");
			}
		}

	}

	private function _init_add(){
		$this->data['form'] = array(
			'kddiv'=> array(
					'attr'        => array(
						'id'    => 'kddiv',
						'class' => 'form-control select',
					),
					'data'        => $this->data['kddiv'],
					'value'       => '',
					'name'        => 'kddiv',
					'placeholder' => 'Cabang',
			),
			'nmdiv'=> array(
					'placeholder' => 'Nama Divisi',
					'type'        => 'hidden',
					'id'          => 'nmdiv',
					'name'        => 'nmdiv',
					'value'       => set_value('nmdiv'),
					'class'       => 'form-control',
					'required'    => '',
			),
			'periode_awal'=> array(
					'placeholder' => 'Periode',
					'id'          => 'periode_awal',
					'name'        => 'periode_awal',
					'value'       => $this->yyyymm,
					'class'       => 'form-control',
			),
			'periode_akhir'=> array(
					'placeholder' => 'Periode Akhir',
					'id'          => 'periode_akhir',
					'name'        => 'periode_akhir',
					'value'       => $this->yyyymm,
					'class'       => 'form-control',
			),
            'bulan_awal'=> array(
                'attr'        => array(
                  'id'    => 'bulan_awal',
                  'class' => 'form-control',
                ),
                'data'        => $this->data['bulan'],
                'value'       => $this->mm,
                'name'        => 'bulan_awal',
            ),
            'tahun'=> array(
                'attr'        => array(
                  'id'    => 'tahun',
                  'class' => 'form-control',
                ),
                'data'        => $this->data['tahun'],
                'value'       => '',
                'name'        => 'tahun',
            ),
		);
	}
}

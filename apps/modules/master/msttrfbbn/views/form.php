<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body"> 

              <div class="col-md-12 col-lg-12">
                  <div class="row"> 

                      <div class="col-md-12">
                        <div class="form-group">
                          <?php 
                              echo form_label($form['kota']['placeholder']);
                              echo form_input($form['kota']);
                              echo form_error('kota','<div class="note">','</div>');
                          ?>
                        </div>
                      </div> 
                  </div>
              </div>

              <div class="col-md-12 col-lg-12">
                  <div class="row"> 

                      <div class="col-md-3">
                        <div class="form-group">
                          <?php 
                              echo form_label($form['byproses']['placeholder']);
                              echo form_input($form['byproses']);
                              echo form_error('byproses','<div class="note">','</div>');
                          ?>
                        </div>
                      </div> 
                  </div>
              </div>

              <div class="col-md-12 col-lg-12">
                  <div class="row"> 

                      <div class="col-md-3">
                        <div class="form-group">
                          <?php 
                              echo form_label($form['byjasa']['placeholder']);
                              echo form_input($form['byjasa']);
                              echo form_error('byjasa','<div class="note">','</div>');
                          ?>
                        </div>
                      </div> 
                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row"> 

                      <div class="col-md-3">
                        <div class="form-group">
                          <?php  
                              echo form_input($form['ket']);
                              echo form_error('ket','<div class="note">','</div>');
                          ?>
                        </div>
                      </div> 
                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row"> 

                      <div class="col-md-3">
                        <div class="form-group">
                          <?php  
                              echo form_input($form['kota2']);
                              echo form_error('kota2','<div class="note">','</div>');
                          ?>
                        </div>
                      </div> 
                  </div>
              </div> 
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 
        $('#byproses').autoNumeric('init');
        $('#byjasa').autoNumeric('init');
        nominal();
        //getKategori();
        $('.btn-submit').click(function(){ 
          submit();  
        });
    }); 

    function nominal(){
        var proses = $('#byproses').autoNumeric('get');
        var jasa = $('#byjasa').autoNumeric('get');

        if(proses === ''){
          $('#byproses').autoNumeric('set',0);
        }

        if(jasa === ''){
          $('#byjasa').autoNumeric('set',0);
        }
    }

    function submit(){
        var kota = $("#kota").val().toUpperCase();
        var kota2 = $("#kota2").val().toUpperCase();
        var byproses = $("#byproses").autoNumeric('get');
        var byjasa = $("#byjasa").autoNumeric('get');
        var ket = $("#ket").val();
        //alert(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("msttrfbbn/submit");?>",
      		data: {"kota":kota,"byproses":byproses,"byjasa":byjasa,"ket":ket,"kota2":kota2},  
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp); 
            if(obj.state==='1'){
              swal({
                title: 'Data Berhasil di Proses',
                text: obj.msg,
                type: 'success'
              }, function(){
                window.location.href = '<?=site_url('msttrfbbn');?>';
              }); 
            }else{ 
              swal({
                title: 'Data Gagal di Proses',
                text: obj.msg,
                type: 'error'
              }); 
            }
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    } 

    function refresh(){
      window.location.reload();
    }
</script>

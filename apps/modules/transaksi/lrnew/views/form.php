<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <!-- <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>  -->

            <div class="box-header">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div>
            <div class="box-body body">
                <div class="row">

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-1">
                            <?php echo form_label($form['nodo']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2">
                            <?php
                                echo form_input($form['nodo']);
                                echo form_error('nodo','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo form_label($form['noso']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['noso']);
                                echo form_error('noso','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo "<b>No. Mesin/RK</b>"; ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['nosin']);
                                echo form_error('nosin','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-3 ">
                            <?php
                                echo form_input($form['norangka']);
                                echo form_error('norangka','<div class="note">','</div>');
                            ?>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-1">
                            <?php echo form_label($form['tgldo']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2">
                            <?php
                                echo form_input($form['tgldo']);
                                echo form_error('tgldo','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo form_label($form['nama']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['nama']);
                                echo form_error('nama','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo "<b>Tipe Unit</b>"; ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['kdtipe']);
                                echo form_error('kdtipe','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-3 ">
                            <?php
                                echo form_input($form['nmtipe']);
                                echo form_error('nmtipe','<div class="note">','</div>');
                            ?>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-1">
                            <?php echo form_label($form['nmsales']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2">
                            <?php
                                echo form_input($form['nmsales']);
                                echo form_error('nmsales','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo form_label($form['nmleasing']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['nmleasing']);
                                echo form_error('nmleasing','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo "<b>Warna/Tahun</b>"; ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php
                                echo form_input($form['nmwarna']);
                                echo form_error('nmwarna','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php
                                echo form_input($form['tahun']);
                                echo form_error('tahun','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo form_label($form['nopo']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['nopo']);
                                echo form_error('nopo','<div class="note">','</div>');
                            ?>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-1">
                            <?php echo form_label($form['nmspv']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2">
                            <?php
                                echo form_input($form['nmspv']);
                                echo form_error('nmspv','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo form_label($form['progleas']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['progleas']);
                                echo form_error('progleas','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo "<b>STATUS OTR</b>"; ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['stat_otr']);
                                echo form_error('stat_otr','<div class="note">','</div>');
                            ?>
                        </div>

                        <div class="col-xs-1 ">
                            <?php echo form_label($form['tglpo']['placeholder']); ?>
                        </div>

                        <div class="col-xs-2 ">
                            <?php
                                echo form_input($form['tglpo']);
                                echo form_error('tglpo','<div class="note">','</div>');
                            ?>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['hrg_otr']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['hrg_otr']);
                                      echo form_error('hrg_otr','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-3 ">
                                <i>Beban Leasing</i>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-4 ">
                                  <?php echo form_label($form['hdd2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['hdd2']);
                                      echo form_error('hdd2','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['progres']);
                                      echo form_error('progres','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['disc_unit1']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['disc_unit1']);
                                      echo form_error('disc_unit1','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['disc_leasing']);
                                      echo form_error('disc_leasing','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-4 ">
                                  <?php echo form_label($form['cad_bbn']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['cad_bbn']);
                                      echo form_error('cad_bbn','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-1">
                              </div>

                              <div class="col-xs-3">
                                  <i>AR Insentif Leasing</i>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_dealer1']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_dealer1']);
                                      echo form_error('sub_dealer1','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['sub_disc1']);
                                      echo form_error('sub_disc1','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['hd_dpp']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['hd_dpp']);
                                      echo form_error('hd_dpp','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-1" style="text-align:right">
                                  <i>JP</i>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['jp']);
                                      echo form_error('jp','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_md1']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_md1']);
                                      echo form_error('sub_md1','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_hso']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['sub_hso']);
                                      echo form_error('sub_hso','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-1" style="text-align:right">
                                  <i>Matriks</i>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['matriks']);
                                      echo form_error('matriks','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_ahm1']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_ahm1']);
                                      echo form_error('sub_ahm1','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['ins_leas_dpp']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['ins_leas_dpp']);
                                      echo form_error('ins_leas_dpp','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-1" style="text-align:right">
                                  <i>Total</i>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['total']);
                                      echo form_error('total','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_leas1']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_leas1']);
                                      echo form_error('sub_leas1','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['ins_ai']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['ins_ai']);
                                      echo form_error('ins_ai','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-2 ">
                              </div>

                              <div class="col-xs-5">
                                  <?php echo form_label($form['hdd1']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['hdd1']);
                                      echo form_error('hdd1','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-4 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['hj_net']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['hj_net']);
                                      echo form_error('hj_net','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['um_cust']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['um_cust']);
                                      echo form_error('um_cust','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['hbu_dpp']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['hbu_dpp']);
                                      echo form_error('hbu_dpp','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['disc_unit2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['disc_unit2']);
                                      echo form_error('disc_unit2','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['jaket']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['jaket']);
                                      echo form_error('jaket','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_dealer2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_dealer2']);
                                      echo form_error('sub_dealer2','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-1 ">
                                  <?php echo form_label($form['ins_sales']['placeholder']); ?>
                              </div>

                              <div class="col-xs-2 ">
                                  <?php
                                      echo form_dropdown($form['insentif']['name'],$form['insentif']['data'] ,$form['insentif']['value'] ,$form['insentif']['attr']);
                                      echo form_error('insentif','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['ins_sales']);
                                      echo form_error('ins_sales','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-2">
                                  <?php
                                      echo form_input($form['sales_ins']);
                                      echo form_error('sales_ins','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_md2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_md2']);
                                      echo form_error('sub_md2','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['biops']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['biops']);
                                      echo form_error('biops','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_ahm2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_ahm2']);
                                      echo form_error('sub_ahm2','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['komisi']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['komisi']);
                                      echo form_error('komisi','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['sub_leas2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['sub_leas2']);
                                      echo form_error('sub_leas2','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['oth']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['oth']);
                                      echo form_error('oth','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-2 ">
                              </div>

                              <div class="col-xs-5">
                                  <?php echo form_label($form['um']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['um']);
                                      echo form_error('um','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-4 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['hpp']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['hpp']);
                                      echo form_error('hpp','<div class="note">','</div>');
                                  ?>
                              </div>

                              <div class="col-xs-2">
                                  <?php
                                      echo form_input($form['iris']);
                                      echo form_error('iris','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-2 ">
                              </div>

                              <div class="col-xs-5">
                                  <?php echo form_label($form['lunas']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                  <?php
                                      echo form_input($form['lunas']);
                                      echo form_error('lunas','<div class="note">','</div>');
                                  ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-4 ">
                              </div>

                              <div class="col-xs-3 ">
                                  <?php echo form_label($form['lr_unit']['placeholder']); ?>
                              </div>

                              <div class="col-xs-3">
                                  <?php
                                      echo form_input($form['lr_unit']);
                                      echo form_error('lr_unit','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                </div>
            </div>
            <!-- /.box-body -->
            <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function () {
        $('input').prop('required',true);
        sales_ins();
        autonum();
        if($('#nmleasing').val()==='TUNAI'){
            $('#sub_dealer2').autoNumeric('set',0);
            $('#sub_md2').autoNumeric('set',0);
            $('#sub_ahm2').autoNumeric('set',0);
            $('#sub_leas2').autoNumeric('set',0);
            $('#disc_unit2').autoNumeric('set',0);
            $('#jp').autoNumeric('set',0);
            $('#matriks').autoNumeric('set',0);
            $('#ins_leas_dpp').autoNumeric('set',0);
            sum_hdd();
            jpmatriks();
        }else{
            sum_hdd();
            jpmatriks();
        }

        $('#insentif').change( function () {
          sales_ins();
          sub_hb();
        });

        $('.btn-submit').click( function () {
            var bbn = $('#cad_bbn').autoNumeric('get') || 0 ;
            var progres = $('#progres').val();
          if(bbn===0){
              swal({
                  title: "Data BBN tidak boleh 0",
                  text: "Data gagal tersimpan",
                  type: "error"
              }, function(){
                $('#cad_bbn').focus();
                $('#cad_bbn').css("border", "2px solid red");
              });
          } else {
            if(progres==='1'){
              $('#cad_bbn').blur();
              $('#cad_bbn').css("border", "1px solid gainsboro");
              submit();
            }else{
              swal({
                  title: "Data Faktur Belum Diinput",
                  text: "Data gagal tersimpan",
                  type: "error"
              }, function(){
                  window.location.href = '<?=site_url('lrnew');?>';
              });
            }
          }

        });

        $('.btn-batal').click( function () {
          batal();
        });

    });

    function sales_ins(){
      var insentif = $("#insentif").val();
      $.ajax({
          type: "POST",
          url: "<?=site_url("lrnew/sales_ins");?>",
          data: {"insentif":insentif },
          success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  $('#sales_ins').val(data.nilai);
                  ins();
              });
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function ins(){
      var sales_ins = $("#sales_ins").val();
      $.ajax({
          type: "POST",
          url: "<?=site_url("lrnew/insentif");?>",
          data: {"sales_ins":sales_ins },
          success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  var ins_sales = (parseFloat(sales_ins) * parseFloat(data.ins_sales))/100;
                  $('#ins_sales').autoNumeric('set',ins_sales);
              });
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function autonum(){
      $('#hrg_otr').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#disc_unit1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#disc_unit2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_dealer1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_dealer2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_md1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_md2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_ahm1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_ahm2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_leas1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_leas2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#disc_leasing').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#hdd1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#hdd2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#um_cust').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#um').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#lunas').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#cad_bbn').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#hd_dpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#sub_hso').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#ins_leas_dpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#ins_ai').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#jp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#matriks').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#total').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#hj_net').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#hbu_dpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#jaket').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#ins_sales').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#biops').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#komisi').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#oth').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#hpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#iris').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
      $('#lr_unit').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
    }

    function sum_hdd(){
      // baris 1 kolom 1
      var hrg_otr = $("#hrg_otr").autoNumeric('get');
      var disc_unit1 = $("#disc_unit1").autoNumeric('get');
      var sub_dealer1 = $("#sub_dealer1").autoNumeric('get');
      var sub_md1 = $("#sub_md1").autoNumeric('get');
      var sub_ahm1 = $("#sub_ahm1").autoNumeric('get');
      var sub_leas1 = $("#sub_leas1").autoNumeric('get');

      //baris 2 kolom 1
      var um_cust = $('#um_cust').autoNumeric('get');
      var disc_unit2 = $("#disc_unit2").autoNumeric('get');
      var sub_dealer2 = $("#sub_dealer2").autoNumeric('get');
      var sub_md2 = $("#sub_md2").autoNumeric('get');
      var sub_ahm2 = $("#sub_ahm2").autoNumeric('get');
      var sub_leas2 = $("#sub_leas2").autoNumeric('get');

      //baris 1 kolom 2
      var bbn = $('#cad_bbn').autoNumeric('get');
      var sub_hso = $("#sub_hso").autoNumeric('get');
      var ins_ai = $("#ins_ai").autoNumeric('get');
      var jp = $("#jp").autoNumeric('get');
      var matriks = $("#matriks").autoNumeric('get');

      //baris 2 kolom 2
      var hbu_dpp = $('#hbu_dpp').autoNumeric('get');
      var sub_hso = $("#jaket").autoNumeric('get');
      var ins_sales = $("#ins_sales").autoNumeric('get');
      var biops = $("#biops").autoNumeric('get');
      var komisi = $("#komisi").autoNumeric('get');
      var oth = $("#oth").autoNumeric('get');

      var hdd = parseFloat(hrg_otr) - parseFloat(disc_unit1) - parseFloat(sub_dealer1) - parseFloat(sub_md1) - parseFloat(sub_ahm1) - parseFloat(sub_leas1);
      if (!isNaN(hdd)){
          $("#hdd1").autoNumeric('set',hdd);
          $("#hdd2").autoNumeric('set',hdd);
      }

      var um = parseFloat(um_cust) - parseFloat(disc_unit2) - parseFloat(sub_dealer2) - parseFloat(sub_md2) - parseFloat(sub_ahm2) - parseFloat(sub_leas2);
      if (!isNaN(um)){
          $("#um").autoNumeric('set',um);
      }

      var lunas = parseFloat(hdd) - parseFloat(um);
      if (!isNaN(lunas)){
          $("#lunas").autoNumeric('set',lunas);
      }
      var tgldo = $('#tgldo').val();
      var tgldo = tgldo.substring(6,10)+tgldo.substring(3,5)+tgldo.substring(0,2); 
        // replaceAll('-',''));
      if(tgldo>'20220331'){
        var hd_dpp = Math.round((parseFloat(hdd) - parseFloat(bbn))/1.11);
      } else {
        var hd_dpp = Math.round((parseFloat(hdd) - parseFloat(bbn))/1.1);
      }
      if (!isNaN(hd_dpp)){
          $("#hd_dpp").autoNumeric('set',hd_dpp);
      }

      // var total = parseFloat(jp) + parseFloat(matriks);
      // if (!isNaN(total)){
      //     $("#total").autoNumeric('set',total);
      // }

      if(tgldo>'20220331'){
        var hj_net = parseFloat(hd_dpp) + (parseFloat(total)/1.11);
      } else {
        var hj_net = parseFloat(hd_dpp) + (parseFloat(total)/1.1);
      }
      if (!isNaN(hj_net)){
          $("#hj_net").autoNumeric('set',hj_net);
      }

      var hpp = parseFloat(hbu_dpp) + parseFloat(sub_hso) + parseFloat(ins_sales) + parseFloat(biops) + parseFloat(komisi) + parseFloat(oth);
      if (!isNaN(hpp)){
          $("#hpp").autoNumeric('set',hpp);
      }

      var lr_unit = parseFloat(hj_net) - parseFloat(hpp);
      if (!isNaN(lr_unit)){
          $("#lr_unit").autoNumeric('set',lr_unit);
      }
    }

    // TUNAIIII
    function sub1(){
      // baris 1 kolom 1
      var hrg_otr = $("#hrg_otr").autoNumeric('get');
      var disc_unit1 = $("#disc_unit1").autoNumeric('get');
      var sub_disc = $("#sub_disc1").val();
      var sub_dealer1 = $("#sub_dealer1").autoNumeric('get');
      var sub_md1 = $("#sub_md1").autoNumeric('get');
      var sub_ahm1 = $("#sub_ahm1").autoNumeric('get');
      var sub_leas1 = $("#sub_leas1").autoNumeric('get'); 
      
      // nilai nol
      if(sub_dealer1!=''){
        var sub_dealer1 = sub_dealer1;  
          if(sub_md1!=''){
            var sub_md1 = sub_md1;  
            if(sub_ahm1!=''){
              var sub_ahm1 = sub_ahm1;  
              if(sub_leas1!=''){
                var sub_leas1 = sub_leas1;  
              }else{
                var sub_leas1 = 0;
                $("#sub_leas1").autoNumeric('set','0'); 
              }
            }else{
              var sub_ahm1 = 0;
              $("#sub_ahm1").autoNumeric('set','0'); 
            }
          }else{
            var sub_md1 = 0;
            $("#sub_md1").autoNumeric('set','0'); 
          }
       }else{
        var sub_dealer1 = 0;
        $("#sub_dealer1").autoNumeric('set','0'); 
       }

       //jumlah nilai
      var disk1 = parseFloat(sub_disc) - (parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
      
      var sub_hso = parseFloat(sub_ahm1) + parseFloat(sub_md1);
      $("#sub_hso").autoNumeric('set',sub_hso);
      sub_bbn();

      if (!isNaN(disk1)){
         $("#disc_unit1").autoNumeric('set',disk1);
         $("#disc_unit2").autoNumeric('set',disk1);
          if (disk1 != sub_disc){
            var jml = parseFloat(hrg_otr) - (parseFloat(disk1)+parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
            // var sel = disc_unit1 - sub_disc;
            // var total = disk1 + sel;
            // $("#disc_unit1").autoNumeric('set',total);
            // $("#disc_unit2").autoNumeric('set',total); 
            $("#hdd1").autoNumeric('set',jml);
            $("#hdd2").autoNumeric('set',jml); 
            $("#lunas").autoNumeric('set',jml); 

          } else {
            var jml = parseFloat(hrg_otr) - (parseFloat(disk1)+parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
            // var sel = disc_unit1 - sub_disc;
            // var total = disk1 + sel;
            // $("#disc_unit1").autoNumeric('set',total);
            // $("#disc_unit2").autoNumeric('set',total); 
            $("#hdd1").autoNumeric('set',jml);
            $("#hdd2").autoNumeric('set',jml); 
            $("#lunas").autoNumeric('set',jml); 
          }
      } 
    }

    function sub_bbn(){
      // baris 1 kolom 1
      var cad_bbn = $("#cad_bbn").autoNumeric('get');
      var hdd = $("#hdd2").autoNumeric('get');
      var ins_leas_dpp = $("#ins_leas_dpp").autoNumeric('get');
      var hpp = $("#hpp").autoNumeric('get');
      var sub_hso = $("#sub_hso").autoNumeric('get');
      var ins_ai = $("#ins_ai").autoNumeric('get');

      
      // nilai nol
      if(cad_bbn!=''){
        var cad_bbn = cad_bbn; 
        $("#cad_bbn").autoNumeric('set',cad_bbn);
          if(ins_ai!=''){
            var ins_ai = ins_ai; 
            $("#ins_ai").autoNumeric('set',ins_ai);  
          }else{
            var ins_ai = 0;
            $("#ins_ai").autoNumeric('set','0');
            $("#ins_ai").autoNumeric('set','0'); 
          }
       }else{
        var cad_bbn = 0;
        $("#cad_bbn").autoNumeric('set','0');
        $("#cad_bbn").autoNumeric('set','0'); 
       }
      var tgldo = $('#tgldo').val();
      var tgldo = tgldo.substring(6,10)+tgldo.substring(3,5)+tgldo.substring(0,2); 
      if(tgldo>'20220331'){
        var hd_dpp = Math.round((parseFloat(hdd) - parseFloat(cad_bbn))/1.11);
      } else {
        var hd_dpp = Math.round((parseFloat(hdd) - parseFloat(cad_bbn))/1.1);
      }
        

      if (!isNaN(hd_dpp)){
          $("#hd_dpp").autoNumeric('set',hd_dpp);
          var hj_net = parseFloat(hd_dpp) + parseFloat(ins_leas_dpp) + parseFloat(sub_hso) + parseFloat(ins_ai);
          if (!isNaN(hj_net)){
            $("#hj_net").autoNumeric('set',hj_net);
            var lr_unit = parseFloat(hj_net) - parseFloat(hpp);
            if (!isNaN(lr_unit)){
              $("#lr_unit").autoNumeric('set',lr_unit);
            }
          }
      }
    }

    function sub_hb(){
      // baris 1 kolom 1
      var hj_net = $("#hj_net").autoNumeric('get');
      var hbu_dpp = $("#hbu_dpp").autoNumeric('get');
      var jaket = $("#jaket").autoNumeric('get');
      var ins_sales = $("#ins_sales").autoNumeric('get');
      var biops = $("#biops").autoNumeric('get');
      var komisi = $("#komisi").autoNumeric('get');
      var oth = $("#oth").autoNumeric('get');
      var iris = $("#iris").autoNumeric('get');

      var hpp = (parseFloat(hbu_dpp) + parseFloat(jaket) + parseFloat(ins_sales) + parseFloat(biops) + parseFloat(komisi) + parseFloat(oth));

      if (!isNaN(hpp)){
          $("#hpp").autoNumeric('set',hpp);
          var lr_unit = parseFloat(hj_net) - parseFloat(hpp);
          if (!isNaN(lr_unit)){
              $("#lr_unit").autoNumeric('set',lr_unit);
          }
      }
    }

    // KREDIITTT
    function sub2(){
      // baris 1 kolom 1
      var hrg_otr = $("#hrg_otr").autoNumeric('get');
      var disc_unit1 = $("#disc_unit1").autoNumeric('get');
      var sub_disc = $("#sub_disc1").val();
      var sub_dealer1 = $("#sub_dealer1").autoNumeric('get');
      var sub_md1 = $("#sub_md1").autoNumeric('get');
      var sub_ahm1 = $("#sub_ahm1").autoNumeric('get');
      var sub_leas1 = $("#sub_leas1").autoNumeric('get');

      // baris 2 kolom 1
      var um_cust = $("#um_cust").autoNumeric('get');
      
      // nilai nol
      if(sub_dealer1!=''){
        var sub_dealer1 = sub_dealer1; 
        $("#sub_dealer2").autoNumeric('set',sub_dealer1);
          if(sub_md1!=''){
            var sub_md1 = sub_md1; 
            $("#sub_md2").autoNumeric('set',sub_md1); 
            if(sub_ahm1!=''){
              var sub_ahm1 = sub_ahm1; 
              $("#sub_ahm2").autoNumeric('set',sub_ahm1); 
              if(sub_leas1!=''){
                var sub_leas1 = sub_leas1; 
                $("#sub_leas2").autoNumeric('set',sub_leas1); 
              }else{
                var sub_leas1 = 0;
                $("#sub_leas1").autoNumeric('set','0');
                $("#sub_leas2").autoNumeric('set','0'); 
              }
            }else{
              var sub_ahm1 = 0;
              $("#sub_ahm1").autoNumeric('set','0');
              $("#sub_ahm2").autoNumeric('set','0'); 
            }
          }else{
            var sub_md1 = 0;
            $("#sub_md1").autoNumeric('set','0');
            $("#sub_md2").autoNumeric('set','0'); 
          }
       }else{
        var sub_dealer1 = 0;
        $("#sub_dealer1").autoNumeric('set','0');
        $("#sub_dealer2").autoNumeric('set','0'); 
       }

       //jumlah nilai
      var disk1 = parseFloat(sub_disc) - (parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
      
      var sub_hso = parseFloat(sub_ahm1) + parseFloat(sub_md1);
      $("#sub_hso").autoNumeric('set',sub_hso);
      sub_bbn();

      if (!isNaN(disk1)){
         $("#disc_unit1").autoNumeric('set',disk1);
         $("#disc_unit2").autoNumeric('set',disk1);
          if (disk1 != sub_disc){
            var jml = parseFloat(hrg_otr) - (parseFloat(disk1)+parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
            // var sel = disc_unit1 - sub_disc;
            // var total = disk1 + sel;
            // $("#disc_unit1").autoNumeric('set',total);
            // $("#disc_unit2").autoNumeric('set',total);
            $("#sub_dealer2").autoNumeric('set',sub_dealer1);
            $("#sub_md2").autoNumeric('set',sub_md1);
            $("#sub_ahm2").autoNumeric('set',sub_ahm1);
            $("#sub_leas2").autoNumeric('set',sub_leas1); 
            $("#hdd1").autoNumeric('set',jml);
            $("#hdd2").autoNumeric('set',jml);
            var sum_diskon = parseFloat(disk1) + (parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
            var um = parseFloat(um_cust)-parseFloat(sum_diskon); 
            $("#um").autoNumeric('set',um);
            var lunas = parseFloat(jml)-parseFloat(um);
            $("#lunas").autoNumeric('set',lunas); 

          } else {
            var jml = parseFloat(hrg_otr) - (parseFloat(disk1)+parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
            // var sel = disc_unit1 - sub_disc;
            // var total = disk1 + sel;
            // $("#disc_unit1").autoNumeric('set',total);
            // $("#disc_unit2").autoNumeric('set',total);
            $("#sub_dealer2").autoNumeric('set',sub_dealer1);
            $("#sub_md2").autoNumeric('set',sub_md1);
            $("#sub_ahm2").autoNumeric('set',sub_ahm1);
            $("#sub_leas2").autoNumeric('set',sub_leas1); 
            $("#hdd1").autoNumeric('set',jml);
            $("#hdd2").autoNumeric('set',jml);
            var sum_diskon = parseFloat(disk1) + (parseFloat(sub_dealer1)+parseFloat(sub_md1)+parseFloat(sub_ahm1)+parseFloat(sub_leas1));
            var um = parseFloat(um_cust)-parseFloat(sum_diskon); 
            $("#um").autoNumeric('set',um);
            var lunas = parseFloat(jml)-parseFloat(um);
            $("#lunas").autoNumeric('set',lunas); 
          }
      }
    } 

    function jpmatriks(){
      // baris 1 kolom 1
      var jp = $("#jp").autoNumeric('get');
      var matriks = $("#matriks").autoNumeric('get');
      var hd_dpp = $("#hd_dpp").autoNumeric('get');
      var hpp = $("#hpp").autoNumeric('get');
      var sub_hso = $("#sub_hso").autoNumeric('get');
      var ins_ai = $("#ins_ai").autoNumeric('get');
      var noso = $("#noso").val();

      var total = (parseFloat(jp) + parseFloat(matriks)); 
      // alert(total);
      if(total===''){
        var total ='0';
      }
      $.ajax({
          type: "POST",
          url: "<?=site_url("lrnew/refund_dpp");?>",
          data: {"total":total,"noso":noso },
          success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  var dpp_total = data.refund_dpp;
                  $("#total").autoNumeric('set',total);
                  $("#ins_leas_dpp").autoNumeric('set',data.refund_dpp);
                  var hj_net = parseFloat(hd_dpp) + parseFloat(data.refund_dpp) + parseFloat(sub_hso) + parseFloat(ins_ai);
                  if (!isNaN(hj_net)){
                    $("#hj_net").autoNumeric('set',hj_net);
                    var lr_unit = parseFloat(hj_net) - parseFloat(hpp);
                    if (!isNaN(lr_unit)){
                      $("#lr_unit").autoNumeric('set',lr_unit);
                    }
                  }
              });
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });

    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('lrnew');?>';
        });
    }

    function submit(){
      var nodo = $('#nodo').val();
        swal({
            title: "Transaksi laba/Rugi dengan No. DO : '"+nodo+"' akan disimpan. Lanjutkan?",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nodo = $("#nodo").val();

            var disc = $("#disc_unit1").autoNumeric('get') || 0;
            var sud_dealer = $("#sub_dealer1").autoNumeric('get') || 0;
            var sub_md = $("#sub_md1").autoNumeric('get') || 0;
            var sub_ahm = $("#sub_ahm1").autoNumeric('get') || 0;
            var sub_leas = $("#sub_leas1").autoNumeric('get') || 0;

            var sub_hso = $("#sub_hso").autoNumeric('get') || 0;
            var ins_leasing = $("#ins_leas_dpp").autoNumeric('get') || 0;
            var ins_ai = $("#ins_ai").autoNumeric('get') || 0;

            var harga_beli = $("#hbu_dpp").autoNumeric('get') || 0;
            var iris = $("#iris").autoNumeric('get') || 0;
            var bbn = $("#cad_bbn").autoNumeric('get') || 0;
            var komisi = $("#komisi").autoNumeric('get') || 0;

            var topcover = $("#oth").autoNumeric('get') || 0;
            var jaket = $("#jaket").autoNumeric('get') || 0;
            var ins_sales = $("#ins_sales").autoNumeric('get') || 0;
            var biops = $("#biops").autoNumeric('get') || 0;

            var jp = $("#jp").autoNumeric('get') || 0;
            var matriks = $("#matriks").autoNumeric('get') || 0;

            var kdsales_ins = $("#insentif").val();
            var sales_ins = $("#sales_ins").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("lrnew/save");?>",
                data: {"nodo":nodo
                      ,"disc":disc
                      ,"sud_dealer":sud_dealer
                      ,"sub_md":sub_md
                      ,"sub_ahm":sub_ahm
                      ,"sub_leas":sub_leas
                      ,"sub_hso":sub_hso
                      ,"ins_leasing":ins_leasing
                      ,"ins_ai":ins_ai
                      ,"harga_beli":harga_beli
                      ,"iris":iris
                      ,"bbn":bbn
                      ,"komisi":komisi
                      ,"topcover":topcover
                      ,"jaket":jaket
                      ,"ins_sales":ins_sales
                      ,"biops":biops
                      ,"jp":jp
                      ,"matriks":matriks
                      ,"kdsales_ins":kdsales_ins
                      ,"sales_ins":sales_ins },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        if(data.tipe==="success"){ 
                          // ctk_lr(nodo);x
                          // window.open('lrnew/ctk_lr/'+nodo, '_blank');
                          window.open('../ctk_lr/'+nodo, '_blank');
                          // window.location.href = '../ctk_lr/'+nodo;
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            },function(){
                                window.location.href = "<?=site_url('lrnew');?>"; 
                            });
                        }
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }
    function ctk_lr(nodo){
        var nodo = nodo.toUpperCase();
          $.ajax({
              type: "POST",
              url: "<?=site_url("lrnew/ctk_lr");?>",
              data: {"nodo":nodo },
              success: function(resp){
              }
          });
    }
</script>

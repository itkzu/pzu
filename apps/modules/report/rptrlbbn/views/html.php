<?php

/* 
 * ***************************************************************
 * Script : pdf
 * Version : 
 * Date : 
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();

if($array['submit']==="excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAPORAN PERHITUNGAN LABA (RUGI) BBN PERIODE " . $array['periode_awal'] . " s/d " .$array['periode_akhir'] ." ".date('Y-m-d H:i:s').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');
$caption = "<b>LAPORAN PERHITUNGAN LABA (RUGI) BBN </b>"
        . "<br>"
        . "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->session->userdata('data')['cabang'])."</b>"
        . "<br>"
        . "<b> PERIODE " . $array['periode_awal'] . " s/d " .$array['periode_akhir'] ."</b>"
        . "<br><br>";
$namaheader1 = array(
    array('data' => 'No.'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; width: 35px; font-size: 12px;'),
    array('data' => 'Terima Tagihan'
                        , 'colspan' => '2'
                        , 'style' => 'text-align: center; font-size: 12px;'),
    array('data' => 'Delivery Order (DO)'
                        , 'colspan' => '2'
                        , 'style' => 'text-align: center; font-size: 12px;'),
    array('data' => 'Nama STNK/BPKB'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; font-size: 12px;'),
    array('data' => 'Kota'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; font-size: 12px;'),
    array('data' => 'Kode'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; width: 100px; font-size: 12px;'),
    array('data' => 'Tipe Unit'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; width: 150px; font-size: 12px;'),
    array('data' => 'Tgl Pengajuan BJ'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; width: 70px; font-size: 12px;'),
    array('data' => 'BBN'
                        , 'colspan' => '7'
                        , 'style' => 'text-align: center; font-size: 12px;'),
    array('data' => 'Cadangan'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Laba (Rugi) BBN'
                        , 'rowspan' => '2'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Biaya Notis'
                        , 'colspan' => '4'
                        , 'style' => 'text-align: center; font-size: 12px;'),
);
$this->table->add_row($namaheader1);
$namaheader2 = array(
    array('data' => 'Nomor'
                        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
    array('data' => 'Tanggal'
                        , 'style' => 'text-align: center; width: 70px; font-size: 12px;'),
    array('data' => 'Nomor'
                        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
    array('data' => 'Tanggal'
                        , 'style' => 'text-align: center; width: 70px; font-size: 12px;'),
    array('data' => 'Tarif'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Proses'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Jasa'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'PPh 21'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),                
    array('data' => 'Bruto'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Diskon'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Netto'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    
    array('data' => 'Terima Notis'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),                
    array('data' => 'Tagihan'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Riil (Notis)'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
    array('data' => 'Selisih'
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
);
$this->table->add_row($namaheader2);
$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
// Caption text
$this->table->set_caption($caption);
// Header detail
$no = 1;
$tnilai = 0;
$tbyproses = 0;
$tjasa = 0;
$tpph21 = 0;
$tbruto = 0;
$tdisc = 0;
$tnetto = 0;
$tbbn = 0;
$tlr_bbn = 0;

$t_pajak_notis = 0;
$t_pajak_tag = 0;
$t_pajak_selisih = 0;
foreach ($rpt as $v) {
    $detail = array(
        array('data' => $no
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $v['nobbn_trm']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $v['tglbbn_trm']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $v['nodo']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $v['tgldo']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $v['nama']
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $v['kota']
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $v['kode']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $v['kdtipe']
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $v['tgl_aju_bbn']
                            , 'style' => 'text-align: center; font-size: 12px;'),                                                       
        array('data' => number_format($v['nilai'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['byproses'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['jasa'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['pph21'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => number_format($v['bruto'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['disc'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['netto'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['bbn'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['lr_bbn'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        
        array('data' =>  $v['tgl_trm_notis']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' =>  number_format($v['pajak_tag'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['pajak_notis'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' =>  number_format($v['pajak_selisih'], 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
    );
    $tnilai += $v['nilai'];
    $tbyproses += $v['byproses'];
    $tjasa += $v['jasa'];
    $tpph21 += $v['pph21'];
    $tbruto += $v['bruto'];
    $tdisc += $v['disc'];
    $tnetto += $v['netto'];
    $tbbn += $v['bbn'];
    $tlr_bbn += $v['lr_bbn'];
    
    $t_pajak_notis += $v['pajak_notis'];
    $t_pajak_tag += $v['pajak_tag'];
    $t_pajak_selisih += $v['pajak_selisih'];
    
    $this->table->add_row($detail);    
    $no++;
}

$total_foot = array(
    array('data' => '<b>TOTAL</b>'
                        , 'colspan' => '10'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' => '<b>'.number_format($tnilai, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tbyproses, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tjasa, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tpph21, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),                        
    array('data' => '<b>'.number_format($tbruto, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tdisc, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tnetto, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tbbn, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($tlr_bbn, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    
    array('data' =>  '<b> </b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($t_pajak_tag, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($t_pajak_notis, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' =>  '<b>'.number_format($t_pajak_selisih, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
);
$this->table->add_row($total_foot);  

$this->table->set_template($template);
echo $this->table->generate();      

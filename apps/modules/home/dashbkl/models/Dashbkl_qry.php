<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashbkl_qry
 *
 * @author adi
 */
class Dashbkl_qry extends CI_Model{
	//put your code here
	public function __construct() {
		parent::__construct();
	}

	public function getBklJualQty() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

		$periode = $this->input->post('tahun');
		$tahun = substr($periode,0,4);

		$sql = "
		
SELECT  count(pk)-1 AS jml
		, bulan
FROM
(

SELECT top 1 '' AS pk, '01' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '02' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '03' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '04' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '05' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '06' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '07' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '08' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '09' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '10' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '11' AS bulan FROM t_wi_hd
UNION ALL
SELECT top 1 '' AS pk, '12' AS bulan FROM t_wi_hd
UNION ALL

SELECT  a.pk
		, format(a.dt,'mm') AS bulan
FROM	t_wi_hd a
WHERE	format(a.dt,'yyyy') = '{$tahun}'

UNION ALL

SELECT  a.pk
		, format(a.dt,'mm') AS bulan
FROM	t_si_hd a
WHERE	format(a.dt,'yyyy') = '{$tahun}'
)
GROUP BY bulan
ORDER BY bulan

				";

		$query = $dbbengkel->query( $sql );

		//echo $dbbengkel->last_query();

		$this->load->database('bengkel', FALSE);

		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return json_encode($res);
	}

	public function getBklJualNom() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

		$periode = $this->input->post('tahun');
		$tahun = substr($periode,0,4);

		$sql = "
		

SELECT	yy.dt as bulan
		, round(sum(IIF(yy.dpp IS NULL,0,yy.dpp))/1000000,2) AS jml
FROM
(
						SELECT TOP 1 '01' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '02' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '03' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '04' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '05' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '06' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '07' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '08' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '09' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '10' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '11' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL
						SELECT TOP 1 '12' AS dt, 0 AS dpp FROM t_wi_hd
						UNION ALL

						SELECT format(a.dt,'mm') AS dt 

							, IIF(xd.nt = '1' AND val(a.kpbto) = 1, x.qty * c.kpb1 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
								IIF(xd.nt = '1' AND val(a.kpbto) = 2, x.qty * c.kpb2 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
									IIF(xd.nt = '1' AND val(a.kpbto) = 3, x.qty * c.kpb3 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
										IIF(xd.nt = '1' AND val(a.kpbto) = 4, x.qty * c.kpb4 * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
											round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0)
										)
									)
								)
							) AS dpp
							
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_srv x ON a.pk = x.fk)
							INNER JOIN m_srv xd ON x.srvfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON trim(b.cmn1) = trim(c.kdsin)
						WHERE format(a.dt,'yyyy')='{$tahun}'

						UNION ALL

						SELECT format(a.dt,'mm') AS dt 
								
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 
								x.qty * c.oli * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
								round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0)
								) AS dpp
								
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_item x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON b.cmn1 = c.kdsin
						WHERE format(a.dt,'yyyy')='{$tahun}'
							
						UNION ALL

						SELECT format(a.dt,'mm') AS dt 
								
							, round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0) AS dpp
							
						FROM 	
							((t_si_hd a INNER JOIN m_cus b ON a.cusfk = b.pk)
							INNER JOIN t_si_dt x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk							
						WHERE format(a.dt,'yyyy')='{$tahun}'
						
) AS yy
GROUP BY yy.dt
ORDER BY yy.dt

				";

		$query = $dbbengkel->query( $sql );

		//echo $dbbengkel->last_query();

		$this->load->database('bengkel', FALSE);
		
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return json_encode($res);
	}












	public function getJualPerTahun() {
		$this->db->select("COUNT(nodo) jml, to_char(tgldo,'YYYY') tahun");
		$this->db->group_by("to_char(tgldo,'YYYY')");
		$this->db->order_by("to_char(tgldo,'YYYY')");
		$query = $this->db->get("pzu.vl_do_real");
		return json_encode($query->result_array());
	}

	public function getDetailStok() {
		$tanggal = $this->input->post('periode');
		$kode = $this->input->post('kode');
		// extract(day from (now() - tglterima)) - 14 as umur
		$this->db->select("nosin,tahun,nora,tglterima, umur, kdwarna, nmwarna,nmlokasi");
		$this->db->order_by("kdwarna","ASC");
		$this->db->order_by("tglterima","ASC");
		$query = $this->db->get("(SELECT * FROM pzu.v_stock_unit_harian WHERE kode='".$kode."' AND to_char(tglpo,'YYYY-MM') <= '".$tanggal."' AND (tgldo is null OR to_char(tgldo,'YYYY-MM') > '".$tanggal."')) AS v_stock_unit_harian");
		$res = array();
		foreach ($query->result_array() as $value) {
			if($value['umur']===0 || $value['umur']<0){
				$value['umur'] = 0;
			}
			$res[]=array(
				"nosin" => $value['nosin'],
				"kdwarna" => $value['kdwarna'],
				"nora" => $value['nora'],
				"tglterima" => $value['tglterima'],
				"umur" => $value['umur'],
				"nmwarna" => $value['nmwarna'],
				"nmlokasi" => $value['nmlokasi'],
				"tahun" => $value['tahun'],
			);
		}

		$this->db->select("count(nosin) jml, kdwarna, nmwarna");
		$this->db->group_by('kdwarna, nmwarna');
		$this->db->order_by("kdwarna","ASC");
		$query_warna = $this->db->get("(SELECT * FROM pzu.v_stock_unit_harian WHERE kode='".$kode."' AND to_char(tglpo,'YYYY-MM') <= '".$tanggal."' AND (tgldo is null OR to_char(tgldo,'YYYY-MM') > '".$tanggal."')) AS v_stock_unit_harian");
		$res_warna = array();
		foreach ($query_warna->result_array() as $value) {
			$res_warna[]=array(
				"jml" => $value['jml'],
				"kdwarna" => $value['kdwarna'],
				"nmwarna" => $value['nmwarna'],
			);
		}
		
		$data = array(
			'jmlwarna' => $res_warna,
			'detail' => $res,
		);
		return json_encode($data);
		
	}

	public function getStokPerTipeAktual() {
		$tanggal = $this->input->post('tanggal');
		$this->db->select("COUNT(nosin) AS jml, kode, nmtipe");
		$this->db->group_by("kode, nmtipe");
		$this->db->order_by("kode","ASC");
		$query = $this->db->get("(SELECT * FROM pzu.v_stock_unit_harian WHERE to_char(tglpo,'YYYY-MM') <= '".$tanggal."' AND (tgldo is null OR to_char(tgldo,'YYYY-MM') > '".$tanggal."')) AS v_stock_unit_harian");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"nmtipegrp" => $value['kode'],
				"nmtipe" => $value['nmtipe'],
			);
		}
		return json_encode($res);
		
	}

	public function getStokPerTipeAktual_Old() {
		$tanggal = $this->input->post('tanggal');
		$this->db->select("COUNT(nosin) AS jml"
		. ", CASE "
				. " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
				. " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
				. " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
				. " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
				. " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
				. " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
				. " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
				. " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
				. " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
		. " ELSE tipe_group.nmtipegrp END AS nmtipegrp");
		$this->db->group_by("nmtipegrp, tipe_group.kdtipegrp");
		$this->db->order_by("tipe_group.kdtipegrp","ASC");
		$this->db->join("pzu.tipe","v_stock_unit_harian.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
		$query = $this->db->get("(SELECT * FROM pzu.v_stock_unit_harian WHERE to_char(tglpo,'YYYY-MM') <= '".$tanggal."' AND (tgldo is null OR to_char(tgldo,'YYYY-MM') > '".$tanggal."')) AS v_stock_unit_harian");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"nmtipegrp" => $value['nmtipegrp'],
			);
		}
		return json_encode($res); 
		
	}

	public function getJualPerSalesAktual() {
		$tanggal = $this->input->post('tanggal');
//        $this->db->select("COUNT(vl_do_real.nodo) AS jml"
//                        . " , sales.nmsales"
//                        . " , CASE WHEN sales.status='CS' THEN 'CS' ELSE 'SL' END AS status ");
//        $this->db->group_by("sales.nmsales, CASE WHEN sales.status='CS' THEN 'CS' ELSE 'SL' END");
//        $this->db->order_by("COUNT(vl_do_real.nodo)","DESC");
//        $this->db->where("sales.faktif","true");
//        $this->db->where("sales.status <>","KC");
//        $this->db->where("sales.status <>","KW");
//        $this->db->where("sales.status <>","SV");
//        $this->db->join("pzu.sales","sales.kdsales = vl_do_real.kdsales");
//        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real "
//                . " WHERE to_char(vl_do_real.tgldo,'YYYY-MM') = '".$tanggal."'"
//                . " ) AS vl_do_real");
		$str = "select nmlengkap, status, total as jml from
						(
							SELECT  c.nmlengkap,c.status, count(c.kdsales)::int as total
							from  (pzu.t_do a inner join
							 pzu.t_so b on a.noso=b.noso) right join
							 pzu.sales c on b.kdsales = c.kdsales
							where a.periode = '".  str_replace("-", "", $tanggal)."'
							group by c.nmlengkap,c.status
							union 
							select nmlengkap,status, 0::int as total
							from pzu.sales
							where faktif = true and 
							 status in ('CS','SL','CO') and
							 kdsales not in 
							(
							SELECT  b.kdsales
							from  pzu.t_do a inner join
							 pzu.t_so b on a.noso=b.noso
							where a.periode = '".  str_replace("-", "", $tanggal)."'
							)
						) as x
						order by x.total desc,x.nmlengkap";
		$query = $this->db->query($str);
		$res = array();
		if($query->num_rows()>0){
			$jmlsales = $query->result_array();
			foreach ($jmlsales as $value) {
				$res[]=array(
					"jml" => $value['jml'],
					"nmsales" => $value['nmlengkap'],
					"status" => $value['status'],
				);
			}
		}
		
//        $this->db->select("COUNT(sales.kdsales) AS jml, CASE WHEN sales.status='CS' THEN 'CS' ELSE 'SL' END AS status");
//        $this->db->group_by("CASE WHEN sales.status='CS' THEN 'CS' ELSE 'SL' END");
//        $this->db->where("faktif","true");
//        $this->db->where("sales.faktif","true");
//        $this->db->where("sales.status <>","KC");
//        $this->db->where("sales.status <>","KW");
//        $this->db->where("sales.status <>","SV");
//        $query_sales = $this->db->get("pzu.sales");
		$query_sales = $this->db->query("select COUNT(nmlengkap) as jml, status from (" . $str .") as jmlsales GROUP BY status");
		$res_sales = array();
		foreach ($query_sales->result_array() as $value) {
			$res_sales[]=array(
				"jml" => $value['jml'],
				"status" => $value['status'],
			);
		}
		
		$data = array(
			'jual' => $res,
			'jmlsales' => $res_sales,
		);
		return json_encode($data);  
	}

	public function getJualPerPos() {
		$tanggal = $this->input->post('tanggal');
		$this->db->where("to_char(tgldo,'YYYY-MM')",$tanggal);
		//$this->db->where("(nmdiv <> '-' AND nmdiv <> '')");
		$this->db->select("COUNT(nodo) AS jml, nmdiv");
		$this->db->group_by("nmdiv");
		$this->db->order_by("COUNT(nodo)","DESC");
		$query = $this->db->get("pzu.vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kec" => $value['nmdiv'],
			);
		}
		return json_encode($res); 
	}

	public function getJualPerKecamatan() {
		$tanggal = $this->input->post('tanggal');
		$pos = $this->input->post('pos');
		$this->db->where("to_char(tgldo,'YYYY-MM')",$tanggal);
		$this->db->where("nmdiv",$pos);
		//$this->db->where("(kec <> '-' AND kec <> '')");
		$this->db->select("COUNT(nodo) AS jml, kec_s");
		$this->db->group_by("kec_s");
		$this->db->order_by("COUNT(nodo)","DESC");
		//$this->db->limit(10);
		$query = $this->db->get("pzu.vl_do_real");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kec" => $value['kec_s'],
			);
		}
		
		$this->db->where('divisi.nmdiv',$pos);
		$this->db->join('mst.divisi', 'divisi.kddiv = mstringarea.kddiv');
		$queryRing = $this->db->get("pzu.mstringarea");
		$resRing = array();
		foreach ($queryRing->result_array() as $value) {
			$resRing[]=array(
				"nama" => $value['nama'],
				"area" => $value['area'],
			);
		}
		$arr = array(
		  'kec'  => $res,
		  'ringarea' => $resRing,
		);
		return json_encode($arr); 
	}

	public function getJualSalesMTD() {
		$tanggal = $this->input->post('tanggal');
		$nama =  $this->input->post('nama');
		$data = array(
			'grafik' => $this->_getJualSalesGrafik($nama,$tanggal),
		);
		return json_encode($data);          
	}

	private function _getJualSalesGrafik($nama,$tanggal) {
		$str = "SELECT COUNT(nodo) AS jml
					,nmsales as sv
					FROM pzu.vl_do_real
							WHERE to_char(vl_do_real.tgldo,'YYYY-MM') = '".$tanggal."'
								AND nmsales_2 = '".$nama."'
					GROUP BY nmsales
					ORDER BY COUNT(nodo) DESC;";
		$query = $this->db->query($str);
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kc" => $value['sv'],
			);
		}
		return $res; 
	}
	
	public function getJualSalesKoorMTD() {
		$tanggal = $this->input->post('tanggal');
		$nama =  $this->input->post('nama');
		$data = array(
			'grafik' => $this->_getJualSalesKoorGrafik($nama,$tanggal),
		);
		return json_encode($data);  
		
	}
	
	private function _getJualSalesKoorGrafik($nama,$tanggal) {
		$str = "SELECT COUNT(nodo) AS jml
					, nmsales_2 as sv
					FROM pzu.vl_do_real
							WHERE to_char(vl_do_real.tgldo,'YYYY-MM') = '".$tanggal."'
								AND vl_do_real.nmsales_3 = '".$nama."'
					GROUP BY nmsales_2
					ORDER BY COUNT(nodo) DESC;";
		$query = $this->db->query($str);
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kc" => $value['sv'],
			);
		}
		return $res; 
	}
	
	public function getJualSalesHeader() {
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$data = array(
			'salesheaderytd' => $this->_getJualSalesYTD($tahun,$tanggal),
			'salesheadermtd' => $this->_getJualSalesMTD($tanggal),
		);
		return json_encode($data);        
	}
	
	private function _getJualSalesYTD($tahun,$tanggal) {
		$str = "SELECT COUNT(nodo) AS jml
					, nmsales_3 as kc
					FROM pzu.vl_do_real
							WHERE (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')
					GROUP BY nmsales_3
					ORDER BY nmsales_3;";
		$query = $this->db->query($str);
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kc" => $value['kc'],
			);
		}
		return $res; 
		
	}
	
	private function _getJualSalesMTD($tanggal) {
		$str = "SELECT COUNT(nodo) AS jml
					, nmsales_3 as kc
					FROM pzu.vl_do_real
							WHERE to_char(vl_do_real.tgldo,'YYYY-MM') = '".$tanggal."'
					GROUP BY nmsales_3
					ORDER BY nmsales_3";
		$query = $this->db->query($str);
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kc" => $value['kc'],
			);
		}
		return $res; 
		
	}
	
	public function getJualPerLeasingOthersYTD() {
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');    
		$this->db->select("COUNT(nodo) AS jml, leasing.kdleasing");
		$this->db->group_by("leasing.kdleasing");
		$this->db->order_by("leasing.kdleasing");
		$this->db->join("(SELECT kdleasing FROM pzu.leasing WHERE (kdleasing <> 'ADR' 
							AND kdleasing <> 'FIF'
							AND kdleasing <> 'OTO'
							AND kdleasing <> 'CSF'
							AND kdleasing <> 'WOM') GROUP BY kdleasing) AS leasing"
						,"leasing.kdleasing = vl_do_real.kdleasing","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real "
				. " WHERE (kdleasing <> 'ADR' "
						. " AND kdleasing <> 'FIF'"
						. " AND kdleasing <> 'OTO'"
						. " AND kdleasing <> 'CSF'"
						. " AND kdleasing <> 'WOM') "
				. " AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."') "
				. " AND tk='K') AS vl_do_real");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kdleasing" => $value['kdleasing'],
			);
		}
		return json_encode($res);      
	}
	
	public function getJualPerLeasingOthers() {
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');    
		$this->db->select("COUNT(nodo) AS jml, leasing.kdleasing");
		$this->db->group_by("leasing.kdleasing");
		$this->db->order_by("leasing.kdleasing");
		$this->db->join("(SELECT kdleasing FROM pzu.leasing WHERE (kdleasing <> 'ADR' 
							AND kdleasing <> 'FIF'
							AND kdleasing <> 'OTO'
							AND kdleasing <> 'CSF'
							AND kdleasing <> 'WOM') GROUP BY kdleasing) AS leasing"
						,"leasing.kdleasing = vl_do_real.kdleasing","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real "
				. " WHERE (kdleasing <> 'ADR' "
						. " AND kdleasing <> 'FIF'"
						. " AND kdleasing <> 'OTO'"
						. " AND kdleasing <> 'CSF'"
						. " AND kdleasing <> 'WOM') "
				. " AND to_char(tgldo,'YYYY-MM') = '".$tanggal."' "
				. " AND tk='K') AS vl_do_real");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kdleasing" => $value['kdleasing'],
			);
		}
		return json_encode($res);      
	}
	
	public function getJualLeasing() {
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$data = array(
			'jualleasingmtd' => $this->_getJualLeasingMTD($tanggal),
			'jualleasingytd' => $this->_getJualLeasingYTD($tanggal,$tahun),
		);
		return json_encode($data);  
	}
	
	private function _getJualLeasingMTD($tanggal) {
		$this->db->select("COUNT(nodo) AS jml"
				. ", CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 'ADR'"
				. " WHEN leasing.kdleasing = 'FIF' THEN 'FIF'"
				. " WHEN leasing.kdleasing = 'OTO' THEN 'OTO'"
				. " WHEN leasing.kdleasing = 'CSF' THEN 'CSF'"
				. " WHEN leasing.kdleasing = 'WOM' THEN 'WOM'"
				. " ELSE 'OTHERS' END AS kdleasing");
		$this->db->group_by("CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 'ADR'"
				. " WHEN leasing.kdleasing = 'FIF' THEN 'FIF'"
				. " WHEN leasing.kdleasing = 'OTO' THEN 'OTO'"
				. " WHEN leasing.kdleasing = 'CSF' THEN 'CSF'"
				. " WHEN leasing.kdleasing = 'WOM' THEN 'WOM'"
				. " ELSE 'OTHERS' END");
		$this->db->group_by("CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 1"
				. " WHEN leasing.kdleasing = 'FIF' THEN 2"
				. " WHEN leasing.kdleasing = 'OTO' THEN 4"
				. " WHEN leasing.kdleasing = 'CSF' THEN 3"
				. " WHEN leasing.kdleasing = 'WOM' THEN 5"
				. " ELSE 6 END");
		$this->db->order_by("CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 1"
				. " WHEN leasing.kdleasing = 'FIF' THEN 2"
				. " WHEN leasing.kdleasing = 'OTO' THEN 4"
				. " WHEN leasing.kdleasing = 'CSF' THEN 3"
				. " WHEN leasing.kdleasing = 'WOM' THEN 5"
				. " ELSE 6 END");
		$this->db->join("(SELECT kdleasing FROM pzu.leasing GROUP BY kdleasing) AS leasing","leasing.kdleasing = vl_do_real.kdleasing","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE to_char(tgldo,'YYYY-MM') = '".$tanggal."' AND tk='K') AS vl_do_real");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kdleasing" => $value['kdleasing'],
			);
		}
		return $res;  
	}
	
	private function _getJualLeasingYTD($tanggal,$tahun) {
		$this->db->select("COUNT(nodo) AS jml"
				. ", CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 'ADR'"
				. " WHEN leasing.kdleasing = 'FIF' THEN 'FIF'"
				. " WHEN leasing.kdleasing = 'OTO' THEN 'OTO'"
				. " WHEN leasing.kdleasing = 'CSF' THEN 'CSF'"
				. " WHEN leasing.kdleasing = 'WOM' THEN 'WOM'"
				. " ELSE 'OTHERS' END AS kdleasing");
		$this->db->group_by("CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 'ADR'"
				. " WHEN leasing.kdleasing = 'FIF' THEN 'FIF'"
				. " WHEN leasing.kdleasing = 'OTO' THEN 'OTO'"
				. " WHEN leasing.kdleasing = 'CSF' THEN 'CSF'"
				. " WHEN leasing.kdleasing = 'WOM' THEN 'WOM'"
				. " ELSE 'OTHERS' END");
		$this->db->group_by("CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 1"
				. " WHEN leasing.kdleasing = 'FIF' THEN 2"
				. " WHEN leasing.kdleasing = 'OTO' THEN 4"
				. " WHEN leasing.kdleasing = 'CSF' THEN 3"
				. " WHEN leasing.kdleasing = 'WOM' THEN 5"
				. " ELSE 6 END");
		$this->db->order_by("CASE "
				. " WHEN leasing.kdleasing = 'ADR' THEN 1"
				. " WHEN leasing.kdleasing = 'FIF' THEN 2"
				. " WHEN leasing.kdleasing = 'OTO' THEN 4"
				. " WHEN leasing.kdleasing = 'CSF' THEN 3"
				. " WHEN leasing.kdleasing = 'WOM' THEN 5"
				. " ELSE 6 END");
		$this->db->join("(SELECT kdleasing FROM pzu.leasing GROUP BY kdleasing) AS leasing","leasing.kdleasing = vl_do_real.kdleasing","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."') AND tk='K') AS vl_do_real");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"kdleasing" => $value['kdleasing'],
			);
		}
		return $res;  
	}
	
	public function getGPHarianPerTipe() {
		$nmtipegrp = $this->input->post('nmtipegrp');
		$tanggal = $this->input->post('tanggal');  
		$this->db->select("vl_do_real.tgldo, vl_do_real.nama_s, vl_do_real.tk"
				. " , vl_do_real.nmtipe"
				. ", CASE WHEN vl_do_real.nmleasing IS NULL THEN '-' ELSE vl_do_real.nmleasing END AS nmleasing"
				. ",  CASE WHEN vl_do_real.nmprogleas IS NULL THEN '-' ELSE vl_do_real.nmprogleas END AS nmprogleas"
				. " , vl_do_real.um, vl_do_real.ar_konsumen, vl_do_real.angsuran"
				. " , vl_do_real.tenor, vl_do_real.jp, vl_do_real.gross_profit");        
		$this->db->order_by("vl_do_real.tgldo, vl_do_real.nama_s");
		$this->db->where("CASE "
								. " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
								. " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
								. " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
								. " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
								. " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
								. " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
								. " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
								. " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
								. " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
						. " ELSE tipe_group.nmtipegrp END = '".$nmtipegrp."'");
		$this->db->where("vl_do_real.tgldo IS NOT NULL");
		$this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");                       
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE (to_char(tgldo,'YYYY-MM-DD') = '".$tanggal."')) AS vl_do_real");
		if($query->num_rows()>0){
			foreach ( $query->result_array() as $aRow )
			{
					$row = array();
					foreach ($aRow as $key => $value) {
						if(is_numeric($value)){
							$row[$key] = (float) $value;
						}else{
							$row[$key] = $value;
						}
					}
				   $res[] = $row;
			}
		}else{
			$res = false;
		}
		return json_encode($res);  
	}
	
	public function getJualHarianPerTipe() {
		$tanggal = $this->input->post('tanggal');
		$tgl = explode("-", $tanggal);
		$tglnow = $tgl[0]."-".$tgl[1]."-".$tgl[2];        
		$data = array(
			'gethariantipe' => $this->_getHarianPerTipe($tanggal),
			'getsaleshariantipe'  => $this->_getSalesHarianPerTipe($tanggal),
			'getjualharian' => $this->_getJualHarian($tglnow),
			'getjualharianbefore' => $this->_getHarianBefore($tglnow),
			'getavgjual' => $this->_getAvgJual($tglnow),
		);
		return json_encode($data);        
	}
	
	private function _getAvgJual($tanggal){
		$tgl = explode("-", $tanggal);
		if(($tgl[1]-1)<=9){
			$m = "0".($tgl[1]-1);
		}else{
			$m = ($tgl[1]-1);
		}
		$tglbefore_a = date('Y-m-01', strtotime('first day of -1 months', strtotime($tanggal))); 
		$tglbefore_b = date('Y-m-'.$tgl[2], strtotime('first day of -1 months', strtotime($tanggal))); 
		$tglnow_a = date('Y-m-01', strtotime($tanggal)); 
		$tglnow_b = date('Y-m-d', strtotime($tanggal)); 
		$str = "SELECT COUNT(nodo) As jml_unit
					, SUM(tot_disc) As disc
					, SUM(jp_dpp) As jp_dpp
					, (SUM(tot_disc) - SUM(jp_dpp)) As disc_net
					, SUM(matriks_dpp) As matriks_dpp
					, (SUM(tot_disc) / COUNT(nodo))::decimal(18,2) As avg_disc
					, (SUM(jp_dpp) / COUNT(nodo))::decimal(18,2) As avg_jp_dpp
					, ((SUM(tot_disc) - SUM(jp_dpp))/ COUNT(nodo))::decimal(18,2) As avg_disc_net
					, (SUM(matriks_dpp) / COUNT(nodo))::decimal(18,2) As avg_matriks_dpp
					, tk
					, to_char(tgldo,'YYYY-MM') as periode
					  FROM pzu.vl_do_real
					WHERE ((to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$tglbefore_a."' AND '".$tglbefore_b."')
						OR (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$tglnow_a."' AND '".$tglnow_b."'))
					GROUP BY tk , to_char(tgldo,'YYYY-MM')
					ORDER BY tk DESC,periode DESC";
		$q = $this->db->query($str);        
		$res['0']=array(
		   "key" => "Unit",
		);  
		$res['1']=array(
		   "key" => "Discount",
		); 
		$res['2']=array(
		   "key" => "JP",
		);   
		$res['3']=array(
		   "key" => "Discount Net",
		);   
		$res['4']=array(
		   "key" => "Matrix",
		);     
		$res['5']=array(
		   "key" => "Average Discount",
		); 
		$res['6']=array(
		   "key" => "Average JP",
		);   
		$res['7']=array(
		   "key" => "Average Discount Net",
		);   
		$res['8']=array(
		   "key" => "Average Matrix",
		);   
		$res['9']=array(
		   "key" => "Avg Refund Leasing",
		);     
		$res['10']=array(
		   "key" => "Avg Refund - Disc.",
		);      
		$tot_jml_unit = array();
		$tot_disc = array();
		$tot_jp_dpp = array();
		$tot_disc_net = array();
		$tot_matriks_dpp = array();
		$tot_avg_disc = array();
		$tot_avg_jp_dpp = array();
		$tot_avg_disc_net = array();
		$tot_avg_matriks_dpp = array();
		$period = array();
		foreach ($q->result_array() as $value) {         
			$period[] = $value['periode'];
			if(!isset($tot_jml_unit[$value['periode']])){
				$tot_jml_unit[$value['periode']] = 0;
			}
			$tot_jml_unit[$value['periode']] += $value['jml_unit'];
			$res['0']+=$res['0']+array($value['tk'].$value['periode'] => (int) $value['jml_unit']);
			
			if(!isset($tot_disc[$value['periode']])){
				$tot_disc[$value['periode']] = 0;
			}
			$tot_disc[$value['periode']] += $value['disc'];
			$res['1']+=$res['1']+array($value['tk'].$value['periode'] => (int) $value['disc']);
			
			if(!isset($tot_jp_dpp[$value['periode']])){
				$tot_jp_dpp[$value['periode']] = 0;
			}
			$tot_jp_dpp[$value['periode']] += $value['jp_dpp'];
			$res['2']+=$res['2']+array($value['tk'].$value['periode'] => (int) $value['jp_dpp']);
			
			if(!isset($tot_disc_net[$value['periode']])){
				$tot_disc_net[$value['periode']] = 0;
			}
			$tot_disc_net[$value['periode']] += $value['disc_net'];
			$res['3']+=$res['3']+array($value['tk'].$value['periode'] => (int) $value['disc_net']);
			
			if(!isset($tot_matriks_dpp[$value['periode']])){
				$tot_matriks_dpp[$value['periode']] = 0;
			}
			$tot_matriks_dpp[$value['periode']] += $value['matriks_dpp'];
			$res['4']+=$res['4']+array($value['tk'].$value['periode'] => (int) $value['matriks_dpp']);
			
//            if(!isset($tot_avg_disc[$value['periode']])){
//                $tot_avg_disc[$value['periode']] = 0;
//            }
//            $tot_avg_disc[$value['periode']] += $value['avg_disc'];
			$res['5']+=$res['5']+array($value['tk'].$value['periode'] => (int) $value['avg_disc']);
			
//            if(!isset($tot_avg_jp_dpp[$value['periode']])){
//                $tot_avg_jp_dpp[$value['periode']] = 0;
//            }
//            $tot_avg_jp_dpp[$value['periode']] += $value['avg_jp_dpp'];
			$res['6']+=$res['6']+array($value['tk'].$value['periode'] => (int) $value['avg_jp_dpp']);
			
//            if(!isset($tot_avg_disc_net[$value['periode']])){
//                $tot_avg_disc_net[$value['periode']] = 0;
//            }
//            $tot_avg_disc_net[$value['periode']] += $value['avg_disc_net'];
			$res['7']+=$res['7']+array($value['tk'].$value['periode'] => (int) $value['avg_disc_net']);
			
//            if(!isset($tot_avg_matriks_dpp[$value['periode']])){
//                $tot_avg_matriks_dpp[$value['periode']] = 0;
//            }
//            $tot_avg_matriks_dpp[$value['periode']] += $value['avg_matriks_dpp'];
			$res['8']+=$res['8']+array($value['tk'].$value['periode'] => (int) $value['avg_matriks_dpp']);
			
			
			// Tambahan untuk Refund Leasing dan Refund - Disc.
			// Avg Refund Leasing = AVG JP + AVG Matrix
			$avg_ref_leasing = ((int) $value['avg_jp_dpp'] + (int) $value['avg_matriks_dpp']);
			$res['9']+=$res['9']+array($value['tk'].$value['periode'] => (int) $avg_ref_leasing);
			
			// Avg Refund Discount = (AVG JP + AVG Matrix) - Average Discount
			$avg_ref_disc = 0;
			if($avg_ref_leasing>0){
				$avg_ref_disc = (int) $avg_ref_leasing - (int) $value['avg_disc'];
			}
			$res['10']+=$res['10']+array($value['tk'].$value['periode'] => $avg_ref_disc);
		}
		$res['0']+=$res['0']+$tot_jml_unit;
		$res['1']+=$res['1']+$tot_disc;
		$res['2']+=$res['2']+$tot_jp_dpp;
		$res['3']+=$res['3']+$tot_disc_net;
		$res['4']+=$res['4']+$tot_matriks_dpp;
		
		foreach ($tot_jml_unit as $key => $value) {
			foreach ($tot_disc as $v_key => $v_disc) {
				if($key===$v_key){
					if(!isset($tot_avg_disc[$key])){
						$tot_avg_disc[$key] = 0;
					}
					$tot_avg_disc[$key] = ($v_disc)/$value;  
				}              
			}
		}
		$res['5']+=$res['5']+$tot_avg_disc;
		
		foreach ($tot_jml_unit as $key => $value) {
			foreach ($tot_jp_dpp as $v_key => $v_data) {
				if($key===$v_key){
					if(!isset($tot_avg_jp_dpp[$key])){
						$tot_avg_jp_dpp[$key] = 0;
					}
					$tot_avg_jp_dpp[$key] = ($v_data)/$value;                      
				}              
			}
		}
		$res['6']+=$res['6']+$tot_avg_jp_dpp;
		
		foreach ($tot_jml_unit as $key => $value) {
			foreach ($tot_disc_net as $v_key => $v_data) {
				if($key===$v_key){
					if(!isset($tot_avg_disc_net[$key])){
						$tot_avg_disc_net[$key] = 0;
					}
					$tot_avg_disc_net[$key] = ($v_data)/$value;  
				}              
			}
		}
		$res['7']+=$res['7']+$tot_avg_disc_net;
		
		foreach ($tot_jml_unit as $key => $value) {
			foreach ($tot_matriks_dpp as $v_key => $v_data) {
				if($key===$v_key){
					if(!isset($tot_avg_matriks_dpp[$key])){
						$tot_avg_matriks_dpp[$key] = 0;
					}
					$tot_avg_matriks_dpp[$key] = ($v_data)/$value;  
				}              
			}
		}
		$res['8']+=$res['8']+$tot_avg_matriks_dpp;
		
		// Menghitung Total Refund Leasing dan Total Refund Discount
		$tot_ref_leasing = array();
		$tot_ref_disc = array();
		if($period){
			foreach ($period as $key) {
				// Total Avg Refund Leasing = Total AVG JP + Total AVG Matrix
				$tot_ref_leasing[$key] = $tot_avg_jp_dpp[$key] + $tot_avg_matriks_dpp[$key];
				
				// Total Avg Refund Discount = (Total AVG JP + Total AVG Matrix) - Total AVG Discount Net
				$tot_ref_disc[$key] = ($tot_ref_leasing[$key] - $tot_avg_disc[$key]);
			}
		}
		$res['9']+=$res['9']+$tot_ref_leasing;
		$res['10']+=$res['10']+$tot_ref_disc;
		
		return $res;  
	}


	private function _getJualHarian($tanggal){
		$tgl = explode("-", $tanggal);
		$tglnow = $tgl[0]."-".$tgl[1]."-01";      
		$this->db->select("COUNT(nodo) jml, sum(gross_profit) gross_profit, idtgl.tgl");        
		$this->db->group_by("idtgl.tgl");
		$this->db->order_by("idtgl.tgl");
		$this->db->join("(SELECT to_char(tgldo,'DD') AS tgl FROM pzu.vl_do_real GROUP BY to_char(tgldo,'DD')) AS idtgl","idtgl.tgl = vl_do_real.tgl","RIGHT");
		$query = $this->db->get("(SELECT nodo,to_char(tgldo,'DD') tgl, gross_profit FROM pzu.vl_do_real WHERE (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$tglnow."' AND '".$tanggal."')) AS vl_do_real");
		//// echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tgl" => $value['tgl'],
				"gross_profit" => $value['gross_profit'],
			);
		}
		return $res;  
		
	}
	
	private function _getHarianBefore($tanggal){
		$tgl = explode("-", $tanggal);
		if(($tgl[1]-1)<=9){
			$m = "0".($tgl[1]-1);
		}else{
			$m = ($tgl[1]-1);
		}
		$tglbefore = date('Y-m', strtotime('first day of -1 months', strtotime($tanggal))); 
		//$tglbefore = $tgl[0]."-".$m;
		$this->db->select("COUNT(nodo) jml, idtgl.tgl");        
		$this->db->group_by("idtgl.tgl");
		$this->db->order_by("idtgl.tgl");
		$this->db->join("(SELECT to_char(tgldo,'DD') AS tgl FROM pzu.vl_do_real GROUP BY to_char(tgldo,'DD')) AS idtgl","idtgl.tgl = vl_do_real.tgl","RIGHT");
		$query = $this->db->get("(SELECT nodo,to_char(tgldo,'DD') tgl FROM pzu.vl_do_real WHERE (to_char(tgldo,'YYYY-MM') = '".$tglbefore."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tgl" => $value['tgl'],
				"yyyymm" => $tglbefore,
			);
		}
		return $res;  
		
	}
	
	private function _getSalesHarianPerTipe($tanggal) {
		$tglawal = substr($tanggal, 0,8)."01";
		$this->db->select("COUNT(nodo) AS jml , nmsales_2 AS nmsales");
		$this->db->group_by("nmsales_2");
		$this->db->order_by("nmsales_2","ASC");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real 
									WHERE (to_char(tgldo,'YYYY-MM-DD') = '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"nmsales" => $value['nmsales'],
			);
		}
		return $res;  
	}
	
	private function _getHarianPerTipe($tanggal) {
		$tglawal = substr($tanggal, 0,8)."01";
//        $this->db->select("COUNT(nodo) AS jml, sum(gross_profit) gross_profit"
//                . ", CASE "
//                        . " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
//                        . " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
//                        . " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
//                        . " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
//                        . " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
//                        . " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
//                        . " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
//                        . " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
//                        . " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
//                . " ELSE tipe_group.nmtipegrp END AS nmtipegrp"
//                . ", tipe_group.nmtipegrp AS nmgrp"
//                . ", SUM(CASE "
//                . "         WHEN mstcptargettipe.cp_target_total IS NULL THEN 0 "
//                . "     ELSE mstcptargettipe.cp_target_total END"
//                . "     ) AS cp_target_total");
//        $this->db->group_by("tipe_group.nmtipegrp, tipe_group.kdtipegrp");
//        $this->db->order_by("tipe_group.kdtipegrp","ASC");
//        $this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
//        $this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
//        $this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
//        $this->db->join("pzu.mstcptargettipe","tipe_series.kdtipesr = mstcptargettipe.kdtipesr","LEFT");
//        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real 
//                                    WHERE (to_char(tgldo,'YYYY-MM-DD') = '".$tanggal."')) AS vl_do_real");
		//echo $this->db->last_query();
		$str = "SELECT SUM(jual.jml) AS jml
					,SUM(CASE WHEN jual.gross_profit IS NULL THEN 0 ELSE jual.gross_profit END) AS gross_profit
					,jual.nmgrp
					,jual.nmtipegrp
					,SUM(CASE WHEN mstcptargettipe.cp_target_total is null then 0 ELSE mstcptargettipe.cp_target_total END ) as cp_target_total
					FROM (
							SELECT COUNT(nodo) AS jml, sum(gross_profit) gross_profit, 
							CASE  
							WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L' 
							WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M' 
							WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H' 
							WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L' 
							WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M' 
							WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H' 
							WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L' 
							WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M' 
							WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H' 
							ELSE tipe_group.nmtipegrp END AS nmtipegrp, tipe_group.nmtipegrp AS nmgrp
							, tipe_group.kdtipegrp
							, tipe_series.kdtipesr
							FROM (SELECT * FROM pzu.vl_do_real 
									WHERE (to_char(tgldo, 'YYYY-MM-DD') = '".$tanggal."')) AS vl_do_real
							JOIN pzu.tipe ON vl_do_real.kode = tipe.kode
							RIGHT JOIN pzu.tipe_series ON tipe.kdtipesr = tipe_series.kdtipesr
							JOIN pzu.tipe_group ON tipe_group.kdtipegrp = tipe_series.kdtipegrp
							GROUP BY tipe_group.nmtipegrp, tipe_group.kdtipegrp,tipe_series.kdtipesr
					) jual
							LEFT JOIN (
									SELECT * FROM pzu.mstcptargettipe 
											WHERE (to_char(periode, 'YYYY-MM') = substring('".$tanggal."',1,7))
									) AS mstcptargettipe ON jual.kdtipesr = mstcptargettipe.kdtipesr
					GROUP BY jual.nmgrp,jual.kdtipegrp,jual.nmtipegrp
					ORDER BY jual.kdtipegrp ASC";
		$query = $this->db->query($str);
		$res = array();
		foreach ($query->result_array() as $value) {
			$mtd = $this->_getMtdTipe($tanggal,$value['nmgrp']);
			$ymd = $this->_getYmdTipe($tanggal,$value['nmgrp']);
			$res[]=array(
				"jml" => $value['jml'],
				"nmtipegrp" => $value['nmtipegrp'],
				"gross_profit" => $value['gross_profit'],
				"cp_target_harian" => $value['cp_target_total'],
				"gp_mtd" => $mtd[0]['gp_mtd'],
				"gp_mpct" => (float) $mtd[0]['gp_mpct'],
				"net_sales_mtd" => (float) $mtd[0]['net_sales_mtd'],
				"jml_mtd" => $mtd[0]['jml_mtd'],
				"gp_ymd" => $ymd[0]['gp_ymd'],
				"gp_ypct" => (float) $ymd[0]['gp_ypct'],
				"net_sales_ymd" => (float) $ymd[0]['net_sales_ymd'],
				"jml_ymd" => $ymd[0]['jml_ymd'],
			);
		}
		return $res;  
	}
	
	private function _getMtdTipe($tanggal,$tipe){
		$tglawal = substr($tanggal, 0,8)."01";
		$this->db->select("COUNT(nodo) AS jml_mtd, sum(gross_profit) gp_mtd, sum(net_sales) net_sales_mtd"
				. " , CASE WHEN sum(net_sales) = 0 THEN 0 ELSE (sum(gross_profit) / sum(net_sales)) * 100 END AS gp_mpct");
		$this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
		$this->db->where('tipe_group.nmtipegrp',$tipe);
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real 
									WHERE (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$tglawal."' AND '".$tanggal."')) AS vl_do_real");
		//echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml_mtd" => $value['jml_mtd'],
				"gp_mtd" => $value['gp_mtd'],
				"gp_mpct" => $value['gp_mpct'],
				"net_sales_mtd" => $value['net_sales_mtd'],
			);
		}
		return $res;
	}
	
	private function _getYmdTipe($tanggal,$tipe){
		$tgl = explode("-", $tanggal);
		if(($tgl[1]-1)<=9){
			$m = "0".($tgl[1]-1);
		}else{
			$m = ($tgl[1]-1);
		}
		$tglbefore = date('Y-m', strtotime('first day of -1 months', strtotime($tanggal)));         
		$tglawal = date('Y-m-01', strtotime('first day of -1 months', strtotime($tanggal)));         
		$this->db->select("COUNT(nodo) AS jml_ymd, sum(gross_profit) gp_ymd, sum(net_sales) net_sales_ymd, (sum(gross_profit) / sum(net_sales)) * 100 gp_ypct");
		$this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
		$this->db->where('tipe_group.nmtipegrp',$tipe);
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real 
									WHERE (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$tglawal."' AND '".$tglbefore."-".$tgl[2]."')) AS vl_do_real");
		//echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml_ymd" => $value['jml_ymd'],
				"gp_ymd" => $value['gp_ymd'],
				"gp_ypct" => $value['gp_ypct'],
				"net_sales_ymd" => $value['net_sales_ymd'],
			);
		}
		return $res;
	}

	public function getJualHarian() {
		$tanggal = $this->input->post('tanggal');
		$data = array(
			'getharian' => $this->_getHarian($tanggal),
			'gettarget' => $this->_getTarget($tanggal),
		);
		return json_encode($data);
	}  
	
	private function _getHarian($tanggal){
		$this->db->select("COUNT(nodo) jml, idtgl.tgl");        
		$this->db->group_by("idtgl.tgl");
		$this->db->order_by("idtgl.tgl");
		$this->db->join("(SELECT to_char(tgldo,'DD') AS tgl FROM pzu.vl_do_real GROUP BY to_char(tgldo,'DD')) AS idtgl","idtgl.tgl = vl_do_real.tgl","RIGHT");
		$query = $this->db->get("(SELECT nodo,to_char(tgldo,'DD') tgl FROM pzu.vl_do_real WHERE (to_char(tgldo,'YYYY-MM') = '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tgl" => $value['tgl'],
			);
		}
		return $res;  
		
	}
	
	private function _getTarget($tanggal){
		$this->db->select("id, kd_cabang, to_char(periode,'YYYY-MM') AS periode, jml_hr_kerja, cp_target_total, cp_target_harian");        
		$this->db->where("to_char(periode,'YYYY-MM')",$tanggal);
		$query = $this->db->get("pzu.mstcptarget");
		$res = array();
		if($query->num_rows()>0){
			foreach ($query->result_array() as $value) {
				$res[]=array(
					"jml" => $value['cp_target_harian'],
					"tgl" => $value['periode'],
				);
			}    
		}else{
			$res[]=array(
				"jml" => 0,
				"tgl" => $tanggal,
			);
			
		}
		return $res;  
		
	}
	
	public function getJualPerItemMTD() {
		$nm = $this->input->post('nm');
		$tanggal = $this->input->post('tanggal');
		$this->db->select("vl_do_real.nodo, vl_do_real.tgldo, vl_do_real.nama_s "
							. " ,vl_do_real.tk, vl_do_real.kode, vl_do_real.nmtipe "
							. " ,tipe_group.nmtipegrp as jenis "
						. " ,vl_do_real.nmwarna, vl_do_real.nosin, vl_do_real.nora");
		$this->db->where("(CASE "
								. " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
								. " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
								. " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
								. " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
								. " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
								. " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
								. " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
								. " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
								. " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
								. " WHEN tipe_group.nmtipegrp = 'PREMIUM' THEN 'PREM'"
						. " ELSE tipe_group.nmtipegrp END = '".$nm."')");
		$this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
		$this->db->order_by("vl_do_real.nodo","ASC");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real 
									WHERE (to_char(tgldo,'YYYY-MM') = '".$tanggal."')) AS vl_do_real");
		//// echo $this->db->last_query();
		if($query->num_rows()>0){
			$res = $query->result_array();
		}else{
			$res = FALSE;
		}
		return json_encode($res);
	}
	
	public function getJualTipeMTD() {
//        $tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$str = "SELECT jual.jml,jual.nmtipegrp,jual.kdtipegrp
					, SUM(mstcptargettipe.cp_target_total) AS cp_target_total
				FROM (SELECT COUNT(nodo) AS jml, 
						CASE  
						WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L' 
						WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M' 
						WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H' 
						WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L' 
						WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M' 
						WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H' 
						WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L' 
						WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M' 
						WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H' 
						WHEN tipe_group.nmtipegrp = 'PREMIUM' THEN 'PREM' ELSE tipe_group.nmtipegrp END AS nmtipegrp
						,tipe_group.kdtipegrp
					FROM (SELECT * FROM pzu.vl_do_real WHERE (to_char(tgldo, 'YYYY-MM') = '".$tanggal."')) AS vl_do_real
					JOIN pzu.tipe ON vl_do_real.kode = tipe.kode
					RIGHT JOIN pzu.tipe_series ON tipe.kdtipesr = tipe_series.kdtipesr
					JOIN pzu.tipe_group ON tipe_series.kdtipegrp = tipe_group.kdtipegrp
					GROUP BY tipe_group.nmtipegrp,tipe_group.kdtipegrp) jual
					LEFT JOIN pzu.tipe_series ON tipe_series.kdtipegrp = jual.kdtipegrp
					LEFT JOIN (SELECT * FROM pzu.mstcptargettipe WHERE (to_char(periode, 'YYYY-MM') = '".$tanggal."')) AS mstcptargettipe  
							ON tipe_series.kdtipesr = mstcptargettipe.kdtipesr
					GROUP BY jual.jml,jual.nmtipegrp,jual.kdtipegrp
					ORDER BY jual.kdtipegrp ASC";
		$query = $this->db->query($str);
		//echo $this->db->last_query();
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"nmtipegrp" => $value['nmtipegrp'],
				"cp_target_total" => $value['cp_target_total'],
			);
		}
		return json_encode($res);  
	}
	
	public function getJualTipe() {
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$data = array(
			'pertipebar' => $this->_getJualPerTipeBar($tahun,$tanggal),
			'alltipepie' => $this->_getJualAllTipePie($tahun,$tanggal),
		);
		return json_encode($data);
	}    
	
	private function _getJualPerTipeBar($tahun,$tanggal) {
		$this->db->select("COUNT(nodo) AS jml"
				. ", CASE "
						. " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
						. " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
						. " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
						. " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
						. " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
						. " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
						. " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
						. " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
						. " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
						. " WHEN tipe_group.nmtipegrp = 'PREMIUM' THEN 'PREM'"
				. " ELSE tipe_group.nmtipegrp END AS nmtipegrp");
		$this->db->group_by("tipe_group.nmtipegrp, tipe_group.kdtipegrp");
		$this->db->order_by("tipe_group.kdtipegrp","ASC");
		$this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real 
									WHERE (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"nmtipegrp" => $value['nmtipegrp'],
			);
		}
		return $res;  
	}
	
	private function _getJualAllTipePie($tahun,$tanggal) {
		$this->db->select("SUM(CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' AND vl_do_real.kode IS NOT NULL THEN 1 
									WHEN tipe_group.nmtipegrp LIKE 'CUB%' AND vl_do_real.kode IS NOT NULL THEN 1 
									WHEN tipe_group.nmtipegrp LIKE 'SPORT%' AND vl_do_real.kode IS NOT NULL THEN 1 
									WHEN tipe_group.nmtipegrp LIKE 'PREMIUM%' AND vl_do_real.kode IS NOT NULL THEN 1 
									ELSE 0 END) AS jml
							,CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 'AT' 
								WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 'CUB' 
								WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 'SPORT'
								WHEN tipe_group.nmtipegrp LIKE 'PREMIUM%' THEN 'PREM'
								ELSE 'LAINNYA' END AS nmtipegrp");
		$this->db->group_by("CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 'AT' 
								WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 'CUB' 
								WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 'SPORT'
								WHEN tipe_group.nmtipegrp LIKE 'PREMIUM%' THEN 'PREM'
								ELSE 'LAINNYA' END,
							 CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 1 
								WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 0 
								WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 3
								WHEN tipe_group.nmtipegrp LIKE 'PREMIUM%' THEN 4
								ELSE 99 END");
		$this->db->order_by("CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 1 
								WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 0 
								WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 3
								WHEN tipe_group.nmtipegrp LIKE 'PREMIUM%' THEN 4
								ELSE 99 END");
		$this->db->join("pzu.tipe","vl_do_real.kode = tipe.kode");
		$this->db->join("pzu.tipe_series","tipe.kdtipesr = tipe_series.kdtipesr","RIGHT");
		$this->db->join("pzu.tipe_group","tipe_group.kdtipegrp = tipe_series.kdtipegrp");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"nmtipegrp" => $value['nmtipegrp'],
			);
		}
		return $res;   
	}
	
	public function getJualTunaiKreditPerBulan() {
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$data = array(
			'tunai' => $this->_getJualTunaiPerBulan($tahun,$tanggal),
			'kredit' => $this->_getJualKreditPerBulan($tahun,$tanggal),
			'tunaiytd' => $this->_getJualTunaiYTD($tahun,$tanggal),
			'kreditytd' => $this->_getJualKreditYTD($tahun,$tanggal),
			'tunaimtd' => $this->_getJualTunaiMTD($tanggal),
			'kreditmtd' => $this->_getJualKreditMTD($tanggal),
		);
		return json_encode($data);
	}
	
	private function _getJualTunaiPerBulan($tahun,$tanggal) {
		$this->db->select("COUNT(nodo) AS jml, idbln.bulan");
		$this->db->group_by("idbln.bulan");
		$this->db->order_by("idbln.bulan");
		$this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'T' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return $res;  
	}
	
	private function _getJualKreditPerBulan($tahun,$tanggal) {
		$this->db->select("COUNT(nodo) AS jml, idbln.bulan");
		$this->db->group_by("idbln.bulan");
		$this->db->order_by("idbln.bulan");
		$this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return $res;  
		
	}
	
	private function _getJualTunaiYTD($tahun,$tanggal) {
		$this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
		$this->db->group_by("vl_do_real.tk");
		$this->db->order_by("vl_do_real.tk");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'T' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tk" => $value['tk'],
			);
		}
		return $res;  
	}
	
	private function _getJualKreditYTD($tahun,$tanggal) {
		$this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
		$this->db->group_by("vl_do_real.tk");
		$this->db->order_by("vl_do_real.tk");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tk" => $value['tk'],
			);
		}
		return $res;  
		
	}
	
	private function _getJualTunaiMTD($tanggal) {
		$this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
		$this->db->group_by("vl_do_real.tk");
		$this->db->order_by("vl_do_real.tk");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'T' AND (to_char(tgldo,'YYYY-MM') = '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tk" => $value['tk'],
			);
		}
		return $res;  
	}
	
	private function _getJualKreditMTD($tanggal) {
		$this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
		$this->db->group_by("vl_do_real.tk");
		$this->db->order_by("vl_do_real.tk");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') = '".$tanggal."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"tk" => $value['tk'],
			);
		}
		return $res;  
		
	}
	
	public function getJualJenisPerBulan() {
		$tahun = $this->input->post('tahun');
		$data = array(
			'tunai' => $this->_getDetailJualJenisPerBulan($tahun),
		);
		return json_encode($data);
	}
	
	private function _getDetailJualJenisPerBulan($tahun) {
		$this->db->select("COUNT(nodo) AS jml, idbln.bulan");
		$this->db->group_by("idbln.bulan");
		$this->db->order_by("idbln.bulan");
		$this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return $res;  
		
	}
	
	public function getJualUnitTahun() {
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$this->db->select("COUNT(nodo) AS jml, nmtipe");
		$this->db->where("(to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')");
		$this->db->group_by("nmtipe");
		$this->db->order_by("COUNT(nodo)","DESC");
		//$this->db->limit(10);
		$query = $this->db->get("pzu.vl_do_real");
		return json_encode($query->result_array());
	}
	
	public function getDetailJualUnitTahun() {
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$namaunit = $this->input->post('namaunit');
		$this->db->select("COUNT(nodo) AS jml, idbln.bulan");
		$this->db->group_by("idbln.bulan");
		$this->db->order_by("idbln.bulan");
		$this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE nmtipe = '".$namaunit."' AND (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return json_encode($res);        
	}
	
	public function getJualHeaderSalesTahun() {
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$this->db->select("COUNT(nodo) AS jml, nmsales_header");
		$this->db->where("(to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')");
		$this->db->group_by("nmsales_header");
		$this->db->order_by("COUNT(nodo)","DESC");
		//$this->db->limit(10);
		$query = $this->db->get("pzu.vl_do_real");
		return json_encode($query->result_array());
	}
	
	public function getJualSalesTahun() {
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$nmsales_header = $this->input->post('nmsales_header');
		$this->db->select("COUNT(nodo) AS jml, nmsales");
		$this->db->where("(to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')");
		$this->db->where('nmsales_header',$nmsales_header);
		$this->db->group_by("nmsales");
		$this->db->order_by("COUNT(nodo)","DESC");
		//$this->db->limit(10);
		$query = $this->db->get("pzu.vl_do_real");
		return json_encode($query->result_array());
	}
	
	public function getDetailJualSalesTahun() {
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$namasales = $this->input->post('namasales');
		$this->db->select("COUNT(nodo) AS jml, idbln.bulan");
		$this->db->group_by("idbln.bulan");
		$this->db->order_by("idbln.bulan");
		$this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
		$query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE nmsales = '".$namasales."' AND (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')) AS vl_do_real");
		$res = array();
		foreach ($query->result_array() as $value) {
			$res[]=array(
				"jml" => $value['jml'],
				"bulan" => (int) $value['bulan'],
			);
		}
		return json_encode($res);        
	}
	
	public function adh_json_trn_harian() {
		error_reporting(-1);    
		$aColumns = array('no',
							'periode',
							'ket',
							'qty',);
		$sIndexColumn = "ket";
		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		$sTable = " (SELECT row_number() OVER () AS no,
						periodex as periode,
						ket,
						qty
					  FROM pzu.v_dbadh_trx ) AS a";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";
		
//        echo $sQuery;
		
		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$no = 1;
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($aRow[ $aColumns[$i] ]===null){
						$aRow[ $aColumns[$i] ] = '-';
					}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = (float) $aRow[ $aColumns[$i] ];
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
			   $output['aaData'][] = $row;
			   $no++;
	   }
	echo  json_encode( $output );  
	}
	
	public function adh_json_trn_kas_bank() {
		error_reporting(-1);    
		$aColumns = array('no',
							'nmkb',
							'tglkb',);
	$sIndexColumn = "nmkb";
		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		$sTable = " (SELECT row_number() OVER () AS no,
						nmkb,
						tglkb
					  FROM pzu.v_dbadh_kb ) AS a";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";
		
//        echo $sQuery;
		
		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$no = 1;
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($aRow[ $aColumns[$i] ]===null){
						$aRow[ $aColumns[$i] ] = '-';
					}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = (float) $aRow[ $aColumns[$i] ];
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
			   $output['aaData'][] = $row;
			   $no++;
	}
	echo  json_encode( $output );  
	}
	
	public function adh_json_trn_prog_bbn() {
		error_reporting(-1);    
		$aColumns = array('no',
							'ket',
							'qty',);
	$sIndexColumn = "ket";
		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		$sTable = " (SELECT row_number() OVER () AS no,
						ket,
						qty
					  FROM pzu.v_dbadh_bbn ) AS a";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";
		
//        echo $sQuery;
		
		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$no = 1;
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($aRow[ $aColumns[$i] ]===null){
						$aRow[ $aColumns[$i] ] = '-';
					}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = (float) $aRow[ $aColumns[$i] ];
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
			   $output['aaData'][] = $row;
			   $no++;
	}
	echo  json_encode( $output );  
	}

	public function set_apps_var() {
		$query = $this->db->get('perusahaan');
		if ($query->num_rows()>0) {
			$result = $query->result();

			//$this->apps->set_cabang($result[0]->nmcabang);
			$this->apps->logintag = $result[0]->nmcabang;

			//echo "<script> console.log('PHP: qry". $this->apps->logintag ."');</script>";
		}
	}
	
	public function get_blm_trm_po_leasing() {
		try {
			$str = "select kdleasing
								, count(nodo) 
						from pzu.vm_trm_po_leasing 
								where tgltrmpo is null 
						group by kdleasing 
						order by kdleasing";
			$query = $this->db->query($str);
			if($query->num_rows()>0){
				$res = $query->result_array();
				return $res;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return 0;
		}
	}

	public function get_blm_trm_po_leasing_detail() {
		$kdleasing = $this->input->post('kdleasing');
		try {        
			$str = "select kdleasing
					, nmprogleas
					, pl
					, nodo
					, tgldo
					, nama_s
					, nmtipe
					, nmwarna 
					from pzu.vm_trm_po_leasing 
					where tgltrmpo is null 
						and kdleasing = '{$kdleasing}'
					order by nodo";
			$query = $this->db->query($str);
			if($query->num_rows()>0){
				$res = $query->result_array();
				return $res;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return 0;
		}
	}
}
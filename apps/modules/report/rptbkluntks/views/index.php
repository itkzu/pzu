<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>

			<div class="box-body">

                <div class="form-group">
                    <?php 
                        echo form_label($form['kddiv']['placeholder']);
                        echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                        echo form_error('kddiv','<div class="note">','</div>');  
                    ?>
                </div>
				<div class="form-group">
					<?php
						echo form_label($form['kdpart']['placeholder']);
						echo form_dropdown( $form['kdpart']['name'],
											$form['kdpart']['data'] ,
											$form['kdpart']['value'] ,
											$form['kdpart']['attr']);
						echo form_error('kdpart','<div class="note">','</div>');
					?>
				</div>

				<div class="form-group">
					<?php
						echo '<div class="radio">';
						echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Periode Bulan <small>per bulan</small>');
						echo '</div>';
						echo form_input($form['periode']);
						echo form_error('akun_multi','<div class="note">','</div>');
					?>
				</div>

				<div class="row">

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Periode Awal');
								echo '</div>';
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Pilih Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							?>
						</div>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				</div>

				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 100px;text-align: center;">Keterangan</th>
								<th style="text-align: center;">No. Ref</th>
								<th style="text-align: center;">Tanggal</th>
								<th style="text-align: center;">Saldo Awal</th>
								<th style="text-align: center;">Masuk</th>
								<th style="text-align: center;">Keluar</th>
								<th style="text-align: center;">Adjustment</th>
								<th style="text-align: center;">Saldo Akhir</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th style="width: 100px;text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
							</tr>
						</tfoot>
					</table>
				</div>

				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		disable();
		init_nama();
		$("#kdpart").change(function(){
			 getData();
		});
		$("#rdakun_multi").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});
		$("#rdakun_between").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});

		$('#periode').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$(".btn-tampil").click(function(){
			$(".dataTable").show();
			tampilData();
		});
	});

	function tampilData(){

		var kddiv = $("#kddiv").val();
		var kdpart = $("#kdpart").val();
		var periode = $("#periode").val();
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val();
		var periode_d = $('#rdakun_multi').is(':checked');
		if(periode_d){
			var ket = "periode_multi";
		} else {
			var ket = "periode_between";
		}

		var column = [];

		column.push({
			"aTargets": [ 0,1,2,3,4,5,6,7 ],
			"orderable": false
		});

		column.push({
			"aTargets": [ 2 ],
			"mRender": function (data, type, full) {
				return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});

		column.push({
			"aTargets": [ 3,4,5,6,7 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "ket" },
				{ "data": "noref" },
				{ "data": "tgl" },
				{ "data": "saw" },
				{ "data": "qtyin" },
				{ "data": "qtyout" },
				{ "data": "qtyadj" },
				{ "data": "sak" }
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"order": false,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
							{ "name": "kddiv", "value": kddiv },
							{ "name": "kdpart", "value": kdpart },
							{ "name": "ket", "value": ket },
							{ "name": "periode", "value": periode },
							{ "name": "periode_awal", "value": periode_awal },
							{ "name": "periode_akhir", "value": periode_akhir }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptbkluntks/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			// jumlah TOTAL
			'footerCallback': function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// computing column Total of the complete result
				var so_awal = api
					.column( 3 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var masuk = api
					.column( 4 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var keluar = api
					.column( 5 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var adj = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var so_akhir =  parseInt(so_awal+masuk-keluar+adj);

				// Update footer by showing the total with the reference of the column index
				$( api.column( 2 ).footer() ).html('Total');
				$( api.column( 3 ).footer() ).html(numeral(so_awal).format('0,0'));
				$( api.column( 4 ).footer() ).html(numeral(masuk).format('0,0'));
				$( api.column( 5 ).footer() ).html(numeral(keluar).format('0,0'));
				$( api.column( 6 ).footer() ).html(numeral(adj).format('0,0'));
				$( api.column( 7 ).footer() ).html(numeral(so_akhir).format('0,0'));
			},

			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print', footer: true,
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {

		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
	}

	function disable(){
		$(".dataTable").hide();
		$("#periode").prop("disabled", true);
		$(".btn-tampil").prop("disabled", true);
		$("#periode_awal").prop("disabled", true);
		$("#periode_akhir").prop("disabled", true);
	}

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#periode").prop("disabled", false);
			$("#periode_awal").prop("disabled", true);
			$("#periode_akhir").prop("disabled", true);
		}else{
			$("#periode").prop("disabled", true);
			$("#periode_awal").prop("disabled", false);
			$("#periode_akhir").prop("disabled", false);

		}
	}

	function getData(){
		var kdpart = $("#kdpart").val();
		$.ajax({
			type: "POST",
			url: "<?=site_url("rptbkluntks/getData");?>",
			data: {"kdpart":kdpart},
			beforeSend: function() {

			},
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$.each(obj, function(key, value){});
			},
			error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		});
	}

	function setBgColour(val,object){
		if(val){
			$("#"+object).css("background-color", "#fff");
		}
		else{
			$("#"+object).css("background-color", "#eee");
		}
	}

	function init_nama(){
		$("#kdpart").select2({
			ajax: {
				url: "<?=site_url('rptbkluntks/getNama');?>",
				type: 'post',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
						page: params.page
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 30) < data.total_count
						}
					};
				},
				cache: true
			},
			placeholder: 'Masukkan Nama Aksesoris ...',
			dropdownAutoWidth : false,
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 2,
			templateResult: format_nama, // omitted for brevity, see the source of this page
			templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
		});
	}

	function format_nama_terpilih (repo) {
		return repo.full_name || repo.text;
	}

	function format_nama (repo) {
		if (repo.loading) return "Mencari data ... ";

		var markup = "<div class='select2-result-repository clearfix'>" +
		"<div class='select2-result-repository__meta'>" +
		"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>" +
		"</div>" +
		"</div>";
		return markup;
	}

</script>

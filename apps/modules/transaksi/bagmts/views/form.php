<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            -->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">
                    <div class="box-header">
                        <button type="button" class="btn btn-primary btn-submit">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-default btn-batal">
                            Batal
                        </button>
                    </div>                     

                    <div class="col-md-12">
                        <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div>  

                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-1">
                                <?php echo form_label($form['nopkb']['placeholder']); ?>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nopkb']);
                                        echo form_error('nopkb','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12">
                        <hr>
                    </div>

                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-1">
                                <?php echo form_label($form['tglsv']['placeholder']); ?>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['tglsv']);
                                        echo form_error('tglsv','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <?php echo form_label($form['nopol']['placeholder']); ?>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nopol']);
                                        echo form_error('nopol','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> 

                            <div class="col-md-2">
                                <?php echo form_label($form['nosin']['placeholder']); ?>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nosin']);
                                        echo form_error('nosin','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nora']);
                                        echo form_error('nora','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-1">
                                <?php echo form_label($form['nama']['placeholder']); ?>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nama']);
                                        echo form_error('nama','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <?php echo form_label($form['nama_s']['placeholder']); ?>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nama_s']);
                                        echo form_error('nama_s','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> 

                            <div class="col-md-2">
                                <?php echo form_label($form['alamat_s']['placeholder']); ?>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['alamat_s']);
                                        echo form_error('alamat_s','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-1">
                                <?php echo form_label($form['ketsv']['placeholder']); ?>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['ketsv']);
                                        echo form_error('ketsv','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>  

                            <div class="col-md-2">
                                <?php echo form_label($form['solvesv']['placeholder']); ?>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['solvesv']);
                                        echo form_error('solvesv','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12"> 
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered display nowrap detailTable" style="width: 100%;" class="display select">
                                <thead>
                                    <tr> 
                                        <th style="width: 1%;text-align: center;">No</th>
                                        <th style="text-align: center;">Grup</th>
                                        <th style="text-align: center;">Nama</th>
                                        <th style="text-align: center;">Jenis</th>
                                        <th style="text-align: center;">No Part</th>
                                        <th style="text-align: center;">Kuantitas</th>
                                        <th style="text-align: center;">Harga</th>
                                        <th style="text-align: center;">Diskon</th> 
                                        <th style="text-align: center;">Total</th>  
                                        <th style="text-align: center;">Edit</th>  
                                    </tr>
                                </thead> 
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th style="text-align: center;"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>  

                </div>
            </div>  

 

            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>

<!-- modal dialog -->
<div id="modal_edit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Jurnal</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">  

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    // echo form_label($form['e_nopkb']['placeholder']);
                                    echo form_input($form['e_nopkb']);
                                    echo form_error('e_nopkb','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>
                                    

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_jenis']['placeholder']);
                                    echo form_input($form['e_jenis']);
                                    echo form_error('e_jenis','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

<!--                     <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_jenispekerjaan']['placeholder']);
                                    echo form_dropdown($form['e_jenispekerjaan']['name'],$form['e_jenispekerjaan']['data'] ,$form['e_jenispekerjaan']['value'] ,$form['e_jenispekerjaan']['attr']);
                                    echo form_error('e_jenispekerjaan','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div> -->

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_jenispekerjaan']['placeholder']);
                                    echo form_input($form['e_jenispekerjaan']);
                                    echo form_error('e_jenispekerjaan','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <!-- <div class="form-group"> -->
                                <?php
                                    // echo form_label($form['e_idjob']['placeholder']);
                                    echo form_input($form['e_idjob']);
                                    // echo form_error('e_idjob','<div class="note">','</div>');
                                  ?>
                            <!-- </div> -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_namapekerjaan']['placeholder']);
                                    echo form_input($form['e_namapekerjaan']);
                                    echo form_error('e_namapekerjaan','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_qty']['placeholder']);
                                    echo form_input($form['e_qty']);
                                    echo form_error('e_qty','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_harga']['placeholder']);
                                    echo form_input($form['e_harga']);
                                    echo form_error('e_harga','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_disk']['placeholder']);
                                    echo form_input($form['e_disk']);
                                    echo form_error('e_disk','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['e_totharga']['placeholder']);
                                    echo form_input($form['e_totharga']);
                                    echo form_error('e_totharga','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button> 
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>



<script type="text/javascript">
    $(document).ready(function () { 
        $("#load").hide();
        $("#e_um").val('');
        $("#e_disk").val('');
        $("#e_harga").val('');
        $("#e_totharga").val(''); 
        $("#e_um").autoNumeric();
        $("#e_disk").autoNumeric();
        $("#e_harga").autoNumeric();
        $("#e_totharga").autoNumeric(); 
        $('#e_jenispekerjaan').val('');
        disabled();     
        // getJenispekerjaan('JASA');

        $('.btn-batal').click(function () { 
            batal();
        });   

        $('.btn-simpan').click(function (){
            save();
        });

        $('.btn-submit').click(function (){
            submit();
        });

        $('#e_harga').keyup(function() {
            var jenis = $('#e_jenis').val();
            var harga = $('#e_harga').autoNumeric('get');
            var disk = $('#e_disk').autoNumeric('get'); 
            var qty = $('#e_qty').val(); 
            total = (harga * qty) - disk;
            $('#e_totharga').autoNumeric('set',total);
            if (jenis==='PARTS') {
                limit = harga * (10/100);
            } else {
                // limit = harga ;//* (50/100);
                limit = 100000000 ;//* (50/100);
            }
            if (harga=0&&jenis==='JASA') {
                $('.btn-simpan').attr('disabled',false);
            } else {
                if (disk>limit) {
                    swal({
                        title: "Error",
                        text: "Tidak boleh dapat diskon lebih dari limit",
                        type: "error"
                    },function () {
                        $('.btn-simpan').attr('disabled',true);
                    }); 
                } else {
                    $('.btn-simpan').attr('disabled',false);
                    
                }
            }
        });

        $('#e_disk').keyup(function() {
            var jenis = $('#e_jenis').val();
            var harga = $('#e_harga').autoNumeric('get');
            var disk = $('#e_disk').autoNumeric('get'); 
            var qty = $('#e_qty').val(); 
            total = (harga * qty) - disk;
            $('#e_totharga').autoNumeric('set',total);
            if (jenis==='PARTS') {
                limit = harga * (10/100);
            } else {
                // limit = harga ;//* (50/100);
                limit = 100000000 ;//* (50/100);
            }
            if (harga=0) {
                $('.btn-simpan').attr('disabled',false);
            } else {
                if (disk>limit) {
                    swal({
                        title: "Error",
                        text: "Tidak boleh dapat diskon lebih dari limit",
                        type: "error"
                    },function () {
                        $('.btn-simpan').attr('disabled',true);
                    }); 
                } else {
                    $('.btn-simpan').attr('disabled',false);
                } 
            }
        });

        // $('#e_jenispekerjaan').change(function() {
        //     set_nmpart();
        // });

        var nopkb = $('#nopkb').val();

        table = $('.detailTable').DataTable({
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "aoColumnDefs": [
                {
                    "aTargets": [6,7,8],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"  
                }],
            "columns": [
                { "data": "no"},
                { "data": "jenis"},
                { "data": "namapekerjaan" },
                { "data": "jenispekerjaan"},
                { "data": "partsnumber" },
                { "data": "kuantitas" },
                { "data": "harga" },
                { "data": "diskon"},  
                { "data": "totalharga"},
                { "data": "edit"},
            ], 
            // "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nopkb", "value": nopkb }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('bagmts/json_dgview_detail');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                //dom: '<"html5buttons"B>lTfgitp',
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 8 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 7 ).footer() ).html('Total');
                $( api.column( 8 ).footer() ).html(numeral(total).format('0,0'));
            },
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        }); 
        
        // $('.detailTable').tooltip({
        //     selector: "[data-toggle=tooltip]",
        //     container: "body"
        // });  

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.detailTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        }); 

        function disabled(){
            $(".upload_kb").hide();
            $("#kddiv").attr("disabled",false);
            $(".btn-set").show();
            $(".btn-reset").hide();
            $('.form-import').hide();
            //$("#fileKB").remove();
            //$("#fileBeli").remove();
            //$("#fileJual").remove();
            //$("#fileServis").remove();
        }

        function enabled(){
            $("#kddiv").attr("disabled",true);
            $(".btn-set").hide();
            $(".btn-reset").show();
            $('.form-import').show();
        }


    });  

    function edit(noworkorder,jenis,idjob,partsnumber,diskon,harga,totalharga,namapekerjaan,jenispekerjaan,qty) {
        // getJenispekerjaan(jenis);
        // alert(jenispekerjaan);
        if (jenispekerjaan==='ASS 1' || jenispekerjaan==='ASS 2' || jenispekerjaan==='ASS 3' || jenispekerjaan==='ASS 4' || jenispekerjaan==='C2') {
            swal({
                title: "Errorr",
                text: "error",
                type: "error"
            });
        } else {
            $('#modal_edit').modal('toggle');
            
            $("#e_jenispekerjaan").val(partsnumber); 
            $("#e_qty").val(qty); 
            // $("#e_jenispekerjaan").trigger("change");
            // $('#e_jenispekerjaan').select2({
            //               dropdownAutoWidth : true,
            //               width: '100%'
            //             });
            $('#e_nopkb').val(noworkorder);
            $('#e_jenis').val(jenis);
            $('#e_idjob').val(idjob);  
            $("#e_disk").autoNumeric('set',diskon);
            $("#e_harga").autoNumeric('set',harga);
            var tot = (harga*qty);
            $("#e_totharga").autoNumeric('set',tot); 
            $('#e_namapekerjaan').val(namapekerjaan); 
            if (totalharga==='0'&&jenis==='JASA') {
                $('.btn-simpan').attr('disabled',false);
            } else { 
                $('.btn-simpan').attr('disabled',true);
            }
            // alert(harga);
        }
    }

    function batal(){ 
        var nopkb = $('#nopkb').val();
        swal({
            title: "Konfirmasi Batal Simpan Nota!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Batalkan!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false
        }, function () {
        // var jnstrans = $("#jnstrans").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("bagmts/batal");?>",
                data: { "nopkb": nopkb},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function() {
                            window.location.href = '<?=site_url('bagmts');?>';  
                        }); 
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }); 
    }

    function save(){  
        var nopkb = $("#nopkb").val(); 
        var jenis = $("#e_jenis").val(); 
        var idjob = $("#e_idjob").val(); 
        var partsnumber = $("#e_jenispekerjaan").val(); 
        var nmkerja = $("#e_namapekerjaan").val(); 
        var disk = $("#e_disk").autoNumeric('get'); 
        var harga = $("#e_harga").autoNumeric('get'); 
        var total = $("#e_totharga").autoNumeric('get'); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("bagmts/update");?>",
            data: { "nopkb": nopkb, "disk":disk, "total":total, "jenis":jenis, "idjob":idjob, "partsnumber":partsnumber, "nmkerja":nmkerja, "harga":harga},
            success: function(resp){ 
                $("#modal_edit").modal("hide");
                table.ajax.reload();  
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function submit(){  
        var nopkb = $("#nopkb").val();  
        $.ajax({
            type: "POST",
            url: "<?=site_url("bagmts/submit");?>",
            data: { "nopkb": nopkb},
            beforeSend: function() {
                $('.btn-submit').attr('disabled',true);
            },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    $('.btn-submit').attr('disabled',false);
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function() {
                        window.open('../ctk_nt/'+nopkb, '_blank');
                        window.location.href = '<?=site_url('bagmts');?>';  
                    }); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function getJenispekerjaan(jenis){ 
        // alert(partsnumber);
        $.ajax({
            type: "POST",
            url: "<?=site_url("bagmts/getJenispekerjaan");?>",
            data: {"jenis":jenis},
            beforeSend: function() {
                $('#e_jenispekerjaan').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis  --'));
                $("#e_jenispekerjaan").trigger("change.chosen");
                if ($('#e_jenispekerjaan').hasClass("chosen-hidden-accessible")) {
                    $('#e_jenispekerjaan').select2('destroy');
                    $("#e_jenispekerjaan").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#e_jenispekerjaan')
                        .append($('<option>', { value : value.kdpart })
                        .html("<b style='font-size: 14px;'>" + value.nmpart + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmpart + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_nmpart(){
        var kdpart = $('#e_jenispekerjaan').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("bagmts/set_nmpart");?>",
            data: { "kdpart": kdpart},
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    $('#e_namapekerjaan').val(data.nmpart);
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 

    }
</script>

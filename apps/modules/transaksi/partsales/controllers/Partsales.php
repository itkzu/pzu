<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Partsales
 *
 * @author adi
 */
class Partsales extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('partsales/submit'),
            'html' => site_url('partsales/html'),
            'add' => site_url('partsales/add'),
            'edit' => site_url('partsales/edit'),
            'reload' => site_url('partsales'),
        );
        $this->load->model('partsales_qry');
        $trm = $this->partsales_qry->getNoSO(); 
        $this->data['noso'] = array(
            "" => "SO CABANG / BAG", 
        );
        foreach ($trm as $value) {
            $this->data['noso'][$value['noso']] = $value['noso']." - ".$value['namacustomer'];
        }

        $cabang = $this->partsales_qry->getDataSupplier();
        foreach ($cabang as $value) {
            $this->data['supplier'][$value['kdsupplier']] = $value['nmsupplier'];
        }

        $cabang = $this->partsales_qry->getPart(); 
        foreach ($cabang as $value) {
            $this->data['part'][$value['kdpart']] = $value['kdpart']." - ".$value['nmpart'];
        }

        $this->data['tipebayar'] = array(
            "Lunas" => "TUNAI",
            "Kredit" => "KREDIT"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    
    public function html() {  
        $noso = $this->uri->segment(3); 
        $this->data['data'] = $this->partsales_qry->html($noso);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);  
    } 
    
    public function html2() {  
        $noso = $this->uri->segment(3); 
        $this->data['data'] = $this->partsales_qry->html($noso);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html2',$this->data);  
    } 

    //import
    public function get_SO() {
        echo $this->partsales_qry->get_SO();
    }

    public function ins_SO() {
        echo $this->partsales_qry->ins_SO();
    }

    public function save_SO() {
        echo $this->partsales_qry->save_SO();
    }

    //set
    public function set_nmpart() {
        echo $this->partsales_qry->set_nmpart();
    }

    public function set_bagal() {
        echo $this->partsales_qry->set_bagal();
    }

    public function set_nmpel() {
        echo $this->partsales_qry->set_nmpel();
    }

    //table
    public function json_dgview_detail() {
        echo $this->partsales_qry->json_dgview_detail();
    }

    public function batal() {
        echo $this->partsales_qry->batal();
    }

    public function update() {
        echo $this->partsales_qry->update();
    }

    public function submit() {
        echo $this->partsales_qry->submit();
    }

    public function det_update() {
        echo $this->partsales_qry->det_update();
    }

    public function del() {
        echo $this->partsales_qry->del();
    }

    public function json_dgview() {
        echo $this->partsales_qry->json_dgview();
    }

    public function json_dgview_bag() {
        echo $this->partsales_qry->json_dgview_bag();
    }

    public function json_dgview_prs() {
        echo $this->partsales_qry->json_dgview_prs();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'periode_so'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_so',
                    'name'        => 'periode_so',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_bag'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_bag',
                    'name'        => 'periode_bag',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_prs'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_prs',
                    'name'        => 'periode_prs',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
            // 'notrm'         => array(
            //     'attr'      => array(
            //         'id'        => 'notrm',
            //         'class'     => 'form-control',
            //         // 'multiple'  => '',
            //     ),
            //     'data'      => $this->data['trm'],
            //     'value'       => set_value('notrm'),
            //     'name'      => 'notrm',
            // ),
            'notrm'    => array( 
                'attr'      => array(
                    'id'      => 'notrm',
                    'class'   => 'form-control',
                ),
                'data'        => $this->data['noso'],
                'class'       => 'form-control',
                'value'       => set_value('notrm'),
                'name'        => 'notrm',
                // 'readonly' => '',
            ),

            'bag_kdsup'    => array(
                'placeholder' => 'Cabang Lain',
                'attr'      => array(
                    'id'      => 'bag_kdsup',
                    'class'   => 'form-control',
                ),
                'data'        => $this->data['supplier'],
                'class'       => 'form-control',
                'value'       => set_value('bag_kdsup'),
                'name'        => 'bag_kdsup',
                // 'readonly' => '',
            ), 
            'bag_alamat'        => array(
                'placeholder' => 'Alamat Cabang Lain',
                'id'          => 'bag_alamat',
                'name'        => 'bag_alamat',
                'value'       => set_value('bag_alamat'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                'readonly'    => ''
            ),

            'nama'        => array(
                'placeholder' => 'Nama Pelanggan',
                'id'          => 'nama',
                'name'        => 'nama',
                'value'       => set_value('nama'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                'readonly'    => ''
            ),
            'alamat'        => array(
                'placeholder' => 'Alamat Pelanggan',
                'id'          => 'alamat',
                'name'        => 'alamat',
                'value'       => set_value('alamat'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                // 'readonly'    => ''
            ),
            'nik'        => array(
                'placeholder' => 'NIK Pelanggan',
                'id'          => 'nik',
                'name'        => 'nik',
                'value'       => set_value('nik'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                // 'readonly'    => ''
            ),

            'tipebayar'    => array(
                'placeholder' => 'Tipe Bayar',
                'attr'      => array(
                    'id'      => 'tipebayar',
                    'class'   => 'form-control',
                ),
                'data'        => $this->data['tipebayar'],
                'class'       => 'form-control',
                'value'       => set_value('tipebayar'),
                'name'        => 'tipebayar',
                // 'readonly' => '',
            ),
            'tglso'        => array(
                'placeholder' => 'Tanggal',
                'id'          => 'tglso',
                'name'        => 'tglso',
                'value'       => date('d-m-Y'),
                'class'       => 'form-control calendar',
                'style'       => '',
                // 'readonly'    => ''
            ),

            //modal
            'm_nofa'        => array(
                'placeholder' => 'No. Faktur',
                'id'          => 'm_nofa',
                'name'        => 'm_nofa',
                'value'       => set_value('m_nofa'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                // 'readonly'    => ''
            ),
            'kdpart'        => array(
                'placeholder' => 'Kode Sparepart',
                'id'          => 'kdpart',
                'name'        => 'kdpart',
                'value'       => set_value('kdpart'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                'readonly'    => ''
            ),
            'qty'        => array(
                'placeholder' => 'Quantity',
                'id'          => 'qty',
                'name'        => 'qty',
                'value'       => set_value('qty'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                'readonly'    => ''
            ),
            'harga'        => array(
                'placeholder' => 'Harga',
                'id'          => 'harga',
                'name'        => 'harga',
                'value'       => set_value('harga'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'discper'        => array(
                'placeholder' => 'Discount Percentage %',
                'id'          => 'discper',
                'name'        => 'discper',
                'value'       => set_value('discper'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'disc'        => array(
                'placeholder' => 'Discount',
                'id'          => 'disc',
                'name'        => 'disc',
                'value'       => set_value('disc'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'total'        => array(
                'placeholder' => 'Total Harga',
                'id'          => 'total',
                'name'        => 'total',
                'value'       => set_value('total'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                'readonly'    => ''
            ),

            'nmpart'        => array(
                'placeholder' => 'Nama Sparepart',
                'id'          => 'nmpart',
                'name'        => 'nmpart',
                'value'       => set_value('nmpart'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                'readonly'    => ''
            ),

            //modal add
            'add_kdpart'        => array(
                'placeholder' => 'Kode Sparepart',
                'attr'      => array(
                    'id'      => 'add_kdpart',
                    'class'   => 'form-control',
                ),
                'data'        => $this->data['part'],
                'class'       => 'form-control',
                'value'       => set_value('add_kdpart'),
                'name'        => 'add_kdpart',
                // 'readonly' => '',
            ),
            'add_nmpart'        => array(
                'placeholder' => 'Nama Sparepart',
                'id'          => 'add_nmpart',
                'name'        => 'add_nmpart',
                'value'       => set_value('add_nmpart'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;',
                'readonly'    => ''
            ),
            'add_qty'        => array(
                'placeholder' => 'Quantity',
                'id'          => 'add_qty',
                'name'        => 'add_qty',
                'value'       => set_value('add_qty'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'add_harga'        => array(
                'placeholder' => 'Harga',
                'id'          => 'add_harga',
                'name'        => 'add_harga',
                'value'       => set_value('add_harga'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'add_discper'        => array(
                'placeholder' => 'Discount Percentage %',
                'id'          => 'add_discper',
                'name'        => 'add_discper',
                'value'       => set_value('add_discper'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'add_disc'        => array(
                'placeholder' => 'Discount',
                'id'          => 'add_disc',
                'name'        => 'add_disc',
                'value'       => set_value('add_disc'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                // 'readonly'    => ''
            ),
            'add_total'        => array(
                'placeholder' => 'Total Harga',
                'id'          => 'add_total',
                'name'        => 'add_total',
                'value'       => set_value('add_total'),
                'class'       => 'form-control',
                'style'     => 'text-transform: uppercase;text-align: right',
                'readonly'    => ''
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(
          // 'nodf'=> array(
          //          'placeholder' => 'No. DF',
          //          //'type'        => 'hidden',
          //          'id'          => 'nodf',
          //          'name'        => 'nodf',
          //          'value'       => $this->val[0]['nodf'],
          //          'class'       => 'form-control',
          //          'style'       => '',
          //          'readonly'    => '',
          //  ),

              'noso'=> array(
                       'placeholder' => 'No. SPK',
                       //'type'        => 'hidden',
                       'id'          => 'noso',
                       'name'        => 'noso',
                       'value'       => set_value('noso'),
                       'class'       => 'form-control',
                       'style'       => '',
               ),
              'tglso'=> array(
                       'placeholder' => 'Tanggal SPK',
                       'id'          => 'tglso',
                       'name'        => 'tglso',
                       'value'       => date('d-m-Y'),
                       'class'       => 'form-control calendar',
                       'style'       => '',
               ),
              'nospk'=> array(
                        'attr'        => array(
                            'id'    => 'nospk',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                       'placeholder' => 'No. SPK HSO',
                       'name'        => 'nospk',
                       'value'       => set_value('nospk'),
                       'style'       => '',
               ),
               'nama1'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama1',
                        'name'        => 'nama1',
                        'value'       => set_value('nama1'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat1'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat1'),
                       'id'          => 'alamat1',
                       'name'        => 'alamat1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel1'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel1'),
                       'id'          => 'kel1',
                       'name'        => 'kel1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'noktp'=> array(
                       'placeholder' => 'No KTP',
                       'value'       => set_value('noktp'),
                       'id'          => 'noktp',
                       'name'        => 'noktp',
                       'class'       => 'form-control',
                       // 'type'        => 'hidden',
                       'style'       => '',
               ),
               'kec1'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec1'),
                       'id'          => 'kec1',
                       'name'        => 'kec1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'hohp1'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp1'),
                       'id'          => 'hohp1',
                       'name'        => 'hohp1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'notelp'=> array(
                       'placeholder' => 'No. Telp',
                       // 'type'        =>  'hidden',
                       'value'       => set_value('nsel'),
                       'id'          => 'nsel',
                       'name'        => 'nsel',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota1'=> array(
                       'placeholder' => 'Kota',
                       'value'       => set_value('kota1'),
                       'id'          => 'kota1',
                       'name'        => 'kota1',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'npwp'=> array(
                       'placeholder' => 'Npwp',
                       'value'       => set_value('npwp'),
                       'id'          => 'npwp',
                       'name'        => 'npwp',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'nama2'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama2',
                        'name'        => 'nama2',
                        'value'       => set_value('nama2'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat2'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat2'),
                       'id'          => 'alamat2',
                       'name'        => 'alamat2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel2'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel2'),
                       'id'          => 'kel2',
                       'name'        => 'kel2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kec2'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec2'),
                       'id'          => 'kec2',
                       'name'        => 'kec2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'ket'=> array(
                       // 'placeholder' => 'Kec.',
                       'value'       => set_value('ket'),
                       'id'          => 'ket',
                       'name'        => 'ket',
                       'class'       => 'form-control',
                       'style'       => '',
                       'type'        => 'hidden',
               ),
               'hohp2'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp2'),
                       'id'          => 'hohp2',
                       'name'        => 'hohp2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota2'=> array(
                       'placeholder' => 'Kota',
                       'value'       => set_value('kota2'),
                       'id'          => 'kota2',
                       'name'        => 'kota2',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdtipe'=> array(
                       'attr'        => array(
                           'id'    => 'kdtipe',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Tipe Unit',
                       'value'       => set_value('kdtipe'),
                       'name'        => 'kdtipe',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'nmtipe'=> array(
                       'placeholder' => 'Nama Tipe',
                       'type'        => 'nmtipe',
                       'value'       => set_value('nmtipe'),
                       'id'          => 'nmtipe',
                       'name'        => 'nmtipe',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'kdwarna'=> array(
                       'attr'        => array(
                           'id'    => 'kdwarna',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Kode Warna',
                       'value'       => set_value('kdwarna'),
                       'name'        => 'kdwarna',
                       'style'       => '',
               ),
               'nmwarna'=> array(
                       'placeholder' => 'Nama Warna',
                       // 'type'        => 'hidden',
                       'value'       => set_value('nmwarna'),
                       'id'          => 'nmwarna',
                       'name'        => 'nmwarna',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'tipebayar'=> array(
                       'placeholder' => 'Pembayaran',
                       'attr'        => array(
                           'id'    => 'tipebayar',
                           'class' => 'form-control select',
                       ),
                       'data'     => $this->data['tipebayar'],
                       'value'    => set_value('tipebayar'),
                       'name'     => 'tipebayar',
                       'required'    => '',
               ),
               'kdsales'=> array(
                       'placeholder' => 'Salesman',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdsales'),
                       'id'          => 'kdsales',
                       'name'        => 'kdsales',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdspv'=> array(
                       'placeholder' => 'Superviser',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdspv'),
                       'id'          => 'kdspv',
                       'name'        => 'kdspv',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->partsales_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashallcabang_qry
 *
 * @author adi
 */
class Dashallcabang_qry extends CI_Model {

    //put your code here
    protected $res = "";
    protected $delete = "";
    protected $state = "";


    /*
    protected $url_ayani    = "pzusmg1.ddns.net";
    protected $url_brebes   = "pzubrebes1.ddns.net";
    protected $url_kudus    = "pzukudus1.ddns.net";
    protected $url_pati     = "pzupati1.ddns.net";
    protected $url_pwd      = "pzupwd1.ddns.net";
    protected $url_stbd     = "zirang.ddns.net:8080";
    */



    public function __construct() {
        parent::__construct();
    }


    protected function set_url_cabang($nama_cabang) {
        switch ($nama_cabang) {
            case "PZU A.YANI":
                // $url = "localhost/pzu";
                $url = "pzusmg1.ddns.net";
                //$url = "localhost/pzu";
                //$url = $this->$url_ayani;
                break;
            case "PZU BREBES":
            // $url = "pzubrebes1.ddns.net";
                // $url = "localhost/pzu";
                $url = "zirang.ddns.net:8085";
                //$url = $this->$url_brebes;
                break;
            case "PZU KUDUS":
                // $url = "localhost/pzu";
                // $url = "pzukudus1.ddns.net";
                $url = "zirang.ddns.net:8082";
                //$url = $this->$url_kudus;
                break;
            case "PZU PATI":
                // $url = "localhost/pzu";
                // $url = "pzupati1.ddns.net";
                $url = "zirang.ddns.net:8081";
                //$url = $this->$url_pati;
                break;
            case "PZU PURWODADI":
                // $url = "localhost/pzu";
                // $url = "pzupwd1.ddns.net";
                $url = "zirang.ddns.net:8083";
                //$url = $this->$url_pwd;
                break;
            case "PZU SETIABUDI":
                // $url = "pzusmg2.ddns.net";
                $url = "zirang.ddns.net:8080";
                // $url = "localhost/pzu";
                //$url = $this->$url_stbd;
                break;
            case "localhost":
                // $url = "pzusmg2.ddns.net";
                // $url = "localhost";
                $url = "localhost/pzu";
                //$url = $this->$url_stbd;
                break;
        }
        return $url;
    }


    public function get_Jual_All_Cab($nama_cabang, $periode) {
        try {

            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getJualAllCab",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }





    public function get_target_jual($nama_cabang, $periode) {
        try {

            $url = $this->set_url_cabang($nama_cabang);


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/gettargetbulan",
                // CURLOPT_URL => "http://localhost/pzu/android/gettargetbulan",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }



    public function get_blm_trm_po_leasing($nama_cabang, $periode) {
        try {

            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getblmtrmpoleasing",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . substr($periode, 0, 7),
                    "nm_cabang:" . $nama_cabang,
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }


    public function get_blm_trm_po_leasing_detail() {
        $nama_cabang = $this->input->post('cabang');
        $kdleasing = $this->input->post('kdleasing');
        $periode = date('Y-m-d');
        try {

            $url = $this->set_url_cabang($nama_cabang);


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getblmtrmpoleasingdetail",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . substr($periode, 0, 7),
                    "nm_cabang: " . $nama_cabang,
                    "kdleasing: ".$kdleasing
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }

    function is_json($string, $return_data = false) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }




    public function get_Jual_All_Cab_leasing($nama_cabang, $periode) {
        try {

            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getJualAllCabLeas",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }


    public function get_Jual_tipex_All_Cab($nama_cabang, $periode) {
        try {

            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getJualAllCabtipex",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }


    public function get_Jual_tipe_All_Cab($nama_cabang, $periode) {
        try {

            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getJualAllCabtipe",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);
            // echo($response);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }


    public function get_Jual_tipe_year_All_Cab($nama_cabang, $periode) {
        try {

            // $url = $this->set_url_cabang('localhost');
            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getJualAllCabtipeyear",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);
            // echo($response);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }


    public function get_stok_m_All_Cab($nama_cabang, $periode) {
        try {

            // $url = $this->set_url_cabang('localhost');
            $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://" . $url . "/android/getStokAllCabtipe",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $periode
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);
            // echo($response);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return $response;
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }

    public function getJualTunaiKreditPerBulan() {
        $tahun = $this->input->post('tahun');
        $tanggal = $this->input->post('tanggal');
        $data = array(  
            'pzusmg1' => $this->_getJualPzusmg1PerBulan($tanggal), 
            'pzusmg1ytd' => $this->_getJualPzusmg1YTD($tanggal),  
            'pzusmg1mtd' => $this->_getJualPzusmg1MTD($tanggal),

            'pzusmg2' => $this->_getJualPzusmg2PerBulan($tanggal), 
            'pzusmg2ytd' => $this->_getJualPzusmg2YTD($tanggal),  
            'pzusmg2mtd' => $this->_getJualPzusmg2MTD($tanggal),

            'pzupati1' => $this->_getJualPzupati1PerBulan($tanggal), 
            'pzupati1ytd' => $this->_getJualPzupati1YTD($tanggal),  
            'pzupati1mtd' => $this->_getJualPzupati1MTD($tanggal),

            'pzukudus1' => $this->_getJualPzukudus1PerBulan($tanggal), 
            'pzukudus1ytd' => $this->_getJualPzukudus1YTD($tanggal),  
            'pzukudus1mtd' => $this->_getJualPzukudus1MTD($tanggal),

            'pzupwd1' => $this->_getJualPzupwd1PerBulan($tanggal), 
            'pzupwd1ytd' => $this->_getJualPzupwd1YTD($tanggal),  
            'pzupwd1mtd' => $this->_getJualPzupwd1MTD($tanggal),

            'pzubrebes1' => $this->_getJualPzubrebes1PerBulan($tanggal), 
            'pzubrebes1ytd' => $this->_getJualPzubrebes1YTD($tanggal),  
            'pzubrebes1mtd' => $this->_getJualPzubrebes1MTD($tanggal),


        );
        return json_encode($data);
    }

    private function _getJualTunaiPerBulan($tahun,$tanggal) {
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'T' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return $res;
    }

    private function _getJualKreditPerBulan($tahun,$tanggal) {
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return $res;
    }

    private function _getJualTunaiYTD($tahun,$tanggal) {
        $this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
        $this->db->group_by("vl_do_real.tk");
        $this->db->order_by("vl_do_real.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'T' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;
    }

    private function _getJualKreditYTD($tahun,$tanggal) {
        $this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
        $this->db->group_by("vl_do_real.tk");
        $this->db->order_by("vl_do_real.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') BETWEEN '".$tahun."-01' AND '".$tanggal."')) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;
    }

    private function _getJualTunaiMTD($tanggal) {
        $this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
        $this->db->group_by("vl_do_real.tk");
        $this->db->order_by("vl_do_real.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'T' AND (to_char(tgldo,'YYYY-MM') = '".$tanggal."')) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;
    }

    private function _getJualKreditMTD($tanggal) {
        $this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
        $this->db->group_by("vl_do_real.tk");
        $this->db->order_by("vl_do_real.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') = '".$tanggal."')) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;
    }


    public function _getJualPzusmg1PerBulan($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://pzusmg1.ddns.net/android/getJualPerBulanKredit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzusmg1YTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://pzusmg1.ddns.net/android/getJualYTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzusmg1MTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://pzusmg1.ddns.net/android/getJualMTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    } 


    public function _getJualPzusmg2PerBulan($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8080/android/getJualPerBulanKredit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzusmg2YTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8080/android/getJualYTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzusmg2MTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8080/android/getJualMTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }  


    public function _getJualPzupati1PerBulan($tanggal) { 
        try {

            // $url = $this->set_url_cabang($nama_cabang);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8081/android/getJualPerBulanKredit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            // echo $response;
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        } 
    }

    public function _getJualPzupati1YTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8081/android/getJualYTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    // return $response;
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzupati1MTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8081/android/getJualMTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);
            // echo $err;

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    // return $response;
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    } 


    public function _getJualPzukudus1PerBulan($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8082/android/getJualPerBulanKredit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzukudus1YTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8082/android/getJualYTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzukudus1MTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8082/android/getJualMTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    } 


    public function _getJualPzupwd1PerBulan($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8083/android/getJualPerBulanKredit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzupwd1YTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8083/android/getJualYTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzupwd1MTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8083/android/getJualMTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    } 


    public function _getJualPzubrebes1PerBulan($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8085/android/getJualPerBulanKredit",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzubrebes1YTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8085/android/getJualYTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    public function _getJualPzubrebes1MTD($tanggal) {
        try {
 

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://zirang.ddns.net:8085/android/getJualMTD",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: pzu",
                    "periode: " . $tanggal
                ),
            ));




            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                if (strpos("Could not resolve ", $err) > 0) {
                    throw new Exception("Request gagal, Kemungkinan koneksi anda bermasalah atau server tidak aktif. Silahkan Coba beberapa saat lagi.");
                } else {
                    throw new Exception("Request gagal, Silahkan Coba beberapa saat lagi.");
                }
            } else {
                if($this->is_json($response)){
                    return json_decode($response);
                }
            }
        } catch (Exception $e) {
            return 0;
        }
    } 

}

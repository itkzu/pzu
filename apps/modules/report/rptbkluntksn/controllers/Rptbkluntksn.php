<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbkluntksn
 *
 * @author
 */

class Rptbkluntksn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptbkluntksn/submit'),
            'add' => site_url('rptbkluntksn/add'),
            'edit' => site_url('rptbkluntksn/edit'),
            'reload' => site_url('rptbkluntksn'),
        );
        $this->load->model('rptbkluntksn_qry');
        $aks = $this->rptbkluntksn_qry->getAks();
        foreach ($aks as $value) {
            $this->data['kdpart'][$value['kdpart']] = $value['nmpart'];
        }
        
        $this->data['kddiv'] = array(
            "ZPH01.01B" => "AHMAD YANI",
            "ZPH01.02B" => "PATI",
            "ZPH02.01B" => "KUDUS",
            "ZPH03.01B" => "BREBES",
            "ZPH04.01B" => "SETIABUDI"
          );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->rptbkluntksn_qry->json_dgview();
    }

    public function getNama() {
        echo $this->rptbkluntksn_qry->getNama();
    }

    public function getData() {
        echo $this->rptbkluntksn_qry->getData();
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                    ),
                    'placeholder' => 'Cabang Bengkel',
                    'data'     => $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '', 
            ), 
            'kdpart'=> array(
                  'attr'        => array(
                      'id'    => 'kdpart',
                      'class' => 'form-control',
                  ),
                  'data'     => '',
                  //$this->data['kdpart']
                  'value'    => set_value('kdpart'),
                  'name'     => 'kdpart',
                  'placeholder'=> "Nama Aksesoris"
            ),
            'periode'=> array(
                     'placeholder' => 'Periode',
                     'id'          => 'periode',
                     'name'        => 'periode',
                     'value'       => date('Y-m'),
                     'class'       => 'form-control',
                     'style'       => 'margin-left: 5px;',
                     'required'    => '',
            ),
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
        );
    }
}

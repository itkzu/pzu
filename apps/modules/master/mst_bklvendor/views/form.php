<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data' );
                echo form_open($submit,$attributes);
            ?>
            <div class="box-body">

                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">
                            <?php
                                echo form_input($form['kdsupplier']);

                                echo form_label($form['nmsupplier']['placeholder']);
                                echo form_input($form['nmsupplier']);
                                echo form_error('nmsupplier','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">
                            <?php
                                echo form_label($form['kdcab']['placeholder']);
                                echo form_dropdown( $form['kdcab']['name'],
                                                    $form['kdcab']['data'] ,
                                                    $form['kdcab']['value'] ,
                                                    $form['kdcab']['attr']);
                                echo form_error('kdcab','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">
                            <?php 
                                echo form_label($form['alamat']['placeholder']);
                                echo form_input($form['alamat']);
                                echo form_error('alamat','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="row">
                        <div class="form-group">
                            <?php 
                                echo form_label($form['npwp']['placeholder']);
                                echo form_input($form['npwp']);
                                echo form_error('npwp','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="row">
                        <div class="form-group"> 
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="row">
                        <div class="form-group">
                            <?php 
                                echo form_label($form['nohp']['placeholder']);
                                echo form_input($form['nohp']);
                                echo form_error('nohp','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="row">
                        <div class="form-group"> 
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="row">
                        <div class="form-group">
                            <?=form_label($form['faktif']['placeholder']);?>
                            <div class="checkbox">
                				<?php
                                    echo form_checkbox($form['faktif']);
                				?>
                			</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <a href="<?php echo $reload;?>" class="btn btn-default">
                    Batal
                </a>
            </div>
            <?php echo form_close(); ?>
        </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        if(!$('#kdsupplier').val()){
            $('#faktif').hide();
            $('.btn-submit').click(function(){
                submit();
            });
        } else {
            $('#faktif').show();
            $('#faktif').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            });
            $('.btn-submit').click(function(){
                update();
            });
        }
        //getKategori();
    }); 

    function submit(){
        var nmsupplier  = $("#nmsupplier").val().toUpperCase();
        var alamat      = $("#alamat").val().toUpperCase();
        var npwp        = $("#npwp").val();
        var nohp        = $("#nohp").val();
        var kdcab       = $("#kdcab").val();
        //alert(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mst_bklvendor/submit");?>",
      		data: {"nmsupplier":nmsupplier,"alamat":alamat,"npwp":npwp,"nohp":nohp,"kdcab":kdcab}, 
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        if (data.tipe==="success"){
                            window.location.href = '<?=site_url('mst_bklvendor');?>';
                        }else{
                            refresh();
                        }
                    });
      			});
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function update(){
        var kdsupplier  = $("#kdsupplier").val();
        var nmsupplier  = $("#nmsupplier").val().toUpperCase();
        var alamat      = $("#alamat").val().toUpperCase();
        var npwp        = $("#npwp").val();
        var nohp        = $("#nohp").val();
        var kdcab       = $("#kdcab").val();
        if ($("#faktif").prop("checked")){
          var faktif = true;
        } else {
          var faktif = false;
        }
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mst_bklvendor/update");?>",
      		data: {"kdsupplier":kdsupplier,"nmsupplier":nmsupplier,"alamat":alamat,"npwp":npwp,"nohp":nohp,"kdcab":kdcab,"faktif":faktif}, 
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        if (data.tipe==="success"){
                            window.location.href = '<?=site_url('mst_bklvendor');?>';
                        }else{
                            refresh();
                        }
                    });
      			});
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function refresh(){
      window.location.reload();
    }
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptadharmatriks
 *
 * @author adi
 */
class Rptadharmatriks extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptadharmatriks/submit'),
            'add' => site_url('rptadharmatriks/add'),
            'edit' => site_url('rptadharmatriks/edit'),
            'reload' => site_url('rptadharmatriks'),
        );
        $this->load->model('rptadharmatriks_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function get_data() {
        echo $this->rptadharmatriks_qry->get_data();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptadharmatriks_qry->get_data();
            $this->data['decode'] = $res;
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('export_html',$this->data);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('01-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_akhir',
                    'label' => 'Periode Akhir',
                    'rules' => 'required|max_length[20]',
                    ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Bklcek_promo
 *
 * @author adi
 */
class Bklcek_promo extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('bklcek_promo/submit'),
            'add' => site_url('bklcek_promo/add'),
            'edit' => site_url('bklcek_promo/edit'),
            'reload' => site_url('bklcek_promo'),
        );
        $this->load->model('bklcek_promo_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function getNama() {
        echo $this->bklcek_promo_qry->getNama();
    }

    public function getDataBayar() {
        echo $this->bklcek_promo_qry->getDataBayar();
    }

    public function proses() {
        echo $this->bklcek_promo_qry->proses();
    }
    public function getDataUnit() {
        echo $this->bklcek_promo_qry->getDataUnit();
    }
    public function getDataKPB() {
        echo $this->bklcek_promo_qry->getDataKPB();
    } 

    private function _init_add(){
        if(isset($_POST['cx_kpb1']) && strtoupper($_POST['cx_kpb1']) == 'CHECKED'){
            $cx_kpb1 = TRUE;
        } else{
            $cx_kpb1 = FALSE;
        }
        if(isset($_POST['cx_kpb2']) && strtoupper($_POST['cx_kpb2']) == 'CHECKED'){
            $cx_kpb2 = TRUE;
        } else{
            $cx_kpb2 = FALSE;
        }
        if(isset($_POST['cx_kpb3']) && strtoupper($_POST['cx_kpb3']) == 'CHECKED'){
            $cx_kpb3 = TRUE;
        } else{
            $cx_kpb3 = FALSE;
        }
        if(isset($_POST['cx_kpb4']) && strtoupper($_POST['cx_kpb4']) == 'CHECKED'){
            $cx_kpb4 = TRUE;
        } else{
            $cx_kpb4 = FALSE;
        }
        $this->data['form'] = array(
            'nama'=> array(
                    'placeholder' => 'Masukkan Kata Kunci <small>(Nama Pemilik / No. Polisi / No. DO / No. BPKB)</small>',
                    'attr'        => array(
                        'id'    => 'nama',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('nama'),
                    'name'     => 'nama',
            ),
            //data utama
            'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    // 'style'       => 'background-color: #fff;',
            ),
            'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ), 
            'nodo'=> array(
                    'placeholder' => 'No. DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ),
            'tgldo'=> array(
                    'placeholder' => 'Tgl DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => set_value('tgldo'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ),
            'nama_s'=> array(
                    'placeholder' => 'Nama Pemilik',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Tipe Motor',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ), 
            'jenis'=> array(
                    'placeholder' => 'Jenis KPB',
                    'id'          => 'jenis',
                    'name'        => 'jenis',
                    'value'       => set_value('jenis'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ), 
            'promo'=> array(
                    'placeholder' => 'Promo',
                    'id'          => 'promo',
                    'name'        => 'promo',
                    'value'       => set_value('promo'),
                    'class'       => 'form-control',
                    'readonly'    => '', 
            ), 

            // KPB 1 - 3

           'c1_kpb1'=> array(
                    'placeholder' => 'KPB 1',
                    'id'          => 'c1_kpb1',
                    'name'        => 'c1_kpb1',
                    'value'       => set_value('c1_kpb1'),
                    'class'       => 'form-control', 
                    'readonly'    => '',
            ),
            
            'faktif_kpb1'=> array(
                    'placeholder' => 'KPB 1',
                    'id'          => 'faktif_kpb1',
                    'value'       => '', 
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif_kpb1',
                    'type'        => 'checkbox',
            ),

           'c1_kpb2'=> array(
                    'placeholder' => 'KPB 2',
                    'id'          => 'c1_kpb2',
                    'name'        => 'c1_kpb2',
                    'value'       => set_value('c1_kpb2'),
                    'class'       => 'form-control', 
                    'readonly'    => '',
            ),
            
            'faktif_kpb2'=> array(
                    'placeholder' => 'KPB 2',
                    'id'          => 'faktif_kpb2',
                    'value'       => '', 
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif_kpb2',
                    'type'        => 'checkbox',
            ),

           'c1_kpb3'=> array(
                    'placeholder' => 'KPB 3',
                    'id'          => 'c1_kpb3',
                    'name'        => 'c1_kpb3',
                    'value'       => set_value('c1_kpb3'),
                    'class'       => 'form-control', 
                    'readonly'    => '',
            ), 
            
            'faktif_kpb3'=> array(
                    'placeholder' => 'KPB 3',
                    'id'          => 'faktif_kpb3',
                    'value'       => '', 
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif_kpb3',
                    'type'        => 'checkbox',
            ),

           'c1_kpb4'=> array(
                    'placeholder' => 'KPB 4',
                    'id'          => 'c1_kpb4',
                    'name'        => 'c1_kpb4',
                    'value'       => set_value('c1_kpb4'),
                    'class'       => 'form-control', 
                    'readonly'    => '',
            ),
            
            'faktif_kpb4'=> array(
                    'placeholder' => 'KPB 4',
                    'id'          => 'faktif_kpb4',
                    'value'       => '', 
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif_kpb4',
                    'type'        => 'checkbox',
            ),
        );
    }
}

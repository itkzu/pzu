<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptkbbklhr
 *
 * @author adi
 */
class Rptkbbklhr extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptkbbklhr/submit'),
            'add' => site_url('rptkbbklhr/add'),
            'edit' => site_url('rptkbbklhr/edit'),
            'reload' => site_url('rptkbbklhr'),
        );
        $this->load->model('rptkbbklhr_qry');

        $kasbank = $this->rptkbbklhr_qry->refKasBank();
        foreach ($kasbank as $value) {
            $this->data['kdkb'][$value['kdkb']] = $value['nmkb'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    // public function submit() {
    //     $kdkb = $this->input->post('kdkb');
    //     $periode_awal = $this->input->post('periode_awal');
    //     $periode_akhir = $this->input->post('periode_akhir');
    //     $this->load->library('dompdf_lib');
    //     $this->dompdf_lib->filename = "Laporan Harian Kas/Bank Bengkel.pdf";
    //     $this->data['data'] = $this->rptkbbklhr_qry->get_data($kdkb,$periode_awal,$periode_akhir);
    //     $this->dompdf_lib->load_view('html', $this->data );
    // }

    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptkbbklhr_qry->get_data();
            $this->data['decode'] = $res;

            $jml = $this->rptkbbklhr_qry->get_jml();
            $this->data2['jml'] = $jml;
            //$this->load->view('export_html', $this->data);
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('export_html',$this->data);

            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('export_html',$this->data2);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    public function tglkb() {
        echo $this->rptkbbklhr_qry->tglkb();
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kdkb'=> array(
                    'attr'        => array(
                        'id'    => 'kdkb',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdkb'],
                    'value'    => '',
                    'name'     => 'kdkb',
                    'autofocus'   => ''
            ),
           'tglkb'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => set_value('tglkb'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
            ),
        );
    }

    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_awal',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>


  <style> 
      caption {
          padding-top: 1px;
          padding-bottom: 1px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto; 
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }
  </style>
  <?php 

      function bulan($tgl) {
        $year = substr($tgl,0,4);
        $month = substr($tgl,5,2);
        $day = substr($tgl,8,2);
        $temp = '';
        if ($month === '01') {
          $temp = "Januari";
        } else if ($month === '02') {
          $temp = "Februari";
        } else if ($month === '03') {
          $temp = "Maret";
        } else if ($month ==='04') {
          $temp = "April";
        } else if ($month === '05') {
          $temp = "Mei";
        } else if ($month === '06') {
          $temp = "Juni";
        } else if ($month === '07') {
          $temp = "Juli";
        } else if ($month === '08') {
          $temp = "Agustus";
        } else if ($month === '09') {
          $temp = "September";
        } else if ($month === '10') {
          $temp = "Oktober";
        } else if ($month === '12') {
          $temp = "November";
        } else if ($month === '13') {
          $temp = "Desember";
        }
        return $day.' '.$temp.' '.$year;
      }

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 
  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $space1 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '<b></b>'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 50%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 50%; font-size: 12px;'), 
  );
  $this->table->add_row($space1);   
  $namaheader = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '<b>'.$data[0]['nokps'].'</b>'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 50%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 50%; font-size: 12px;'), 
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);   
  $col1 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 18%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;'.$data[0]['nama_s'].' - '.$data[0]['alamat_s']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 72%; font-size: 12px;'),  
  );
  $this->table->add_row($col1);
  $col2 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 18%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;'.$data[0]['kel_s'].', '.$data[0]['kec_s'].' - '.$data[0]['kota_s']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 72%; font-size: 12px;'), 
      // array('data' => ':&nbsp;&nbsp;&nbsp;'.date_format(date_create($data[0]['tgldo']),"d/m/Y")
      //                     , 'colspan' => 5
      //                     , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $this->table->add_row($col2);
  $col3 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 14%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;'.penyebut($data[0]['harga']).' RUPIAH'
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; width: 76%; font-size: 12px;'), 
      // array('data' => ':&nbsp;&nbsp;&nbsp;'.date_format(date_create($data[0]['tgldo']),"d/m/Y")
      //                     , 'colspan' => 5
      //                     , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $this->table->add_row($col3); 
  $col3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; width: 100%; font-size: 5px;'),   
  );
  $this->table->add_row($col3); 
  $col4 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 18%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;1 (satu) unit kendaraan sepeda motor Honda '.$data[0]['kdtipe']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 72%; font-size: 12px;'),  
  );
  $this->table->add_row($col4);
  $col5 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 18%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;Nosin : '.$data[0]['nosin'].', Noka : '.$data[0]['nora']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 72%; font-size: 12px;'),  
  );
  $this->table->add_row($col5);
  $col6 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 18%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;Waarna : '.$data[0]['nmwarna'].', Tahun : '.$data[0]['tahun'].', Kondisi 100% Baru'
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 72%; font-size: 12px;'),  
  );
  $this->table->add_row($col6);
  $col7 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; width: 100%; font-size: 30px;'),   
  );
  $this->table->add_row($col7);
  $col8 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 16
                          , 'style' => 'text-align: left; width: 70%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;'.bulan($data[0]['tglcetak'])  
                          , 'colspan' => 4
                          , 'style' => 'text-align: center; width: 15%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 15%; font-size: 12px;'), 
  );
  $this->table->add_row($col8);
  $col9 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; width: 100%; font-size: 30px;'),   
  );
  $this->table->add_row($col9);
  $col8 = array(
      array('data' => '<b></b>'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 14%; font-size: 12px;'), 
      array('data' => '<b>&nbsp;'.number_format($data[0]['harga'],2,",",".").'&nbsp;'  
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; width: 86%; font-size: 12px;'),  
  );
  $this->table->add_row($col8);
  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();  
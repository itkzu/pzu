<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Unpostkbbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getData() {
		error_reporting(-1);

        //$nokb = $this->input->post('nokb');


		if( isset($_GET['nokb']) ){
			if($_GET['nokb']){
				$nokb = $_GET['nokb'];
			}else{
				$nokb = '';
			}
		}else{
			$nokb = '';
        }


		//var_dump($periode_awal, $periode_awal);

		$aColumns = array('no',
                          'nocetak',
						  'nokb',
						  'tglkb',
						  'jenis',
                          'drkpd',
                          'ket',
						  'debet',
						  'kredit',
						  'nourut'
                        );

		$sIndexColumn = "nokb";

		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}

		$sTable = " (SELECT '' as no, nocetak, to_char(tglkb,'DD-MM-YYYY') AS tglkb, nokb, jenis,drkpd,ket,debet,kredit,nourut
					    FROM bkl.vl_kas_bulanan WHERE nokb = '". $nokb ."' and posting_status = 'SUDAH POSTING' ORDER BY ordered,nourut
					) AS a";


		//$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
		$sWhere = "";

		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
					$sOrder = "";
			}
		}

		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{

			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}


		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";

        //echo $sQuery;

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";	//SELECT FOUND_ROWS()

              //  echo $sQuery;
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

                //    echo $sQuery;
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if($aRow[ $aColumns[$i] ]===null){
					$aRow[ $aColumns[$i] ] = '';
				}
				if(is_numeric($aRow[ $aColumns[$i] ])){
					$row[] = floatval($aRow[ $aColumns[$i] ]);
				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$row[9] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"unposting('".$aRow['nokb']."',".$aRow['nourut'].");\">Unpost</button>";
			$output['aaData'][] = $row;
		}
		return json_encode( $output );
	}

    public function proses() {
        $nokb = $this->input->post('nokb');
        $nourut = $this->input->post('nourut');
		$q = $this->db->query("select code,title,msg,tipe from bkl.kb_unpost('". $nokb ."',". $nourut .")");
		//echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptcekdatakpb_qry
 *
 * @author adi
 */
class Rptcekdatakpb_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		// $dbbengkel = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode = $_GET['periode_awal'];//$tgl1[1].$tgl1[0];
			}else{
				$periode = '';
			} 
		}else{
			$periode = '';
		}        
		

//echo "<script> console.log('PHP: ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";


		$aColumns = array('pk'
							, 'dt'
							, 'nm'
							, 'kpbto'
							, 'cmn1'
		);



		$sIndexColumn = "dt";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}




		$sTable = "(

SELECT	a.pk
		, format(a.dt,'dd/mm/yyyy') as dt
		, b.nm
		, a.kpbto
		, b.cmn1
FROM 	(t_wi_hd a 
		INNER JOIN m_vc b ON a.vcfk = b.pk)
		LEFT OUTER JOIN m_kpb c ON b.cmn1 = c.kdsin
WHERE	trim(a.kpbto) <> ''
		AND c.kdsin IS null
ORDER BY a.pk

					) AS A ";


		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
       echo $sQuery;
		
		$rResult = $this->db->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}

	public function json_dgview2() {
		error_reporting(-1);
		// $this->db = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode = $_GET['periode_awal'];//$tgl1[1].$tgl1[0];
			}else{
				$periode = '';
			} 
		}else{
			$periode = '';
		}        
		

//echo "<script> console.log('PHP: ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";


		$aColumns = array('pk'
							, 'dt'
							, 'nm'
							, 'cmn1'
		);



		$sIndexColumn = "dt";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}




		$sTable = "(


SELECT	a.pk
		, format(a.dt,'dd/mm/yyyy') as dt
		, b.nm
		, b.cmn1
FROM 	
		(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
		INNER JOIN t_wi_dt_srv x ON a.pk = x.fk)
		INNER JOIN m_srv xd ON x.srvfk = xd.pk)
		LEFT OUTER JOIN m_kpb c ON trim(b.cmn1) = trim(c.kdsin)
WHERE	trim(xd.nt) = '1'		
		AND trim(a.kpbto) = ''
ORDER BY a.pk
					) AS A ";


		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
//        echo $sQuery;
		
		$rResult = $this->db->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}

	public function submit(){

	}
}

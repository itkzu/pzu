<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptneraca
 *
 * @author adi
 */
class Rptneraca extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptneraca/submit'),
            'add' => site_url('rptneraca/add'),
            'edit' => site_url('rptneraca/edit'),
            'reload' => site_url('rptneraca'),
        );
        $this->load->model('rptneraca_qry');
        // $kdaks = $this->fkpo_qry->getKodefkpo();
        $this->data['bulan'] = array(
            "01" => "JANUARI",
            "02" => "FEBRUARI",
            "03" => "MARET",
            "04" => "APRIL",
            "05" => "MEI",
            "06" => "JUNI",
            "07" => "JULI",
            "08" => "AGUSTUS",
            "09" => "SEPTEMBER",
            "10" => "OKTOBER",
            "11" => "NOVEMBER",
            "12" => "DESEMBER",
            "13" => "PENUTUPAN"
          );
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $this->data['rptakun'] = $this->rptneraca_qry->submit();

        //echo "<script> console.log('PHP: ". json_encode($this->data) ."');</script>";

        
        if(empty($this->data['rptakun']['mtd'])){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('debug/err_000',$this->data);
        }else{
            $array = $this->input->post();
            if($array['submit']==="html"){
                $this->template
                    ->title($this->data['msg_main'],$this->apps->name)
                    ->set_layout('print-layout')
                    ->build('html',$this->data);            
            }elseif($array['submit']==="excel"){
                $this->load->view('excel',$this->data);        
            }else{
                redirect("rptneraca");            
            }    
        }
        
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            'periode'=> array(
                    'attr'        => array(
                        'id'    => 'periode',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['bulan'],
                    'value'    => set_value('periode'),
                    'name'     => 'periode',
                    'required'    => '',
                    'placeholder' => 'No. DO'
            ), 
            'tahun'=> array( 
                    'id'    => 'tahun',
                    'class' => 'form-control',  
                    'value'    => set_value('tahun'),
                    'name'     => 'tahun',
                    'type'     => 'number',
                    'required'    => '', 
            ), 
            'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => $this->yyyymm,
                    'class'       => 'form-control',
            ),
            'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => $this->yyyymm,
                    'class'       => 'form-control',
            ),
        );
    }
}

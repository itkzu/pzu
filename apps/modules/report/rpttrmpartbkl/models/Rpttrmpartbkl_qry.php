<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rpttrmpartbkl_qry
 *
 * @author adi
 */
class Rpttrmpartbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }  
       
    public function get_data_tunai() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $kddiv = $this->input->post('kddiv');
        $str = "select * from bkl.vl_po_tunai where kddiv = '".$kddiv."' and podate between '".$this->apps->dateConvert($periode_awal)."' and '".$this->apps->dateConvert($periode_akhir)."' order by podate";
        $q = $this->db->query($str);
                // echo $this->db->last_query();
        if($q->num_rows()>0){
            $arr = $q->result_array();
            $leas = array();
            $cbg = $arr[0]['nmcabang'];
            foreach ($arr as $leasing) {
                $leas[$leasing['kdgrup']] = $leasing['kdgrup'];
            }
            $dt_arr = array();
            foreach ($leas as $v) {
                $no = 1;
                foreach ($arr as $data) {
                    if($v==$data['kdgrup']){
                        $dt = $data + array('no'=>$no);
                        $dt_arr[] = $dt;
                        $no++;
                    }
                }
            }
            $res = array(
                'leasing' => $leas,
                'data' => $dt_arr,
                'cabang' => $cbg,
            );
        }else{
            $res = null;
        }
        return json_encode($res);
    }
       
    public function get_data_astra() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $kddiv = $this->input->post('kddiv');
        $str = "select * from bkl.vl_po_astra where kddiv = '".$kddiv."' and podate between '".$this->apps->dateConvert($periode_awal)."' and '".$this->apps->dateConvert($periode_akhir)."' order by podate";
        $q = $this->db->query($str);
                // echo $this->db->last_query();
        if($q->num_rows()>0){
            $arr = $q->result_array();
            $leas = array();
            $cbg = $arr[0]['nmcabang'];
            foreach ($arr as $leasing) {
                $leas[$leasing['kdgrup']] = $leasing['kdgrup'];
            }
            $dt_arr = array();
            foreach ($leas as $v) {
                $no = 1;
                foreach ($arr as $data) {
                    if($v==$data['kdgrup']){
                        $dt = $data + array('no'=>$no);
                        $dt_arr[] = $dt;
                        $no++;
                    }
                }
            }
            $res = array(
                'leasing' => $leas,
                'data' => $dt_arr,
                'cabang' => $cbg,
            );
        }else{
            $res = null;
        }
        return json_encode($res);
    }
       
    public function get_data_bag() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $kddiv = $this->input->post('kddiv');
        $str = "select * from bkl.vl_po_bag where kddiv = '".$kddiv."' and podate between '".$this->apps->dateConvert($periode_awal)."' and '".$this->apps->dateConvert($periode_akhir)."' order by podate";
        $q = $this->db->query($str);
                // echo $this->db->last_query();
        if($q->num_rows()>0){
            $arr = $q->result_array();
            $leas = array();
            $cbg = $arr[0]['nmcabang'];
            foreach ($arr as $leasing) {
                $leas[$leasing['kdgrup']] = $leasing['kdgrup'];
            }
            $dt_arr = array();
            foreach ($leas as $v) {
                $no = 1;
                foreach ($arr as $data) {
                    if($v==$data['kdgrup']){
                        $dt = $data + array('no'=>$no);
                        $dt_arr[] = $dt;
                        $no++;
                    }
                }
            }
            $res = array(
                'leasing' => $leas,
                'data' => $dt_arr,
                'cabang' => $cbg,
            );
        }else{
            $res = null;
        }
        return json_encode($res);
    }

}

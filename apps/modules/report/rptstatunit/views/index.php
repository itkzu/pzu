
<?php
/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<div class="row">

					<div class="col-xs-12">
						<div class="form-group has-error">
							<?php
									echo form_label('Masukkan Kata Kunci <small><i>( Nama STNK / No. SPK / No. DO / No. Rangka / No. Mesin )</i></small>');
									echo form_dropdown($form['nama']['name']
																		,$form['nama']['data']
																		,$form['nama']['value']
																		,$form['nama']['attr']);
									echo form_error('nama','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-4 col-md-4 ">
								<div class="row">

									<div class="col-xs-12">
										<div class="form-group">
											<?php
													echo form_label($form['nosin']['placeholder']);
													echo form_input($form['nosin']);
													echo form_error('nosin','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
													echo form_label($form['nora']['placeholder']);
													echo form_input($form['nora']);
													echo form_error('nora','<div class="note">','</div>');
											?>
										</div>
									</div>

								</div>
							</div>

							<div class="col-xs-4 col-md-4 ">
								<div class="row">

									<div class="col-xs-12">
										<div class="form-group">
											<?php
													echo form_label($form['noso']['placeholder']);
													echo form_input($form['noso']);
													echo form_error('noso','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="form-group">
											<?php
													echo form_label($form['nodo']['placeholder']);
													echo form_input($form['nodo']);
													echo form_error('nodo','<div class="note">','</div>');
											?>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-4 col-md-4 ">
								<div class="row">

									<div class="col-xs-12">
										<div class="form-group">
											<?php
													echo form_label($form['tglso']['placeholder']);
													echo form_input($form['tglso']);
													echo form_error('tglso','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="form-group">
											<?php
													echo form_label($form['tgldo']['placeholder']);
													echo form_input($form['tgldo']);
													echo form_error('tgldo','<div class="note">','</div>');
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-xs-12">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item" role="presentation">
										<a class="nav-link active" id="unit-tab" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Data Unit</a>
								</li>
								<li class="nav-item" role="presentation">
										<a class="nav-link" id="piutang-tab" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Data Piutang</a>
								</li>
								<li class="nav-item" role="presentation">
										<a class="nav-link" id="bbn-tab" data-toggle="tab" href="#bbn" role="tab" aria-controls="bbn" aria-selected="false">Data BBN</a>
								</li>
							</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade active" id="unit" role="tabpanel" aria-labelledby="unit-tab">

								<div class="col-xs-12">
										<hr>
								</div>

								<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['kdtipe']['placeholder']);
																		echo form_input($form['kdtipe']);
																		echo form_error('kdtipe','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-6 ">
															<div class="form-group">
																<?php
																		echo form_label($form['nmtipe']['placeholder']);
																		echo form_input($form['nmtipe']);
																		echo form_error('nmtipe','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-8">
															<div class="form-group">
																<?php
																		echo form_label($form['warna']['placeholder']);
																		echo form_input($form['warna']);
																		echo form_error('warna','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-4">
														<div class="form-group">
															<?php
																	echo form_label($form['tahun']['placeholder']);
																	echo form_input($form['tahun']);
																	echo form_error('tahun','<div class="note">','</div>');
															?>
														</div>
													</div>

													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['nofaktur']['placeholder']);
																		echo form_input($form['nofaktur']);
																		echo form_error('nofaktur','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['tglfaktur']['placeholder']);
																		echo form_input($form['tglfaktur']);
																		echo form_error('tglfaktur','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-12">
															<div class="form-group">
																<?php
																		echo form_label($form['nmsup']['placeholder']);
																		echo form_input($form['nmsup']);
																		echo form_error('nmsup','<div class="note">','</div>');
																?>
															</div>
														</div>

													</div>
												</div>

										</div>
								</div>

								<div class="col-xs-12">
										<hr>
								</div>

								<div class="col-xs-12">
									<div class="row">

											<div class="col-xs-12 col-md-12 ">
												<div class="row">

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['nmstnk']['placeholder']);
																		echo form_input($form['nmstnk']);
																		echo form_error('nmstnks','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3 ">
															<div class="form-group">
																<?php
																		echo form_label($form['sotr']['placeholder']);
																		echo form_input($form['sotr']);
																		echo form_error('sotr','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['alamat_ktp']['placeholder']);
																		echo form_input($form['alamat_ktp']);
																		echo form_error('alamat_ktp','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['hotr']['placeholder']);
																		echo form_input($form['hotr']);
																		echo form_error('hotr','<div class="note">','</div>');
																?>
															</div>
														</div>

												</div>
											</div>

											<div class="col-xs-12 col-md-12 ">
												<div class="row">

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['kel']['placeholder']);
																		echo form_input($form['kel']);
																		echo form_error('kel','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['kec']['placeholder']);
																		echo form_input($form['kec']);
																		echo form_error('kec','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['um']['placeholder']);
																		echo form_input($form['um']);
																		echo form_error('um','<div class="note">','</div>');
																?>
															</div>
														</div>

												</div>
											</div>

											<div class="col-xs-12 col-md-12 ">
												<div class="row">

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['kota']['placeholder']);
																		echo form_input($form['kota']);
																		echo form_error('kota','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['nohp']['placeholder']);
																		echo form_input($form['nohp']);
																		echo form_error('nohp','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['disc']['placeholder']);
																		echo form_input($form['disc']);
																		echo form_error('disc','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['leas']['placeholder']);
																		echo form_input($form['leas']);
																		echo form_error('leas','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['sls']['placeholder']);
																		echo form_input($form['sls']);
																		echo form_error('sls','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['umnet']['placeholder']);
																		echo form_input($form['umnet']);
																		echo form_error('umnet','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php echo "<b>Kirim Tagih</b>"; ?>
																<div class="checkbox">
																	<label>
																		<?php
																				echo form_checkbox($form['kitag']);
																				echo "Tidak Kirim Tagih";
																		?>
																	</label>
																</div>
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['spv']['placeholder']);
																		echo form_input($form['spv']);
																		echo form_error('spv','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['bayar']['placeholder']);
																		echo form_input($form['bayar']);
																		echo form_error('bayar','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['prog']['placeholder']);
																		echo form_input($form['prog']);
																		echo form_error('prog','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<?php
																		echo form_label($form['ket']['placeholder']);
																		echo form_input($form['ket']);
																		echo form_error('ket','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['angsur']['placeholder']);
																		echo form_input($form['angsur']);
																		echo form_error('angsur','<div class="note">','</div>');
																?>
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<?php
																		echo form_label($form['tenor']['placeholder']);
																		echo form_input($form['tenor']);
																		echo form_error('tenor','<div class="note">','</div>');
																?>
															</div>
														</div>

												</div>
											</div>
									</div>
								</div>

							</div>

							<div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="piutang-tab">

									<div class="col-xs-12">
											<hr>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6">
													<div class="form-group">
														<?php
																echo form_label($form['viabayar']['placeholder']);
																echo form_input($form['viabayar']);
																echo form_error('viabayar','<div class="note">','</div>');
														?>
													</div>
												</div>

												<div class="col-xs-6 ">
													<div class="form-group">
														<?php
																echo form_label($form['progleas']['placeholder']);
																echo form_input($form['progleas']);
																echo form_error('progleas','<div class="note">','</div>');
														?>
													</div>
												</div>

										</div>
									</div>

									<div class="col-xs-12">
											<hr>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['npk']['placeholder']);
																			echo form_input($form['npk']);
																			echo form_error('npk','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['nppk']['placeholder']);
																			echo form_input($form['nppk']);
																			echo form_error('nppk','<div class="note">','</div>');
																	?>
																</div>
															</div>

													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['npjpleas']['placeholder']);
																			echo form_input($form['npjpleas']);
																			echo form_error('npjpleas','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['npmleas']['placeholder']);
																			echo form_input($form['npmleas']);
																			echo form_error('npmleas','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['ppk']['placeholder']);
																			echo form_input($form['ppk']);
																			echo form_error('ppk','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['pppk']['placeholder']);
																			echo form_input($form['pppk']);
																			echo form_error('pppk','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['ppjpleas']['placeholder']);
																			echo form_input($form['ppjpleas']);
																			echo form_error('ppjpleas','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['ppmleas']['placeholder']);
																			echo form_input($form['ppmleas']);
																			echo form_error('ppmleas','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['spk']['placeholder']);
																			echo form_input($form['spk']);
																			echo form_error('spk','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['sppk']['placeholder']);
																			echo form_input($form['sppk']);
																			echo form_error('sppk','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['spjpleas']['placeholder']);
																			echo form_input($form['spjpleas']);
																			echo form_error('spjpleas','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['spmleas']['placeholder']);
																			echo form_input($form['spmleas']);
																			echo form_error('spmleas','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
											<hr>
									</div>

									<div class="col-md-12">

											<div class="table-responsive">
												<table class="dataTable table table-bordered table-striped table-hover dataTable">
													<thead>
														<tr>
														 	<th style="width: 50px;text-align: center;">No.</th>
														  <th style="text-align: center;">Jenis Pembayaran Piutang</th>
														  <th style="text-align: center;">Transaksi Via</th>
															<th style="text-align: center;">No. Cetak</th>
															<th style="text-align: center;">No. Transaksi</th>
															<th style="text-align: center;">Tanggal</th>
															<th style="text-align: center;">Diterima Dari</th>
															<th style="text-align: center;">Keterangan</th>
														  <th style="text-align: center;">Nominal</th>
														</tr>
													</thead>
													<tbody></tbody>
													<tfoot>
														<tr>
														  <th style="width: 50px;text-align: center;"></th>
														  <th style="text-align: center;"></th>
														  <th style="text-align: center;"></th>
															<th style="text-align: center;"></th>
															<th style="text-align: center;"></th>
															<th style="text-align: center;"></th>
															<th style="text-align: center;"></th>
															<th style="text-align: center;"></th>
														  <th style="text-align: center;"></th>
														</tr>
													</tfoot>
												</table>
											</div>
									</div>
							</div>

							<div class="tab-pane fade" id="bbn" role="tabpanel" aria-labelledby="bbn-tab">

									<div class="col-xs-12">
											<hr>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6">
													<div class="form-group">
														<?php
																echo form_label($form['nopol']['placeholder']);
																echo form_input($form['nopol']);
																echo form_error('nopol','<div class="note">','</div>');
														?>
													</div>
												</div>

												<div class="col-xs-6 ">
													<div class="form-group">
														<?php
																echo form_label($form['nobpkb']['placeholder']);
																echo form_input($form['nobpkb']);
																echo form_error('nobpkb','<div class="note">','</div>');
														?>
													</div>
												</div>

										</div>
									</div>

									<div class="col-xs-12">
											<hr>
									</div>

									<div class="col-xs-3">
										<div class="form-group">
											<?php
													echo form_label($form['tgl_aju_fa']['placeholder']);
													echo form_input($form['tgl_aju_fa']);
													echo form_error('tgl_aju_fa','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trb_fa']['placeholder']);
																			echo form_input($form['tgl_trb_fa']);
																			echo form_error('tgl_trb_fa','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trm_notis']['placeholder']);
																			echo form_input($form['tgl_trm_notis']);
																			echo form_error('tgl_trm_notis','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_srh_notis']['placeholder']);
																			echo form_input($form['tgl_srh_notis']);
																			echo form_error('tgl_srh_notis','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trm_fa']['placeholder']);
																			echo form_input($form['tgl_trm_fa']);
																			echo form_error('tgl_trm_fa','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['notrans_trm_fa']['placeholder']);
																			echo form_input($form['notrans_trm_fa']);
																			echo form_error('notrans_trm_fa','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trm_stnk']['placeholder']);
																			echo form_input($form['tgl_trm_stnk']);
																			echo form_error('tgl_trm_stnk','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_srh_stnk']['placeholder']);
																			echo form_input($form['tgl_srh_stnk']);
																			echo form_error('tgl_srh_stnk','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_kps']['placeholder']);
																			echo form_input($form['tgl_kps']);
																			echo form_error('tgl_kps','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['notrans_aju_stnk']['placeholder']);
																			echo form_input($form['notrans_aju_stnk']);
																			echo form_error('notrans_aju_stnk','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trm_tnkb']['placeholder']);
																			echo form_input($form['tgl_trm_tnkb']);
																			echo form_error('tgl_trm_tnkb','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_srh_tnkb']['placeholder']);
																			echo form_input($form['tgl_srh_tnkb']);
																			echo form_error('tgl_srh_tnkb','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_aju_bbn']['placeholder']);
																			echo form_input($form['tgl_aju_bbn']);
																			echo form_error('tgl_aju_bbn','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['notrans_aju_bbn']['placeholder']);
																			echo form_input($form['notrans_aju_bbn']);
																			echo form_error('notrans_aju_bbn','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trm_bpkb']['placeholder']);
																			echo form_input($form['tgl_trm_bpkb']);
																			echo form_error('tgl_trm_bpkb','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_srh_bpkb']['placeholder']);
																			echo form_input($form['tgl_srh_bpkb']);
																			echo form_error('tgl_srh_bpkb','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['tgl_trm_bbn']['placeholder']);
																			echo form_input($form['tgl_trm_bbn']);
																			echo form_error('tgl_trm_bbn','<div class="note">','</div>');
																	?>
																</div>
															</div>

															<div class="col-xs-6 ">
																<div class="form-group">
																	<?php
																			echo form_label($form['notrans_trm_stnk']['placeholder']);
																			echo form_input($form['notrans_trm_stnk']);
																			echo form_error('notrans_trm_stnk','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>

												<div class="col-xs-6 col-md-6 ">
													<div class="row">

															<div class="col-xs-12">
																<div class="form-group">
																	<?php
																			echo form_label($form['no_srh_bpkb_leas']['placeholder']);
																			echo form_input($form['no_srh_bpkb_leas']);
																			echo form_error('no_srh_bpkb_leas','<div class="note">','</div>');
																	?>
																</div>
															</div>
													</div>
												</div>
										</div>
									</div>

									<div class="col-xs-12">
											<hr>
									</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		init_nama();
		//clear();

		$("#nama").change(function(){
			Nomer();
			getDataUnit();
			getTable();
			getDataBBN();
			getDataTFA();
			getDataBBN_aju();
			getDataBBN_trm();
			getDataKPS();
			getDataKBBN();
			getDataBayar();
			//Sum();
		});

		$("#spk").change(function(){
			xSum();
		});
	});

	function Nomer(){
		//piutang
		$("#hotr").autoNumeric();
		$("#um").autoNumeric();
		$("#disc").autoNumeric();
		$("#umnet").autoNumeric();
		$("#angsur").autoNumeric();
		$("#leas").autoNumeric();
		$("#tenor").autoNumeric();

		//piutang
		$("#npk").autoNumeric();
		$("#nppk").autoNumeric();
		$("#npjpleas").autoNumeric();
		$("#npmleas").autoNumeric();
		$("#ppk").autoNumeric();
		$("#pppk").autoNumeric();
		$("#ppjpleas").autoNumeric();
		$("#ppmleas").autoNumeric();
		$("#spk").autoNumeric();
		$("#sppk").autoNumeric();
		$("#spjpleas").autoNumeric();
		$("#spmleas").autoNumeric();
	}
function getDataUnit(){
	var nama = $("#nama").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataUnit");?>",
		data: {"nama":nama},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#nosin").val(value.nosin);
				$("#noso").val(value.noso);
				$("#tglso").val(value.tglso);
				$("#nora").val(value.nora);
				$("#nodo").val(value.nodo);
				$("#tgldo").val(moment(value.tgldo).format('LL'));

				//data unit
				$("#kdtipe").val(value.kdtipex); 			$("#nmtipe").val(value.nmtipe); 							$("#nofaktur").val(value.nopo);		$("#tglfaktur").val(value.tglpo);
				$("#warna").val(value.nmwarna);				$("#tahun").val(value.tahun);									$("#nmsup").val(value.nmsup);

				$("#nmstnk").val(value.nama);																												$("#sotr").val(value.status_otr);
				$("#alamat_ktp").val(value.alamat);																									$("#hotr").autoNumeric('set',value.harga_otr);
				$("#kel").val(value.kel);							$("#kec").val(value.kec);											$("#um").autoNumeric('set',value.um);
				$("#kota").val(value.kota);						$("#nohp").val(value.nohp);										$("#disc").autoNumeric('set',value.disc);				$("#leas").autoNumeric('set',value.disc_leasing);
				$("#sls").val(value.nmsales);					$("#umnet").autoNumeric('set',value.um_riel);			//$("#kitag").val(value.kirim_tagih);
				var kt = value.kirim_tagih;
				//alert(kt);
				if(kt=='f'){
						$("#kitag").prop("checked",true);
				} else {
						$("#kitag").prop("checked",false);
				}
				$("#spv").val(value.nmsales_header);	$("#bayar").val(value.nmleasingx);							$("#prog").val(value.nmprogleas);
				$("#viabayar").val(value.nmleasingx);	$("#progleas").val(value.nmprogleas);
				$("#ket").val(value.ket);							$("#angsur").autoNumeric('set',value.angsuran);	$("#tenor").autoNumeric('set',value.tenor);
				//alert(value.ar_konsumen);
				if(value.ar_konsumen==''){
						$("#npk").autoNumeric('set','0');
				} else {
						$("#npk").autoNumeric('set',value.ar_konsumen);
				}
				if(value.ar_fincoy==''){
						$("#nppk").autoNumeric('set','0');
				} else {
						$("#nppk").autoNumeric('set',value.ar_fincoy);
				}
				if(value.ar_jp==''){
						$("#npjpleas").autoNumeric('set','0');
				} else {
						$("#npjpleas").autoNumeric('set',value.ar_jp);
				}
				if(value.ar_matriks==''){
						$("#npmleas").autoNumeric('set','0');
				} else {
						$("#npmleas").autoNumeric('set',value.ar_matriks);
				}
			});
		},
		error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}

function getDataBayar(){
	var nama = $("#nama").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataBayar");?>",
		data: {"nama":nama},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				if(value.pc==null){
						$("#ppk").autoNumeric('set','0');
				} else {
						$("#ppk").autoNumeric('set',value.pc);
				}
				//alert(value.pl);
				if(value.pl==null){
						$("#pppk").autoNumeric('set','0');
				} else {
						$("#pppk").autoNumeric('set',value.pl);
				}
				if(value.jp==null){
						$("#ppjpleas").autoNumeric('set','0');
				} else {
						$("#ppjpleas").autoNumeric('set',value.jp);
				}
				if(value.mx==null){
						$("#ppmleas").autoNumeric('set','0');
				} else {
						$("#ppmleas").autoNumeric('set',value.mx);
				}
			});
			xSum();
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function getDataBBN(){
	var nodo = $("#nodo").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataBBN");?>",
		data: {"nodo":nodo},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#nopol").val(value.nopol);																																$("#nobpkb").val(value.nobpkb);
				$("#tgl_aju_fa").val(value.tgl_aju_fa);
				$("#tgl_trb_fa").val(value.tgl_buat_fa);		$("#tgl_trm_notis").val(value.tgl_trm_notis);		$("#tgl_srh_notis").val(value.tgl_srh_notis);
				$("#tgl_trm_fa").val(value.tgl_trm_fa);			$("#tgl_trm_stnk").val(value.tgl_trm_stnk);			$("#tgl_srh_stnk").val(value.tgl_srh_stnk);
				$("#tgl_kps").val(value.tgl_kps);						$("#tgl_trm_tnkb").val(value.tgl_trm_tnkb);			$("#tgl_srh_tnkb").val(value.tgl_srh_tnkb);
				$("#tgl_aju_bbn").val(value.tgl_aju_bbn);		$("#tgl_trm_bpkb").val(value.tgl_trm_bpkb);			$("#tgl_srh_bpkb").val(value.tgl_srh_bpkb);
				$("#tgl_trm_bbn").val(value.tgl_trm_bbn);
			});
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function getDataTFA(){
	var nodo = $("#nodo").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataTFA");?>",
		data: {"nodo":nodo},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#notrans_trm_fa").val(value.notfa);
			});
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function getDataKPS(){
	var nodo = $("#nodo").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataKPS");?>",
		data: {"nodo":nodo},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#notrans_aju_stnk").val(value.nokps);
			});
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function getDataBBN_aju(){
	var nodo = $("#nodo").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataBBN_aju");?>",
		data: {"nodo":nodo},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#notrans_aju_bbn").val(value.nobbn_aju);
			});
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function getDataBBN_trm(){
	var nodo = $("#nodo").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataBBN_trm");?>",
		data: {"nodo":nodo},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#notrans_trm_stnk").val(value.nobbn_trm);
			});
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function getDataKBBN(){
	var nodo = $("#nodo").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatunit/getDataKBBN");?>",
		data: {"nodo":nodo},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#no_srh_bpkb_leas").val(value.noskbbn_leas);
			});
		},
		error:function(event, textStatus, errorThrown) {
			swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
function xSum(){
	var nilai_kons = $("#npk").autoNumeric('get');					var bayar_kons = $("#ppk").autoNumeric('get');
	var nilai_pok = $("#nppk").autoNumeric('get');					var bayar_pok = $("#pppk").autoNumeric('get');
	var nilai_jp = $("#npjpleas").autoNumeric('get');			var bayar_jp = $("#ppjpleas").autoNumeric('get');
	var nilai_mx = $("#npmleas").autoNumeric('get');				var bayar_mx = $("#ppmleas").autoNumeric('get');

	var skon = parseInt(nilai_kons) - parseInt(bayar_kons);
	var spok = parseInt(nilai_pok) - parseInt(bayar_pok);
	var sjp = parseInt(nilai_jp) - parseInt(bayar_jp);
	var smx = parseInt(nilai_mx) - parseInt(bayar_mx);

	$("#spk").autoNumeric('set',skon);
	$("#sppk").autoNumeric('set',spok);
	$("#spjpleas").autoNumeric('set',sjp);
	$("#spmleas").autoNumeric('set',smx);
}

function getTable(){
	var noso = $("#noso").val();
	var column = [];

	column.push({
			"aTargets": [ 8 ],
			"mRender": function (data, type, full) {
					return type === 'export' ? data : numeral(data).format('0,0.00');
			},
			"sClass": "right",
			"orderable": false
	});

	column.push({
			"aTargets": [ 5 ],
			"mRender": function (data, type, full) {
					return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
	});

	table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "no"  },
				{ "data": "jenis"  },
				{ "data": "nmkb"  },
				{ "data": "nocetak"  },
				{ "data": "notrx"  },
				{ "data": "tgltrx"  },
				{ "data": "drkpd" },
				{ "data": "ket" },
				{ "data": "nilai" },
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			//"ordering": false,
			"bSort": false,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
					aoData.push({ "name": "nama", "value": $("#nama").val()});
					$.ajax( {
							"dataType": 'json',
							"type": "GET",
							"url": sSource,
							"data": aoData,
							"success": fnCallback
					} );
			},
			'rowCallback': function(row, data, index){
			},
			"sAjaxSource": "<?=site_url('rptstatunit/json_dgview');?>",
			"oLanguage": {
					"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			buttons: [{
					extend:    'excelHtml5', footer: true,
					text:      'Export To Excel',
					titleAttr: 'Excel',
					"oSelectorOpts": { filter: 'applied', order: 'current' },
					"sFileName": "report.xls",
					action : function( e, dt, button, config ) {
							exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
					},
					exportOptions: {orthogonal: 'export'}
			}],
			"sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
	});

	table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
					//if (ev.keyCode == 13) { //only on enter keypress (code 13)
							that
									.search( this.value )
									.draw();
					//}
			} );
	});

	//row number
	table.on( 'draw.dt', function () {
	var PageInfo = $('.dataTable').DataTable().page.info();
			table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
					cell.innerHTML = i + 1 + PageInfo.start;
			} );
	} );
}

function setBgColour(val,object){
	if(val){
		$("#"+object).css("background-color", "#fff");
	}else{
		$("#"+object).css("background-color", "#eee");
	}
}

function init_nama(){
	$("#nama").select2({
		ajax: {
			url: "<?=site_url('rptstatunit/getNama');?>",
			type: 'post',
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term,
					page: params.page
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total_count
					}
				};
			},
			cache: true
		},
		placeholder: 'Masukkan Nama Pemilik ...',
		dropdownAutoWidth : false,
					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					minimumInputLength: 3,
					templateResult: format_nama, // omitted for brevity, see the source of this page
					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
				});
}

function format_nama_terpilih (repo) {
	return repo.full_name || repo.text;
}

function format_nama (repo) {
	if (repo.loading) return "Mencari data ... ";


	var markup = "<div class='select2-result-repository clearfix'>" +
	"<div class='select2-result-repository__meta'>" +
	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

	if (repo.alamat) {
		markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.alamat + "</div>";
	}

	markup += "<div class='select2-result-repository__statistics'>" +
	"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nosin + " | <i class=\"fa fa-tag\"></i> " + repo.noso + " | <i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " </div>" +
	"<div class='select2-result-repository__stargazers'> <i class=\"fa fa-file-o\"></i> " + repo.nodo + " | <i class=\"fa fa-calendar\"></i> " + repo.tgldo + "</div>" +
	"</div>" +
	"</div>" +
	"</div>";
	return markup;
}
function clear(){
	$("#nosin").val('');
	$("#noso").val('');
	$("#tglso").val('');
	$("#nora").val('');
	$("#nodo").val('');
	$("#tgldo").val('');

	//data unit
	$("#kdtipe").val(''); 						$("#nmtipe").val(''); 								$("#nofaktur").val('');							$("#tglfaktur").val('');
	$("#warna").val('');							$("#tahun").val('');									$("#nmsup").val('');

	$("#nmstnk").val('');																										$("#sotr").val('');
	$("#alamat_ktp").val('');																								$("#hotr").autoNumeric('set','0');
	$("#kel").val('');								$("#kec").val('');										$("#um").autoNumeric('set','0');
	$("#kota").val('');								$("#nohp").val('');										$("#disc").autoNumeric('set','0');	$("#leas").autoNumeric('set','0');
	$("#sls").val('');								$("#umnet").autoNumeric('set','0');			//$("#kitag").val(value.kirim_tagih);
	$("#kitag").prop("checked",false);

	$("#spv").val('');								$("#bayar").val('');									$("#prog").val('');
	$("#viabayar").val('');						$("#progleas").val('');
	$("#ket").val('');								$("#angsur").autoNumeric('set','0');	$("#tenor").autoNumeric('set','0');

	$("#npk").autoNumeric('set','0');
	$("#nppk").autoNumeric('set','0');
	$("#npjpleas").autoNumeric('set','0');
	$("#npmleas").autoNumeric('set','0');

	$("#ppk").autoNumeric('set','0');
	$("#pppk").autoNumeric('set','0');
	$("#ppjpleas").autoNumeric('set','0');
	$("#ppmleas").autoNumeric('set','0');

	$("#spk").autoNumeric('set','0');
	$("#sppk").autoNumeric('set','0');
	$("#spjpleas").autoNumeric('set','0');
	$("#spmleas").autoNumeric('set','0');

	$("#nopol").val('');																																$("#nobpkb").val('');
	$("#tgl_aju_fa").val('');
	$("#tgl_trb_fa").val('');																				$("#tgl_trm_notis").val('');		$("#tgl_srh_notis").val('');
	$("#tgl_trm_fa").val('');			$("#notrans_trm_fa").val('');			$("#tgl_trm_stnk").val('');			$("#tgl_srh_stnk").val('');
	$("#tgl_kps").val('');				$("#notrans_aju_stnk").val('');		$("#tgl_trm_tnkb").val('');			$("#tgl_srh_tnkb").val('');
	$("#tgl_aju_bbn").val('');		$("#notrans_aju_bbn").val('');		$("#tgl_trm_bpkb").val('');			$("#tgl_srh_bpkb").val('');
	$("#tgl_trm_bbn").val('');		$("#notrans_trm_stnk").val('');		$("#no_srh_bpkb_leas").val('');
}
</script>

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbb_qry
 *
 * @author adi
 */
class Rptbb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getAkun() {
        $this->db->select("kdakun, nmakun, kddiv, parent, par_div, par_head, jnsakun, dk,
                            sawal, periode, faktif");
        $this->db->where("faktif","t");
        $this->db->where("( jnsakun <> 'LRD' AND jnsakun <> 'LRT' AND jnsakun <> 'LRB' ) ");
        $this->db->order_by("kdakun","ASC");
        $query = $this->db->get("glr.v_akun_div");
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }

    public function submit() {
        $array = $this->input->post();
        $array['periode_awal'] = $this->apps->dateConvert($array['periode_awal']);
        $array['periode_akhir'] = $this->apps->dateConvert($array['periode_akhir']);
        $res = array(
            'akun_saldo_awal' => $this->akun_saldo_awal($array),
            'akun_h' => $this->akun_h($array),
            'akun_d' => $this->akun_d($array),
        );

        //console.log($res);
        return $res;
    }

    private function akun_saldo_awal($array) {
        // Function untuk menentukan saldo awal pada periode terpilih
        $tgl = explode("-", $array['periode_awal']);
        $mnth = $tgl[0].$tgl[1];
//        $mnth = date('Ym', strtotime('first day of -1 months', strtotime($array['periode_awal'])));
        $p_selisih_awal = $tgl[0]."-".$tgl[1]."-"."01";
        $p_selisih_akhir = date('Y-m-d',strtotime ( '-1 day' , strtotime ( $array['periode_awal'] ) ));
//        var_dump($array['periode_awal']);
//        var_dump(str_replace("-", "", $this->yyyymm));
//        var_dump($mnth);
//        exit;
        if(isset($array['akun_multi'])){
            // Jika yang dipilih adalah multi akun
            $kdakun = "";
            foreach ($array['akun_multi'] as $value) {
                if(empty($kdakun)){
                   $kdakun = " x.kdakun = '".$value."' ";
                }else{
                   $kdakun = $kdakun . " OR x.kdakun = '".$value."' ";
                }
            }
            if($kdakun){
                $kdakun = " AND ( " . $kdakun . " ) ";
            }

            if($mnth>=str_replace("-", "", $this->yyyymm)){

                $str = "select * from
                            (
                            select kddiv, kdakun, (sawal) as sakhir, periode,dk
                                    from glr.vl_buku_besar
                                    GROUP BY kddiv, kdakun,  (sawal) , periode,dk
                            ) as x
                        where ( x.periode = '".$mnth."')"
                        . $kdakun
                        . " ORDER BY kdakun ";
            }else{
                $str = "select * from
                            (
                            select kddiv, kdakun, (sawal) as sakhir, periode,dk
                                    from glr_h.vl_buku_besar
                                    GROUP BY kddiv, kdakun,  (sawal) , periode,dk
                            ) as x
                        where ( x.periode = '".$mnth."')"
                        . $kdakun
                        . " ORDER BY kdakun ";
            }
            // Selisih dari tgl 1 sampai tgl periode awal terpilih
            $str_selisih = "select kdakun,nmakun,SUM(debet) as debet_s,SUM(kredit) as kredit_s from
                        (
                        select * from glr.vl_buku_besar
                        union
                        select * from glr_h.vl_buku_besar
                        ) as x
                    where ( to_char(x.tgljurnal,'YYYY-MM-DD') BETWEEN '".$p_selisih_awal."' AND  '".$p_selisih_akhir."' )"
                    . $kdakun
                    . " GROUP BY kdakun,nmakun "
                    . " ORDER BY kdakun,nmakun ";
            // ./Selisih dari tgl 1 sampai tgl periode awal terpilih
        }elseif(isset($array['akun_awal']) &&  isset($array['akun_akhir'])){
            // Jika yang dipilih adalah akun between
            if($mnth>=str_replace("-", "", $this->yyyymm)){
                $str = "select * from
                            (
                            select kddiv, kdakun, (sawal) as sakhir, periode,dk
                                    from glr.vl_buku_besar
                                    GROUP BY kddiv, kdakun,  (sawal) , periode,dk
                            ) as x
                        where ( x.periode = '".$mnth."')"
                        . " AND ( x.kdakun BETWEEN '".$array['akun_awal']."' AND '".$array['akun_akhir']."' )"
                        . " ORDER BY kdakun ";
            }else{
                $str = "select * from
                            (
                            select kddiv, kdakun, (sawal) as sakhir, periode,dk
                                    from glr_h.vl_buku_besar
                                    GROUP BY kddiv, kdakun,  (sawal) , periode,dk
                            ) as x
                        where ( x.periode = '".$mnth."')"
                        . " AND ( x.kdakun BETWEEN '".$array['akun_awal']."' AND '".$array['akun_akhir']."' )"
                        . " ORDER BY kdakun ";
            }

            // Selisih dari tgl 1 sampai tgl periode awal terpilih
            $str_selisih = "select kdakun,nmakun,SUM(debet) as debet_s,SUM(kredit) as kredit_s  from
                        (
                        select * from glr.vl_buku_besar
                        union
                        select * from glr_h.vl_buku_besar
                        ) as x
                    where ( to_char(x.tgljurnal,'YYYY-MM-DD') BETWEEN '".$p_selisih_awal."' AND  '".$p_selisih_akhir."' )"
                    . " AND ( x.kdakun BETWEEN '".$array['akun_awal']."' AND '".$array['akun_akhir']."' )"
                    . " GROUP BY kdakun,nmakun "
                    . " ORDER BY kdakun,nmakun ";
            // ./Selisih dari tgl 1 sampai tgl periode awal terpilih
        }
        if(isset($str)){
            $q = $this->db->query($str);
            if($q->num_rows() > 0){
                $res = $q->result_array();
            }else{
                $res = null;
            }
        }else{
            $res = null;
        }

        if(isset($str_selisih)){
            $q_selisih = $this->db->query($str_selisih);
            if($q_selisih->num_rows() > 0){
                $res_selisih = $q_selisih->result_array();
            }else{
                $res_selisih = null;
            }
        }
        // Menghitung saldo awal start
        $data = array();
        if($res){
            foreach ($res as $value) {
                if($res_selisih){
                    foreach ($res_selisih as $v_selisih) {
                        if($value['kdakun']===$v_selisih['kdakun'] && $value['dk']==="D"){
                            $data[]=array(
                                 "dk" => $value['dk'],
                                 "kdakun" => $value['kdakun'],
                                 "s_awal" => (($value['sakhir'] + $v_selisih['debet_s']) - $v_selisih['kredit_s']),
                             );
                        }elseif($value['kdakun']===$v_selisih['kdakun'] && $value['dk']==="K"){
                            $data[]=array(
                                 "kdakun" => $value['kdakun'],
                                 "s_awal" => (($value['sakhir'] + $v_selisih['kredit_s']) - $v_selisih['debet_s']),
                             );
                        }
                    }
                }else{
                    $data[]=array(
                         "kdakun" => $value['kdakun'],
                         "s_awal" => ($value['sakhir']),
                         "dk" => $value['dk'],
                     );

                }
            }
        }
        return $data;
    }

    private function akun_h($array) {

        if(isset($array['akun_multi'])){
            // Jika yang dipilih adalah multi akun
            $kdakun = "";
            foreach ($array['akun_multi'] as $value) {
                if(empty($kdakun)){
                   $kdakun = " x.kdakun = '".$value."' ";
                }else{
                   $kdakun = $kdakun . " OR x.kdakun = '".$value."' ";
                }
            }
            if($kdakun){
                $kdakun = " AND ( " . $kdakun . " ) ";
            }

            $str = "select kdakun,nmakun,SUM(debet) as debet_s,SUM(kredit) as kredit_s from
                        (
                        select * from glr.vl_buku_besar
                        union
                        select * from glr_h.vl_buku_besar
                        ) as x
                    where ( to_char(x.tgljurnal,'YYYY-MM-DD') BETWEEN '".$array['periode_awal']."' AND  '".$array['periode_akhir']."' )"
                    . $kdakun
                    . " GROUP BY kdakun,nmakun "
                    . " ORDER BY kdakun,nmakun ";
        }elseif(isset($array['akun_awal']) &&  isset($array['akun_akhir'])){
            // Jika yang dipilih adalah akun between
            $str = "select kdakun,nmakun,SUM(debet) as debet_s,SUM(kredit) as kredit_s  from
                        (
                        select * from glr.vl_buku_besar
                        union
                        select * from glr_h.vl_buku_besar
                        ) as x
                    where ( to_char(x.tgljurnal,'YYYY-MM-DD') BETWEEN '".$array['periode_awal']."' AND  '".$array['periode_akhir']."' )"
                    . " AND ( x.kdakun BETWEEN '".$array['akun_awal']."' AND '".$array['akun_akhir']."' )"
                    . " AND (x.kdakun <> '31020101' AND x.kdakun <> '31030101' AND x.kdakun <> '31040101')"
                    . " GROUP BY kdakun,nmakun "
                    . " ORDER BY kdakun,nmakun ";
        }
        if(isset($str)){
            $q = $this->db->query($str);
            if($q->num_rows() > 0){
                return $q->result_array();
            }else{
                return false;
            }
        }
    }

    private function akun_d($array) {

        if(isset($array['akun_multi'])){
            // Jika yang dipilih adalah multi akun
            $kdakun = "";
            foreach ($array['akun_multi'] as $value) {
                if(empty($kdakun)){
                   $kdakun = " x.kdakun = '".$value."' ";
                }else{
                   $kdakun = $kdakun . " OR x.kdakun = '".$value."' ";
                }
            }
            if($kdakun){
                $kdakun = " AND ( " . $kdakun . " ) ";
            }

            $str = "select * from
                        (
                        select * from glr.vl_buku_besar
                        union
                        select * from glr_h.vl_buku_besar
                        ) as x
                    where ( to_char(x.tgljurnal,'YYYY-MM-DD') BETWEEN '".$array['periode_awal']."' AND  '".$array['periode_akhir']."' )"
                    . $kdakun
                    . " ORDER BY x.tgljurnal,x.nojurnal ";
        }elseif(isset($array['akun_awal']) &&  isset($array['akun_akhir'])){
            // Jika yang dipilih adalah akun between
            $str = "select * from
                        (
                        select * from glr.vl_buku_besar
                        union
                        select * from glr_h.vl_buku_besar
                        ) as x
                    where ( to_char(x.tgljurnal,'YYYY-MM-DD') BETWEEN '".$array['periode_awal']."' AND  '".$array['periode_akhir']."' )"
                    . " AND ( x.kdakun BETWEEN '".$array['akun_awal']."' AND '".$array['akun_akhir']."' )"
                    . " ORDER BY x.tgljurnal,x.nojurnal ";
        }
        if(isset($str)){
            $q = $this->db->query($str);
            if($q->num_rows() > 0){
                return $q->result_array();
            }else{
                return false;
            }
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Revkbbkld
 *
 * @author
 */

class Revkbbkld extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('revkbbkld/submit'),
            'add' => site_url('revkbbkld/add'),
            'edit' => site_url('revkbbkld/edit'),
            'reload' => site_url('revkbbkld'),
        );
        $this->load->model('revkbbkld_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
        }

    public function getKdRef() {
        echo $this->revkbbkld_qry->getKdRef();
    }

    public function getData() {
        echo $this->revkbbkld_qry->getData();

    }

    public function set_jenis() {
        echo $this->revkbbkld_qry->set_jenis();

    }

    public function proses(){
        echo $this->revkbbkld_qry->proses();

    }

    public function submit() {
        $nokb = $this->input->post('nokb');
        $nourut = $this->input->post('nourut');

        if($this->validate() == TRUE){
            $res = $this->revkbbkld_qry->submit();
            if(empty($nourut)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($nokb)){
                        $this->_check_id();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_init_edit($nokb);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($nokb)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_check_id($nokb);
                $this->template->build('form', $this->data);
            }
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
           'nokb'=> array(
                    'placeholder' => 'Nomor Transaksi',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    //'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),

        );
    }

    private function _init_edit($nokb = null){
        if(!$nokb){
            $nokb = $this->uri->segment(3);
            $nourut = $this->uri->segment(4);
        }
        $this->_check_id($nokb,$nourut);
        $this->data['form'] = array(
           'nokb'=> array(
                    'placeholder' => 'No. Kas/Bank Bengkel',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => $this->val[0]['nokb'],
                    'class'       => 'form-control',
                    'readonly'    => ''
            ),
           'nourut'=> array(
                    'type'         => 'hidden',
                    'placeholder' => 'No. Urut',
                    'id'           => 'nourut',
                    'value'       => $this->val[0]['nourut'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nocetak'=> array(
                    'placeholder' => 'No. Cetak',
                    'id'           => 'nocetak',
                    'value'       => $this->val[0]['nocetak'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'kddiv'=> array(
                    'placeholder' => 'Kode Divisi',
                    'id'           => 'kddiv',
                    'value'       => $this->val[0]['kddiv'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),

            'tglkb'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => $this->val[0]['tglkb'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => ''
            ),
        //    'jenis'=> array(
        //             'placeholder' => 'Jenis Transaksi',
        //             'id'      => 'jenis',
        //             'name'        => 'jenis',
        //             'value'       => $this->val[0]['nmrefkb'],
        //             'class'       => 'form-control',
        //             'required'    => '',
        //             'style'       => 'text-transform: uppercase;',
        //             // 'readonly'    => '',
        //     ),
            'jenis'=> array(
                    'attr'        => array(
                        'id'    => 'jenis',
                        'class' => 'form-control  select2',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('jenis'),
                    'name'     => 'jenis',
                    'required'    => '',
                    'placeholder' => 'Jenis Transaki',
             ),
           'nofaktur'=> array(
                    'placeholder' => 'No. Faktur',
                    'id'      => 'nofaktur',
                    'name'        => 'nofaktur',
                    'value'       => $this->val[0]['nofaktur'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'drkpd'=> array(
                    'placeholder' => 'Dari/Kepada',
                    'id'      => 'drkpd',
                    'name'        => 'drkpd',
                    'value'       => $this->val[0]['darike'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'    => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['ket'],
                    'name'     => 'ket',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'debet'=> array(
                    'placeholder' => 'Debet',
                    'id'    => 'debet',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['debet'],
                    'name'     => 'debet',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kredit'=> array(
                    'placeholder' => 'Kredit',
                    'id'    => 'kredit',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['kredit'],
                    'name'     => 'kredit',
                    'required'    => '',
                    'readonly' => '',
            ),

            //edit debet + kredit dari readonly
        );
    }
    private function _check_id($nokb,$nourut){
        if(empty($nokb)){
            redirect($this->data['edit']);
        }

        $this->val= $this->revkbbkld_qry->select_data($nokb,$nourut);

        if(empty($this->val)){
            redirect($this->data['edit']);
        }
    }

    private function validate($nokb,$nourut) {
      if(!empty($nokb) && !empty($nourut)){
          return true;
      }
      $config = array(
            array(
                    'field' => 'nokb',
                    'label' => 'No. Kas Bank',
                    'rules' => 'required|max_length[10]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => 'form-inline'
                        , 'target' => 'blank');
                    echo form_open($submit,$attributes);
                ?>
                    <div class="form-group">
                        <?php
                            echo form_label($form['periode_awal']['placeholder']);
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['periode_akhir']['placeholder']);
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>');
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                    <button type="submit" class="btn btn-success" name="submit" value="excel">
                        <i class="fa fa-file-excel-o"></i> Export Excel
                    </button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                    <table style="width: 3600px" class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr class="tr-head">
                            </tr>
                            <tr class="tr-head-tk">
                            </tr>
                        </thead>
                        <tbody class="tr-body"></tbody>
                        <tfoot class="tr-foot"></tfoot>
                    </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {
    $(".table-responsive").hide();
    $(".btn-tampil").click(function(){
        getPenjualanPerSales();
    });

    function getPenjualanPerSales(){
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rptpenjualansales/getPenjualanPerSales');?>",
            data: {"periode_awal":periode_awal
                    ,"periode_akhir":periode_akhir},
            beforeSend: function(msg){
                $(".table-responsive").hide();
                $(".tr-head").html('');
                $(".tr-head-tk").html('');
            },
            success: function(resp){
                $(".table-responsive").show();
                $(".tr-head").html('<th style="text-align: center;width: 10px;" rowspan="2">NO</th>'+
                                 '<th style="text-align: center;width: 180px;" rowspan="2">SALES</th>');
                $(".tr-head").append('<th style="text-align: center;width: 10px;" rowspan="2">TOTAL</th>');
                var obj = jQuery.parseJSON(resp);
                // Menampilkan kolom header start
                var col_num = 1;
                $.each(obj.header, function(key, data){
                    //console.log(col_num);
                    $(".tr-head").append('<th style="text-align: center;width: 80px;" colspan="2">'+data.nmtipe+'</th>');
                    $(".tr-head-tk").append('<th style="text-align: center;width: 40px;">T</th>'+
                                            '<th style="text-align: center;width: 40px;">K</th>');
                    col_num++;
                });
                // Menampilkan kolom header end
                setDetailPenjualanSales(obj.detail,obj.footer);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function setDetailPenjualanSales(detail,footer){
        // Menampilkan kolom detail start
        var tr_name = 1;
        $(".tr-body").html('');
        $.each(detail, function(key, data){
            $(".tr-body").append('<tr class="row'+tr_name+'">');
            $(".row"+tr_name).append('<td>'+tr_name+'</td>');
            var coldetail = 1;
            var subtotal = 0;
            $.each(data, function(key, value){
                if(!isNaN(value)){
                    subtotal = subtotal+Number(value);
                }else{
                    $(".row"+tr_name).append('<td>'+value+'</td>');
                }
            });
            $(".row"+tr_name).append('<td>'+subtotal+'</td>');
            $.each(data, function(key, value){
                if(!isNaN(value)){
                    $(".row"+tr_name).append('<td>'+value+'</td>');
                   // alert(tr_name);
                }
                coldetail++;
            });
            $(".tr-body").append('</tr>');
            tr_name++;
        });
        // Menampilkan kolom detail end
        setFooterPenjualanSales(footer);
    }

    function setFooterPenjualanSales(footer){
        // Menampilkan kolom footer start
        var tr_name = 'TOTAL';
        $(".tr-foot").html('');
        $.each(footer, function(key, data){
            $(".tr-foot").append('<tr class="row'+tr_name+'">');
            $(".row"+tr_name).append('<td colspan="2" align="center"><b>'+tr_name+'</b></td>');
            var coldetail = 1;
            var subtotal = 0;
            $.each(data, function(key, value){
                if(!isNaN(value)){
                    subtotal = subtotal+Number(value);
                }
            });
            $(".row"+tr_name).append('<td><b>'+subtotal+'</b></td>');
            $.each(data, function(key, value){
                $(".row"+tr_name).append('<td><b>'+value+'</b></td>');
                coldetail++;
            });
            $(".tr-foot").append('</tr>');
            tr_name++;
        });
        // Menampilkan kolom footer end

    }
});
</script>

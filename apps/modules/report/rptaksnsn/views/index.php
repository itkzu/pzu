<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>
			<div class="box-body">
				<div class="form-group">
					<?php
						echo '<div class="radio">';
						echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Periode Bulan <small>per bulan</small>');
						echo '</div>';
						echo form_input($form['periode']);
						echo form_error('akun_multi','<div class="note">','</div>');
					?>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Periode Awal');
								echo '</div>';
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Pilih Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							 ?>
						</div>

					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				</div>

				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 20%; text-align: center;" rowspan="2">Nama Aksesoris</th>
								<th style="width: 14%; text-align: center;" rowspan="2">Kategori Aksesoris</th>
								<th style="width: 13%; text-align: center;" rowspan="2">Harga</th>
								<th style="width: 10%; text-align: center;" colspan="2">Saldo Awal</th>
								<th style="width: 10%; text-align: center;" colspan="2">Masuk</th>
								<th style="width: 10%; text-align: center;" colspan="2">Keluar</th>
								<th style="width: 10%; text-align: center;" colspan="2">Adjustment</th>
								<th style="width: 10%; text-align: center;" colspan="2">Saldo Akhir</th>
							</tr>
							<tr>
								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<td style="text-align: center;" colspan="4"></td>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		disable();
		$("#rdakun_multi").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});
		$("#rdakun_between").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});

		$('#periode').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});
		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});
		$(".btn-tampil").click(function(){
			$(".dataTable").show();
			tampilData();
		});
	});

	function tampilData(){

		var periode = $("#periode").val();
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val();
		var periode_d = $('#rdakun_multi').is(':checked');
		if(periode_d){
			var ket = "periode_multi";
		} else {
			var ket = "periode_between";
		}

		var column = [];

		column.push({
			"aTargets": [ 2,4,6,8,10,12 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
			},
			"sClass": "right"
		});

		column.push({
			"aTargets": [ 3,5,7,9,11 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "nmaks" },
				{ "data": "nmaksgr" },
				{ "data": "harga" },
				{ "data": "saw_qty" },
				{ "data": "saw_total" },
				{ "data": "in_qty" },
				{ "data": "in_total" },
				{ "data": "out_qty" },
				{ "data": "out_total" },
				{ "data": "adj_qty" },
				{ "data": "adj_total" },
				{ "data": "sak_qty" },
				{ "data": "sak_total" }
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
							{ "name": "ket", "value": ket },
							{ "name": "periode", "value": periode },
							{ "name": "periode_awal", "value": periode_awal },
							{ "name": "periode_akhir", "value": periode_akhir }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptaksnsn/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			// jumlah TOTAL
			'footerCallback': function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// computing column Total of the complete result
				var saw_tot = api
					.column( 4 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var in_tot = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var out_tot = api
					.column( 8 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var adj_tot = api
					.column( 10 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var sak_tot =  parseInt(saw_tot+in_tot-out_tot+adj_tot);

				// Update footer by showing the total with the reference of the column index
				$( api.column( 1 ).footer() ).html('<b>Total</b>');
				$( api.column( 4 ).footer() ).html(numeral(saw_tot).format('0,0.00'));
				$( api.column( 6 ).footer() ).html(numeral(in_tot).format('0,0.00'));
				$( api.column( 8 ).footer() ).html(numeral(out_tot).format('0,0.00'));
				$( api.column( 10 ).footer() ).html(numeral(adj_tot).format('0,0.00'));
				$( api.column( 12 ).footer() ).html(numeral(sak_tot).format('0,0.00'));
			},
			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print', footer: true,
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {

		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
	}

	function disable(){
		$(".dataTable").hide();
		$("#periode").prop("disabled", true);
		$(".btn-tampil").prop("disabled", true);
		$("#periode_awal").prop("disabled", true);
		$("#periode_akhir").prop("disabled", true);
	}

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#periode").prop("disabled", false);
			$("#periode_awal").prop("disabled", true);
			$("#periode_akhir").prop("disabled", true);
		}else{
			$("#periode").prop("disabled", true);
			$("#periode_awal").prop("disabled", false);
			$("#periode_akhir").prop("disabled", false);

		}
	}
</script>

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Msttrfbbn_qry
 *
 * @author adi
 */
class Msttrfbbn_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function select_data($kota) {
        $kota2 = str_replace('-', ' ', $kota);
        $this->db->select("*");
        $this->db->where('kota',$kota2);
        $query = $this->db->get('pzu.bbn_tarif');
        return $query->result_array();
    } 

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kota']) ){
            $id = $_GET['kota'];
        }else{
            $id = '';
        }

        $aColumns = array('no',
                            'kota',
                            'byproses',
                            'byjasa',
                            'kota');
	$sIndexColumn = "kota";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT ''::varchar as no, kota, byproses, byjasa
                        FROM pzu.bbn_tarif ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
      {
          if($_GET['iDisplayStart']>0){
              $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                      intval( $_GET['iDisplayStart'] );
          }
      }

      $sOrder = "";
      if ( isset( $_GET['iSortCol_0'] ) )
      {
              $sOrder = " ORDER BY  ";
              for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
              {
                      if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                      {
                              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                      ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                      }
              }

              $sOrder = substr_replace( $sOrder, "", -2 );
              if ( $sOrder == " ORDER BY" )
              {
                      $sOrder = "";
              }
      }
      $sWhere = "";

      if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
      {
  $sWhere = " Where (";
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  }
  $sWhere = substr_replace( $sWhere, "", -3 );
  $sWhere .= ')';
      }

      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
              if ( $sWhere == "" )
              {
                  $sWhere = " WHERE ";
              }
              else
              {
                  $sWhere .= " AND ";
              }
              //echo $sWhere."<br>";
              $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
      }


      /*
       * SQL queries
       * QUERY YANG AKAN DITAMPILKAN
       */
      $sQuery = "
              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
              FROM   $sTable
              $sWhere
              $sOrder
              $sLimit
              ";

      // echo $sQuery;

      $rResult = $this->db->query( $sQuery);

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";    //SELECT FOUND_ROWS()

      $rResultFilterTotal = $this->db->query( $sQuery);
      $aResultFilterTotal = $rResultFilterTotal->result_array();
      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";
      $rResultTotal = $this->db->query( $sQuery);
      $aResultTotal = $rResultTotal->result_array();
      $iTotal = $aResultTotal[0]['jml'];

      $output = array(
              "sEcho" => intval($_GET['sEcho']),
              "iTotalRecords" => $iTotal,
              "iTotalDisplayRecords" => $iFilteredTotal,
              "aaData" => array()
      );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                } 
                $kota = str_replace(' ', '-', $aRow['kota']);
                $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('msttrfbbn/edit/'.$kota)."\">Edit</a>"; 
                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    } 

    public function submit() {
        try {
            $array = $this->input->post();  
            if($array['ket']==='0'){ 
                $array['kota']          = strtoupper($array['kota']);
                $array['byproses']      = $array['byproses'];
                $array['byjasa']        = $array['byjasa'];  
                $array2  = array(
                    'kota' => $array['kota'],
                    'byproses' => $array['byproses'],
                    'byjasa' => $array['byjasa']);

                $resl = $this->db->insert('bbn_tarif',$array2);
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif($array['ket']==='1'){
                $array['kota']          = strtoupper($array['kota']);
                $array['byproses']      = $array['byproses'];
                $array['byjasa']        = $array['byjasa'];  
                $array2  = array(
                    'kota' => $array['kota'],
                    'byproses' => $array['byproses'],
                    'byjasa' => $array['byjasa']);

                $this->db->where('kota', strtoupper($array['kota2']));
                $resl = $this->db->update('bbn_tarif', $array2);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error(); 
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{ 
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }else{ 
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {           
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array( 
            'state' => $this->state, 
            'msg' => $this->res,
            );
        // $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr); 
    } 

}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Jpppn
 *
 * @author
 */
class Jpppn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(    
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('jpppn/submit'),
            'add' => site_url('jpppn/add'),
            'edit' => site_url('jpppn/edit'),
            'reload' => site_url('jpppn'),
        );
        $this->load->model('jpppn_qry');
        $supplier = $this->jpppn_qry->getDataSupplier();
        foreach ($supplier as $value) {
            $this->data['kdsup'][$value['kdsup']] = $value['nmsup'];
        }
        /*
        $kdaks = $this->jpppn_qry->getKodejpppn();
        $this->data['kdaks'] = array(
            "" => "-- Pilih Kode Aksesoris --",
          );
        foreach ($kdaks as $value) {
            $this->data['kdaks'][$value['kdaks']] = $value['kdaks']." - ".$value['nmaks'];
          }
          */

        $this->data['statppn'] = array(
            "I" => "INCLUDE",
            "E" => "EXCLUDE",
            "N" => "NON-PPN",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 

    public function json_dgview() {
        echo $this->jpppn_qry->json_dgview();
    }

    public function set_nokb() {
        echo $this->jpppn_qry->set_nokb();
    } 

    public function getKey() {
        echo $this->jpppn_qry->getKey();
    }

    public function delDetail() {
        echo $this->jpppn_qry->delDetail();
    }

    public function getdetPO() {
        echo $this->jpppn_qry->getdetPO();
    }   

    public function json_dgview_detail() {
        echo $this->jpppn_qry->json_dgview_detail();
    }   

    public function addDetail() {
        echo $this->jpppn_qry->addDetail();
    }   

    public function set_do() {
        echo $this->jpppn_qry->set_do();
    }   

    public function submit() {
        echo $this->jpppn_qry->submit();
    }

    public function batal() {
        echo $this->jpppn_qry->batal();
    }
    private function _init_add(){ 

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
            'nokb'     => array(
                    'placeholder' => 'No. Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'set_nokb();',
                    // 'readonly' => '',
            ),
            'nmkb'     => array(
                    'placeholder' => 'Jenis Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nmkb',
                    'name'        => 'nmkb',
                    'value'       => set_value('nmkb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'nilai'     => array(
                    'placeholder' => 'Nilai Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nilai',
                    'name'        => 'nilai',
                    'value'       => set_value('nilai'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ),
            'tglkb'     => array(
                    'placeholder' => 'Tgl Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ), 
            'nmleasing'     => array(
                    'placeholder' => 'Nama Leasing',
                    //'type'        => 'hidden',
                    'id'          => 'nmleasing',
                    'name'        => 'nmleasing',
                    'value'       => set_value('nmleasing'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'tot_ar_jp'     => array(
                    'placeholder' => 'Total AR PPN JP',
                    //'type'        => 'hidden',
                    'id'          => 'tot_ar_jp',
                    'name'        => 'tot_ar_jp',
                    'value'       => set_value('tot_ar_jp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ),  
            'ket'     => array(
                    'placeholder' => 'Keterangan',
                    //'type'        => 'hidden',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'selisih'     => array(
                    'placeholder' => 'Selisih',
                    //'type'        => 'hidden',
                    'id'          => 'selisih',
                    'name'        => 'selisih',
                    'value'       => set_value('selisih'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'ket_selisih'     => array(
                    'placeholder' => 'Keterangan Selisih',
                    //'type'        => 'hidden',
                    'id'          => 'ket_selisih',
                    'name'        => 'ket_selisih',
                    'value'       => set_value('ket_selisih'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nopo = $this->uri->segment(3);
        }
        $this->_check_id($nopo);
        $this->data['form'] = array(    
            'nokb'     => array(
                    'placeholder' => 'No. Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => $this->val[0]['nokb'],  
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'onkeyup'     => 'set_nokb();',
                    'readonly' => '',
            ),
            'nmkb'     => array(
                    'placeholder' => 'Jenis Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nmkb',
                    'name'        => 'nmkb',
                    'value'       => $this->val[0]['nmkb'],  
                    // 'value'       => set_value('nmkb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'nilai'     => array(
                    'placeholder' => 'Nilai Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nilai',
                    'name'        => 'nilai',
                    'value'       => $this->val[0]['nilai'],  
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ),
            'tglkb'     => array(
                    'placeholder' => 'Tgl Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => $this->val[0]['tglkb'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ), 
            'nmleasing'     => array(
                    'placeholder' => 'Nama Leasing',
                    //'type'        => 'hidden',
                    'id'          => 'nmleasing',
                    'name'        => 'nmleasing',
                    'value'       => $this->val[0]['nmleasing'],  
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'kdleasing'     => array(
                    'placeholder' => 'Kode Leasing',
                    'type'        => 'hidden',
                    'id'          => 'kdleasing',
                    'name'        => 'kdleasing',
                    'value'       => $this->val[0]['kdleasing'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'tot_ar_jp'     => array(
                    'placeholder' => 'Total AR PPN JP',
                    //'type'        => 'hidden',
                    'id'          => 'tot_ar_jp',
                    'name'        => 'tot_ar_jp',
                    'value'       => set_value('tot_ar_jp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ),  
            'ket'     => array(
                    'placeholder' => 'Keterangan',
                    //'type'        => 'hidden',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => $this->val[0]['ket'], 
                    'class'       => 'form-control ',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'selisih'     => array(
                    'placeholder' => 'Selisih',
                    //'type'        => 'hidden',
                    'id'          => 'selisih',
                    'name'        => 'selisih',
                    'value'       => set_value('selisih'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'ket_selisih'     => array(
                    'placeholder' => 'Keterangan Selisih',
                    //'type'        => 'hidden',
                    'id'          => 'ket_selisih',
                    'name'        => 'ket_selisih',
                    'value'       => $this->val[0]['ket_selisih'],  
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),  
            'kunci'=> array(
                    'placeholder' => 'Masukkan Kata Kunci <small><i>(No. DO, Nama Pemesan, Nama PO, dan Nama STNK)</i></small>',
                    'attr'        => array(
                        'id'    => 'kunci',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kunci'),
                    'name'     => 'kunci',
                    'style'       => 'text-transform: uppercase;',
            ), 
            'nodo'     => array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'noso'     => array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'tgldo'     => array(
                    'placeholder' => 'Tgl DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'tglso'     => array(
                    'placeholder' => 'Tgl SPK',
                    //'type'        => 'hidden',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'nama'     => array(
                    'placeholder' => 'Nama Pemesan',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'nama_p'     => array(
                    'placeholder' => 'Atas Nama PO',
                    //'type'        => 'hidden',
                    'id'          => 'nama_p',
                    'name'        => 'nama_p',
                    'value'       => set_value('nama_p'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'nama_s'     => array(
                    'placeholder' => 'Nama STNK',
                    //'type'        => 'hidden',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'alamat'     => array(
                    'placeholder' => 'Alamat STNK',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            // nominal 
            'jp'     => array(
                    'placeholder' => 'Total JP',
                    //'type'        => 'hidden',
                    'id'          => 'jp',
                    'name'        => 'jp',
                    'value'       => set_value('jp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'readonly' => '',
            ), 
            'jp_bayar'     => array(
                    'placeholder' => 'JP Terbayar',
                    //'type'        => 'hidden',
                    'id'          => 'jp_bayar',
                    'name'        => 'jp_bayar',
                    'value'       => set_value('jp_bayar'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'readonly' => '',
            ),  
            'jp_saldo'     => array(
                    'placeholder' => 'Saldo JP',
                    //'type'        => 'hidden',
                    'id'          => 'jp_saldo',
                    'name'        => 'jp_saldo',
                    'value'       => set_value('jp_saldo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'readonly' => '',
            ),  
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->jpppn_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstcptarget
 *
 * @author adi
 */
class Mstcptarget extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('mstcptarget/submit'),
            'add' => site_url('mstcptarget/add'),
            'edit' => site_url('mstcptarget/edit'),
            'reload' => site_url('mstcptarget'),
        );
        $this->load->model('mstcptarget_qry');
        
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){  
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function json_dgview() {
        echo $this->mstcptarget_qry->json_dgview();
    }
    
    public function submit() {  
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        
        if($this->validate($id,$stat) == TRUE){
            $res = $this->mstcptarget_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($id);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($id)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_check_id($id);
                $this->template->build('form', $this->data);
            }
        }
    }
    
    private function _init_add(){
        
        
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'id',
                    'name'        => 'id',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'periode'=> array(
                    'placeholder' => 'Periode Target',
                    'id'      => 'periode',
                    'name'        => 'periode',
                    'value'       => set_value('periode'),
                    'class'       => 'form-control month',
                    'required'    => '',
            ),
           'jml_hr_kerja'=> array(
                    'placeholder' => 'Jumlah Hari Kerja',
                    'id'      => 'jml_hr_kerja',
                    'name'        => 'jml_hr_kerja',
                    'value'       => set_value('jml_hr_kerja'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_total'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Bulan',
                    'id'      => 'cp_target_total',
                    'name'        => 'cp_target_total',
                    'value'       => set_value('cp_target_total'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_harian'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Hari',
                    'id'      => 'cp_target_harian',
                    'name'        => 'cp_target_harian',
                    'value'       => set_value('cp_target_harian'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
        );
    }
    
    private function _init_edit(){
        $groupid = $this->uri->segment(3);
        $this->_check_id($groupid);
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'id',
                    'name'        => 'id',
                    'value'       => $this->val[0]['id'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
           'periode'=> array(
                    'placeholder' => 'Periode Target',
                    'id'      => 'periode',
                    'name'        => 'periode',
                    'value'       => $this->val[0]['periode'],
                    'class'       => 'form-control month',
                    'required'    => '',
                    
            ),
           'jml_hr_kerja'=> array(
                    'placeholder' => 'Jumlah Hari Kerja',
                    'id'      => 'jml_hr_kerja',
                    'name'        => 'jml_hr_kerja',
                    'value'       => $this->val[0]['jml_hr_kerja'],
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_total'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Bulan',
                    'id'      => 'cp_target_total',
                    'name'        => 'cp_target_total',
                    'value'       => $this->val[0]['cp_target_total'],
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_harian'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Hari',
                    'id'      => 'cp_target_harian',
                    'name'        => 'cp_target_harian',
                    'value'       => $this->val[0]['cp_target_harian'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
        );
    }
    
    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }
        
        $this->val= $this->mstcptarget_qry->select_data($id);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'periode',
                    'label' => 'Periode Target',
                    'rules' => 'required|max_length[10]',
                ),
            array(
                    'field' => 'jml_hr_kerja',
                    'label' => 'Jumlah Hari Kerja',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'cp_target_total',
                    'label' => 'Jumlah Capaian Target Per Bulan',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'cp_target_harian',
                    'label' => 'Jumlah Capaian Target Per Hari',
                    'rules' => 'required|integer',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

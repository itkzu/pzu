<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
			<!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
			-->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">

                    <div class="form-import main-form">

                        <div class="col-lg-8">

                        	<div class="form-group">
                                <label for="exampleInputFile">Data Jurnal Memorial Bengkel</label>

                                <div class="fileinputBeli fileinput-new input-group" name="fileinputBeli" id="fileinputBeli" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                    	<i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    	<span class="fileinput-filename"></span>
                                	</div>

                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Cari...</span>
                                      	<span class="fileinput-exists">Ubah</span>
                                      	<input type="file" name="fileBeli" id="fileBeli" data-original-value="" required accept=".xls, .xlsx">
                                	</span>

    								<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

    								<span class="input-group-btn fileinput-exists">
    									<button class="btn btn-primary" name="importBeli" id="importBeli"><i class="fa fa-cloud-upload"></i> Import</button>
                                	</span>

                              	</div>
    					  	</div>

                        </div>

                        <div class="col-xs-4">
                            <label>&nbsp;</label>
                            <div><a href="<?=site_url('impjmbkl/download_template_jm');?>" name="d_jm" id="d_jm" target="_blank">
                                <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                    Download Template
                                </button></a>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
 
                    </div>
                </div>


                <span id="status" class="hidden"></span>
                <div class="form-group" id="process" style="display:none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" id="file-progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row"> 
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?=form_label($form['kddiv']['placeholder']);?>
                                        <div class="input-group">
                                            <?php
                                                echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                                echo form_error('kddiv', '<div class="note">', '</div>');
                                            ?> 
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tgljurnal']['placeholder']);
                                            echo form_input($form['tgljurnal']);
                                            echo form_error('tgljurnal','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div> 

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">No. Referensi</th>
                                            <th style="width: 100px;text-align: center;">Kode Akun</th>
                                            <th style="text-align: center;">Nama Akun</th>
                                            <th style="width: 150px;text-align: center;">Debet</th>
                                            <th style="width: 150px;text-align: center;">Kredit</th>
                                            <th style="width: 300px;text-align: center;">Keterangan</th> 
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center;"></th>
                                            <th style="text-align: center;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th> 
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-submit">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>

        	<?php echo form_close(); ?>
      	</div>
      	<!-- /.box -->
	</div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        tbl();
        $(".btn-reset").click(function(){
            //disabled();
            window.location.reload();
        });
        $(".btn-submit").click(function(){
            //disabled();
            submit();
        });
        $(".btn-batal").click(function(){
            //disabled();
            btl();
        });
        $("#kddiv").change(function(){
            tbl();
        });

        $("#fileinputKB").click(function(){
            $(".upload_kb").show();
        });

        //import data Pembelian
    	$('#importBeli').click(function(event){
            event.preventDefault();
            var formData = new FormData();
    		formData.append('file', $('input[type=file]')[0].files[0]);
    		formData.append('kddiv',  $("#kddiv").val());

    		$.ajax({
    			url:"<?php echo base_url(); ?>impjmbkl/importJm",
    			method:"POST",
                data:formData,
                dataType:'json',
    			contentType:false,
    			cache:false,
    			processData:false,
   				success:function(resp){

                    var obj = resp;
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputBeli').fileinput('clear');
                                    $('#importBeli').attr('disabled', false);
                                    tbl();
                                });
                            }
                        });
                }
            })
    	});

        function progress_bar_process(percentage, timer){

            $('.progress-bar').css('width', percentage + '%');
            if(percentage > 100){
                clearInterval(timer);
                $('#process').css('display', 'none');
                $('.progress-bar').css('width', '0%');

                $('#importBeli').attr('disabled', false);
                $('#importServis').attr('disabled', false);
                $('#importJual').attr('disabled', false);
                $('#importKB').attr('disabled', false);

                setTimeout(function(){
                    }, 5000);
                $('#status').html("ok");
            }
        }

        function disabled(){
            $(".upload_kb").hide();
            $("#kddiv").attr("disabled",false);
            $(".btn-set").show();
            $(".btn-reset").hide();
            $('.form-import').hide();
            //$("#fileKB").remove();
            //$("#fileBeli").remove();
            //$("#fileJual").remove();
            //$("#fileServis").remove();
        }

        function enabled(){
            $("#kddiv").attr("disabled",true);
            $(".btn-set").hide();
            $(".btn-reset").show();
            $('.form-import').show();
        }

        function tbl(){
            var column = [];

            //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
            //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

            column.push({
                "aTargets": [ 3,4 ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                },
                "sClass": "right"
            }); 

            table = $('.dataTable').DataTable({
                "aoColumnDefs": column,
                "columns": [
                    { "data": "noref"  },
                    { "data": "kdakun"  },
                    { "data": "nmakun" },
                    { "data": "debet"   },
                    { "data": "kredit"  },
                    { "data": "ket"  }
                ],
                "lengthMenu": [[ -1], [ "Semua Data"]],
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                //"ordering": false,
                "bSort": false,
                "bAutoWidth": false,
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                    aoData.push({"name": "kddiv", "value": $("#kddiv").val()});
                    $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                },
                'rowCallback': function(row, data, index){
                },
                "sAjaxSource": "<?=site_url('impjmbkl/json_dgview_detail');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                // jumlah TOTAL
                'footerCallback': function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // computing column Total of the complete result
                    var debet = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var kredit = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer by showing the total with the reference of the column index
                    $( api.column( 1 ).footer() ).html('Total');
                    $( api.column( 3 ).footer() ).html(numeral(debet).format('0,0.00'));
                    $( api.column( 4 ).footer() ).html(numeral(kredit).format('0,0.00'));
                },
                buttons: [{
                    extend:    'excelHtml5', footer: true,
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                    },
                    exportOptions: {orthogonal: 'export'}
                }],
                "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                    //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                        that
                            .search( this.value )
                            .draw();
                    //}
                } );
            });
        }


    });


    function submit(){ 
        var tgljurnal = $("#tgljurnal").val();
        var kddiv = $("#kddiv").val();  
        swal({
            title: "Konfirmasi Data Jurnal!",
            text: "Data Jurnal Memorial Bengkel!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("impjmbkl/tambah");?>",
                data: {"tgljurnal":tgljurnal ,"kddiv":kddiv}, 
                success: function(resp){
                     // console.log(JSON.parse(resp));
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        // }, function(){
                        //     if (data.tipe==="success"){
                        //         window.location.href = '<?=site_url('impjmbkl');?>';
                        //     }
                        // });
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('impjmbkl');?>';
                        }
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }


    function btl(){  
        var tgljurnal = $("#tgljurnal").val();
        var kddiv = $("#kddiv").val();  
        swal({
            title: "Konfirmasi Batal Jurnal!",
            text: "Data Jurnal Memorial Bengkel!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("impjmbkl/batal");?>",
                data: {"tgljurnal":tgljurnal ,"kddiv":kddiv}, 
                success: function(resp){ 
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe 
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('impjmbkl');?>';
                        }
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }


    // function getkb(){
    //
    //     var kddiv = $("#kddiv").val();
    //
    //
    //     var select = document.getElementById("kb");
    //     var length = select.options.length;
    //     for (i = length-1; i >= 0; i--) {
    //         select.options[i] = null;
    //     }
    //
    //     var select = document.getElementById("kb");
    //     var options = [];
    //     var option = document.createElement('option');
    //
    //     $.ajax({
  	// 	    type: "POST",
    //   		url: "<?=site_url("impjmbkl/getkb");?>",
    //   		data: {"kddiv":kddiv},
    //   		beforeSend: function() {
  	// 	    },
    //   		success: function(resp){
    //   			var obj = jQuery.parseJSON(resp);
    //   			$.each(obj, function(key, value){
    //                 //$('#dk').attr("disabled",false);
    //
    //                 option.value = value.kdkb;
    //                 option.text = value.nmkb;
    //                 options.push(option.outerHTML);
    //             });
    //
    //             select.insertAdjacentHTML('beforeEnd', options.join('\n'));
    //
    //         },
    //         error:function(event, textStatus, errorThrown) {
    //             swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //         }
    //     });
    // }

</script>

  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style>
      caption {
          padding-top: 8px;
          padding-bottom: 8px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto; 
      } 

      table{
        margin-top: 5px; 
        margin-bottom: 1px; 
      }

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }
  </style>
  <?php
  $file = $this->uri->segment(4);
  if($file==="excel"){
      header("Content-type: application/vnd-ms-excel");
      header("Content-Disposition: attachment; filename=Cetak DO " .date('Y-m-d H:i:s').".xls");
      header("Pragma: no-cache");
      header("Expires: 0");
  }

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $namaheader = array(
      // array('data' => '&nbsp;'
      //                     , 'colspan' => 24
      //                     , 'style' => 'text-align: center; width: 100%; font-size: 16px;'),
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;'
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 16px;'), 
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);   
  $col1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 20
                          , 'style' => 'text-align: center; width: 83%; font-size: 20px;'), 
      array('data' => $data[0]['wkt']
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 17%; font-size: 20px;'), 
  );
  $col2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 16px;'),  
  );
  $col3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),  
  );
  $col4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),  
  );
  $col5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),  
  );
  $col6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  );
  $col7 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 8%; font-size: 13px;'), 
      array('data' => '&nbsp;&nbsp;'.$data[0]['nama']
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 13px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 13px;'), 
      array('data' => $data[0]['tgldo']
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 13px;'), 
  );
  $col8 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 4%; font-size: 13px;'), 
      array('data' => '&nbsp;&nbsp;'.$data[0]['alamat'].'&nbsp;'.$data[0]['kel']
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 13px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 9
                          , 'style' => 'text-align: center; width: 32%; font-size: 13px;'), 
      array('data' => $data[0]['noso']
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 24%; font-size: 13px;'), 
  );
  $col9 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 4%; font-size: 13px;'), 
      array('data' => '&nbsp;&nbsp;'.$data[0]['kec']."&nbsp;".$data[0]['kota']
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 13px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 9
                          , 'style' => 'text-align: center; width: 32%; font-size: 13px;'), 
      array('data' => $data[0]['tglso']
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 24%; font-size: 13px;'), 
  );
  $col10 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 32px;'),  
  );
  $c1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  );
  $c2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $c3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  );
  $c4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $col11 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),  
      array('data' => '&nbsp;&nbsp;HONDA'
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $p1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $col12 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'.$data[0]['kdtipe'].'&nbsp;-&nbsp;'.$data[0]['nmtipe']
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),   
  );
  $p2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $col13 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'.$data[0]['nora'] 
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $p3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col14 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'.$data[0]['nosin'] 
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $p4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col15 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'.$data[0]['nmwarna'] 
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $p5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col16 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'.$data[0]['tahun'] 
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),    
  );
  $p6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col17 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 11
                          , 'style' => 'text-align: center; width: 44%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;NEW / BARU 100%'
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),    
  );
  $p7 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 123px;'),  
  );
  $col18 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;1 (SATU) SAFETY HELM'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),   
  );
  $p8 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col19 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;1 (SATU) BUAH JAKET'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),   
  ); 
  $col20 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  ); 
  $col21 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'.$data[0]['ket'] 
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $p11 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );  
  $col24 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 12px;'),  
  );
  $p14 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col25 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;SALESMAN : '.$data[0]['nmsales']
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $p15 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );  
  $this->table->add_row($col1);
  $this->table->add_row($col2); 
  $this->table->add_row($col3); 
  $this->table->add_row($col4); 
  $this->table->add_row($col5); 
  $this->table->add_row($col6); 
  $this->table->add_row($col7); 
  $this->table->add_row($col8); 
  $this->table->add_row($col9); 
  $this->table->add_row($col10); 
  $this->table->add_row($c1); 
  $this->table->add_row($c2); 
  $this->table->add_row($c3); 
  $this->table->add_row($c4); 
  $this->table->add_row($col11); 
  $this->table->add_row($p1); 
  $this->table->add_row($col12); 
  $this->table->add_row($p2); 
  $this->table->add_row($col13); 
  $this->table->add_row($p3); 
  $this->table->add_row($col14); 
  $this->table->add_row($p4);  
  $this->table->add_row($col15); 
  $this->table->add_row($p5);  
  $this->table->add_row($col16); 
  $this->table->add_row($p6);  
  $this->table->add_row($col17); 
  $this->table->add_row($p7);  
  $this->table->add_row($col18); 
  $this->table->add_row($p8);  
  $this->table->add_row($col19); 
  // $this->table->add_row($p9);  
  $this->table->add_row($col20); 
  // $this->table->add_row($p10);  
  $this->table->add_row($col21); 
  $this->table->add_row($p11);  
  // $this->table->add_row($col22); 
  // $this->table->add_row($p12);    
  $this->table->add_row($col25); 
  $this->table->add_row($p15);  

  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();    

  $col26 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; CEK DATA KONSUMEN &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
  );
  $p16 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; CEK NOKA & NOSIN SPD MOTOR DENGAN BASTIK &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),   
  );
  $col27 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; MENJELASKAN FITUR SPD MOTOR &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),   
  );
  $p17 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; MENJALANSI GARANSI KELISTRIKAN 1TH/10.000 KM &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),   
  );
  $col28 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; MENJELASKAN GARANSI MESIN 3TH/30.000 KM &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),   
  );
  $p18 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; MENJELASKAN GARANSI PGMFI 5TH/50.000KM &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
  );
  $col29 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; MENJELASKAN KPB DAN KELENGKAPAN SPD MOTOR &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
  );
  $p19 = array(
      array('data' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp; IKAN KARTU APRESIASI DEALER KEPADA KONSUMEN &nbsp;' 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
  ); 
  $this->table->add_row($col26); 
  $this->table->add_row($p16);  
  $this->table->add_row($col27); 
  $this->table->add_row($p17);  
  $this->table->add_row($col28); 
  $this->table->add_row($p18);  
  $this->table->add_row($col29); 
  $this->table->add_row($p19);   
  $template2 = array(
          'table_open'            => '<table style="border-collapse: collapse;" class="center" border="1" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template2);
  echo $this->table->generate();    


  $t1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $i1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  ); 
  $t2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 8
                          , 'style' => 'text-align: center; width: 32%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 40%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
  );
  $i2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  ); 
  $t3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; width: 100%; font-size: 12px;'),     
  );
  $i3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );  
  $q1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25 
                          , 'style' => 'text-align: center; width: 100%; font-size: 12px;'),     
  );
  $q2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );  
  $q3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; width: 100%; font-size: 12px;'),     
  );
  $q4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );  
  // $q5 = array(
  //     array('data' => '&nbsp;'
  //                         , 'colspan' => 25
  //                         , 'style' => 'text-align: center; width: 100%; font-size: 12px;'),     
  // );
  // $q6 = array(
  //     array('data' => '&nbsp;'
  //                         , 'colspan' => 25
  //                         , 'style' => 'text-align: center; font-size: 3px;'),  
  // );  
  // $q7 = array(
  //     array('data' => '&nbsp;'
  //                         , 'colspan' => 25
  //                         , 'style' => 'text-align: center; width: 100%; font-size: 12px;'),     
  // );
  // $q8 = array(
  //     array('data' => '&nbsp;'
  //                         , 'colspan' => 25
  //                         , 'style' => 'text-align: center; font-size: 3px;'),  
  // );   
  $t5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),   
      array('data' => '&nbsp;&nbsp;'
                          , 'colspan' => 15
                          , 'style' => 'text-align: left; width: 60%; font-size: 12px;'),        
      array('data' => '&nbsp;&nbsp;DISERAHKAN OLEH :'
                          , 'colspan' => 8
                          , 'style' => 'text-align: left; width: 32%; font-size: 12px;'),  
  );
  $i5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  ); 


  $this->table->add_row($t1); 
  $this->table->add_row($i1);   
  $this->table->add_row($t2); 
  $this->table->add_row($i2);   
  $this->table->add_row($t3); 
  $this->table->add_row($i3);   
  $this->table->add_row($q1);  
  $this->table->add_row($q2);  
  $this->table->add_row($q3);  
  $this->table->add_row($q4);  
  // $this->table->add_row($q5);  
  // $this->table->add_row($q6);  
  // $this->table->add_row($q7);  
  // $this->table->add_row($q8);      
  $this->table->add_row($i5);  
  $this->table->add_row($t5);  

  $template3 = array(
          'table_open'            => '<table style="border-collapse: collapse;"  width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template3);
  echo $this->table->generate(); 

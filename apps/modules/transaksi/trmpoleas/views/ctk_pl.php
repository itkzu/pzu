  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style>
      caption {
          padding-top: 8px;
          padding-bottom: 8px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto;
          font-family: verdana;
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }
  </style>
  <?php 

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $namaheader = array(
      // array('data' => '&nbsp;'
      //                     , 'colspan' => 24
      //                     , 'style' => 'text-align: center; width: 100%; font-size: 15px;'),
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;'
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 15px;'), 
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);   
  $col1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; width: 83%; font-size: 70px;'),  
  );
  $col2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: ;'),  
  );
  $tambah1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 18px;'),  
  );
  $col3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),  
      array('data' => $data[0]['nmleasing'].' QQ '.$data[0]['nama'].', '.$data[0]['alamat'].'&nbsp;'.$data[0]['kel'].' - '.$data[0]['kota']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 76%; font-size: 15px;'),  
  );
  $col4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),  
  );
  $col5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),  
      array('data' => number_format($data[0]['pelunasan_fincoy'],2,",",".")
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 76%; font-size: 15px;'), 
  );
  $col6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $col7 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),  
      array('data' => penyebut($data[0]['pelunasan_fincoy']).' RUPIAH'
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 76%; font-size: 15px;'),  
  );
  $col8 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $col9 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 20px;'), 
  );
  $tambah2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 18px;'),  
  );
  $col10 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),  
      array('data' => "PELUNASAN 1 (SATU) SMH TIPE ".$data[0]['nmtipe']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 76%; font-size: 15px;'),    
  );
  $col11 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),   
  );
  $col12 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),  
      array('data' => $data[0]['kdtipe']
                          , 'colspan' => 19
                          , 'style' => 'text-align: left; width: 76%; font-size: 15px;'),    
  );
  $col13 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),    
  );
  $col14 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),   
      array('data' => 'NORA'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 15px;'), 
      array('data' => ' : '.$data[0]['nora']
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 15px;'),   
      array('data' => 'WARNA'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 15px;'),  
      array('data' => ' : '.$data[0]['nmwarna']
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 15px;'),     
  );
  $col15 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),    
  ); 
  $col16 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; width: 24%; font-size: 15px;'),  
      array('data' => 'NOSIN'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 15px;'), 
      array('data' => ' : '.$data[0]['nosin']
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 15px;'),   
      array('data' => 'NOPOL'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 15px;'),  
      array('data' => ' : '
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 15px;'),        
  );
  $col17 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 13px;'),    
  );
  $col18 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 20
                          , 'style' => 'text-align: center; width: 80%; font-size: 15px;'),  
      array('data' => $this->apps->dateConvert(date("Y-m-d")) 
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 20%; font-size: 15px;'),     
  );
  $col19 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 60px;'),    
  ); 
  $col20 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 18
                          , 'style' => 'text-align: center; width: 75%; font-size: 15px;'),  
      array('data' => $this->session->userdata('data')['adh']
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 25%; font-size: 15px;'),     
  );
  $col21 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 3px;'),    
  );
  $this->table->add_row($col1);
  $this->table->add_row($col2); 
  $this->table->add_row($tambah1); 
  $this->table->add_row($col3); 
  $this->table->add_row($col4); 
  $this->table->add_row($col5); 
  $this->table->add_row($col6); 
  $this->table->add_row($col7); 
  $this->table->add_row($col8); 
  $this->table->add_row($col9); 
  $this->table->add_row($tambah2); 
  $this->table->add_row($col10);  
  $this->table->add_row($col11);  
  $this->table->add_row($col12);  
  $this->table->add_row($col13);  
  $this->table->add_row($col14);  
  $this->table->add_row($col15);  
  $this->table->add_row($col16);  
  $this->table->add_row($col17);  
  $this->table->add_row($col18);  
  $this->table->add_row($col19);  
  $this->table->add_row($col20);  
  // $this->table->add_row($col21);  
  // $this->table->add_row($col22);  
  // $this->table->add_row($col25);  

  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();    
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Donew
 *
 * @author adi
 */
class Donew extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main'    => $this->msg_main,
            'msg_detail'  => $this->msg_detail,

            'submit'      => site_url('donew/submit'),
            'ctk_do'      => site_url('donew/ctk_do'),
            'ctk_um'      => site_url('donew/ctk_um'),
            'ctk_stk'     => site_url('donew/ctk_stk'),
            'ctk_stkkpb'     => site_url('donew/ctk_stkkpb'),
            'ctk_krd'     => site_url('donew/ctk_krd'),
            'add'         => site_url('donew/add'),
            'edit'        => site_url('donew/edit'),
            'reload'      => site_url('donew'),
        );
        $this->load->model('donew_qry');
        $cabang = $this->donew_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }

        $leas = $this->donew_qry->getleas();
        $this->data['kdleasing'] = array(
            "T" => "TUNAI"
        );

        foreach ($leas as $value) {
            $this->data['kdleasing'][$value['kdleasing']] = $value['kdleasing'];
        }

        $salesins = $this->donew_qry->getkdsalesins();

        foreach ($salesins as $value) {
            $this->data['kdsales_ins'][$value['kdsales_ins']] = $value['nmsales_ins'];
        }  

        $this->data['stat_otr'] = array(
            "N" => "ON THE ROAD",
            "F" => "OFF THE ROAD"
        );

        $this->data['ket_astra'] = array(
            "T" => "TUNAI",
            "K" => "KREDIT"
        );

        $this->data['promo'] = array(
            "T" => "TIDAK",
            "Y" => "GRATIS OLI"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    } 

    public function printAll() {
        $this->data['data'] = $this->mstrefposkb_qry->printAll();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }
    
    public function ctk_do() {  
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->donew_qry->ctk_do($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_do',$this->data);  
    } 
    
    public function ctk_um() {  
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->donew_qry->ctk_um($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_um',$this->data);  
    }  

    public function ctk_stk() {
        $nodo = $this->uri->segment(3); 
        $alamat = $this->uri->segment(4); 
        $margin = $this->uri->segment(5); 
        $this->data['data'] = $this->donew_qry->ctk_stiker($nodo,$margin,$alamat);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_stk',$this->data); 
    }

    public function ctk_stkkpb() {
        $nodo = $this->uri->segment(3);  
        $margin = $this->uri->segment(4); 
        $this->data['data'] = $this->donew_qry->ctk_stkkpb($nodo,$margin);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_stkkpb',$this->data); 
    }

    public function ctk_stiker() {
        $nodo = $this->input->post('nodo');
        $margin_up = $this->input->post('margin_up');
        $alamat_ctk = $this->input->post('alamat_ctk');

        $this->load->library('dompdf_lib');

        $this->dompdf_lib->filename = "Cetak_Stiker.pdf";
        $customPaper = array(0,0,360,360);

        $this->data['data'] = $this->donew_qry->ctk_stiker($nodo,$margin_up,$alamat_ctk);
        // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
        //
        // $this->dompdf_lib->load_view('sample_view', $this->sum );
        $this->dompdf_lib->load_view('ctk_stk', $this->data );
    }

    public function ctk_krd() {
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->donew_qry->ctk_kredit($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_krd',$this->data); 
    }

    // public function ctk_kredit() {
    //     $nodo = $this->input->post('nodo');

    //     $this->load->library('dompdf_lib');

    //     $this->dompdf_lib->filename = "Cetak Kredit ".$nodo.".pdf";
    //     $customPaper = array(0,0,360,360);

    //     $this->data['data'] = $this->donew_qry->ctk_kredit($nodo);
    //     // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
    //     //
    //     // $this->dompdf_lib->load_view('sample_view', $this->sum );
    //     $this->dompdf_lib->load_view('ctk_krd', $this->data );
    // }

    public function set_spk() {
        echo $this->donew_qry->set_spk();
    }

    public function set_nodo() {
        echo $this->donew_qry->set_nodo();
    }

    public function setket() {
        echo $this->donew_qry->setket();
    }

    public function carispk() {
        echo $this->donew_qry->carispk();
    }

    public function getNoAssist() {
        echo $this->donew_qry->getNoAssist();
    }

    public function getSPKAssist() {
        echo $this->donew_qry->getSPKAssist();
    }

    public function set_leas() {
        echo $this->donew_qry->set_leas();
    }

    public function full_leas() {
        echo $this->donew_qry->full_leas();
    }

    public function adddo() {
        echo $this->donew_qry->adddo();
    }

    public function set_nosin() {
        echo $this->donew_qry->set_nosin();
    }

    public function updatedo() {
        echo $this->donew_qry->updatedo();
    }

    public function bbn_tarif() {
        echo $this->donew_qry->bbn_tarif();
    }

    public function set_apispk() {
        echo $this->donew_qry->set_apispk();
    }

    public function set_apispk_ubh() {
        echo $this->donew_qry->set_apispk_ubh();
    }

    public function getNoID() {
        echo $this->donew_qry->getNoID();
    }

    public function deletedo() {
        echo $this->donew_qry->deletedo();
    }

    public function getKodeSales() {
        echo $this->donew_qry->getKodeSales();
    }

    public function getNoSPK() {
        echo $this->donew_qry->getNoSPK();
    }

    public function getTipeUnit() {
        echo $this->donew_qry->getTipeUnit();
    }

    public function getKodeSalesHeader() {
        echo $this->donew_qry->getKodeSalesHeader();
    }

    public function getNoDOHso() {
        echo $this->donew_qry->getNoDOHso();
    }

    public function getKdWarna() {
        echo $this->donew_qry->getKdWarna();
    }

    public function getKdKota() {
        echo $this->donew_qry->getkdkota();
    }

    public function getKdKerja() {
        echo $this->donew_qry->getKdKerja();
    }

    public function getKdWarnaHeader() {
        echo $this->donew_qry->getKdWarnaHeader();
    }

    public function gethrg_otr() {
        echo $this->donew_qry->gethrg_otr();
    }

    public function gethrg_otr_edt() {
        echo $this->donew_qry->gethrg_otr_edt();
    }

    public function getKodeTipeHeader() {
        echo $this->donew_qry->getKodeTipeHeader();
    }

    public function getKdAks1() {
        echo $this->donew_qry->getKdAks1();
    }

    public function getKdAks2() {
        echo $this->donew_qry->getKdAks2();
    }

    public function getKdAks3() {
        echo $this->donew_qry->getKdAks3();
    }

    public function getKdAks4() {
        echo $this->donew_qry->getKdAks4();
    }

    public function getKdAks5() {
        echo $this->donew_qry->getKdAks5();
    } 

    public function getAks1() {
        echo $this->donew_qry->getAks1();
    }

    public function getAks2() {
        echo $this->donew_qry->getAks2();
    }

    public function getAks3() {
        echo $this->donew_qry->getAks3();
    }

    public function getAks4() {
        echo $this->donew_qry->getAks4();
    }
    public function set_akso() {
        echo $this->donew_qry->set_akso();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
          //baris 1
         'tglso'=> array(
                  'placeholder' => 'Tanggal SPK',
                  'id'          => 'tglso',
                  'name'        => 'tglso',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control calendar',
                  'style'       => '',
          ),
          'noso'=> array(
                   'placeholder' => 'No. SPK',
                   //'type'        => 'hidden',
                   'id'          => 'noso',
                   'name'        => 'noso',
                   'value'       => set_value('noso'),
                   'class'       => 'form-control',
                   'style'       => 'text-transform: uppercase;',
          ),
         'nospk'=> array(
                   'attr'        => array(
                       'id'    => 'nospk',
                       'class' => 'form-control ',
                   ),
                  'data'     => '',
                  'placeholder' => 'No. Assist',
                  'name'        => 'nospk',
                  'value'       => set_value('nospk'),
          ),
          'tgldo'=> array(
                  'placeholder' => 'Tanggal DO',
                  'id'          => 'tgldo',
                  'name'        => 'tgldo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control calendar',
                  'style'       => '',
          ),
          'nodo'=> array(
                   'placeholder' => 'No. DO',
                   //'type'        => 'hidden',
                   'id'          => 'nodo',
                   'name'        => 'nodo',
                   'value'       => set_value('nodo'),
                   'class'       => 'form-control',
                   'style'       => 'text-transform: uppercase;',
           ),
           'ket'=> array(
                   // 'placeholder' => 'Kec.',
                   'value'       => set_value('ket'),
                   'id'          => 'ket',
                   'name'        => 'ket',
                   'class'       => 'form-control',
                   'style'       => '',
                   'type'        => 'hidden',
           ),
           'ket_bbn'=> array(
                   // 'placeholder' => 'Kec.',
                   'value'       => set_value('ket_bbn'),
                   'id'          => 'ket_bbn',
                   'name'        => 'ket_bbn',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
                   // 'type'        => 'hidden',
           ),
           'ket_lr'=> array(
                   // 'placeholder' => 'Kec.',
                   'value'       => set_value('ket_lr'),
                   'id'          => 'ket_lr',
                   'name'        => 'ket_lr',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
                   // 'type'        => 'hidden',
           ),
          'ket_astra'=> array(
              'attr'        => array(
                  'id'    => 'ket_astra',
                  'class' => 'form-control',
              ),
              'data'     => $this->data['ket_astra'],
              'placeholder' => 'Ket ASTRA',
              'value'       => set_value('ket_astra'),
              'name'        => 'ket_astra',
              'style'       => '',
              // 'type'        => 'hidden', 
          ),

           //baris 2

            'nama1'=> array(
                     'placeholder' => 'Nama',
                     'id'          => 'nama1',
                     'name'        => 'nama1',
                     'value'       => set_value('nama1'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
            ),
            'alamat1'=> array(
                    'placeholder' => 'Alamat',
                    'value'       => set_value('alamat1'),
                    'id'          => 'alamat1',
                    'name'        => 'alamat1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kel1'=> array(
                    'placeholder' => 'Kel.',
                    'value'       => set_value('kel1'),
                    'id'          => 'kel1',
                    'name'        => 'kel1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kec1'=> array(
                    'placeholder' => 'Kec.',
                    'value'       => set_value('kec1'),
                    'id'          => 'kec1',
                    'name'        => 'kec1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdkerja'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdkerja',
                        'class'       => 'form-control',
                    ), 
                    'data'     => '', 
                    'placeholder' => 'Pekerjaan',
                    'value'       => set_value('kdkerja'),
                    'name'        => 'kdkerja',
                    'style'       => 'text-transform: uppercase;',
            ),
            'nohp1'=> array(
                    'placeholder' => 'No. HP',
                    'value'       => set_value('nohp1'),
                    'id'          => 'nohp1',
                    'name'        => 'nohp1',
                    'class'       => 'form-control',
                    'style'       => '',
            ),
            'notelp'=> array(
                    'placeholder' => 'No. Telp',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('notelp'),
                    'id'          => 'notelp',
                    'name'        => 'notelp',
                    'class'       => 'form-control',
                    'style'       => '',
            ),
            'kota1'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kota1',
                        'class'       => 'form-control',
                    ), 
                    'data'     => '', 
                    'placeholder' => 'Kota',
                    'value'       => set_value('kota1'),
                    'name'        => 'kota1',
                    'style'       => 'text-transform: uppercase;',
            ),
            'npwp'=> array(
                    'placeholder' => 'Npwp',
                    'value'       => set_value('npwp'),
                    'id'          => 'npwp',
                    'name'        => 'npwp',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'nama2'=> array(
                     'placeholder' => 'Nama',
                     'id'          => 'nama2',
                     'name'        => 'nama2',
                     'value'       => set_value('nama2'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
            ),
            'alamat2'=> array(
                    'placeholder' => 'Alamat KTP/KIMS',
                    'value'       => set_value('alamat2'),
                    'id'          => 'alamat2',
                    'name'        => 'alamat2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'alamattt'=> array(
                    'placeholder' => 'Alamat T. Tinggal',
                    'value'       => set_value('alamattt'),
                    'id'          => 'alamattt',
                    'name'        => 'alamattt',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kel2'=> array(
                    'placeholder' => 'Kel.',
                    'value'       => set_value('kel2'),
                    'id'          => 'kel2',
                    'name'        => 'kel2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kec2'=> array(
                    'placeholder' => 'Kec.',
                    'value'       => set_value('kec2'),
                    'id'          => 'kec2',
                    'name'        => 'kec2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'noktp'=> array(
                    'placeholder' => 'No KTP',
                    'value'       => set_value('noktp'),
                    'id'          => 'noktp',
                    'name'        => 'noktp',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
            ),
            'nohp2'=> array(
                    'placeholder' => 'No. HP',
                    'value'       => set_value('nohp2'),
                    'id'          => 'nohp2',
                    'name'        => 'nohp2',
                    'class'       => 'form-control',
                    'style'       => '',
            ),
            'kota2'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kota2',
                        'class'       => 'form-control',
                    ), 
                    'data'     => '', 
                    'placeholder' => 'Kota',
                    'value'       => set_value('kota2'),
                    'name'        => 'kota2',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),

            //baris 3
            'nosin'=> array(
                    'placeholder' => 'No Mesin',
                    'value'       => set_value('nosin'),
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'norangka'=> array(
                    'placeholder' => 'No Rangka',
                    'value'       => set_value('norangka'),
                    'id'          => 'norangka',
                    'name'        => 'norangka',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
            'stat_otr'=> array(
                    'attr'        => array(
                        'id'    => 'stat_otr',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['stat_otr'],
                    'placeholder' => 'Status OTR',
                    'value'       => set_value('stat_otr'),
                    'name'        => 'stat_otr',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'hrg_otr'=> array(
                    'placeholder' => 'Harga OTR',
                    'value'       => set_value('hrg_otr'),
                    'id'          => 'hrg_otr',
                    'name'        => 'hrg_otr',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'kdtipe'=> array(
                    'attr'        => array(
                        'id'    => 'kdtipe',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'placeholder' => 'Tipe Unit',
                    'value'       => set_value('kdtipe'),
                    'name'        => 'kdtipe',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'kdwarna'=> array(
                    'attr'        => array(
                        'id'    => 'kdwarna',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'placeholder' => 'Kode Warna',
                    'value'       => set_value('kdwarna'),
                    'name'        => 'kdwarna',
                    'style'       => '',
            ),
            'nmwarna'=> array(
                    'placeholder' => 'Nama Warna',
                    // 'type'        => 'hidden',
                    'value'       => set_value('nmwarna'),
                    'id'          => 'nmwarna',
                    'name'        => 'nmwarna',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    // 'type'        => 'hidden',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),

            // baris 4
            'kdleasing'=> array(
                    'placeholder' => 'Pembayaran',
                    'attr'        => array(
                        'id'    => 'kdleasing',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdleasing'],
                    'class' => 'form-control',
                    'value'    => set_value('kdleasing'),
                    'name'     => 'kdleasing',
                    // 'readonly' => '',
            ),
            'diskon'=> array(
                    'placeholder' => 'Discount (-)',
                    // 'type'        => 'hidden',
                    'value'       => set_value('diskon'),
                    'id'          => 'diskon',
                    'name'        => 'diskon',
                    'class'       => 'form-control',
                    // 'readonly'    => '',
            ),
            'diskon1'=> array(
                    'placeholder' => 'Discount (-)',
                    // 'type'        => 'hidden',
                    'value'       => set_value('diskon1'),
                    'id'          => 'diskon1',
                    'name'        => 'diskon1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'gettotal();'
                    // 'readonly'    => '',
            ),
            'progleas'=> array(
                    'placeholder' => 'Program',
                    'attr'        => array(
                        'id'    => 'progleas',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('progleas'),
                    'name'     => 'progleas',
                    'required'    => '',
            ),
            'um'=> array(
                    'placeholder' => 'Uang Muka',
                    // 'type'        => 'hidden',
                    'value'       => set_value('um'),
                    'id'          => 'um',
                    'name'        => 'um',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'gettotal();'
                    // 'readonly'    => '',
            ),
            'angsuran'=> array(
                    'placeholder' => 'Angsuran',
                    // 'type'        => 'hidden',
                    'value'       => set_value('angsuran'),
                    'id'          => 'angsuran',
                    'name'        => 'angsuran',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'tenor'=> array(
                    'placeholder' => 'Tenor',
                    // 'type'        => 'hidden',
                    'value'       => set_value('tenor'),
                    'id'          => 'tenor',
                    'name'        => 'tenor',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'sub_leas'=> array(
                    'placeholder' => 'Subsidi Leasing',
                    // 'type'        => 'hidden',
                    'value'       => set_value('sub_leas'),
                    'id'          => 'sub_leas',
                    'name'        => 'sub_leas',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'getsubleas();'
                    // 'readonly'    => '',
            ),
            'insentif'=> array(
                    'placeholder' => 'Program',
                    'attr'        => array(
                        'id'    => 'insentif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdsales_ins'],
                    'value'    => set_value('insentif'),
                    'name'     => 'insentif',
                    'required'    => '',
            ),
            'total'=> array(
                    'placeholder' => 'Total',
                    // 'type'        => 'hidden',
                    'value'       => set_value('total'),
                    'id'          => 'total',
                    'name'        => 'total',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'gettotal();',
                    'readonly'    => '',
            ),
            'promo'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'type'        => 'hidden',
                    'attr'        => array(
                        'id'          => 'promo',
                        'class'       => 'form-control',
                    ), 
                    'data'        => $this->data['promo'],
                    'placeholder' => 'Gratis Oli',
                    'value'       => set_value('promo'),
                    'name'        => 'promo',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),

            //baru aksesoris 
            'kdaks1'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdaks1',
                        'class'       => 'form-control',
                    ), 
                    'data'        => '',
                    'placeholder' => 'Aksesoris',
                    'value'       => set_value('kdaks1'),
                    'name'        => 'kdaks1',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'qtyaks1'=> array(
                    'placeholder' => 'Qty',
                    // 'type'        => 'hidden',
                    'value'       => set_value('qtyaks1'),
                    'id'          => 'qtyaks1',
                    'name'        => 'qtyaks1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
            ),
            'kdaks2'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdaks2',
                        'class'       => 'form-control',
                    ), 
                    'data'        => '',
                    'placeholder' => 'Aksesoris',
                    'value'       => set_value('kdaks2'),
                    'name'        => 'kdaks2',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'qtyaks2'=> array(
                    'placeholder' => 'Qty',
                    // 'type'        => 'hidden',
                    'value'       => set_value('qtyaks2'),
                    'id'          => 'qtyaks2',
                    'name'        => 'qtyaks2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
            ),
            'kdaks3'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdaks3',
                        'class'       => 'form-control',
                    ), 
                    'data'        => '',
                    'placeholder' => 'Aksesoris',
                    'value'       => set_value('kdaks3'),
                    'name'        => 'kdaks3',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'qtyaks3'=> array(
                    'placeholder' => 'Qty',
                    // 'type'        => 'hidden',
                    'value'       => set_value('qtyaks3'),
                    'id'          => 'qtyaks3',
                    'name'        => 'qtyaks3',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
            ),
            'kdaks4'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdaks4',
                        'class'       => 'form-control',
                    ), 
                    'data'        => '',
                    'placeholder' => 'Aksesoris',
                    'value'       => set_value('kdaks4'),
                    'name'        => 'kdaks4',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'qtyaks4'=> array(
                    'placeholder' => 'Qty',
                    // 'type'        => 'hidden',
                    'value'       => set_value('qtyaks4'),
                    'id'          => 'qtyaks4',
                    'name'        => 'qtyaks4',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
            ),
            'kdaks5'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdaks5',
                        'class'       => 'form-control',
                    ), 
                    'data'        => '',
                    'placeholder' => 'Aksesoris',
                    'value'       => set_value('kdaks5'),
                    'name'        => 'kdaks5',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'qtyaks5'=> array(
                    'placeholder' => 'Qty',
                    // 'type'        => 'hidden',
                    'value'       => set_value('qtyaks5'),
                    'id'          => 'qtyaks5',
                    'name'        => 'qtyaks5',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
            ),
            'tot_aks'=> array(
                    'placeholder' => 'Total Aksesoris',
                    // 'type'        => 'hidden',
                    'value'       => set_value('tot_aks'),
                    'id'          => 'tot_aks',
                    'name'        => 'tot_aks',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => '',
                    'readonly'    => '',
            ),

            // baris 5
            'kdsales'=> array(
                    'attr'        => array(
                        'id'    => 'kdsales',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdsales'),
                    'name'     => 'kdsales',
                    'required'    => '',
            ),
            'nmsales'=> array(
                    'placeholder' => 'Salesman',
                    'type'        => 'hidden',
                    'value'       => set_value('nmsales'),
                    'id'          => 'nmsales',
                    'name'        => 'nmsales',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'nmspv'=> array(
                    'placeholder' => 'Superviser',
                    // 'type'        => 'hidden',
                    'value'       => set_value('nmspv'),
                    'id'          => 'nmspv',
                    'name'        => 'nmspv',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'nmpos'=> array(
                    // 'placeholder' => 'Superviser',
                    // 'type'        => 'hidden',
                    'value'       => set_value('nmpos'),
                    'id'          => 'nmpos',
                    'name'        => 'nmpos',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
      		  'kirim_tagih'=> array(
                    'placeholder' => 'Kirim Tagih',
            				'id'          => 'kirim_tagih',
            			  // 'value'       => 't',
            				'checked'     => $faktif,
            				'class'       => 'custom-control-input',
            				'name'		  => 'kirim_tagih',
            				'type'		  => 'checkbox',
      			),
      		  'nilai_kt'=> array(
                    'placeholder' => 'Nominal Kirim Tagih',
            				'id'          => 'nilai_kt',
                    'value'       => set_value('nilai_kt'),
                    'name'        => 'nilai_kt',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
      			),
      		  'keterangan'=> array(
                    'placeholder' => 'Keterangan',
            				'id'          => 'keterangan',
                    'value'       => set_value('keterangan'),
                    'name'        => 'keterangan',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
      			),
      		  'margin_up'=> array(
                    'placeholder' => 'Margin Atas',
            				'id'          => 'margin_up',
                    'value'       => set_value('margin_up'),
                    'name'        => 'margin_up',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
      			),
      		  'alamat_stnk'=> array(
                    'placeholder' => 'Alamat STNK',
            				'id'          => 'alamat_stnk',
                    'value'       => set_value('alamat_stnk'),
                    'name'        => 'alamat_stnk',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
      			),
            'alamat_ctk'=> array(
                    'placeholder' => 'Alamat Cetak',
                    'id'          => 'alamat_ctk',
                    'value'       => set_value('alamat_ctk'),
                    'name'        => 'alamat_ctk',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'nodo_ctk'=> array(
                    'placeholder' => 'No DO Cetak',
                    'id'          => 'nodo_ctk',
                    'value'       => set_value('nodo_ctk'),
                    'name'        => 'nodo_ctk',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'type'        => 'hidden',
                    // 'readonly'    => '',
            ),
            'margin_up2'=> array(
                    'placeholder' => 'Margin Atas',
                    'id'          => 'margin_up2',
                    'value'       => set_value('margin_up2'),
                    'name'        => 'margin_up2',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'kdtipe_ctk'=> array(
                    'placeholder' => 'Alamat Cetak',
                    'id'          => 'kdtipe_ctk',
                    'value'       => set_value('kdtipe_ctk'),
                    'name'        => 'kdtipe_ctk',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'nodo_ctk2'=> array(
                    'placeholder' => 'No DO Cetak',
                    'id'          => 'nodo_ctk2',
                    'value'       => set_value('nodo_ctk2'),
                    'name'        => 'nodo_ctk2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'type'        => 'hidden',
                    // 'readonly'    => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(
          // 'nodf'=> array(
          //          'placeholder' => 'No. DF',
          //          //'type'        => 'hidden',
          //          'id'          => 'nodf',
          //          'name'        => 'nodf',
          //          'value'       => $this->val[0]['nodf'],
          //          'class'       => 'form-control',
          //          'style'       => '',
          //          'readonly'    => '',
          //  ),

              'noso'=> array(
                       'placeholder' => 'No. SPK',
                       //'type'        => 'hidden',
                       'id'          => 'noso',
                       'name'        => 'noso',
                       'value'       => set_value('noso'),
                       'class'       => 'form-control',
                       'style'       => '',
               ),
              'tglso'=> array(
                       'placeholder' => 'Tanggal SPK',
                       'id'          => 'tglso',
                       'name'        => 'tglso',
                       'value'       => date('d-m-Y'),
                       'class'       => 'form-control calendar',
                       'style'       => '',
               ),
              'nospk'=> array(
                        'attr'        => array(
                            'id'    => 'nospk',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                       'placeholder' => 'No. Assist',
                       'name'        => 'nospk',
                       'value'       => set_value('nospk'),
               ),
               'nama1'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama1',
                        'name'        => 'nama1',
                        'value'       => set_value('nama1'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat1'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat1'),
                       'id'          => 'alamat1',
                       'name'        => 'alamat1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel1'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel1'),
                       'id'          => 'kel1',
                       'name'        => 'kel1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'noktp'=> array(
                       'placeholder' => 'No KTP',
                       'value'       => set_value('noktp'),
                       'id'          => 'noktp',
                       'name'        => 'noktp',
                       'class'       => 'form-control',
                       // 'type'        => 'hidden',
                       'style'       => '',
               ),
               'kec1'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec1'),
                       'id'          => 'kec1',
                       'name'        => 'kec1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'hohp1'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp1'),
                       'id'          => 'hohp1',
                       'name'        => 'hohp1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'notelp'=> array(
                       'placeholder' => 'No. Telp',
                       'type'        =>  'hidden',
                       'value'       => set_value('nsel'),
                       'id'          => 'nsel',
                       'name'        => 'nsel',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota1'=> array(
                      // 'id'          => 'kota1',
                      // 'class'       => 'form-control',
                      'attr'        => array(
                          'id'          => 'kota1',
                          'class'       => 'form-control',
                      ), 
                      'data'     => '', 
                      'placeholder' => 'Kota',
                      'value'       => set_value('kota1'),
                      'name'        => 'kota1',
                      'style'       => 'text-transform: uppercase;', 
                       // 'readonly'    => '',
               ),
               'npwp'=> array(
                       'placeholder' => 'Npwp',
                       'value'       => set_value('npwp'),
                       'id'          => 'npwp',
                       'name'        => 'npwp',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'nama2'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama2',
                        'name'        => 'nama2',
                        'value'       => set_value('nama2'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat2'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat2'),
                       'id'          => 'alamat2',
                       'name'        => 'alamat2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel2'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel2'),
                       'id'          => 'kel2',
                       'name'        => 'kel2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kec2'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec2'),
                       'id'          => 'kec2',
                       'name'        => 'kec2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'ket'=> array(
                       // 'placeholder' => 'Kec.',
                       'value'       => set_value('ket'),
                       'id'          => 'ket',
                       'name'        => 'ket',
                       'class'       => 'form-control',
                       'style'       => '',
                       'type'        => 'hidden',
               ),
               'hohp2'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp2'),
                       'id'          => 'hohp2',
                       'name'        => 'hohp2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota2'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kota2',
                        'class'       => 'form-control',
                    ), 
                    'data'     => '', 
                    'placeholder' => 'Kota',
                    'value'       => set_value('kota2'),
                    'name'        => 'kota2',
                    'style'       => 'text-transform: uppercase;',
               ),
               'kdtipe'=> array(
                       'attr'        => array(
                           'id'    => 'kdtipe',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Tipe Unit',
                       'value'       => set_value('kdtipe'),
                       'name'        => 'kdtipe',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'nmtipe'=> array(
                       'placeholder' => 'Nama Tipe',
                       'type'        => 'nmtipe',
                       'value'       => set_value('nmtipe'),
                       'id'          => 'nmtipe',
                       'name'        => 'nmtipe',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'kdwarna'=> array(
                       'attr'        => array(
                           'id'    => 'kdwarna',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Kode Warna',
                       'value'       => set_value('kdwarna'),
                       'name'        => 'kdwarna',
                       'style'       => '',
               ),
               'nmwarna'=> array(
                       'placeholder' => 'Nama Warna',
                       // 'type'        => 'hidden',
                       'value'       => set_value('nmwarna'),
                       'id'          => 'nmwarna',
                       'name'        => 'nmwarna',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'kdleasing'=> array(
                       'placeholder' => 'Pembayaran',
                       'attr'        => array(
                           'id'    => 'kdleasing',
                           'class' => 'form-control select',
                       ),
                       'data'     => $this->data['kdleasing'],
                       'value'    => set_value('kdleasing'),
                       'name'     => 'kdleasing',
                       'required'    => '',
               ),
               'kdsales'=> array(
                       'placeholder' => 'Salesman',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdsales'),
                       'id'          => 'kdsales',
                       'name'        => 'kdsales',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdspv'=> array(
                       'placeholder' => 'Superviser',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdspv'),
                       'id'          => 'kdspv',
                       'name'        => 'kdspv',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->donew_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptunitentrymon_qry
 *
 * @author adi
 */
class Rptunitentrymon_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['tahun']) ){
			if($_GET['tahun']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$tahun = $_GET['tahun'];//$tgl1[1].$tgl1[0];
			}else{
				$tahun = '';
			} 
		}else{
			$tahun = '';
		}        
		

//echo "<script> console.log('PHP: ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";

//		$aColumns = array('dt','pk','no_ktp','nm','adr','subtotsrv','subtotitem','subtot','distrs','tottrs');

							//, 'nmtipe'
		$aColumns = array('periode'
							, 'qty_hari'

							, 'srv_qty'
							, 'srv_dpp_tot'
							, 'srv_dpp_jasa'
							, 'srv_dpp_oli'
							, 'srv_dpp_part'

							, 'reg_qty'
							, 'reg_dpp_tot'
							, 'reg_dpp_jasa'
							, 'reg_dpp_oli'
							, 'reg_dpp_part'
							
							, 'kpb_qty'
							, 'kpb_dpp_tot'
							, 'kpb_dpp_jasa'
							, 'kpb_dpp_oli'
							, 'kpb_dpp_part'
		);


/*
		$aColumns = array('pk'
							, 'dt'
							, 'kpbto'
							, 'distrs'
		);
*/

		$sIndexColumn = "periode";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}




		$sTable = "(


SELECT bb.bl AS periode
		, count(bb.dtx) AS qty_hari
		
		, sum(bb.srv_qty) AS srv_qty
		, sum(bb.srv_dpp_tot) AS srv_dpp_tot
		, sum(bb.srv_dpp_jasa) AS srv_dpp_jasa
		, sum(bb.srv_dpp_oli) AS srv_dpp_oli
		, sum(bb.srv_dpp_part) AS srv_dpp_part

		, sum(bb.reg_qty) AS reg_qty
		, sum(bb.reg_dpp_tot) AS reg_dpp_tot
		, sum(bb.reg_dpp_jasa) AS reg_dpp_jasa
		, sum(bb.reg_dpp_oli) AS reg_dpp_oli
		, sum(bb.reg_dpp_part) AS reg_dpp_part

		, sum(bb.kpb_qty) AS kpb_qty
		, sum(bb.kpb_dpp_tot) AS kpb_dpp_tot
		, sum(bb.kpb_dpp_jasa) AS kpb_dpp_jasa
		, sum(bb.kpb_dpp_oli) AS kpb_dpp_oli
		, sum(bb.kpb_dpp_part) AS kpb_dpp_part


FROM
(
SELECT aa.bl
		, aa.blx
		, aa.dtx

		
		, sum(aa.qty_reg + aa.qty_kpb) AS srv_qty
		, sum(aa.reg_jasa + aa.reg_oli + aa.reg_part + aa.kpb_jasa + aa.kpb_oli + aa.kpb_part) AS srv_dpp_tot
		, sum(aa.reg_jasa + aa.kpb_jasa) AS srv_dpp_jasa
		, sum(aa.reg_oli + aa.kpb_oli) AS srv_dpp_oli
		, sum(aa.reg_part + aa.kpb_part) AS srv_dpp_part

		, sum(aa.qty_reg) AS reg_qty
		, sum(aa.reg_jasa + aa.reg_oli + aa.reg_part) AS reg_dpp_tot
		, sum(aa.reg_jasa) AS reg_dpp_jasa
		, sum(aa.reg_oli) AS reg_dpp_oli
		, sum(aa.reg_part) AS reg_dpp_part

		, sum(aa.qty_kpb) AS kpb_qty
		, sum(aa.kpb_jasa + aa.kpb_oli + aa.kpb_part) AS kpb_dpp_tot
		, sum(aa.kpb_jasa) AS kpb_dpp_jasa
		, sum(aa.kpb_oli) AS kpb_dpp_oli
		, sum(aa.kpb_part) AS kpb_dpp_part

FROM
(

SELECT zz.blx
		, zz.bl
		, dtx
		
		, IIF(zz.tp='REG',1,0) AS qty_reg
		, IIF(zz.tp='KPB',1,0) AS qty_kpb
		
		, sum(zz.reg_jasa) AS reg_jasa
		, sum(zz.reg_oli) AS reg_oli
		, sum(zz.reg_part) AS reg_part

		, sum(zz.kpb_jasa) AS kpb_jasa
		, sum(zz.kpb_oli) AS kpb_oli
		, sum(zz.kpb_part) AS kpb_part

FROM
(

SELECT	yy.pk
		, yy.dtx
		, yy.blx
		, yy.bl
		, yy.tp

		, IIF(yy.tp='REG' AND yy.grp='JASA',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS reg_jasa
		, IIF(yy.tp='REG' AND yy.grp='OLI',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS reg_oli
		, IIF(yy.tp='REG' AND yy.grp='PART',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS reg_part
		
		, IIF(yy.tp='KPB' AND yy.grp='JASA',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS kpb_jasa
		, IIF(yy.tp='KPB' AND yy.grp='OLI',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS kpb_oli
		, IIF(yy.tp='KPB' AND yy.grp='PART',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS kpb_part
		
FROM
(


						SELECT a.pk
						, format(dt,'ddmm') AS dtx
						, format(dt,'mm') AS blx
	 ,iif(format(dt,'mm')='01','JANUARI',
	 iif(format(dt,'mm')='02','FEBRUARI',
	 iif(format(dt,'mm')='03','MARET',
	 iif(format(dt,'mm')='04','APRIL',
	 iif(format(dt,'mm')='05','MEI',
	 iif(format(dt,'mm')='06','JUNI',
	 iif(format(dt,'mm')='07','JULI',
	 iif(format(dt,'mm')='08','AGUSTUS',
	 iif(format(dt,'mm')='09','SEPTEMBER',
	 iif(format(dt,'mm')='10','OKTOBER',
	 iif(format(dt,'mm')='11','NOVEMBER',
	 iif(format(dt,'mm')='12','DESEMBER','')))))))))))) AS bl

							, IIF(val(a.kpbto) > 0, 'KPB', 'REG') AS tp
							, IIF(xd.nt = '1' AND val(a.kpbto) > 0, 'YA', '') AS isklaim
							
							, 'JASA' as grp

							, IIF(xd.nt = '1' AND val(a.kpbto) = 1, x.qty * c.kpb1 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
								IIF(xd.nt = '1' AND val(a.kpbto) = 2, x.qty * c.kpb2 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
									IIF(xd.nt = '1' AND val(a.kpbto) = 3, x.qty * c.kpb3 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
										IIF(xd.nt = '1' AND val(a.kpbto) = 4, x.qty * c.kpb4 * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
											round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0)
										)
									)
								)
							) AS dpp

							
							
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_srv x ON a.pk = x.fk)
							INNER JOIN m_srv xd ON x.srvfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON trim(b.cmn1) = trim(c.kdsin)
						WHERE format(a.dt,'yyyy')='{$tahun}'

						UNION ALL

						SELECT a.pk
						, format(dt,'ddmm') AS dtx
						, format(dt,'mm') AS blx 
	 ,iif(format(dt,'mm')='01','JANUARI',
	 iif(format(dt,'mm')='02','FEBRUARI',
	 iif(format(dt,'mm')='03','MARET',
	 iif(format(dt,'mm')='04','APRIL',
	 iif(format(dt,'mm')='05','MEI',
	 iif(format(dt,'mm')='06','JUNI',
	 iif(format(dt,'mm')='07','JULI',
	 iif(format(dt,'mm')='08','AGUSTUS',
	 iif(format(dt,'mm')='09','SEPTEMBER',
	 iif(format(dt,'mm')='10','OKTOBER',
	 iif(format(dt,'mm')='11','NOVEMBER',
	 iif(format(dt,'mm')='12','DESEMBER','')))))))))))) AS bl

							, IIF(val(a.kpbto) > 0, 'KPB', 'REG') AS tp
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 'YA', '') AS isklaim

							, IIF(xd.grpfk=2, 'OLI', 'PART') AS grp
								
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 
								x.qty * c.oli * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
								round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0)
								) AS dpp
													
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_item x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON b.cmn1 = c.kdsin
						WHERE format(a.dt,'yyyy')='{$tahun}'

) AS yy

) AS zz
GROUP BY zz.bl, zz.blx, zz.tp, zz.pk, zz.dtx

) AS aa
GROUP BY aa.bl, aa.blx, aa.dtx

) AS bb
GROUP BY bb.bl, bb.blx
ORDER BY bb.blx

					) AS A ";


		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
//        echo $sQuery;
		
		$rResult = $dbbengkel->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $dbbengkel->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $dbbengkel->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}
	
	public function submit(){

	}
}

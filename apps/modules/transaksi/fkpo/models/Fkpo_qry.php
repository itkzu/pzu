<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Fkpo_qry
 *
 * @author adi
 */
class Fkpo_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataSupplier() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.v_supplier");
        return $q->result_array();
    }

    public function set_nopo() {
        $nopo = $this->input->post('nopo');  
        $query = $this->db->query("select * from pzu.vm_po where nopo =  '".$nopo."'"); 
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    }

    public function set_ket() {
        $nopo = $this->input->post('nopo');  
        $query = $this->db->query("select c.fposting::int, sum(b.progres)::int as ket_lr FROM pzu.t_po_d a LEFT JOIN pzu.t_po c ON a.nopo=c.nopo LEFT JOIN pzu.t_do b ON a.nosin=b.nosin WHERE a.nopo = '".$nopo."' group by c.fposting  "); 
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    }

    public function getKodefkpo() {
        $this->db->select("*");
        $this->db->order_by('nmaks');
        $query = $this->db->get('pzu.v_aks');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $no = str_replace("-", "", $no);
        $nopo = str_replace("-", "", $no);
        $this->db->select('*');
        $this->db->where('nopo',$nopo);
        $q = $this->db->get("pzu.vm_po");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    private function getdetail(){
        $output = array();
        $str = "SELECT  nopo,
                        kode,
                        kdtipe,
                        nmtipe,
                        warna,
                        qty,
                        harga
                         FROM pzu.vm_po_d";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
	    }
        return $output;
    } 

    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('nopox', 'tglpox', 'nmsup', 'nopo');
    	$sIndexColumn = "nopo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT nopox
                              , to_char(tglpo, 'DD-MM-YYYY') as tglpox
                              , nmsup
                              , nopo 
                              FROM pzu.vm_po where progres = 0 and fposting = 0) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by nopo ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['nopo']==$value['nopo']){
                    $aRow['detail'][]= $value;
                }
            }

            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('fkpo/edit/'.$aRow['nopo'])."\">Edit</a>"; 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['nopo']) ){
            if($_GET['nopo']){
                $nopo = $_GET['nopo'];
            }else{
                $nopo = '';
            }
        }else{
            $nopo = '';
        }

        $aColumns = array('no',
                          'kode',
                          'kdtipe',
                          'nmtipe',
                          'warna',
                          'kdwarna',
                          'qty',
                          'harga',
                          'totharga',
                          'nopo', );
	    $sIndexColumn = "nopo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select ''::varchar as no, nopo, qty, kode, kdtipe, nmtipe, kdwarna, warna, harga, harga * qty as totharga from pzu.vm_po_d where nopo = '".$nopo."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }/*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nourut']==$value['nourut']){
                    $aRow['detail'][]= $value;
                }
            }*/
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nopo']."','".$aRow['kode']."','".$aRow['kdtipe']."','".$aRow['nmtipe']."','".$aRow['qty']."',".$aRow['harga'].",'".$aRow['kdwarna']."');\">Edit</button>"; 

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }

    public function getdetPO() {
        error_reporting(-1);
        if( isset($_GET['nopo']) ){
            if($_GET['nopo']){
                $nopo = $_GET['nopo'];
            }else{
                $nopo = '';
            }
        }else{
            $nopo = '';
        }

        $aColumns = array('no',
                          'qty',
                          'kode',
                          'kdtipe',
                          'nmtipe',
                          'warna',
                          'harga',
                          'totharga',
                          'nopo');
      $sIndexColumn = "nopo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select ''::varchar as no, nopo, qty, kode, kdtipe, nmtipe, warna, harga , harga * qty as totharga from pzu.vm_po_d where nopo = '".$nopo."' order by nopo ) AS a";


        //$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
        $sWhere = "";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
          if($_GET['iDisplayStart']>0){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                intval( $_GET['iDisplayStart'] );
          }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
          $sOrder = " ORDER BY  ";
          for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
          {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
          }

          $sOrder = substr_replace( $sOrder, "", -2 );
          if ( $sOrder == " ORDER BY" )
          {
              $sOrder = "";
          }
        }

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " AND ";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
            if ( $sWhere == "" )
            {
              $sWhere = " WHERE ";
            }
            else
            {
              $sWhere .= " AND ";
            }
            //echo $sWhere."<br>";
            $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
        }


        /*
         * SQL queries
         */
        $sQuery = "
            SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";

            //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";   //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo json_encode( $output );
    }

    public function addDetail() {

        try {   
            $nopo = $this->input->post('nopo');  
            $kode = $this->input->post('kode'); 
            $kdwarna = $this->input->post('kdwarna'); 
            $harga = $this->input->post('harga'); 

            $q = $this->db->query("update pzu.t_po_d set harga = ".$harga." where kode = '".$kode."' and nopo = '".$nopo."' and kdwarna = '".$kdwarna."'");
            if (!$q) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "Data Terupdate";
                $this->state = "1";
            } 
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr); 
    } 

    public function batal() {

        try {   
            $nopo = $this->input->post('nopo');    

            $q = $this->db->query("update pzu.t_po_d set harga = 0 where nopo = '".$nopo."'");
            if (!$q) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "Data Terupdate";
                $this->state = "1";
            } 
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr); 
    }  

    public function submit() { 
        $nopo = $this->input->post('nopo'); 
        $sub_tot = $this->input->post('sub_tot');
        $potongan = $this->input->post('potongan');
        $disc_top = $this->input->post('disc_top'); 
        $q = $this->db->query("select title,msg,tipe from pzu.fk_po_ins( '". $nopo ."',
                                                                        ". $sub_tot .", 
                                                                        ". $potongan .",
                                                                        ". $disc_top .",
                                                                        0, 
                                                                        '".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

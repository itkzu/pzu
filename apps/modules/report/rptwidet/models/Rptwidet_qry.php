<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptwidet_qry
 *
 * @author adi
 */
class Rptwidet_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
			}else{
				$periode_awal = '';
			} 
		}else{
			$periode_awal = '';
		}        
		
		if( isset($_GET['periode_akhir']) ){
			if($_GET['periode_akhir']){
				//$tgl2 = explode('-', $_GET['periode_akhir']);
				$periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl2[1].$tgl2[0];
			}else{
				$periode_akhir = '';
			} 
		}else{
			$periode_akhir = '';
		} 

//echo "<script> console.log('PHP: qry ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";

//		$aColumns = array('dt','pk','no_ktp','nm','adr','subtotsrv','subtotitem','subtot','distrs','tottrs');

							//, 'nmtipe'
		$aColumns = array('pk'
							, 'dt'
							, 'nm'
							, 'pkc'
							, 'hp'
							, 'tgl_beli'
							, 'kmnow'
							, 'cmn1'
							, 'mn1'
							, 'nmtipe'
							, 'kpbto'
							, 'isklaim'
							, 'grp'
							, 'kdbrg'
							, 'nmbrg'
							, 'qty'
							, 'sprice'
							, 'dis'
							, 'distrs'
							, 'subtotal'
							, 'dpp'
							, 'ppn'
		);


/*
		$aColumns = array('pk'
							, 'dt'
							, 'kpbto'
							, 'distrs'
		);
*/

		$sIndexColumn = "pk";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}


		$sTable = "(

					SELECT xx.*
						, IIF(xx.isklaim = '', xx.subtotal - round(xx.subtotal/11,0) , xx.subtotal) as dpp
						, IIF(xx.isklaim = '', round(xx.subtotal/11,0) , round(xx.subtotal * 10/100,0)) as ppn
					FROM
					(
						SELECT a.pk
							, Format(a.dt, 'dd/mm/yyyy') as dt
							, a.dt as dt2
							, b.nm
							, b.pkc

							, b.hp
							, Format(b.tgl_beli, 'dd/mm/yyyy') as tgl_beli
							, b.cmn1
							, b.mn1
							, c.nmtipe
							, a.kmnow

							, xd.nt as nt

							, a.kpbto
							, 0 AS grpfk
							, 'JASA' as grp
								
							, IIF(xd.nt = '1' AND val(a.kpbto) > 0, 'YA', '') AS isklaim


							, xd.pkc as kdbrg 
							, xd.nm as nmbrg  
							, x.qty

							, IIF(xd.nt = '1' AND val(a.kpbto) = 1, c.kpb1,
								IIF(xd.nt = '1' AND val(a.kpbto) = 2, c.kpb2,
									IIF(xd.nt = '1' AND val(a.kpbto) = 3, c.kpb3,
										IIF(xd.nt = '1' AND val(a.kpbto) = 4, c.kpb4, x.sprice
										)
									)
								)
							) as sprice

							, x.dis
							, a.distrs

							, IIF(xd.nt = '1' AND val(a.kpbto) = 1, x.qty * c.kpb1 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
								IIF(xd.nt = '1' AND val(a.kpbto) = 2, x.qty * c.kpb2 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
									IIF(xd.nt = '1' AND val(a.kpbto) = 3, x.qty * c.kpb3 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
										IIF(xd.nt = '1' AND val(a.kpbto) = 4, x.qty * c.kpb4 * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
											x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100
										)
									)
								)
							) AS subtotal
							
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_srv x ON a.pk = x.fk)
							INNER JOIN m_srv xd ON x.srvfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON trim(b.cmn1) = trim(c.kdsin)


						UNION ALL

						SELECT a.pk
							, Format(a.dt, 'dd/mm/yyyy') as dt
							, a.dt as dt2
							, b.nm
							, b.pkc

							, b.hp
							, Format(b.tgl_beli, 'dd/mm/yyyy') as tgl_beli
							, b.cmn1
							, b.mn1
							, c.nmtipe
							, a.kmnow

							, '' as nt

							, a.kpbto
							, xd.grpfk
							, IIF(xd.grpfk=2, 'OLI', 'PART') AS grp
									
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 'YA', '') AS isklaim


							, xd.pkc as kdbrg
							, xd.nm as nmbrg
							, x.qty

							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, c.oli, x.sprice) AS sprice

							, x.dis
							, a.distrs
							
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 
								x.qty * c.oli * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
								x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100
								) AS subtotal
													
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_item x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON b.cmn1 = c.kdsin

						UNION ALL

						SELECT a.pk
							, Format(a.dt, 'dd/mm/yyyy') as dt
							, a.dt as dt2
							, b.nm
							, '' as pkc

							, '' as hp
							, '' as tgl_beli
							, '' as cmn1
							, '' as mn1
							, '' as nmtipe
							, '' as kmnow

							, '' as nt

							, '' as kpbto
							, xd.grpfk
							, IIF(xd.grpfk=2, 'OLI', 'PART') AS grp
									
							, '' AS isklaim


							, xd.pkc as kdbrg
							, xd.nm as nmbrg
							, x.qty

							, x.sprice AS sprice

							, x.dis
							, a.distrs
							
							, x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100 AS subtotal
							
						FROM 	
							((t_si_hd a INNER JOIN m_cus b ON a.cusfk = b.pk)
							INNER JOIN t_si_dt x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk


					) AS xx	
					WHERE Format(xx.dt2, 'yyyy-mm-dd') BETWEEN '{$periode_awal}' AND '{$periode_akhir}'
					ORDER BY xx.dt2, xx.pk, xx.grpfk

										) AS A ";


		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		$sTable = "( SELECT Format(t_wi_hd.dt, 'yyyy-mm-dd') AS dt
						, t_wi_hd.pk, m_vc.no_ktp, m_vc.nm
												
						, m_vc.adr
						, t_wi_hd.subtotsrv, t_wi_hd.subtotitem
						, t_wi_hd.subtot, Format((t_wi_hd.distrs/100),'Standard') *  t_wi_hd.subtot AS distrs, t_wi_hd.tottrs
						FROM t_wi_hd INNER JOIN m_vc ON t_wi_hd.vcfk = m_vc.pk 
						WHERE Format(t_wi_hd.dt, 'yyyy-mm-dd') BETWEEN 
							 '{$periode_awal}' AND  '{$periode_akhir}'
					) AS A ";
		*/
		
		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
//        echo $sQuery;
		
		$rResult = $dbbengkel->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $dbbengkel->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $dbbengkel->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($aRow[ $aColumns[$i] ]===null){
						$aRow[ $aColumns[$i] ] = '';
					}

					if(is_numeric($aRow[ $aColumns[$i] ]) and $aColumns[$i] <> 'hp'){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}
	
	public function submit(){

	}
}

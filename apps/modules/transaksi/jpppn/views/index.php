<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
</style>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">   
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?>            -->

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-1">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                                </div>
                            </div>

                            <div class="col-xs-3">
                              <div class="box-header box-view">
                                <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()" > Ubah</button> 
                              </div>
                            </div>

                            <!-- <div class="col-xs-2">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-cetak"><i class="fa fa-print"></i> Cetak L/R</button>
                                 </div>
                            </div> --> 
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link " id="list-kb" name="list-kb" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Daftar Pembayaran JP (PPN) yang belum di Detail kan</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="input-kb" name="input-kb" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Input Detail Piutang JP (PPN)</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-kb">

                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">Kas / Bank</th>
                                                    <th style="text-align: center;">No. Cetak</th>
                                                    <th style="text-align: center;">No. K/B</th> 
                                                    <th style="text-align: center;">Tanggal</th> 
                                                    <th style="text-align: center;">Leasing</th> 
                                                    <th style="text-align: center;">Nilai</th> 
                                                    <th style="text-align: center;">Keterangan</th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-kb"> 

                                <div class="col-xs-12">
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-3">
                                            <div class="row">
                                                <div class="col-xs-5"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nokb']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-7"> 
                                                    <?php
                                                        echo form_input($form['nokb']);
                                                        echo form_error('nokb','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-4">
                                            <div class="row">
                                                <div class="col-xs-4"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nmkb']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['nmkb']);
                                                        echo form_error('nmkb','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['nilai']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['nilai']);
                                                        echo form_error('nilai','<div class="note">','</div>');
                                            ?>
                                        </div>     
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-3">
                                            <div class="row">
                                                <div class="col-xs-5"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tglkb']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-7"> 
                                                    <?php
                                                        echo form_input($form['tglkb']);
                                                        echo form_error('tglkb','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-4">
                                            <div class="row">
                                                <div class="col-xs-4"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nmleasing']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['nmleasing']);
                                                        echo form_error('nmleasing','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['tot_ar_jp']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['tot_ar_jp']);
                                                echo form_error('tot_ar_jp','<div class="note">','</div>');
                                            ?>
                                        </div>    
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-3"> 
                                        </div> 

                                        <div class="col-xs-4">
                                            <div class="row">
                                                <div class="col-xs-4"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['ket']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['ket']);
                                                        echo form_error('ket','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['selisih']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['selisih']);
                                                echo form_error('selisih','<div class="note">','</div>');
                                            ?>
                                        </div>       
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row"> 
                                        <div class="col-xs-7">  
                                        </div>
                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['ket_selisih']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['ket_selisih']);
                                                echo form_error('ket_selisih','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label> Detail Pembayaran PPN JP </label>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTableindex display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">No. DO</th>
                                                    <th style="text-align: center;">Tgl DO</th>
                                                    <th style="text-align: center;">No. SPK</th> 
                                                    <th style="text-align: center;">Tgl SPK</th> 
                                                    <th style="text-align: center;">Atas Nama PO</th> 
                                                    <th style="text-align: center;">Nama Pemesan</th> 
                                                    <th style="text-align: center;">Nama STNK</th> 
                                                    <th style="text-align: center;">Alamat</th> 
                                                    <th style="text-align: center;">AR PPN JP</th> 
                                                </tr>
                                            </thead> 
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div>
              </div>
          </div>
            <!-- /.box-body -->
        <?php echo form_close(); ?>
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        reset();
        autonum();
        $('#nokb').mask('*****-****-****');
        $('.btn-ubah').hide();  
        $('#btn-refresh').show();  

        $("#nokb").change(function(){
            set_nokb(); 
        });
        $('.btn-refresh').click(function(){
            table.ajax.reload();
        });

        $('#input-kb').click(function(){
            $('.btn-ubah').show(); 
            $('.btn-cetak').show();
            $('.btn-ubah').attr("disabled",true); 
            // set_nokb();
            // getdetJP();
            // $('#ket').hide();
            $('.btn-refresh').hide();
        });

        $('#list-kb').click(function(){
            $('.btn-ubah').hide();  
            // $('#ket').hide();
            $('.btn-refresh').show();
        });

        var column = [];

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        }); 

        table = $('.dataTable').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mData" : "nokb",
                    "mData" : "nmkb",
                    "mRender": function (data, type, row) {
                        var btn = '<a href="jpppn/edit/' + row.nokb +'">' + row.nmkb + ' </a>';
                        return btn;
                    }
                },
                {
                    "aTargets": [ 4 ],
                    "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                    },
                    "sClass": "center"
                    
                },
                {
                    "aTargets": [ 6 ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"
                    
                } ],
            // "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nmkb" },
                { "data": "nocetak" },
                { "data": "nokb"},
                { "data": "tglkb" },
                { "data": "nmleasing"},
                { "data": "nilai"},
                { "data": "ket"},
                { "data": "edit"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('jpppn/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="No." ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
    });

    function autonum(){
        $('#nilai').autoNumeric('init');
        $('#tot_ar_jp').autoNumeric('init');
        $('#selisih').autoNumeric('init'); 
    }

    function set_nokb(){
      // alert(ket);
      var nokb = $("#nokb").val(); 
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("jpppn/set_nokb");?>",
            data: {"nokb":nokb},
            success: function(resp){ 
              if(resp==='"empty"'){ 
                    clear();  
                    $('.btn-ubah').attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                    // baris 1 
                      $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb))); 
                      $("#nmkb").val(data.nmkb);
                      $("#nmleasing").val(data.nmleasing);
                      $("#ket").val(data.ket);
                      $("#ket_selisih").val(data.ket_selisih);

                      $("#nilai").autoNumeric('set',data.nilai); 
                      getdetJP(); 
                      $('.btn-ubah').attr('disabled',false);
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function sum(){
        var nilai = $('#nilai').autoNumeric('get');
        var selisih = $('#nilai').autoNumeric('get'); 
        if(nilai===''){
            $('#nilai').autoNumeric('set',0);
            var nilai = 0;
        }
        if(selisih===''){
            $('#selisih').autoNumeric('set',0);
            var selisih = 0;
        } 

        var tot_ar_jp = parseInt(nilai) - parseInt(selisih);
        if(!isNaN(tot_ar_jp)){
            $('#tot_ar_jp').autoNumeric('set',tot_ar_jp);
        } else {
            $('#tot_ar_jp').autoNumeric('set',0);
            var tot_ar_jp = 0;
        } 
    }

    function ubah(){
        var nokb = $('#nokb').val();
        var nokb = nokb.toUpperCase();
        window.location.href = 'jpppn/edit/'+nokb;
    }
    
    function getdetJP(){
      var nokb = $("#nokb").val(); 
      var nokb = nokb.toUpperCase();
        //alert(kddiv);
        var column1 = [];   

        column1.push({
            "aTargets": [ 9 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        column1.push({
            "aTargets": [ 2,4 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        }); 

        table1 = $('.dataTableindex').DataTable({
            "aoColumnDefs": column1,
            "columns": [
                { "data": "no"},
                { "data": "nodo" },
                { "data": "tgldo" },
                { "data": "noso" },
                { "data": "tglso"},
                { "data": "nama_p" },
                { "data": "nama" },
                { "data": "nama_s" },
                { "data": "alamat" },
                { "data": "nilai" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],  
            // "orderCellsTop": true,
            "fixedHeader": true, 
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            // "fixedColumns": {
            //     leftColumns: 2
            // },
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": nokb});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){

            },
            "sAjaxSource": "<?=site_url('jpppn/getdetPO');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) { 
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 9 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index  
                var nilai = $('#nilai').autoNumeric('get'); 
                if(nilai===''){
                    $('#nilai').autoNumeric('set',0);
                    var nilai = 0;
                } 

                if(!isNaN(nilai)){ 
                    $('#tot_ar_jp').autoNumeric('set',total);
                    var selisih = parseInt(nilai) - parseInt(total);
                    if(!isNaN(selisih)){
                        $('#selisih').autoNumeric('set',selisih);
                    } else {
                        $('#selisih').autoNumeric('set',0);
                        var selisih = 0;
                    }
                } 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });  

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            });
        });

        //row number
        table1.on( 'draw.dt', function () {
        var PageInfo = $('.dataTableindex').DataTable().page.info();
                table1.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
    }

    function clear(){  
        $('#nmkb').val(''); 
        $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#nmleasing").val(''); 
        $('#ket').val(''); 
        $('#ket_selisih').val('');

        //nominal
        $('#nilai').autoNumeric('set',0);   
        $('#tot_ar_jp').autoNumeric('set',0); 
    }

    function reset(){
        $('#nokb').val('');
        $('#nmkb').val('');
        $('#nilai').val('');
        $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#nmleasing").val('');
        $('#tot_ar_jp').val('');
        $('#ket').val('');
        $('#selisih').val('');
        $('#ket_selisih').val('');
    }


    function dateback(tgl) {
      var less = $("#jth_tmp").val();
      var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var res = day + '-' + month + '-' + year;
        return res;
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
      } 
</script>

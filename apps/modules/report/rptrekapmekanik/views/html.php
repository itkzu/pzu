<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php

/* 
 * ***************************************************************
 * Script : html.php
 * Version : 
 * Date : Oct 31, 2017 11:46:13 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();
$decode = json_decode($decode); 
// echo "<script>console.log('" . $decode->cabang . "');</script>"; 
// echo $decode->data->nmcabang;
if($array['submit']==="tunai_excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAPORAN REKAP MEKANIK DARI " .$array['periode_awal'] ." SAMPAI " .$array['periode_akhir'] ." ".date('Y-m-d H:i:s').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');
//$caption = "<b>DAFTAR PIUTANG PELUNASAN LEASING </b>"
//        . "<br>"
//        . "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->apps->logintag)."</b>"
//        . "<br>"
//        . "<b> PER TANGGAL " . $array['periode_akhir'] ."</b>"
//        . "<br><br>";

$this->table->add_row(array(
    array('data' => '<b>DAFTAR REKAP MEKANIK</b>'
                        , 'colspan' => '13'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$this->table->add_row(array(
    array('data' => "<b>". strtoupper($this->apps->title)." - ". strtoupper($decode->cabang)."</b>"
                        , 'colspan' => '13'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);   
$this->table->add_row(array(
    array('data' => "<b> DARI  " . $array['periode_awal'] ." TANGGAL " . $array['periode_akhir'] ."</b>"
                        , 'colspan' => '13'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$namaheader = array(
    array('data' => 'NO'
                        , 'align' => 'center'
                        , 'width' => '10px'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'TANGGAL'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'NAMA MEKANIK'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'TOTAL'
                        , 'align' => 'center'
                        , 'style' => ' width: 100px; font-size: 12px;'),
    array('data' => 'QTY JASA'
                        , 'align' => 'center'
                        , 'style' => ' width: 100px; font-size: 12px;'),
    array('data' => 'TOTAL JASA'
                        , 'align' => 'center'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'QTY REGULER'
                        , 'align' => 'center'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'TOTAL REGULER'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'QTY OLI'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'TOTAL OLI'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'QTY PART'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'TOTAL PART'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'QTY AMIC'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
);
$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
// Caption text
//$this->table->set_caption($caption);
$this->table->add_row($namaheader);

$leas = array();
foreach ($decode->leasing as $leasing) {
    $leas[$leasing] = $leasing;
}
$res = array();
$g_1 = 0;
$g_2 = 0;
$g_3 = 0;
$g_4 = 0;
$g_5 = 0;
$g_6 = 0;
$g_7 = 0;
$g_8 = 0;
$g_9 = 0;
$g_10 = 0;
foreach ($leas as $v) {
    $no = 1;
    $s_1 = 0;
    $s_2 = 0;
    $s_3 = 0;
    $s_4 = 0;
    $s_5 = 1;
    $s_6 = 0;
    $s_7 = 0;
    $s_8 = 0;
    $s_9 = 0;
    $s_10 = 0;
    foreach ($decode->data as $data) {
        if($v==$data->nmmekanik){          
            $akun_d = array(
                array('data' => $data->no
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->tglsv
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->nmmekanik
                                    , 'style' => 'text-align: left; font-size: 12px;'), 
                array('data' => number_format($data->total, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->jml_jasa, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->tot_jasa, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->jml_reg, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->tot_reg, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->jml_oli, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->tot_oli, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->jml_part, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->tot_part, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->amic, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
            );                     
            $this->table->add_row($akun_d);    
            $s_1+=$data->total;
            $s_2+=$data->jml_jasa;
            $s_3+=$data->tot_jasa;
            $s_4+=$data->jml_reg;  
            $s_5+=$data->tot_reg;
            $s_6+=$data->jml_oli;
            $s_7+=$data->tot_oli;
            $s_8+=$data->jml_part;
            $s_9+=$data->tot_part;
            $s_10+=$data->amic;
        }
    }
    $separator = array(
        array('data' => '<b>TOTAL ' . $v.'</b>'
                            , 'colspan' => '3'
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => '<b>'.number_format($s_1, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_2, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_3, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_4, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_5, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_6, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_7, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_8, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_9, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_10, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
    );
    $this->table->add_row($separator);    
    $g_1 = $g_1+$s_1;
    $g_2 = $g_2+$s_2;
    $g_3 = $g_3+$s_3;
    $g_4 = $g_4+$s_4; 
    $g_5 = $g_5+$s_5;
    $g_6 = $g_6+$s_6;
    $g_7 = $g_7+$s_7;
    $g_8 = $g_8+$s_8;
    $g_9 = $g_9+$s_9;
    $g_10 = $g_10+$s_10;
}

$g_total = array(
    array('data' => '<b>GRAND TOTAL</b>'
                        , 'colspan' => '7'
                        , 'style' => 'text-align: left; font-size: 12px;'),
    array('data' => '<b>'.number_format($g_1, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_2, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_3, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_4, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_5, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_6, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_7, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_8, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_9, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_10, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
);
$this->table->add_row($g_total); 


// $desc_1 = array(
//     array('data' => '&nbsp;'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => 'DIBUAT OLEH'
//                         , 'colspan' => '2'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => '&nbsp;'
//                         , 'colspan' => '3'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => 'MENGETAHUI'
//                         , 'colspan' => '5'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
// );
// $this->table->add_row($desc_1);   
// for($i=1;$i<=5;$i++){
//   $this->table->add_row(array(
//         array('data' => '&nbsp;'
//                             , 'colspan' => '11'
//                             , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     ));   
// }
// $desc_2 = array(
//     array('data' => '&nbsp;'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => $this->perusahaan[0]['nmadh']
//                         , 'colspan' => '2'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => '&nbsp;'
//                         , 'colspan' => '3'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => $this->perusahaan[0]['nmkacab']
//                         , 'colspan' => '5'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
// );
// $this->table->add_row($desc_2); 

$this->table->set_template($template);
echo $this->table->generate();      
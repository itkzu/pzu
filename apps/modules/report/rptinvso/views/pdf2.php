
<style>  
      caption {
          padding-top: 8px;
          padding-bottom: 8px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto;
          font-family: verdana;
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      } 

      @media print {
        footer {page-break-after: always;}
      }
</style> 

  <?php 
   
  foreach ($data as $value) { 

    ?>
      <table style="border-collapse: collapse;" width="100%" rules="none" border="1px" cellspacing="1">
        <tbody  style=''>
          <tr>
            <td colspan="13" style="text-align: left; width: 55%; font-size: 12px;">&nbsp; PT. PRIMA ZIRANG UTAMA </td>
            <td colspan="11" rowspan="2" style="text-align: center; width: 45%; font-size: 24px;">&nbsp; <b>FAKTUR PENJUALAN<b></td> 
          </tr>
          <tr>
            <td colspan="24" style="text-align: left; width: 55%; font-size: 12px;">&nbsp; <?php echo $value['alamat_div'];?></td> 
          </tr> 
          <tr>
            <td colspan="24" style="text-align: left; width: 100%; font-size: 12px;">&nbsp; <?php echo $value['notelp2'];?></td> 
          </tr> 
          <tr>
            <td colspan="24" style="text-align: left; width: 100%; font-size: 12px;">&nbsp; 02.511.814.2-512.000</td> 
          </tr> 
        </tbody>
      </table>  
      <table style="border-collapse: collapse;" width="100%" >
        <tbody >
          <tr>
            <td colspan="23" style="text-align: left; width: 99%; font-size: 12px; border-top: 1px solid #000; border-left: 1px solid #000; ">&nbsp; <b>Kepada Yth :</b></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; border-top: 1px solid #000;">&nbsp;</td> 
          </tr>
          <tr>
            <td colspan="15" style="text-align: left; width: 54%; font-size: 12px; border-left: 1px solid #000;">&nbsp;</td> 
            <td colspan="8" style="text-align: left; width: 45%; font-size: 12px; border-top: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000;">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000;">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 8%; font-size: 12px;">Nama</td> 
            <td colspan="12" style="text-align: left; width: 46%; font-size: 12px;">: <?php echo $value['nama']; ?></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-left: 1px solid #000;">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp; <b>No. Transaksi</b></td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp; <b>Tanggal</b></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 8%; font-size: 12px;">Alamat</td> 
            <td colspan="12" style="text-align: left; width: 46%; font-size: 12px;">: <?php echo $value['alamat']; ?></td> 
            <td colspan="8" style="text-align: left; width: 45%; font-size: 12px; border-left: 1px solid #000; border-right: 1px solid #000">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr>  
          <tr>
            <td colspan="15" style="text-align: center; width: 55%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-left: 1px solid #000;">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp; DO &nbsp;: <?php echo $value['nodo']; ?></td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp; <?php echo date_format(date_create($value['tgldo']),"d/m/Y"); ?></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 8%; font-size: 12px;">Kota</td> 
            <td colspan="12" style="text-align: left; width: 46%; font-size: 12px;">: <?php echo $value['kota']; ?></td> 
            <td colspan="8" style="text-align: left; width: 45%; font-size: 12px; border-left: 1px solid #000; border-right: 1px solid #000">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 8%; font-size: 12px;">No. Telp</td> 
            <td colspan="12" style="text-align: left; width: 46%; font-size: 12px;">: <?php echo $value['notelp']; ?></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-left: 1px solid #000;">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp; SPK : <?php echo $value['noso']; ?></td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp; </td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <?php
              if($value['npwp']==='00.000.000.0-000.000'){
                $npwp = '';
              } else if($value['npwp']==='.   .   . -   .'){
                $npwp = '';
              } else if($value['npwp']===''){
                $npwp = '';
              } else {
                $npwp = $value['npwp']; 
              }
            ?>
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 8%; font-size: 12px;"></td> 
            <td colspan="12" style="text-align: left; width: 46%; font-size: 12px;"></td> 
            <td colspan="8" style="text-align: left; width: 45%; font-size: 12px; border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000;">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000;">&nbsp;</td> 
          </tr>   
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000 ">&nbsp;</td> 
            <td colspan="2" style="text-align: center; width: 8%; font-size: 12px;">&nbsp; <b>No.</b></td> 
            <td colspan="18" style="text-align: left; width: 80%; font-size: 12px;">&nbsp; <b>Keterangan</b></td> 
            <td colspan="2" style="text-align: center; width: 10%; font-size: 12px;">&nbsp; <b>Nilai</b></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000;">&nbsp;</td> 
          </tr>   
          <?php  $nilai = $data[0]['dpp_unit'] + $data[0]['ppn_unit']; ?>
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: center; width: 8%; font-size: 12px;">&nbsp; 1</b></td> 
            <td colspan="18" style="text-align: left; width: 80%; font-size: 12px;">&nbsp; 1 Unit <?php echo $data[0]['nmtipe'];?></td> 
            <td colspan="2" style="text-align: center; width: 10%; font-size: 12px;">&nbsp; <?php echo number_format($nilai,0,",",".");?></td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000;">&nbsp; </td> 
          </tr>   
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: center; width: 8%; font-size: 12px;">&nbsp; </b></td>  
            <td colspan="3" style="text-align: left; width: 15%; font-size: 12px;">&nbsp; No Rangka </td> 
            <td colspan="5" style="text-align: left; width: 25%; font-size: 12px; ">&nbsp;: <?php echo $data[0]['nora'];?></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp; Warna</td> 
            <td colspan="4" style="text-align: left; width: 1%; font-size: 12px;">&nbsp; : <?php echo $data[0]['nmwarna'];?></td> 
            <td colspan="7" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000;">&nbsp;</td> 
          </tr>  
          <tr>
            <td colspan="1" style="text-align: center; width: 1%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="2" style="text-align: center; width: 8%; font-size: 12px;">&nbsp; </b></td>  
            <td colspan="3" style="text-align: left; width: 15%; font-size: 12px;">&nbsp; No Mesin </td> 
            <td colspan="5" style="text-align: left; width: 25%; font-size: 12px; ">&nbsp;: <?php echo $data[0]['nosin'];?></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="2" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp; Tahun</td> 
            <td colspan="4" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp; : <?php echo $data[0]['tahun'];?></td>
            <td colspan="7" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000;">&nbsp; </td> 
          </tr>   
          <tr>
            <td colspan="24" style="text-align: center; font-size: 1px; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">&nbsp;</td>  
          </tr>  
          <tr>
            <td colspan="10" style="text-align: center; width: 34%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="10" style="text-align: right; width: 40%; font-size: 12px;">&nbsp;   Harga Sebelum Diskon</b></td>  
            <td colspan="2" style="text-align: right; width: 20%; font-size: 12px; ">&nbsp; <?php echo number_format($data[0]['dpp_unit'],0,",",".");?> &nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 5%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; ">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="10" style="text-align: center; width: 34%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="10" style="text-align: right; width: 40%; font-size: 12px;">&nbsp;  Diskon</b></td>  
            <td colspan="2" style="text-align: right; width: 20%; font-size: 12px; border-bottom: 3px solid #000;">&nbsp; <?php echo number_format($data[0]['tot_disc_dpp'],0,",",".");?> -</td> 
            <td colspan="1" style="text-align: left; width: 5%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; ">&nbsp;</td> 
          </tr> 
           <?php  $nilai2 = $data[0]['dpp_unit'] - $data[0]['tot_disc_dpp'] ; ?>
          <tr>
            <td colspan="10" style="text-align: center; width: 34%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="10" style="text-align: right; width: 40%; font-size: 12px;">&nbsp;  Harga Setelah Diskon (DPP)</b></td>  
            <td colspan="2" style="text-align: right; width: 20%; font-size: 12px; ">&nbsp; <?php echo number_format($nilai2,0,",",".");?> &nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 5%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; ">&nbsp;</td> 
          </tr>  
           <?php  $nilai3 = $data[0]['ppn_unit'] - $data[0]['tot_disc_ppn'] ; ?> 
          <tr>
            <td colspan="10" style="text-align: center; width: 34%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="10" style="text-align: right; width: 40%; font-size: 12px;">&nbsp; PPN</b></td>  
            <td colspan="2" style="text-align: right; width: 20%; font-size: 12px;">&nbsp; <?php echo number_format($nilai3,0,",",".");?> &nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 5%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; ">&nbsp;</td>
          </tr>  
          <tr>
            <td colspan="10" style="text-align: center; width: 34%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="10" style="text-align: right; width: 40%; font-size: 12px;">&nbsp; Pengurusan Surat Kendaraan (BBN)</b></td>  
            <td colspan="2" style="text-align: right; width: 20%; font-size: 12px; border-bottom: 3px solid #000;">&nbsp; <?php echo number_format($data[0]['bbn'],0,",",".");?> +</td> 
            <td colspan="1" style="text-align: left; width: 5%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; ">&nbsp;</td>
          </tr>  
          <?php   $total = $nilai2 + $nilai3 + $data[0]['bbn']; 

                  if($data[0]['bbn']>0){
                    $otr = 'OTR';
                  } else {
                    $otr = 'OFTR';
                  }
          ?>
          <tr>
            <td colspan="10" style="text-align: center; width: 34%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td> 
            <td colspan="10" style="text-align: right; width: 40%; font-size: 12px;">&nbsp; <b>TOTAL HARGA </b></b></td>  
            <td colspan="2" style="text-align: right; width: 20%; font-size: 12px;">&nbsp; <?php echo number_format($total,0,",",".");?> &nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 5%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000; ">&nbsp;</td>
          </tr>  
          <tr>
            <td colspan="15" style="text-align: center; width: 55%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp;</td> 
            <td colspan="3" style="text-align: center; width: 21%; font-size: 12px;">&nbsp; Mengetahui ,</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="15" style="text-align: center; width: 55%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp;</td> 
            <td colspan="3" style="text-align: center; width: 21%; font-size: 12px;">&nbsp; </td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="15" style="text-align: center; width: 55%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp;</td> 
            <td colspan="3" style="text-align: center; width: 21%; font-size: 12px;">&nbsp; </td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="15" style="text-align: center; width: 55%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp;</td> 
            <td colspan="3" style="text-align: center; width: 21%; font-size: 12px;">&nbsp; </td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="15" style="text-align: center; width: 55%; font-size: 12px; border-left: 1px solid #000">&nbsp;</td>  
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="3" style="text-align: left; width: 21%; font-size: 12px;">&nbsp;</td> 
            <td colspan="3" style="text-align: center; width: 21%; font-size: 12px;">&nbsp;<b><?php echo $value['nmadh']; ?></td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; ">&nbsp;</td> 
            <td colspan="1" style="text-align: left; width: 1%; font-size: 12px; border-right: 1px solid #000">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="24" style="text-align: center; font-size: 12px; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">&nbsp;</td>  
          </tr>  

        </tbody>
      </table>
      <table style="border-collapse: collapse;" width="100%" rules="none" border="0px" cellspacing="1">
        <tbody>
          <tr>
            <td colspan="24" style="text-align: left; width: 100%; font-size: 12px;">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="24" style="text-align: left; width: 100%; font-size: 12px;">&nbsp;</td> 
          </tr> 
          <tr>
            <td colspan="24" style="text-align: left; width: 100%; font-size: 2px;">&nbsp;</td> 
          </tr> 
        </tbody>
      </table>       
<?php 
  }

    ?>
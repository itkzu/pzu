<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

/**
 * Description of MY_Controller
 *
 * @author pudyasto
 */
class MY_Controller extends CI_Controller{
    var $menu_app="";
    var $msg_active="";
    var $msg_main="";
    var $msg_detail="";
    var $csrf="";
    public $mm="";
    public $yyyymm="";
    public $yyyymmdd="";
    public $ddmmyyyy="";
    public $last_ddmmyyyy="";

    public $perusahaan="";
    public $cabbkl="";
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('enc'));
        if(isset($_GET['token'])){
            $tmptoken = $this->enc->decode($_GET['token']);
            $userdata = json_decode($tmptoken);
            if($userdata->expd < time()){
                redirect('access/logout','refresh');
            }else{
                foreach ($userdata as $key => $object) {
                    $this->session->set_userdata($key, $object);
                }
            }
        }
        $logged_in = $this->session->userdata('logged_in');
        if(!$logged_in){
            redirect($this->apps->ssoapp . '/access?url=' . $this->apps->curPageURL(),'refresh');
            exit;
        }



        $kelas = $this->uri->segment(1);
        if($kelas=="dashboard" || empty($kelas)){
            $this->msg_main = "Selamat Datang ". ucwords(strtolower($this->session->userdata('nmuser')));
            $this->msg_detail = ' <i class="fa fa-map-marker text-success"></i> '.$this->apps->logintag;
        }else{
            foreach($this->rbac->ceksubmenu_app($kelas) as $val){
                $this->msg_main = $val->nmmenu;
                $this->msg_detail = $val->description;
            }
        }

        $show = $this->rbac->module_access('show');
        if(empty($show)){
            redirect('debug/err_505','refresh');
            exit;
        }

        $q_2 = $this->db->query("select * from pzu.perusahaan");
        if($q_2->num_rows()>0){
            $this->perusahaan = $q_2->result_array();
        }

        // if($this->perusahaan[0]['kddiv'] === $this->session->userdata('data')['kddiv']){
        //     $bkl = $this->db->query("select * from bkl.perusahaan");
        //     // echo $this->db->last_query();
        //     if($bkl->num_rows()>0){
        //         $this->cabbkl = $bkl->result_array();
        //     }
        // } else {
        //   $bkl = $this->db->query("select * from bkl.perusahaan where kddiv = '".$this->session->userdata('data')['kddiv']."'");
        //   // echo $this->db->last_query();
        //   if($bkl->num_rows()>0){
        //       $this->cabbkl = $bkl->result_array();
        //   }
        // }

        //echo "<script> console.log('PHP: ". json_encode($this->perusahaan) ."');</script>";

        //$q = $this->db->query("select glr.get_periode_gl('". $this->apps->kd_div ."') as periode");

        $q = $this->db->query("select glr.get_periode_gl('". $this->perusahaan[0]['kddiv'] ."') as periode");
        if($q->num_rows()>0){
            $res = $q->result_array();
			//var_dump($res);
            $mm = substr($res[0]['periode'], 4, 2);
            $yyyy= substr($res[0]['periode'], 0, 4);
            $this->mm = $this->num_format((int) $mm);
            $this->yyyymm = $yyyy."-".$this->num_format((int) $mm);
            $this->yyyymmdd = $yyyy."-".$this->num_format((int) $mm)."-01";
            $this->ddmmyyyy = "01-".$this->num_format((int) $mm)."-".$yyyy;
            $this->last_ddmmyyyy = date('t-m-Y', strtotime($yyyy."-".$this->num_format((int) $mm)."-01"));
            $this->periode_aktif = $this->get_month_name($this->yyyymm);
        }

        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

    }

    public function num_format($int) {
        if($int<=9){
            $int = "0".$int;
        }else{
            $int = $int;
        }
        return $int;
    }

    function get_month_name($param){
        $date = explode("-", $param);
        $mm = strtoupper(month($date[1]));
        $yy = $date[0];
        $dm = $param."-01";
        $dd = '';//date("t", strtotime($dm));
        return $dd." ".$mm." ".$yy;
    }
}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Srhbpkb_qry
 *
 * @author adi
 */
class Srhbpkb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getleas() {
        $this->db->select('kdleasing');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $this->db->order_by("kdleasing");
        $q = $this->db->get("pzu.v_leasing"); 
        return $q->result_array();
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function ctk($no) {
        $this->db->select("*");
        $this->db->where('noskbbn_leas',$no);
        $q = $this->db->get("pzu.vb_skbbn_leas");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function set_nosin() {
      $nosin = $this->input->post('nosin');
      $query = $this->db->query("select * from pzu.vm_kps where nosin = '" . $nosin . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_nosrh() {
      $nosrh = $this->input->post('nosrh');
      $query = $this->db->query("select * from pzu.t_skbbn_leas where noskbbn_leas = '" . $nosrh . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    } 

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nosrh']) ){
            if($_GET['nosrh']){
                $nosrh = $_GET['nosrh'];
            }else{
                $nosrh = '';
            }
        }else{
            $nosrh = '';
        }
        $aColumns = array('nobpkb',
                             'nama', 
                             'alamat',
                             'nopol',
                             'nosin',
                             'nodo',
                             'tgldo',
                             'noskbbn_leas',
                             'no');
      $sIndexColumn = "noskbbn_leas";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             nobpkb,
                             nama,
                             alamat,
                             nopol,
                             nosin,
                             nodo,
                             noskbbn_leas,
                             tgldo from pzu.v_skbbn_leas_det 
                        where noskbbn_leas = '".$nosrh."' order by nama) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noskbbn_leas ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            // $aRow['detail'] = array();

            // foreach ($detail as $value) {
            //     if($aRow['nopo']==$value['nopo']){
            //         $aRow['detail'][]= $value;
            //     }
            // } 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_det() {
        error_reporting(-1); 

        if( isset($_GET['kdleasing']) ){
            if($_GET['kdleasing']){
                $kdleasing = $_GET['kdleasing'];
            }else{
                $kdleasing = '';
            }
        }else{
            $kdleasing = '';
        }
        $aColumns = array('no',
                            'nodo',
                            'nobpkb',
                            'nama',
                            'alamat',
                            'nopol',
                            'nosin',
                            'nodo',
                            'tgldo'                           
        );

        $sIndexColumn = "nodo";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //row_number() over(order by nodo) as no
        $sTable = " (select '' as no,
                             nobpkb,
                             nama,
                             alamat,
                             nopol,
                             nosin,
                             nodo, 
                             tgldo from pzu.v_skbbn_leas_det 
                        where tgl_srh_bpkb is null and noskbbn_leas is null and kdleasing = '".$kdleasing."' order by nama) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if($aRow[ $aColumns[$i] ]===null){
                    $aRow[ $aColumns[$i] ] = '';
                }

                if(is_numeric($aRow[ $aColumns[$i] ])){
                    $row[] = floatval($aRow[ $aColumns[$i] ]);
                }else{
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
//            $row[6] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"" . site_url('ajufa/edit/' . $aRow['kode']) . "\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
//            $row[6] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('" . $aRow['kode'] . "');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";
            $output['aaData'][] = $row; 
        }
        echo  json_encode( $output );

    }

    public function tambah() {
        $noskbbn_leas = $this->input->post('noskbbn_leas');
        $tglskbbn_leas = $this->apps->dateConvert($this->input->post('tglskbbn_leas')); 
        $kdleasing = $this->input->post('kdleasing');
        $nodo = $this->input->post('nodo');
        $jml = $this->input->post('jml');
        $q = $this->db->query("select * from pzu.sskleas_d_ins('".$noskbbn_leas."','".$tglskbbn_leas."','".$kdleasing."','".$this->session->userdata("username")."',".$jml.",'{".$nodo."}')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function delDetail() {
        $noskbbn_leas         = $this->input->post('noskbbn_leas');
        $nodo         = $this->input->post('nodo'); 
        $q = $this->db->query("select title,msg,tipe from pzu.sskleas_d_del( '". $noskbbn_leas ."',
                                                                        '". $nodo ."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    public function submit() {     
        $noskbbn_leas = $this->input->post('noskbbn_leas');
        $tglskbbn_leas = $this->apps->dateConvert($this->input->post('tglskbbn_leas')); 
        $q = $this->db->query("select * from pzu.sskleas_ins( '". $noskbbn_leas ."', '". $tglskbbn_leas ."','".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function batal() {     
        $noskbbn_leas = $this->input->post('noskbbn_leas'); 
        $q = $this->db->query("select * from pzu.sskleas_btl('".$noskbbn_leas."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

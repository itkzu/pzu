<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-header">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div> 

          <div class="col-xs-12"> 
                <div style="border-top: 1px solid #ddd; height: 10px;"></div> 
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nosrh']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['nosrh']);
                                          echo form_error('nosrh','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglsrh']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tglsrh']);
                                          echo form_error('tglsrh','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>  

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdleasing']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php
                                          echo form_dropdown($form['kdleasing']['name'],
                                                              $form['kdleasing']['data'],
                                                              $form['kdleasing']['value'],
                                                              $form['kdleasing']['attr']);
                                          echo form_error('kdleasing','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 
                          </div>
                        </div>  

                        <div class="col-xs-2 col-md-2 ">
                          <div class="row"> 
                              <button type="button" class="btn btn-primary btn-set"> Set</button>
                              <button type="button" class="btn btn-secondary btn-unset"> Unset</button>
                          </div>
                        </div>  

                    </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                    <div class="row"> 
                      <div class="col-xs-12">
                          <label></label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-xs-12">
                    <p><a id="add" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-del"><i class="fa fa-minus"></i> Hapus</a></p>
                    <p id="alldata" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. BPKB</th> 
                            <th style="text-align: center;">Nama Konsumen</th> 
                            <th style="text-align: center;">Alamat Konsumen</th> 
                            <th style="text-align: center;">Nopol</th> 
                            <th style="text-align: center;">No. Mesin</th> 
                            <th style="text-align: center;">No. DO</th> 
                            <th style="text-align: center;">Tgl. DO</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>

              </div>
          </div>
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- modal dialog -->
<div id="modal_trans" class="modal fade">
    <div class="modal-dialog"  style="width: 99%;">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Faktur yang akan diproses BBN</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12"> 
                      <p id="nodo" class="kata"></p>
                      <p id="no" class="kata"></p>
                      <div class="table-responsive">
                        <table class="table table-hover table-bordered dataTable_modal display nowrap">
                          <thead>
                            <tr>
                              <th style="width: 10px;text-align: center;">No.</th>
                              <th style="width: 10px;text-align: center;">CL.</th>
                              <th style="text-align: center;">No. BPKB</th> 
                              <th style="text-align: center;">Nama Konsumen</th> 
                              <th style="text-align: center;">Alamat Konsumen</th> 
                              <th style="text-align: center;">Nopol</th> 
                              <th style="text-align: center;">No. Mesin</th> 
                              <th style="text-align: center;">No. DO</th> 
                              <th style="text-align: center;">Tgl. DO</th> 
                            </tr>
                          </thead>
                          <tfoot>
                            <tr>
                              <th style="width: 10px;text-align: center;">No.</th>
                              <th style="width: 10px;text-align: center;">CL.</th> 
                              <th style="text-align: center;"></th>
                              <th style="text-align: center;"></th>
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                            </tr>
                          </tfoot>
                          <tbody></tbody>
                        </table>
                      </div>
                    </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>
            <?php echo form_close(); ?>
 




<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        $('.btn-set').show();
        $('.btn-unset').hide();
        $('.btn-add').hide();
        $('.btn-del').hide();
        // $("#nosrh").val('ABJ-____-____');  
        $("#nosrh").attr('disabled',true  );   

        $('.btn-batal').click(function () { 
            batal();
        });   

        $('.btn-set').click(function () { 
            set();
        });  

        $('.btn-unset').click(function () { 
            unset();
        });   

        $('.btn-simpan').click(function () { 
            tambah();
        });  

        $('.btn-submit').click(function () { 
            submit();
        });  

        $('.btn-del').click(function () { 
            var nodo = table.row('.selected').data()['nodo']; 
            // alert(nodo);
            delDetail(nodo);
        });  

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return type === 'export' ? data : numeral(data).format('0,0.00');
        //     },
        //     "sClass": "right"
        // }); 

        column.push({
            "aTargets": [ 7 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });  

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nobpkb" },
                { "data": "nama" },
                { "data": "alamat"},
                { "data": "nopol" },
                { "data": "nosin"},
                { "data": "nodo"},
                { "data": "tgldo"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nosrh", "value": $('#nosrh').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('srhbpkb/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.dataTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });



        $('.btn-add').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            init_modal();
            $('#modal_trans').modal('toggle'); 
        });

    });

    function reset(){
      // $("#nosrh").val('ABJ-____-____');
      $("#tglsrh").val($.datepicker.formatDate('dd-mm-yy', new Date()));  
    }

    function clear(){
      $("#nosrh").val('');
      $("#tglsrh").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
    }

    function init_modal(){ 
      //var rows_selected = [];
      var column = []; 

      column.push({
        "aTargets":  [0] ,
        "searchable": false,
        "orderable": false, 

      });

      column.push({
        "aTargets":  [1] ,
        "searchable": false,
        "orderable": false,
        "checkboxes": {
          'selectRow': true
        }

      });

      // column.push({
      //  "aTargets": [ 4,5,6 ],
      //  "mRender": function (data, type, full) {
      //    return type === 'export' ? data : numeral(data).format('0,0');
      //  },
      //  "sClass": "right"
      // });

      column.push({
        "aTargets": [ 7 ],
        "mRender": function (data, type, full) {
          return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        },
        "sClass": "center"
      });


      tb_modal = $('.dataTable_modal').DataTable({
        "aoColumnDefs": column,
        "bProcessing": true,
        "select":{
          style: 'multi',
        },
        "fixedColumns": {
          leftColumns: 2
        },
              "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Semua Data"]],
        "bPaginate": true,
        "bDestroy": true,
        "bServerSide": true,
        "order": [[ 0, 'asc' ],[ 1, 'asc' ]],
        "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "kdleasing", "value": $('#kdleasing').val()});
            $.ajax( {
                "dataType": 'json',
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            } );
        },
        'rowCallback': function(row, data, index){
        },
        // "pagingType": "simple",
        "sAjaxSource": "<?=site_url('srhbpkb/json_dgview_det');?>",
        "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
        "oLanguage": {
          "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
        }
      });

      $('.dataTable_modal').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
      });

      $('.dataTable_modal tfoot th').each( function () {
        var title = $('.dataTable_modal tfoot th').eq( $(this).index() ).text();
        if(title!=="Edit" && title!=="CL." && title!=="No."){
          $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
        }else{
          $(this).html( '' );
        }
      } );

      tb_modal.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function (ev) {
          //if (ev.keyCode == 13) { //only on enter keypress (code 13)
            that
              .search( this.value )
              .draw();
          //}
        });
      });

        // row number
        tb_modal.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable_modal').DataTable().page.info();
                tb_modal.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        }); 

    }   

  function tambah(){  
      var rows_selected = tb_modal.column(1).checkboxes.selected();
      $('#nodo').val(rows_selected.join(","))
      var allkd = $("#nodo").val(); 
      var count = tb_modal.rows( { selected: true } ).count(); 
      var kdleasing = $("#kdleasing").val(); 
      var noskbbn_leas = $("#nosrh").val(); 
      var tglskbbn_leas = $("#tglsrh").val();  
      // alert(count);

      $.ajax({
          type: "POST",
          url: "<?=site_url('srhbpkb/tambah');?>",
          data: { "noskbbn_leas":noskbbn_leas, "tglskbbn_leas":tglskbbn_leas, "kdleasing":kdleasing, "nodo":allkd, "jml":count },
          success: function(resp){
            $("#modal_trans").modal("hide");
            var obj = jQuery.parseJSON(resp);
            $.each(obj, function(key, data){
              $('#nosrh').val(data.noskbbn_leas);
                swal({
                  title: data.title,
                  text: data.msg,
                  type: data.tipe
                }, function(){
                  table.ajax.reload();
                });
              });
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  } 

  function delDetail(nodo){     
    
      var noskbbn_leas = $('#nosrh').val();
      $.ajax({
          type: "POST",
          url: "<?=site_url('srhbpkb/delDetail');?>",
          data: {"noskbbn_leas":noskbbn_leas
                  ,"nodo":nodo 
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){ 
                  table.ajax.reload(); 
              });
          },
          error: function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      }); 
    }

    function batal(){ 
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var noskbbn_leas = $("#nosrh").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("srhbpkb/batal");?>",
                data: {"noskbbn_leas":noskbbn_leas },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function() {
                            window.location.href = '<?=site_url('srhbpkb');?>';  
                        }); 
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }); 
    }

    function set(){  
        $(".btn-set").hide();
        $(".btn-unset").show();
        $(".btn-add").show();
        $(".btn-del").show();
        $("#kdleasing").prop('disabled',true);
    }

    function unset(){  
        var noskbbn_leas = $("#nosrh").val(); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("srhbpkb/batal");?>",
            data: {"noskbbn_leas":noskbbn_leas },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function() {
                        clear();
                        table.ajax.reload();  
                        $(".btn-set").show();
                        $(".btn-unset").hide();
                        $(".btn-add").hide();
                        $(".btn-del").hide();
                        $("#kdleasing").prop('disabled',false);
                    }); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var noskbbn_leas = $("#nosrh").val();
            var tglskbbn_leas = $("#tglsrh").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("srhbpkb/submit");?>",
                data: {"noskbbn_leas":noskbbn_leas
                        ,"tglskbbn_leas":tglskbbn_leas},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){  
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function() {
                                // window.location.href = '<?=site_url('srhbpkb');?>'; 
                                // window.location.href = '../ctk_lr/'+nodo; 

                            window.open('ctk/'+noskbbn_leas, '_blank');
                            window.location.href = '<?=site_url('srhbpkb');?>';  
                        });  
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Closekbsh
 *
 * @author adi
 */
class Closekbsh extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('closekbsh/submit'),
            'add' => site_url('closekbsh/add'),
            'edit' => site_url('closekbsh/edit'),
            'reload' => site_url('closekbsh'),
        );
        $this->load->model('closekbsh_qry');

        $kasbank = $this->closekbsh_qry->refKasBank();
        foreach ($kasbank as $value) {
            $this->data['kasbank'][$value['kdkb']] = $value['nmkb'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->closekbsh_qry->json_dgview();
    }

    public function tglkb() {
        echo $this->closekbsh_qry->tglkb();
    }

    public function setNilai() {
        echo $this->closekbsh_qry->setNilai();
    }

    public function process() {
        echo $this->closekbsh_qry->process();
    }

    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->closekbsh_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kdkb'=> array(
                    'attr'        => array(
                        'id'    => 'kdkb',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kasbank'],
                    'value'    => '',
                    'name'     => 'kdkb',
                    'autofocus'   => ''
            ),
           'tglkb'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),

            //detail
           'so_awal'=> array(
                    'placeholder' => 'Saldo Awal',
                    'id'          => 'so_awal',
                    'name'        => 'so_awal',
                    'value'       => set_value('so_awal'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'so_fisik'=> array(
                    'placeholder' => 'Saldo Fisik',
                    'id'          => 'so_fisik',
                    'name'        => 'so_fisik',
                    'value'       => set_value('so_fisik'),
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => ''
            ),
           'tot_debet'=> array(
                    'placeholder' => 'Total Debet',
                    'id'          => 'tot_debet',
                    'name'        => 'tot_debet',
                    'value'       => set_value('tot_debet'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'tot_kredit'=> array(
                    'placeholder' => 'Total Kredit',
                    'id'          => 'tot_kredit',
                    'name'        => 'tot_kredit',
                    'value'       => set_value('tot_kredit'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'so_kasbon'=> array(
                    'placeholder' => 'Saldo Kasbon',
                    'id'          => 'so_kasbon',
                    'name'        => 'so_kasbon',
                    'value'       => set_value('so_kasbon'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'so_akhir'=> array(
                    'placeholder' => 'Saldo Akhir',
                    'id'          => 'so_akhir',
                    'name'        => 'so_akhir',
                    'value'       => set_value('so_akhir'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'sel_kb'=> array(
                    'placeholder' => 'Selisih Kas/Bank',
                    'id'          => 'sel_kb',
                    'name'        => 'sel_kb',
                    'value'       => set_value('sel_kb'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),

        );
    }

    private function validate() {
        $config = array(
            array(
                    'field' => 'Periode',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php
/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-md-12 ">
                        <div class="form-group has-error">
                            <?php
                            echo form_label('Masukkan Kata Kunci <small><i>( Nama Pemilik / No. DO / Alamat )</i></small>');
                            echo form_dropdown($form['nama']['name']
                                    , $form['nama']['data']
                                    , $form['nama']['value']
                                    , $form['nama']['attr']);
                            echo form_error('nama', '<div class="note">', '</div>');
                            ?>
                        </div> 
                    </div>
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <?php
                            echo form_label($form['tgldo']['placeholder']);
                            echo form_input($form['tgldo']);
                            echo form_error('tgldo', '<div class="note">', '</div>');
                            ?>
                        </div> 
                    </div> 
                    <div class="col-xs-12 col-md-12 ">
                        <div class="form-group">
                            <?php
                            echo form_label($form['pemilik']['placeholder']);
                            echo form_input($form['pemilik']);
                            echo form_error('pemilik', '<div class="note">', '</div>');
                            ?>
                        </div> 
                    </div>
                    <div class="col-xs-12 col-md-12 ">
                        <div class="form-group">
                            <?php
                            echo form_label($form['alamat']['placeholder']);
                            echo form_textarea($form['alamat']);
                            echo form_error('alamat', '<div class="note">', '</div>');
                            ?>
                        </div>                       
                    </div>
                    <div class="col-xs-12 col-md-12 ">
                        <div class="form-group">
                            <?=form_label($form['nohp']['placeholder'].' <small><i>( Format : 08xxxxxxx )</i></small>');?>
                            <div class="input-group">
                                <?php
                                    echo form_input($form['nohp']);
                                    echo form_error('nohp', '<div class="note">', '</div>');
                                ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-update">Ubah</button>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <?php
                            echo form_label($form['nmtipe']['placeholder']);
                            echo form_input($form['nmtipe']);
                            echo form_error('nmtipe', '<div class="note">', '</div>');
                            ?>
                        </div> 
                    </div>                
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <?php
                            echo form_label($form['jnsbayarx']['placeholder']);
                            echo form_input($form['jnsbayarx']);
                            echo form_error('jnsbayarx', '<div class="note">', '</div>');
                            ?>
                        </div> 
                    </div>                    
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        init_nama();
        $("#nama").change(function () {
            getData();
        });
        
        $(".btn-update").click(function(){
            updateData();
        });
    });
    
    function updateData() {
        var noso = $("#nama").val();
        var nohp = $("#nohp").val();
        if(nohp.trim()===""){
            swal("Kesalahan", "Nomor HP Tidak Boleh Kosong!", "error");
            return false;
        }
        $.ajax({
            type: "POST",
            url: "<?= site_url("mstdatacust/updateData"); ?>",
            data: {"noso":noso, "nohp_s": nohp},
            beforeSend: function () {
                $(".btn").attr("disabled",true);
            },
            success: function (resp) {
                $(".btn").attr("disabled",false);
                var obj = jQuery.parseJSON(resp);
                if(obj.state==="1"){
                    swal("Informasi", obj.msg, "info");
                }else{
                    swal("Kesalahan", obj.msg, "error");
                }
            },
            error: function (event, textStatus, errorThrown) {
                $(".btn").attr("disabled",false);
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
    
    function getData() {
        var nama = $("#nama").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url("mstdatacust/getData"); ?>",
            data: {"nama": nama},
            beforeSend: function () {

            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function (key, value) {
                    $("#pemilik").val(value.nama);
                    $("#nohp").val(value.nohp);
                    $("#nmtipe").val(value.nmtipe);
                    $("#nodo").val(value.nodo);
                    $("#tgldo").val(value.tgldo);
                    $("#alamat").val(value.alamat2);
                    $("#jnsbayarx").val(value.jnsbayarx);
                    $("#nohp").focus();
                });
            },
            error: function (event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function init_nama() {
        $("#nama").select2({
            ajax: {
                url: "<?= site_url('mstdatacust/getNama'); ?>",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan Nama Pemilik ...',
            dropdownAutoWidth: false,
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: format_nama, // omitted for brevity, see the source of this page
            templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
        });
    }

    function format_nama_terpilih(repo) {
        return repo.full_name || repo.text;
    }

    function format_nama(repo) {
        if (repo.loading)
            return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'><b style='font-size: 14px;'>NO SO : " + repo.noso + " / <i class=\"fa fa-user\"></i> " + repo.nama + "</b></div>";

        if (repo.alamat) {
            markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.alamat + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><i class=\"fa fa-file-o\"></i> " + repo.nodo + " | <i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " | <i class=\"fa fa-calendar\"></i> " + repo.tgldo + "</div>" +
                "</div>" +
                "</div>" +
                "</div>";
        return markup;
    }
</script>
<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Closegl_qry
 *
 * @author adi
 */
class Closegl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getDataCabang() {
        // if($this->perusahaan[0]['kddiv']===$this->session->userdata('data')['kddiv']){

          $query = $this->db->get('bkl.get_div_unit_bkl()');
        // } else {
        //   $this->db->where('kddiv',$this->session->userdata('data')['kddiv']);
        //   $query = $this->db->get('bkl.get_div_unit_bkl()');
        // }
        return $query->result_array();
        // $q = $this->db->query("select * from bkl.get_divisi_bkl()");
        // $res = $q->result_array();
        // return $res;
    }

    public function tglkb() {
        $kddiv = $this->input->post('kddiv');
        $q = $this->db->query("select substring(glr.get_periode_gl('".$kddiv."'),1,4) || '-' || substring(glr.get_periode_gl('".$kddiv."'),5,6) as tglkb");
        $res = $q->result_array();
        return json_encode($res);
    }

    public function cekpost() {
        $kddiv = $this->input->post('kddiv');
        $tglkbs = $this->input->post('tglkb');
        $tgl2 = explode('-',$tglkbs);
        $tglkb = $tgl2[0].$tgl2[1]; //$this->apps->dateConvert($_GET['periode_akhir']);//

        $q = $this->db->query("select count(jml) as jml from bkl.v_trx_blm_post where kddiv = '".$kddiv."' and periode = '".$tglkb."'");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return json_encode($res);
    }

    public function setAktiva() {
        $kddiv = $this->input->post('kddiv');
        $query = $this->db->query("select * from glr.v_nilai_aktiva where kddiv = '".$kddiv."'");
        $res = $query->result_array();
        return json_encode($res);
    }

    public function setPasiva() {
        $kddiv = $this->input->post('kddiv');
        $query = $this->db->query("select * from glr.v_nilai_pasiva where kddiv = '".$kddiv."'");
        $res = $query->result_array();
        return json_encode($res);
    }

    public function setLR() {
        $kddiv = $this->input->post('kddiv');
        $query = $this->db->query("select * from glr.v_nilai_labarugi where kddiv = '".$kddiv."'");
        $res = $query->result_array();
        return json_encode($res);
    }

    public function process() {
        $kddiv = $this->input->post('kddiv');
        $query = $this->db->query("select * from glr.closing_gl('".$kddiv."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function json_dgview() {
        error_reporting(-1);

        if( isset($_GET['kddiv']) ){
            if($_GET['kddiv']){
                $kddiv = $_GET['kddiv'];
            }else{
                $kddiv = '';
            }
        }else{
            $kddiv = '';
        }

  			if( isset($_GET['tglkb']) ){
  					if($_GET['tglkb']){
  							$tgl2 = explode('-',$_GET['tglkb']);
  							$periode = $tgl2[0].$tgl2[1]; //$this->apps->dateConvert($_GET['periode_akhir']);//
  					}else{
  							$periode = '';
  					}
  			}else{
  					$periode = '';
  			}
        $aColumns = array('jml', 'ket', 'kddiv', 'periode');
    	$sIndexColumn = "kddiv";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT kddiv
                              , jml
                              , ket
                              , periode
                              FROM bkl.v_trx_blm_post where kddiv = '".$kddiv."' and periode = '".$periode."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by kddiv ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptadharjp_qry
 *
 * @author adi
 */
class Rptadharjp_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }    
    public function get_data() {
        $periode_akhir = $this->input->post('periode_akhir');
        $str = "select * from pzu.pl_ar_jp('".$this->apps->dateConvert($periode_akhir)."')";
        $q = $this->db->query($str);
        if($q->num_rows()>0){
            $arr = $q->result_array();
            $leas = array();
            foreach ($arr as $dts) {
                $leas[$dts['kdleasing']] = $dts['kdleasing'];
            }
            $dt_arr = array();
            foreach ($leas as $v) {
                $no = 1;
                foreach ($arr as $data) {
                    if($v==$data['kdleasing']){
                        $dt = $data + array('no'=>$no);
                        $dt_arr[] = $dt;
                        $no++;
                    }
                }
            }
            $res = array(
                'jp' => $leas,
                'data' => $dt_arr,
            );
        }else{
            $res = null;
        }
        return json_encode($res);
    }

}

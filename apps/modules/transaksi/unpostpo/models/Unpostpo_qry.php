<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of 
 *
 * @author 
 */

class Unpostpo_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getData() {
        $this->db->select("to_char(tglpo,'DD-MM-YYYY') AS tglpo"
                . " , to_char(tglterima,'DD-MM-YYYY') AS tglterima, nmsup"
                . " , to_char(tgljt,'DD-MM-YYYY') AS tgljt");
                //. " , to_char(tgljt,'DD-MM-YYYY') AS tgljt, jnsbayar, harga, disc, disctop, dpp, ppn, total");
        $nopo = $this->input->post('nopo');
        $this->db->where("nopo",$nopo);
        $q = $this->db->get("pzu.vl_po_m");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
    
    public function proses() {
        $nodo = $this->input->post('nopo');
        $q = $this->db->query("select code,title,msg,tipe from pzu.po_unpost('". $nodo ."')");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    /*
    public function submit() {
        try {
            $array = $this->input->post();
            if(!empty($array['nodo']) && !empty($array['stat'])){
                $this->db->where('nodo', $array['nodo']);
                $this->db->set('tgl_aju_fa',NULL);
                $resl = $this->db->update('pzu.t_do');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }
    */
}

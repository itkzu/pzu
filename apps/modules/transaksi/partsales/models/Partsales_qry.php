<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Partsales_qry
 *
 * @author adi
 */
class Partsales_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    } 

    //cari no assist
    public function getNoSO() { 
        if ($this->session->userdata('data')['kddiv']==='ZPH01.01B'){
            $idtrm = "AND a.dealerid = '10091'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B'){
            $idtrm = "AND a.dealerid = '12603'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B'){
            $idtrm = "AND a.dealerid = '13435'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B'){
            $idtrm = "AND a.dealerid = '13528'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B'){
            $idtrm = "AND a.dealerid = '14031'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B'){
            $idtrm = "AND a.dealerid = '15544'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.01B.01'){
            $idtrm = "AND a.dealerid = '10091'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.01'){
            $idtrm = "AND a.dealerid = '12603'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.02'){
            $idtrm = "AND a.dealerid = '12603'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B.01'){
            $idtrm = "AND a.dealerid = '13435'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B.01'){
            $idtrm = "AND a.dealerid = '13528'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B.01'){
            $idtrm = "AND a.dealerid = '14031'";
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B.01'){
            $idtrm = "AND a.dealerid = '15544'";
        } else {
            $idtrm = '';
        } 
        $q = $this->db->query("select a.noso,upper(a.namacustomer) as namacustomer from api.bso a left join bkl.tb_so b on a.noso = b.noso where a.noso like '%PRS%' AND b.noso is null ".$idtrm." group by a.noso,a.namacustomer order by noso ASC ");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return $res;
    }

    public function getDataSupplier() {
        $this->db->select("kdsupplier,nmsupplier");
        $this->db->where("faktif",true);
        if($this->session->userdata('data')['kddiv']!='ZPH01.02S'){
            // $idr = array()
            $this->db->where("fcab",true);
            $this->db->or_where("kddiv");
        }
        $this->db->order_by("kdsupplier","ASC");
        $query = $this->db->get("bkl.supplier");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }

    public function getPart() {
        $q = $this->db->query("select a.kdpart,a.nmpart from bkl.part a join bkl.grup b on a.kdgrup = b.kdgrup where b.fjasa = false group by a.kdpart,a.nmpart");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return $res;
    }

    //import
    public function get_SO() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }  
        }

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/prsl/read',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "fromTime": "'.$rdate7.'",
                "toTime": "'.$rdate1.'",
                "dealerId":"'.$dealerId.'",
                "noSO" : ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=Xi.HNu3sqsLkerdNZU.H9qZHR67P.35BMakyEhSO7Kk-1698760488-0-Acd1habkwOKW47ghkKJyFhp6Ighd9wvt/tBKjfI5EgKNB5pVqcnrcqIe2uh/UNnu+2NT6IXHAaYMHMGc6Q8s+2E='
            ),
        ));   

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response; 
    } 

    public function ins_SO() {    
        $info   = $this->input->post('info'); 
        $info   = str_replace("'","''",$info);
        $q = $this->db->query("insert into api.bso_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function save_SO() { 
        $q = $this->db->query("select title,msg,tipe from api.in_bso()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    //table 
    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['notrm']) ){
            if($_GET['notrm']){
                $notrm = $_GET['notrm'];
            }else{
                $notrm = '';
            }
        }else{
            $notrm = '';
        } 

        $aColumns = array('kodepart',
                          'qty',
                          'harga',
                          'discount',
                          'total',
                          'noref',
                          'no');
        $sIndexColumn = "kodepart";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( SELECT '' as no, a.*
                              FROM api.in_bso('".$notrm."','".$this->session->userdata('data')['kddiv']."') a) AS a"; 

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "order by kodepart";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }

            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['noref']."','".$aRow['kodepart']."','".$aRow['qty']."',".$aRow['harga'].",".$aRow['discount'].",".$aRow['total'].");\">Edit</button>"; 
            $aRow['hapus'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-warning btn-xs \" onclick=\"del('".$aRow['noref']."','".$aRow['kodepart']."');\">Delete</button>"; 

            $output['data'][] = $aRow;
        }
      echo  json_encode( $output );
    }

    public function set_nmpart(){
        $kdpart = $this->input->post('materialno');

        $query = $this->db->query("select * from bkl.part where kdpart =  '".$kdpart."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function set_bagal(){
        $kdsup = $this->input->post('kdsup');

        $query = $this->db->query("select * from bkl.supplier where kdsupplier =  '".$kdsup."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function set_nmpel(){
        $notrm = $this->input->post('notrm');

        $query = $this->db->query("select * from api.bso where noso =  '".$notrm."'");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function batal() {
        $nobpo = $this->input->post('notrm');
        $q = $this->db->query("select * from api.btl_bso('".$nobpo."','".$this->session->userdata('data')['kddiv']."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    public function update() {
      $notrm  = $this->input->post('notrm');
      $kdpart = $this->input->post('kdpart');
      $qty    = $this->input->post('qty');
      $harga  = $this->input->post('harga');
      $disc   = $this->input->post('disc');
      $total  = $this->input->post('total');

      $q  = $this->db->query("update api.t_bso_tmp set harga = ".$harga.", discount = ".$disc." , total = ".$total." where noref = '".$notrm."' and kddiv = '".$this->session->userdata('data')['kddiv']."' and kodepart = '".$kdpart."' and qty = '".$qty."'");
      
      if(!$q) { 
          $this->state = "0";
      } else { 
          $this->state = "1";
      }   
      return json_encode($this->state);  
    }

    public function det_update() {  
      $kdpart   = $this->input->post('kdpart');
      $qty      = $this->input->post('qty');
      $kdgrup   = $this->input->post('kdgrup');
      $harga    = $this->input->post('harga');
      $disc     = $this->input->post('disc');
      $total    = $this->input->post('total');
      $query        = $this->db->query("select * from api.bso_d_ins('".$kdpart."'
                                                                  ,".$qty."
                                                                  ,'".$kdgrup."'
                                                                  ,".$harga."
                                                                  ,".$disc."
                                                                  ,".$total." 
                                                                  ,'".$this->session->userdata('data')['kddiv']."')");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    } 

    public function del() {  
      $notrm        = $this->input->post('notrm');
      $materialno   = $this->input->post('kdpart'); 
      $query        = $this->db->query("delete from api.t_bso_tmp where noref = '".$notrm."' and kodepart = '".$materialno."' and kddiv = '".$this->session->userdata('data')['kddiv']."'");
      // echo $this->db->last_query();
      if(!$query) { 
          $this->state = "0";
      } else { 
          $this->state = "1";
      }   
      return json_encode($this->state);  
    } 

    public function submit() { 
        $notrm          = $this->input->post('notrm');
        $kdsup          = $this->input->post('kdsup');
        $tipebayar      = $this->input->post('tipebayar');
        $nik            = $this->input->post('alamat');
        $alamat         = $this->input->post('nik');
        $tglso          = $this->apps->dateConvert($this->input->post('tglso'));

        $query        = $this->db->query("select * from api.bso_ins('".$notrm."'
                                                                  ,".$kdsup."
                                                                  ,'".$tipebayar."'
                                                                  ,'".$tglso."'
                                                                  ,'".$alamat."'
                                                                  ,'".$nik."'
                                                                  ,'".$this->session->userdata('data')['kddiv']."'
                                                                  ,'".$this->session->userdata('username')."')");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    } 

    private function getdetail(){
        $output = array();
        $str = "SELECT a.kodepart
                    , a.namapart
                    , a.qty
                    , a.harga
                    , a.discount
                    , a.um
                    , a.total 
                    , a.noref
                      FROM bkl.tb_so_d a 
                    WHERE a.noref not like '%BAGO%'
                    ORDER BY a.kodepart";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
      }
        return $output;
    }

    private function getdetail_bag(){
        $output = array();
        $str = "SELECT a.kodepart
                    , a.namapart
                    , a.qty
                    , a.harga
                    , a.discount
                    , a.um
                    , a.total 
                    , a.noref
                      FROM bkl.tb_so_d a 
                    WHERE a.noref like '%BAGO%'
                    ORDER BY a.kodepart";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
      }
        return $output;
    }

    private function getdetail_prs(){
        $output = array();
        $str = "SELECT a.partsnumber
                    , CASE WHEN b.nmpart is null THEN 'DATA BELUM MASUK' ELSE b.nmpart END AS nmpart
                    , a.promoidparts
                    , a.kuantitas
                    , a.hargaparts
                    , a.discamount
                    , a.uangmuka
                    , a.totalhargaparts
                    , a.noso
                      FROM api.bso_d a
                      LEFT JOIN bkl.part b on a.partsnumber = b.kdpart  
                      GROUP by a.partsnumber, b.nmpart, a.promoidparts, a.kuantitas, a.hargaparts, a.discamount, a.uangmuka, a.totalhargaparts, a.noso
                    ORDER BY a.noso";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
      }
        return $output;
    }

    public function json_dgview() {
        error_reporting(-1);
        
        if( isset($_GET['periode_so']) ){
            if($_GET['periode_so']){
                $tgl2 = explode('-', $_GET['periode_so']);
                $periode_awal = $tgl2[1].$tgl2[0]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode_awal = '';
            } 
        }else{
            $periode_awal = '';
        } 

        $aColumns = array('noref', 'noso', 'tanggal', 'idcust', 'nama_pelanggan','total' );
        $sIndexColumn = "noref";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 

        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, noref, noso, tanggal, idcust, nama_pelanggan, total
                        FROM bkl.tb_so where date_to_periode(tanggal) = '".$periode_awal."' and noref not like '%BAGO%') AS a";
        } else {
            $sTable = " ( SELECT '' as no, noref, noso, tanggal, idcust, nama_pelanggan, total
                        FROM bkl.tb_so where date_to_periode(tanggal) = '".$periode_awal."' and noref not like '%BAGO%' and substring(kddiv,1,9) = substring('".$this->session->userdata('data')['kddiv']."',1,9)) AS a";
        }
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
              $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by noref desc";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['noref']==$value['noref']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function json_dgview_bag() {
        error_reporting(-1);
        
        if( isset($_GET['periode_bag']) ){
            if($_GET['periode_bag']){
                $tgl2 = explode('-', $_GET['periode_bag']);
                $periode_awal = $tgl2[1].$tgl2[0]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode_awal = '';
            } 
        }else{
            $periode_awal = '';
        } 

        $aColumns = array('noref', 'noso', 'tanggal', 'idcust', 'nama_pelanggan','total' );
        $sIndexColumn = "noref";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 

        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, noref, noso, tanggal, idcust, nama_pelanggan, total
                        FROM bkl.tb_so where date_to_periode(tanggal) = '".$periode_awal."' and noref like '%BAGO%') AS a";
        } else {
            $sTable = " ( SELECT '' as no, noref, noso, tanggal, idcust, nama_pelanggan, total
                        FROM bkl.tb_so where date_to_periode(tanggal) = '".$periode_awal."' and noref like '%BAGO%' and substring(kddiv,1,9) = substring('".$this->session->userdata('data')['kddiv']."',1,9)) AS a";
        }
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
              $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by noref desc";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_bag();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['noref']==$value['noref']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function json_dgview_prs() {
        error_reporting(-1);
        
        if( isset($_GET['periode_prs']) ){
            if($_GET['periode_prs']){
                $tgl2 = explode('-', $_GET['periode_prs']);
                $periode_awal = $tgl2[1].$tgl2[0]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode_awal = '';
            } 
        }else{
            $periode_awal = '';
        } 

        $aColumns = array('noso', 'tglso', 'idcustomer', 'namacustomer', 'discso', 'dealerid', 'totalhargaso' );
        $sIndexColumn = "noso";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 

        $kddiv = $this->session->userdata('data')['kddiv'];

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, noso, tglso, idcustomer, namacustomer, discso, dealerid, totalhargaso
                        FROM api.bso where date_to_periode(tglso::date) = '".$periode_awal."' and noso not in (select noso from bkl.tb_so where noso is not null group by noso)
                        GROUP by noso, tglso, idcustomer, namacustomer, discso, dealerid, totalhargaso) AS a";
        } else {
            $sTable = " ( SELECT '' as no, noso, tglso, idcustomer, namacustomer, discso, dealerid, totalhargaso
                        FROM api.bso where date_to_periode(tglso::date) = '".$periode_awal."' and dealerid = '".$dealerId."' and noso not in (select noso from bkl.tb_so where noso is not null group by noso) GROUP by noso, tglso, idcustomer, namacustomer, discso, dealerid, totalhargaso) AS a";
        }
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
              $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by noso desc";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_prs();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['noso']==$value['noso']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function html($noso) {
        $q = $this->db->query("SELECT * FROM bkl.vb_so where noref = upper('".$noso."') ");
        // $this->db->select("to_char(now(),'HH:MI:SS') as wkt, *");
        // $this->db->where('nodo',upper('.$nodo.'));
        // $q = $this->db->get("pzu.vb_do");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-lg-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-header">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div> 

          <div class="col-md-12"> 
                <div style="border-top: 1px solid #ddd; height: 10px;"></div> 
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['noabj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-9">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['noabj']);
                                          echo form_error('noabj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglabj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tglabj']);
                                          echo form_error('tglabj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-md-12">
                          <label> Detail Faktur </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div> 
                    </div>
                  </div>  

                  <div class="col-md-12">
                    <p><a id="add" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-del"><i class="fa fa-minus"></i> Hapus</a></p>
                    <p id="alldata" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Kwitansi</th> 
                            <th style="text-align: center;">Nama Konsumen</th> 
                            <th style="text-align: center;">Alamat Konsumen</th> 
                            <th style="text-align: center;">Tipe Unit</th> 
                            <th style="text-align: center;">No. Mesin</th> 
                            <th style="text-align: center;">No. DO</th> 
                            <th style="text-align: center;">Tgl. DO</th> 
                            <th style="text-align: center;">Trm Faktur</th>  
                          </tr>
                        </thead>
                        <tfoot>
                          <!-- <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                          </tr> -->
                        </tfoot>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>

              </div>
          </div>
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- modal dialog -->
<div id="modal_trans" class="modal fade">
    <div class="modal-dialog"  style="width: 99%;">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Faktur yang akan diproses BBN</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-md-12"> 
                      <p id="nodo" class="kata"></p>
                      <p id="no" class="kata"></p>
                      <div class="table-responsive">
                        <table class="table table-hover table-bordered dataTable_modal display nowrap">
                          <thead>
                            <tr>
                              <th style="width: 10px;text-align: center;">No.</th>
                              <th style="width: 10px;text-align: center;">CL.</th>
                              <th style="text-align: center;">No. Kwitansi</th> 
                              <th style="text-align: center;">Nama Konsumen</th> 
                              <th style="text-align: center;">Alamat Konsumen</th> 
                              <th style="text-align: center;">Tipe Unit</th> 
                              <th style="text-align: center;">No. Mesin</th> 
                              <th style="text-align: center;">No. DO</th> 
                              <th style="text-align: center;">Tgl. DO</th> 
                              <th style="text-align: center;">Trm Faktur</th>  
                            </tr>
                          </thead>
                          <tfoot>
                            <tr>
                              <th style="width: 10px;text-align: center;">No.</th>
                              <th style="width: 10px;text-align: center;">CL.</th> 
                              <th style="text-align: center;"></th>
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th>
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                              <th style="text-align: center;"></th> 
                            </tr>
                          </tfoot>
                          <tbody></tbody>
                        </table>
                      </div>
                    </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>
            <?php echo form_close(); ?>
 




<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        // $("#noabj").val('ABJ-____-____');  
        $("#noabj").attr('disabled',true  );  

        $('#nosin').keyup(function () {
            var jml = $('#nosin').val();
            var nosin = jml.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("_","");
            var n = nosin.length;
            
            if(n===12){
                // console.log(n);    
                set_nosin();   
            }
            // set_nosin();
        }); 

        $('.btn-batal').click(function () { 
            batal();
        });   

        $('.btn-simpan').click(function () { 
            tambah();
        });  

        $('.btn-submit').click(function () { 
            submit();
        });  

        $('.btn-del').click(function () { 
            var nodo = table.row('.selected').data()['nodo']; 
            delDetail(nodo);
        });  

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return type === 'export' ? data : numeral(data).format('0,0.00');
        //     },
        //     "sClass": "right"
        // }); 

        column.push({
            "aTargets": [ 7,8 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });  

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nokps" },
                { "data": "nama" },
                { "data": "alamat"},
                { "data": "nmtipe" },
                { "data": "nosin"},
                { "data": "nodo"},
                { "data": "tgldo"},
                { "data": "tgl_trm_fa"} 
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noabj", "value": $('#noabj').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('ajubbn/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.dataTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });



        $('.btn-add').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            init_modal();
            $('#modal_trans').modal('toggle'); 
        });

    });

    function reset(){
      // $("#noabj").val('ABJ-____-____');
      $("#tglabj").val($.datepicker.formatDate('dd-mm-yy', new Date()));  
    }

    function clear(){
      $("#noajustnk").val('');
      $("#tglajustnk").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#noso").val('');
      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $("#nora").val('');
      $("#kdtipe").val('');
      $("#nmtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#harga_beli").val('');
      $("#nama").val('');
      $("#alamat").val('');
      $("#kel").val('');
      $("#kec").val('');
      $("#nohp").val('');
      $("#notelp").val('');
    }

    function init_modal(){ 
      //var rows_selected = [];
      var column = []; 

      column.push({
        "aTargets":  [0] ,
        "searchable": false,
        "orderable": false, 

      });

      column.push({
        "aTargets":  [1] ,
        "searchable": false,
        "orderable": false,
        "checkboxes": {
          'selectRow': true
        }

      });

      // column.push({
      //  "aTargets": [ 4,5,6 ],
      //  "mRender": function (data, type, full) {
      //    return type === 'export' ? data : numeral(data).format('0,0');
      //  },
      //  "sClass": "right"
      // });

      column.push({
        "aTargets": [ 8,9 ],
        "mRender": function (data, type, full) {
          return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        },
        "sClass": "center"
      });


      tb_modal = $('.dataTable_modal').DataTable({
        "aoColumnDefs": column,
        "bProcessing": true,
        "select":{
          style: 'multi',
        },
        "fixedColumns": {
          leftColumns: 2
        },
              "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Semua Data"]],
        "bPaginate": true,
        "bDestroy": true,
        "bServerSide": true,
        "order": [[ 0, 'asc' ],[ 1, 'asc' ]],
        "rowCallback": function(row, data, dataIndex){
          /*
              // Get row ID
              var rowId = data[0];

              // If row ID is in the list of selected row IDs
              if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
              }
              */
          },

        // "pagingType": "simple",
        "sAjaxSource": "<?=site_url('ajubbn/json_dgview_det');?>",
        "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
        "oLanguage": {
          "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
        }
      });

      $('.dataTable_modal').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
      });

      $('.dataTable_modal tfoot th').each( function () {
        var title = $('.dataTable_modal tfoot th').eq( $(this).index() ).text();
        if(title!=="Edit" && title!=="CL." && title!=="No."){
          $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
        }else{
          $(this).html( '' );
        }
      } );

      tb_modal.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function (ev) {
          //if (ev.keyCode == 13) { //only on enter keypress (code 13)
            that
              .search( this.value )
              .draw();
          //}
        });
      });

        // row number
        tb_modal.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable_modal').DataTable().page.info();
                tb_modal.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        }); 

    }   

  function tambah(){  
      var rows_selected = tb_modal.column(1).checkboxes.selected();
      $('#nodo').val(rows_selected.join(","))
      var allkd = $("#nodo").val(); 
      var count = tb_modal.rows( { selected: true } ).count(); 
      var nobbn_aju = $("#noabj").val(); 
      var tglbbn_aju = $("#tglabj").val();  
      // alert(count);

      $.ajax({
          type: "POST",
          url: "<?=site_url('ajubbn/tambah');?>",
          data: { "nobbn_aju":nobbn_aju, "tglbbn_aju":tglbbn_aju, "nodo":allkd, "jml":count },
          success: function(resp){
            $("#modal_trans").modal("hide");
            var obj = jQuery.parseJSON(resp);
            $.each(obj, function(key, data){
              $('#noabj').val(data.nobbn_aju);
                swal({
                  title: data.title,
                  text: data.msg,
                  type: data.tipe
                }, function(){
                  table.ajax.reload();
                });
              });
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  } 

  function delDetail(nodo){     
    
      var nobbn_aju = $('#noabj').val();
      $.ajax({
          type: "POST",
          url: "<?=site_url('ajubbn/delDetail');?>",
          data: {"nobbn_aju":nobbn_aju
                  ,"nodo":nodo 
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){ 
                  table.ajax.reload(); 
              });
          },
          error: function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      }); 
    }

    function batal(){ 
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
            var nobbn_aju = $("#noabj").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("ajubbn/batal");?>",
                data: {"nobbn_aju":nobbn_aju },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                      swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                      }, function() {
                        window.location.href = '<?=site_url('ajubbn');?>';  
                      }); 
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
            }); 
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nobbn_aju = $("#noabj").val();
            var tglbbn_aju = $("#tglabj").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("ajubbn/submit");?>",
                data: {"nobbn_aju":nobbn_aju
                        ,"tglbbn_aju":tglbbn_aju},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){  
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function() {
                                // window.location.href = '<?=site_url('ajubbn');?>'; 
                                // window.location.href = '../ctk_lr/'+nodo; 

                            window.open('ctk/'+nobbn_aju, '_blank');
                            window.location.href = '<?=site_url('ajubbn');?>';  
                        });  
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 
</script>

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
                        , 'class' => "form-inline");
					echo form_open($submit,$attributes);
				?>
				<div class="form-group">
					<?php 
						echo form_label($form['periode_awal']['placeholder']);
						echo form_input($form['periode_awal']);
						echo form_error('periode_awal','<div class="note">','</div>');
					?>
				</div>
				<div class="form-group">
					<?php 
						echo form_label($form['periode_akhir']['placeholder']);
						echo form_input($form['periode_akhir']);
						echo form_error('periode_akhir','<div class="note">','</div>');
					?>
				</div>

				<button type="button" class="btn btn-primary btn-tampil">Tampil</button>

				<?php echo form_close(); ?>
				<hr>
				<div class="table-responsive">
					<table style="width: 2700px;" class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="width: 70px;text-align: center;">No. Transaksi</th>
								<th style="width: 70px;text-align: center;">Tanggal</th>
								<th style="text-align: center;">Konsumen</th>
								<th style="width: 50px;text-align: center;">Nopol</th>
								<th style="width: 50px;text-align: center;">No. HP</th>
								<th style="width: 70px;text-align: center;">Tgl Beli</th>
								<th style="width: 50px;text-align: center;">KM</th>
								<th style="width: 50px;text-align: center;">Kode Mesin</th>
								<th style="width: 50px;text-align: center;">No. Mesin</th>
								<th style="width: 200px;text-align: center;">Tipe</th>
								<th style="width: 50px;text-align: center;">KPB ke-</th>
								<th style="width: 50px;text-align: center;">Klaim Astra</th>
								<th style="width: 50px;text-align: center;">Jenis</th>
								<th style="width: 150px;text-align: center;">Kode Jasa / Barang</th>
								<th style="width: 200px;text-align: center;">Nama Jasa / Barang</th>
								<th style="width: 20px;text-align: center;">Qty</th>
								<th style="width: 70px;text-align: center;">Harga</th>
								<th style="width: 50px;text-align: center;">Diskon Satuan (%)</th>
								<th style="width: 50px;text-align: center;">Diskon Total (%)</th>
								<th style="width: 100px;text-align: center;">Total</th>
								<th style="width: 70px;text-align: center;">DPP</th>
								<th style="width: 50px;text-align: center;">PPN</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		$(".btn-tampil").click(function(){
			table.ajax.reload();
		});

		var column = [];

		column.push({ 
				"aTargets": [ 6,15,16,17,18,19,20,21 ],
				"mRender": function (data, type, full) {
						return type === 'export' ? data : numeral(data).format('0,0');
				},
				"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
							,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
				$.ajax( {
					"dataType": 'json', 
					"type": "GET", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptwidet/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//"dom": 'Bfrtip',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					 }
				}
			],
			//"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});
		


		
		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" ){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
	});
</script>
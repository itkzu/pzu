<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstasethapus
 *
 * @author adi
 */
class Mstasethapus extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('mstasethapus/submit'),
            'add' => site_url('mstasethapus/add'),
            'edit' => site_url('mstasethapus/edit'),
            'reload' => site_url('mstasethapus'),
        );
        $this->load->model('mstasethapus_qry');        
        
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    public function add(){  
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }    
    
    public function json_dgview() {
        echo $this->mstasethapus_qry->json_dgview();
    }
    
    public function get_detail_ahp() {
        echo $this->mstasethapus_qry->get_detail_ahp();
    }
    
    public function submit() {  
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        
        if($this->validate($id,$stat) == TRUE){
            $res = $this->mstasethapus_qry->submit();
            echo $res;
        }else{
            $arr = array(
                'state' => '0', 
                'msg' => strip_tags(validation_errors()),
            );
            echo json_encode($arr);
        }
    }
    
    public function proses() {       
        $id = $this->input->post('kode');
        $stat = $this->input->post('stat'); 
        if($this->validate_proses($id,$stat) == TRUE){
            $res = $this->mstasethapus_qry->proses();
            echo $res;
        }else{
            $arr = array(
                'state' => '0', 
                'msg' => strip_tags(validation_errors()),
            );
            echo json_encode($arr);
        }
    }

    public function get_aset() {
        $q = $this->input->post('q');
        $arr_aset = $this->input->post('arr_aset');
        if($arr_aset){
            $this->db->where_not_in('kdasetx', $arr_aset);
        }
        $this->db->where("(LOWER(kdaset::varchar) LIKE '%".strtolower($q)."%' "
                . " OR LOWER(nmaset) LIKE '%".strtolower($q)."%' "
                . " OR to_char(tglbeli,'DD MM YYYY') LIKE '%".strtolower($q)."%' "
                . " OR LOWER(ket_aset) LIKE '%".strtolower($q)."%')");
        $this->db->order_by("CASE "
                . " WHEN LOWER(kdaset::varchar) LIKE '".strtolower($q)."' THEN 1"
                . " WHEN LOWER(kdaset::varchar) LIKE '".strtolower($q)."%' THEN 2"
                . " WHEN LOWER(kdaset::varchar) LIKE '%".strtolower($q)."' THEN 3"
                . " WHEN LOWER(kdaset::varchar) LIKE '%".strtolower($q)."%' THEN 4"
                
                . " WHEN LOWER(nmaset) LIKE '".strtolower($q)."' THEN 1"
                . " WHEN LOWER(nmaset) LIKE '".strtolower($q)."%' THEN 2"
                . " WHEN LOWER(nmaset) LIKE '%".strtolower($q)."' THEN 3"
                . " WHEN LOWER(nmaset) LIKE '%".strtolower($q)."%' THEN 4"
                
                . " WHEN LOWER(ket_aset) LIKE '".strtolower($q)."' THEN 5"
                . " WHEN LOWER(ket_aset) LIKE '".strtolower($q)."%' THEN 6"
                . " WHEN LOWER(ket_aset) LIKE '%".strtolower($q)."' THEN 7"
                . " WHEN LOWER(ket_aset) LIKE '%".strtolower($q)."%' THEN 8"
                . " END");
        $this->db->order_by('nmaset','ASC');
        $this->db->select("kdaset, kdasetx, nmaset, ket_aset, to_char(tglbeli,'DD MM YYYY') as tglbeli, harga, nmjaset, nmlokasi, ket_posisi");
        $query = $this->db->get('v_aset_riel');
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['kdasetx'],
                    'text' => $value['nmaset'],
                    'ket_aset' => $value['ket_aset'],
                    'kdaset' => $value['kdaset'],
                    'tglbeli' => $value['tglbeli'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            echo json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'deskripsi' => '',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            echo json_encode($data);
        }
    }
    
    private function _init_add(){
        
        $this->data['form'] = array(
           'kode'=> array(
                    'placeholder' => 'Kode Transaksi Aset - Otomatis',
                    'id'      => 'kode',
                    'name'        => 'kode',
                    'value'       => set_value('kode'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'  => '',
            ),
            'kdaset'=> array(
                    'attr'        => array(
                        'id'    => 'kdaset',
                        'class' => 'form-control',
                    ),
                    'data'     => array(),
                    'value'    => '',
                    'name'     => 'kdaset',
                    'required'    => '',
            ),
           'tglhapus'=> array(
                    'placeholder' => 'Tanggal Hapus Aset',
                    'id'      => 'tglhapus',
                    'name'        => 'tglhapus',
                    'value'       => set_value('tglhapus'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
           'catatan'=> array(
                    'placeholder' => 'Catatan',
                    'id'      => 'catatan',
                    'name'        => 'catatan',
                    'value'       => set_value('alasan'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 100px; min-height: 100px;'
            ),
           'alasan'=> array(
                    'placeholder' => 'Alasan Penghapusan',
                    'id'      => 'alasan',
                    'name'        => 'alasan',
                    'value'       => set_value('alasan'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 100px; min-height: 100px;'
            ),
        );
    }
    
    private function _init_edit(){
        $groupid = $this->uri->segment(3);
        $this->_check_id($groupid);
        $this->data['form'] = array(
           'kode'=> array(
                    'placeholder' => 'Kode Transaksi Aset - Otomatis',
                    'id'      => 'kode',
                    'name'        => 'kode',
                    'value'       => $this->val[0]['noashp'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'  => '',
            ),
           'tglhapus'=> array(
                    'placeholder' => 'Tanggal Hapus Aset',
                    'id'      => 'tglhapus',
                    'name'        => 'tglhapus',
                    'value'       => $this->val[0]['tglashp'],
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
           'catatan'=> array(
                    'placeholder' => 'Catatan',
                    'id'      => 'catatan',
                    'name'        => 'catatan',
                    'value'       => $this->val[0]['ket'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 100px; min-height: 100px;'
            ),
            'kdaset'=> array(
                    'attr'        => array(
                        'id'    => 'kdaset',
                        'class' => 'form-control',
                    ),
                    'data'     => array(),
                    'value'    => '',
                    'name'     => 'kdaset',
                    'required'    => '',
            ),
           'alasan'=> array(
                    'placeholder' => 'Alasan Penghapusan',
                    'id'      => 'alasan',
                    'name'        => 'alasan',
                    'value'       => set_value('alasan'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 100px; min-height: 100px;'
            ),  
        );
    }
    
    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }
        $this->val= $this->mstasethapus_qry->select_data($id);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaset',
                    'label' => 'Nama Aset',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'alasan',
                    'label' => 'Alasan Penghapusan',
                    'rules' => 'required',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
    
    private function validate_proses($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'tglhapus',
                    'label' => 'Tanggal Hapus',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'catatan',
                    'label' => 'Catatan Penghapusan',
                    'rules' => 'required',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

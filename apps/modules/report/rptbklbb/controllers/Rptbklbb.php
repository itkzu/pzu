<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbklbb
 *
 * @author adi
 */
class Rptbklbb extends MY_Controller {
	protected $data = '';
	protected $val = '';
	public function __construct()
	{
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,

			'submit' => site_url('rptbklbb/submit'),
			'add' => site_url('rptbklbb/add'),
			'edit' => site_url('rptbklbb/edit'),
			'reload' => site_url('rptbklbb'),
		);
		$this->load->model('rptbklbb_qry');

        //$akun = $this->rptbklbb_qry->getAkun();
        //foreach ($akun as $value) {
            //$this->data['akun'][$value['kdakun']] = $value['kdakun']." - ".$value['nmakun'];
        //}

		$cabang = $this->rptbklbb_qry->getDataCabang();
		foreach ($cabang as $value) {
			$this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
		}
	}

	//redirect if needed, otherwise display the user list

	public function index(){
		$this->_init_add();
		$this->template
			->title($this->data['msg_main'],$this->apps->name)
			->set_layout('main')
			->build('index',$this->data);
	}

	public function getAkun() {
		echo $this->rptbklbb_qry->getAkun();
	}

	public function submit() {
		$this->data['rptakun'] = $this->rptbklbb_qry->submit();
		if(empty($this->data['rptakun']['akun_h'])){
			$this->template
				->title($this->data['msg_main'],$this->apps->name)
				->set_layout('main')
				->build('debug/err_000',$this->data);
		}else{
			$array = $this->input->post();

			if($array['submit']==="html" || $array['submit']==="excel"){
				$this->template
					->title($this->data['msg_main'],$this->apps->name)
					->set_layout('print-layout')
					->build('html',$this->data);
			}else{
				redirect("rptbklbb");
			}
		}
	}

	private function _init_add(){
		$this->data['form'] = array(
			'kddiv'=> array(
					'attr'        => array(
						'id'    => 'kddiv',
						'class' => 'form-control  select',
					),
					'data'        =>  $this->data['kddiv'],
					'value'       => set_value('kddiv'),
					'name'        => 'kddiv',
					'required'    => '',
					'placeholder' => 'Cabang',
			),
			'kddiv2'=> array(
					'type'        => 'hidden',
					'id'          => 'kddiv2',
					'name'        => 'kddiv2',
					'value'       => set_value('kddiv2'),
					'class'       => 'form-control',
					'required'    => '',
			),
			'nmdiv'=> array(
					'placeholder' => 'Nama Divisi',
					'type'        => 'hidden',
					'id'          => 'nmdiv',
					'name'        => 'nmdiv',
					'value'       => set_value('nmdiv'),
					'class'       => 'form-control',
					'required'    => '',
			),
			'akun_multi'=> array(
					'attr'        => array(
						'id'    => 'akun_multi',
						'class' => 'form-control',
						'multiple' => '',
					),
					'data'        => '',
					'value'       => set_value('akun_multi'),
					'name'        => 'akun_multi[]',
			),
			'akun_awal'=> array(
					'attr'        => array(
						'id'    => 'akun_awal',
						'class' => 'form-control',
					),
					'data'        => '',
					'value'       => set_value('akun_awal'),
					'name'        => 'akun_awal',
			),
			'akun_akhir'=> array(
					'attr'        => array(
						'id'    => 'akun_akhir',
						'class' => 'form-control',
					),
					'data'        => '',
					'value'       => set_value('akun_akhir'),
					'name'        => 'akun_akhir',
			),
			'periode_awal'=> array(
					'placeholder' => 'Periode Awal',
					'id'          => 'periode_awal',
					'name'        => 'periode_awal',
					'value'       => $this->ddmmyyyy,
					'class'       => 'form-control',
			),
			'periode_akhir'=> array(
					'placeholder' => 'Periode Akhir',
					'id'          => 'periode_akhir',
					'name'        => 'periode_akhir',
					'value'       => $this->last_ddmmyyyy,
					'class'       => 'form-control',
			),
		);
	}
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptunitentryday_qry
 *
 * @author adi
 */
class Rptunitentryday_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		// $this->db = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode = $_GET['periode_awal'];//$tgl1[1].$tgl1[0];
			}else{
				$periode = '';
			}
		}else{
			$periode = '';
		}
		

//echo "<script> console.log('PHP: ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";

//		$aColumns = array('dt','pk','no_ktp','nm','adr','subtotsrv','subtotitem','subtot','distrs','tottrs');

							//, 'nmtipe'
		$aColumns = array('dt'
							, 'qty_tot'
							, 'qty_reg'
							, 'qty_kpb'
							, 'tot'
							, 'jasa'
							, 'oli'
							, 'part'
		);


/*
		$aColumns = array('pk'
							, 'dt'
							, 'kpbto'
							, 'distrs'
		);
*/

		$sIndexColumn = "dt";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}




		$sTable = "(


SELECT aa.dt
		, sum(aa.qty_reg+aa.qty_kpb) AS qty_tot
		, sum(aa.qty_reg) AS qty_reg
		, sum(aa.qty_kpb) AS qty_kpb
		, sum(aa.jasa) AS jasa
		, sum(aa.oli) AS oli
		, sum(aa.part) AS part
		, sum(aa.jasa+aa.oli+aa.part) AS tot
FROM
(

SELECT --zz.pk, 
		zz.dt
		, IIF(zz.tp='REG',1,0) AS qty_reg
		, IIF(zz.tp='KPB',1,0) AS qty_kpb
		, sum(zz.jasa) AS jasa
		, sum(zz.oli) AS oli
		, sum(zz.part) AS part

FROM
(

SELECT	yy.pk
		, yy.dt
		, IIF(yy.kpbto='','REG','KPB') AS tp
		, IIF(yy.grp='JASA',yy.dpp,0) AS jasa
		, IIF(yy.grp='OLI',yy.dpp,0) AS oli
		, IIF(yy.grp='PART',yy.dpp,0) AS part
FROM
(

SELECT 
		xx.pk
		, xx.dt
		, xx.grp
		, xx.kpbto
		, IIF(xx.isklaim = '', round(IIF(xx.subtotal is null,0,xx.subtotal),0) - round(IIF(xx.subtotal is null,0,xx.subtotal)/11,0) , IIF(xx.subtotal is null,0,xx.subtotal)) as dpp
FROM
(
						SELECT a.pk
							, format(a.dt,'dd/mm/yyyy') AS dt 
							, a.kpbto
							, 'JASA' as grp

							, IIF(xd.nt = '1' AND val(a.kpbto) > 0, 'YA', '') AS isklaim

							, IIF(xd.nt = '1' AND val(a.kpbto) = 1, x.qty * c.kpb1 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
								IIF(xd.nt = '1' AND val(a.kpbto) = 2, x.qty * c.kpb2 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
									IIF(xd.nt = '1' AND val(a.kpbto) = 3, x.qty * c.kpb3 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
										IIF(xd.nt = '1' AND val(a.kpbto) = 4, x.qty * c.kpb4 * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
											x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100
										)
									)
								)
							) AS subtotal
							
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_srv x ON a.pk = x.fk)
							INNER JOIN m_srv xd ON x.srvfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON trim(b.cmn1) = trim(c.kdsin)
						WHERE format(a.dt,'mm-yyyy')='{$periode}'

						UNION ALL

						SELECT a.pk
							, format(a.dt,'dd/mm/yyyy') AS dt 
							, a.kpbto

							, IIF(xd.grpfk=2, 'OLI', 'PART') AS grp
								
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 'YA', '') AS isklaim
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 
								x.qty * c.oli * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
								x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100
								) AS subtotal
													
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_item x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON b.cmn1 = c.kdsin
						WHERE format(a.dt,'mm-yyyy')='{$periode}'
							
							
) AS xx

) AS yy

) AS zz
GROUP BY zz.dt, zz.tp, zz.pk


) AS aa
GROUP BY aa.dt
ORDER BY val(aa.dt)

					) AS A ";


		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
       echo $sQuery;
		
		$rResult = $this->db->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}
	
	public function submit(){

	}
}

<?php

/* 
 * ***************************************************************
 * Script : pdf
 * Version : 
 * Date : 
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();
$array['periode_sebelumnya'] = month_before($array['tahun']."-".$array['periode']);

$template = array(
        'table_open'            => '<table class="table-responsive" style="padding: 0px 2px;min-width: 850px;border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);

function month_name($param){
    $date = explode("-", $param);
    $mm = strtoupper(month($date[1]));
    $yy = $date[0];
    $dm = $param."-01";
    $dd = date("t", strtotime($dm));
    return $dd." ".$mm." ".$yy;
}

function spacing($level){
    $res = "&nbsp;";
    for ($x = 1; $x <= $level; $x++) {
        $res.=$res;
    } 
    return $res;
}

$this->load->library('table');

$caption = "<b>LAPORAN NERACA</b>"
        . "<br>"
        . "<b>". strtoupper($this->apps->title)." - ". $this->session->userdata('data')['cabang'] ."</b>"
        . "<br>"
        . "<b> PER " .month_name($array['tahun']."-".$array['periode']) ."</b>"
        . "<br><br>";

// Caption text
$this->table->set_caption($caption);

$namaheader = array(
    array('data' => '<b>AKTIVA</b>'
                        , 'style' => 'text-align: center; width: 50%;font-size: 10px;'),
    array('data' => '<b>PASIVA</b>'
                        , 'style' => 'text-align: center; width: 50%;font-size: 10px;'),  
);
// Header detail
$this->table->add_row($namaheader);

if(isset($array['compare_myd'])){
    $judul = array(
        array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;">'
                        . '<div class="row">'
                        . '<div class="col-xs-6 col-md-6" style="font-size: 10px;"><b>&nbsp;&nbsp;PERIODE AKTIVA</b></div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.month_name($array['tahun']."-".$array['periode']).'</div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.month_name($array['periode_sebelumnya']).'</div>'
                        . '</div>'
                        . '</div>'
                            , 'valign' => "top"
                            , 'style' => ''),

        array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;">'
                        . '<div class="row">'
                        . '<div class="col-xs-6 col-md-6" style="font-size: 10px;"><b>&nbsp;&nbsp;PERIODE PASIVA</b></div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.month_name($array['tahun']."-".$array['periode']).'</div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.month_name($array['periode_sebelumnya']).'</div>'
                        . '</div>'
                        . '</div>'
                            ,  'valign' => "top"
                            , 'style' => ''),
    ); 
    
    $this->table->add_row($judul);
}
$akun_a = "";
$jml_a_myd = "";

$jml_a_mtd = "";

$akun_b = "";
$jml_b_myd = "";
$jml_b_mtd = "";

$div_a = "";
$div_b = "";

$sum_a_myd = 0;
$sum_b_myd = 0;

$sum_a_mtd = 0;
$sum_b_mtd = 0;
if(isset($array['compare_myd'])){
    $is_nol_header = array();
    foreach ($rptakun['myd'] as $v_myd) {
        foreach ($rptakun['mtd'] as $v_mtd) {
            if($v_myd['nmheader'] == $v_mtd['nmheader'] && ((float) $v_myd['nominal']==0 && (float) $v_mtd['nominal']==0)){
                $is_nol_header[] = $v_myd['nmheader'];
            }
        }
    }
    
    foreach ($rptakun['myd'] as $v_mtd) {
        if($v_mtd['level']==="1"){
            $bold = "text-bold";
            if($v_mtd['jenis']==="A"){
                $sum_a_myd = $sum_a_myd + $v_mtd['nominal'];
            }else{
                $sum_b_myd = $sum_b_myd + $v_mtd['nominal'];
            }
        }else{
            $bold = "";
        }
        if($v_mtd['jenis']==="A"){
            if((isset($array['is_nol']) && $array['is_nol']==true) && in_array($v_mtd['nmheader'], $is_nol_header)){
                $akun_a .= '<div class="col-xs-12 col-md-12 '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $jml_a_myd .= '<div class="col-xs-12 col-md-12 text-right  '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>'; 
            }elseif((float) $v_mtd['nominal']>0){
                $akun_a .= '<div class="col-xs-12 col-md-12 '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $jml_a_myd .= '<div class="col-xs-12 col-md-12 text-right  '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>'; 
            }
        }else{
            if((isset($array['is_nol']) && $array['is_nol']==true) && in_array($v_mtd['nmheader'], $is_nol_header)){
                $akun_b .= '<div class="col-xs-12 col-md-12  '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $jml_b_myd .= '<div class="col-xs-12 col-md-12 text-right  '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>';
            }elseif((float) $v_mtd['nominal']>0){
                $akun_b .= '<div class="col-xs-12 col-md-12  '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $jml_b_myd .= '<div class="col-xs-12 col-md-12 text-right  '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>';
            }
        }
    }
    foreach ($rptakun['mtd'] as $v_mtd) {
        if($v_mtd['level']==="1"){
            $bold = "text-bold";
            if($v_mtd['jenis']==="A"){
                $sum_a_mtd = $sum_a_mtd + $v_mtd['nominal'];
            }else{
                $sum_b_mtd = $sum_b_mtd + $v_mtd['nominal'];
            }
        }else{
            $bold = "";
        }
        if($v_mtd['jenis']==="A"){
            if((isset($array['is_nol']) && $array['is_nol']==true) && in_array($v_mtd['nmheader'], $is_nol_header)){
                $jml_a_mtd .= '<div class="col-xs-12 col-md-12 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>'; 
            }elseif((float) $v_mtd['nominal']>0){
                $jml_a_mtd .= '<div class="col-xs-12 col-md-12 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>'; 
            }
        }else{
            if((isset($array['is_nol']) && $array['is_nol']==true) && in_array($v_mtd['nmheader'], $is_nol_header)){
                $jml_b_mtd .= '<div class="col-xs-12 col-md-12 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>';
            }elseif((float) $v_mtd['nominal']>0){
                $jml_b_mtd .= '<div class="col-xs-12 col-md-12 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>';
            }
        }
    }
    $div_nama_akun_a = '<div class="col-xs-6 col-md-6"><div class="row">'.$akun_a.'</div></div>';
    $div_myd_a = '<div class="col-xs-3 col-md-3"><div class="row">'.$jml_a_myd.'</div></div>';
    $div_mtd_a = '<div class="col-xs-3 col-md-3"><div class="row">'.$jml_a_mtd.'</div></div>';
    
    $div_a = $div_nama_akun_a.$div_mtd_a.$div_myd_a;
    
    
    $div_nama_akun_b = '<div class="col-xs-6 col-md-6"><div class="row">'.$akun_b.'</div></div>';
    $div_myd_b = '<div class="col-xs-3 col-md-3"><div class="row">'.$jml_b_myd.'</div></div>';
    $div_mtd_b = '<div class="col-xs-3 col-md-3"><div class="row">'.$jml_b_mtd.'</div></div>';
    
    $div_b = $div_nama_akun_b.$div_mtd_b.$div_myd_b;
}
else{
    foreach ($rptakun['mtd'] as $v_mtd) {
        if($v_mtd['level']==="1"){
            $bold = "text-bold";
            if($v_mtd['jenis']==="A"){
                $sum_a_mtd = $sum_a_mtd + $v_mtd['nominal'];
            }else{
                $sum_b_mtd = $sum_b_mtd + $v_mtd['nominal'];
            }
        }else{
            $bold = "";
        }
        if($v_mtd['jenis']==="A"){
            if((isset($array['is_nol']) && $array['is_nol']==true) && (float) $v_mtd['nominal']==0){
                $div_a .= '<div class="col-xs-8 col-md-8 '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $div_a .= '<div class="col-xs-4 col-md-4 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>'; 
            }elseif((float) $v_mtd['nominal']!=0){
                $div_a .= '<div class="col-xs-8 col-md-8 '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $div_a .= '<div class="col-xs-4 col-md-4 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>'; 
            }
        }else{
            if((isset($array['is_nol']) && $array['is_nol']==true) && (float) $v_mtd['nominal']==0){
                $div_b .= '<div class="col-xs-8 col-md-8 '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $div_b .= '<div class="col-xs-4 col-md-4 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>';
            }elseif((float) $v_mtd['nominal']!=0){
                $div_b .= '<div class="col-xs-8 col-md-8 '.$bold.'" style="font-size: 10px;">'.spacing($v_mtd['level']).$v_mtd['nmheader'].'</div>';
                $div_b .= '<div class="col-xs-4 col-md-4 text-right '.$bold.'" style="font-size: 10px;">'.number_format($v_mtd['nominal'], 2,',','.').'</div>';
            }
        }
    }
}


$isi = array(
    array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;"><div class="row">'.$div_a.'</div></div>'
                        , 'valign' => "top"
                        , 'style' => ''),
    array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;"><div class="row">'.$div_b.'</div></div>'
                        , 'valign' => "top"
                        , 'style' => ''),
); 
$this->table->add_row($isi);



// Footer
if(isset($array['compare_myd'])){
    $footer = array(
        array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;">'
                        . '<div class="row">'
                        . '<div class="col-xs-6 col-md-6" style="font-size: 10px;"><b>&nbsp;&nbsp;TOTAL AKTIVA</b></div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.number_format($sum_a_mtd, 2,',','.').'</div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.number_format($sum_a_myd, 2,',','.').'</div>'
                        . '</div>'
                        . '</div>'
                            , 'valign' => "top"
                            , 'style' => ''),

        array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;">'
                        . '<div class="row">'
                        . '<div class="col-xs-6 col-md-6" style="font-size: 10px;"><b>&nbsp;&nbsp;TOTAL PASIVA</b></div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.number_format($sum_b_mtd, 2,',','.').'</div>'
                        . '<div class="col-xs-3 col-md-3 text-right" style="font-size: 10px;">'.number_format($sum_b_myd, 2,',','.').'</div>'
                        . '</div>'
                        . '</div>'
                            ,  'valign' => "top"
                            , 'style' => ''),
    ); 
}else{
    $footer = array(
        array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;">'
                        . '<div class="row">'
                        . '<div class="col-xs-8 col-md-8" style="font-size: 10px;"><b>&nbsp;&nbsp;TOTAL AKTIVA</b></div>'
                        . '<div class="col-xs-4 col-md-4 text-right" style="font-size: 10px;">'.number_format($sum_a_mtd, 2,',','.').'</div>'
                        . '</div>'
                        . '</div>'
                            , 'valign' => "top"
                            , 'style' => ''),

        array('data' => '<div class="content" style="min-height: 0px;padding: 0px 2px;">'
                        . '<div class="row">'
                        . '<div class="col-xs-8 col-md-8" style="font-size: 10px;"><b>&nbsp;&nbsp;TOTAL PASIVA</b></div>'
                        . '<div class="col-xs-4 col-md-4 text-right" style="font-size: 10px;">'.number_format($sum_b_mtd, 2,',','.').'</div>'
                        . '</div>'
                        . '</div>'
                            ,  'valign' => "top"
                            , 'style' => ''),
    ); 
    
}
    
$this->table->add_row($footer);

$this->table->set_template($template);
echo '<div class="content" style="padding: 0px 2px;min-width: 860px;">';
echo $this->table->generate();
echo '</div>';

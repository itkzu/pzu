<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
						, 'target' => '_blank'
						, 'method' => 'post'
                        , 'class' => "");
                    echo form_open($submit,$attributes); 
                ?> 
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label($form['periode_awal']['placeholder']);
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>'); 
							?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label($form['periode_akhir']['placeholder']);
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>'); 
							?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label($form['no_faktur_awal']['placeholder']);
								echo form_input($form['no_faktur_awal']);
								echo form_error('no_faktur_awal','<div class="note">','</div>'); 
							?>
							<label id="last_faktur" style="color: #dd4b39;"></label>
						</div>
					</div>
				</div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
					<button type="submit" class="btn btn-default btn-export-csv" name="submit" value="csv">Export CSV</button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="width: 70px;text-align: center;">TANGGAL</th>
                            <th style="text-align: center;">KODE</th>
                            <th style="text-align: center;">NO. KTP</th>
                            <th style="text-align: center;">NAMA</th>
                            <th style="text-align: center;">ALAMAT</th>
                            <th style="width: 100px;text-align: center;">JUMLAH SERVICE</th>
                            <th style="width: 100px;text-align: center;">JUMLAH PENJUALAN</th>
                            <th style="width: 100px;text-align: center;">TOTAL</th>
                            <th style="width: 100px;text-align: center;">DISKON</th>
                            <th style="width: 100px;text-align: center;">TOTAL BERSIH</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 70px;text-align: center;">TANGGAL</th>
                            <th style="text-align: center;">KODE</th>
                            <th style="text-align: center;">NO. KTP</th>
                            <th style="text-align: center;">NAMA</th>
                            <th style="text-align: center;">ALAMAT</th>
                            <th style="width: 100px;text-align: center;">JUMLAH SERVICE</th>
                            <th style="width: 100px;text-align: center;">JUMLAH PENJUALAN</th>
                            <th style="width: 100px;text-align: center;">TOTAL</th>
                            <th style="width: 100px;text-align: center;">DISKON</th>
                            <th style="width: 100px;text-align: center;">TOTAL BERSIH</th>
                        </tr>                        
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
		cek_last_faktur();
		$("#no_faktur_awal").mask("999-99-99999999");
		
		$(".btn-export-csv").click(function(){
			
		});

        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        var column = [];
        for (i = 5; i <= 9; i++) { 
            column.push({ 
                "aType":"numeric-comma",
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0');
                    // return formmatedvalue;
                    } 
                });
        }   
        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
                            ,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptbkfaktur/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
	
	function cek_last_faktur(){
		$.ajax({
			type: "POST",
			url: "<?=site_url('rptbkfaktur/cek_last_faktur');?>",
			data: {"key":"pzu"},
			success: function(resp){   
				if(resp!==""){
					$("#last_faktur").html("Nomor faktur terakhir yang digunakan adalah : " + resp);
				}
			},
			error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		});
	}
</script>
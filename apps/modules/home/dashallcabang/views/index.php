<?php
/*
 * ***************************************************************
 * Script : adh.php
 * Version :
 * Date : Nov 21, 2017 10:11:14 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    .dt-buttons{
        margin-bottom: 10px;
    }
    div.dataTables_processing { z-index: 1; }
    .nopadding {
        padding: 0 !important;
        margin: 0 !important;
        border: none;
    }
    .box{
        border-radius: 0px;
        padding-left: 7px;
        padding-right: 7px;
        border: 1px solid #d2d6de;
        margin-bottom: 0px;
    }
    .content-header{
        display: none;
    }

    .content {
        min-height: 250px;
        padding: 0px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
    .input-sm{
        height: 20px;
    }

    .table>tbody>tr>td
    , .table>tfoot>tr>td
    , .table>thead>tr>td
    {
        padding: 3px 8px;
    }
    .table{
        font-size: 12px;
    }
</style>
<!-- Main row -->
<div class="row">
    <div class="col-lg-12">
        <div class="box box-danger">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i> Periode
                                </div>
                                <?= form_input($form['periode_awal']); ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-tampil">Tampil</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                            <?= form_error('periode_awal', '<div class="note">', '</div>'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Penjualan Prima Zirang Utama <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_penjualan" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 270px;text-align: center;" colspan="3">
                                            Bulan
                                            <span id="nm_bulan"></span>
                                        </th>
                                        <th style="width: 270px;text-align: center;" colspan="3">
                                            Tahun
                                            <span id="nm_tahun"></span>
                                        </th>
                                        <th  style="text-align: center;" rowspan="2" class="sum">Qty Stock</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" class="sum">MTD</th>
                                        <th style="width: 90px;text-align: center;" class="sum">MTD-1</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Selisih</th>
                                        <th style="width: 90px;text-align: center;" class="sum">YTD</th>
                                        <th style="width: 90px;text-align: center;" class="sum">YTD-1</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Selisih</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_penjualan"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 90px;text-align: right;" id="th_1">0</th>
                                        <th style="width: 90px;text-align: right;" id="th_2">0</th>
                                        <th style="width: 90px;text-align: right;" id="th_3">0</th>
                                        <th style="width: 90px;text-align: right;" id="th_4">0</th>
                                        <th style="width: 90px;text-align: right;" id="th_5">0</th>
                                        <th style="width: 90px;text-align: right;" id="th_6">0</th>
                                        <th style="width: 90px;text-align: right;" id="th_7">0</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    
                    <div class="col-lg-12 table-penjualan-all">
                    </div>
                    
                    <div class="col-lg-12 table-penjualan-all-persen">
                    </div>


                    <div class="col-lg-6">
                        <h4><i class="fa fa-arrow-circle-up"></i> Target Penjualan Prima Zirang Utama <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_target_penjualan" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;">No.</th>
                                        <th  style="text-align: center;">Nama Cabang</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Target</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Aktual</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Profit</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Selisih</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 90px;text-align: right;" id="tht_1">0</th>
                                        <th style="width: 90px;text-align: right;" id="tht_2">0</th>
                                        <th style="width: 90px;text-align: right;" id="tht_3">0</th>
                                        <th style="width: 90px;text-align: right;" id="tht_4">0</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <h4><i class="fa fa-tags"></i> Rekap DO Belum Terima PO Leasing <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_blm_trm_po_leasing" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;">No.</th>
                                        <th  style="text-align: center;">Nama Cabang</th>
                                        <th style="width: 140px;text-align: center;">Leasing</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Jumlah DO</th>
                                        <th style="width: 90px;text-align: center;">Detail</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="text-align: center;" colspan="3">TOTAL</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">&nbsp;</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Penjualan Leasing Prima Zirang Utama <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_jual_leas" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 270px;text-align: center;" colspan="6">
                                            Bulan
                                            <span id="nm_bulan_leas"></span>
                                        </th>
<!--                                         <th style="width: 270px;text-align: center;" colspan="6">
                                            Tahun
                                            <span id="nm_tahun_leas"></span>
                                        </th>  -->
                                    </tr>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" class="sum">ADR</th>
                                        <th style="width: 90px;text-align: center;" class="sum">FIF</th>
                                        <th style="width: 90px;text-align: center;" class="sum">BCA</th>
                                        <th style="width: 90px;text-align: center;" class="sum">OTO</th>
                                        <th style="width: 90px;text-align: center;" class="sum">MUF</th>
                                        <th style="width: 90px;text-align: center;" class="sum">OTHERS</th> 
                                        <!-- <th style="width: 90px;text-align: center;" class="sum">ADR</th>
                                        <th style="width: 90px;text-align: center;" class="sum">FIF</th>
                                        <th style="width: 90px;text-align: center;" class="sum">BCA</th>
                                        <th style="width: 90px;text-align: center;" class="sum">OTO</th>
                                        <th style="width: 90px;text-align: center;" class="sum">MUF</th>
                                        <th style="width: 90px;text-align: center;" class="sum">OTHERS</th>  -->
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_jual_leas"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_1">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_2">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_3">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_4">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_5">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_6">0</th>
                                        <!-- <th style="width: 90px;text-align: right;" id="thleas_7">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_8">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_9">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_10">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_11">0</th>
                                        <th style="width: 90px;text-align: right;" id="thleas_12">0</th> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    
<!--                     <div class="col-lg-12 grafik-jualall-leas nopadding" style="border-bottom: solid 1px #ddd;">
                        <div class="col-lg-4 nopadding" style="border-bottom: solid 1px #ddd;">
                            <div class="row">
                                <div class="col-lg-7 nopadding">
                                    <div style="padding-left: 15px;">
                                        <canvas id="barJualTunaiKreditYTD" style="height:205px;"></canvas>
                                    </div>
                                </div>
                                <div class="col-lg-5 nopadding">
                                    <div style="padding: 0px 0px;">
                                        <canvas id="barJualTunaiKreditMTD" style="height:205px;"></canvas>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div class="col-lg-8 nopadding" style="border-bottom: solid 1px #ddd;">
                            <canvas id="barJualTunaiKreditPerBulan" style="height:250px;"></canvas>
                        </div>
                    </div> -->
                    
                    <div class="col-lg-12 table-pembelian-astra">
                    </div>


                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Penjualan Per Tipe Prima Zirang Utama <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_jual_tipe" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 600px;text-align: center;" colspan="10">
                                            CABANG MTD 
                                            <span id="tipe_bln_before"></span>
                                        </th> 
                                    </tr>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" class="sum1">CUB L</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">CUB M</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">CUB H</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">AT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">AT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">AT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">SPORT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">SPORT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">SPORT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">PREMIUM</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_jual_tipe"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_1">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_2">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_3">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_4">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_5">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_6">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_7">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_8">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_9">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_10">0</th>  
                                        <th style="width: 60px;text-align: right;" id="thtipe_11">0</th>  
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
<!--                         <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_jual_tp_x" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 600px;text-align: center;" colspan="10">
                                            CABANG MTD-1
                                            <span id="tipe_bln_before"></span>
                                        </th> 
                                    </tr>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" class="sum2">CUB L</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">CUB M</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">CUB H</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">AT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">AT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">AT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">SPORT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">SPORT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">SPORT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">PREMIUM</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_jual_tp_x"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex_1">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex_2">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex_3">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex_4">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex_5">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex_6">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex_7">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex_8">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex_9">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex_10">0</th>  
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_jual_tp_x_2" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 600px;text-align: center;" colspan="10">
                                            CABANG MTD-2
                                            <span id="tipe_bln_before"></span>
                                        </th> 
                                    </tr>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" class="sum2">CUB L</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">CUB M</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">CUB H</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">AT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">AT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">AT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">SPORT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">SPORT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">SPORT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum2">PREMIUM</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_jual_tp_x_2"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex2_1">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex2_2">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex2_3">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex2_4">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex2_5">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipex2_6">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex2_7">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex2_8">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex2_9">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipex2_10">0</th>  
                                    </tr>
                                </tfoot>
                            </table>
                        </div> -->
                    </div>


                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Penjualan Per Tipe Prima Zirang Utama Setahun<small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_jual_tipe_y" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 600px;text-align: center;" colspan="10">
                                            CABANG YTD 
                                            <span id="tipe_bln_before"></span>
                                        </th> 
                                    </tr>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" class="sum1">CUB L</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">CUB M</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">CUB H</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">AT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">AT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">AT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">SPORT L</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">SPORT M</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">SPORT H</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">PREMIUM</th>
                                        <th style="width: 60px;text-align: center;" class="sum1">TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_jual_tipe_y"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 60px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_1">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_2">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_3">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_4">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_5">0</th>
                                        <th style="width: 60px;text-align: right;" id="thtipe_6">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_7">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_8">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_9">0</th> 
                                        <th style="width: 60px;text-align: right;" id="thtipe_10">0</th>  
                                        <th style="width: 60px;text-align: right;" id="thtipe_11">0</th>  
                                    </tr>
                                </tfoot>
                            </table>
                        </div> 
                    </div>


                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Stock Per Segmen Prima Zirang Utama Setahun<small> Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_stok_m" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 200px;text-align: center;" colspan="3">
                                            CUB  
                                            <span id="tipe_cub"></span>
                                        </th> 
                                        <th style="width: 200px;text-align: center;" colspan="3">
                                            AT  
                                            <span id="tipe_at"></span>
                                        </th> 
                                        <th style="width: 300px;text-align: center;" colspan="4">
                                            SPORT  
                                            <span id="tipe_sport"></span>
                                        </th> 
                                        <th  style="text-align: center;" rowspan="2">TOTAL</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 70px;text-align: center;" class="sum1">CUB L</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">CUB M</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">CUB H</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">AT L</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">AT M</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">AT H</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">SPORT L</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">SPORT M</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">SPORT H</th>
                                        <th style="width: 70px;text-align: center;" class="sum1">PREMIUM</th>
                                        <!-- <th style="width: 60px;text-align: center;" class="sum1">STOCK ACTUAL</th> -->
                                    </tr>
                                </thead>
                                <tbody id="tbody_trn_stok_m"></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 70px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 70px;text-align: right;" id="thtipe_1">0</th>
                                        <th style="width: 70px;text-align: right;" id="thtipe_2">0</th>
                                        <th style="width: 70px;text-align: right;" id="thtipe_3">0</th>
                                        <th style="width: 70px;text-align: right;" id="thtipe_4">0</th>
                                        <th style="width: 70px;text-align: right;" id="thtipe_5">0</th>
                                        <th style="width: 70px;text-align: right;" id="thtipe_6">0</th> 
                                        <th style="width: 70px;text-align: right;" id="thtipe_7">0</th> 
                                        <th style="width: 70px;text-align: right;" id="thtipe_8">0</th> 
                                        <th style="width: 70px;text-align: right;" id="thtipe_9">0</th> 
                                        <th style="width: 70px;text-align: right;" id="thtipe_10">0</th>  
                                        <th style="width: 60px;text-align: right;" id="thtipe_11">0</th>  
                                    </tr>
                                </tfoot>
                            </table>
                        </div> 
                    </div>
                    
                    <!-- <div class="col-lg-12 table-jualall-leas">
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Chart -->
<div id="modal-detail-po-leasing" class="modal fade" role="dialog" style="overflow-y: scroll;">
    <div class="modal-dialog" style="width: 99%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Daftar DO Belum Terima PO Leasing</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered trn_blm_trm_po_leasing_detail" style="margin-top: 0px !important;">
                        <thead>
                            <tr>
                                <th  style="width:10px;text-align: center;">No.</th>
                                <th style="width: 140px;text-align: center;">Leasing</th>
                                <th style="width: 140px;text-align: left;">Program</th>
                                <th style="width: 90px;text-align: right;" class="sum">Pelunasan</th>
                                <th style="width: 140px;text-align: left;">No. DO</th>
                                <th style="width: 140px;text-align: left;">Tgl DO</th>
                                <th style="width: 140px;text-align: left;">Nama STNK</th>
                                <th style="width: 140px;text-align: left;">Jenis Unit</th>
                                <th style="width: 140px;text-align: left;">Warna</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align: center;" colspan="3">TOTAL</th>
                                <th style="text-align: right;">0</th>
                                <th style="text-align: right;" colspan="5">&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>

    </div>
</div>




<script src="http://cdn.datatables.net/plug-ins/1.10.20/api/processing().js"></script>

<script src="<?=base_url('assets/plugins/chartjs/Chart.bundle.js');?>"></script>
<script src="<?=base_url('assets/plugins/chartjs/utils.js');?>"></script>
<!-- FastClick -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>

<script type="text/javascript">
    // $(document).ready(function () {
    // }

    $(function () { 
        var bulan = ['','Januari','Februari','Maret','April','Mei','Juni'
                        ,'Juli','Agustus','September','Oktober','November','Desember'];

        var periode = $("#periode_awal").val();
        var mm = periode.substring(0, 2);
        var yyyy = periode.substring(3, 7);


        var bulan_awal = '01';
        var bulan_akhir = periode.substring(3, 5);
        var tahun = periode.substring(6, 10);

        // alert(bulan_akhir);
        //table penjualan leasing
        


        $(".btn-tampil").click(function () {
            var periode = $("#periode_awal").val();
            mm = periode.substring(0, 2);
            yyyy = periode.substring(3, 7);


            //reset datatable penjualan

            trn_penjualan
                .clear()
                .draw();

            $('#th_1').html('0');
            $('#th_2').html('0');
            $('#th_3').html('0');
            $('#th_4').html('0');
            $('#th_5').html('0');
            $('#th_6').html('0');
            $('#th_7').html('0');

            get_penjualan();








            //reset datatable target

            trn_target_penjualan
                .clear()
                .draw();

            $('#tht_1').html('0');
            $('#tht_2').html('0');
            $('#tht_3').html('0');
            $('#tht_4').html('0');

            get_target();







            //reset datatable po leasing

            trn_blm_trm_po_leasing
                .clear()
                .draw();

            get_blm_trm_po_leasing();


            


            //reset datatable penjualan leasing

            trn_jual_leas
                .clear()
                .draw();

            $('#thleas_1').html('0');
            $('#thleas_2').html('0');
            $('#thleas_3').html('0');
            $('#thleas_4').html('0');
            $('#thleas_5').html('0');
            $('#thleas_6').html('0');
            // $('#thleas_7').html('0');
            // $('#thleas_8').html('0');
            // $('#thleas_9').html('0');
            // $('#thleas_10').html('0');
            // $('#thleas_11').html('0');
            // $('#thleas_12').html('0');

            get_penjualan_leas();

            getChart(); 
            get_beli_astra();
            get_jual_allx();
            get_jual_all_persen();
            // trn_jual_tp_x
            //     .clear()
            //     .draw();

            // $('#thtipex_1').html('0');
            // $('#thtipex_2').html('0');
            // $('#thtipex_3').html('0');
            // $('#thtipex_4').html('0');
            // $('#thtipex_5').html('0');
            // $('#thtipex_6').html('0');
            // $('#thtipex_7').html('0');
            // $('#thtipex_8').html('0');
            // $('#thtipex_9').html('0');
            // $('#thtipex_10').html('0'); 

            // get_penjualan_tipex();



            // trn_jual_tp_x_2
            //     .clear()
            //     .draw();

            // $('#thtipex2_1').html('0');
            // $('#thtipex2_2').html('0');
            // $('#thtipex2_3').html('0');
            // $('#thtipex2_4').html('0');
            // $('#thtipex2_5').html('0');
            // $('#thtipex2_6').html('0');
            // $('#thtipex2_7').html('0');
            // $('#thtipex2_8').html('0');
            // $('#thtipex2_9').html('0');
            // $('#thtipex2_10').html('0'); 

            // get_penjualan_tipex_2();



            trn_jual_tipe
                .clear()
                .draw();

            $('#thtipe_1').html('0');
            $('#thtipe_2').html('0');
            $('#thtipe_3').html('0');
            $('#thtipe_4').html('0');
            $('#thtipe_5').html('0');
            $('#thtipe_6').html('0');
            $('#thtipe_7').html('0');
            $('#thtipe_8').html('0');
            $('#thtipe_9').html('0');
            $('#thtipe_10').html('0'); 
            $('#thtipe_11').html('0'); 

            get_penjualan_tipe();
            // get_jual_all_leas();



            trn_jual_tipe_y
                .clear()
                .draw();

            $('#thtipe_1').html('0');
            $('#thtipe_2').html('0');
            $('#thtipe_3').html('0');
            $('#thtipe_4').html('0');
            $('#thtipe_5').html('0');
            $('#thtipe_6').html('0');
            $('#thtipe_7').html('0');
            $('#thtipe_8').html('0');
            $('#thtipe_9').html('0');
            $('#thtipe_10').html('0'); 
            $('#thtipe_11').html('0'); 

            get_penjualan_tipe_year(); 



            trn_stok_m
                .clear()
                .draw();

            $('#thtipe_1').html('0');
            $('#thtipe_2').html('0');
            $('#thtipe_3').html('0');
            $('#thtipe_4').html('0');
            $('#thtipe_5').html('0');
            $('#thtipe_6').html('0');
            $('#thtipe_7').html('0');
            $('#thtipe_8').html('0');
            $('#thtipe_9').html('0');
            $('#thtipe_10').html('0'); 
            // $('#thtipe_11').html('0'); 

            get_stok_m(); 
            

        });



        // Define a plugin to provide data labels
        Chart.plugins.register({
            beforeDraw: function (chart, easing) {
                if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                    var helpers = Chart.helpers;
                    var ctx = chart.chart.ctx;
                    var chartArea = chart.chartArea;

                    ctx.save();
                    ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                    ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                    ctx.restore();
                }
            },
            afterDatasetsDraw: function(chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        if(meta.type!=="line"){
                            meta.data.forEach(function(element, index) {
                                // Draw the text in black, with the specified font
                                ctx.fillStyle = 'rgb(0, 0, 0)';

                                var fontSize = 11;
                                var fontStyle = 'normal';
                                var fontFamily = 'Source Sans Pro';
                                ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                                // Just naively convert to string for now
                                var dataString = dataset.data[index].toString();

                                // Make sure alignment settings are correct
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'middle';

                                var padding = 5;
                                var position = element.tooltipPosition();
                                ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                            });
                        }
                    }
                });
            }
        });



        trn_penjualan = $('.trn_penjualan').DataTable({
            "ordering": false,


            "columns": [
                {"data": null},
                {"data": "kddiv"},
                {"data": "mtd"},
                {"data": "mtdx"},
                {"data": "mtdiff"},
                {"data": "ytd"},
                {"data": "ytdx"},
                {"data": "ytdiff"},
                {"data": "stock"},
            ],

            "columnDefs": [
                  {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
                , {className: "text-right", "targets": [6]}
                , {className: "text-right", "targets": [7]}
                , {className: "text-right", "targets": [8]}
            ],



            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
            //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
            //               // $(row).find('td:eq(3)').css('color', 'green');
            //               $('td', nRow).css('background-color', 'white');
            //             } else {
            //               // $(row).find('td:eq(3)').css('color', 'red');
            //               $('td', nRow).css('background-color', 'Red');
            //             }
            //     },
            'rowCallback': function (row, data, index) {
            //     if(data[2]>0){
            //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
            //     }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
            "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_penjualan.on('order.dt search.dt', function () {
            trn_penjualan.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        trn_target_penjualan = $('.trn_target_penjualan').DataTable({
            "ordering": false,

            "columns": [
                {"data": null},
                {"data": "kddiv"},
                {"data": "target"},
                {"data": "aktual"},
                {"data": "gp",
                    render: function (data, type, row) {
                        return type === 'export' ? data : numeral(data).format('0,0');
                    }
                }, 
                {"data": "diff"},
            ],

            "columnDefs": [
                  {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
            ],



            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
            //             if(parseFloat(aData['target'])+parseFloat(aData['aktual'])){
            //               // $(row).find('td:eq(3)').css('color', 'green');
            //               $('td', nRow).css('background-color', 'white');
            //             } else {
            //               // $(row).find('td:eq(3)').css('color', 'red');
            //               $('td', nRow).css('background-color', 'Red');
            //             }
            //     },
            'rowCallback': function (row, data, index) {
            //     //if(data[23]){
            //     //$(row).find('td:eq(23)').css('background-color', '#ff9933');
            //     //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
            "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_target_penjualan.on('order.dt search.dt', function () {
            trn_target_penjualan.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        trn_jual_leas = $('.trn_jual_leas').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "kddiv"},
                {"data": "mtdadr"},
                {"data": "mtdfif"},
                {"data": "mtdbca"},
                {"data": "mtdoto"},
                {"data": "mtdmuf"},
                {"data": "mtdothers"},
                // {"data": "ytdadr"},
                // {"data": "ytdfif"},
                // {"data": "ytdbca"},
                // {"data": "ytdoto"},
                // {"data": "ytdmuf"},
                // {"data": "ytdothers"}
            ],

            "columnDefs": [
                  {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
                , {className: "text-right", "targets": [6]}
                , {className: "text-right", "targets": [7]}
                // , {className: "text-right", "targets": [8]}
                // , {className: "text-right", "targets": [9]}
                // , {className: "text-right", "targets": [10]}
                // , {className: "text-right", "targets": [11]}
                // , {className: "text-right", "targets": [12]}
                // , {className: "text-right", "targets": [13]}
            ],



            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
            //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
            //               // $(row).find('td:eq(3)').css('color', 'green');
            //               $('td', nRow).css('background-color', 'white');
            //             } else {
            //               // $(row).find('td:eq(3)').css('color', 'red');
            //               $('td', nRow).css('background-color', 'Red');
            //             }
            //     },
            'rowCallback': function (row, data, index) {
            //     if(data[2]>0){
            //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
            //     }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();
                api.columns('.sum', {
                    page: 'current'
                }).every(function () { 
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0); 
                    var totadr = this                
                            .column( 2 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var totfif = this                
                            .column( 3 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var totbca = this                
                            .column( 4 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tototo = this                
                            .column( 5 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var totmuf = this                
                            .column( 6 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);  
                    var totothers = this                
                            .column( 7 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   

                    // var total = this
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var baris = parseFloat(a) || 0;
                    //             var baris2 = parseFloat(b) || 0;
                    //             console.log(baris);
                    //             console.log(baris2);
                    //         }) 
                    var total = totadr+totfif+totbca+tototo+totmuf+totothers;
                    $(this.footer()).html(numeral(sum).format('0,0')+'['+ Math.round(sum / total * 100)+'%]');
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
            "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_jual_leas.on('order.dt search.dt', function () {
            trn_jual_leas.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        trn_blm_trm_po_leasing = $('.trn_blm_trm_po_leasing').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "cabang"},
                {"data": "kdleasing"},
                {"data": "count"},
                {"data": null,
                    render: function (data, type, row) {
                        var btn = '<center>' +
                                '<a class="btn btn-xs btn-info"' +
                                ' onclick="detail_po(\'' + row.cabang + '\', \'' + row.kdleasing + '\')"' +
                                ' href="javascript:void(0);">' +
                                'Detail' +
                                '</a>' +
                                '</center>';
                        return btn;
                    }
                }
            ],
            "lengthMenu": [[5, 10, 15, 20, 25, 50, -1], [5, 10, 15, 20, 25, 50, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_blm_trm_po_leasing.on('order.dt search.dt', function () {
            trn_blm_trm_po_leasing.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        trn_blm_trm_po_leasing_detail = $('.trn_blm_trm_po_leasing_detail').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "kdleasing"},
                {"data": "nmprogleas"},
                {"data": "pl",
                    render: function (data, type, row) {
                        return type === 'export' ? data : numeral(data).format('0,0');
                    }
                },
                {"data": "nodo"},
                {"data": "tgldo"},
                {"data": "nama_s"},
                {"data": "nmtipe"},
                {"data": "nmwarna"}
            ],
            "columnDefs": [
                {className: "text-right", "targets": [3]}
            ],
            "lengthMenu": [[5, 10, 15, 20, 25, 50, -1], [5, 10, 15, 20, 25, 50, "Semua Data"]],
            "bProcessing": false,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });
        trn_blm_trm_po_leasing_detail.on('order.dt search.dt', function () {
            trn_blm_trm_po_leasing_detail.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();


        trn_jual_tipe = $('.trn_jual_tipe').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "kddiv"},
                {"data": "cub_l"},
                {"data": "cub_m"},
                {"data": "cub_h"},
                {"data": "at_l"},
                {"data": "at_m"},
                {"data": "at_h"},
                {"data": "sport_l"},
                {"data": "sport_m"},
                {"data": "sport_h"},
                {"data": "prem"}, 
                {"data": "tot"}, 
            ],

            "columnDefs": [
                  {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
                , {className: "text-right", "targets": [6]}
                , {className: "text-right", "targets": [7]}
                , {className: "text-right", "targets": [8]}
                , {className: "text-right", "targets": [9]}
                , {className: "text-right", "targets": [10]} 
                , {className: "text-right", "targets": [11]}
                , {className: "text-right", "targets": [12]}
                // , {className: "text-right", "targets": [13]}
            ],



            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
            //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
            //               // $(row).find('td:eq(3)').css('color', 'green');
            //               $('td', nRow).css('background-color', 'white');
            //             } else {
            //               // $(row).find('td:eq(3)').css('color', 'red');
            //               $('td', nRow).css('background-color', 'Red');
            //             }
            //     },
            'rowCallback': function (row, data, index) {
            //     if(data[2]>0){
            //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
            //     }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();
                api.columns('.sum1', {
                    page: 'current'
                }).every(function () { 
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0); 
                    // var totadr = this                
                    //         .column( 2 )
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var x = parseFloat(a) || 0;
                    //             var y = parseFloat(b) || 0; 
                    //             return x + y;
                    //         }, 0);
                    var tot3 = this                
                            .column( 3 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot4 = this                
                            .column( 4 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot5 = this                
                            .column( 5 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot6 = this                
                            .column( 6 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);  
                    var tot7 = this                
                            .column( 7 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot8 = this                
                            .column( 8 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot9 = this                
                            .column( 9 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot10 = this                
                            .column( 10 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot11 = this                
                            .column( 11 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   

                    // var total = this
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var baris = parseFloat(a) || 0;
                    //             var baris2 = parseFloat(b) || 0;
                    //             console.log(baris);
                    //             console.log(baris2);
                    //         }) 
                    var total = tot3+tot4+tot5+tot6+tot7+tot8+tot9+tot10+tot11;
                    $(this.footer()).html(numeral(sum).format('0,0')+'['+ Math.round(sum / total * 100)+'%]');
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
            "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_jual_tipe.on('order.dt search.dt', function () {
            trn_jual_tipe.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        // trn_jual_tp_x = $('.trn_jual_tp_x').DataTable({
        //     "ordering": false,
        //     "columns": [
        //         {"data": null},
        //         {"data": "kddiv"},
        //         {"data": "cub_l"},
        //         {"data": "cub_m"},
        //         {"data": "cub_h"},
        //         {"data": "at_l"},
        //         {"data": "at_m"},
        //         {"data": "at_h"},
        //         {"data": "sport_l"},
        //         {"data": "sport_m"},
        //         {"data": "sport_h"},
        //         {"data": "prem"},
        //         // {"data": "ytdadr"},
        //         // {"data": "ytdfif"},
        //         // {"data": "ytdbca"},
        //         // {"data": "ytdoto"},
        //         // {"data": "ytdmuf"},
        //         // {"data": "ytdothers"}
        //     ],

        //     "columnDefs": [
        //           {className: "text-right", "targets": [2]}
        //         , {className: "text-right", "targets": [3]}
        //         , {className: "text-right", "targets": [4]}
        //         , {className: "text-right", "targets": [5]}
        //         , {className: "text-right", "targets": [6]}
        //         , {className: "text-right", "targets": [7]}
        //         , {className: "text-right", "targets": [8]}
        //         , {className: "text-right", "targets": [9]}
        //         , {className: "text-right", "targets": [10]} 
        //         , {className: "text-right", "targets": [11]}
        //         // , {className: "text-right", "targets": [12]}
        //         // , {className: "text-right", "targets": [13]}
        //     ], 
        //     "lengthMenu": [[-1], ["Semua Data"]],
        //     "bProcessing": true,
        //     "bServerSide": false,
        //     "bDestroy": true,
        //     "bAutoWidth": false,
        //     "aaSorting": [],
        //     // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        //     //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
        //     //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
        //     //               // $(row).find('td:eq(3)').css('color', 'green');
        //     //               $('td', nRow).css('background-color', 'white');
        //     //             } else {
        //     //               // $(row).find('td:eq(3)').css('color', 'red');
        //     //               $('td', nRow).css('background-color', 'Red');
        //     //             }
        //     //     },
        //     'rowCallback': function (row, data, index) {
        //     //     if(data[2]>0){
        //     //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
        //     //     }
        //     },
        //     "footerCallback": function (row, data, start, end, display) {
        //         var api = this.api();
        //         api.columns('.sum2', {
        //             page: 'current'
        //         }).every(function () {
        //             var sum = this
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);  
        //             var tot3 = this                
        //                     .column( 3 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);
        //             var tot4 = this                
        //                     .column( 4 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);
        //             var tot5 = this                
        //                     .column( 5 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);
        //             var tot6 = this                
        //                     .column( 6 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);  
        //             var tot7 = this                
        //                     .column( 7 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot8 = this                
        //                     .column( 8 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot9 = this                
        //                     .column( 9 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot10 = this                
        //                     .column( 10 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot11 = this                
        //                     .column( 11 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   

        //             // var total = this
        //             //         .data()
        //             //         .reduce(function (a, b) {
        //             //             var baris = parseFloat(a) || 0;
        //             //             var baris2 = parseFloat(b) || 0;
        //             //             console.log(baris);
        //             //             console.log(baris2);
        //             //         }) 
        //             var total = tot3+tot4+tot5+tot6+tot7+tot8+tot9+tot10+tot11;
        //             $(this.footer()).html(numeral(sum).format('0,0')+'['+ Math.round(sum / total * 100)+'%]');
        //         });
        //     },
        //     "oLanguage": {
        //         "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
        //     },
        //     //dom: '<"html5buttons"B>lTfgitp',
        //     buttons: [
        //         {extend: 'copy',
        //             exportOptions: {orthogonal: 'export'}},
        //         {extend: 'excel',
        //             exportOptions: {orthogonal: 'export'}},
        //         {extend: 'pdf',
        //             orientation: 'landscape',
        //         },
        //         {extend: 'print',
        //             customize: function (win) {
        //                 $(win.document.body).addClass('white-bg');
        //                 $(win.document.body).css('font-size', '10px');
        //                 $(win.document.body).find('table')
        //                         .addClass('compact')
        //                         .css('font-size', 'inherit');
        //             }
        //         }
        //     ],
        //     //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        //     "sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        //     // "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        // });

        // trn_jual_tp_x.on('order.dt search.dt', function () {
        //     trn_jual_tp_x.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
        //         cell.innerHTML = i + 1;
        //     });
        // }).draw();



        // trn_jual_tp_x_2 = $('.trn_jual_tp_x_2').DataTable({
        //     "ordering": false,
        //     "columns": [
        //         {"data": null},
        //         {"data": "kddiv"},
        //         {"data": "cub_l"},
        //         {"data": "cub_m"},
        //         {"data": "cub_h"},
        //         {"data": "at_l"},
        //         {"data": "at_m"},
        //         {"data": "at_h"},
        //         {"data": "sport_l"},
        //         {"data": "sport_m"},
        //         {"data": "sport_h"},
        //         {"data": "prem"},
        //         // {"data": "ytdadr"},
        //         // {"data": "ytdfif"},
        //         // {"data": "ytdbca"},
        //         // {"data": "ytdoto"},
        //         // {"data": "ytdmuf"},
        //         // {"data": "ytdothers"}
        //     ],

        //     "columnDefs": [
        //           {className: "text-right", "targets": [2]}
        //         , {className: "text-right", "targets": [3]}
        //         , {className: "text-right", "targets": [4]}
        //         , {className: "text-right", "targets": [5]}
        //         , {className: "text-right", "targets": [6]}
        //         , {className: "text-right", "targets": [7]}
        //         , {className: "text-right", "targets": [8]}
        //         , {className: "text-right", "targets": [9]}
        //         , {className: "text-right", "targets": [10]} 
        //         , {className: "text-right", "targets": [11]}
        //         // , {className: "text-right", "targets": [12]}
        //         // , {className: "text-right", "targets": [13]}
        //     ], 
        //     "lengthMenu": [[-1], ["Semua Data"]],
        //     "bProcessing": true,
        //     "bServerSide": false,
        //     "bDestroy": true,
        //     "bAutoWidth": false,
        //     "aaSorting": [],
        //     // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        //     //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
        //     //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
        //     //               // $(row).find('td:eq(3)').css('color', 'green');
        //     //               $('td', nRow).css('background-color', 'white');
        //     //             } else {
        //     //               // $(row).find('td:eq(3)').css('color', 'red');
        //     //               $('td', nRow).css('background-color', 'Red');
        //     //             }
        //     //     },
        //     'rowCallback': function (row, data, index) {
        //     //     if(data[2]>0){
        //     //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
        //     //     }
        //     },
        //     "footerCallback": function (row, data, start, end, display) {
        //         var api = this.api();
        //         api.columns('.sum2', {
        //             page: 'current'
        //         }).every(function () {
        //             var sum = this
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);  
        //             var tot3 = this                
        //                     .column( 3 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);
        //             var tot4 = this                
        //                     .column( 4 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);
        //             var tot5 = this                
        //                     .column( 5 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);
        //             var tot6 = this                
        //                     .column( 6 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);  
        //             var tot7 = this                
        //                     .column( 7 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot8 = this                
        //                     .column( 8 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot9 = this                
        //                     .column( 9 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot10 = this                
        //                     .column( 10 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   
        //             var tot11 = this                
        //                     .column( 11 )
        //                     .data()
        //                     .reduce(function (a, b) {
        //                         var x = parseFloat(a) || 0;
        //                         var y = parseFloat(b) || 0; 
        //                         return x + y;
        //                     }, 0);   

        //             // var total = this
        //             //         .data()
        //             //         .reduce(function (a, b) {
        //             //             var baris = parseFloat(a) || 0;
        //             //             var baris2 = parseFloat(b) || 0;
        //             //             console.log(baris);
        //             //             console.log(baris2);
        //             //         }) 
        //             var total = tot3+tot4+tot5+tot6+tot7+tot8+tot9+tot10+tot11;
        //             $(this.footer()).html(numeral(sum).format('0,0')+'['+ Math.round(sum / total * 100)+'%]');
        //         });
        //     },
        //     "oLanguage": {
        //         "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
        //     },
        //     //dom: '<"html5buttons"B>lTfgitp',
        //     buttons: [
        //         {extend: 'copy',
        //             exportOptions: {orthogonal: 'export'}},
        //         {extend: 'excel',
        //             exportOptions: {orthogonal: 'export'}},
        //         {extend: 'pdf',
        //             orientation: 'landscape',
        //         },
        //         {extend: 'print',
        //             customize: function (win) {
        //                 $(win.document.body).addClass('white-bg');
        //                 $(win.document.body).css('font-size', '10px');
        //                 $(win.document.body).find('table')
        //                         .addClass('compact')
        //                         .css('font-size', 'inherit');
        //             }
        //         }
        //     ],
        //     //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        //     "sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        //     // "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        // });

        // trn_jual_tp_x_2.on('order.dt search.dt', function () {
        //     trn_jual_tp_x_2.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
        //         cell.innerHTML = i + 1;
        //     });
        // }).draw();


        trn_jual_tipe_y = $('.trn_jual_tipe_y').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "kddiv"},
                {"data": "cub_l"},
                {"data": "cub_m"},
                {"data": "cub_h"},
                {"data": "at_l"},
                {"data": "at_m"},
                {"data": "at_h"},
                {"data": "sport_l"},
                {"data": "sport_m"},
                {"data": "sport_h"},
                {"data": "prem"}, 
                {"data": "tot"}, 
            ],

            "columnDefs": [
                  {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
                , {className: "text-right", "targets": [6]}
                , {className: "text-right", "targets": [7]}
                , {className: "text-right", "targets": [8]}
                , {className: "text-right", "targets": [9]}
                , {className: "text-right", "targets": [10]} 
                , {className: "text-right", "targets": [11]}
                , {className: "text-right", "targets": [12]}
                // , {className: "text-right", "targets": [13]}
            ],



            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
            //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
            //               // $(row).find('td:eq(3)').css('color', 'green');
            //               $('td', nRow).css('background-color', 'white');
            //             } else {
            //               // $(row).find('td:eq(3)').css('color', 'red');
            //               $('td', nRow).css('background-color', 'Red');
            //             }
            //     },
            'rowCallback': function (row, data, index) {
            //     if(data[2]>0){
            //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
            //     }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();
                api.columns('.sum1', {
                    page: 'current'
                }).every(function () { 
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0); 
                    // var totadr = this                
                    //         .column( 2 )
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var x = parseFloat(a) || 0;
                    //             var y = parseFloat(b) || 0; 
                    //             return x + y;
                    //         }, 0);
                    var tot3 = this                
                            .column( 3 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot4 = this                
                            .column( 4 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot5 = this                
                            .column( 5 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot6 = this                
                            .column( 6 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);  
                    var tot7 = this                
                            .column( 7 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot8 = this                
                            .column( 8 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot9 = this                
                            .column( 9 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot10 = this                
                            .column( 10 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot11 = this                
                            .column( 11 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    // var tot12 = this                
                    //         .column( 12 )
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var x = parseFloat(a) || 0;
                    //             var y = parseFloat(b) || 0; 
                    //             return x + y;
                    //         }, 0);   

                    // var total = this
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var baris = parseFloat(a) || 0;
                    //             var baris2 = parseFloat(b) || 0;
                    //             console.log(baris);
                    //             console.log(baris2);
                    //         }) 
                    var total = tot3+tot4+tot5+tot6+tot7+tot8+tot9+tot10+tot11;
                    $(this.footer()).html(numeral(sum).format('0,0')+'['+ Math.round(sum / total * 100)+'%]');
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
            "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_jual_tipe_y.on('order.dt search.dt', function () {
            trn_jual_tipe_y.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();


        trn_stok_m = $('.trn_stok_m').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "kddiv"},
                {"data": "cub_l"},
                {"data": "cub_m"},
                {"data": "cub_h"},
                {"data": "at_l"},
                {"data": "at_m"},
                {"data": "at_h"},
                {"data": "sport_l"},
                {"data": "sport_m"},
                {"data": "sport_h"},
                {"data": "prem"}, 
                {"data": "total"}, 
            ],

            "columnDefs": [
                  {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
                , {className: "text-right", "targets": [6]}
                , {className: "text-right", "targets": [7]}
                , {className: "text-right", "targets": [8]}
                , {className: "text-right", "targets": [9]}
                , {className: "text-right", "targets": [10]} 
                , {className: "text-right", "targets": [11]}
                , {className: "text-right", "targets": [12]}
                // , {className: "text-right", "targets": [13]}
            ],



            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //             // alert(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx']));
            //             if(parseFloat(aData['mtd'])+parseFloat(aData['mtdx'])+parseFloat(aData['ytd'])+parseFloat(aData['ytdx'])){
            //               // $(row).find('td:eq(3)').css('color', 'green');
            //               $('td', nRow).css('background-color', 'white');
            //             } else {
            //               // $(row).find('td:eq(3)').css('color', 'red');
            //               $('td', nRow).css('background-color', 'Red');
            //             }
            //     },
            'rowCallback': function (row, data, index) {
            //     if(data[2]>0){
            //       $(row).find('td:eq(23)').css('background-color', '#ff9933');
            //     }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();
                api.columns('.sum1', {
                    page: 'current'
                }).every(function () { 
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0); 
                    // var totadr = this                
                    //         .column( 2 )
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var x = parseFloat(a) || 0;
                    //             var y = parseFloat(b) || 0; 
                    //             return x + y;
                    //         }, 0);
                    var tot3 = this                
                            .column( 3 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot4 = this                
                            .column( 4 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot5 = this                
                            .column( 5 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);
                    var tot6 = this                
                            .column( 6 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);  
                    var tot7 = this                
                            .column( 7 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot8 = this                
                            .column( 8 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot9 = this                
                            .column( 9 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot10 = this                
                            .column( 10 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot11 = this                
                            .column( 11 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   
                    var tot12 = this                
                            .column( 12 )
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0; 
                                return x + y;
                            }, 0);   

                    // var total = this
                    //         .data()
                    //         .reduce(function (a, b) {
                    //             var baris = parseFloat(a) || 0;
                    //             var baris2 = parseFloat(b) || 0;
                    //             console.log(baris);
                    //             console.log(baris2);
                    //         }) 
                    var total = tot3+tot4+tot5+tot6+tot7+tot8+tot9+tot10+tot11;
                    $(this.footer()).html(numeral(sum).format('0,0')+'['+ Math.round(sum / total * 100)+'%]');
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            //"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
            "sDom": "<'row'B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });

        trn_stok_m.on('order.dt search.dt', function () {
            trn_stok_m.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        get_beli_astra();
        get_jual_allx();
        get_jual_all_persen();
        // get_jual_all_leas();
        get_penjualan();
        get_target();
        get_blm_trm_po_leasing();
        get_penjualan_leas();
        get_penjualan_tipe();
        // get_penjualan_tipex();
        // get_penjualan_tipex_2();
        get_penjualan_tipe_year();
        get_stok_m();
        // getChart();

 


        setInterval(function () {
            get_beli_astra();
            get_jual_allx();
            // get_jual_all_leas();
            get_penjualan();
            get_target();
            get_blm_trm_po_leasing();
            get_penjualan_leas();
            get_penjualan_tipe();
            // get_penjualan_tipex(); 
            // get_penjualan_tipex_2(); 
            get_penjualan_tipe_year();
            get_stok_m();
            // getChart();
        }, 500000);
    });



    function getChart(){
        var periode = $('#periode_awal').val(); 
        var p1 = periode.toString().substring(0,2); 
        var p2 = periode.toString().substring(3,5); 
        var p3 = periode.toString().substring(6,10);   
        var tgl = p3+'-'+p2; 
        getJualTunaiKreditPerBulan(tgl); 
    }

    function get_beli_astra(){
        var bulan = ['','Januari','Februari','Maret','April','Mei','Juni'
                        ,'Juli','Agustus','September','Oktober','November','Desember'];

        var periode = $("#periode_awal").val();
        var mm = periode.substring(0, 2);
        var yyyy = periode.substring(3, 7);


        var bulan_awal = '01';
        var bulan_akhir = periode.substring(3, 5);
        var tahun = periode.substring(6, 10);

        // alert(bulan_akhir);
        //table pembelian unit astra
        var jml_kolom = 0;
        $(".table-pembelian-astra").html('');
            
        var t_beli_as = '<h4><i class="fa fa-line-chart"></i>'+
                    ' Rekap Pembelian Dari ASTRA Oleh Prima Zirang Utama <small>Semua Cabang</small></h4>'+
                    '<div class="table-responsive">'+
                        '<table class="table table-hover table-bordered trn_beli_astra" style="margin-top: 0px !important;">'+
                            '<thead>'+
                                '<tr class="periode">'+
                                    '<th style="text-align: center;">Cabang</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>'+
                                '<tr class="pzusmg1">'+
                                    '<td>'+
                                        '<b>A. Yani</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pzubrebes1">'+
                                    '<td>'+
                                        '<b>Brebes</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pzukudus1">'+
                                    '<td>'+
                                        '<b>Kudus</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pzupati1">'+
                                    '<td>'+
                                        '<b>Pati</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pzupwd1">'+
                                    '<td>'+
                                        '<b>Purwodadi</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pzusmg2">'+
                                    '<td>'+
                                        '<b>Setiabudi</b>'+
                                    '</td>'+
                                '</tr>'+
                            '</tbody>'+
                            '<tfoot>'+
                                '<tr class="total-periode" style="background: #dadada;">'+
                                    '<th style="text-align: left;">Total</th>'+
                                '</tr>'+
                            '</tfoot>'+
                        '</table>';
                $(".table-pembelian-astra").html(t_beli_as);
            for (i = Number(bulan_awal); i <= Number(bulan_akhir); i++) { 
                if(i<=9){
                    xx = "0"+i;
                }else{
                    xx = i;
                }
                $(".periode").append('<th style="text-align: center;width: 90px;">'+bulan[i]+' '+tahun+'</th>');
                $(".total-periode").append('<th style="text-align: right;width: 90px;"></th>');
                $(".pzusmg1").append('<td style="text-align: right;" id="pzusmg1-'+tahun+xx+'">-</td>');
                $(".pzusmg2").append('<td style="text-align: right;" id="pzusmg2-'+tahun+xx+'">-</td>');
                $(".pzubrebes1").append('<td style="text-align: right;" id="pzubrebes1-'+tahun+xx+'">-</td>');
                $(".pzukudus1").append('<td style="text-align: right;" id="pzukudus1-'+tahun+xx+'">-</td>');
                $(".pzupati1").append('<td style="text-align: right;" id="pzupati1-'+tahun+xx+'">-</td>');
                $(".pzupwd1").append('<td style="text-align: right;" id="pzupwd1-'+tahun+xx+'">-</td>');
                jml_kolom++;
            }
            
            $(".periode").append('<th style="text-align: center;width: 90px; background: #dadada; font-weight: bold;">Total</th>');
            $(".total-periode").append('<th style="text-align: right;width: 90px;"></th>');
            $(".pzusmg1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzusmg1-total">-</td>');
            $(".pzusmg2").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzusmg2-total">-</td>');
            $(".pzubrebes1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzubrebes1-total">-</td>');
            $(".pzukudus1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzukudus1-total">-</td>');
            $(".pzupati1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzupati1-total">-</td>');
            $(".pzupwd1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pzupwd1-total">-</td>');
            
            var cbg1 = $.post("<?=site_url('android/getBeliAstra');?>",{
                "url":"http://pzusmg1.ddns.net/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg1"
                ,"kddiv":"ZPH01.01S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue(data,"pzusmg1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pzusmg1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg2 = $.post("<?=site_url('android/getBeliAstra');?>",{
                //"url":"http://pzusmg2.ddns.net"
                "url":"http://zirang.ddns.net:8080/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg2"
                ,"kddiv":"ZPH04.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pzusmg2");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pzusmg2").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg3 = $.post("<?=site_url('android/getBeliAstra');?>",{
                "url":"http://zirang.ddns.net:8085/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzubrebes1"
                ,"kddiv":"ZPH03.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pzubrebes1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pzubrebes1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg4 = $.post("<?=site_url('android/getBeliAstra');?>",{
                "url":"http://zirang.ddns.net:8082/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzukudus1"
                ,"kddiv":"ZPH02.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pzukudus1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pzukudus1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg5 = $.post("<?=site_url('android/getBeliAstra');?>",{
                "url":"http://zirang.ddns.net:8081/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupati1"
                ,"kddiv":"ZPH01.02S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pzupati1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pzupati1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg6 = $.post("<?=site_url('android/getBeliAstra');?>",{
                "url":"http://zirang.ddns.net:8083/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupwd1"
                ,"kddiv":"ZPH02.02S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue(data,"pzupwd1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pzupwd1").css("background-color", "#ef9a9a");
                }
            });
            
            $.when(cbg1, cbg2, cbg3, cbg4, cbg5, cbg6).done(function(e) { 
                    
                    var column = [];
                    for (i = 1; i <= jml_kolom + 1; i++) { 
                        column.push({ 
                            "aType":"numeric-comma",
                            "aTargets": [ i ],
                            "mRender": function (data, type, full) {
                                return type === 'export' ? data : numeral(data).format('0,0');
                                // return formmatedvalue;
                                } 
                            });
                    } 
                    $(".trn_beli_astra").DataTable({
                        "aoColumnDefs": column,
                        "lengthMenu": [[-1], ["Semua Data"]],
                        "bProcessing": false,
                        "bServerSide": false,
                        //"searching": false,
                        "bDestroy": true,
                        "bAutoWidth": false,
                            buttons: [
                                {extend: 'copy',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'csv',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'excel',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'pdf', 
                                    orientation: 'landscape',
                                },
                                {extend: 'print',
                                    customize: function (win){
                                            $(win.document.body).addClass('white-bg');
                                            $(win.document.body).css('font-size', '10px');
                                            $(win.document.body).find('table')
                                                    .addClass('compact')
                                                    .css('font-size', 'inherit');
                                    }
                                }
                            ],
                        "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> ",
                        "footerCallback": function ( row, data, start, end, display ) {
                            var api = this.api(), data;

                            // Remove the formatting to get integer data for summation
                            var intVal = function ( i ) {
                                var myNumeral = numeral(i);
                                var value = myNumeral.value();
                                
                                return typeof value === 'string' ?
                                    value.replace(/[\$,]/g, '')*1 :
                                    typeof value === 'number' ?
                                        value : 0;
                            };
                            
                            for (i = 1; i <= jml_kolom + 1; i++) { 
                                // Total over all pages
                                total = api
                                    .column( i )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                // Update footer
                                $( api.column( i ).footer() ).html(
                                     numeral(total).format('0,0')
                                );
                            }
                        }
                    });  
            });
        }



    function get_jual_allx(){
        var bulan = ['','Januari','Februari','Maret','April','Mei','Juni'
                        ,'Juli','Agustus','September','Oktober','November','Desember'];

        var periode = $("#periode_awal").val();
        var mm = periode.substring(0, 2);
        var yyyy = periode.substring(3, 7);


        var bulan_awal = '01';
        var bulan_akhir = periode.substring(3, 5);
        var tahun = periode.substring(6, 10);

        // alert(bulan_akhir);
        //table pembelian unit astra
        var jml_kolom = 0;
        $(".table-penjualan-all").html('');
            
        var t_jual_as = '<h4><i class="fa fa-line-chart"></i>'+
                    ' Rekap Penjualan Prima Zirang Utama <small>Semua Cabang</small></h4>'+
                    '<div class="table-responsive">'+
                        '<table class="table table-hover table-bordered trn_jual_all" style="margin-top: 0px !important;">'+
                            '<thead>'+
                                '<tr class="so-periode">'+
                                    '<th style="text-align: center;">Cabang</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>'+
                                '<tr class="so-pzusmg1">'+
                                    '<td>'+
                                        '<b>A. Yani</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="so-pzubrebes1">'+
                                    '<td>'+
                                        '<b>Brebes</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="so-pzukudus1">'+
                                    '<td>'+
                                        '<b>Kudus</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="so-pzupati1">'+
                                    '<td>'+
                                        '<b>Pati</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="so-pzupwd1">'+
                                    '<td>'+
                                        '<b>Purwodadi</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="so-pzusmg2">'+
                                    '<td>'+
                                        '<b>Setiabudi</b>'+
                                    '</td>'+
                                '</tr>'+
                            '</tbody>'+
                            '<tfoot>'+
                                '<tr class="so-total-periode" style="background: #dadada;">'+
                                    '<th style="text-align: left;">Total</th>'+
                                '</tr>'+
                            '</tfoot>'+
                        '</table>';
                $(".table-penjualan-all").html(t_jual_as);
            for (i = Number(bulan_awal); i <= Number(bulan_akhir); i++) { 
                if(i<=9){
                    xx = "0"+i;
                }else{
                    xx = i;
                }
                $(".so-periode").append('<th style="text-align: center;width: 90px;">'+bulan[i]+' '+tahun+'</th>');
                $(".so-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
                $(".so-pzusmg1").append('<td style="text-align: right;" id="so-pzusmg1-'+tahun+xx+'">-</td>');
                $(".so-pzusmg2").append('<td style="text-align: right;" id="so-pzusmg2-'+tahun+xx+'">-</td>');
                $(".so-pzubrebes1").append('<td style="text-align: right;" id="so-pzubrebes1-'+tahun+xx+'">-</td>');
                $(".so-pzukudus1").append('<td style="text-align: right;" id="so-pzukudus1-'+tahun+xx+'">-</td>');
                $(".so-pzupati1").append('<td style="text-align: right;" id="so-pzupati1-'+tahun+xx+'">-</td>');
                $(".so-pzupwd1").append('<td style="text-align: right;" id="so-pzupwd1-'+tahun+xx+'">-</td>');
                jml_kolom++;
            }
            
            $(".so-periode").append('<th style="text-align: center;width: 90px; background: #dadada; font-weight: bold;">Total</th>');
            $(".so-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
            $(".so-pzusmg1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="so-pzusmg1-total">-</td>');
            $(".so-pzusmg2").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="so-pzusmg2-total">-</td>');
            $(".so-pzubrebes1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="so-pzubrebes1-total">-</td>');
            $(".so-pzukudus1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="so-pzukudus1-total">-</td>');
            $(".so-pzupati1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="so-pzupati1-total">-</td>');
            $(".so-pzupwd1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="so-pzupwd1-total">-</td>');
            
            var cbg1 = $.post("<?=site_url('android/getJualAll');?>",{
                "url":"http://pzusmg1.ddns.net/"
                // "url":"localhost/pzu"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg1"
                ,"kddiv":"ZPH01.01S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue(data,"so-pzusmg1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzusmg1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg2 = $.post("<?=site_url('android/getJualAll');?>",{
                //"url":"http://pzusmg2.ddns.net"
                "url":"http://zirang.ddns.net:8080/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg2"
                ,"kddiv":"ZPH04.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"so-pzusmg2");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzusmg2").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg3 = $.post("<?=site_url('android/getJualAll');?>",{
                "url":"http://zirang.ddns.net:8085/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzubrebes1"
                ,"kddiv":"ZPH03.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"so-pzubrebes1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzubrebes1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg4 = $.post("<?=site_url('android/getJualAll');?>",{
                "url":"http://zirang.ddns.net:8082/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzukudus1"
                ,"kddiv":"ZPH02.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"so-pzukudus1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzukudus1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg5 = $.post("<?=site_url('android/getJualAll');?>",{
                "url":"http://zirang.ddns.net:8081/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupati1"
                ,"kddiv":"ZPH01.02S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"so-pzupati1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzupati1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg6 = $.post("<?=site_url('android/getJualAll');?>",{
                "url":"http://zirang.ddns.net:8083/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupwd1"
                ,"kddiv":"ZPH02.02S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue(data,"so-pzupwd1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzupwd1").css("background-color", "#ef9a9a");
                }
            });
 
            $.when(cbg1, cbg2, cbg3, cbg4, cbg5, cbg6).done(function(e) { 
                    
                    var column = [];
                    for (i = 1; i <= jml_kolom + 1; i++) { 
                        column.push({ 
                            "aType":"numeric-comma",
                            "aTargets": [ i ],
                            "mRender": function (data, type, full) {
                                return type === 'export' ? data : numeral(data).format('0,0');
                                // return formmatedvalue;
                                } 
                            });
                    } 
                    $(".trn_jual_all").DataTable({
                        "aoColumnDefs": column,
                        "lengthMenu": [[-1], ["Semua Data"]],
                        "bProcessing": false,
                        "bServerSide": false,
                        //"searching": false,
                        "bDestroy": true,
                        "bAutoWidth": false,
                            buttons: [
                                {extend: 'copy',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'csv',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'excel',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'pdf', 
                                    orientation: 'landscape',
                                },
                                {extend: 'print',
                                    customize: function (win){
                                            $(win.document.body).addClass('white-bg');
                                            $(win.document.body).css('font-size', '10px');
                                            $(win.document.body).find('table')
                                                    .addClass('compact')
                                                    .css('font-size', 'inherit');
                                    }
                                }
                            ],
                        "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> ",
                        "footerCallback": function ( row, data, start, end, display ) {
                            var api = this.api(), data;

                            // Remove the formatting to get integer data for summation
                            var intVal = function ( i ) {
                                var myNumeral = numeral(i);
                                var value = myNumeral.value();
                                
                                return typeof value === 'string' ?
                                    value.replace(/[\$,]/g, '')*1 :
                                    typeof value === 'number' ?
                                        value : 0;
                            };
                            
                            for (i = 1; i <= jml_kolom + 1; i++) { 
                                // Total over all pages
                                // alert(i);
                                total = api
                                    .column( i )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                // Update footer
                                $( api.column( i ).footer() ).html(
                                     numeral(total).format('0,0')
                                     // numeral(i).format('0,0')
                                );
                            }
                        }
                    });  
            });
        }

    function get_jual_all_persen(){
        var bulan = ['','Januari','Februari','Maret','April','Mei','Juni'
                        ,'Juli','Agustus','September','Oktober','November','Desember'];

        var periode = $("#periode_awal").val();
        var mm = periode.substring(0, 2);
        var yyyy = periode.substring(3, 7);


        var bulan_awal = '01';
        var bulan_akhir = periode.substring(3, 5);
        var tahun = periode.substring(6, 10);

        // alert(bulan_akhir);
        //table pembelian unit astra
        var jml_kolom = 0;
        $(".table-penjualan-all-persen").html('');
            
        var t_jual_as_persen = '<h4><i class="fa fa-line-chart"></i>'+
                    ' Rekap Penjualan Prima Zirang Utama Persentase <small>Semua Cabang</small></h4>'+
                    '<div class="table-responsive">'+
                        '<table class="table table-hover table-bordered trn_jual_all_persen" style="margin-top: 0px !important;">'+
                            '<thead>'+
                                '<tr class="pso-periode">'+
                                    '<th style="text-align: center;">Cabang</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>'+
                                '<tr class="pso-pzusmg1">'+
                                    '<td>'+
                                        '<b>A. Yani</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pso-pzubrebes1">'+
                                    '<td>'+
                                        '<b>Brebes</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pso-pzukudus1">'+
                                    '<td>'+
                                        '<b>Kudus</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pso-pzupati1">'+
                                    '<td>'+
                                        '<b>Pati</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pso-pzupwd1">'+
                                    '<td>'+
                                        '<b>Purwodadi</b>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr class="pso-pzusmg2">'+
                                    '<td>'+
                                        '<b>Setiabudi</b>'+
                                    '</td>'+
                                '</tr>'+
                            '</tbody>'+
                            '<tfoot>'+
                                '<tr class="pso-total-periode" style="background: #dadada;">'+
                                    '<th style="text-align: left;">Total</th>'+
                                '</tr>'+
                            '</tfoot>'+
                        '</table>';
                $(".table-penjualan-all-persen").html(t_jual_as_persen);
            for (i = Number(bulan_awal); i <= Number(bulan_akhir); i++) { 
                if(i<=9){
                    xx = "0"+i;
                }else{
                    xx = i;
                }
                $(".pso-periode").append('<th style="text-align: center;width: 90px;">'+bulan[i]+' '+tahun+' Tunai</th>');
                $(".pso-periode").append('<th style="text-align: center;width: 90px;">'+bulan[i]+' '+tahun+' Kredit</th>');
                $(".pso-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
                $(".pso-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
                $(".pso-pzusmg1").append('<td style="text-align: right;" id="pso-pzusmg1-'+tahun+xx+'-t">-</td>');
                $(".pso-pzusmg1").append('<td style="text-align: right;" id="pso-pzusmg1-'+tahun+xx+'-k">-</td>');
                $(".pso-pzusmg2").append('<td style="text-align: right;" id="pso-pzusmg2-'+tahun+xx+'-t">-</td>');
                $(".pso-pzusmg2").append('<td style="text-align: right;" id="pso-pzusmg2-'+tahun+xx+'-k">-</td>');
                $(".pso-pzubrebes1").append('<td style="text-align: right;" id="pso-pzubrebes1-'+tahun+xx+'-t">-</td>');
                $(".pso-pzubrebes1").append('<td style="text-align: right;" id="pso-pzubrebes1-'+tahun+xx+'-k">-</td>');
                $(".pso-pzukudus1").append('<td style="text-align: right;" id="pso-pzukudus1-'+tahun+xx+'-t">-</td>');
                $(".pso-pzukudus1").append('<td style="text-align: right;" id="pso-pzukudus1-'+tahun+xx+'-k">-</td>');
                $(".pso-pzupati1").append('<td style="text-align: right;" id="pso-pzupati1-'+tahun+xx+'-t">-</td>');
                $(".pso-pzupati1").append('<td style="text-align: right;" id="pso-pzupati1-'+tahun+xx+'-k">-</td>');
                $(".pso-pzupwd1").append('<td style="text-align: right;" id="pso-pzupwd1-'+tahun+xx+'-t">-</td>');
                $(".pso-pzupwd1").append('<td style="text-align: right;" id="pso-pzupwd1-'+tahun+xx+'-k">-</td>');
                jml_kolom++;
            }
            
            $(".pso-periode").append('<th style="text-align: center;width: 90px; background: #dadada; font-weight: bold;">Total Tunai</th>');
            $(".pso-periode").append('<th style="text-align: center;width: 90px; background: #dadada; font-weight: bold;">Total Kredit</th>');
            $(".pso-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
            $(".pso-total-periode").append('<th style="text-align: right;width: 90px;"></th>')
;           $(".pso-pzusmg1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzusmg1-total">-</td>');
            $(".pso-pzusmg1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzusmg1-total-k">-</td>');
            $(".pso-pzusmg2").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzusmg2-total">-</td>');
            $(".pso-pzusmg2").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzusmg2-total-k">-</td>');
            $(".pso-pzubrebes1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzubrebes1-total">-</td>');
            $(".pso-pzubrebes1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzubrebes1-total-k">-</td>');
            $(".pso-pzukudus1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzukudus1-total">-</td>');
            $(".pso-pzukudus1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzukudus1-total-k">-</td>');
            $(".pso-pzupati1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzupati1-total">-</td>');
            $(".pso-pzupati1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzupati1-total-k">-</td>');
            $(".pso-pzupwd1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzupwd1-total">-</td>');
            $(".pso-pzupwd1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="pso-pzupwd1-total-k">-</td>');
            
            var cbg1 = $.post("<?=site_url('android/getJualAllPersen');?>",{
                "url":"http://pzusmg1.ddns.net/"
                // "url":"localhost/pzu"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg1"
                ,"kddiv":"ZPH01.01S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue(data,"pso-pzusmg1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pso-pzusmg1").css("background-color", "#ef9a9a");
                }
            });

            var cbg11 = $.post("<?=site_url('android/getJualAllPersenK');?>",{
                "url":"http://pzusmg1.ddns.net/"
                // "url":"localhost/pzu"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg1"
                ,"kddiv":"ZPH01.01S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue2(data,"pso-pzusmg1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#pso-pzusmg1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg2 = $.post("<?=site_url('android/getJualAllPersen');?>",{
                //"url":"http://pzusmg2.ddns.net"
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8080/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg2"
                ,"kddiv":"ZPH04.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pso-pzusmg2");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzusmg2").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg22 = $.post("<?=site_url('android/getJualAllPersenK');?>",{
                //"url":"http://pzusmg2.ddns.net"
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8080/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzusmg2"
                ,"kddiv":"ZPH04.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue2(data,"pso-pzusmg2");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzusmg2").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg3 = $.post("<?=site_url('android/getJualAllPersen');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8085/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzubrebes1"
                ,"kddiv":"ZPH03.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pso-pzubrebes1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzubrebes1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg33 = $.post("<?=site_url('android/getJualAllPersenK');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8085/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzubrebes1"
                ,"kddiv":"ZPH03.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue2(data,"pso-pzubrebes1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzubrebes1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg4 = $.post("<?=site_url('android/getJualAllPersen');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8082/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzukudus1"
                ,"kddiv":"ZPH02.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pso-pzukudus1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzukudus1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg44 = $.post("<?=site_url('android/getJualAllPersenK');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8082/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzukudus1"
                ,"kddiv":"ZPH02.01S"
            },
            function(data, status){
                if(status==="success"){
                    setValue2(data,"pso-pzukudus1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzukudus1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg5 = $.post("<?=site_url('android/getJualAllPersen');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8081/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupati1"
                ,"kddiv":"ZPH01.02S"
            },
            function(data, status){
                if(status==="success"){
                    setValue(data,"pso-pzupati1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzupati1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg55 = $.post("<?=site_url('android/getJualAllPersenK');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8081/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupati1"
                ,"kddiv":"ZPH01.02S"
            },
            function(data, status){
                if(status==="success"){
                    setValue2(data,"pso-pzupati1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzupati1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg6 = $.post("<?=site_url('android/getJualAllPersen');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8083/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupwd1"
                ,"kddiv":"ZPH02.02S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue(data,"pso-pzupwd1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzupwd1").css("background-color", "#ef9a9a");
                }
            });
            
            var cbg66 = $.post("<?=site_url('android/getJualAllPersenK');?>",{
                // "url":"localhost/pzu"
                "url":"http://zirang.ddns.net:8083/"
                ,"bulan_awal":bulan_awal
                ,"bulan_akhir":bulan_akhir
                ,"tahun":tahun 
                ,"idcabang":"pzupwd1"
                ,"kddiv":"ZPH02.02S"
            },
            function(data, status){
                // alert(data);
                if(status==="success"){
                    setValue2(data,"pso-pzupwd1");
                }else{
                    alert("Data: " + data + "\nStatus: " + status);
                    $("#so-pzupwd1").css("background-color", "#ef9a9a");
                }
            });
            var jml_kolom = (jml_kolom * 2) + 1;
 
            $.when(cbg1, cbg11, cbg2, cbg22, cbg3, cbg33, cbg4, cbg44, cbg5, cbg55, cbg6, cbg66).done(function(e) { 
                    
                    var column = [];
                    for (i = 1; i <= jml_kolom + 1; i++) { 
                        column.push({ 
                            "aType":"numeric-comma",
                            "aTargets": [ i ],
                            "mRender": function (data, type, full) {
                                return type === 'export' ? data : numeral(data).format('0,0');
                                // return formmatedvalue;
                                } 
                            });
                    } 
                    $(".trn_jual_all_persen").DataTable({
                        "aoColumnDefs": column,
                        "lengthMenu": [[-1], ["Semua Data"]],
                        "bProcessing": false,
                        "bServerSide": false,
                        //"searching": false,
                        "bDestroy": true,
                        "bAutoWidth": false,
                            buttons: [
                                {extend: 'copy',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'csv',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'excel',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'pdf', 
                                    orientation: 'landscape',
                                },
                                {extend: 'print',
                                    customize: function (win){
                                            $(win.document.body).addClass('white-bg');
                                            $(win.document.body).css('font-size', '10px');
                                            $(win.document.body).find('table')
                                                    .addClass('compact')
                                                    .css('font-size', 'inherit');
                                    }
                                }
                            ],
                        "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> ",
                        "footerCallback": function ( row, data, start, end, display ) {
                            var api = this.api(), data;

                            // Remove the formatting to get integer data for summation
                            var intVal = function ( i ) {
                                var myNumeral = numeral(i);
                                var value = myNumeral.value();
                                
                                return typeof value === 'string' ?
                                    value.replace(/[\$,]/g, '')*1 :
                                    typeof value === 'number' ?
                                        value : 0;
                            }; 
                            
                            for (i = 1; i <= jml_kolom + 1; i++) { 
                                // Total over all pages
                                total = api
                                    .column( i )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                // Update footer
                                $( api.column( i ).footer() ).html(
                                     numeral(total).format('0,0')
                                     // numeral(i).format('0,0')
                                );
                            }
                        }
                    });  
            });
        }


    // function get_jual_all_leas(){
    //     var bulan = ['','Januari','Februari','Maret','April','Mei','Juni'
    //                     ,'Juli','Agustus','September','Oktober','November','Desember'];

    //     var periode = $("#periode_awal").val();
    //     var mm = periode.substring(0, 2);
    //     var yyyy = periode.substring(3, 7);


    //     var bulan_awal = '01';
    //     var bulan_akhir = periode.substring(3, 5);
    //     var tahun = periode.substring(6, 10);
    //     var jmlleas_kolom = 0;
    //     $(".table-jualall-leas").html('');
            
    //     var t_jualall_as = '<h4><i class="fa fa-line-chart"></i>'+
    //                 ' Rekap Penjualan Leasing Oleh Prima Zirang Utama <small>Semua Cabang</small></h4>'+
    //                 '<div class="table-responsive">'+
    //                     '<table class="table table-hover table-bordered trn_jualall_leas" style="margin-top: 0px !important;">'+
    //                         '<thead>'+
    //                             '<tr class="leas-periode">'+
    //                                 '<th style="text-align: center;">Cabang</th>'+
    //                             '</tr>'+
    //                         '</thead>'+
    //                         '<tbody>'+
    //                             '<tr class="leas-pzusmg1">'+
    //                                 '<td>'+
    //                                     '<b>A. Yani</b>'+
    //                                 '</td>'+
    //                             '</tr>'+
    //                             '<tr class="leas-pzubrebes1">'+
    //                                 '<td>'+
    //                                     '<b>Brebes</b>'+
    //                                 '</td>'+
    //                             '</tr>'+
    //                             '<tr class="leas-pzukudus1">'+
    //                                 '<td>'+
    //                                     '<b>Kudus</b>'+
    //                                 '</td>'+
    //                             '</tr>'+
    //                             '<tr class="leas-pzupati1">'+
    //                                 '<td>'+
    //                                     '<b>Pati</b>'+
    //                                 '</td>'+
    //                             '</tr>'+
    //                             '<tr class="leas-pzupwd1">'+
    //                                 '<td>'+
    //                                     '<b>Purwodadi</b>'+
    //                                 '</td>'+
    //                             '</tr>'+
    //                             '<tr class="leas-pzusmg2">'+
    //                                 '<td>'+
    //                                     '<b>Setiabudi</b>'+
    //                                 '</td>'+
    //                             '</tr>'+
    //                         '</tbody>'+
    //                         '<tfoot>'+
    //                             '<tr class="leas-total-periode" style="background: #dadada;">'+
    //                                 '<th style="text-align: left;">Total</th>'+
    //                             '</tr>'+
    //                         '</tfoot>'+
    //                     '</table>';
    //             $(".table-jualall-leas").html(t_jualall_as);
    //         for (i = Number(bulan_awal); i <= Number(bulan_akhir); i++) { 
    //             if(i<=9){
    //                 xx = "0"+i;
    //             }else{
    //                 xx = i;
    //             }
    //             $(".leas-periode").append('<th style="text-align: center;width: 90px;">'+bulan[i]+' '+tahun+'</th>');
    //             $(".leas-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
    //             $(".leas-pzusmg1").append('<td style="text-align: right;" id="l_pzusmg1-'+tahun+xx+'">-</td>');
    //             $(".leas-pzusmg2").append('<td style="text-align: right;" id="l_pzusmg2-'+tahun+xx+'">-</td>');
    //             $(".leas-pzubrebes1").append('<td style="text-align: right;" id="l_pzubrebes1-'+tahun+xx+'">-</td>');
    //             $(".leas-pzukudus1").append('<td style="text-align: right;" id="l_pzukudus1-'+tahun+xx+'">-</td>');
    //             $(".leas-pzupati1").append('<td style="text-align: right;" id="l_pzupati1-'+tahun+xx+'">-</td>');
    //             $(".leas-pzupwd1").append('<td style="text-align: right;" id="l_pzupwd1-'+tahun+xx+'">-</td>');
    //             jmlleas_kolom++; 
    //         }   
            
    //         $(".leas-periode").append('<th style="text-align: center;width: 90px; background: #dadada; font-weight: bold;">Total</th>');
    //         $(".leas-total-periode").append('<th style="text-align: right;width: 90px;"></th>');
    //         $(".leas-pzusmg1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="leas_pzusmg1-total">-</td>');
    //         $(".leas-pzusmg2").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="leas_pzusmg2-total">-</td>');
    //         $(".leas-pzubrebes1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="leas_pzubrebes1-total">-</td>');
    //         $(".leas-pzukudus1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="leas_pzukudus1-total">-</td>');
    //         $(".leas-pzupati1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="leas_pzupati1-total">-</td>');
    //         $(".leas-pzupwd1").append('<td style="text-align: right; background: #dadada; font-weight: bold;" id="leas_pzupwd1-total">-</td>');
            
    //         var cbg_leas1 = $.post("<?=site_url('android/getJualLeas');?>",{
    //             "url":"http://pzusmg1.ddns.net/"
    //             // "url":"http://localhost/pzu/"
    //             ,"bulan_awal":bulan_awal
    //             ,"bulan_akhir":bulan_akhir
    //             ,"tahun":tahun 
    //             ,"idcabang":"pzusmg1"
    //             ,"kddiv":"ZPH01.01S"
    //         },
    //         function(data, status){
    //             // alert(data);
    //             if(status==="success"){
    //                 setValue2(data,"pzusmg1");
    //             }else{
    //                 alert("Data: " + data + "\nStatus: " + status);
    //                 $("#pzusmg1").css("background-color", "#ef9a9a");
    //             }
    //         });
            
    //         var cbg_leas2 = $.post("<?=site_url('android/getJualLeas');?>",{
    //             //"url":"http://pzusmg2.ddns.net"
    //             // "url":"http://localhost/pzu/"
    //             "url":"http://zirang.ddns.net:8080/"
    //             ,"bulan_awal":bulan_awal
    //             ,"bulan_akhir":bulan_akhir
    //             ,"tahun":tahun 
    //             ,"idcabang":"pzusmg2"
    //             ,"kddiv":"ZPH04.01S"
    //         },
    //         function(data, status){
    //             if(status==="success"){
    //                 setValue2(data,"pzusmg2");
    //             }else{
    //                 alert("Data: " + data + "\nStatus: " + status);
    //                 $("#pzusmg2").css("background-color", "#ef9a9a");
    //             }
    //         });
            
    //         var cbg_leas3 = $.post("<?=site_url('android/getJualLeas');?>",{
    //             "url":"http://zirang.ddns.net:8085/"
    //             // "url":"http://localhost/pzu/"
    //             ,"bulan_awal":bulan_awal
    //             ,"bulan_akhir":bulan_akhir
    //             ,"tahun":tahun 
    //             ,"idcabang":"pzubrebes1"
    //             ,"kddiv":"ZPH03.01S"
    //         },
    //         function(data, status){
    //             if(status==="success"){
    //                 setValue2(data,"pzubrebes1");
    //             }else{
    //                 alert("Data: " + data + "\nStatus: " + status);
    //                 $("#pzubrebes1").css("background-color", "#ef9a9a");
    //             }
    //         });
            
    //         var cbg_leas4 = $.post("<?=site_url('android/getJualLeas');?>",{
    //             "url":"http://zirang.ddns.net:8082/"
    //             // "url":"http://localhost/pzu/"
    //             ,"bulan_awal":bulan_awal
    //             ,"bulan_akhir":bulan_akhir
    //             ,"tahun":tahun 
    //             ,"idcabang":"pzukudus1"
    //             ,"kddiv":"ZPH02.01S"
    //         },
    //         function(data, status){
    //             if(status==="success"){
    //                 setValue2(data,"pzukudus1");
    //             }else{
    //                 alert("Data: " + data + "\nStatus: " + status);
    //                 $("#pzukudus1").css("background-color", "#ef9a9a");
    //             }
    //         });
            
    //         var cbg_leas5 = $.post("<?=site_url('android/getJualLeas');?>",{
    //             "url":"http://zirang.ddns.net:8081/"
    //             // "url":"http://localhost/pzu/"
    //             ,"bulan_awal":bulan_awal
    //             ,"bulan_akhir":bulan_akhir
    //             ,"tahun":tahun 
    //             ,"idcabang":"pzupati1"
    //             ,"kddiv":"ZPH01.02S"
    //         },
    //         function(data, status){
    //             if(status==="success"){
    //                 setValue2(data,"pzupati1");
    //             }else{
    //                 alert("Data: " + data + "\nStatus: " + status);
    //                 $("#pzupati1").css("background-color", "#ef9a9a");
    //             }
    //         });
            
    //         var cbg_leas6 = $.post("<?=site_url('android/getJualLeas');?>",{
    //             "url":"http://zirang.ddns.net:8083/"
    //             // "url":"http://localhost/pzu/"
    //             ,"bulan_awal":bulan_awal
    //             ,"bulan_akhir":bulan_akhir
    //             ,"tahun":tahun 
    //             ,"idcabang":"pzupwd1"
    //             ,"kddiv":"ZPH02.02S"
    //         },
    //         function(data, status){
    //             // alert(data);
    //             if(status==="success"){
    //                 setValue2(data,"pzupwd1");
    //             }else{
    //                 alert("Data: " + data + "\nStatus: " + status);
    //                 $("#pzupwd1").css("background-color", "#ef9a9a");
    //             }
    //         });
            
    //         $.when(cbg_leas1, cbg_leas2, cbg_leas3, cbg_leas4, cbg_leas5, cbg_leas6).done(function(e) { 
                    
    //                 var column = [];
    //                 for (i = 1; i <= jmlleas_kolom + 1; i++) { 
    //                     column.push({ 
    //                         "aType":"numeric-comma",
    //                         "aTargets": [ i ],
    //                         "mRender": function (data, type, full) {
    //                             return type === 'export' ? data : numeral(data).format('0,0');
    //                             // return formmatedvalue;
    //                             } 
    //                         });
    //                 } 
    //                 $(".trn_jualall_leas").DataTable({
    //                     "aoColumnDefs": column,
    //                     "lengthMenu": [[-1], ["Semua Data"]],
    //                     "bProcessing": false,
    //                     "bServerSide": false,
    //                     //"searching": false,
    //                     "bDestroy": true,
    //                     "bAutoWidth": false,
    //                         buttons: [
    //                             {extend: 'copy',
    //                                 exportOptions: {orthogonal: 'export'}},
    //                             {extend: 'csv',
    //                                 exportOptions: {orthogonal: 'export'}},
    //                             {extend: 'excel',
    //                                 exportOptions: {orthogonal: 'export'}},
    //                             {extend: 'pdf', 
    //                                 orientation: 'landscape',
    //                             },
    //                             {extend: 'print',
    //                                 customize: function (win){
    //                                         $(win.document.body).addClass('white-bg');
    //                                         $(win.document.body).css('font-size', '10px');
    //                                         $(win.document.body).find('table')
    //                                                 .addClass('compact')
    //                                                 .css('font-size', 'inherit');
    //                                 }
    //                             }
    //                         ],
    //                     "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> ",
    //                     "footerCallback": function ( row, data, start, end, display ) {
    //                         var api = this.api(), data;

    //                         // Remove the formatting to get integer data for summation
    //                         var intVal = function ( i ) {
    //                             var myNumeral = numeral(i);
    //                             var value = myNumeral.value();
                                
    //                             return typeof value === 'string' ?
    //                                 value.replace(/[\$,]/g, '')*1 :
    //                                 typeof value === 'number' ?
    //                                     value : 0;
    //                         };
                            
    //                         for (i = 1; i <= jmlleas_kolom + 1; i++) { 
    //                             // Total over all pages
    //                             total = api
    //                                 .column( i )
    //                                 .data()
    //                                 .reduce( function (a, b) {
    //                                     return intVal(a) + intVal(b);
    //                                 }, 0 );

    //                             // Update footer
    //                             $( api.column( i ).footer() ).html(
    //                                  numeral(total).format('0,0')
    //                             );
    //                         }
    //                     }
    //                 });  
    //         });
    //     }

    function get_penjualan() {
        var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_penjualan'); ?>",
            data: {"periode": periode},
            beforeSend: function () {
                trn_penjualan.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_penjualan.clear().draw();
                $.each(obj, function (key, data) {
                    trn_penjualan.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_penjualan.processing( false );
            }
        });
    }




    function get_target() {
        var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_target_penjualan'); ?>",
            data: {"periode": periode},
            beforeSend: function () {
                trn_target_penjualan.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_target_penjualan.clear().draw();
                $.each(obj, function (key, data) {
                    trn_target_penjualan.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_target_penjualan.processing( false );
            }
        });
    }



    function get_blm_trm_po_leasing() {
        var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_blm_trm_po_leasing'); ?>",
            data: {"periode": periode},
            beforeSend: function () {
                trn_blm_trm_po_leasing.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_blm_trm_po_leasing.clear().draw();
                $.each(obj, function (key, data) {
                    trn_blm_trm_po_leasing.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_blm_trm_po_leasing.processing( false );
            }
        });
    }

    function detail_po(cabang, kdleasing) {
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_blm_trm_po_leasing_detail'); ?>",
            data: {"cabang": cabang
                ,"kdleasing": kdleasing},
            beforeSend: function () {
                trn_blm_trm_po_leasing_detail.clear().draw();
                $("#modal-detail-po-leasing").modal("show");
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_blm_trm_po_leasing_detail.rows.add(obj).draw();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
 

    function get_penjualan_leas() {
        var periode = $("#periode_awal").val(); 
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_penjualan_leas'); ?>",
            data: {"periode": periode},
            beforeSend: function () {
                trn_jual_leas.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_jual_leas.clear().draw();
                $.each(obj, function (key, data) {
                    trn_jual_leas.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_jual_leas.processing( false );
            }
        });
    }
 

    function get_penjualan_tipe() {
        var periode = $("#periode_awal").val();
        var p1 = periode.toString().substring(0,2); 
        var p2 = periode.toString().substring(3,5); 
        var p3 = periode.toString().substring(6,10);  
        if(p2>9){
            var tgl = p3+'-'+p2+'-'+p1;
        } else {
            var tgl = p3+'-'+p2+'-'+p1;
        }  
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_penjualan_tipe'); ?>",
            data: {"periode": tgl},
            beforeSend: function () {
                trn_jual_tipe.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_jual_tipe.clear().draw();
                $.each(obj, function (key, data) {
                    trn_jual_tipe.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_jual_tipe.processing( false );
            }
        });
    }
 

    function get_stok_m() {
        var periode = $("#periode_awal").val();
        var p1 = periode.toString().substring(0,2); 
        var p2 = periode.toString().substring(3,5); 
        var p3 = periode.toString().substring(6,10);  
        if(p2>9){
            var tgl = p3+'-'+p2+'-'+p1;
        } else {
            var tgl = p3+'-'+p2+'-'+p1;
        }  
        // alert(tgl);
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_stok_m'); ?>",
            data: {"periode": tgl},
            beforeSend: function () {
                trn_stok_m.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_stok_m.clear().draw();
                $.each(obj, function (key, data) {
                    trn_stok_m.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_stok_m.processing( false );
            }
        });
    }
 

    // function get_penjualan_tipex() {
    //     var periode = $("#periode_awal").val(); 
    //     var p1 = periode.toString().substring(0,2); 
    //     var p2 = periode.toString().substring(3,5); 
    //     var p3 = periode.toString().substring(6,10);  
    //     if(p2>9){
    //         var tgl = p3+'-'+p2+'-'+p1;
    //     } else {
    //         var tgl = p3+'-'+p2+'-'+p1;
    //     } 
    //     $.ajax({
    //         type: "POST",
    //         url: "<?= site_url('dashallcabang/get_penjualan_tipex'); ?>",
    //         data: {"periode": tgl},
    //         beforeSend: function () {
    //             trn_jual_tp_x.processing( true );
    //         },
    //         success: function (resp) {
    //             var obj = jQuery.parseJSON(resp);
    //             trn_jual_tp_x.clear().draw();
    //             $.each(obj, function (key, data) {
    //                 trn_jual_tp_x.rows.add(data).draw();
    //                 ;
    //             });
    //         },
    //         error: function (event, textStatus, errorThrown) {
    //             console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //         },
    //         complete: function(){
    //             trn_jual_tp_x.processing( false );
    //         }
    //     });
    // }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;

        return [year, month, day].join('-');
    }
 

    // function get_penjualan_tipex_2() {
    //     var periode = $("#periode_awal").val();
    //     var p1 = periode.toString().substring(0,2); 
    //     var p2 = periode.toString().substring(3,5); 
    //     var p3 = periode.toString().substring(6,10); 
    //     var p21 = p2 - 1;
    //     if(p21>9){
    //         var tgl = p3+'-'+p21+'-'+'01';
    //         var tgl0 = p3+'-'+p2+'-'+'01';
    //     } else {
    //         var tgl = p3+'-0'+p21+'-'+'01';
    //         var tgl0 = p3+'-'+p2+'-'+'01';
    //     } 
    //     var tgl00 = formatDate(new Date(new Date(tgl0).getTime()-(1*24*60*60*1000)));
    //     // alert(tgl00);
    //     $.ajax({
    //         type: "POST",
    //         url: "<?= site_url('dashallcabang/get_penjualan_tipex'); ?>",
    //         data: {"periode": tgl00},
    //         beforeSend: function () {
    //             trn_jual_tp_x_2.processing( true );
    //         },
    //         success: function (resp) {
    //             var obj = jQuery.parseJSON(resp);
    //             trn_jual_tp_x_2.clear().draw();
    //             $.each(obj, function (key, data) {
    //                 trn_jual_tp_x_2.rows.add(data).draw();
    //                 ;
    //             });
    //         },
    //         error: function (event, textStatus, errorThrown) {
    //             console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //         },
    //         complete: function(){
    //             trn_jual_tp_x_2.processing( false );
    //         }
    //     });
    // }
 

    function get_penjualan_tipe_year() {
        var periode = $("#periode_awal").val();
        var p1 = periode.toString().substring(0,2); 
        var p2 = periode.toString().substring(3,5); 
        var p3 = periode.toString().substring(6,10); 
        if(p2>9){
            var tgl = p3+'-'+p2+'-'+p1;
        } else {
            var tgl = p3+'-'+p2+'-'+p1;
        }  
        // alert(tgl00);
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_penjualan_tipe_year'); ?>",
            data: {"periode": tgl},
            beforeSend: function () {
                trn_jual_tipe_y.processing( true );
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_jual_tipe_y.clear().draw();
                $.each(obj, function (key, data) {
                    trn_jual_tipe_y.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            },
            complete: function(){
                trn_jual_tipe_y.processing( false );
            }
        });
    }

    
    function setValue(resp,idcabang){
        // alert(resp);
        if(resp===""){
            $("."+idcabang).css("background-color", "#ffcdd2");
        }else{
            var obj = jQuery.parseJSON(resp);
            // alert(obj);
            if(obj.stat===0){
                //alert(obj.msg);
                $("."+idcabang).css("background-color", "#ffcdd2");
            }else{
                // console.log(obj);
                var total = 0;
                $.each(obj, function(key, head){
                    // alert(key);
                    if(!isNaN(head)){
                        $("#"+idcabang+"-"+key).html(Number(head).toFixed(0));
                        if(key==="p_aktif"){
                            $("#"+idcabang+"-"+head).css("background-color", "#c8e6c9");
                            //$("#"+idcabang+"-"+head).css("text-decoration", "underline");
                        }else{
                            total = Number(total) + Number(Number(head).toFixed(0));
                        }
                    }
                });
                $("#"+idcabang+"-total").html(total);
            }
        }
    }

    
    function setValue2(resp,idcabang){
        // alert(resp);
        if(resp===""){
            $("."+idcabang).css("background-color", "#ffcdd2");
        }else{
            var obj = jQuery.parseJSON(resp);
            // alert(obj);
            if(obj.stat===0){
                //alert(obj.msg);
                $("."+idcabang).css("background-color", "#ffcdd2");
            }else{
                // console.log(obj);
                var total = 0;
                $.each(obj, function(key, head){
                    // alert(key);
                    if(!isNaN(head)){
                        $("#"+idcabang+"-"+key).html(Number(head).toFixed(0));
                        if(key==="p_aktif"){
                            $("#"+idcabang+"-"+head).css("background-color", "#c8e6c9");
                            //$("#"+idcabang+"-"+head).css("text-decoration", "underline");
                        }else{
                            total = Number(total) + Number(Number(head).toFixed(0));
                        }
                    }
                });
                $("#"+idcabang+"-total-k").html(total);
            }
        }
    }

    
    // function setValue2(resp,idcabang){
    //     // alert(resp);
    //     if(resp===""){
    //         $(".leas-"+idcabang).css("background-color", "#ffcdd2");
    //     }else{
    //         var obj = jQuery.parseJSON(resp);
    //         // alert(obj);
    //         if(obj.stat===0){
    //             //alert(obj.msg);
    //             $(".leas-"+idcabang).css("background-color", "#ffcdd2");
    //         }else{
    //             // console.log(obj);
    //             var total = 0;
    //             $.each(obj, function(key, head){
    //                 // alert(key);
    //                 if(!isNaN(head)){
    //                     $("#l_"+idcabang+"-"+key).html(Number(head).toFixed(0));
    //                     if(key==="p_aktif"){
    //                         $("#l_"+idcabang+"-"+head).css("background-color", "#c8e6c9");
    //                         //$("#"+idcabang+"-"+head).css("text-decoration", "underline");
    //                     }else{
    //                         total = Number(total) + Number(Number(head).toFixed(0));
    //                     }
    //                 }
    //             });
    //             $("#leas_"+idcabang+"-total").html(total);
    //         }
    //     }
    // }

    // Grafik untuk penjualan Tunai Kredit per bulan start
    function getJualTunaiKreditPerBulan(tahun){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashallcabang/getJualTunaiKreditPerBulan');?>",
          data: {"tahun":tahun.substring(0, 4)
                  ,"tanggal":tahun},
          success: function(resp){
              // getJualTunaiKreditYTD(resp);
              // getJualTunaiKreditMTD(resp);
              getJualMTD(resp);
              getJualYTD(resp);
              // getJualTunaiPzupati1(resp);
              var obj = jQuery.parseJSON(resp); 

              var PDataDetailPzusmg1 = [];
              var PLabelDetailPzusmg1 = [];
              $.each(obj.pzusmg1, function(key, data){
                  PDataDetailPzusmg1.push(data.jml);
                  PLabelDetailPzusmg1.push(bulan_short[data.bulan]);
              });
              var PDataDetailPzusmg2 = [];
              var PLabelDetailPzusmg2 = [];
              $.each(obj.pzusmg2, function(key, data){
                  PDataDetailPzusmg2.push(data.jml);
                  PLabelDetailPzusmg2.push(bulan_short[data.bulan]);
              });
              var PDataDetailPzupati1 = [];
              var PLabelDetailPzupati1 = [];
              $.each(obj.pzupati1, function(key, data){
                  PDataDetailPzupati1.push(data.jml);
                  PLabelDetailPzupati1.push(bulan_short[data.bulan]);
              });
              var PDataDetailPzukudus1 = [];
              var PLabelDetailPzukudus1 = [];
              $.each(obj.pzukudus1, function(key, data){
                  PDataDetailPzukudus1.push(data.jml);
                  PLabelDetailPzukudus1.push(bulan_short[data.bulan]);
              });
              var PDataDetailPzupwd1 = [];
              var PLabelDetailPzupwd1 = [];
              $.each(obj.pzupwd1, function(key, data){
                  PDataDetailPzupwd1.push(data.jml);
                  PLabelDetailPzupwd1.push(bulan_short[data.bulan]);
              });
              var PDataDetailPzubrebes1 = [];
              var PLabelDetailPzubrebes1 = [];
              $.each(obj.pzubrebes1, function(key, data){
                  PDataDetailPzubrebes1.push(data.jml);
                  PLabelDetailPzubrebes1.push(bulan_short[data.bulan]);
              }); 
              var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Penjualan Kredit Semua Cabang Tahun ' + tahun.substring(0, 4)
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                },
                  scales: {
                      xAxes: [{
                          ticks: {
                              fontSize: 8
                          }
                      }]
                  }
              };

              var config = {
                  type: 'bar',
                  data: {
                          datasets: [{
                                data: PDataDetailPzusmg1,
                                backgroundColor: window.chartColors.a,
                                borderColor: window.chartColors.a,
                                fill: false,
                                lineTension:0.5,
                                label: 'A.Yani'
                          },{ 
                                data: PDataDetailPzupati1,
                                backgroundColor: window.chartColors.b,
                                borderColor: window.chartColors.b,
                                fill: false,
                                lineTension:0.5,
                                label: 'Pati'
                          },{
                                data: PDataDetailPzukudus1,
                                backgroundColor: window.chartColors.c,
                                borderColor: window.chartColors.c,
                                fill: false,
                                lineTension:0.5,
                                label: 'Kudus'
                          },{ 
                                data: PDataDetailPzusmg2,
                                backgroundColor: window.chartColors.e,
                                borderColor: window.chartColors.e,
                                fill: false,
                                lineTension:0.5,
                                label: 'Setiabudi'
                          },{ 
                                data: PDataDetailPzupwd1,
                                backgroundColor: window.chartColors.f,
                                borderColor: window.chartColors.f,
                                fill: false,
                                lineTension:0.5,
                                label: 'Purwodadi'
                          },{ 
                                data: PDataDetailPzubrebes1,
                                backgroundColor: window.chartColors.g,
                                borderColor: window.chartColors.g,
                                fill: false,
                                lineTension:0.5,
                                label: 'Brebes'
                          }],
                          labels:PLabelDetailPzupati1
                      },
                  options: pieOptions
              };
              console.log(config);
              var Sales = $('#barJualTunaiKreditPerBulan').get(0).getContext('2d');
              if(typeof mybarJualTunaiKreditPerBulan != 'undefined' ){
                  mybarJualTunaiKreditPerBulan.destroy();
              }
              mybarJualTunaiKreditPerBulan = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    } 

    function getJualYTD(resp){
    //barJualTunaiKreditYTD
          var obj = jQuery.parseJSON(resp);  

          // Pati
          var PDataDetailPatiYTD = [];
          var PLabelDetailPatiYTD = [];
          var sumPatiYTD = 0; 
          var sumPatiMTD = 0; 
          if(obj.pzupati1ytd.length>0){
              $.each(obj.pzupati1ytd, function(key, data){
                  PDataDetailPatiYTD.push(data.jml);
                  PLabelDetailPatiYTD.push("YTD"); 
                  sumPatiYTD = sumPatiYTD + data.jml;
              });
          }else{
              PDataDetailPatiYTD.push(0);
              PLabelDetailPatiYTD.push("YTD");
          }
          if(obj.pzupati1mtd.length>0){
              $.each(obj.pzupati1mtd, function(key, data){ 
                  sumPatiMTD = sumPatiMTD + data.jml;
              });
          } 

          // Kudus
          var PDataDetailKudusYTD = [];
          var PLabelDetailKudusYTD = [];
          var sumKudusYTD = 0; 
          var sumKudusMTD = 0; 
          if(obj.pzukudus1ytd.length>0){
              $.each(obj.pzukudus1ytd, function(key, data){
                  PDataDetailKudusYTD.push(data.jml);
                  PLabelDetailKudusYTD.push("YTD");
                  sumKudusYTD = sumKudusYTD + data.jml;
              });
          }else{
              PDataDetailKudusYTD.push(0);
              PLabelDetailKudusYTD.push("YTD");
          }
          if(obj.pzukudus1mtd.length>0){
              $.each(obj.pzukudus1mtd, function(key, data){ 
                  sumKudusMTD = sumKudusMTD + data.jml;
              });
          } 

          // SMG1
          var PDataDetailSmg1YTD = [];
          var PLabelDetailSmg1YTD = [];
          var sumSmg1YTD = 0; 
          var sumSmg1MTD = 0; 
          if(obj.pzusmg1ytd.length>0){
              $.each(obj.pzusmg1ytd, function(key, data){
                  PDataDetailSmg1YTD.push(data.jml);
                  PLabelDetailSmg1YTD.push("YTD");
                  sumSmg1YTD = sumSmg1YTD + data.jml;
              });
          }else{
              PDataDetailSmg1YTD.push(0);
              PLabelDetailSmg1YTD.push("YTD");
          }
          if(obj.pzusmg1mtd.length>0){
              $.each(obj.pzusmg1mtd, function(key, data){ 
                  sumSmg1MTD = sumSmg1MTD + data.jml;
              });
          } 


          // SMG2
          var PDataDetailSmg2YTD = [];
          var PLabelDetailSmg2YTD = [];
          var sumSmg2YTD = 0; 
          var sumSmg2MTD = 0; 
          if(obj.pzusmg2ytd.length>0){
              $.each(obj.pzusmg2ytd, function(key, data){
                  PDataDetailSmg2YTD.push(data.jml);
                  PLabelDetailSmg2YTD.push("YTD");
                  sumSmg2YTD = sumSmg2YTD + data.jml;
              });
          }else{
              PDataDetailSmg2YTD.push(0);
              PLabelDetailSmg2YTD.push("YTD");
          }
          if(obj.pzusmg2mtd.length>0){
              $.each(obj.pzusmg2mtd, function(key, data){ 
                  sumSmg2MTD = sumSmg2MTD + data.jml;
              });
          } 


          // Pwd1
          var PDataDetailPwd1YTD = [];
          var PLabelDetailPwd1YTD = [];
          var sumPwd1YTD = 0; 
          var sumPwd1MTD = 0; 
          if(obj.pzupwd1ytd.length>0){
              $.each(obj.pzupwd1ytd, function(key, data){
                  PDataDetailPwd1YTD.push(data.jml);
                  PLabelDetailPwd1YTD.push("YTD");
                  sumPwd1YTD = sumPwd1YTD + data.jml;
              });
          }else{
              PDataDetailPwd1YTD.push(0);
              PLabelDetailPwd1YTD.push("YTD");
          }
          if(obj.pzupwd1mtd.length>0){
              $.each(obj.pzupwd1mtd, function(key, data){ 
                  sumPwd1MTD = sumPwd1MTD + data.jml;
              });
          } 


          // Brebes1
          var PDataDetailBrebes1YTD = [];
          var PLabelDetailBrebes1YTD = [];
          var sumBrebes1YTD = 0; 
          var sumBrebes1MTD = 0; 
          if(obj.pzubrebes1ytd.length>0){
              $.each(obj.pzubrebes1ytd, function(key, data){
                  PDataDetailBrebes1YTD.push(data.jml);
                  PLabelDetailBrebes1YTD.push("YTD");
                  sumBrebes1YTD = sumBrebes1YTD + data.jml;
              });
          }else{
              PDataDetailBrebes1YTD.push(0);
              PLabelDetailBrebes1YTD.push("YTD");
          }
          if(obj.pzubrebes1mtd.length>0){
              $.each(obj.pzubrebes1mtd, function(key, data){ 
                  sumBrebes1MTD = sumBrebes1MTD + data.jml;
              });
          } 

          // var sumAllYTD = Number(sumTunaiYTD) + Number(sumKreditYTD);
          // var sumAllMTD = Number(sumTunaiMTD) + Number(sumKreditMTD);

          // var avgTunaiYTD = ((Number(sumTunaiYTD)/Number(sumAllYTD)) * 100).toFixed(2);
          // var avgKreditYTD = ((Number(sumKreditYTD)/Number(sumAllYTD)) * 100).toFixed(2);

          // var avgTunaiMTD = ((Number(sumTunaiMTD)/Number(sumAllMTD)) * 100).toFixed(2);
          // var avgKreditMTD = ((Number(sumKreditMTD)/Number(sumAllMTD)) * 100).toFixed(2);


          // if(sumAllYTD){
          //     $(".label-tunai-ytd").html("T : " + avgTunaiYTD + "%");
          //     $(".label-kredit-ytd").html("K : " + avgKreditYTD + "%");
          // }else{
          //     $(".label-tunai-ytd").html("Tunai : 0%");
          //     $(".label-kredit-ytd").html("Kredit : 0%" );
          // }

          // if(sumAllMTD){
          //     $(".label-tunai-mtd").html(avgTunaiMTD + "%");
          //     $(".label-kredit-mtd").html(avgKreditMTD + "%");
          // }else{
          //     $(".label-tunai-mtd").html("0%");
          //     $(".label-kredit-mtd").html("0%" );
          // }
          var pieOptions     = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            //String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth   : 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 0, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps       : 100,
            //String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : false,
            //String - A legend template
            legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
            legend: {
                display: false,
                position: 'bottom',
                fontSize: 9,
                boxWidth: 20
            },
            title: {
                display: true,
                text: 'YTD'
            },
            chartArea: {
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
            scales: {
              xAxes: [{
                  stacked: true,
              }],
              yAxes: [{
                  stacked: true
              }]
            }
          };

          var config = {
              type: 'bar',
              data: {
                      datasets: [{
                            data: PDataDetailSmg1YTD,
                            backgroundColor: window.chartColors.a,
                            borderColor: window.chartColors.a,
                            fill: false,
                            lineTension:0.5,
                            label: 'A. Yani'
                      },{
                            data: PDataDetailPatiYTD,
                            backgroundColor: window.chartColors.b,
                            borderColor: window.chartColors.b,
                            fill: false,
                            lineTension:0.5,
                            label: 'Pati'
                      },{
                            data: PDataDetailKudusYTD,
                            backgroundColor: window.chartColors.c,
                            borderColor: window.chartColors.c,
                            fill: false,
                            lineTension:0.5,
                            label: 'Kudus'
                      },{
                            data: PDataDetailSmg2YTD,
                            backgroundColor: window.chartColors.e,
                            borderColor: window.chartColors.e,
                            fill: false,
                            lineTension:0.5,
                            label: 'Setiabudi'
                      },{
                            data: PDataDetailPwd1YTD,
                            backgroundColor: window.chartColors.f,
                            borderColor: window.chartColors.f,
                            fill: false,
                            lineTension:0.5,
                            label: 'Purwodadi'
                      },{
                            data: PDataDetailBrebes1YTD,
                            backgroundColor: window.chartColors.g,
                            borderColor: window.chartColors.g,
                            fill: false,
                            lineTension:0.5,
                            label: 'Brebes'
                      }],
                      labels:PLabelDetailSmg1YTD
                  },
              options: pieOptions
          };
          var Sales = $('#barJualTunaiKreditYTD').get(0).getContext('2d');
          if(typeof mybarJualTunaiKreditYTD != 'undefined' ){
              mybarJualTunaiKreditYTD.destroy();
          }
          mybarJualTunaiKreditYTD = new Chart(Sales, config);
    }

    function getJualMTD(resp){
      var obj = jQuery.parseJSON(resp);
          var PDataDetailPatiYTD = [];
          var PLabelDetailPatiYTD = [];
          var sumPatiMTD = 0; 
          if(obj.pzupati1mtd.length>0){
              $.each(obj.pzupati1mtd, function(key, data){
                  PDataDetailPatiYTD.push(data.jml);
                  PLabelDetailPatiYTD.push("MTD");
                  sumPatiMTD = sumPatiMTD + data.jml;
              });
          }else{
              PDataDetailPatiYTD.push(0);
              PLabelDetailPatiYTD.push("MTD");
          } 
          var PDataDetailSmg1YTD = [];
          var PLabelDetailSmg1YTD = [];
          var sumSmg1MTD = 0; 
          if(obj.pzusmg1mtd.length>0){
              $.each(obj.pzusmg1mtd, function(key, data){
                  PDataDetailSmg1YTD.push(data.jml);
                  PLabelDetailSmg1YTD.push("MTD");
                  sumSmg1MTD = sumSmg1MTD + data.jml;
              });
          }else{
              PDataDetailSmg1YTD.push(0);
              PLabelDetailSmg1YTD.push("MTD");
          }
          var PDataDetailKudusYTD = [];
          var PLabelDetailKudusYTD = [];
          var sumKudusMTD = 0; 
          if(obj.pzukudus1mtd.length>0){
              $.each(obj.pzukudus1mtd, function(key, data){
                  PDataDetailKudusYTD.push(data.jml);
                  PLabelDetailKudusYTD.push("KUDUS");
                  sumKudusMTD = sumKudusMTD + data.jml;
              });
          }else{
              PDataDetailKudusYTD.push(0);
              PLabelDetailKudusYTD.push("KUDUS");
          } 
          var PDataDetailSmg2YTD = [];
          var PLabelDetailSmg2YTD = [];
          var sumSmg2MTD = 0; 
          if(obj.pzusmg2mtd.length>0){
              $.each(obj.pzusmg2mtd, function(key, data){
                  PDataDetailSmg2YTD.push(data.jml);
                  PLabelDetailSmg2YTD.push("SETIABUDI");
                  sumSmg2MTD = sumSmg2MTD + data.jml;
              });
          }else{
              PDataDetailSmg2YTD.push(0);
              PLabelDetailSmg2YTD.push("SETIABUDI");
          } 
          var PDataDetailPwdYTD = [];
          var PLabelDetailPwdYTD = [];
          var sumPwdMTD = 0; 
          if(obj.pzupwd1mtd.length>0){
              $.each(obj.pzupwd1mtd, function(key, data){
                  PDataDetailPwdYTD.push(data.jml);
                  PLabelDetailPwdYTD.push("PURWODADI");
                  sumPwdMTD = sumPwdMTD + data.jml;
              });
          }else{
              PDataDetailPwdYTD.push(0);
              PLabelDetailPwdYTD.push("PURWODADI");
          } 
          var PDataDetailBrebesYTD = [];
          var PLabelDetailBrebesYTD = [];
          var sumBrebesMTD = 0; 
          if(obj.pzubrebes1mtd.length>0){
              $.each(obj.pzubrebes1mtd, function(key, data){
                  PDataDetailBrebesYTD.push(data.jml);
                  PLabelDetailBrebesYTD.push("BREBES");
                  sumBrebesMTD = sumBrebesMTD + data.jml;
              });
          }else{
              PDataDetailBrebesYTD.push(0);
              PLabelDetailBrebesYTD.push("BREBES");
          } 
          var pieOptions     = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            //String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth   : 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 0, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps       : 100,
            //String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : false,
            //String - A legend template
            legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
            legend: {
                display: false,
                position: 'bottom',
                fontSize: 9,
                boxWidth: 20
            },
            title: {
                display: true,
                text: 'MTD'
            },
            chartArea: {
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
            scales: {
              xAxes: [{
                  stacked: true,
              }],
              yAxes: [{
                  stacked: true
              }]
            }
          };

          var config = {
              type: 'bar',
              data: {
                      datasets: [{
                            data: PDataDetailSmg1YTD,
                            backgroundColor: window.chartColors.a,
                            borderColor: window.chartColors.a,
                            fill: false,
                            lineTension:0.5,
                            label: 'A. Yani'
                      },{
                            data: PDataDetailPatiYTD,
                            backgroundColor: window.chartColors.b,
                            borderColor: window.chartColors.b,
                            fill: false,
                            lineTension:0.5,
                            label: 'Pati'
                      },{
                            data: PDataDetailKudusYTD,
                            backgroundColor: window.chartColors.c,
                            borderColor: window.chartColors.c,
                            fill: false,
                            lineTension:0.5,
                            label: 'Kudus'
                      },{
                            data: PDataDetailSmg2YTD,
                            backgroundColor: window.chartColors.e,
                            borderColor: window.chartColors.e,
                            fill: false,
                            lineTension:0.5,
                            label: 'Setiabudi'
                      },{
                            data: PDataDetailPwdYTD,
                            backgroundColor: window.chartColors.f,
                            borderColor: window.chartColors.f,
                            fill: false,
                            lineTension:0.5,
                            label: 'Purwodadi'
                      },{
                            data: PDataDetailBrebesYTD,
                            backgroundColor: window.chartColors.g,
                            borderColor: window.chartColors.g,
                            fill: false,
                            lineTension:0.5,
                            label: 'Brebes'
                      }],
                      labels:PLabelDetailSmg1YTD
                  },
              options: pieOptions
          };
          var Sales = $('#barJualTunaiKreditMTD').get(0).getContext('2d');
          if(typeof mybarJualTunaiKreditMTD != 'undefined' ){
              mybarJualTunaiKreditMTD.destroy();
          }
          mybarJualTunaiKreditMTD = new Chart(Sales, config);
    }
</script>

<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptbkfakturshowroom
 *
 * @author adi
 */
class Rptbkfakturshowroom extends MY_Controller {

    protected $data = '';
    protected $val = '';

    public function __construct() {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            'submit' => site_url('rptbkfakturshowroom/submit'),
            'add' => site_url('rptbkfakturshowroom/add'),
            'edit' => site_url('rptbkfakturshowroom/edit'),
            'reload' => site_url('rptbkfakturshowroom'),
        );
        $this->load->model('rptbkfakturshowroom_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index() {
        $this->_init_add();
        $this->template
                ->title($this->data['msg_main'], $this->apps->name)
                ->set_layout('main')
                ->build('index', $this->data);
    }

    public function json_dgview() {
        echo $this->rptbkfakturshowroom_qry->json_dgview();
    }

    public function cek_last_faktur() {
        $my_faktur = './files/setting_faktur_showroom.txt';
        if (file_exists($my_faktur)) {
            //$number = file_get_contents($my_faktur);
            $handle = fopen($my_faktur, "r") or die("Unable to open file!");
            $number = fread($handle, filesize($my_faktur));
            fclose($handle);
            $var_a = substr($number, 0, 3);
            $var_b = substr($number, 4, 2);
            $var_c = substr($number, 5, 8);
            echo $var_a . "-" . $var_b . "-" . $var_c;
        }
    }

    public function submit() {
        if ($this->validate() == TRUE) {
            $fullPath = "./files/";
            array_map('unlink', glob("$fullPath*.csv"));
            $res = $this->rptbkfakturshowroom_qry->submit();
            $array = $this->input->post();
            if ($res) {
                $list = array(
                    array('"FK"', '"KD_JENIS_TRANSAKSI"', '"FG_PENGGANTI"', '"NOMOR_FAKTUR"', '"MASA_PAJAK"', '"TAHUN_PAJAK"', '"TANGGAL_FAKTUR"', '"NPWP"', '"NAMA"', '"ALAMAT_LENGKAP"', '"JUMLAH_DPP"', '"JUMLAH_PPN"', '"JUMLAH_PPNBM"', '"ID_KETERANGAN_TAMBAHAN"', '"FG_UANG_MUKA"', '"UANG_MUKA_DPP"', '"UANG_MUKA_PPN"', '"UANG_MUKA_PPNBM"', '"REFERENSI"'),
                    array('"LT"', '"NPWP"', '"NAMA"', '"JALAN"', '"BLOK"', '"NOMOR"', '"RT"', '"RW"', '"KECAMATAN"', '"KELURAHAN"', '"KABUPATEN"', '"PROPINSI"', '"KODE_POS"', '"NOMOR_TELEPON"'),
                    array('"OF"', '"KODE_OBJEK"', '"NAMA"', '"HARGA_SATUAN"', '"JUMLAH_BARANG"', '"HARGA_TOTAL"', '"DISKON"', '"DPP"', '"PPN"', '"TARIF_PPNBM"', '"PPNBM"'),
                );
                //var_dump($res);
                $no = (int) substr(str_replace("-", "", $array['no_faktur_awal']), 5, 8);
                $header_kode = substr(str_replace("-", "", $array['no_faktur_awal']), 0, 5);
                $no_fak = 0;
                foreach ($res as $value) {
                    if ($no<=9) {
                        $detail_kode = "0000000".$no;
                    } else if ($no<=99) {
                        $detail_kode = "000000".$no;
                    } else if ($no<=999) {
                        $detail_kode = "00000".$no;
                    } else if ($no<=9999) {
                        $detail_kode = "0000".$no;
                    } else if ($no<=99999) {
                        $detail_kode = "000".$no;
                    } else if ($no<=999999) {
                        $detail_kode = "000".$no;
                    } else if ($no<=9999999) {
                        $detail_kode = "00".$no;
                    } else {
                        $detail_kode = $no;
                    }

                    $no_fak = $header_kode . $detail_kode; 
                    
                    $npwp = preg_replace("/[^0-9 ]/", '', $value['npwp']);
                    if(!$npwp){
                        $npwp = "000000000000000";
                    }
                    if($value['noktp']){
                        $ktp = $value['noktp'];
                    }else{
                        $ktp = '-';
                    }
                    $ppn = $value['ppn_unit'];

                    if (empty($value['alamat'])) {
                        $value['alamat'] = "-";
                    }
                    $data = array('"FK"', '"01"', '"0"', '"' . $no_fak . '"', '"' . $value['mm'] . '"', '"' . $value['yyyy'] . '"', '"' . $value['tgldo'] . '"'
                        , '"' . $npwp . '"', '"' . $value['nama'] . '"', '"' . $value['alamat'] . '"'
                        , '"' . round($value['dpp_unit']) . '"', '"' . round($ppn) . '"'
                        , '"0"', '""', '"0"', '"0"', '"0"', '"0"'
                        , '"' . $value['ket'] .  '"');
                    array_push($list, $data);

                    $company = array('"FAPR"', '"PT PRIMA ZIRANG UTAMA"', '"JL A. YANI NO. 170 SEMARANG"', '""', '""', '""', '""');
                    array_push($list, $company);

                    $diskon = $value['tot_disc'];
                    $ppn_item =  $value['ppn_unit'];
                    $data_penjualan = array('"OF"', '"001"', '"' . $value['nmbarang'] . '"', '"' . round($value['harga_net_oftr']) . '"', '"1"', '"' . round($value['harga_net_oftr']) . '"'
                        , '"' . round($diskon) . '"', '"' . (int) ($value['dpp_unit']) . '"', '"' . round($ppn_item) . '"', '"0"', '"0.0"');
                    array_push($list, $data_penjualan);
                    $no++;
                }

                //var_dump($list);
                $my_file = './files/faktur-pajak-showroom-' . date('YmdHis') . '.csv';
                if (!file_exists($my_file)) {
                    $fp = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
                }

                $fp = fopen($my_file, 'a') or die('Cannot open file:  ' . $my_file);
                foreach ($list as $fields) {
                    $data = implode(",", $fields);
                    fwrite($fp, $data);
                    fwrite($fp, "\n");
                    //fputcsv($fp, $fields,',');
                }
                fclose($fp);


                $my_faktur = './files/setting_faktur_showroom.txt';
                $s_faktur = fopen($my_faktur, 'w') or die('Cannot open file:  ' . $my_faktur);
                fwrite($s_faktur, $no_fak);
                fclose($s_faktur);

                $this->load->helper('download');
                force_download($my_file, NULL);
            } else {
                $this->_init_add();
                $this->template
                        ->title($this->data['msg_main'], $this->apps->name)
                        ->set_layout('main')
                        ->build('index', $this->data);
            }
        } else {
            $this->_init_add();
            $this->template
                    ->title($this->data['msg_main'], $this->apps->name)
                    ->set_layout('main')
                    ->build('index', $this->data);
        }
    }

    private function _init_add() {
        $this->data['form'] = array(
            'periode_awal' => array(
                'placeholder' => 'Periode Awal',
                'id' => 'periode_awal',
                'name' => 'periode_awal',
                'value' => date('01-m-Y'),
                'class' => 'form-control calendar',
                'style' => 'margin-left: 5px;',
                'required' => '',
            ),
            'periode_akhir' => array(
                'placeholder' => 'Periode Akhir',
                'id' => 'periode_akhir',
                'name' => 'periode_akhir',
                'value' => date('d-m-Y'),
                'class' => 'form-control calendar',
                'style' => 'margin-left: 5px;',
                'required' => '',
            ),
            'no_faktur_awal' => array(
                'placeholder' => 'Nomor Faktur Awal',
                'id' => 'no_faktur_awal',
                'name' => 'no_faktur_awal',
                'value' => '',
                'class' => 'form-control',
                'style' => 'margin-left: 5px;',
                'required' => '',
            ),
        );
    }

    private function validate() {
        $config = array(
            array(
                'field' => 'no_faktur_awal',
                'label' => 'Nomor Faktur Awal',
                'rules' => 'required|max_length[20]',
            ),
            array(
                'field' => 'periode_awal',
                'label' => 'Periode Awal',
                'rules' => 'required|max_length[20]',
            ),
            array(
                'field' => 'periode_akhir',
                'label' => 'Periode Akhir',
                'rules' => 'required|max_length[20]',
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            return false;
        } else {
            return true;
        }
    }

}

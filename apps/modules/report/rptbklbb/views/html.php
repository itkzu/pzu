<?php

/*
 * ***************************************************************
 * Script : pdf
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto;
    }
    td {
        padding: 2px;
    }
</style>
<?php

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();

//echo json_encode($array);

if ($array['submit'] === "excel") {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAPORAN BUKU BESAR PERIODE " . $array['periode_awal'] . " s/d " . $array['periode_akhir'] . " " . date('Y-m-d H:i:s') . ".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');
$caption = "<b>LAPORAN BUKU BESAR BENGKEL</b>"
        . "<br>"
        //. "<b>" . strtoupper($this->apps->title) . " - " . strtoupper($this->session->userdata('data')['cabang']) . "</b>"
        . "<b>" . strtoupper($this->apps->title) . " - " . strtoupper( $array['nmdiv'] ) . "</b>"
        . "<br>"
        . "<b> PERIODE " . $array['periode_awal'] . " s/d " . $array['periode_akhir'] . "</b>"
        . "<br><br>";
$namaheader = array(
    array('data' => 'NO'
        , 'style' => 'text-align: center; width: 20px; font-size: 12px;'),
    array('data' => 'NO. JURNAL'
        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
    array('data' => 'TGL. JURNAL'
        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
    array('data' => 'KETERANGAN'
        , 'style' => 'text-align: center; font-size: 12px;'),
    array('data' => 'DEBET'
        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
    array('data' => 'KREDIT'
        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
    array('data' => 'SALDO'
        , 'style' => 'text-align: center; width: 90px; font-size: 12px;'),
);
$template = array(
    'table_open' => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',
    'thead_open' => '<thead>',
    'thead_close' => '</thead>',
    'heading_row_start' => '<tr>',
    'heading_row_end' => '</tr>',
    'heading_cell_start' => '<th>',
    'heading_cell_end' => '</th>',
    'tbody_open' => '<tbody>',
    'tbody_close' => '</tbody>',
    'row_start' => '<tr>',
    'row_end' => '</tr>',
    'cell_start' => '<td>',
    'cell_end' => '</td>',
    'row_alt_start' => '<tr>',
    'row_alt_end' => '</tr>',
    'cell_alt_start' => '<td>',
    'cell_alt_end' => '</td>',
    'table_close' => '</table>'
);
// Caption text
$this->table->set_caption($caption);
// Header detail
//if($rptakun['akun_h']){
$total_debit = 0;
$total_kredit = 0;
$total_akhir = 0;

foreach ($rptakun['akun_h'] as $value_h) {
    $akun_h = array(
        array('data' => '<b>' . $value_h['kdakun'] . ' - ' . $value_h['nmakun'] . '</b>'
            , 'colspan' => '7'
            , 'style' => 'text-align: left; font-size: 12px;'),
    );
    $this->table->add_row($akun_h);
    $this->table->add_row($namaheader);
    $no = 1;
    $saldo_awal = 0;
    $dk = "";
    if (isset($rptakun['akun_saldo_awal'])) {
        foreach ($rptakun['akun_saldo_awal'] as $v_sawal) {
            if ($value_h['kdakun'] === $v_sawal['kdakun']) {
                $akun_sawal = array(
                    array('data' => $no
                        , 'style' => 'text-align: center; width: 20px; font-size: 12px;'),
                    array('data' => "&nbsp&nbsp&nbsp&nbsp<b>SALDO AWAL</b>"
                        , 'colspan' => '5'
                        , 'style' => 'text-align: left; width: 160px; font-size: 12px;'),
                    array('data' => '<b>' . number_format($v_sawal['s_awal'], 2, ',', '.') . '</b>'
                        , 'style' => 'text-align: right; width: 90px; font-size: 12px;'),
                );
                $this->table->add_row($akun_sawal);
                $saldo_awal = $v_sawal['s_awal'];
                $dk = $v_sawal['dk'];
            }
        }
    }
    if (isset($rptakun['akun_d'])) {
        foreach ($rptakun['akun_d'] as $value) {
            if ($value_h['kdakun'] === $value['kdakun']) {
                $no++;
                //if ((int) $saldo_awal !== 0) {
                    if ($dk === "D") {
                        $saldo_awal = ($saldo_awal + $value['debet']) - $value['kredit'];
                    } elseif ($dk === "K") {
                        $saldo_awal = ($saldo_awal + $value['kredit']) - $value['debet'];
                    }
                //}

                $akun_d = array(
                    array('data' => $no
                        , 'style' => 'text-align: center; width: 20px; font-size: 12px;'),
                    array('data' => $value['nojurnal']
                        , 'style' => 'text-align: center; width: 160px; font-size: 12px;'),
                    array('data' => date("d/m/Y",strtotime($value['tgljurnal']))
                        , 'style' => 'text-align: center; width: 80px; font-size: 12px;'),
                    array('data' => $value['ket_d']
                        , 'style' => 'text-align: left;  font-size: 12px;'),
                    array('data' => number_format($value['debet'], 2, ',', '.')
                        , 'style' => 'text-align: right; width: 90px; font-size: 12px;'),
                    array('data' => number_format($value['kredit'], 2, ',', '.')
                        , 'style' => 'text-align: right; width: 90px; font-size: 12px;'),
                    array('data' => number_format($saldo_awal, 2, ',', '.')
                        , 'style' => 'text-align: right; width: 90px; font-size: 12px;'),
                );
                $this->table->add_row($akun_d);
            }
        }
    }
    $akun_h_foot = array(
        array('data' => '<b>TOTAL ' . $value_h['kdakun'] . ' - ' . $value_h['nmakun'] . ' </b>'
            , 'colspan' => '4'
            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => '<b>' . number_format($value_h['debet_s'], 2, ',', '.') . '</b>'
            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => '<b>' . number_format($value_h['kredit_s'], 2, ',', '.') . '</b>'
            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => '<b>' . number_format($saldo_awal, 2, ',', '.') . '</b>'
            , 'style' => 'text-align: right; font-size: 12px;'),
    );
    $total_debit += $value_h['debet_s'];
    $total_kredit += $value_h['kredit_s'];
    $total_akhir += $saldo_awal;

    $this->table->add_row($akun_h_foot);
    $separator = array(
        array('data' => '&nbsp;'
            , 'colspan' => '7'
            , 'style' => 'text-align: left; font-size: 12px;'),
    );
    $this->table->add_row($separator);
}

$total_all = array(
    array('data' => '<b>GRAND TOTAL</b>'
        , 'colspan' => '4'
        , 'style' => 'text-align: left; width: 130px;'),
    array('data' => '<b>' . number_format($total_debit, 2, ',', '.') . '</b>'
        , 'style' => 'text-align: right; width: 130px;'),
    array('data' => '<b>' . number_format($total_kredit, 2, ',', '.') . '</b>'
        , 'style' => 'text-align: right; width: 130px;'),
    array('data' => '<b>' . number_format($total_akhir, 2, ',', '.') . '</b>'
        , 'style' => 'text-align: right'),
);
$this->table->add_row($total_all);


$this->table->set_template($template);
echo $this->table->generate();
//}

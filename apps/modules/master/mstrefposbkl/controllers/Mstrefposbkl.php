<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstrefposbkl
 *
 * @author adi
 */
class Mstrefposbkl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstrefposbkl/submit'),
            'add' => site_url('mstrefposbkl/add'),
            'edit' => site_url('mstrefposbkl/edit'),
            'reload' => site_url('mstrefposbkl'),
        );
        $this->load->model('mstrefposbkl_qry');
        $this->data['dk'] = array(
            "D" => "DEBIT",
            "K" => "KREDIT",
        );

        $kddiv = $this->mstrefposbkl_qry->getKddiv();
        foreach ($kddiv as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }
/*
        $kdakun = $this->mstrefposbkl_qry->getKodeAkun();
        $this->data['kdakun'] = array(
            "" => "-- Pilih Kode Akun --",
        );
        foreach ($kdakun as $value) {
            $this->data['kdakun'][$value['kdakun']] = $value['kdakun']." - ".$value['nmakun'];
        }*/
        $this->data['faktif'] = array(
            "t" => "YA",
            "f" => "TIDAK",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstrefposbkl_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->mstrefposbkl_qry->json_dgview_detail();
    }

    public function getKodeAkun() {
        echo $this->mstrefposbkl_qry->getKodeAkun();
    }

    public function printAll() {
        $this->data['data'] = $this->mstrefposbkl_qry->printAll();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function addDetail() {
        $nourut = $this->input->post('nourut');
        $stat = $this->input->post('stat');
        if($this->validate_detail($nourut,$stat) == TRUE){
            $res = $this->mstrefposbkl_qry->addDetail();
        }else{
            $res = array(
                'state' => 0,
                'msg' => strip_tags(validation_errors()),
            );
        }
        echo json_encode($res);

    }

    public function submit() {
        $kdrefkb = $this->input->post('kdrefkb');
        $stat = $this->input->post('stat');
        $kddiv = $this->input->post('kddiv');
        if($this->validate($kdrefkb,$stat,$kddiv) == TRUE){
            $res = $this->mstrefposbkl_qry->submit();
        }else{
            $res = array(
                'state' => 0,
                'msg' => validation_errors(),
            );
        }
        echo json_encode($res);
    }

    public function validasi() {
        echo $this->mstrefposbkl_qry->validasi();
         $res;
    }

    private function _init_add(){
        $this->data['form'] = array(
           'kdrefkb'=> array(
                    'placeholder' => 'ID',
                   'type'        => 'hidden',
                    'id'          => 'kdrefkb',
                    'name'        => 'kdrefkb',
                    'value'       => set_value('kdrefkb'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nourut'=> array(
                    'placeholder' => 'ID',
                    'type'        => 'hidden',
                    'id'          => 'nourut',
                    'name'        => 'nourut',
                    'value'       => '',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '',
                    'placeholder' => 'Cabang',
            ),
           'nmrefkb'=> array(
                    'placeholder' => 'Nama Referensi',
                    'id'          => 'nmrefkb',
                    'name'        => 'nmrefkb',
                    'value'       => set_value('nmrefkb'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'required'    => '',
            ),
            'dk'=> array(
                    'attr'        => array(
                        'id'    => 'dk',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['dk'],
                    'value'    => set_value('dk'),
                    'name'     => 'dk',
                    'required'    => '',
            ),
            'faktif'=> array(
                    'attr'        => array(
                        'id'    => 'faktif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['faktif'],
                    'value'    => set_value('faktif'),
                    'name'     => 'faktif',
                    'required'    => '',
            ),
           'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'height: 181px;min-height: 70px;resize: vertical;',
                    'required'    => '',
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdakun'),
                    'name'     => 'kdakun',
                    'required'    => '',
            ),
           'mpl'=> array(
                    'placeholder' => 'Persentase (%) 1 s/d 100',
                    'id'          => 'mpl',
                    'name'        => 'mpl',
                    'value'       => set_value('mpl'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'required'    => '',
            ),
        );
    }

    private function _init_edit($kdrefkb = null){
        if(!$kdrefkb){
            $kdrefkb = $this->uri->segment(3);
        }
        $this->_check_id($kdrefkb);
        $this->data['form'] = array(
           'kdrefkb'=> array(
                    'placeholder' => 'ID',
                    'type'        => 'hidden',
                    'id'          => 'kdrefkb',
                    'name'        => 'kdrefkb',
                    'value'       => $this->val[0]['kdrefkb'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nourut'=> array(
                    'placeholder' => 'ID',
                    'type'        => 'hidden',
                    'id'          => 'nourut',
                    'name'        => 'nourut',
                    'value'       => '',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control ',
                    ),
                    'data'     =>  $this->data['kddiv'],
                    'value'    => $this->val[0]['kddiv'],
                    'name'     => 'kddiv',
                    'required'    => '',
                    'placeholder' => 'Cabang',
            ),
           'nmrefkb'=> array(
                    'placeholder' => 'Nama Referensi',
                    'id'          => 'nmrefkb',
                    'name'        => 'nmrefkb',
                    'value'       => $this->val[0]['nmrefkb'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'required'    => '',
            ),
            'dk'=> array(
                    'attr'        => array(
                        'id'    => 'dk',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['dk'],
                    'value'    => $this->val[0]['dk'],
                    'name'     => 'dk',
                    'required'    => '',
            ),
            'faktif'=> array(
                    'attr'        => array(
                        'id'    => 'faktif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['faktif'],
                    'value'    => $this->val[0]['faktif'],
                    'name'     => 'faktif',
                    'required'    => '',
            ),
           'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => $this->val[0]['ket'],
                    'class'       => 'form-control',
                    'style'       => 'height: 108px;min-height: 70px;resize: vertical;',
                    'required'    => '',
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdakun'),
                    'name'     => 'kdakun',
                    'required'    => '',
            ),
           'mpl'=> array(
                    'placeholder' => 'Persentase (%) 1 s/d 100',
                    'id'          => 'mpl',
                    'name'        => 'mpl',
                    'value'       => '',
                    'class'       => 'form-control',
                    'style'       => '',
                    'required'    => '',
            ),
        );
    }

    private function _check_id($kdrefkb){
        if(empty($kdrefkb)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstrefposbkl_qry->select_data($kdrefkb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdrefkb,$stat,$kddiv) {
        if(!empty($kdrefkb) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nmrefkb',
                    'label' => 'Nama Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'dk',
                    'label' => 'Debit/Kredit',
                    'rules' => 'required',
                    ),
            array(
                    'field' => 'kddiv',
                    'label' => 'Cabang',
                    'rules' => 'required',
                        ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nourut,$stat) {
        if(!empty($nourut) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nmrefkb',
                    'label' => 'Nama Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kdakun',
                    'label' => 'Kode Akun',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'mpl',
                    'label' => 'Persentase (%)',
                    'rules' => 'required|is_natural|greater_than_equal_to[1]|less_than_equal_to[100]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Set_keterangando
 *
 * @author adi
 */
class Set_keterangando extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('set_keterangando/submit'),
            'add' => site_url('set_keterangando/add'),
            'edit' => site_url('set_keterangando/edit'),
            'reload' => site_url('set_keterangando'),
        );
        $this->load->model('set_keterangando_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function getNoID() {
        echo $this->set_keterangando_qry->getNoID();
    }

    public function set_nodo() {
        echo $this->set_keterangando_qry->set_nodo();
    }

    public function ubah_ketdo() {
        echo $this->set_keterangando_qry->ubah_ketdo();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'kdtipe'=> array(
                     'placeholder' => 'Tipe SMH',
                     'id'          => 'kdtipe',
                     'name'        => 'kdtipe',
                     'value'       => set_value('kdtipe'),
                     'class'       => 'form-control',
                     'style'       => '',
                     'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna',
                    'value'       => set_value('warna'),
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'nmleasing'=> array(
                    'placeholder' => 'Leasing',
                    'value'       => set_value('nmleasing'),
                    'id'          => 'nmleasing',
                    'name'        => 'nmleasing',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'harga_otr'=> array(
                    'placeholder' => 'Harga OTR',
                    'value'       => set_value('harga_otr'),
                    'id'          => 'harga_otr',
                    'name'        => 'harga_otr',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
            ),
            'um'=> array(
                    'placeholder' => 'Uang Muka',
                    'value'       => set_value('um'),
                    'id'          => 'um',
                    'name'        => 'um',
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
            ),
            'nama_p'=> array(
                    'placeholder' => 'Nama Pemesan',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('nama_p'),
                    'id'          => 'nama_p',
                    'name'        => 'nama_p',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'nama_s'=> array(
                    'placeholder' => 'Nama STNK',
                    'value'       => set_value('nama_s'),
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'alamat_k'=> array(
                    'placeholder' => 'Alamat KTP',
                    'value'       => set_value('alamat_k'),
                    'id'          => 'alamat_k',
                    'name'        => 'alamat_k',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'ket_do'=> array(
                     'placeholder' => 'Keterangan DO',
                     'id'          => 'ket_do',
                     'name'        => 'ket_do',
                     'value'       => set_value('ket_do'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => '',
                    'type'        => 'hidden',
            ),
        );
    }
    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

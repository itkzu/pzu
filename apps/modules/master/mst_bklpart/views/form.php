<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-md-12">
                  <div class="row">
                      <div class="form-group">
                          <?php 
                              echo form_label($form['kdpart']['placeholder']);
                              echo form_input($form['kdpart']);
                              echo form_error('kdpart','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['nmpart']['placeholder']);
                              echo form_input($form['nmpart']);
                              echo form_error('nmpart','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['kdgrup']['placeholder']);
                              echo form_dropdown( $form['kdgrup']['name'],
                                                  $form['kdgrup']['data'] ,
                                                  $form['kdgrup']['value'] ,
                                                  $form['kdgrup']['attr']);
                              echo form_error('kdgrup','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-md-3">
                  <div class="row">
                      <div class="form-group">
                          <?=form_label($form['faktif']['placeholder']);?>
                          <div class="checkbox">
            							    <?php
                                  echo form_checkbox($form['faktif']);
            							    ?>
            					    </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        if(!$('#kdaks').val()){
            $('#faktif').hide();
            $('.btn-submit').click(function(){
                submit();
            });
        } else if($('#kdaks').val()){
            $('#faktif').show();
            $('#faktif').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            });
            $('.btn-submit').click(function(){
            update();
            });
        }
        //getKategori();
    });

    function getKategori(){
       //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("mst_bklpart/getKategori");?>",
            data: {},
            beforeSend: function() {
                $('#kdgrup').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kategori --'));
                $("#kdgrup").trigger("change.chosen");
                $("#kdgrup").chosen({ width: '100%' });
                if ($('#kdgrup').hasClass("chosen-hidden-accessible")) {
                    $('#kdgrup').select2('destroy');
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdgrup')
                        .append($('<option>', { value : value.kdgrup })
                        .html("<b style='font-size: 14px;'>" + value.kdgrup + " </b>"));
                });

                $('#kdgrup').select2({
                    placeholder: '-- Pilih Grup Part / Jasa --',
                    dropdownAutoWidth : true,
                    width: '100%',
                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                  //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
                  //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmaks + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function submit(){
        var kdgrup = $("#kdgrup").val();
        var kdpart = $("#kdpart").val().toUpperCase();
        var nmpart = $("#nmpart").val().toUpperCase();
        //alert(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mst_bklpart/submit");?>",
      		data: {"kdpart":kdpart,"nmpart":nmpart,"kdgrup":kdgrup},
      		beforeSend: function() {

      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              if (data.tipe==="success"){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      window.location.href = '<?=site_url('mst_bklpart');?>';
                  });
              }else{
                  refresh();
              }
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function update(){
        var kdpart = $("#kdpart").val().toUpperCase();
        var kdgrup = $("#kdgrup").val();
        var nmpart = $("#nmpart").val().toUpperCase();
        if ($("#faktif").prop("checked")){
          var faktif = 't';
        } else {
          var faktif = 'f';
        }
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mst_bklpart/update");?>",
      		data: {"kdpart":kdpart,"nmpart":nmpart,"kdgrup":kdgrup,"faktif":faktif},
      		beforeSend: function() {

      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              if (data.tipe==="success"){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      window.location.href = '<?=site_url('mst_bklpart');?>';
                  });
              }else{
                  refresh();
              }
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function refresh(){
      window.location.reload();
    }
</script>

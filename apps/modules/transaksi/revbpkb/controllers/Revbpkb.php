<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Revbpkb
 *
 * @author adi
 */
class Revbpkb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('revbpkb/submit'),
            'add' => site_url('revbpkb/add'),
            'edit' => site_url('revbpkb/edit'),
            'ctk'      => site_url('revbpkb/ctk'),
            'reload' => site_url('revbpkb'),
        );
        $this->load->model('revbpkb_qry');
        $kota = $this->revbpkb_qry->getkota();
        foreach ($kota as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }  

    public function getDtbpkb() {
        echo $this->revbpkb_qry->getDtbpkb();
    }

    public function setbpkb() {
        echo $this->revbpkb_qry->setbpkb();
    }

    public function set_nosin() {
        echo $this->revbpkb_qry->set_nosin();
    }

    public function submit() {
        echo $this->revbpkb_qry->submit();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
           'nobpkb'=> array(
                    'placeholder' => 'No. BPKB',
                    //'type'        => 'hidden',
                    'attr'        => array(
                       'id'    => 'nobpkb',
                       'class' => 'form-control',
                     ),
                    'data'        => '',
                    'name'        => 'nobpkb',
                    'value'       => set_value('nobpkb'), 
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '', 
            ),
           'tgltrm'=> array(
                    'placeholder' => 'Tgl Terima',
                    //'type'        => 'hidden',
                    'id'          => 'tgltrm',
                    'name'        => 'tgltrm',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Atas Nama',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'alamat'=> array(
                    'placeholder' => 'Alamat',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'kel'=> array(
                    'placeholder' => 'Kel.',
                    'id'          => 'kel',
                    'name'        => 'kel',
                    'value'       => set_value('kel'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'kec'=> array(
                    'placeholder' => 'Kec.',
                    'id'          => 'kec',
                    'name'        => 'kec',
                    'value'       => set_value('kec'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'kota'     => array(
                    'placeholder' => 'Kota',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'nohp'=> array(
                    'placeholder' => 'No HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin/Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nora'=> array( 
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'kdtipe'=> array(
                     'placeholder' => 'Tipe Unit',
                     'id'          => 'kdtipe',
                     'name'        => 'kdtipe',
                     'value'       => set_value('kdtipe'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => set_value('warna'),
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'byk_revbpkb'=> array(
                    'placeholder' => 'BPKB pernah direvisi sebanyak',
                    'value'       => set_value('byk_revbpkb'),
                    'id'          => 'byk_revbpkb',
                    'name'        => 'byk_revbpkb',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right',
                    'readonly'    => '',
            ),
            'tglrevakhir'=> array(
                    'placeholder' => 'Tanggal revisi terakhir',
                    'id'          => 'tglrevakhir',
                    'name'        => 'tglrevakhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'ket_rev'=> array(
                    'placeholder' => 'Keterangan Revisi',
                    'value'       => set_value('ket_rev'),
                    'id'          => 'ket_rev',
                    'name'        => 'ket_rev',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly'    => '',
            ),   
        );
    }  

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

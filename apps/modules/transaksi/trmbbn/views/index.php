<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-xs-12 box-header box-view">
              <div class="col-xs-3">
                <div class="row">
                    <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a>
                    <a class="btn btn-primary btn-edit">Ubah</a>
                    <a class="btn btn-danger btn-del">Hapus</a> 
                </div>
              </div>
              <div class="col-xs-2">
                <div class="row"> 
                </div>
              </div>
              <div class="col-xs-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> L/R BBN</button>
                </div>
              </div>
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['notbj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['notbj']);
                                          echo form_error('notbj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgltbj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tgltbj']);
                                          echo form_error('tgltbj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row"> 
                      <div class="col-xs-12">
                          <label></label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-xs-12">
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Mesin</th> 
                            <th style="text-align: center;">Tipe Unit</th> 
                            <th style="text-align: center;">Nama Konsumen</th> 
                            <th style="text-align: center;">Alamat Konsumen</th> 
                            <th style="text-align: center;">Biaya BBN</th> 
                            <th style="text-align: center;">Jasa</th> 
                            <th style="text-align: center;">Total</th> 
                            <th style="text-align: center;">No. DO</th> 
                            <th style="text-align: center;">Tgl. DO</th>    
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                          </tr>
                        </tfoot>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row"> 

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['total']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['total']);
                                          echo form_error('total','<div class="note">','</div>');
                                      ?> 
                              </div> 

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['disc']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['disc']);
                                          echo form_error('disc','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row"> 

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-6"> 
                              </div> 

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['pph21']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['pph21']);
                                          echo form_error('pph21','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-8 col-md-8 ">
                          <div class="row"> 

                              <div class="col-xs-12"> 
                                      <?php
                                          echo form_input($form['banner']);
                                          echo form_error('banner','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['alltot']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['alltot']);
                                          echo form_error('alltot','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>
                </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        disabled(); 
        autoNum();
        $('#notbj').mask('TBJ-9999-9999');

        $('#notbj').keyup(function () {
            set_notbj();
        }); 

        $('.btn-ctk').click(function(){
            var notbj = $('#notbj').val();
                        // var lik = nokps.replace("/","+");
                        // var lik = lik.replace("/","+");
                        // var lik = lik.replace("/","+");
            window.open('trmbbn/ctk/'+notbj, '_blank');
            // window.location.href = 'lrnew/ctk_lr/'+nodo;

            // ctk_lr();
        });
    });

    function disabled(){  
      $("#tgltbj").attr('disabled',true); 
      $("#disc").attr('disabled',true); 
      $("#pph21").attr('disabled',true); 
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',true);
      $(".btn-del").attr('disabled',true);
      $(".btn-ctk").attr('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){  
      $("#tgltbj").attr('disabled',true); 
      $("#disc").attr('disabled',true); 
      $("#pph21").attr('disabled',true); 
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',false);
      $(".btn-del").attr('disabled',false);
      $(".btn-ctk").attr('disabled',false); 
    }

    function autoNum(){  
      $("#total").autoNumeric('init',{ currencySymbol : 'Rp.'});  
      $("#disc").autoNumeric('init',{ currencySymbol : 'Rp.'}); 
      $("#pph21").autoNumeric('init',{ currencySymbol : 'Rp.'}); 
      $("#alltot").autoNumeric('init',{ currencySymbol : 'Rp.'}); 
    }

    function clear(){ 
      $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $("#total").val('');
      $("#disc").val('');
      $("#pph21").val('');
      $("#alltot").val('');
      $("#banner").val('');
    }

    function reset(){
      $("#notbj").val('');
      $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $("#total").val('');
      $("#disc").val('');
      $("#pph21").val('');
      $("#alltot").val('');
      $("#banner").val('');
    } 

    function set_notbj(){

      var notbj = $("#notbj").val();
      var notbj = notbj.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmbbn/set_notbj");?>",
            data: {"notbj":notbj },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  clear();
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true); 
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                    $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglbbn_trm))); 
                    $('#disc').autoNumeric('set',data.disc); 
                    $('#pph21').autoNumeric('set',data.pph21); 
                    $(".btn-ctk").attr('disabled',false);
                    $(".btn-edit").attr('disabled',false);
                    $(".btn-del").attr('disabled',false);
                    tb_det();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function tb_det(){ 
      table = $('.dataTable').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [ {
                    "aTargets": [ 5,6,7 ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"
                    
                },  
                {
                    "aTargets": [ 9 ],
                    "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                    },
                    "sClass": "center"
                    
                }],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nosin" },
                { "data": "kdtipe" },
                { "data": "nama"},
                { "data": "alamat" },
                { "data": "bbn"},
                { "data": "jasa"},
                { "data": "total"},
                { "data": "nodo"},
                { "data": "tgldo"},
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "notbj", "value": $("#notbj").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('trmbbn/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                $('#total').autoNumeric('set',total); 
                sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="No." ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });   
    }

    function sum(){
        var total = $('#total').autoNumeric('get'); 
        var disc  = $('#disc').autoNumeric('get'); 
        var pph21 = $('#pph21').autoNumeric('get'); 

        var alltot = parseInt(total) - (parseInt(disc) + parseInt(pph21));
        if(!isNaN(alltot)){
            $('#alltot').autoNumeric('set',alltot); 
            var terbilang = penyebut(alltot);
            if(alltot==='0'){
              $('#banner').val(terbilang);  
            } else {
              $('#banner').val(terbilang+" RUPIAH");  
            }
            
        }
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 
</script>

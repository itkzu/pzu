<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Postkbbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getdiv(){
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        $this->db->order_by('kddiv');
        $q = $this->db->get("pzu.v_divbkl");
        return $q->result_array();
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kddiv']) ){
            if($_GET['kddiv']){
                $kddiv = $_GET['kddiv'];
            }else{
                $kddiv = '';
            }
        }else{
            $kddiv = '';
        }

        $aColumns = array('nokb',
                            'nocetak',
                            'tglkb',
                            'nofaktur',
                            'drkpd',
                            'ket',
                            'nmrefkb',
                            'nilai',
                            'nourut',
                            'kdrefkb'
                        );
	    $sIndexColumn = "tglkb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        // if($this->perusahaan[0]['kddiv']===$this->session->userdata('data')['kddiv']){
          // $sTable = " (SELECT nokb,nocetak,tglkb,kdkb,nmkb,kdakun,dk,debet,kredit,ket,drkpd,nofaktur,nourut,kdrefkb,nmrefkb,fposting,nilai
          //                  FROM bkl.v_kb_posting order by nokb , nourut asc where kddiv_head = '".$kddiv."'
          //               ) AS a";
        // } else {
          $sTable = " (SELECT nokb,nocetak,tglkb,kdkb,nmkb,kdakun,dk,debet,kredit,ket,drkpd,nofaktur,nourut,kdrefkb,nmrefkb,fposting,nilai
                         FROM bkl.v_kb_posting where kddiv_head = '".$kddiv."' order by tglkb 
                      ) AS a";
        // }
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                            ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
               $sOrder = "";
            }
        }
        $sWhere = "where fposting=0";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " and (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if($aRow[ $aColumns[$i] ]===null){
                    $aRow[ $aColumns[$i] ] = '';
                }
                if(($i>=7 && $i<=7)){
                    $row[] = (float) $aRow[ $aColumns[$i] ];

                }else{
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
                //23 - 28
            $row[7] = (float)$aRow['nilai']; //number_format($saldo, 2);
            $row[8] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs btn-proses \" onclick=\"posting('".$aRow['nokb']."',".$aRow['nourut'].",".$aRow['kdrefkb'].");\">Posting</button>";
		    $output['aaData'][] = $row;
	    }
	    echo  json_encode( $output );
    }

    public function proses() {
        $user = $this->session->userdata("username"); 
        $kddiv = $this->input->post('kddiv');
        $nourut = $this->input->post('nourut');
        $kdrefkb = $this->input->post('kdrefkb');
        $nokb = $this->input->post('nokb');

        $q = $this->db->query("select title,msg,tipe from bkl.kb_posting('". $kddiv ."','". $nokb ."',". $nourut .",". $kdrefkb .",'". $user ."')");
        echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstaset
 *
 * @author adi
 */
class Mstaset extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('mstaset/submit'),
            'add' => site_url('mstaset/add'),
            'edit' => site_url('mstaset/edit'),
            'reload' => site_url('mstaset'),
        );
        $this->load->model('mstaset_qry');

        $this->data['kdklpaset'] = array(
            "" => "-- Pilih Kelompok Aset --",
        ); 
        /*
        $kdklpaset = $this->mstaset_qry->getKlpAset();
        foreach ($kdklpaset as $value) {
            $this->data['kdklpaset'][$value['kdklpaset']] = $value['nmklpaset'];
        }
        */
        
        $this->data['kdjaset'] = array(
            "" => "-- Pilih Jenis Aset --",
        ); 
        $kdjaset = $this->mstaset_qry->getJAset();
        foreach ($kdjaset as $value) {
            $this->data['kdjaset'][$value['kdjaset']] = $value['nmjaset'];
        }
        
        $this->data['kdlokaset'] = array(
            "" => "-- Pilih Lokasi --",
        ); 
        $kdlokaset = $this->mstaset_qry->getLokasi();
        foreach ($kdlokaset as $value) {
            $this->data['kdlokaset'][$value['kdlokasi']] = $value['nmlokasi'];
        }
        
        $this->data['kdklpdep'] = array(
            "" => "-- Pilih Pemakai Aset --",
        ); 
        
        
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function getKlpAsetDep() {
        $kdklpdep = $this->mstaset_qry->getKlpAsetDep();
        echo $kdklpdep;
    }
    
    public function add(){  
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function edit() {
        $this->_init_edit();


        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('edit',$this->data);
        
    }
    
    public function json_dgview() {
        echo $this->mstaset_qry->json_dgview();
    }
    
    public function getKlpAset() {
        $kdklpaset = $this->input->post('kdklpaset');
        if($kdklpaset){
            $res = $this->mstaset_qry->getKlpAset($kdklpaset);
            echo json_encode($res);
        }
    }
    
    public function submit() {  
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        
        if($this->validate($id,$stat) == TRUE){
            $res = $this->mstaset_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->set_layout('main')->build('form', $this->data);
                    }else{
                        $this->_check_id($id);
                        $this->template->set_layout('main')->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($id)){
                $this->_init_add();
                $this->template->set_layout('main')->build('form', $this->data);
            }else{
                $this->_check_id($id);
                $this->template->set_layout('main')->build('form', $this->data);
            }
        }
    }
    
    private function _init_add(){
        
        $this->data['form'] = array(
           'kdaset'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kdaset',
                    'name'        => 'kdaset',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmaset'=> array(
                    'placeholder' => 'Nama Aset',
                    'id'      => 'nmaset',
                    'name'        => 'nmaset',
                    'value'       => set_value('nmaset'),
                    'class'       => 'form-control',
                    'autofocus'   => '',
                    'required'    => '',
            ),
           'harga'=> array(
                    'placeholder' => 'Harga @ (Rp.)',
                    'id'      => 'harga',
                    'name'        => 'harga',
                    'value'       => set_value('harga'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'tglbeli'=> array(
                    'placeholder' => 'Tanggal Terima',
                    'id'      => 'tglbeli',
                    'name'        => 'tglbeli',
                    'value'       => set_value('tglbeli'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
            'kdjaset'=> array(
                    'attr'        => array(
                        'id'    => 'kdjaset',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdjaset'],
                    'value'    => set_value('kdjaset'),
                    'name'     => 'kdjaset',
                    'required'    => '',
            ),
           'jumlah'=> array(
                    'placeholder' => 'Jumlah Aset',
                    'id'      => 'jumlah',
                    'name'        => 'jumlah',
                    'value'       => set_value('jumlah'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'total_harga'=> array(
                    'placeholder' => 'Total Harga',
                    'id'      => 'total_harga',
                    'name'        => 'total_harga',
                    'value'       => set_value('total_harga'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'ket_aset'=> array(
                    'placeholder' => 'Keterangan Aset',
                    'id'      => 'ket_aset',
                    'name'        => 'ket_aset',
                    'value'       => set_value('ket_aset'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 115px; min-height: 35px;'
            ),
           'ket_posisi'=> array(
                    'placeholder' => 'Keterangan Posisi Aset',
                    'id'      => 'ket_posisi',
                    'name'        => 'ket_posisi',
                    'value'       => set_value('ket_posisi'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 55px; min-height: 35px;'
            ),
            'kdlokaset'=> array(
                    'attr'        => array(
                        'id'    => 'kdlokaset',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdlokaset'],
                    'value'    => set_value('kdlokaset'),
                    'name'     => 'kdlokaset',
                    'required'    => '',
            ),
            'kdklpdep'=> array(
                    'attr'        => array(
                        'id'    => 'kdklpdep',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdklpdep'],
                    'value'    => set_value('kdklpdep'),
                    'name'     => 'kdklpdep',
                    'required'    => '',
            ),
        );
    }
    
    private function _init_edit(){
        $groupid = $this->uri->segment(3);
        $this->_check_id($groupid);

        echo "<script> console.log('PHP: ". json_encode($this->val) ."');</script>";

        $this->data['form'] = array(
           'kdaset'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kdaset',
                    'name'        => 'kdaset',
                    'value'       => $this->val[0]['kdaset'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
           'kdasetga'=> array(
                    'placeholder' => 'Kode Aset',
                    'id'      => 'kdasetga',
                    'name'        => 'kdasetga',
                    'value'       => $this->val[0]['kdasetga'],
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'required'    => '',
                    'style'       => 'background-color: #fff;',
            ),
           'nmaset'=> array(
                    'placeholder' => 'Nama Aset',
                    'id'      => 'nmaset',
                    'name'        => 'nmaset',
                    'value'       => $this->val[0]['nmaset'],
                    'class'       => 'form-control',
                    'autofocus'    => '',
                    'required'    => '',
            ),
           'harga'=> array(
                    'placeholder' => 'Harga @ (Rp.)',
                    'id'      => 'harga',
                    'name'        => 'harga',
                    'value'       => floatval($this->val[0]['harga']),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'tglbeli'=> array(
                    'placeholder' => 'Tanggal Terima',
                    'id'      => 'tglbeli',
                    'name'        => 'tglbeli',
                    'value'       => $this->val[0]['tglbeli'],
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
            'kdjaset'=> array(
                    'attr'        => array(
                        'id'    => 'kdjaset',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdjaset'],
                    'value'    => $this->val[0]['kdjaset'],
                    'name'     => 'kdjaset',
                    'required'    => '',
            ),
           'jumlah'=> array(
                    'placeholder' => 'Jumlah',
                    'id'      => 'jumlah',
                    'name'        => 'jumlah',
                    'value'       => 1,
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'total_harga'=> array(
                    'placeholder' => 'Total Harga',
                    'id'      => 'total_harga',
                    'name'        => 'total_harga',
                    'value'       => $this->val[0]['harga'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'ket_aset'=> array(
                    'placeholder' => 'Keterangan Aset',
                    'id'      => 'ket_aset',
                    'name'        => 'ket_aset',
                    'value'       => $this->val[0]['ket_aset'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 115px; min-height: 35px;'
            ),
           'ket_posisi'=> array(
                    'placeholder' => 'Keterangan Posisi Aset',
                    'id'      => 'ket_posisi',
                    'name'        => 'ket_posisi',
                    'value'       => $this->val[0]['ket_posisi'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'resize: vertical; height: 55px; min-height: 35px;'
            ),
            'kdlokaset'=> array(
                    'attr'        => array(
                        'id'    => 'kdlokaset',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdlokaset'],
                    'value'    => $this->val[0]['kdlokaset'],
                    'name'     => 'kdlokaset',
                    'required'    => '',
            ),
            'kdklpdep'=> array(
                    'attr'        => array(
                        'id'    => 'kdklpdep',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdklpdep'],
                    'value'    => $this->val[0]['kdklpdep'],
                    'name'     => 'kdklpdep',
                    'required'    => '',
            ),
            
        );
    }
    
    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }
        
        $this->val = $this->mstaset_qry->select_data($id);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nmaset',
                    'label' => 'Periode Target',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kdjaset',
                    'label' => 'Jenis Aset',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'tglbeli',
                    'label' => 'Tanggal Beli',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'jumlah',
                    'label' => 'Jumlah Barang',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'harga',
                    'label' => 'Harga Barang',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'total_harga',
                    'label' => 'Total Harga',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kdlokaset',
                    'label' => 'Lokasi Aset',
                    'rules' => 'required',
                ),
//            array(
//                    'field' => 'kdklpdep',
//                    'label' => 'Pemakai Aset',
//                    'rules' => 'required',
//                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

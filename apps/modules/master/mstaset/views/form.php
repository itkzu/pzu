<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>
<style type="text/css">
	.form-control{
		text-transform: uppercase;
	}
</style>

<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes); 
			?>
			<div class="box-body">

				<div class="row">

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_input($form['kdaset']);
							
								echo form_label($form['nmaset']['placeholder']);
								echo form_input($form['nmaset']);
								echo form_error('nmaset','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label($form['tglbeli']['placeholder']);
								echo form_input($form['tglbeli']);
								echo form_error('tglbeli','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Jenis Aset');
								echo form_dropdown($form['kdjaset']['name']
										,$form['kdjaset']['data'] 
										,$form['kdjaset']['value'] 
										,$form['kdjaset']['attr']);
								echo form_error('kdjaset','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Lokasi Aset');
								echo form_dropdown($form['kdlokaset']['name']
										,$form['kdlokaset']['data'] 
										,$form['kdlokaset']['value'] 
										,$form['kdlokaset']['attr']);
								echo form_error('kdlokaset','<div class="note">','</div>');
							?>
						</div>
					</div>


					<div class="col-lg-4">
						<div class="form-group">
							<?php 
								echo form_label($form['jumlah']['placeholder']);
								echo form_input($form['jumlah']);
								echo form_error('jumlah','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<?php 
								echo form_label($form['harga']['placeholder']);
								echo form_input($form['harga']);
								echo form_error('harga','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<?php 
								echo form_label($form['total_harga']['placeholder']);
								echo form_input($form['total_harga']);
								echo form_error('total_harga','<div class="note">','</div>');
							?>
						</div>
					</div>


					<div class="col-lg-12">
						<div class="form-group">
							<?php 
								echo form_label($form['ket_aset']['placeholder']);
								echo form_textarea($form['ket_aset']);
								echo form_error('ket_aset','<div class="note">','</div>');
							?>
						</div>
					</div>



					<div class="col-lg-12">
						<div class="form-group">
							<?php 
								echo form_label($form['ket_posisi']['placeholder']);
								echo form_textarea($form['ket_posisi']);
								echo form_error('ket_posisi','<div class="note">','</div>');
							?>
						</div>
					</div>
				</div>
				<!-- </div> -->

			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">
					Simpan
				</button>
				<a href="<?php echo $reload;?>" class="btn btn-default">
					Batal
				</a>
			</div>

			<?php echo form_close(); ?>

		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		getKlpAsetDep();


		$("#kdjaset").change(function(){
			getKlpAsetDep();
		});


		//$("select").chosen({ width: '100%' });





		$("#jumlah").keyup(function(){
			hitung_total_harga();
		});

		$("#jumlah").focus(function(){
			$(this).val(numeral($(this).val()).value());
		});
		$("#jumlah").blur(function(){
			//$("harga").html('Rp. ' + numeral($(this).val()).format('0,0'));
			$(this).val(numeral($(this).val()).format('0,0'));
		});



/*
		$("#harga").keyup(function(){
			hitung_total_harga();
		});
*/

		$("#harga").keyup(function(){
			hitung_total_harga();
			//$("harga").html('Rp. ' + numeral($(this).val()).format('0,0'));
		});



		$("#harga").focus(function(){
			$(this).val(numeral($(this).val()).value());
		});
		$("#harga").blur(function(){
			//$("harga").html('Rp. ' + numeral($(this).val()).format('0,0'));
			$(this).val(numeral($(this).val()).format('0,0'));
		});

	});
	
	function getKlpAsetDep(){
		var kdjaset = $("#kdjaset").val();
		if(kdjaset){
			$('#kdklpdep').html('');
			$("#kdklpdep").trigger("chosen:updated");
			$.ajax({
				type: "POST",
				url: "<?=site_url('mstaset/getKlpAsetDep');?>",
				data: {"key":"pzu","kdjaset":kdjaset},
				success: function(resp){   
					var obj = jQuery.parseJSON(resp);
					$.each(obj, function(key, data){
						$('#kdklpdep')
							.append($('<option>', { value : data.kdklpdep })
							.text(data.nmklpdep));
					});
					$("#kdklpdep").trigger("chosen:updated");
				},
				error:function(event, textStatus, errorThrown) {
						swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				}
			});
		}
	}
	
	function hitung_total_harga(){
		//var jumlah = $("#jumlah").val();
		//var harga = $("#harga").val();

		var jumlah = numeral($("#jumlah").val()).value();
		var harga = numeral($("#harga").val()).value();

		//var total_harga = Number(jumlah) * Number(harga);
		var total_harga = Number(jumlah) * harga;
		//$("#total_harga").val(total_harga);
		$("#total_harga").val(numeral(total_harga).format('0,0'));
	}
</script>
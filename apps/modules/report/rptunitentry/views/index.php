<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
                        , 'class' => "form-inline");
					echo form_open($submit,$attributes);
				?>
				<div class="form-group">
					<?php 
						echo form_label($form['tahun']['placeholder']);
                        echo form_dropdown($form['tahun']['name'],
                        					$form['tahun']['data'],
                        					$form['tahun']['value'],
                        					$form['tahun']['attr']);
						echo form_error('tahun','<div class="note">','</div>');
					?>
				</div>

				<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				
				<?php echo form_close(); ?>
				<hr>
				<div class="table-responsive">
					<table class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th rowspan="2" style="text-align: center;" id="periode">Periode</th>
								<th rowspan="2" style="text-align: center;width: 100px;">Jumlah Hari</th>
								<th rowspan="2" style="text-align: center;width: 100px;">Total Transaksi Penjualan</th>
								<th colspan="3" style="text-align: center;">Unit Entry (Servis)</th>
								<th rowspan="2" style="text-align: center;width: 100px;">Direct Sales</th>
							</tr>

							<tr>
								<th style="width: 100px;text-align: center;">Total</th>
								<th style="width: 100px;text-align: center;">Reguler</th>
								<th style="width: 100px;text-align: center;">KPB</th>
							</tr>
						</thead>

						<tbody></tbody>

						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
							</tr>
						</tfoot>

					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		$(".btn-tampil").click(function(){
			table.ajax.reload();
		});

		var column = [];

		column.push({ 
				"aTargets": [ 1,2,3,4,5,6 ],
				"mRender": function (data, type, full) {
						return type === 'export' ? data : numeral(data).format('0,0');
				},
				"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"bPaginate": false,
			"bInfo": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "tahun", "value": $("#tahun").val() }
							);
				$.ajax( {
					"dataType": 'json', 
					"type": "GET", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				$('#periode').text('Periode '+$("#tahun").val());
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},

			'footerCallback': function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // converting to interger to find total
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };

	            function setTotal(col) {
	            	return api
	                .column( col )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	            }

				var tot_1 = numeral(setTotal(1)).format('0,0');
				var tot_2 = numeral(setTotal(2)).format('0,0');
				var tot_3 = numeral(setTotal(3)).format('0,0');
				var tot_4 = numeral(setTotal(4)).format('0,0');
				var tot_5 = numeral(setTotal(5)).format('0,0');
				var tot_6 = numeral(setTotal(6)).format('0,0');
	 

	            // computing column Total of the complete result 
	            /*
	            var tot_1 = api
	                .column( 1 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
				*/
					

				
				//alert(monTotal);
	            // Update footer by showing the total with the reference of the column index 
				$( api.column( 0 ).footer() ).html('Total');
				$( api.column( 1 ).footer() ).html(tot_1);
				$( api.column( 2 ).footer() ).html(tot_2);
				$( api.column( 3 ).footer() ).html(tot_3);
				$( api.column( 4 ).footer() ).html(tot_4);
				$( api.column( 5 ).footer() ).html(tot_5);
				$( api.column( 6 ).footer() ).html(tot_6);
			},

			"sAjaxSource": "<?=site_url('rptunitentry/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//"dom": 'Bfrtip',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					 }
				}
			],
			//"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});
		


		
		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		/*
		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" ){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
		*/
	});
</script>
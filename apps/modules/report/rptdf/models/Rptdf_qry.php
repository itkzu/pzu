<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptdf_qry
 *
 * @author adi
 */
class Rptdf_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();
	}

	private function getdetail(){
		$output = array();
		$str = "SELECT
					  nodf
					, nopo
					, tglpo
					, nap
					, nint
					, nsel
					, total
					, nmdiv
					  FROM pzu.v_df_d";
		$q = $this->db->query($str);
		$res = $q->result_array();

		foreach ( $res as $aRow )
		{
			foreach ($aRow as $key => $value) {
				if(is_numeric($value)){
					$aRow[$key] = (float) $value;
				}else{
					$aRow[$key] = $value;
				}
			}
		   $output[] = $aRow;
		}
		return $output;
	}

	public function json_dgview() {
		error_reporting(-1);
		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
			}else{
				$periode_awal = '';
			}
		}else{
			$periode_awal = '';
		}

		if( isset($_GET['periode_akhir']) ){
			if($_GET['periode_akhir']){
				//$tgl2 = explode('-', $_GET['periode_akhir']);
				$periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl2[1].$tgl2[0];
			}else{
				$periode_akhir = '';
			}
		}else{
			$periode_akhir = '';
		}
		$aColumns = array(  'nodf',
							'tgldf',
							'tglrnb',
							'nap',
							'nint',
							'total',
							'nmdiv',
							'nokb',
							'tglkb',
							'nmkb');
		$sIndexColumn = "nodf";
		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}
		$sTable = " (SELECT
						nodf,
						tgldf,
						tglrnb,
						nap,
						nint,
						total,
						ket,
						nokb,
						tglkb,
						nmkb,
						nmdiv
					  FROM pzu.vm_df ) AS a";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
					intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "ORDER BY nodf";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
					$sOrder = "";
			}
		}
		$sWhere = "WHERE (to_char(tgldf,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";


		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				$ix = $i - 1;
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//
				$sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
				//echo $sWhere;
			}
		}


		/*
		 * SQL queries
		 */
		if(empty($sOrder)){
			$sOrder = " order by nodf ";
		}
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";

		//echo $sQuery;

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"data" => array()
		);

		$detail = $this->getdetail();
		foreach ( $rResult->result_array() as $aRow )
		{
			foreach ($aRow as $key => $value) {
				if(is_numeric($value)){
					$aRow[$key] = (float) $value;
				}else{
					$aRow[$key] = $value;
				}
			}
			$aRow['detail'] = array();

			foreach ($detail as $value) {
				if($aRow['nodf']==$value['nodf']){
					$aRow['detail'][]= $value;
				}
			}

			$output['data'][] = $aRow;
		}
		echo  json_encode( $output );

	}


}

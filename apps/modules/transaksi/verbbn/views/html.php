  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style> 
      caption {
          padding-top: 1px;
          padding-bottom: 1px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto; 
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }

      tr#r1{
        border: 2px solid black;
      }

      td#r1{
        border: 2px solid black;
      }

      th#r1{
        border: 2px solid black;
      }

      /*table, td, th {
        border: 3px solid black;
      }*/
  </style>
  <?php 

      function bulan($tgl) {
        $year = substr($tgl,0,4);
        $month = substr($tgl,5,2);
        $day = substr($tgl,8,2);
        $temp = '';
        if ($month === '01') {
          $temp = "Januari";
        } else if ($month === '02') {
          $temp = "Februari";
        } else if ($month === '03') {
          $temp = "Maret";
        } else if ($month ==='04') {
          $temp = "April";
        } else if ($month === '05') {
          $temp = "Mei";
        } else if ($month === '06') {
          $temp = "Juni";
        } else if ($month === '07') {
          $temp = "Juli";
        } else if ($month === '08') {
          $temp = "Agustus";
        } else if ($month === '09') {
          $temp = "September";
        } else if ($month === '10') {
          $temp = "Oktober";
        } else if ($month === '12') {
          $temp = "November";
        } else if ($month === '13') {
          $temp = "Desember";
        }
        return $day.' '.$temp.' '.$year;
      }

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 
  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $namaheader = array(
      array('data' => '<b> PT. PRIMA ZIRANG UTAMA </b>'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'),  
      array('data' => '<b>LAPORAN LEBIH/KURANG BAYAR BBN</b>'
                          , 'colspan' => 12
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; width: 50%; font-size: 24px;'),  
      array('data' => '<b> No. Terima Tagihan : '.$data[0]['nobbn_trm']
                          , 'colspan' => 6
                          , 'rowspan' => 2
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'), 
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);   
  $col1 = array(
      array('data' => '<b> '.$this->session->userdata('data')['cabang'].' </b>'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 50%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'),  
  );
  $this->table->add_row($col1);
  $col2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 50%; font-size: 12px;'),  
      array('data' => '<b> Tgl. Terima Tagihan : '.date_format(date_create($data[0]['tglbbn_trm']),"d/m/Y")
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'),  
  );
  $this->table->add_row($col2);
  $col3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),   
  );
  $this->table->add_row($col3);  
  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();  


  $bol1 = array(
      array('data' => ' No. '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => ' Delivery Order (DO) '
                          , 'colspan' => 2
                          , 'rowspan' => 1
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Nama STNK '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Kota '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Tahun '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Tagihan BBN '
                          , 'colspan' => 4
                          , 'rowspan' => 1
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Tarif Pajak '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Tambahan Lain '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Tarif Tetap '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),   
      array('data' => ' Lebih/Kurang '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),   
      array('data' => ' Tanggal Penetapan '
                          , 'colspan' => 1
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; font-size: 12px;'),   
  );
  $this->table->add_row($bol1);   

  $bol2 = array(
      array('data' => ' Nomor '
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Tanggal '
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Total Biaya'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Diskon'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => ' Jasa'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: center; font-size: 12px;'),    
      array('data' => ' Biaya BBN'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: center; font-size: 12px;'),    
  );
  $this->table->add_row($bol2);  

$tot_by = array();
$tot_disc = array();  
$tot_jasa = array();  
$tot_bybbn = array();  
$tot_trfpjk = array();  
$tot_tmblain = array();  
$tot_trfttp = array();  
$tot_lk = array();  
$no = 1;
foreach ($data as $value) {
    //var_dump($value['detail']);
    $by_bbn = $value['nilai'] - $value['disc'] - $value['jasa'];
    $tmb_lain = $by_bbn - $value['pajak_bbn'];
    $kl = $tmb_lain - $value['byproses_bbn'];
    $header_data = array( 
      array('data' => '&nbsp;'.$no
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 2%; font-size: 12px;'),  
      array('data' => '&nbsp;'.$value['nodo']
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp;'.date_format(date_create($value['tgldo']),"d/m/Y").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => '&nbsp;'.$value['nama'].'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp;'.$value['kota'].'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; font-size: 12px;'),  
      array('data' => '&nbsp;'.$value['tahun'].'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($value['nilai'],2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($value['disc'],2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($value['jasa'],2,",",".").'&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($by_bbn,2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($value['pajak_bbn'],2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($tmb_lain,2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($value['byproses_bbn'],2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format($kl,2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.date_format(date_create($value['tgl_penetapan_notis']),"d/m/Y").'&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; font-size: 12px;'),  
    );  
    $this->table->add_row($header_data); 
    $no++;

    $tot_by[]       = $value['nilai'];
    $tot_disc[]     = $value['disc'];
    $tot_jasa[]     = $value['jasa'];
    $tot_bybbn[]    = $by_bbn;
    $tot_trfpajak[] = $value['pajak_bbn'];
    $tot_tmblain[]  = $tmb_lain;
    $tot_trfttp[]   = $value['byproses_bbn'];
    $tot_lk[]       = $kl; 
}
    
  $bol2 = array(
      array('data' => ' TOTAL '
                          , 'colspan' => 6
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format(array_sum($tot_by),2,",",".").'&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format(array_sum($tot_disc),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format(array_sum($tot_jasa),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),  
      array('data' => '&nbsp;'.number_format(array_sum($tot_bybbn),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),    
      array('data' => '&nbsp;'.number_format(array_sum($tot_trfpajak),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),   
      array('data' => '&nbsp;'.number_format(array_sum($tot_tmblain),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),   
      array('data' => '&nbsp;'.number_format(array_sum($tot_trfttp),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),   
      array('data' => '&nbsp;'.number_format(array_sum($tot_lk),2,",",".").'&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: right; font-size: 12px;'),    
  );
  $this->table->add_row($bol2);  
  $template2 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="2">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr id="r1">',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th id="r1">',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr id="r1">',
          'row_end'               => '</tr>',
          'cell_start'            => '<td id="r1">',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr id="r1">',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td id="r1">',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template2); 
  echo $this->table->generate();  

  $dol1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: left; width: 100%; font-size: 30px;'),   
  );
  $this->table->add_row($dol1);  

  $dol2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'.$this->session->userdata('data')['kota'].', '.strtoupper(bulan(date_format(date_create(),"Y/m/d")))
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
  );
  $this->table->add_row($dol2); 

  $dol2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => 'Dibuat Oleh,'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => 'Mengetahui,'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
  );
  $this->table->add_row($dol2);  

  $dol1 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: left; width: 100%; font-size: 50px;'),   
  );
  $this->table->add_row($dol1);  

  $dol2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;<u>'.$this->session->userdata('data')['adh'].'</u>&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
  );
  $this->table->add_row($dol2);  

  $dol2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => 'Admin BBN'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => 'A D H'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: center; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),  
  );
  $this->table->add_row($dol2);  
  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();  
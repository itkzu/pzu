<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php

/* 
 * ***************************************************************
 * Script : html.php
 * Version : 
 * Date : Oct 31, 2017 11:46:13 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();
$decode = json_decode($decode);

if($array['submit']==="excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAPORAN DAFTAR PIUTANG PELUNASAN JP MATRIKS (AKTUAL) " .$array['periode_akhir'] ." ".date('Y-m-d H:i:s').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');
//$caption = "<b>DAFTAR PIUTANG PELUNASAN LEASING </b>"
//        . "<br>"
//        . "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->apps->logintag)."</b>"
//        . "<br>"
//        . "<b> PER TANGGAL " . $array['periode_akhir'] ."</b>"
//        . "<br><br>";

$this->table->add_row(array(
    array('data' => '<b>DAFTAR PIUTANG PELUNASAN JP MATRIKS</b>'
                        , 'colspan' => '11'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$this->table->add_row(array(
    array('data' => "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->session->userdata('data')['cabang'])."</b>"
                        , 'colspan' => '11'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$this->table->add_row(array(
    array('data' => "<b> PER TANGGAL " . $array['periode_akhir'] ."</b>"
                        , 'colspan' => '11'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$namaheader = array(
    array('data' => 'NO'
                        , 'align' => 'center'
                        , 'width' => '10px'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'NAMA PO'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
    array('data' => 'ALAMAT STNK'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
    array('data' => 'NO. DO'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
    array('data' => 'TGL DO'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
    array('data' => 'TIPE'
                        , 'align' => 'center'
                        , 'style' => ' width: 150px; font-size: 12px;'),
    array('data' => 'LEASING'
                        , 'align' => 'center'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'PROGRAM'
                        , 'align' => 'center'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'SALESMAN'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
    array('data' => 'SALDO PIUTANG'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
    array('data' => 'KETERANGAN'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
);
$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
// Caption text
//$this->table->set_caption($caption);
$this->table->add_row($namaheader);

$leas = array();
foreach ($decode->matriks as $leasing) {
    $leas[$leasing] = $leasing;
}
$res = array();
foreach ($leas as $v) {
    $no = 1;
    $total = 0;
    foreach ($decode->data as $data) {
        if($v==$data->kdleasing){          
            $akun_d = array(
                array('data' => $data->no
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->nama_s
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $data->alamat
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $data->nodo
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->tgldo
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->kdtipe
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->jnsbayar
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->nmprogleas
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->nmsales
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => number_format($data->mx_saldo, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => $data->ket_ar
                                    , 'style' => 'text-align: center; font-size: 12px;'),
            );                     
            $this->table->add_row($akun_d);   
            $total+=$data->jp_saldo;
        }
    }
    $separator = array(
        array('data' => 'TOTAL'
                            , 'colspan' => '9'
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => number_format($total, 0,',','.')
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => '&nbsp;'
                            , 'style' => 'text-align: left; font-size: 12px;'),
    );
    $this->table->add_row($separator);      
}


$desc_1 = array(
    array('data' => '&nbsp;'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => 'DIBUAT OLEH'
                        , 'colspan' => '2'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => '&nbsp;'
                        , 'colspan' => '3'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => 'MENGETAHUI'
                        , 'colspan' => '5'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
);
$this->table->add_row($desc_1);   
for($i=1;$i<=5;$i++){
  $this->table->add_row(array(
        array('data' => '&nbsp;'
                            , 'colspan' => '11'
                            , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    ));   
}
$desc_2 = array(
    array('data' => '&nbsp;'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => $this->perusahaan[0]['nmadh']
                        , 'colspan' => '2'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => '&nbsp;'
                        , 'colspan' => '3'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => $this->perusahaan[0]['nmkacab']
                        , 'colspan' => '5'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
);
$this->table->add_row($desc_2); 

$this->table->set_template($template);
echo $this->table->generate();      
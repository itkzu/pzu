<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptcekdatakpb
 *
 * @author adi
 */
class Rptcekdatakpb extends MY_Controller {

	protected $data = '';
	protected $val = '';

	public function __construct() {
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			'submit' => site_url('rptcekdatakpb/submit'),
			'add' => site_url('rptcekdatakpb/add'),
			'edit' => site_url('rptcekdatakpb/edit'),
			'reload' => site_url('rptcekdatakpb'),
		);
		$this->load->model('rptcekdatakpb_qry');

		//$this->load->database('bengkel', true);

	}

	public function index() {
		$this->_init_add();
		$this->template
				->title($this->data['msg_main'], $this->apps->name)
				->set_layout('main')
				->build('index', $this->data);
	}

	public function json_dgview() {
		echo $this->rptcekdatakpb_qry->json_dgview();
/*
		if ($this->validate() == TRUE) {
		  echo $this->rptunitentry_qry->json_dgview();
		} else {
			$this->_init_add();
			$this->template
					->title($this->data['msg_main'], $this->apps->name)
					->set_layout('main')
					->build('index', $this->data);
		}
*/
	}


	public function json_dgview2() {
		echo $this->rptcekdatakpb_qry->json_dgview2();
	}

	public function submit() {
		if ($this->validate() == TRUE) {

		} else {
			$this->_init_add();
			$this->template
					->title($this->data['msg_main'], $this->apps->name)
					->set_layout('main')
					->build('index', $this->data);
		}
	}

	private function _init_add() {
		$this->data['form'] = array(
			'periode_awal' => array(
				'placeholder' => 'Periode',
				'id' 		  => 'periode_awal',
				'name' 		  => 'periode_awal',
				'value' 	  => date('m-Y'),
				'class' 	  => 'form-control calendar',
				'style' 	  => 'margin-left: 5px;',
				'required' 	  => '',
			),
		);
	}

	private function validate() {
		$config = array(
			array(
				'field' => 'periode_awal',
				'label' => 'Periode Awal',
				'rules' => 'required|max_length[20]',
			),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			return false;
		} else {
			return true;
		}
	}
}
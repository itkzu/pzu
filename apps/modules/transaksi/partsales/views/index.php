<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    .pass-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    } 
    #load{
        width: 80%;
        height: 80%;
        position: fixed;
        text-indent: 100%;
        background: #ebebeb url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <div class="col-md-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">  
          <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                      <div class="row">
                          <div class="col-md-10">
                              <div class="form-group">
                                  <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a>     
                              </div>
                          </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                  <button type="button" class="btn btn-primary btn-imp"> <i class="fa fa-download"></i> Tarik Assist </button>    
                              </div>
                          </div>
                          <div class="col-md-12"> 
                              <div style="border-top: 2px solid #ddd; height: 10px;"></div>
                          </div> 
                      </div>
                  </div> 

                  <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs" id="myTab">
                          <li class="active">
                              <a href="#tab_1_1" class="tab_sc" data-toggle="tab"> List SC </a>
                          </li>
                          <li>
                              <a href="#tab_1_2" class="tab_bag" data-toggle="tab"> List BAG </a>
                          </li>  
                          <li>
                              <a href="#tab_1_3" class="tab_prs" data-toggle="tab"> List Belum Terbuat SC </a>
                          </li>  
                      </ul>

                      <div class="tab-content">

                          <!--tab_1_1-->
                          <div class="tab-pane active" id="tab_1_1">
                              <div class="row"> 

                                  <div class="col-md-12">
                                      <div class="row">
                                          <div class="col-md-1"> 
                                                  <?php 
                                                      echo form_label($form['periode_so']['placeholder']);  
                                                  ?>
                                          </div> 
                                          <div class="col-md-2">  
                                              <div class="form-group">
                                                  <?php  
                                                      echo form_input($form['periode_so']);
                                                      echo form_error('periode_so','<div class="note">','</div>'); 
                                                  ?> 
                                              </div>
                                          </div> 
                                          <div class="col-md-2"> 
                                              <div class="form-group">
                                                  <button type="button" class="btn btn-primary btn-so-tampil"> <i class="fa fa-eye"></i> Tampil </button>   
                                              </div>
                                          </div> 
                                      </div>
                                  </div> 

                                  <div class="col-md-12">
                                      <div class="table-responsive">
                                          <table class="table table-hover table-bordered SO_dataTable">
                                              <thead>
                                                  <tr>
                                                      <th style="width: 1%;text-align: center;">Detail</th>
                                                      <th style="text-align: center;">No. Part Sales</th>
                                                      <th style="text-align: center;">No. Sales Order</th>
                                                      <th style="text-align: center;">Tanggal</th>
                                                      <th style="text-align: center;">ID Pelanggan</th>
                                                      <th style="text-align: center;">Nama Pelanggan</th>
                                                      <th style="text-align: center;">Total</th>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                                  <tr>
                                                      <th></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th> 
                                                      <th style="text-align: center;"></th> 
                                                  </tr>
                                              </tfoot>
                                              <tbody></tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div> 

                                  <!-- tab_1_2 -->
                          <div class="tab-pane" id="tab_1_2">
                              <div class="row"> 

                                  <div class="col-md-12">
                                      <div class="row">
                                          <div class="col-md-1"> 
                                                  <?php 
                                                      echo form_label($form['periode_bag']['placeholder']);  
                                                  ?>
                                          </div> 
                                          <div class="col-md-2">  
                                              <div class="form-group">
                                                  <?php  
                                                      echo form_input($form['periode_bag']);
                                                      echo form_error('periode_bag','<div class="note">','</div>'); 
                                                  ?> 
                                              </div>
                                          </div> 
                                          <div class="col-md-2"> 
                                              <div class="form-group">
                                                  <button type="button" class="btn btn-primary btn-bag-tampil"> <i class="fa fa-eye"></i> Tampil </button>   
                                              </div>
                                          </div> 
                                      </div>
                                  </div> 

                                  <div class="col-md-12">
                                      <div class="table-responsive">
                                          <table class="table table-hover table-bordered BAG_dataTable">
                                              <thead>
                                                  <tr>
                                                      <th style="width: 1%;text-align: center;">Detail</th>
                                                      <th style="text-align: center;">No. Part Sales</th>
                                                      <th style="text-align: center;">No. Sales Order</th>
                                                      <th style="text-align: center;">Tanggal</th>
                                                      <th style="text-align: center;">ID Pelanggan</th>
                                                      <th style="text-align: center;">Nama Pelanggan</th>
                                                      <th style="text-align: center;">Total</th>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                                  <tr>
                                                      <th></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th> 
                                                      <th style="text-align: center;"></th> 
                                                  </tr>
                                              </tfoot>
                                              <tbody></tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div> 

                                  <!-- tab_1_3 -->
                          <div class="tab-pane" id="tab_1_3">
                              <div class="row"> 

                                  <div class="col-md-12">
                                      <div class="row">
                                          <div class="col-md-1"> 
                                                  <?php 
                                                      echo form_label($form['periode_prs']['placeholder']);  
                                                  ?>
                                          </div> 
                                          <div class="col-md-2">  
                                              <div class="form-group">
                                                  <?php  
                                                      echo form_input($form['periode_prs']);
                                                      echo form_error('periode_prs','<div class="note">','</div>'); 
                                                  ?> 
                                              </div>
                                          </div> 
                                          <div class="col-md-2"> 
                                              <div class="form-group">
                                                  <button type="button" class="btn btn-primary btn-prs-tampil"> <i class="fa fa-eye"></i> Tampil </button>   
                                              </div>
                                          </div> 
                                      </div>
                                  </div> 

                                  <div class="col-md-12">
                                      <div class="table-responsive">
                                          <table class="table table-hover table-bordered PRS_dataTable">
                                              <thead>
                                                  <tr>
                                                      <th style="width: 1%;text-align: center;">Detail</th>
                                                      <th style="text-align: center;">ID Dealer</th>
                                                      <th style="text-align: center;">No. Sales Order</th>
                                                      <th style="text-align: center;">Tanggal SO</th>
                                                      <th style="text-align: center;">ID Pelanggan</th>
                                                      <th style="text-align: center;">Nama Pelanggan</th>
                                                      <th style="text-align: center;">Disc SO</th>
                                                      <th style="text-align: center;">Total</th>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                                  <tr>
                                                      <th></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                      <th style="text-align: center;"></th>
                                                  </tr>
                                              </tfoot>
                                              <tbody></tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div> 

                                  <!--end tab-pane-->
                        </div>
                    </div>
              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div> 

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide(); 
        list_so();

        //button
        $('.btn-imp').click(function(){ 

            var date7 = new Date(Date.now() - 2*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-01 00:00:00');
            var date1 = new Date(Date.now() + 2*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD 23:00:00'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            var kddiv       = 'SHOWROOM'; 
            get_SO(secret_key,api_key,req_time,rdate1,rdate7,kddiv); 
            // alert(rdate1);
        }); 

        $('.tab_sc').click(function(){
          list_so();
        });

        $('.tab_bag').click(function(){
          list_bag();
        });

        $('.tab_prs').click(function(){
          list_prs();
        });

        //button
        $(".btn-so-tampil").click(function(){
            table_so.ajax.reload(); 
        });

        $(".btn-bag-tampil").click(function(){
            table_bag.ajax.reload(); 
        });

        $(".btn-prs-tampil").click(function(){
            table_prs.ajax.reload(); 
        });
    }); 

    //PO
    function get_SO(secret_key,api_key,req_time,rdate1,rdate7,kddiv){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SO'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7, "kddiv" : kddiv },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                console.log(secret_key,api_key,req_time,rdate1,rdate7,kddiv);
                ins_SO(resp);
                console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_SO(resp){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_SO'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){ 
                    save_SO(); 
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_SO(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_SO'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        // tbl_spk.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    //list
    function list_so(){

        var column = []; 

        column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });

        column.push({
            "aTargets": [ 6 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [1],
            "mData" : "noref",
            "mRender": function (data, type, row) {
              var btn = '<a href="partsales/html/' + data +'">' + data + ' </a>';
              return btn;
            }
        });

        $('.SO_dataTableDetail').DataTable();
        table_so = $('.SO_dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "noref" },
                { "data": "noso" },
                { "data": "tanggal" },
                { "data": "idcust" },
                { "data": "nama_pelanggan" },
                { "data": "total" },
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                        { "name": "periode_so", "value": $("#periode_so").val() }
                      );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
          },
          'rowCallback': function(row, data, index){
            //if(data[23]){
              //$(row).find('td:eq(23)').css('background-color', '#ff9933');
            //}
          },
          "sAjaxSource": "<?=site_url('partsales/json_dgview');?>",
          "oLanguage": {
              "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
          },
          //dom: '<"html5buttons"B>lTfgitp',
          buttons: [
            //{extend: 'copy'},
            //{extend: 'csv'},
            //{extend: 'excel'},
            {
              extend:    'excelHtml5',
              text:      'Export To Excel',
              titleAttr: 'Excel',
              "oSelectorOpts": { filter: 'applied', order: 'current' },
              "sFileName": "report.xls",
              action : function( e, dt, button, config ) {
                exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

              },
              exportOptions: {orthogonal: 'export'}

            },
            /*
            {extend: 'pdf',
              orientation: 'landscape',
              pageSize: 'A3'
            },
            {extend: 'print',
              customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                  .addClass('compact')
                  .css('font-size', 'inherit');
               }
            }
            */
          ],
          "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.SO_dataTable').tooltip({
          selector: "[data-toggle=tooltip]",
          container: "body"
        });

        // Add event listener for opening and closing details
        $('.SO_dataTable tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table_so.row( tr );

          if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
          }
          else {
            // Open this row
            row.child( format(row.data()) ).show();
            //format(row.data());
            tr.addClass('shown');
          }
        } );

        $('.SO_dataTable tfoot th').each( function () {
          var title = $('.SO_dataTable thead th').eq( $(this).index() ).text();
          if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
            $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
          }else{
            $(this).html( '' );
          }
        } );

        table_so.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function (ev) {
            //if (ev.keyCode == 13) { //only on enter keypress (code 13)
              that
                .search( this.value )
                .draw();
            //}
          } );
        });

        function exportTableToCSV($table, filename) {

          //rescato los títulos y las filas
          var $Tabla_Nueva = $table.find('tr:has(td,th)');
          // elimino la tabla interior.
          var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
             return (this.childElementCount != 1 );
          });

          var $rows = Tabla_Nueva2,
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // Solo Dios Sabe por que puse esta linea
            colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
            rowDelim = '"\r\n"',


            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
              var $row = $(row);
              var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

              return $cols.map(function (j, col) {
                var $col = $(col);
                var text = $col.text().replace(/\./g, '');
                return text.replace('"', '""'); // escape double quotes

              }).get().join(tmpColDelim);
              csv =csv +'"\r\n"' +'fin '+'"\r\n"';
            }).get().join(tmpRowDelim)
              .split(tmpRowDelim).join(rowDelim)
              .split(tmpColDelim).join(colDelim) + '"';


           download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
          var csvFile;
          var downloadLink;

          // CSV FILE
          csvFile = new Blob([csv], {type: "text/csv"});

          // Download link
          downloadLink = document.createElement("a");

          // File name
          downloadLink.download = filename;

          // We have to create a link to the file
          downloadLink.href = window.URL.createObjectURL(csvFile);

          // Make sure that the link is not displayed
          downloadLink.style.display = "none";

          // Add the link to your DOM
          document.body.appendChild(downloadLink);

          // Lanzamos
          downloadLink.click();
        }

        function format ( d ) {
          //console.log(d);
          var tdetail =  '<table class="table table-bordered table-hover SO_dataTableDetail">'+
            '<thead>' +
              '<tr style="background-color: #d73925;color: #fff;">'+
                '<th style="text-align: center;width:16px;"></th>'+
                '<th style="text-align: center;width:10px;">NO.</th>'+
                '<th style="text-align: center;">Kode Part</th>'+
                '<th style="text-align: center;">Nama Part</th>'+
                '<th style="text-align: center;">Qty</th>'+
                '<th style="text-align: center;">Uang Muka</th>'+
                '<th style="text-align: center;">Harga</th>'+
                '<th style="text-align: center;">Discount</th>'+
                '<th style="text-align: center;">Total</th>'+ 
              '</tr>'+
            '</thead><tbody>';
          var no = 1;
          var total = 0;
          if(d.detail.length>0){
            $.each(d.detail, function(key, data){
              //console.log(data);
              tdetail+='<tr>'+
                '<td style="text-align: center;"></td>'+
                '<td style="text-align: center;">'+no+'</td>'+
                '<td style="text-align: center;">'+data.kodepart+'</td>'+
                '<td style="text-align: center;">'+data.namapart+'</td>'+ 
                '<td style="text-align: right;">'+data.qty+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.um).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.harga).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.discount).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.total).format('0,0.00')+'</td>'+ 
              '</tr>';
              no++;
              total = Number(total) + Number(data.total);
            });

          }
          
          tdetail +=  '<tfoot>' +
                  '<tr>'+
                    '<th style="text-align: right;" colspan="8">TOTAL</th>'+
                    '<th style="text-align: right;">'+numeral(total).format('0,0.00')+'</th>'+
                  '</tr>'+
                '</tfoot>';
          
          tdetail += '</tbody></table>';
          return tdetail;
        }
    }

    function list_bag(){

        var column = []; 

        column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });

        column.push({
            "aTargets": [ 6 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [1],
            "mData" : "noref",
            "mRender": function (data, type, row) {
              var btn = '<a href="partsales/html2/' + data +'">' + data + ' </a>';
              return btn;
            }
        });

        $('.BAG_dataTableDetail').DataTable();
        table_bag = $('.BAG_dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "noref" },
                { "data": "noso" },
                { "data": "tanggal" },
                { "data": "idcust" },
                { "data": "nama_pelanggan" },
                { "data": "total" },
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                        { "name": "periode_bag", "value": $("#periode_bag").val() }
                      );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
          },
          'rowCallback': function(row, data, index){
            //if(data[23]){
              //$(row).find('td:eq(23)').css('background-color', '#ff9933');
            //}
          },
          "sAjaxSource": "<?=site_url('partsales/json_dgview_bag');?>",
          "oLanguage": {
              "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
          },
          //dom: '<"html5buttons"B>lTfgitp',
          buttons: [
            //{extend: 'copy'},
            //{extend: 'csv'},
            //{extend: 'excel'},
            {
              extend:    'excelHtml5',
              text:      'Export To Excel',
              titleAttr: 'Excel',
              "oSelectorOpts": { filter: 'applied', order: 'current' },
              "sFileName": "report.xls",
              action : function( e, dt, button, config ) {
                exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

              },
              exportOptions: {orthogonal: 'export'}

            },
            /*
            {extend: 'pdf',
              orientation: 'landscape',
              pageSize: 'A3'
            },
            {extend: 'print',
              customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                  .addClass('compact')
                  .css('font-size', 'inherit');
               }
            }
            */
          ],
          "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.BAG_dataTable').tooltip({
          selector: "[data-toggle=tooltip]",
          container: "body"
        });

        // Add event listener for opening and closing details
        $('.BAG_dataTable tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table_bag.row( tr );

          if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
          }
          else {
            // Open this row
            row.child( format(row.data()) ).show();
            //format(row.data());
            tr.addClass('shown');
          }
        } );

        $('.BAG_dataTable tfoot th').each( function () {
          var title = $('.BAG_dataTable thead th').eq( $(this).index() ).text();
          if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
            $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
          }else{
            $(this).html( '' );
          }
        } );

        table_bag.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function (ev) {
            //if (ev.keyCode == 13) { //only on enter keypress (code 13)
              that
                .search( this.value )
                .draw();
            //}
          } );
        });

        function exportTableToCSV($table, filename) {

          //rescato los títulos y las filas
          var $Tabla_Nueva = $table.find('tr:has(td,th)');
          // elimino la tabla interior.
          var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
             return (this.childElementCount != 1 );
          });

          var $rows = Tabla_Nueva2,
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // Solo Dios Sabe por que puse esta linea
            colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
            rowDelim = '"\r\n"',


            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
              var $row = $(row);
              var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

              return $cols.map(function (j, col) {
                var $col = $(col);
                var text = $col.text().replace(/\./g, '');
                return text.replace('"', '""'); // escape double quotes

              }).get().join(tmpColDelim);
              csv =csv +'"\r\n"' +'fin '+'"\r\n"';
            }).get().join(tmpRowDelim)
              .split(tmpRowDelim).join(rowDelim)
              .split(tmpColDelim).join(colDelim) + '"';


           download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
          var csvFile;
          var downloadLink;

          // CSV FILE
          csvFile = new Blob([csv], {type: "text/csv"});

          // Download link
          downloadLink = document.createElement("a");

          // File name
          downloadLink.download = filename;

          // We have to create a link to the file
          downloadLink.href = window.URL.createObjectURL(csvFile);

          // Make sure that the link is not displayed
          downloadLink.style.display = "none";

          // Add the link to your DOM
          document.body.appendChild(downloadLink);

          // Lanzamos
          downloadLink.click();
        }

        function format ( d ) {
          //console.log(d);
          var tdetail =  '<table class="table table-bordered table-hover BAG_dataTableDetail">'+
            '<thead>' +
              '<tr style="background-color: #d73925;color: #fff;">'+
                '<th style="text-align: center;width:16px;"></th>'+
                '<th style="text-align: center;width:10px;">NO.</th>'+
                '<th style="text-align: center;">Kode Part</th>'+
                '<th style="text-align: center;">Nama Part</th>'+
                '<th style="text-align: center;">Qty</th>'+
                '<th style="text-align: center;">Uang Muka</th>'+
                '<th style="text-align: center;">Harga</th>'+
                '<th style="text-align: center;">Discount</th>'+
                '<th style="text-align: center;">Total</th>'+ 
              '</tr>'+
            '</thead><tbody>';
          var no = 1;
          var total = 0;
          if(d.detail.length>0){
            $.each(d.detail, function(key, data){
              //console.log(data);
              tdetail+='<tr>'+
                '<td style="text-align: center;"></td>'+
                '<td style="text-align: center;">'+no+'</td>'+
                '<td style="text-align: center;">'+data.kodepart+'</td>'+
                '<td style="text-align: center;">'+data.namapart+'</td>'+ 
                '<td style="text-align: right;">'+data.qty+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.um).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.harga).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.discount).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.total).format('0,0.00')+'</td>'+ 
              '</tr>';
              no++;
              total = Number(total) + Number(data.total);
            });

          }
          
          tdetail +=  '<tfoot>' +
                  '<tr>'+
                    '<th style="text-align: right;" colspan="8">TOTAL</th>'+
                    '<th style="text-align: right;">'+numeral(total).format('0,0.00')+'</th>'+
                  '</tr>'+
                '</tfoot>';
          
          tdetail += '</tbody></table>';
          return tdetail;
        }
    }

    function list_prs(){

        var column = []; 

        column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });

        column.push({
            "aTargets": [ 6,7 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        $('.PRS_dataTableDetail').DataTable();
        table_prs = $('.PRS_dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "dealerid" },
                { "data": "noso" },
                { "data": "tglso" }, 
                { "data": "idcustomer" },
                { "data": "namacustomer" },
                { "data": "discso" },
                { "data": "totalhargaso" },
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                        { "name": "periode_prs", "value": $("#periode_prs").val() }
                      );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
          },
          'rowCallback': function(row, data, index){
            //if(data[23]){
              //$(row).find('td:eq(23)').css('background-color', '#ff9933');
            //}
          },
          "sAjaxSource": "<?=site_url('partsales/json_dgview_prs');?>",
          "oLanguage": {
              "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
          },
          //dom: '<"html5buttons"B>lTfgitp',
          buttons: [
            //{extend: 'copy'},
            //{extend: 'csv'},
            //{extend: 'excel'},
            {
              extend:    'excelHtml5',
              text:      'Export To Excel',
              titleAttr: 'Excel',
              "oSelectorOpts": { filter: 'applied', order: 'current' },
              "sFileName": "report.xls",
              action : function( e, dt, button, config ) {
                exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

              },
              exportOptions: {orthogonal: 'export'}

            },
            /*
            {extend: 'pdf',
              orientation: 'landscape',
              pageSize: 'A3'
            },
            {extend: 'print',
              customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                  .addClass('compact')
                  .css('font-size', 'inherit');
               }
            }
            */
          ],
          "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.PRS_dataTable').tooltip({
          selector: "[data-toggle=tooltip]",
          container: "body"
        });

        // Add event listener for opening and closing details
        $('.PRS_dataTable tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table_prs.row( tr );

          if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
          }
          else {
            // Open this row
            row.child( format(row.data()) ).show();
            //format(row.data());
            tr.addClass('shown');
          }
        } );

        $('.PRS_dataTable tfoot th').each( function () {
          var title = $('.PRS_dataTable thead th').eq( $(this).index() ).text();
          if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
            $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
          }else{
            $(this).html( '' );
          }
        } );

        table_prs.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function (ev) {
            //if (ev.keyCode == 13) { //only on enter keypress (code 13)
              that
                .search( this.value )
                .draw();
            //}
          } );
        });

        function exportTableToCSV($table, filename) {

          //rescato los títulos y las filas
          var $Tabla_Nueva = $table.find('tr:has(td,th)');
          // elimino la tabla interior.
          var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
             return (this.childElementCount != 1 );
          });

          var $rows = Tabla_Nueva2,
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // Solo Dios Sabe por que puse esta linea
            colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
            rowDelim = '"\r\n"',


            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
              var $row = $(row);
              var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

              return $cols.map(function (j, col) {
                var $col = $(col);
                var text = $col.text().replace(/\./g, '');
                return text.replace('"', '""'); // escape double quotes

              }).get().join(tmpColDelim);
              csv =csv +'"\r\n"' +'fin '+'"\r\n"';
            }).get().join(tmpRowDelim)
              .split(tmpRowDelim).join(rowDelim)
              .split(tmpColDelim).join(colDelim) + '"';


           download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
          var csvFile;
          var downloadLink;

          // CSV FILE
          csvFile = new Blob([csv], {type: "text/csv"});

          // Download link
          downloadLink = document.createElement("a");

          // File name
          downloadLink.download = filename;

          // We have to create a link to the file
          downloadLink.href = window.URL.createObjectURL(csvFile);

          // Make sure that the link is not displayed
          downloadLink.style.display = "none";

          // Add the link to your DOM
          document.body.appendChild(downloadLink);

          // Lanzamos
          downloadLink.click();
        }

        function format ( d ) {
          //console.log(d);
          var tdetail =  '<table class="table table-bordered table-hover PRS_dataTableDetail">'+
            '<thead>' +
              '<tr style="background-color: #d73925;color: #fff;">'+
                '<th style="text-align: center;width:16px;"></th>'+
                '<th style="text-align: center;width:10px;">NO.</th>'+
                '<th style="text-align: center;">Kode Part</th>'+
                '<th style="text-align: center;">Nama Part</th>'+
                '<th style="text-align: center;">ID Promo</th>'+
                '<th style="text-align: center;">Qty</th>'+
                '<th style="text-align: center;">Uang Muka</th>'+
                '<th style="text-align: center;">Harga</th>'+
                '<th style="text-align: center;">Discount</th>'+
                '<th style="text-align: center;">Total</th>'+ 
              '</tr>'+
            '</thead><tbody>';
          var no = 1;
          var total = 0;
          if(d.detail.length>0){
            $.each(d.detail, function(key, data){
              //console.log(data);
              tdetail+='<tr>'+
                '<td style="text-align: center;"></td>'+
                '<td style="text-align: center;">'+no+'</td>'+
                '<td style="text-align: center;">'+data.partsnumber+'</td>'+
                '<td style="text-align: center;">'+data.nmpart+'</td>'+ 
                '<td style="text-align: center;">'+data.promoidparts+'</td>'+ 
                '<td style="text-align: right;">'+data.kuantitas+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.uangmuka).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.hargaparts).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.discamount).format('0,0.00')+'</td>'+ 
                '<td style="text-align: right;">'+numeral(data.totalhargaparts).format('0,0.00')+'</td>'+ 
              '</tr>';
              no++;
              total = Number(total) + Number(data.totalhargaparts);
            });

          }
          
          tdetail +=  '<tfoot>' +
                  '<tr>'+
                    '<th style="text-align: right;" colspan="8">TOTAL</th>'+
                    '<th style="text-align: right;">'+numeral(total).format('0,0.00')+'</th>'+
                  '</tr>'+
                '</tfoot>';
          
          tdetail += '</tbody></table>';
          return tdetail;
        }
    }

    function disabled(){
      $(".box-new").hide();
      $(".box-view").show();
      $(".btn-addDetail").hide();
      $(".btn-updDetail").hide();
      $(".btn-delDetail").hide();
      $("#tipebayar").attr('disabled',true);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").hide();
    }

    function clear(){
      $("#kdsup").val('');
      $("#nopo").val('');
      $("#noslhso").val('');
      $("#tipebayar").val('');
      $("#tgltrm").val(moment().format('DD-MM-YYYY'));
      $("#tglpo").val(moment().format('DD-MM-YYYY'));
      $("#jth_tmp").val('15');
    }

    function dateback(tgl) {
      var less = $("#jth_tmp").val();
      var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var res = day + '-' + month + '-' + year;
    	return res;
    }

    function kdlokasi(){
      // var kdsales = $('#kdsales').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("partsales/kdlokasi");?>",
            data: { },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#kdlokasi").val(data.kdlokasi);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getTipeUnit(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("partsales/getTipeUnit");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdtipe').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Tipe Unit --'));
                $("#kdtipe").trigger("change.chosen");
                if ($('#kdtipe').hasClass("chosen-hidden-accessible")) {
                    $('#kdtipe').select2('destroy');
                    $("#kdtipe").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdtipe')
                      .append($('<option>', { value : value.kode })
                      .html("<b style='font-size: 14px;'>" + value.kode + " </b>"));
                });

                $('#kdtipe').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  
</script>

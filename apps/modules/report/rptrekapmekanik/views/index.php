<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: none;
    }
    .radio {
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .checkbox label, .radio label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 5px;
        font-weight: bold;
        cursor: pointer;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'target' => '_blank'
                    , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>
            <div class="box-body">  
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php 
                                echo form_label('Pilih Periode Awal');
                                echo form_input($form['periode_awal']);
                                echo form_error('periode_awal','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php
                                echo form_label('Pilih Periode Akhir');
                                echo form_input($form['periode_akhir']);
                                echo form_error('periode_akhir','<div class="note">','</div>');
                             ?>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                </div>

                <div class="col-md-12">
                    <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                </div>
 
                <div class="col-md-12 table">
                    <div class="form-group">
                    
                        <button type="submit" class="btn btn-default" name="submit" value="tunai" >
                            <i class="fa fa-print"></i> Cetak
                        </button>
                        <button type="submit" class="btn btn-success" name="submit" value="tunai_excel">
                            <i class="fa fa-file-excel-o"></i> Excel
                        </button>
                    </div>
                </div>

                <div class="col-md-12"> 
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered display nowrap DataTable" style="width: 100%;" class="display select">
                            <thead>
                                <tr> 
                                    <th style="width: 1%;text-align: center;" rowspan="2">No</th>
                                    <th style="width: 9%;text-align: center;" rowspan="2">Tanggal</th> 
                                    <th style="width: 10%;text-align: center;" rowspan="2">Nama Mekanik</th>
                                    <th style="width: 16%;text-align: center;" rowspan="2">Total</th>
                                    <th style="width: 16%;text-align: center;" colspan="2">JASA</th> 
                                    <th style="width: 16%;text-align: center;" colspan="2">REGULER</th> 
                                    <th style="width: 16%;text-align: center;" colspan="2">OLI</th> 
                                    <th style="width: 16%;text-align: center;" colspan="2">PART</th> 
                                    <th style="width: 16%;text-align: center;" >AMIC</th>  
                                </tr>
                                <tr>  
                                    <td style="text-align: center;" >JML</td>  
                                    <td style="text-align: center;" >TOTAL</td>  
                                    <td style="text-align: center;" >JML</td>  
                                    <td style="text-align: center;" >TOTAL</td>  
                                    <td style="text-align: center;" >JML</td>  
                                    <td style="text-align: center;" >TOTAL</td>  
                                    <td style="text-align: center;" >JML</td>  
                                    <td style="text-align: center;" >TOTAL</td>  
                                    <td style="text-align: center;" >JML</td>  
                                </tr>
                            </thead>
                            <tbody class="table_bd"></tbody>
                        </table>
                    </div>
                    </div>    
                <?php echo form_close(); ?>  
            </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        disable(); 

        $('#periode_awal').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        $('#periode_akhir').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        $(".btn-tampil").click(function(){
            $(".table").show();
            getTable();
        }); 
    });
    
    function getTable(){
        if ( $.fn.DataTable.isDataTable('.DataTable') ) {
          $('.DataTable').DataTable().destroy();
          $(".table_bd").html('');
        }
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val(); 
        $.ajax({
            type: "POST",
            url: "<?=site_url('rptrekapmekanik/get_data');?>",
            data: {"periode_awal":periode_awal, "periode_akhir":periode_akhir},
            beforeSend: function(){
                $(".table_bd").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){  
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp); 
                    var gtotal = 0;
                    var gjmljasa = 0;
                    var gtotjasa = 0;
                    var gjmlreg = 0;
                    var gtotreg = 0;
                    var gjmloli = 0;
                    var gtotoli = 0;
                    var gjmlpart = 0;
                    var gtotpart = 0;
                    var gjmlamic = 0;

                    $.each(obj.leasing, function(key, leas){
                        var stotal = 0;
                        var sjmljasa = 0;
                        var stotjasa = 0;
                        var sjmlreg = 0;
                        var stotreg = 0;
                        var sjmloli = 0;
                        var stotoli = 0;
                        var sjmlpart = 0;
                        var stotpart = 0;
                        var sjmlamic = 0;
                        $.each(obj.data, function(key, data){
                            if(leas===data.nmmekanik){
                                $(".table_bd").append('<tr>' + 
                                            '<td style="text-align: center;">'+data.no+'</td>' + 
                                            '<td style="text-align: center;">'+data.tglsv+'</td>' + 
                                            '<td style="text-align: center;">'+data.nmmekanik+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.total)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.jml_jasa)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.tot_jasa)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.jml_reg)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.tot_reg)).format('0,0.00')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.jml_oli)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.tot_oli)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.jml_part)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.tot_part)).format('0,0.00')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.jml_amic)).format('0,0.00')+'</td>' +  
                                        '</tr>');
                                stotal    = Number(stotal) + Number(data.total);
                                sjmljasa    = Number(sjmljasa) + Number(data.jml_jasa);
                                stotjasa    = Number(stotjasa) + Number(data.tot_jasa);
                                sjmlreg     = Number(sjmlreg) + Number(data.jml_reg);
                                stotreg     = Number(stotreg) + Number(data.tot_reg);
                                sjmloli     = Number(sjmloli) + Number(data.jml_oli);
                                stotoli     = Number(stotoli) + Number(data.tot_oli);
                                sjmlpart    = Number(sjmlpart) + Number(data.jml_part);
                                stotpart    = Number(sjmlpart) + Number(data.tot_part);
                                sjmlamic    = Number(sjmlamic) + Number(data.jml_amic);
                            }
                        }); 
                        
                    $(".table_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;font-weight: bold;"></td>' +  
                                '<td style="text-align: left;font-weight: bold;">TOTAL '+leas+'</td>' +
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(stotal)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sjmljasa)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(stotjasa)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sjmlreg)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(stotreg)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sjmloli)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(stotoli)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sjmlpart)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(stotpart)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sjmlamic)).format('0,0.00')+'</td>' + 
                            '</tr>');
                        gtotal      = Number(gtotal) + Number(stotal);
                        gjmljasa    = Number(gjmljasa) + Number(sjmljasa);
                        gtotjasa    = Number(gtotjasa) + Number(stotjasa);
                        gjmlreg     = Number(gjmlreg) + Number(sjmlreg);
                        gtotreg     = Number(gtotreg) + Number(stotreg);
                        gjmloli     = Number(gjmloli) + Number(sjmloli);
                        gtotoli     = Number(gtotoli) + Number(stotoli);
                        gjmlpart    = Number(gjmlpart) + Number(sjmlpart);
                        gtotpart    = Number(gtotpart) + Number(stotpart);
                        gjmlamic    = Number(gjmlamic) + Number(sjmlamic);
                    });
                    
                        
                    $(".table_bd").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;font-weight: bold;"></td>' +  
                                '<td style="text-align: left;font-weight: bold;">GRAND TOTAL</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gtotal)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gjmljasa)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gtotjasa)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gjmlreg)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gtotreg)).format('0,0.00')+'</td>' +
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gjmloli)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gtotoli)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gjmlpart)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gtotpart)).format('0,0.00')+'</td>' +
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(gjmlamic)).format('0,0.00')+'</td>' + 
                            '</tr>');
                }    
              $(".DataTable").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    },
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false
                });                
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",true);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    } 

    function disable(){
        $(".table").hide();
    } 
</script>
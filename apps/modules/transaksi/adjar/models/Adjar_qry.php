<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Adjar_qry
 *
 * @author adi
 */
class Adjar_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();  
      return json_encode($res);
    }

    public function set_nodo() {
      $nodo = $this->input->post('nodo');
      $noso = $this->input->post('noso');
      $ket = $this->input->post('ket');
      if($ket==='nodo'){
          $query = $this->db->query("select * from pzu.v_aradj_spk where nodo = '" . $nodo . "'");
      } else {
          $query = $this->db->query("select * from pzu.v_aradj_spk where noso = '" . $noso . "'");
      }
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_noaradj() {
      $noaradj = $this->input->post('noaradj');
      $query = $this->db->query("select * from pzu.vm_aradj where noaradj = '" . $noaradj . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_harga() {
      $jenis = $this->input->post('jenis');
      $noso = $this->input->post('noso');
      if ($jenis==='PC'){
          $query = $this->db->query("select tot_ar as nilai, byr_ar as byr, tot_ar-byr_ar as saldo_ar FROM pzu.vl_ar_kons WHERE noso = '" . $noso . "'");
      } else if ($jenis==='PL'){
          $query = $this->db->query("select pl as nilai, pl_bayar as byr, pl-pl_bayar as saldo_ar FROM pzu.vl_ar_leas WHERE noso = '" . $noso . "'");
      } else if ($jenis==='JP'){
          $query = $this->db->query("select jp as nilai, jp_bayar as byr, jp-jp_bayar as saldo_ar FROM pzu.vl_ar_leas WHERE noso = '" . $noso . "'");
      } else if ($jenis==='MX'){
          $query = $this->db->query("select nilai_ar as nilai, byr_ar  as byr, nilai_ar-byr_ar as saldo_ar FROM pzu.vl_ar_matriks WHERE noso = '" . $noso . "'");
      } 
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getjenis() {
        $jenis = $this->input->post('jenis');
        $this->db->select("*"); 
        if($jenis==='TUNAI'){
            $this->db->where('tipebayar','T');
        }
        $query = $this->db->get('pzu.j_ar_adj');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    }

    public function simpan() {
        $tglaradj = $this->apps->dateConvert($this->input->post('tglaradj'));
        $nodo            = $this->input->post('nodo');
        $noso            = $this->input->post('noso');
        $jenis            = $this->input->post('jenis');
        $nilai            = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
      
        $q = $this->db->query("select noaradj,title,msg,tipe from pzu.adjar_ins( '". $tglaradj ."'::date,
                                                                                  '". $noso ."'::varchar,
                                                                                  '". $jenis ."'::varchar,
                                                                                  ". $nilai ."::numeric,
                                                                                  '". $ket ."'::varchar,
                                                                                  '".$this->session->userdata("username")."'::varchar)");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

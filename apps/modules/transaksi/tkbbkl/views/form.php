<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nokb']['placeholder']);
                                            echo form_input($form['nokb']);
                                            echo form_error('nokb','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglkb']['placeholder']);
                                            echo form_input($form['tglkb']);
                                            echo form_error('tglkb','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=form_label($form['kdkb']['placeholder']);?>
                                        <div class="input-group">
                                            <?php
                                                echo form_dropdown($form['kdkb']['name'],$form['kdkb']['data'] ,$form['kdkb']['value'] ,$form['kdkb']['attr']);
                                                echo form_error('kdkb', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=form_label($form['jnstrx']['placeholder']);?>
                                        <div class="input-group">
                                            <?php
                                                echo form_dropdown($form['jnstrx']['name'],$form['jnstrx']['data'] ,$form['jnstrx']['value'] ,$form['jnstrx']['attr']);
                                                echo form_error('jnstrx', '<div class="note">', '</div>');
                                            ?>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info btn-set" id="btn-set">Set</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php
                                                echo form_input($form['nourut']);
                                                echo form_error('nourut', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-add">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%;text-align: center;">Jenis Transaksi</th>
                                            <th style="text-align: center;">Dari/Kepada</th>
                                            <th style="text-align: center;">No. Faktur</th>
                                            <th style="text-align: center;">Keterangan</th>
                                            <th style="width: 150px;text-align: center;">Nominal</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                            <th style="width: 10px;text-align: center;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center;"></th>
                                            <th style="text-align: center;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-save">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Kas/Bank Bengkel</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label('Jenis Transaki');
                                    echo form_dropdown($form['kdrefkb']['name'],$form['kdrefkb']['data'] ,$form['kdrefkb']['value'] ,$form['kdrefkb']['attr']);
                                    echo form_error('kdrefkb','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['darike']['placeholder']);
                                    echo form_input($form['darike']);
                                    echo form_error('darike','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                      echo form_label($form['nofaktur']['placeholder']);
                                    echo form_input($form['nofaktur']);
                                    echo form_error('nofaktur','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['ket']['placeholder']);
                                    echo form_input($form['ket']);
                                    echo form_error('ket','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['nilai']['placeholder']);
                                    echo form_input($form['nilai']);
                                    echo form_error('nilai','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {
        tglkb();
        $('#kdkb').change(function () {
          tglkb();
        });
        if($('#nokb').val()){
          set();
          getKdRef();
        } else {
          unset();
        }
        clear();
        $('#nilai').autoNumeric();

        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                jnstrx : "required"
            },
            messages : {
                jnstrx : ""
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdrefkb  : "required",
                nilai  : "required",
                ket  : "required",
                darike  : "required"
            },
            messages : {
                kdrefkb  : "Masukkan Data Transaki",
                nilai  : "Masukkan Nilai",
                ket  : "Masukkan Keterangan",
                darike  : "required"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 4 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 5,6 ],
            "sClass": "center"
        });

        // column.push({
        //     "aTargets": [ 1 ],
        //     "mRender": function (data, type, full) {
        //         return type === 'export' ? data : numeral(data).format('0,0');
        //     },
        //     "sClass": "right"
        // });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nmrefkb"  },
                { "data": "darike" },
                { "data": "nofaktur"   },
                { "data": "ket"  },
                { "data": "nilai"  },
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": $("#nokb").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('tkbbkl/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 3 ).footer() ).html('Total');
                $( api.column( 4 ).footer() ).html(numeral(total).format('0,0.00'));
                $('#total').autoNumeric('set',total);
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        $('.btn-set').click(function(){
            set();
            getKdRef();
        });


        $('.btn-add').click(function(){
            $('.btn-simpan').show();
            $('.btn-update').hide();
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kdrefkb').select2({
                placeholder: '-- Pilih Jenis Transaki --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            clear();
        });


        $('.btn-simpan').click(function(){
          //alert($('#kdaks').val());
            if ($("#modalform").valid()) {
                addDetail();
            }
        });

        $('.btn-update').click(function(){
            if ($("#modalform").valid()) {
                UpdateDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-save").click(function(){
            if ($("#myform").valid()) {
              save();
            }
        });
    });

    function set(){
      $('.btn-set').hide();
      $('#jnstrx').prop('disabled',true);
      $('#kdkb').prop('disabled',true);
      $('.btn-add').attr('disabled',false);
    }

    function unset(){
      $('.btn-set').show();
      $('#jnstrx').prop('disabled',false);
      $('#kdkb').prop('disabled',false);
      $('.btn-add').attr('disabled',true);
    }

    function clear(){
        $("#darike").val('');
        $("#nofaktur").val('');
        $("#ket").val('');
        $("#nilai").val('');
    }

    function edit(kdrefkb,darike,ket,nofaktur,nilai,nourut,nokb){
        $("#nourut").val(nourut);
        $("#kdrefkb").val(kdrefkb);
        //$("#kdakun").val(nmakun);
        $("#kdrefkb").trigger("change");
        $('#kdrefkb').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });
        $("#nofaktur").val(nofaktur);
        $("#ket").val(ket);
        $("#nilai").autoNumeric('set',nilai);
        $("#darike").val(darike);
        $("#nokb").val(nokb);
        $('#modal_transaksi').modal('toggle');
    }

    function tglkb(){
        var kdkb = $("#kdkb").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("tkbbkl/tglkb");?>",
            data: {"kdkb":kdkb },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb)));
                    // alert(data.tglkb);
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdRef(){
        var jnstrx = $("#jnstrx").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("tkbbkl/getKdRef");?>",
            data: {"jnstrx":jnstrx},
            beforeSend: function() {
                $('#kdrefkb').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis Transaksi --'));
                $("#kdrefkb").trigger("change.chosen");
                if ($('#kdrefkb').hasClass("chosen-hidden-accessible")) {
                    $('#kdrefkb').select2('destroy');
                    $("#kdrefkb").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdrefkb')
                        .append($('<option>', { value : value.kdrefkb })
                        .html("<b style='font-size: 14px;'>" + value.nmrefkb + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmrefkb + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }


    function addDetail(){
        var nokb = $("#nokb").val();
        var tglkb = $("#tglkb").val();
        var kdkb = $("#kdkb").val();
        var jnstrx = $("#jnstrx").val();
        var kdrefkb = $("#kdrefkb").val();
          // alert(kdrefkb);
        var darike = $("#darike").val();
        var nofaktur = $("#nofaktur").val();
        var ket = $("#ket").val();
        var nilai = $("#nilai").autoNumeric('get');
        var nourut = $("#nourut").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("tkbbkl/addDetail");?>",
            data: {"nokb":nokb
                    ,"tglkb":tglkb
                    ,"kdkb":kdkb
                    ,"kdkb":kdkb
                    ,"jnstrx":jnstrx
                    ,"kdrefkb":kdrefkb
                    ,"darike":darike.toUpperCase()
                    ,"nofaktur":nofaktur.toUpperCase()
                    ,"nourut":nourut
                    ,"ket":ket.toUpperCase()
                    ,"nilai":nilai  },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                $("#nourut").val('');
                clear();

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $("#nokb").val(data.nokb);
                    if (data.tipe==="success"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                        });
                    }else{
                        //refresh();
                    };
                        refresh();
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function deleted(nokb,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("tkbbkl/detaildeleted");?>",
                data: {"nokb":nokb ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('tkbbkl');?>';
        });
    }

    function save(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nokb = $("#nokb").val();
            var tglkb = $("#tglkb").val();
            var kdkb = $("#kdkb").val();
            var jnstrx = $("#jnstrx").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("tkbbkl/save");?>",
                data: {"nokb":nokb
                    ,"tglkb":tglkb
                    ,"kdkb":kdkb
                    ,"jnstrx":jnstrx},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('tkbbkl');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

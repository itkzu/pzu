<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>

<!--
<script type='text/javascript' src="<?php echo base_url(); ?>assets/plugins/daterangepicker/moment.js"></script>
<script type='text/javascript' src='http://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js'></script>
<div class="row" data-locale="id">
-->


<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
						, 'class' => "form-inline");
					echo form_open($submit,$attributes);
				?>
				<div class="form-group">
					<?php
					echo form_label("Periode");
					echo form_input($form['periode_awal']);
					echo form_input($form['periode_akhir']);
					echo form_error('periode_awal','<div class="note">','</div>');
					?>
				</div>
				<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				<?php echo form_close(); ?>
				<hr>
				<div class="table-responsive">
					<!--<table style="width: 2500px;"  class="table table-bordered table-hover js-basic-example dataTable">-->
					<table class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="width: 10px;text-align: center;" rowspan="2">No.</th>
								<th style="width: 70px;text-align: center;" rowspan="2">No. Kasbon</th>
								<th style="width: 60px;text-align: center;" rowspan="2">Tgl Kasbon</th>
								<th style="text-align: center;" rowspan="2">Keterangan</th>
								<th style="width: 80px;text-align: center;" rowspan="2">No. Kas</th>
								<th style="width: 60px;text-align: center;" rowspan="2">Tgl Kas</th>
								<th style="text-align: center;" colspan="3">Kontribusi</th>
								<th style="width: 20px;text-align: center;" rowspan="2">Umur (hari)</th>
							</tr>
							<tr>
								<th style="width: 20px;text-align: center;">Nama</th>
								<th style="width: 20px;text-align: center;">Nilai</th>
								<th style="width: 20px;text-align: center;">Catatan</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th style="text-align: center;">NO.</th>
								<th style="text-align: center;">NO. KASBON</th>
								<th style="text-align: center;">TGL KASBON</th>
								<th style="text-align: center;">KETERANGAN</th>
								<th style="text-align: center;">NO. KAS</th>
								<th style="text-align: center;">TGL KAS</th>
								<th style="text-align: center;">NAMA</th>
								<th style="text-align: center;">NILAI</th>
								<th style="text-align: center;">CATATAN</th>
								<th style="text-align: center;">UMUR (HARI)</th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>



<script type="text/javascript">

	//scroll to top page
	//$('html,body').scrollTop(0);
	//$('html, body').animate({ scrollTop: 0 }, 'fast');


	$(document).ready(function () {
		$(".btn-tampil").click(function(){
				table.ajax.reload();
		});


		var column = [];

		column.push({
				"aTargets": [ 7,9 ],
				"mRender": function (data, type, full) {
						//return type === 'export' ? data : numeral(data).format('0,0');
						return type === 'export' ? data : numeral(data).format('0,0');
				},
				"sClass": "right"
		});



		column.push({
				"aTargets": [ 2,5 ],
				"mRender": function (data, type, full) {
						return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
//                return type === 'export' ? data : moment(data).format('L');
				},
				"sClass": "center"
		});


		column.push({
				"aTargets": [ 0 ],
				"searchable": false,
				"orderable": false
		});


		table = $('.dataTable').DataTable({
				"aoColumnDefs": column,
				"order": [[ 1, 'asc' ]],
				"fixedColumns": {
						leftColumns: 2
				},
				"lengthMenu": [[-1], ["Semua Data"]],
				//"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],

				//"bLengthChange": false,
				//"bPaging": false,
				"bPaginate": false,
				//"bInfo": false,

				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"fnServerData": function ( sSource, aoData, fnCallback ) {
						aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
									,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
						$.ajax( {
								"dataType": 'json',
								"type": "GET",
								"url": sSource,
								"data": aoData,
								"success": fnCallback
						} );
				},
				'rowCallback': function(row, data, index){
						//if(data[23]){
								//$(row).find('td:eq(23)').css('background-color', '#ff9933');
						//}
				},
				"sAjaxSource": "<?=site_url('rptkevent/json_dgview');?>",
				"oLanguage": {
						"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
				},
				//dom: '<"html5buttons"B>lTfgitp',
				buttons: [
						{extend: 'copy',
								exportOptions: {orthogonal: 'export'}},
						{extend: 'csv',
								exportOptions: {orthogonal: 'export'}},
						{extend: 'excel',
								exportOptions: {orthogonal: 'export'}},
						{extend: 'pdf',
								orientation: 'landscape',
								pageSize: 'A3'
						},
						{extend: 'print',
								customize: function (win){
											 $(win.document.body).addClass('white-bg');
											 $(win.document.body).css('font-size', '10px');
											 $(win.document.body).find('table')
															 .addClass('compact')
															 .css('font-size', 'inherit');
							 }
						}
				],
				"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});


		//row number
		table.on( 'order.dt search.dt', function () {
						table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
								cell.innerHTML = i+1;
						} );
				} ).draw();



		$('.dataTable').tooltip({
				selector: "[data-toggle=tooltip]",
				container: "body"
		});

		$('.dataTable tfoot th').each( function () {
				var title = $('.dataTable thead th').eq( $(this).index() ).text();
				if(title!=="Edit" && title!=="Delete" && title.toUpperCase()!=="NO." ){
						$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
				}else{
						$(this).html( '' );
				}
		} );

		table.columns().every( function () {
				var that = this;
				$( 'input', this.footer() ).on( 'keyup change', function (ev) {
						//if (ev.keyCode == 13) { //only on enter keypress (code 13)
								that
										.search( this.value )
										.draw();
						//}
				} );
		});
	});
</script>

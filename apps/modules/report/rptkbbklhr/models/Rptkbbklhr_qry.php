<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptkbbklhr_qry
 *
 * @author adi
 */
class Rptkbbklhr_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function refKasBank() {
      $kddiv = $this->session->userdata('data')['kddiv'];
      if ($kddiv==="ZPH01.02S") {
        $kddiv = "";
      } else {
        $kddiv = "and kddiv ='".$this->session->userdata('data')['kddiv']."'";
      }
    $q = $this->db->query("select kdkb, nmkb, tglkb from bkl.v_kasbank where faktif = 't' ".$kddiv."");
      // echo $this->db->last_query();
        return $q->result_array();
    }

    public function tglkb() {
        $kdkb = $this->input->post('kdkb');
        $kddiv = $this->session->userdata('data')['kddiv'];
        if ($kddiv==="ZPH01.02S") {
          $kddiv = "";
        } else {
          $kddiv = "and kddiv ='".$this->session->userdata('data')['kddiv']."'";
        }
        $q = $this->db->query("select kdkb, nmkb, tglkb from bkl.v_kasbank where kdkb = '".$kdkb."' ".$kddiv."");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return json_encode($res);
    }

    public function get_data() {
        $kdkb = $this->input->post('kdkb');
        $periode_awal = $this->input->post('periode_awal');
        $str = "select * from bkl.vl_kas_harian where kdkb = '".$kdkb."' and tglkb = '".$this->apps->dateConvert($periode_awal)."'";
        $q = $this->db->query($str);
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function get_jml() {
        $kdkb = $this->input->post('kdkb');
        $periode_awal = $this->input->post('periode_awal');
        $str = "select sum(debet) as debet, sum(kredit) as kredit, sum(so_awal) as saldo from bkl.vl_kas_harian where kdkb = '".$kdkb."' and tglkb = '".$this->apps->dateConvert($periode_awal)."'";
        $q = $this->db->query($str);
        // echo $this->db->last_query();
        $jml = $q->result_array();
        return $jml;
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['periode']) ){
            if($_GET['periode']){
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode = $this->apps->dateConvert($_GET['periode']);//$tgl1[1].$tgl1[0];
            }else{
                $periode = '';
            }
        }else{
            $periode = '';
        }

        if( isset($_GET['kdkb']) ){
            if($_GET['kdkb']){
                $kdkb = $_GET['kdkb'];
            }else{
                $kdkb = '';
            }
        }else{
            $kdkb = '';
        }

        $aColumns = array('nokb',
                        'tglkb',
                        'nocetak',
                        'nofaktur',
                        'drkpd',
                        'ket',
                        'statpos',
                        'debit',
                        'kredit',
                        'so_awal',
                        'nourut',
                    );
	$sIndexColumn = "nokb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT
                        nocetak,
                        nokb,
                        tglkb,
                        kdkb,
                        nmkb,
                        jnstrx,
                        jenis,
                        nofaktur,
                        drkpd,
                        ket,
                        so_awal,
                        debet as debit,
                        kredit,
                        ordered,
                        statpos,
                        nourut
                      FROM bkl.vl_kas_harian
                      WHERE kdkb = '".$kdkb."' AND tglkb = '".$periode."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "ORDER BY nokb,tglkb,nocetak,ordered,nourut";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = "";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

//        echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        $saldo = 0;
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '';
                    }
                    if(($i>=6 && $i<=8)){
                        $row[] = (float) $aRow[ $aColumns[$i] ];

                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                //23 - 28
                $saldo = $saldo + ($aRow['so_awal'] + $aRow['debit'] - $aRow['kredit']);
                $row[8] = $saldo; //number_format($saldo, 2);
               $output['aaData'][] = $row;
	}
	echo  json_encode( $output );
    }

    public function submit() {
        try {
            $array = $this->input->post();
            if(empty($array['id'])){
                unset($array['id']);
                $resl = $this->db->insert('menus',$array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && empty($array['stat'])){
                if($array['mainmenuid']=="" || empty($array['mainmenuid'])){
                    unset($array['mainmenuid']);
                    $this->db->set('mainmenuid', 'NULL', FALSE);
                }
                $this->db->where('id', $array['id']);
                $resl = $this->db->update('menus', $array);
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && !empty($array['stat'])){
                $this->db->where('id', $array['id']);
                $resl = $this->db->delete('menus');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }
            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

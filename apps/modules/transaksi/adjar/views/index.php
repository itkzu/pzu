<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-xs-3 box-header box-view">
            <button type="button" class="btn btn-primary btn-add">Tambah</button>
            <button type="button" class="btn btn-primary btn-edit">Ubah</button>
            <button type="button" class="btn btn-primary btn-del">Hapus</button>
              <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
          </div>
          <div class="col-xs-12 box-header box-new">
            <button type="button" class="btn btn-primary btn-save">Simpan</button>
            <button type="button" class="btn btn-danger btn-cancel">Batal</button>
          </div>

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['noaradj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-6"> 
                                  <?php
                                      echo form_input($form['noaradj']);
                                      echo form_error('noaradj','<div class="note">','</div>');
                                  ?> 
                              </div>

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglaradj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-6"> 
                                  <?php
                                      echo form_input($form['tglaradj']);
                                      echo form_error('tglaradj','<div class="note">','</div>');
                                  ?> 
                              </div>

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <br>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-5 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['noso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['noso']);
                                        echo form_error('noso','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                    <?php echo form_label($form['nodo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['nodo']);
                                        echo form_error('nodo','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-5 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['tglso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['tglso']);
                                        echo form_error('tglso','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                    <?php echo form_label($form['tgldo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['tgldo']);
                                        echo form_error('tgldo','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row"> 
                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['jnsbayar']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['jnsbayar']);
                                        echo form_error('jnsbayar','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['nama']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['nama']);
                                        echo form_error('nama','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['jenis']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php 
                                        echo form_dropdown( $form['jenis']['name'],
                                                            $form['jenis']['data'] ,
                                                            $form['jenis']['value'] ,
                                                            $form['jenis']['attr']);
                                        echo form_error('jenis','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['nama_s']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['nama_s']);
                                        echo form_error('nama_s','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['nilai_ar']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['nilai_ar']);
                                        echo form_error('nilai_ar','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['alamat']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['alamat']);
                                        echo form_error('alamat','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['nilai_byr']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['nilai_byr']);
                                        echo form_error('nilai_byr','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row"> 

<!--                               <div class="col-xs-12"> 
                                    <?php
                                        echo form_input($form['kdjns']);
                                        echo form_error('kdjns','<div class="note">','</div>');
                                    ?> 
                              </div> -->

                          </div>
                        </div>
                        
                      <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['nilai_adj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['nilai_adj']);
                                        echo form_error('nilai_adj','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row"> 

                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['saldo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['saldo']);
                                        echo form_error('saldo','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-12 col-md-12 ">
                          <div class="row">

                              <div class="col-xs-1 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['ket']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-11"> 
                                    <?php
                                        echo form_input($form['ket']);
                                        echo form_error('ket','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-2 col-md-2 ">
                          <div class="row"> 

                              <div class="col-xs-12"> 
                                    <?php
                                        echo form_input($form['status']);
                                        echo form_error('status','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-12">
                          <label>&nbsp;</label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div> 
                    </div>
                  </div> 
              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        clear();
        $("#noaradj").mask("PAR-****-****");
        $("#status").val("0");
        autonum();
        disabled();
        getjenis();
        set_kode();

        $('#noaradj').keyup(function () {
            set_noaradj();
        });

        $('#nodo').keyup(function () {
            var stat = $("#status").val();
            if(stat==='1'){
                var nodo = $("#nodo").val();
                var nodo = nodo.toUpperCase();
                var noso = $("#noso").val();
                var noso = noso.toUpperCase();
                var ket = 'nodo'; 
                set_nodo(nodo,noso,ket);
            }
        });

        $('#noso').keyup(function () {
            var stat = $("#status").val();
            if(stat==='1'){
                var nodo = $("#nodo").val();
                var nodo = nodo.toUpperCase();
                var noso = $("#noso").val();
                var noso = noso.toUpperCase();
                var ket = 'noso'; 
                set_nodo(nodo,noso,ket);
            } 
        });

        $('#jenis').change(function () {
            set_harga();
        });

        $('#nilai_adj').keyup(function () {
            sum_saldo();
        });

        $('.btn-add').click(function () {
          clear();
          $("#status").val('1');
            enabled();
            $("#noaradj").prop('disabled',true); 
            $("#noaradj").mask('PAR-****-****'); 
        }); 

        // $('.btn-edit').click(function () {
        //   $("#ket").val('2');
        //     enabled();
        //     $("#nodo").attr('disabled',true); 
        // });

        $('.btn-save').click(function () {
            simpan();
        });

        $('.btn-cancel').click(function () {
          swal({
              title: "Input data akan dibatalkan!",
              text: "Data tidak akan Tersimpan!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#c9302c",
              confirmButtonText: "Ya, Lanjutkan!",
              cancelButtonText: "Batalkan!",
              closeOnConfirm: false
          }, function () {
            window.location.reload();
          });
        });
    });

    function disabled(){
      $(".box-new").hide();
      $(".box-view").show(); 
      $("#noaradj").prop('disabled',false);
      $("#tglaradj").attr('disabled',true);
      $("#nodo").prop('disabled',true);
      $("#noso").prop('disabled',true);
      $("#nilai_adj").prop('disabled',true);
      $("#jenis").prop('disabled',true);
      $("#ket").prop('disabled',true);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){
      $(".box-view").hide();
      $(".box-new").show();
      $("#noaradj").prop('disabled',true);
      $("#tglaradj").attr('disabled',false);
      $("#nodo").prop('disabled',false);
      $("#noso").prop('disabled',false);
      $("#nilai_adj").prop('disabled',true);
      $("#jenis").prop('disabled',true);
      $("#ket").prop('disabled',true);
      $("#ket_ar").attr("disabled",false);
      $(".btn-edit").prop('disabled',true);
      // $(".btn-batal").show();
    }

    function clear(){
      $("#noaradj").val('');
      $('#tglaradj').val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $("#nodo").val('');
      $("#noso").val('');
      $('#tglso').val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $('#tgldo').val($.datepicker.formatDate('dd-mm-yy', new Date()));  
      $("#nama").val('');
      $("#nama_s").val('');
      $("#alamat").val('');
      $("#jnsbayar").val('');
      $("#jenis").val('');
      $("#ket").val('');
      $("#nilai_ar").val('');
      $("#nilai_byr").val('');
      $("#nilai_adj").val('');
      $("#saldo").val('');
    }

    function empty(){ 
      $('#tglaradj').val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $("#nodo").val('');
      $("#noso").val('');
      $('#tglso').val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $('#tgldo').val($.datepicker.formatDate('dd-mm-yy', new Date()));  
      $("#nama").val('');
      $("#nama_s").val('');
      $("#alamat").val('');
      $("#jnsbayar").val('');
      $("#jenis").val('');
      $("#ket").val('');
      $("#nilai_ar").val('');
      $("#nilai_byr").val('');
      $("#nilai_adj").val('');
      $("#saldo").val('');
    }

    function empty2(){  
      // $("#nodo").val('');
      // $("#noso").val('');
      $('#tglso').val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $('#tgldo').val($.datepicker.formatDate('dd-mm-yy', new Date()));  
      $("#nama").val('');
      $("#nama_s").val('');
      $("#alamat").val('');
      $("#jnsbayar").val('');
      $("#jenis").val('');
    }

    function empty_nominal(){   
      $("#nilai_ar").autoNumeric('set',0);
      $("#nilai_byr").autoNumeric('set',0); 
      $("#saldo").autoNumeric('set',0);
    }

    function autonum(){
      $('#nilai_ar').autoNumeric('init',{ currencySymbol : 'Rp.'});
      $('#nilai_byr').autoNumeric('init',{ currencySymbol : 'Rp.'});
      $('#nilai_adj').autoNumeric('init',{ currencySymbol : 'Rp.'});
      $('#saldo').autoNumeric('init',{ currencySymbol : 'Rp.'}); 
    }

    function set_kode(){
        var tg = new Date();
        var tgl = tg.toString();
        var tanggal = tgl.substring(13,15);
        $.ajax({
            type: "POST",
            url: "<?=site_url("adjar/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#nodo").mask(data.kode+"99-999999");
                      $("#noso").mask(data.kode+"99-999999");
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function getjenis(){
       //alert(kddiv);
       var jenis = $('#jnsbayar').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("adjar/getjenis");?>",
            data: { "jenis" : jenis },
            beforeSend: function() {
                $('#jenis').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis --'));
                $("#jenis").trigger("change.chosen");
                if ($('#jenis').hasClass("chosen-hidden-accessible")) {
                    $('#jenis').select2('destroy');
                    $("#jenis").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#jenis')
                        .append($('<option>', { value : value.kdjar })
                        .html("<b style='font-size: 14px;'>" + value.nmjar + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmaks + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_noaradj(){

      var noaradj = $("#noaradj").val();
      var noaradj = noaradj.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("adjar/set_noaradj");?>",
            data: {"noaradj":noaradj },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  empty(); 
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#tglaradj").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglaradj)));
                      $("#noso").val(data.noso);
                      $("#nodo").val(data.nodo);
                      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                      $("#nama").val(data.nama);
                      $("#nama_s").val(data.nama_s);
                      $("#alamat").val(data.alamat);
                      $("#jnsbayar").val(data.nmleasingx);
                      $("#kdjns").val(data.jenis);
                      $("#jenis").val(data.jenis);
                      $("#jenis").trigger("change");
                      $('#jenis').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#ket").val(data.ket); 
                      $('#nilai_adj').autoNumeric('set',data.nilai); 
                      $(".btn-edit").attr('disabled',false);
                      $(".btn-del").attr('disabled',false);
                      $(".btn-batal").show();
                      set_harga();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_harga(){

      var jenis = $("#jenis").val();
      var jenis = jenis.toUpperCase(); 
      var noso = $("#noso").val();
      var noso = noso.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("adjar/set_harga");?>",
            data: {"jenis":jenis, "noso":noso },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  empty_nominal(); 
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){   
                        $('#nilai_ar').autoNumeric('set',data.nilai);  
                        $('#nilai_byr').autoNumeric('set',data.byr); 
                      sum_saldo();
                      $(".btn-edit").attr('disabled',false);
                      $(".btn-batal").show();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function sum_saldo(){
        var nar = $('#nilai_ar').autoNumeric('get'); 
        var nbay = $('#nilai_byr').autoNumeric('get'); 
        var nadj = $("#nilai_adj").autoNumeric('get');
        if(nadj===''){
            var nadj = '0';
        }

        var saldo = parseInt(nar) - parseInt(nbay) - parseInt(nadj);
        if(!isNaN(saldo)){
            $("#saldo").autoNumeric('set',saldo);          
        }
        
    }

    function set_nodo(nodo,noso,ket){

        $.ajax({
            type: "POST",
            url: "<?=site_url("adjar/set_nodo");?>",
            data: {"nodo":nodo,"noso":noso,"ket":ket },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  empty2(); 
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                      if(ket==='noso'){
                          $("#nodo").val(data.nodo);
                      } else {
                          $("#noso").val(data.noso);
                      }
                      $("#nama").val(data.nama);
                      $("#nama_s").val(data.nama_s);
                      $("#alamat").val(data.alamat_s);
                      $("#jnsbayar").val(data.nmleasingx); 
                      getjenis(); 

                      $("#jenis").attr('disabled',false); 
                      $("#nilai_adj").attr('disabled',false); 
                      $("#ket").attr('disabled',false); 
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function simpan(){
      var tglaradj = $("#tglaradj").val();
      var nodo = $("#nodo").val();
      var nodo = nodo.toUpperCase();
      var noso = $("#noso").val();
      var noso = noso.toUpperCase();
      var jenis = $("#jenis").val();
      var nilai = $("#nilai_adj").autoNumeric('get');
      var ket = $("#ket").val();
      var ket = ket.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("adjar/simpan");?>",
            data: {"tglaradj":tglaradj ,"nodo":nodo ,"noso":noso ,"jenis":jenis ,"nilai":nilai ,"ket":ket },
            success: function(resp){
              if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    });
                      clear();
                      disabled();
                      $('#noaradj').val(data.noaradj);
                      set_noaradj();
              } else {
                    swal({
                        title: "Data Tidak Disimpan",
                        text: "Data Gagal",
                        type: "error"
                    });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
</script>

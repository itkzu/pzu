<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-lg-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-md-12 box-header box-view">
              <div class="col-md-3">
                <div class="row">
                    <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a>
                    <a class="btn btn-primary btn-edit">Ubah</a>
                    <a class="btn btn-danger btn-del">Hapus</a> 
                </div>
              </div>
              <div class="col-md-2">
                <div class="row"> 
                </div>
              </div>
              <div class="col-md-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak</button>
                </div>
              </div>
          </div>  

          <div class="col-md-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['noabj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-9">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['noabj']);
                                          echo form_error('noabj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglabj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tglabj']);
                                          echo form_error('tglabj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row"> 
                      <div class="col-md-12">
                          <label></label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Kwitansi</th> 
                            <th style="text-align: center;">Nama Konsumen</th> 
                            <th style="text-align: center;">Alamat Konsumen</th> 
                            <th style="text-align: center;">Tipe Unit</th> 
                            <th style="text-align: center;">No. Mesin</th> 
                            <th style="text-align: center;">No. DO</th> 
                            <th style="text-align: center;">Tgl. DO</th> 
                            <th style="text-align: center;">Trm Faktur</th>  
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                            <th style="text-align: center;"></th> 
                          </tr>
                        </tfoot>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        disabled(); 
        $('#noabj').mask('ABJ-9999-9999');

        $('#noabj').keyup(function () {
            set_noabj();
        }); 

        $('.btn-ctk').click(function(){
            var noabj = $('#noabj').val();
                        // var lik = nokps.replace("/","+");
                        // var lik = lik.replace("/","+");
                        // var lik = lik.replace("/","+");
            window.open('ajubbn/ctk/'+noabj, '_blank');
            // window.location.href = 'lrnew/ctk_lr/'+nodo;

            // ctk_lr();
        });
    });

    function disabled(){  
      $("#tglabj").attr('disabled',true); 
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',true);
      $(".btn-del").attr('disabled',true);
      $(".btn-ctk").attr('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){  
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',false);
      $(".btn-del").attr('disabled',false);
      $(".btn-ctk").attr('disabled',false); 
    }

    function clear(){ 
      $("#tglabj").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
    }

    function reset(){
      $("#noabj").val('');
      $("#tglabj").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
    } 

    function set_noabj(){

      var noabj = $("#noabj").val();
      var noabj = noabj.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("ajubbn/set_noabj");?>",
            data: {"noabj":noabj },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  clear();
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                    $("#tglabj").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglbbn_aju))); 
                    $(".btn-ctk").attr('disabled',false);
                    $(".btn-edit").attr('disabled',false);
                    $(".btn-del").attr('disabled',false);
                    tb_det();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function tb_det(){ 
      table = $('.dataTable').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [ 
                {
                    "aTargets": [ 7,8 ],
                    "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                    },
                    "sClass": "center"
                    
                }],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nokps" },
                { "data": "nama" },
                { "data": "alamat"},
                { "data": "nmtipe" },
                { "data": "nosin"},
                { "data": "nodo"},
                { "data": "tgldo"},
                { "data": "tgl_trm_fa"},
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "noabj", "value": $("#noabj").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('ajubbn/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="No." ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });   
    }
</script>

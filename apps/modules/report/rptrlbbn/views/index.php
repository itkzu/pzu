<?php
/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'target' => '_blank'
                , 'data-validate' => 'parsley');
            echo form_open($submit, $attributes);
            ?> 
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">    
                            <?=form_label('Periode Awal');?>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" name="chk_periode" id="chk_periode" value="periode" checked="">
                                </span>
                                <?=form_input($form['periode_awal']);?>
                            </div>
                            <?php
                            echo form_error('periode_awal', '<div class="note">', '</div>');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">    
                            <?php
                            echo form_label('Periode Akhir');
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir', '<div class="note">', '</div>');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">    
                                    <?= form_label($form['tbj']['placeholder']); ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" name="chk_tbj" id="chk_tbj" value="tbj">
                                        </span>
                                        <?= form_input($form['tbj']); ?>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">    
                                    <?= form_label($form['do']['placeholder']); ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" name="chk_do" id="chk_do" value="do">
                                        </span>
                                        <?= form_input($form['do']); ?>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="submit" value="html">
                        <i class="fa fa-print"></i> Cetak
                    </button>
                    <button type="submit" class="btn btn-success" name="submit" value="excel">
                        <i class="fa fa-file-excel-o"></i> Export Ke Excel
                    </button>
                    <a href="<?php echo $reload; ?>" class="btn btn-default">
                        Batal
                    </a>    
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbj").mask("TBJ-9999-9999");
        $("#do").mask("S99-999999");
        $('#tbj').attr("disabled",true);
        $('#do').attr("disabled",true);
        
        $('#periode_awal').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        
        $('#periode_akhir').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        
        $("#chk_periode").click(function (e) {
            var checked = $(this).prop("checked");
            // Jika riwayat Alergi ada maka harus disebutkan
            if(checked){
                $('#periode_awal').attr("disabled",false).focus();
                $('#periode_akhir').attr("disabled",false);
                
                $("#chk_do").prop("checked",false);
                $("#chk_tbj").prop("checked",false);
                $('#do').attr("disabled",true);
                $('#tbj').attr("disabled",true);
            }else{
                $('#periode_awal').attr("disabled",true);
                $('#periode_akhir').attr("disabled",true);
            }
        });
        
        $("#chk_tbj").click(function (e) {
            var checked = $(this).prop("checked");
            // Jika riwayat Alergi ada maka harus disebutkan
            if(checked){
                $('#tbj').attr("disabled",false).focus();
                
                $("#chk_periode").prop("checked",false);
                $("#chk_do").prop("checked",false);
                $('#periode_awal').attr("disabled",true);
                $('#periode_akhir').attr("disabled",true);
                $('#do').attr("disabled",true);
            }else{
                $('#tbj').attr("disabled",true);
            }
        });
        
        $("#chk_do").click(function (e) {
            var checked = $(this).prop("checked");
            // Jika riwayat Alergi ada maka harus disebutkan
            if(checked){
                $('#do').attr("disabled",false).focus();
                $("#chk_tbj").prop("checked",false);
                $("#chk_periode").prop("checked",false);
                $('#periode_awal').attr("disabled",true);
                $('#periode_akhir').attr("disabled",true);
                $('#tbj').attr("disabled",true);
            }else{
                $('#do').attr("disabled",true);
            }
        });
    });
</script>
<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Kbins_qry
 *
 * @author adi
 */
class Kbins_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getKB() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        $this->db->order_by('kdkb');
        $this->db->where('faktif',true);
        $q = $this->db->get("pzu.kasbank");
        return $q->result_array();
    }

    public function getjnstrans() {
        $this->db->select("jnstrx,nourut,case when jnstrx like '%(D)%' THEN 'D' ELSE 'K' END AS kd");
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->order_by('nourut');
        $this->db->group_by('jnstrx,nourut');
        $q = $this->db->get("pzu.kasbank_trx");
        return $q->result_array();
    }

    public function proses_dk() {
        $jnstrans = $this->input->post('jnstrans');
        $this->db->select("jnstrx,nourut,case when jnstrx like '%(D)%' THEN 'D' ELSE 'K' END AS kd");
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('jnstrx',$jnstrans);
        $this->db->order_by('nourut');
        $this->db->group_by('jnstrx,nourut');
        $q = $this->db->get("pzu.kasbank_trx");
          if($q->num_rows()>0){
              $res = $q->result_array();
          }else{
              $res = "empty";
          }
          return json_encode($res); 
    }

    public function getKdjenis() {
        $ket = $this->input->post('ket');
        $this->db->select('*');
        $this->db->where('faktif',true);
        $this->db->where('dk',$ket);
        $this->db->order_by('kdrefkb');
        $q = $this->db->get("pzu.refkb_m"); 
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    }

    public function getjnskredit() {
        $this->db->select('kdrefkb as kdkredit,nmrefkb,dk,faktif');
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->where('dk','K');
        $this->db->order_by('kdrefkb');
        $q = $this->db->get("pzu.refkb_m");
        return $q->result_array();
    }

    public function getleas() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->order_by('kdleasing'); 
        $q = $this->db->get("pzu.leasing");
        return $q->result_array();
    }

    public function set_nocetak() {
      $kdkb = $this->input->post('refkb');
      $query = $this->db->query("select max(nocetak),pzu.set_nocetak(max(nocetak)) as nocetak from pzu.t_kb a inner join pzu.t_kb_ar b on a.nokb=b.nokb inner join pzu.kasbank c on a.kdkb=c.kdkb and a.tglkb=c.tglkb where a.kdkb='".$kdkb."' and (b.jenis='PC' or b.jenis='DP') ");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_nokb() {
        $kdkb = $this->input->post('kdkb');
        $tglkb  = $this->apps->dateConvert($this->input->post('tglkb'));
        $query = $this->db->query("select * from pzu.kb_setnokb('".$tglkb."','".$kdkb."') ");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_nokasbon() {
        $nokasbon = $this->input->post('nokasbon'); 
        $query = $this->db->query("select * from pzu.t_kasbon where nokb is null and nokasbon = '".$nokasbon."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_jmlctk() {
      $max = $this->input->post('max');
      $query = $this->db->query("select count(nocetak) from pzu.t_kb where nocetak='".$max."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function get_pkons() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(noso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vm_kb_kons');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'alamat' => $value['alamat'],
                    'nama' => $value['nama']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function get_pleas() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(noso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vm_kb_leas');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'alamat' => $value['alamat'],
                    'nama' => $value['nama']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function get_blkkons() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(noso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vl_ar_kons');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'alamat' => $value['alamat'],
                    'nama' => $value['nama']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function set_pkonsnoso() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from pzu.vm_kb_kons where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pkonsnodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vm_kb_kons where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pleasnoso() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from pzu.vm_kb_leas where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pleasnodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vm_kb_leas where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_blkkonsnoso() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from pzu.vl_ar_kons where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_blkkonsnodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vl_ar_kons where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getNoID() { 
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function ctk($no) {
        $this->db->select("*");
        $this->db->where('nobbn_trm',$no);
        $q = $this->db->get("pzu.vb_lr_bbn");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function set_kb() {
        $nokb   = $this->input->post('nokb');
        $query  = $this->db->query("select * from pzu.t_kb where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pkons() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_pkons where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pleas() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_pleas where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_rleas() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_rleas where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_blkkons() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_blkkons where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function refkb() {
      $kdkb = $this->input->post('refkb');
      $query = $this->db->query("select * from pzu.kasbank where kdkb = '" . $kdkb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    } 

    public function set_kbm() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        $aColumns = array('no',
                             'nmrefkb',
                             'darike',
                             'nofaktur',
                             'ket',
                             'nilai' );
      $sIndexColumn = "nourut";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             kdrefkb,
                             nmrefkb,
                             darike,
                             nofaktur,
                             ket,
                             nilai,
                             nokb,
                             nourut  from pzu.vm_kb_d 
                        where nokb = '".$nokb."' order by nourut) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        // $sWhere = "";

        // if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        // {
        // $sWhere = " WHERE (";
        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //   $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        // }
        // $sWhere = substr_replace( $sWhere, "", -3 );
        // $sWhere .= ')';
        // }

        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //     if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        //     {
        //         $ix = $i - 1;
        //         if ( $sWhere == "" )
        //         {
        //             $sWhere = " WHERE ";
        //         }
        //         else
        //         {
        //             $sWhere .= " AND ";
        //         }
        //         //
        //         $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
        //         //echo $sWhere;
        //     }
        // }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable 
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable ";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable ";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            // $aRow['detail'] = array();

            // foreach ($detail as $value) {
            //     if($aRow['nopo']==$value['nopo']){
            //         $aRow['detail'][]= $value;
            //     }
            // } 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        $aColumns = array('no',
                             'nosin',
                             'kdtipe',
                             'nama',
                             'alamat',
                             'bbn',
                             'jasa',
                             'total',
                             'nodo',
                             'tgldo',
                             'nobbn_trm');
      $sIndexColumn = "nobbn_trm";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             nosin,
                             kdtipe,
                             nama,
                             alamat,
                             bbn,
                             jasa,
                             total,
                             nodo,
                             tgldo,
                             nobbn_trm  from pzu.vm_bbn_trm_d 
                        where nobbn_trm = '".$nokb."' order by nobbn_trm) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            // $aRow['detail'] = array();

            // foreach ($detail as $value) {
            //     if($aRow['nopo']==$value['nopo']){
            //         $aRow['detail'][]= $value;
            //     }
            // } 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_det() {
        error_reporting(-1);
        $aColumns = array('no', 
                             'nodo',
                             'nokps',
                             'nama',
                             'alamat',
                             'nmtipe',
                             'nosin',
                             'nodo',
                             'tgldo',
                             'tgl_trm_fa');
      $sIndexColumn = "nokps";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( ) AS a";
        

        $aColumns = array(  'no', 
                             'nodo',
                             'nokps',
                             'nama',
                             'alamat',
                             'nmtipe',
                             'nosin',
                             'nodo',
                             'tgldo',
                             'tgl_trm_fa'
        );

        $sIndexColumn = "nodo";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //row_number() over(order by nodo) as no
        $sTable = " (select '1' as no, 
                             nokps,
                             nama,
                             alamat,
                             nmtipe,
                             nosin,
                             nodo,
                             tgldo,
                             tgl_trm_fa from pzu.v_bbn_aju_det order by nokps,tglkps) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if($aRow[ $aColumns[$i] ]===null){
                    $aRow[ $aColumns[$i] ] = '';
                }

                if(is_numeric($aRow[ $aColumns[$i] ])){
                    $row[] = floatval($aRow[ $aColumns[$i] ]);
                }else{
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
//            $row[6] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"" . site_url('ajufa/edit/' . $aRow['kode']) . "\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
//            $row[6] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('" . $aRow['kode'] . "');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";
            $output['aaData'][] = $row; 
        }
        echo  json_encode( $output );

    }

    public function kb_json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        if( isset($_GET['jnstrans']) ){
            if($_GET['jnstrans']){
                $jnstrans = $_GET['jnstrans'];
            }else{
                $jnstrans = '';
            }
        }else{
            $jnstrans = '';
        }
        $aColumns = array('no',
                             'nourut',
                             'nmrefkb',
                             'darike',
                             'nofaktur',
                             'ket',
                             'nilai');
      $sIndexColumn = "nourut";
        $sLimit = "";
        $dk = '';
        if ($jnstrans==='K/B MASUK - UMUM (D)') {
          $dk = 'D';
        } else {
          $dk = 'K';
        }
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             a.nourut,
                             a.kdrefkb,
                             b.nmrefkb,
                             a.darike,
                             a.nofaktur,
                             a.ket,
                             a.nilai from pzu.t_kb_d_tmp a LEFT JOIN pzu.refkb_m b ON a.kdrefkb = b.kdrefkb 
                        where a.nokb = '".$this->session->userdata("username")."'||substring('".$nokb."',6,10) and a.dk = '".$dk."' and a.userentry = '".$this->session->userdata("username")."' order by a.nourut) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function kasbon_json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        if( isset($_GET['jnstrans']) ){
            if($_GET['jnstrans']){
                $jnstrans = $_GET['jnstrans'];
            }else{
                $jnstrans = '';
            }
        }else{
            $jnstrans = '';
        }
        $aColumns = array('nokasbon',
                             'no',
                             'nokasbon',
                             'tglkasbon',
                             'nilai',
                             'nama',
                             'ket',
                             'kdkb');
      $sIndexColumn = "nokasbon";
        $sLimit = "";
        $dk = ''; 
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             nokasbon,
                             tglkasbon,
                             nilai,
                             nama,
                             ket,
                             kdkb from pzu.t_kasbon_tmp where userentry = '".$this->session->userdata("username")."' order by nokasbon) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function tambah() {
        $nobbn_trm = $this->input->post('nobbn_trm');
        $tglbbn_trm = $this->apps->dateConvert($this->input->post('tglbbn_trm')); 
        $nodo = $this->input->post('nodo');
        $nilai = $this->input->post('nilai');
        $jasa = $this->input->post('jasa');
        $q = $this->db->query("select * from pzu.tbj_ins_d('".$nobbn_trm."','".$tglbbn_trm."','".$nodo."',".$nilai.",".$jasa.",'".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function kbm_submit() {  
        $nokb   = $this->input->post('nokb');  
        $nourut   = $this->input->post('nourut');  
        if ($nourut==='') {
          $nourut = 0;
        }
        $kdrefkb    = $this->input->post('kdrefkb');
        $drkpd    = $this->input->post('drkpd');
        $nofaktur = $this->input->post('nofaktur');
        $ket  = $this->input->post('ket');
        $nilai = $this->input->post('nilai');
        $jnstrans = $this->input->post('jnstrans');
        $dk = '';
        if ($jnstrans==='K/B MASUK - UMUM (D)') {
          $dk = 'D';
        } else {
          $dk = 'K';
        }
        $q = $this->db->query("select * from pzu.tmpkb_ins('".$nokb."',".$nourut.",".$kdrefkb.",'".$dk."','".$nofaktur."','".$drkpd."',".$nilai.",'".$ket."','".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function kbm_del() {  
        $nokb   = $this->input->post('nokb');  
        $nourut   = $this->input->post('nourut');    
        $q = $this->db->query("select * from pzu.tmpkb_del('".$nokb."',".$nourut.",'".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function kasbon_del() {   

        try {   
            $nokasbon   = $this->input->post('nokasbon');    

            $q = $this->db->query("delete from pzu.t_kasbon_tmp where nokasbon = '".$nokasbon."' and userentry = '".$this->session->userdata("username")."'");

                // echo $this->db->last_query();
            if (!$q) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "success";
                $this->state = "1";
            } 
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr); 
    } 

    public function kbsubmit() {   
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb    = $this->input->post('kdkb');
        $jnstrans    = $this->input->post('jnstrans');
        $dk = $this->input->post('dk');
        $ket  = $this->input->post('ket'); 
        $q = $this->db->query("select * from pzu.kb_ins_w('".$tglkb."','".$kdkb."','".$jnstrans."','".$dk."','".$ket."','".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function rkasbon_submit() {   
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb    = $this->input->post('kdkb');
        $jnstrans    = $this->input->post('jnstrans');
        $dk = $this->input->post('dk');
        $ket  = $this->input->post('ket'); 
        $q = $this->db->query("select * from pzu.kb_kasbon_ins_w('".$tglkb."','".$kdkb."','".$jnstrans."','".$dk."','".$ket."','','".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function delDetail() {
        $nobbn_trm         = $this->input->post('nobbn_trm');
        $nodo         = $this->input->post('nodo'); 
        $q = $this->db->query("select title,msg,tipe from pzu.tbj_del_d( '". $nobbn_trm ."',
                                                                        '". $nodo ."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    public function rkasbon_ins() { 

        try {   
            $nokasbon   = $this->input->post('nokasbon');  
            $nama       = $this->input->post('nama'); 
            $tglkb      = $this->apps->dateConvert($this->input->post('tglkb')); 
            $kdkb       = $this->input->post('kdkb'); 
            $ket        = $this->input->post('ket'); 
            $nilai      = $this->input->post('nilai'); 

            $q = $this->db->query("insert into pzu.t_kasbon_tmp(nokasbon,tglkasbon,nilai,kdkb,ket,nama,userentry) values ('".$nokasbon."','".$tglkb."',".$nilai.",'".$kdkb."','".$ket."','".$nama."','".$this->session->userdata("username")."')");

                // echo $this->db->last_query();
            if (!$q) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "success";
                $this->state = "1";
            } 
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);  
    } 

    public function pkons_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $noso           = $this->input->post('noso');
        $nilaibyr       = $this->input->post('nilaibyr');
        $jenis          = $this->input->post('jenis');
        $nocetak        = $this->input->post('nocetak');   
        $q = $this->db->query("select * from pzu.kb_arkons_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $noso ."',
                                                                ". $nilaibyr .",
                                                                '". $jenis ."',
                                                                '". $nocetak ."', 
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function pleas_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $noso           = $this->input->post('noso');
        $nilai_pl       = $this->input->post('nilai_pl');
        $nilai_jp       = $this->input->post('nilai_jp'); 
        $nocetak        = $this->input->post('nocetak');   
        $q = $this->db->query("select * from pzu.kb_arleas_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $noso ."',
                                                                ". $nilai_pl .", 
                                                                ". $nilai_jp .", 
                                                                '". $nocetak ."', 
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function blkkons_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $noso           = $this->input->post('noso');
        $nilaibyr       = $this->input->post('nilaibyr');  
        $q = $this->db->query("select * from pzu.kb_retkons_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $noso ."',
                                                                ". $nilaibyr .",   
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function rleas_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $jenis          = $this->input->post('jenis');
        $kdleasing      = $this->input->post('kdleasing');
        $nilaibyr       = $this->input->post('nilaibyr'); 
        $ket_det        = $this->input->post('ket_det');   
        $q = $this->db->query("select * from pzu.kb_leasing_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $jenis ."',
                                                                '". $kdleasing ."', 
                                                                ". $nilaibyr .", 
                                                                '". $ket_det ."', 
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function batal() {     
        $q = $this->db->query("select * from pzu.kb_btl('".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    //cetak
    public function html($nokb,$tglkb) {
        $tgl          = $this->apps->dateConvert($tglkb);
        // $q = $this->db->query("select * from pzu.vb_kb WHERE nokb = upper('".$nokb."') order by nourut");
        $q = $this->db->query("SELECT ROW_NUMBER() OVER (ORDER BY nokb ASC) AS nourut2, * FROM pzu.vb_kb WHERE nocetak = (SELECT nocetak FROM pzu.vb_kb where nokb = upper('".$nokb."') ) and tglkb = '".$tgl."'");
        // echo $this->db->last_query();

        // $this->db->select("to_char(now(),'HH:MI:SS') as wkt, *");
        // $this->db->where('nodo',upper('.$nodo.'));
        // $q = $this->db->get("pzu.vb_do");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }
}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Acrdf_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getData() {
		error_reporting(-1);

        //$nokb = $this->input->post('nokb');
            /*
      		if( isset($_GET['nokb']) ){
      			if($_GET['nokb']){
      				$nokb = $_GET['nokb'];
      			}else{
      				$nokb = '';
      			}
      		}else{
      			$nokb = '';
              }
        */
		$aColumns = array('nopo',
                      'tglpo',
						          'nap','x');

		$sIndexColumn = "nopo";

		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}

		$sTable = " (SELECT nopo, tglpo, nap, '' as x
					    FROM pzu.vm_acrdf ORDER BY nopo
					) AS a";


		//$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
		$sWhere = "";

		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
					$sOrder = "";
			}
		}

		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{

			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}


		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";

        //echo $sQuery;

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";	//SELECT FOUND_ROWS()

              //  echo $sQuery;
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

                //    echo $sQuery;
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if($aRow[ $aColumns[$i] ]===null){
					$aRow[ $aColumns[$i] ] = '';
				}
				if(is_numeric($aRow[ $aColumns[$i] ])){
					$row[] = floatval($aRow[ $aColumns[$i] ]);
				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$row[3] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"proses('".$aRow['nopo']."');\">Process</button>";
			$output['aaData'][] = $row;
		}
		return json_encode( $output );
	}

    public function proses() {
        $nopo = $this->input->post('nopo');
		$q = $this->db->query("select title,msg,tipe from pzu.acrdf_ins('". $nopo ."','".$this->session->userdata("username")."')");
		//echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 
}

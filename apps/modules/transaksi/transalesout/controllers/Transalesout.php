<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Transalesout
 *
 * @author adi
 */
class Transalesout extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('transalesout/submit'),
            'add' => site_url('transalesout/add'),
            'edit' => site_url('transalesout/edit'),
            'reload' => site_url('transalesout'),
        );
        $this->load->model('transalesout_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->transalesout_qry->json_dgview();
    }
    
    public function submit() {  
        $kdsales = $this->input->post('kdsales');
        $stat = $this->input->post('stat');
        if($this->validate($kdsales,$stat) == TRUE){
            $res = $this->transalesout_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($kdsales)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($kdsales);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($kdsales)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_init_edit();
                $this->_check_id($kdsales);
                $this->template->build('form', $this->data);
            }
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'kdsales'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kdsales',
                    'name'        => 'kdsales',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'tgl_out'=> array(
                    'placeholder' => 'Tanggal Keluar',
                    'id'          => 'tgl_out',
                    'name'        => 'tgl_out',
                    'value'       => set_value('tgl_out'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
        );
    }
    
    private function _check_id($kdsales){
        if(empty($kdsales)){
            redirect($this->data['add']);
        }
        
        $this->val= $this->transalesout_qry->select_data($kdsales);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($kdsales,$stat) {
        if(!empty($kdsales) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nmlengkap',
                    'label' => 'Nama Lengkap Sales',
                    'rules' => 'required|max_length[100]',
                ),
            array(
                    'field' => 'nmsales',
                    'label' => 'Nama Cetak Sales',
                    'rules' => 'required|max_length[100]',
                ),
            array(
                    'field' => 'status',
                    'label' => 'Jabatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kdsales_header',
                    'label' => 'Nama Atasan/SPV',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'kddiv',
                    'label' => 'Nama POS/Cabang',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgl_in',
                    'label' => 'Tanggal Masuk Sales',
                    'rules' => 'required',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

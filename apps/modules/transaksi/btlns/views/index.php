<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <?php
                                echo form_label($form['nosv']['placeholder']);
                            ?> <small><i>( bukan no. cetak )</i></small>

                            <div class="input-group">
                                <?php
                                    echo form_input($form['nosv']);
                                    echo form_error('nosv','<div class="note">','</div>');
                                ?>

                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-tampil" id="btn-tampil">Tampil</button>
                                    <button type="button" class="btn btn-success btn-reset">Reset</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- detail transaksi via ajax -->
				<div class="table-responsive">

                    <!-- <button type="button" class="btn btn-danger" id="btn-del">Batal Baris yang dipilih</button> -->
                    <button type="button" class="btn btn-danger" id="btn-delheader">Batal Nota Service</button>

					<!--<table style="width: 2500px;"  class="table table-bordered table-hover js-basic-example dataTable">-->
					<table class="table table-bordered table-hover js-basic-example dataTable">
						<thead style="background-color: #effffc ">
							<tr>
                                <th style="width: 10px;text-align: center;"></th>
                                <th style="width: 10px;text-align: center;">No.</th>
								<th style="width: 150px;text-align: center;">No. Nota Servis</th>
                                <th style="width: 100px;text-align: center;">Tgl Nota Servis</th>
								<th style="text-align: center;">Nama Pemilik</th>
								<th style="width: 80px;text-align: center;">Jenis Bayar</th>
								<th style="text-align: center;">Nama Jasa Part</th>
								<th style="width: 10px;text-align: center;">Harga</th>
								<!-- <th style="width: 10px;text-align: center;">
									<i class="fa fa-th-large"></i>
                                </th> -->
							</tr>
						</thead>
						<tbody id="tbodyid"></tbody>
                        <!--
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
                        -->
					</table>
				</div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //$("#nokb").mask("a99-999999");

        //init
        $(".btn-reset").hide();
        $("#nosv").focus();

        $(".btn-tampil").click(function(){
            getData();
        });

        $(".btn-reset").click(function(){
            reset();
        });

        $(".btn-unposting").click(function(){
            unposting();
        });


        var input = document.getElementById("nosv");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("btn-tampil").click();
            }
        });

        $("#btn-delheader").click(function(){
            //alert( table.rows('.selected').data().length +' row(s) selected' );
            batheadkb();
        });


    });



    function buttonState(){
        $(".btn-tampil").toggle();
        $(".btn-reset").toggle();
    }

    function reset(){
        //$(".form-control").val("");
        buttonState();

        //$("#nokb").val('');
        $("#nosv").prop("readonly", false);
        $("#nosv").focus(); $("#nosv").select();

        //$('.dataTable').DataTable().fnClearTable();
        table.clear(); //table.draw();
        $("#tbodyid").empty();
    }

    function getData(){
        $("#nosv").prop("readonly", true);
        buttonState();

        var column = [];

        column.push({
            "aTargets": [ 0,1,2,3,4,5,6,7 ],
            "orderable": false
        });

        column.push({
            "aTargets": [ 7 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right",
            "orderable": false
        });

        column.push({
                "aTargets": [ 0 ],
                "searchable": false,
                "orderable": false,

//              "className": 'select-checkbox',

                "checkboxes": {
                    'selectRow': true
                }

        });


        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            //"orderable": false,
            "order": [],
            "fixedColumns": {
                leftColumns: 2
            },

            "select": {
                style: 'multi'
            },


            "lengthMenu": [[-1], ["Semua Data"]],
            //"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],

            //"bLengthChange": false,
            //"bPaging": false,
            "bPaginate": false,
            //"bInfo": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "sAjaxSource": "<?=site_url('btlns/getData');?>",
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "nosv", "value": $("#nosv").val() } );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },

            'rowCallback': function(row, data, index){
                    //if(data[23]){
                            //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                    //}
            },

            "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [],
            "sDom": "<'row'<'col-sm-6'l ><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p>> "
        });

        /*
        //tdk sesuai karena ajax jalan 2x
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        */

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
            table.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );

        /*
        table.on('xhr', function (settings, json) {
            //alert(JSON.parse(json));
            //alert( json.data.length +' row(s) were loaded' );
            if ( json.aaData === null ) {
                alert('x');
            }
        } );
        */


        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        /*
        $('.dataTable tfoot th').each( function () {
                var title = $('.dataTable thead th').eq( $(this).index() ).text();
                if(title!=="Edit" && title!=="Delete" && title.toUpperCase()!=="NO." ){
                        $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
                }else{
                        $(this).html( '' );
                }
        } );

        table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                        //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                                that
                                        .search( this.value )
                                        .draw();
                        //}
                } );
        });
        */




    } 


    function batheadkb(){
        swal({
            title: "Konfirmasi",
            text: "Proses Batal Transaksi Nota Servis akan dilakukan, data tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Batalkan!",
            cancelButtonText: "Kembali!",
            closeOnConfirm: true
            },

            function () {
                var nosv = $("#nosv").val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('btlns/prosesHead');?>",
                    data: {"nosv":nosv},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                $("#nosv").val('');
                                reset();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }


</script>

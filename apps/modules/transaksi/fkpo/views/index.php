<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
</style>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">   
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?>            -->

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-1">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                                </div>
                            </div>

                            <div class="col-xs-3">
                              <div class="box-header box-view">
                                <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()" > Ubah</button> 
                              </div>
                            </div>

                            <!-- <div class="col-xs-2">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-cetak"><i class="fa fa-print"></i> Cetak L/R</button>
                                 </div>
                            </div> -->

                            <div class="col-xs-3">
                                <div class="box-header form-group">
                                    <?php
                                        echo form_input($form['ket']);
                                        echo form_error('ket','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link " id="list-po" name="list-po" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Daftar Shipping List yang belum input faktur</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="input-po" name="input-po" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Input Faktur Pembelian</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-po">

                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">Detail</th>
                                                    <th style="text-align: center;">No. Faktur</th>
                                                    <th style="text-align: center;">Tgl. Faktur</th>
                                                    <th style="text-align: center;">Supplier</th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-po"> 

                                <div class="col-xs-12">
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nopo']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <?php
                                                echo form_input($form['nopo']);
                                                echo form_error('nopo','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php echo form_label($form['nmsup']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-4 ">
                                            <?php
                                                echo form_input($form['nmsup']);
                                                echo form_error('nmsup','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tglpo']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <?php
                                                echo form_input($form['tglpo']);
                                                echo form_error('tglpo','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php echo form_label($form['tipebayar']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-3 ">
                                            <?php
                                                echo form_input($form['tipebayar']);
                                                echo form_error('tipebayar','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tgltrm']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <?php
                                                echo form_input($form['tgltrm']);
                                                echo form_error('tgltrm','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php echo form_label($form['jth_tmp']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-1 ">
                                            <?php
                                                echo form_input($form['jth_tmp']);
                                                echo form_error('jth_tmp','<div class="note">','</div>');
                                            ?>
                                        </div> 

                                        <div class="col-xs-1 ">
                                            <?php echo form_label($form['tgltmp']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['tgltmp']);
                                                echo form_error('tgltmp','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 


                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label> Detail Shipping List </label>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTableindex display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">Qty</th>
                                                    <th style="text-align: center;">Kode</th>
                                                    <th style="text-align: center;">Tipe Unit</th> 
                                                    <th style="text-align: center;">Nama Unit</th> 
                                                    <th style="text-align: center;">Warna</th> 
                                                    <th style="text-align: center;">Harga</th> 
                                                    <th style="text-align: center;">Total Harga</th> 
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>  
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-1">
                                            <div class="form-group"> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-1 ">
                                            <div class="form-group"> 
                                                <?php echo form_label($form['sub_tot']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['sub_tot']);
                                                echo form_error('sub_tot','<div class="note">','</div>');
                                            ?>
                                        </div> 

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['potongan']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['potongan']);
                                                echo form_error('potongan','<div class="note">','</div>');
                                            ?>
                                        </div>  

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['dpp']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['dpp']);
                                                echo form_error('dpp','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-4">
                                            <div class="form-group"> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-2">
                                            <div class="form-group"> 
                                                <?php echo form_label($form['disc_top']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['disc_top']);
                                                echo form_error('disc_top','<div class="note">','</div>');
                                            ?>
                                        </div>  

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['ppn']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php
                                                echo form_input($form['ppn']);
                                                echo form_error('ppn','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-6">
                                            <div class="form-group"> 
                                                <?php
                                                    echo form_input($form['banner']);
                                                    echo form_error('banner','<div class="note">','</div>');
                                                ?>
                                            </div>
                                        </div>  

                                        <div class="col-xs-1">
                                            <div class="form-group"> 
                                                <?php echo form_label($form['total']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-5">
                                            <?php
                                                echo form_input($form['total']);
                                                echo form_error('total','<div class="note">','</div>');
                                            ?>
                                        </div>    
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div>
              </div>
          </div>
            <!-- /.box-body -->
        <?php echo form_close(); ?>
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        reset();
        autonum();
        $('#nopo').mask('**9-9999-999');
        $('.btn-ubah').hide(); 
        $('#ket').hide(); 
        $('#btn-refresh').show(); 
        
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        
        $("#nopo").change(function(){
            set_nopo(); 
        });
        $('.btn-refresh').click(function(){
            table.ajax.reload();
        });

        $('#input-po').click(function(){
            $('.btn-ubah').show(); 
            $('.btn-cetak').show();
            $('.btn-ubah').attr("disabled",true); 
            $('#ket').hide();
            $('.btn-refresh').hide();
        });

        $('#list-po').click(function(){
            $('.btn-ubah').hide();  
            $('#ket').hide();
            $('.btn-refresh').show();
        });

        var column = [];

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        $('.dataTableDetail').DataTable({});
        table = $('.dataTable').DataTable({
            "order": [[ 1, "asc" ]], 
            
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mData" : "nopo",
                    "mRender": function (data, type, row) {
                        var btn = '<a href="fkpo/edit/' + data +'">' + data + ' </a>';
                        return btn;
                    }
                } ],
            "columns": [
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "nopox"},
                { "data": "tglpox" },
                { "data": "nmsup"},
                { "data": "edit"}
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('fkpo/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                    .search( this.value )
                    .draw();
                //}
            });
        }); 

        // Add event listener for opening and closing details
        $('.dataTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );    

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover dataTableDetail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
                        '<th style="text-align: center;">Kode</th>'+
                        '<th style="text-align: center;">Tipe Unit</th>'+
                        '<th style="text-align: center;">Nama Unit</th>'+
                        '<th style="text-align: center;">Warna</th>'+
                        '<th style="text-align: center;width:1px;">Quantity</th>'+
                        '<th style="text-align: center;">Harga Per Unit</th>'+ 
                        '<th style="text-align: center;">Total Harga</th>'+ 
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
                        '<td style="text-align: ">'+data.kode+'</td>'+
                        '<td style="text-align: ">'+data.kdtipe+'</td>'+
                        '<td style="text-align: ">'+data.nmtipe+'</td>'+
                        '<td style="text-align: ">'+data.warna+'</td>'+
                        '<td style="text-align: right;">'+data.qty+'</td>'+
                        '<td style="text-align: right;">'+numeral(data.harga).format('0,0.00')+'</td>'+ 
                        '<td style="text-align: right;">'+numeral(data.harga * data.qty).format('0,0.00')+'</td>'+ 
                    '</tr>';
                    no++;
                }); 
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    });

    function autonum(){
        $('#sub_tot').autoNumeric('init');
        $('#potongan').autoNumeric('init');
        $('#disc_top').autoNumeric('init');
        $('#ppn').autoNumeric('init');
        $('#dpp').autoNumeric('init');
        $('#total').autoNumeric('init');
    }

    function set_nopo(){
      // alert(ket);
      var po = $("#nopo").val();
      var po = po.replace('-',''); 
      var nopo = po.replace('-','');
        $.ajax({
            type: "POST",
            url: "<?=site_url("fkpo/set_nopo");?>",
            data: {"nopo":nopo},
            success: function(resp){ 
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();  
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                    // baris 1 
                      $("#tglpo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpo)));
                      $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglterima)));
                      $("#tgltmp").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgljt)));
                      $("#nmsup").val(data.nmsup);
                      $("#nmsup").val(data.nmsup);
                      $("#tipebayar").val(data.jnsbayar);
                      $("#jth_tmp").val(data.njtempo);

                      $("#potongan").autoNumeric('set',data.disc);
                      $("#disc_top").autoNumeric('set',data.disctop); 
                      // $("#potongan").val(data.disc);
                      // $("#disc_top").val(data.disctop); 
                      getdetPO();
                      set_ket(nopo);
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function set_ket(nopo){
      // alert(ket); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("fkpo/set_ket");?>",
            data: {"nopo":nopo},
            success: function(resp){  
                var obj = JSON.parse(resp); 
                if(resp==='"empty"'){
                     $('.btn-ubah').attr('disabled',false);
                    $("#ket").hide();   
                    $("#ket").val(''); 
                }else{
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                    // $('#ket').show(); 
                        // baris 1  
                        if(data.fposting==='1' & data.ket_lr==='1'){
                            $('#ket').show(); 
                            $('.btn-ubah').attr('disabled',true); 
                            $("#ket").val('SUDAH PROSES POSTING'); 
                        } else if(data.fposting==='0' & data.ket_lr==='1'){
                            $('#ket').show(); 
                            $('.btn-ubah').attr('disabled',true); 
                            $("#ket").val('SUDAH PROSES L/R');   
                        }  else if(data.fposting==='1' & data.ket_lr===null){
                            $('#ket').show(); 
                            $('.btn-ubah').attr('disabled',true);
                            $("#ket").val('SUDAH PROSES POSTING');   
                        }  else {
                            // $("#ket").show();   
                            $('.btn-ubah').attr('disabled',false);
                            $("#ket").hide();   
                            $("#ket").val('');    
                        } 
                  });
                } 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function sum(){
        var sub_tot = $('#sub_tot').autoNumeric('get');
        var potongan = $('#potongan').autoNumeric('get');
        var disc_top = $('#disc_top').autoNumeric('get');
        if(sub_tot===''){
            $('#sub_tot').autoNumeric('set',0);
            var sub_tot = 0;
        }
        if(potongan===''){
            $('#potongan').autoNumeric('set',0);
            var potongan = 0;
        }
        if(disc_top===''){
            $('#disc_top').autoNumeric('set',0);
            var disc_top = 0;
        }

        var dpp = parseFloat(sub_tot) - parseFloat(disc_top) - parseFloat(potongan);
        if(!isNaN(dpp)){
            $('#dpp').autoNumeric('set',dpp);
        } else {
            $('#dpp').autoNumeric('set',0);
            var dpp = 0;
        }

        var ppn = parseFloat(dpp) / 10;
        if(!isNaN(ppn)){
            $('#ppn').autoNumeric('set',ppn);
        } else {
            $('#ppn').autoNumeric('set',0);
            var ppn = 0;
        }

        var total = parseFloat(dpp) + parseFloat(ppn);
        if(!isNaN(total)){
            $('#total').autoNumeric('set',total);
        } else {
            $('#total').autoNumeric('set',0);
            var total = 0;
        }

        var terbilang = penyebut(total);
        $('#banner').val(terbilang);
    }

    function ubah(){
        var nopo = $('#nopo').val();
        window.location.href = 'fkpo/edit/'+nopo;
    }
    
    function getdetPO(){
        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();
        //alert(kddiv);
        var column1 = [];   

        column1.push({
            "aTargets": [ 6,7 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        column1.push({
            "aTargets": [ 1 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
            },
            "sClass": "right"
        }); 

        table1 = $('.dataTableindex').DataTable({
            "aoColumnDefs": column1,
            "columns": [
                { "data": "no"},
                { "data": "qty" },
                { "data": "kode" },
                { "data": "kdtipe" },
                { "data": "nmtipe"},
                { "data": "warna" },
                { "data": "harga" },
                { "data": "totharga" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],  
            // "orderCellsTop": true,
            "fixedHeader": true, 
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            // "fixedColumns": {
            //     leftColumns: 2
            // },
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nopo", "value": nopo});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){

            },
            "sAjaxSource": "<?=site_url('fkpo/getdetPO');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                $('#sub_tot').autoNumeric('set',total); 
                sum();
                // sum();
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });  

        $('.dataTableindex tfoot th').each( function () {
            var title = $('.dataTableindex thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table1.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table1.on( 'draw.dt', function () {
        var PageInfo = $('.dataTableindex').DataTable().page.info();
                table1.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
    }

    function clear(){
        $('#nopo').val('');
        $('#tipebayar').val('');
        $('#nmsup').val('');
        $("#tglpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#jth_tmp").val('15');
        $('#tgltmp').val(dateback($("#tglpo").val()));

        //nominal
        $('#sub_tot').autoNumeric('set',0);
        $('#potongan').autoNumeric('set',0); 
        $('#disc_top').autoNumeric('set',0); 
        $('#dpp').autoNumeric('set',0);
        $('#ppn').autoNumeric('set',0);
        $('#total').autoNumeric('set',0);

        $('#banner').val(''); 
    }

    function reset(){
        $('#nopo').val('');
        $('#tipebayar').val('');
        $('#nmsup').val('');
        $("#tglpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#jth_tmp").val('15');
        $('#tgltmp').val(dateback($("#tglpo").val()));

        //nominal
        $('#sub_tot').val('');
        $('#potongan').val('');
        $('#disc_top').val('');
        $('#dpp').val('');
        $('#ppn').val('');
        $('#total').val('');

        $('#banner').val(''); 
    }


    function dateback(tgl) {
      var less = $("#jth_tmp").val();
      var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var res = day + '-' + month + '-' + year;
        return res;
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
      } 
</script>

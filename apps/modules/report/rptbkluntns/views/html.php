<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php

/* 
 * ***************************************************************
 * Script : html.php
 * Version : 
 * Date : Oct 31, 2017 11:46:13 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();
$decode = json_decode($decode); 
// echo "<script>console.log('" . $this->input->post('ket') . "');</script>"; 
// echo $decode->data->nmcabang;
if($array['submit']==="excel"){
    header("Content-type: application/vnd-ms-excel");
    if($array['ket']==='periode_between'){
       header("Content-Disposition: attachment; filename=LAPORAN STOCK DARI " .$array['periode_awal'] ." SAMPAI " .$array['periode_akhir'] ." ".date('Y-m-d H:i:s').".xls");
    } else {
       header("Content-Disposition: attachment; filename=LAPORAN STOCK PERIODE " .$array['periode'] ." ".date('Y-m-d H:i:s').".xls");
    }
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');

$this->table->add_row(array(
    array('data' => '<b>DAFTAR STOCK</b>'
                        , 'colspan' => '8'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$this->table->add_row(array(
    array('data' => "<b>". strtoupper($this->apps->title)." - ". strtoupper($decode->cabang)."</b>"
                        , 'colspan' => '8'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);   
if ($array['ket']==='periode_between') {
    $this->table->add_row(array(
        array('data' => "<b> DARI  " . $array['periode_awal'] ." TANGGAL " . $array['periode_akhir'] ."</b>"
                            , 'colspan' => '8'
                            , 'align' => 'center'
                            , 'style' => ' font-size: 12px;'),
            )
    );  
} else {
    $this->table->add_row(array(
        array('data' => "<b> PERIODE  " . $array['periode'] ."</b>"
                            , 'colspan' => '8'
                            , 'align' => 'center'
                            , 'style' => ' font-size: 12px;'),
            )
    );   
}
$namaheader = array(
    array('data' => 'Kode '
                        , 'align' => 'center' 
                        , 'style' => ' width: 200px; font-size: 12px;'),
    array('data' => 'Nama '
                        , 'align' => 'center' 
                        , 'style' => ' width: 200px; font-size: 12px;'),
    array('data' => 'Kategori'
                        , 'align' => 'center'
                        , 'style' => ' width: 100px; font-size: 12px;'),
    array('data' => 'Saldo AWAL'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'Masuk'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'BAG Masuk'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'Keluar'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'BAG Keluar'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'Saldo Akhir'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'), 
);
$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
// Caption text
//$this->table->set_caption($caption);
$this->table->add_row($namaheader);

$leas = array();
foreach ($decode->leasing as $leasing) {
    $leas[$leasing] = $leasing;
}
$res = array();
$g_saw          = 0;
$g_qtyin        = 0;
$g_qtybagin     = 0;
$g_qtyout       = 0;
$g_qtybagout    = 0;
$g_sak          = 0;
foreach ($leas as $v) {
    $no = 1;
    $s_qtyin = 0;
    $s_qtybagin = 0;
    $s_qtyout = 0;
    $s_qtybagout = 0;
    $s_sak = 0;
    $s_saw = 0;
    foreach ($decode->data as $data) {
        if($v==$data->kategori){          
            $akun_d = array(
                array('data' =>$data->kdpart
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' =>$data->nmpart
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' =>$data->kdgrup
                                    , 'style' => 'text-align: left; font-size: 12px;'), 
                array('data' => number_format($data->saw, 0,',','.').''
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->qtyin, 0,',','.').''
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->qtybagin, 0,',','.').''
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->qtyout, 0,',','.').''
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->qtybagout, 0,',','.').''
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->sak, 0,',','.').''
                                    , 'style' => 'text-align: right; font-size: 12px;'),
            );                     
            $this->table->add_row($akun_d);    
            $s_saw+=$data->saw;
            $s_qtyin+=$data->qtyin;
            $s_qtybagin+=$data->qtybagin;
            $s_qtyout+=$data->qtyout;
            $s_qtybagout+=$data->qtybagout;
            $s_sak+=$data->sak;
        }
    }
    $separator = array(
        array('data' => '<b>&nbsp; TOTAL ' . $v.'</b>'
                            , 'colspan' => '3'
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => '<b>'.number_format($s_saw, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_qtyin, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_qtybagin, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_qtyout, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_qtybagout, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_sak, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
    );
    $this->table->add_row($separator);    
    $g_saw          = $g_saw+$s_saw;
    $g_qtyin        = $g_qtyin+$s_qtyin;
    $g_qtybagin     = $g_qtybagin+$s_qtybagin;
    $g_qtyout       = $g_qtyout+$s_qtyout;
    $g_qtybagout    = $g_qtybagout+$s_qtybagout;
    $g_sak          = $g_sak+$s_sak;
}

$g_total = array(
    array('data' => '<b>GRAND TOTAL</b>'
                        , 'colspan' => '3'
                        , 'style' => 'text-align: left; font-size: 12px;'),
    array('data' => '<b>'.number_format($g_saw, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_qtyin, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_qtybagin, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_qtyout, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_qtybagout, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_sak, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
);
$this->table->add_row($g_total); 


// $desc_1 = array(
//     array('data' => '&nbsp;'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => 'DIBUAT OLEH'
//                         , 'colspan' => '2'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => '&nbsp;'
//                         , 'colspan' => '3'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => 'MENGETAHUI'
//                         , 'colspan' => '5'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
// );
// $this->table->add_row($desc_1);   
// for($i=1;$i<=5;$i++){
//   $this->table->add_row(array(
//         array('data' => '&nbsp;'
//                             , 'colspan' => '11'
//                             , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     ));   
// }
// $desc_2 = array(
//     array('data' => '&nbsp;'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => $this->perusahaan[0]['nmadh']
//                         , 'colspan' => '2'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => '&nbsp;'
//                         , 'colspan' => '3'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => $this->perusahaan[0]['nmkacab']
//                         , 'colspan' => '5'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
// );
// $this->table->add_row($desc_2); 

$this->table->set_template($template);
echo $this->table->generate();      
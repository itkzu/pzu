<!DOCTYPE html>
<html lang="en" dir="ltr">
<style>
table, th, td {
  border: 1px black;
  border-collapse: collapse;
}
</style>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <?php

    $array = $this->input->post();
     ?>
          <table border="0" style="width:750px">
            <thead>
              <tr>
                <td style="width:25%;font-size:14px" colspan="3"><small>PT. <?php echo strtoupper($this->apps->title) ?><br>
                                                    <?php echo strtoupper($this->session->userdata('data')['cabang'])?></small></td>
                <td style="width:50%;text-align:center;" colspan="5"> LAPORAN HARIAN KAS/BANK BENGKEL<br>Rekening : <?php echo $data[0]['nmkb'] ?><br> Tanggal :
                                                                      <?= $array['periode_awal'];?> sampai <?= $array['periode_akhir'];?></td>
                <td style="width:25%;" colspan="3"></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="width:25%;" colspan="3"></td>
                <td style="width:50%;" colspan="5"></td>
                <td style="width:25%;" colspan="3"></td>
              </tr>
            </tbody>
          </table>
          <br><br>
          <table border="1" style="width:750px">
            <thead>
              <tr>
                <th style="width:5%;text-align:center;" colspan="2">BUKTI</th>
                <th style="width:15%;text-align:center;" colspan="2">DARI/KE</th>
                <th style="width:15%;text-align:center;" colspan="2">NO. BG/CEK</th>
                <th style="width:15%;text-align:center;" colspan="2">KETERANGAN</th>
                <th style="width:15%;text-align:center;" colspan="1">DEBET</th>
                <th style="width:15%;text-align:center;" colspan="1">KREDIT</th>
                <th style="width:15%;text-align:center;" colspan="1">SALDO</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $value) {  ?>
                  <tr>
                  <td style="width:5%;text-align:center;" colspan="2"><?php echo $value["nocetak"];?></td>
                  <td style="width:15%;"  colspan="2"><?php echo $value["drkpd"];?></td>
                  <td style="width:15%;"  colspan="2"><?php echo $value["nofaktur"];?></td>
                  <td style="width:15%;"  colspan="2"><?php echo $value["ket"];?></td>
                  <td style="width:15%;text-align:right;"  colspan="1"><?php echo number_format($value["debet"],2,",",".");?></td>
                  <td style="width:15%;text-align:right;"  colspan="1"><?php echo number_format($value["kredit"],2,",",".");?></td>
                  <td style="width:15%;text-align:right;"  colspan="1"><?php echo number_format($value["so_awal"],2,",",".");?></td>
                </tr>
                <?php
              }

                $jml = $this->rptkbbklhr_qry->get_jml($array['kdkb'],$array['periode_awal'],$array['periode_akhir']);
                ?>
          </tbody>
            <tfoot>
              <?php foreach ($jml as $value): ?>
              <tr>
                <td style="width:55%;text-align:center;" colspan="8">TOTAL</td>
                <td style="width:15%;text-align:right;" colspan="1"><?php echo number_format($value['debet'],2,",","."); ?></td>
                <td style="width:15%;text-align:right;" colspan="1"><?php echo number_format($value["kredit"],2,",","."); ?></td>
                <td style="width:15%;text-align:right;" colspan="1"><?php echo number_format($value["saldo"],2,",","."); ?></td>
              </tr>

              <?php endforeach; ?>
            </tfoot>
          </table>
          <br><br>
          <table border="0" style="width:750px">
            <thead>
              <tr>
                <td colspan="1" ></td>
                <td colspan="3"></td>
                <td colspan="3"></td>
                <td colspan="3"></td>
                <td colspan="1"></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="1" style="width:15%;"></td>
                <td style="width:25%;text-align:center;" colspan="3">Semarang, <?php echo $array['periode_akhir']?></td>
                <td style="width:20%;" colspan="3"></td>
                <td style="width:25%;text-align:center;" colspan="3"></td>
                <td colspan="1" style="width:15%;"></td>
              </tr>
              <tr>
                <td colspan="1" style="width:15%;"></td>
                <td style="width:25%;text-align:center;" colspan="3">Diperiksa oleh,</td>
                <td style="width:20%;" colspan="3"></td>
                <td style="width:25%;text-align:center;" colspan="3">Dibuat oleh,</td>
                <td colspan="1" style="width:15%;"></td>
              </tr>
              <tr>
                <td colspan="1" style="width:15%;"></td>
                <td style="width:25%;text-align:center;" colspan="3"></td>
                <td style="width:20%;" colspan="3"></td>
                <td style="width:25%;text-align:center;" colspan="3"></td>
                <td colspan="1" style="width:15%;"></td>
              </tr>
              <tr>
                <td colspan="1">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="1">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="1">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="1">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="1">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="1">&nbsp;</td>
              </tr>
            </tbody>
            <tfoot>
                <tr>
                  <td colspan="1" style="width:15%;"></td>
                  <td style="width:25%;text-align:center;" colspan="3"><?php echo $this->cabbkl[0]['nmadh'];?></td>
                  <td style="width:20%;" colspan="3"></td>
                  <td style="width:25%;text-align:center;" colspan="3"><?php echo $this->cabbkl[0]['nmkasir'];?></td>
                  <td colspan="1" style="width:15%;"></td>
                </tr>
            </tfoot>
          </table>
  </body>
</html>

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
				<div class="box-header with-border">
					<h1 class="box-title"> <?php echo $nmgroups;?> </h1>

					<!--
					<div class="box-tools pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-wrench"></i></button>
								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="<?=site_url('groups/index/');?>" ><i class="fa fa-arrow-left"></i> Back to Group</a>
									</li>
									<li>
										<a href="javascript:void(0);" class="btn-refresh">Refresh</a>
									</li>
								</ul>
							</div>
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					-->
				</div>
				
				<!-- /.box-header -->
				<div class="box-body">

					<div>
						<a href="<?=site_url('groups');?>" class="btn btn-info" data-toggle="tooltip" data-placement="auto" title="Kembali ke daftar Grup Pengguna">
							<i class="fa fa-arrow-left"></i> Kembali
						</a>
						<button class="btn btn-default btn-refresh" type="button" data-toggle="tooltip" data-placement="auto" title="Refresh Data">
							<span class="fa fa-refresh"></span> Refresh
						</button> 

						<input type="hidden" id="group_id" name="group_id" value="<?php echo $kdgroups;?>">
					</div>
					<div class="box-body"><i id="ids"></i></div>

					<div class="table-responsive">
  					<table class="dataTable table table-bordered table-striped table-hover js-basic-example dataTable">
						
						<!-- <table class="table table-hover dataTable"> -->
							<thead>
								<tr>
									<th width="50">Akses</th>
									<th >Menu</th>
									<th>Induk Menu</th>
									<th>Deskripsi Menu</th>
									<th width="100">Link URL</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Akses</th>
									<th>Main Menu</th>
									<th>Sub Menu</th>
									<th>Sub Menu</th>
									<th>Sub Menu</th>
								</tr>
							</tfoot>
							<tbody></tbody>
						</table>
						<a href="<?=site_url('groups/');?>" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to Group</a>
						<button class="btn btn-default btn-refresh" type="button" >Refresh</button> 
					</div>
				</div>
				<!-- /.box-body -->
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {


		$(".btn-refresh").click(function(){
			table.ajax.reload();
		});


		//var url = "<?=site_url('groups/json_dgview_akses?$uid='.$kdgroups) ?>";
		var url = "<?=site_url('groups/json_dgview_akses?$uid='.$kdgroups) ?>";
		//var url = "<?=site_url('groups/json_dgview_akses?$uid='.$kdgroups) ?>";
		var kdgroups = "<?=$kdgroups;?>";

		//alert(url);
		table = $('.dataTable').DataTable({
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 1, 'asc' ]],
			"columnDefs": [
				{ "orderable": false, "targets": 0 }
				//{ "orderable": false, "targets": 5 }
				//{ "orderable": false, "targets": 4 }
			],
			//"pagingType": "simple",
			//"sAjaxSource": "<?=site_url('groups/json_dgview_akses');?>",

			"sAjaxSource": url,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
					aoData.push( { "name": "uid", "value": kdgroups });
					$.ajax( {
							"dataType": 'json', 
							"type": "GET", 
							//"url": url,
							"url": sSource, 
							"data": aoData, 
							"success": fnCallback
					} );
			},
			'rowCallback': function(row, data, index){
					//if(data[23]){
							//$(row).find('td:eq(23)').css('background-color', '#ff9933');
					//}
			},


			/*
			"ajax": {
         url:url,
         error:function(err, status){
             // what error is seen(it could be either server side or client side.
             console.log(err);
         }
      },
      */

			"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}}/*,
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					 }
				}*/
			]
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});


		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" && title!=="Akses"){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
								//if (ev.keyCode == 13) { //only on enter keypress (code 13)
									that
									.search( this.value )
									.draw();
								//}
							} );
		});
	});

	function set_submenu(menu_id) {  
		$("#ids").html('');
		var group_id = $("#group_id").val();
		var submit = "<?=site_url('groups/submit_akses');?>";
		$.ajax({
			type: "POST",
			url: submit,
			data: {"group_id":group_id,"menu_id":menu_id,"privilege":"1,1,1"},
			success: function(resp){
				var json = $.parseJSON(resp);
				//alert(json.msg);

				if(json.state=='1'){
					swal("Data berhasil diupdate", "", "success");
				} else{
					swal("Action Error!", json.msg , "error");
				}

				//$("#ids").html(json.msg);

				table.ajax.reload();
			}
		});

	}

  /*
	function set_access(menu_id) {
		var group_id = $("#group_id").val();
		var submit = "<?=site_url('groupaccess/submit');?>";
		var T = "";
		var E = "";
		var H = "";
		var privilege = "0,0,0";
		if($("#T"+menu_id).prop("checked")) {
			T = "1";
		} else {
			T = "0";
		}
		if($("#E"+menu_id).is(":checked")) {
			E = "1";
		} else {
			E = "0";
		}
		if($("#H"+menu_id).is(":checked")) {
			H = "1";
		} else {
			H = "0";
		}
		privilege = T+","+E+","+H;
		$.ajax({
			type: "POST",
			url: submit,
			data:{"group_id":group_id,"menu_id":menu_id,"privilege":privilege,"stat":"access"},
			success: function(resp){
				table.ajax.reload();
			}
		});
	}
	*/
</script>
<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo form_label($form['kode']['placeholder']);?>
                        </div>
                      </div>

                      <div class="col-md-3"> 
                          <?php
                              echo form_input($form['kode']);
                              echo form_error('kode','<div class="note">','</div>');
                          ?> 
                      </div>   
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['beli']['placeholder']);?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['beli']);
                              echo form_error('beli','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo " <small><i>tanpa PPN</i></small>";?>
                        </div>
                      </div>  

                      <div class="col-md-1 faktif">
                        <div class="form-group">
                          <?php echo "<u>Status</u>"; ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <div class="checkbox">
                              <?php
                                  echo form_checkbox($form['faktif']);
                              ?>
                          </div>
                      </div> 

                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo form_label($form['kdtipe']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-3"> 
                          <?php
                              echo form_input($form['kdtipe']);
                              echo form_error('kdtipe','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['jual']['placeholder']);?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['jual']);
                              echo form_error('jual','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo " <small><i>On The Road</i></small>";?>
                        </div>
                      </div>  
                  </div>
              </div>  

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo form_label($form['nmtipe']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-3"> 
                          <?php
                              echo form_input($form['nmtipe']);
                              echo form_error('nmtipe','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['jual_off']['placeholder']);?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['jual_off']);
                              echo form_error('jual_off','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo " <small><i>Off The Road</i></small>";?>
                        </div>
                      </div>  
                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo form_label($form['kdtipesr']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-3"> 
                          <?php
                              echo form_dropdown( $form['kdtipesr']['name'],
                                                  $form['kdtipesr']['data'] ,
                                                  $form['kdtipesr']['value'] ,
                                                  $form['kdtipesr']['attr']);
                              echo form_error('kdtipesr','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['bbn']['placeholder']);?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['bbn']);
                              echo form_error('bbn','<div class="note">','</div>');
                          ?> 
                      </div>  
 
                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo " <small><i>Harga Jual On - Off TR</i></small>";?>
                        </div>
                      </div>  
                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo form_label($form['nmtipegrp']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-3"> 
                          <?php
                              echo form_input($form['nmtipegrp']);
                              echo form_error('nmtipegrp','<div class="note">','</div>');
                          ?> 
                      </div>   

                      <div class="col-md-3"> 
                          <?php
                              echo form_input($form['kode2']);
                              echo form_error('kode2','<div class="note">','</div>');
                          ?> 
                      </div>  
                  </div>
              </div>

              <div class="col-md-12 col-lg-12">
                  <hr>
              </div>    
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 
      
      autonum();
      sum();
      gettipegrp();
      
      $('.btn-submit').attr('disabled',false);

      if(!$('#kode2').val()){
          $('.faktif').hide();
          $('#faktif').hide(); 
          $('.btn-submit').click(function(){ 
            submit();  
          });
      } else if($('#kode2').val()){
          $('.faktif').show();
          $('#faktif').show(); 
          $('#faktif').bootstrapToggle({
              on: 'AKTIF',
              off: 'TIDAK AKTIF',
              onstyle: 'success',
              offstyle: 'danger'
          }); 
          $('.btn-submit').click(function(){ 
            update();  
          });
      }  

      $('#kdtipesr').change(function(){
        gettipegrp();
      }); 
    }); 

    function autonum(){
      $('#beli').autoNumeric('init');
      $('#jual').autoNumeric('init');
      $('#jual_off').autoNumeric('init');
      $('#bbn').autoNumeric('init');
    }

    function sum(){
      var beli = $('#beli').autoNumeric('get');
      var jual = $('#jual').autoNumeric('get');
      var jual_off = $('#jual_off').autoNumeric('get');

      if(beli===''){
        $('#beli').autoNumeric('set',0);
      }
      if(jual===''){
        $('#jual').autoNumeric('set',0);
        var jual = 0;
      }
      if(jual_off===''){
        $('#jual_off').autoNumeric('set',0);
        var jual_off = 0;
      }

      var sel = parseFloat(jual) - parseFloat(jual_off);
      if(!isNaN(sel)){
        if(sel>0){
          $('#bbn').autoNumeric('set',sel);
        } else {
          $('#bbn').autoNumeric('set',0);
        }
      }
    }

    function gettipegrp(){ 
        var kdtipesr = $("#kdtipesr").val(); 
        //alert(nmaks);
        $.ajax({
          type: "POST",
          url: "<?=site_url("mstaddunit/gettipegrp");?>",
          data: {"kdtipesr":kdtipesr}, 
          success: function(resp){
            var obj = jQuery.parseJSON(resp);
            $.each(obj, function(key, data){
               $('#nmtipegrp').val(data.nmtipegrp);
            });
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function submit(){
        var kode = $("#kode").val().toUpperCase();
        var kdtipe = $("#kdtipe").val().toUpperCase();
        var nmtipe = $("#nmtipe").val().toUpperCase();
        var kdtipesr = $("#kdtipesr").val();
        var beli = $("#beli").autoNumeric('get');
        var jual = $("#jual").autoNumeric('get');
        var jual_off = $("#beli").autoNumeric('get');
        //alert(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstaddunit/submit");?>",
      		data: {"kode":kode,"kdtipe":kdtipe,"nmtipe":nmtipe,"kdtipesr":kdtipesr,"beli":beli,"jual":jual,"jual_off":jual_off},
      		beforeSend: function() {
            $('.btn-submit').attr('disabled',true);
      		},
      		success: function(resp){
            var obj = jQuery.parseJSON(resp);
            if(obj.state==='1'){
              swal({
                title: 'Data Berhasil Disimpan',
                text: obj.msg,
                type: 'success'
              }, function(){
                window.location.href = '<?=site_url('mstaddunit');?>';
              }); 
            }else{ 
              swal({
                title: 'Data Gagal Disimpan',
                text: obj.msg,
                type: 'error'
              }); 
            }
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function update(){
        var kode = $("#kode2").val().toUpperCase();
        var kode2 = $("#kode").val().toUpperCase();
        var kdtipe = $("#kdtipe").val().toUpperCase();
        var nmtipe = $("#nmtipe").val().toUpperCase();
        var kdtipesr = $("#kdtipesr").val().toUpperCase(); 

        var beli = $('#beli').autoNumeric('get');
        var jual = $('#jual').autoNumeric('get');
        var jual_off = $('#jual_off').autoNumeric('get');

        if ($("#faktif").prop("checked")){
          var faktif = 't';
        } else {
          var faktif = 'f';
        }

      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstaddunit/update");?>",
      		data: {"kode":kode,"kode2":kode2,"kdtipe":kdtipe,"nmtipe":nmtipe,"kdtipesr":kdtipesr,"beli":beli,"jual":jual,"jual_off":jual_off,"faktif":faktif},
      		beforeSend: function() { 
            $('.btn-submit').attr('disabled',true);
      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      if (data.tipe==="success"){
                        window.location.href = '<?=site_url('mstaddunit');?>';
                      }else{
                        refresh();
                      }
                  });
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function refresh(){
      window.location.reload();
    }
</script>

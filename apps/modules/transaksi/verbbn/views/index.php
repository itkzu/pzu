<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style>  
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-xs-12 box-header box-view">
              <div class="col-xs-5">
                <div class="row"> 
                </div>
              </div> 
              <div class="col-xs-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak</button>
                </div>
              </div>
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['notbj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['notbj']);
                                          echo form_error('notbj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgltbj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tgltbj']);
                                          echo form_error('tgltbj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row"> 
                      <div class="col-xs-12">
                          <label></label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-xs-12">
                    <p><a id="add" class="btn btn-primary btn-ubah"><i class="fa fa-star"></i> Ubah</a> </p>
                    <p id="nodox" class="kata"></p>
                    <p id="no" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. DO</th> 
                            <th style="text-align: center;">Tgl. DO</th>
                            <th style="text-align: center;">Nama STNK</th> 
                            <th style="text-align: center;">Tipe Unit</th> 
                            <th style="text-align: center;">Kota</th> 
                            <th style="text-align: center;">Tarif Pajak</th> 
                            <th style="text-align: center;">By. Proses</th>  
                            <th style="text-align: center;">By. Jasa</th>  
                          </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                      </table>
                    </div>
                  </div>
                </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>



<!-- modal dialog -->
<div id="modal_trans" class="modal fade">
    <div class="modal-dialog"  style="width: 50%;">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Faktur yang akan diproses BBN</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['nodo']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-3"> 
                            <?php
                              echo form_input($form['nodo']);
                              echo form_error('nodo','<div class="note">','</div>');
                            ?>
                        </div> 
                            
                        <div class="col-xs-2"> 
                          <div class="form-group">
                          </div>
                        </div>
                            
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['tgldo']['placeholder']); ?>
                          </div>
                        </div>

                        <div class="col-xs-3"> 
                            <?php
                              echo form_input($form['tgldo']);
                              echo form_error('tgldo','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">   
                        <br>
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['nama']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-10"> 
                            <?php
                              echo form_input($form['nama']);
                              echo form_error('nama','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['kdtipe']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-10"> 
                            <?php
                              echo form_input($form['kdtipe']);
                              echo form_error('kdtipe','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['kota']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-10"> 
                            <?php
                              echo form_input($form['kota']);
                              echo form_error('kota','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <br> 
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['pajak_bbn']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-10"> 
                            <?php
                              echo form_input($form['pajak_bbn']);
                              echo form_error('pajak_bbn','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['tarif_bbn']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-10"> 
                            <?php
                              echo form_input($form['tarif_bbn']);
                              echo form_error('tarif_bbn','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <div class="col-xs-12"> 
                      <div class="row">  
                        <div class="col-xs-2"> 
                          <div class="form-group">
                              <?php echo form_label($form['jasa']['placeholder']); ?>
                          </div>
                        </div>
                        
                        <div class="col-xs-10"> 
                            <?php
                              echo form_input($form['jasa']);
                              echo form_error('jasa','<div class="note">','</div>');
                            ?>
                        </div>  
                      </div>
                    </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>
            <?php echo form_close(); ?>
  
<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        disabled(); 
        $('#notbj').mask('TBJ-9999-9999');

        $('#notbj').keyup(function () {
            var jml = $('#notbj').val(); 
            var n = jml.length;
            
            if(n===13){
                // console.log(n);
              set_notbj();
            } 
        }); 

        $('.btn-ubah').click(function(){
            modal();
        })

        $('.btn-simpan').click(function(){
            ubah();
        })

        $('.btn-ctk').click(function(){
            var notbj = $('#notbj').val();
                        // var lik = nokps.replace("/","+");
                        // var lik = lik.replace("/","+");
                        // var lik = lik.replace("/","+");
            window.open('verbbn/ctk/'+notbj, '_blank');
            // window.location.href = 'lrnew/ctk_lr/'+nodo;

            // ctk_lr();
        });

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 6,7,8 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });  

        table = $('.dataTable').DataTable({ 
            "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nodo" },
                { "data": "tgldo" },
                { "data": "nama" },
                { "data": "kdtipe"},
                { "data": "kota" },
                { "data": "pajak_bbn"},
                { "data": "tarif_bbn"},
                { "data": "jasa"},
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,   
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "notbj", "value": $("#notbj").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('verbbn/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });   

        $('.dataTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });  
    });

    function disabled(){  
      $("#tgltbj").attr('disabled',true); 
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',true);
      $(".btn-del").attr('disabled',true);
      $(".btn-ctk").attr('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){  
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',false);
      $(".btn-del").attr('disabled',false);
      $(".btn-ctk").attr('disabled',false); 
    }

    function clear(){ 
      $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
    }

    function autonum(){ 
      $("#pajak_bbn").autoNumeric('init'); 
      $("#tarif_bbn").autoNumeric('init'); 
      $("#jasa").autoNumeric('init'); 
    }

    function reset(){
      $("#notbj").val('');
      $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
    }

    function c_modal(){
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $("#nama").val('');
      $("#kdtipe").val('');
      $("#kota").val('');
      $("#pajak_bbn").val('');
      $("#tarif_bbn").val('');
      $("#jasa").val('');
    } 

    function set_notbj(){

      var notbj = $("#notbj").val();
      var notbj = notbj.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("verbbn/set_notbj");?>",
            data: {"notbj":notbj },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  clear();
                  $(".btn-ctk").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                    $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglbbn_trm))); 
                    $(".btn-ctk").attr('disabled',false);
                    table.ajax.reload();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function modal(){   
      c_modal();
      autonum();
      var nodo    = table.row('.selected').data()['nodo'];
      var tgldo   = table.row('.selected').data()['tgldo'];
      var nama   = table.row('.selected').data()['nama'];
      var kdtipe   = table.row('.selected').data()['kdtipe'];
      var kota   = table.row('.selected').data()['kota'];
      var pajak_bbn   = table.row('.selected').data()['pajak_bbn'];
      var tarif_bbn   = table.row('.selected').data()['tarif_bbn'];
      var jasa   = table.row('.selected').data()['jasa']; 

      $('#modal_trans').modal('toggle'); 
      $('#nodo').val(nodo);
      $('#tgldo').val(tgldo);
      $('#nama').val(nama);
      $('#kdtipe').val(kdtipe);
      $('#kota').val(kota);
      $('#pajak_bbn').autoNumeric('set',pajak_bbn);
      $('#tarif_bbn').autoNumeric('set',tarif_bbn);
      $('#jasa').autoNumeric('set',jasa); 
  }

    function ubah(){
      var nodo        = $('#nodo').val();  
      var tarif_bbn   = $('#tarif_bbn').autoNumeric('get');
      var jasa        = $('#jasa').autoNumeric('get'); 

      $.ajax({
          type: "POST",
          url: "<?=site_url('verbbn/update');?>",
          data: { "nodo":nodo, "tarif_bbn":tarif_bbn, "jasa":jasa },
          success: function(resp){
            $("#modal_trans").modal("hide");
            var obj = jQuery.parseJSON(resp);
            $.each(obj, function(key, data){ 
                swal({
                  title: data.title,
                  text: data.msg,
                  type: data.tipe
                }, function(){
                  table.ajax.reload();
                });
              });
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  }
</script>

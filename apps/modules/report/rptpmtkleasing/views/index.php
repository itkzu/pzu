<?php
/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?= base_url('assets/plugins/dtables/resource/details_open.png'); ?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?= base_url('assets/plugins/dtables/resource/details_close.png'); ?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">
                <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'class' => "form-inline");
                echo form_open($submit, $attributes);
                ?> 
                <div class="form-group">
                    <?php
                    echo form_label($form['periode_awal']['placeholder']);
                    echo form_input($form['periode_awal']);
                    echo form_error('periode_awal', '<div class="note">', '</div>');
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label($form['periode_akhir']['placeholder']);
                    echo form_input($form['periode_akhir']);
                    echo form_error('periode_akhir', '<div class="note">', '</div>');
                    ?>
                </div>
                <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                    <table style="width: 1500px;" class="table table-bordered dataTable">
                        <thead>
                            <tr>
                                <th style="width: 10px;text-align: center;">DETAIL</th>
                                <th style="width: 100px;text-align: center;">NO. KAS/BANK</th>
                                <th style="text-align: center;">TGL. KAS/BANK</th>
                                <th style="text-align: center;">JENIS</th>
                                <th style="text-align: center;">LEASING</th>
                                <th style="text-align: center;">KETERANGAN</th>
                                <th style="text-align: center;">NILAI KAS/BANK</th>
                                <th style="width: 100px;text-align: center;">TOTAL REFUND</th>
                                <th style="width: 100px;text-align: center;">SELISIH</th>
                                <th style="text-align: center;">KET. SELISIH</th>
                                <th style="width: 100px;text-align: center;">JENIS REFUND</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 10px;text-align: center;">DETAIL</th>
                                <th style="width: 100px;text-align: center;">NO. KAS/BANK</th>
                                <th style="text-align: center;">TGL. KAS/BANK</th>
                                <th style="text-align: center;">JENIS</th>
                                <th style="text-align: center;">LEASING</th>
                                <th style="text-align: center;">KETERANGAN</th>
                                <th style="text-align: center;">NILAI KAS/BANK</th>
                                <th style="width: 100px;text-align: center;">TOTAL REFUND</th>
                                <th style="width: 100px;text-align: center;">SELISIH</th>
                                <th style="text-align: center;">KET. SELISIH</th>
                                <th style="width: 100px;text-align: center;">JENIS REFUND</th>
                            </tr>                        
                        </tfoot>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function () {
            table.ajax.reload();
        });
        var column = [];
        for (i = 6; i <= 8; i++) {
            column.push({
                "aType": "numeric-comma",
                "aTargets": [i],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    // return formmatedvalue;
                }
            });
        }
        $('.dataTableDetail').DataTable();
        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "nokb"},
                {"data": "tglkb"},
                {"data": "nmkb"},
                {"data": "kdleasing"},
                {"data": "ket"},
                {"data": "nilai"},
                {"data": "total"},
                {"data": "selisih"},
                {"data": "ket_selisih"},
                {"data": "jenis"}
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({"name": "periode_awal", "value": $("#periode_awal").val()}
                , {"name": "periode_akhir", "value": $("#periode_akhir").val()});
                $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                });
            },
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?= site_url('rptpmtkleasing/json_dgview'); ?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
//                {extend: 'copy'},
//                {extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": {filter: 'applied', order: 'current'},
                    "sFileName": "report.xls",
                    action: function (e, dt, button, config) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                },
//                {extend: 'pdf', 
//                    orientation: 'landscape',
//                    pageSize: 'A3'
//                },
//                {extend: 'print',
//                    customize: function (win){
//                           $(win.document.body).addClass('white-bg');
//                           $(win.document.body).css('font-size', '10px');
//                           $(win.document.body).find('table')
//                                   .addClass('compact')
//                                   .css('font-size', 'inherit');
//                   }
//                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // Add event listener for opening and closing details
        $('.dataTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                //format(row.data());
                tr.addClass('shown');
            }
        });

        $('.dataTable tfoot th').each(function () {
            var title = $('.dataTable thead th').eq($(this).index()).text();
            if (title !== "Edit" && title !== "Delete" && title !== "DETAIL") {
                $(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
            } else {
                $(this).html('');
            }
        });

        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                        .search(this.value)
                        .draw();
                //}
            });
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2 = $Tabla_Nueva.filter(function () {
                return (this.childElementCount != 1);
            });

            var $rows = Tabla_Nueva2,
                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    tmpColDelim = String.fromCharCode(11), // vertical tab character
                    tmpRowDelim = String.fromCharCode(0), // null character

                    // Solo Dios Sabe por que puse esta linea
                    colDelim = (filename.indexOf("xls") != -1) ? '"\t"' : '","',
                    rowDelim = '"\r\n"',
                    // Grab text from table into CSV formatted string
                    csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row);
                        var $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                        return $cols.map(function (j, col) {
                            var $col = $(col);
                            var text = $col.text().replace(/\./g, '');
                            return text.replace('"', '""'); // escape double quotes

                        }).get().join(tmpColDelim);
                        csv = csv + '"\r\n"' + 'fin ' + '"\r\n"';
                    }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


            download_csv(csv, filename);


        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format(d) {
            //console.log(d);
            var tdetail = '<table class="table table-bordered table-hover dataTableDetail">' +
                    '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">' +
                    '<th style="text-align: center;width:16px;"></th>' +
                    '<th style="text-align: center;width:10px;">NO.</th>' +
                    '<th style="text-align: center;">NO. DO</th>' +
                    '<th style="text-align: center;">TGL. DO</th>' +
                    '<th style="text-align: center;">NO. SPK</th>' +
                    '<th style="text-align: center;">TGL. SPK</th>' +
                    '<th style="text-align: center;">ATAS NAMA PO</th>' +
                    '<th style="text-align: center;">ATAS STNK/BPKB</th>' +
                    '<th style="text-align: center;">ALAMAT</th>' +
                    '<th style="text-align: center;">AR REFUND</th>' +
                    '</tr>' +
                    '</thead><tbody>';
            var no = 1;
            var total = 0;
            if (d.detail.length > 0) {
                $.each(d.detail, function (key, data) {
                    //console.log(data);
                    tdetail += '<tr>' +
                            '<td style="text-align: center;"></td>' +
                            '<td style="text-align: center;">' + no + '</td>' +
                            '<td style="text-align: center;">' + data.nodo + '</td>' +
                            '<td style="text-align: center;">' + data.tgldo + '</td>' +
                            '<td style="text-align: center;">' + data.noso + '</td>' +
                            '<td style="text-align: center;">' + data.tglso + '</td>' +
                            '<td>' + data.nama_p + '</td>' +
                            '<td>' + data.nama_s + '</td>' +
                            '<td>' + data.alamatktp_s + '</td>' +
                            '<td style="text-align: right;">' + numeral(data.nilai_d).format('0,0.00') + '</td>' +
                            '</tr>';
                    no++;
                    total = Number(total) + Number(data.nilai_d);
                });

            }
//            tdetail += '<tfoot>' +
//                    '<tr>'+
//                        '<th style="text-align: right;" colspan="8">TOTAL</th>'+
//                        '<th style="text-align: right;">'+numeral(total).format('0,0.00')+'</th>'+
//                    '</tr>'+
//                '</tfoot>';
            tdetail += '</tbody></table>';
            return tdetail;
        }
    });
</script>
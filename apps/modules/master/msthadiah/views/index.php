<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */

?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-header">
				<a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
				<a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>

				<!--
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-wrench"></i></button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo $add;?>" >Tambah Data Baru</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="btn-refersh">Refresh</a>
							</li>
						</ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
				-->

			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 10px;text-align: center;">No.</th>
								<th style="width: 100px;text-align: center;">Nama Program</th>
								<th style="width: 60px;text-align: center;">Tgl Mulai</th>
								<th style="width: 60px;text-align: center;">Tgl Berakhir</th>
								<th style="text-align: center;">Deskripsi Hadiah</th>
								<th style="width: 50px;text-align: center;">
									<i class="fa fa-th-large"></i>
								</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>No.</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th>Edit</th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".btn-refersh").click(function(){
			table.ajax.reload();
		});
		
		var column = [];

		column.push({ 
				"aTargets": [ 0,5 ],
				"searchable": false,
				"orderable": false  
		});

		column.push({ 
			"aTargets": [ 2,3 ],
			"mRender": function (data, type, full) {
					return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});

		/*
		column.push({ 
			"aTargets": [ 4 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "right"
		});
		*/

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 2, 'asc' ]],
			// "pagingType": "simple",
			"sAjaxSource": "<?=site_url('msthadiah/json_dgview');?>",
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
			}
		});
		
		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Hapus" && title!=="No."){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
	});
	
	function refresh(){
		table.ajax.reload();
	}
	
	function deleted(id){
		swal({
			title: "Konfirmasi Hapus !",
			text: "Data yang dihapus tidak dapat dikembalikan!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#c9302c",
			confirmButtonText: "Ya, Lanjutkan!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false
		}, function () {
			var submit = "<?php echo $submit;?>"; 
				$.ajax({
					type: "POST",
					url: submit,
					data: {"id":id,"stat":"delete"},
					success: function(resp){   
						var obj = jQuery.parseJSON(resp);
						if(obj.state==="1"){
							swal({
								title: "Terhapus",
								text: obj.msg,
								type: "success"
							}, function(){
								table.ajax.reload();
							});
						}else{
							swal("Gagal!", obj.msg, "error");
						}
					},
					error:function(event, textStatus, errorThrown) {
						swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
					}
				});
		});
	}
</script>
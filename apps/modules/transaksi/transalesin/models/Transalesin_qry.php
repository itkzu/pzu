<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Transalesin_qry
 *
 * @author adi
 */
class Transalesin_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }
    
    public function select_data($param) { 
        $this->db->select("kdsales, nmsales, kddiv, kdsales_header, status, nik"
                            . ", faktif, kdtk, nmlengkap"
                            . ", to_char(tgl_in,'DD-MM-YYYY') as tgl_in, tgl_out");
        $this->db->where('kdsales',$param);
        $query = $this->db->get('sales');
        return $query->result_array();
    }
    
    public function getKddiv() {
        $kddiv = $this->session->userdata('data')['kddiv'];
        $this->db->select("nmdiv,alamat,kddiv");
        $this->db->where("kddiv like '{$kddiv}%'");
        $this->db->where("status","POS");
        $this->db->like("nmdiv","");
        $this->db->order_by("nmdiv","ASC");
        $query = $this->db->get('mst.divisi');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }
    
    public function getSalesheader() {
        $status = $this->input->post('status');
        if($status==="KP"){
            $this->db->where("sales.status","KC");
        }elseif($status==="SV"){
            $this->db->where("sales.status","KP");
        }elseif($status==="CO"){
            $this->db->where("sales.status","SV");
        }elseif($status==="SL" || $status==="CS" ){
            $this->db->where("(sales.status = 'SV' or sales.status = 'CO')");
        }
        $this->db->select("sales.kdsales, sales.nmlengkap, sales.status, divisi.nmdiv");
        $this->db->where("sales.faktif","true");
        $this->db->order_by("sales.nmlengkap","ASC");
        $this->db->join('mst.divisi','divisi.kddiv = sales.kddiv');
        $query = $this->db->get('pzu.sales');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function getSalesHeaderDivisi() {
        $kdsales_header = $this->input->post('kdsales_header');
        if(!$kdsales_header){
            return false;
        }
        $this->db->where("sales.kdsales",$kdsales_header);
        $this->db->where("sales.faktif","true");
        $this->db->order_by("sales.nmlengkap","ASC");
        $query = $this->db->get('pzu.sales');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }
    
    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdsales']) ){
            $kdsales = $_GET['kdsales'];
        }else{
            $kdsales = '';
        }        
        
        $aColumns = array('nik',
                            'nmlengkap',
                            'nmsales',
                            'status_ket',
                            'nmsales_header',
                            'nmdiv',
                            'tgl_in', 
                            'tgl_out',
                            'faktif',
                            'kdsales',);
	$sIndexColumn = "kdsales";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT kdsales
                        , nmsales
                        , nmlengkap
                        , kddiv
                        , nmdiv
                        , status
                        , status_ket
                        , kdsales_header
                        , nmsales_header
                        , CASE WHEN faktif='t' THEN 'YA' ELSE 'TIDAK' END as faktif
                        , tgl_in
                        , tgl_out
                        , nik
                       FROM pzu.v_sales
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
        //echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[9] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('transalesin/edit/'.$aRow['kdsales'])."\">Edit</a>";
                $row[10] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdsales']."');\">Hapus</button>";
		$output['aaData'][] = $row;
	}
	echo  json_encode( $output );  
    }
    
    public function submit() {
        try {
            $array = $this->input->post();  
            if(empty($array['kdsales'])){
                unset($array['kdsales']);
                $array['nik'] = strtoupper($array['nik']);
                $array['tgl_in'] = $this->apps->dateConvert($array['tgl_in']);
                $array['nmsales'] = strtoupper($array['nmsales']);
                $array['nmlengkap'] = strtoupper($array['nmlengkap']);

                $resl = $this->db->insert('sales',$array);
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['kdsales']) && empty($array['stat'])){
                $array['nik'] = strtoupper($array['nik']);
                $array['tgl_in'] = $this->apps->dateConvert($array['tgl_in']);
                $array['nmsales'] = strtoupper($array['nmsales']);
                $array['nmlengkap'] = strtoupper($array['nmlengkap']);

                $this->db->where('kdsales', $array['kdsales']);
                $resl = $this->db->update('sales', $array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['kdsales']) && !empty($array['stat'])){                
                $this->db->where('kdsales', $array['kdsales']);
                //$this->db->set("faktif","false");
                //$this->db->set("tgl_out",date('Y-m-d'));
                $resl = $this->db->delete('sales');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }             
            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

    public function insertNik($data,$kddiv)
    { 
        if(count($data)>0){
            $this->db->empty_table('pzu.tmp_niksales');
            $this->db->insert_batch('pzu.tmp_niksales', $data);
        }
        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from pzu.updnik()");

            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
            return 'Error! '.$this->db->_error_message();
        }

        return json_encode($res);
    }

}

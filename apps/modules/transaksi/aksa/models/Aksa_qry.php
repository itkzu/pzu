<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Aksa_qry
 *
 * @author adi
 */
class Aksa_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataSupplier() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.v_supplier");
        return $q->result_array();
    }

    public function getKodeAksa() {
        $this->db->select("*");
        $this->db->order_by('nmaks');
        $query = $this->db->get('pzu.v_aks');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->where('noaksa',$no);
        $q = $this->db->get("pzu.vm_aksa");
        $res = $q->result_array();
        return $res;
    }

    private function getdetail(){
        $output = array();
        $str = "SELECT  noaksa,
                        kdaks,
                        nmaks,
                        qty,
                        nilai
                         FROM pzu.v_aksa_d";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
	    }
        return $output;
    }

    private function getdetail2(){
        $output2 = array();
        $str2 = "SELECT  noaksa,sum(nilai) as tot_all
                        FROM pzu.v_aksa_d group by noaksa";
        $q2 = $this->db->query($str2);
        $res2 = $q2->result_array();

        foreach ( $res2 as $aRow2 ){
            foreach ($aRow2 as $key2 => $value2) {
                if(is_numeric($value2)){
                    $aRow2[$key2] = (float) $value2;
                }else{
                    $aRow2[$key2] = $value2;
                }
            }
           $output2[] = $aRow2;
	    }
        return $output2;
    }

    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('noaksa', 'tglaksa', 'ket');
    	$sIndexColumn = "noaksa";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT noaksa
                              , tglaksa
                              , kddiv
                              , ket
                              FROM pzu.vm_aksa) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noaksa ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        $detail2 = $this->getdetail2();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['noaksa']==$value['noaksa']){
                    $aRow['detail'][]= $value;
                }
            }

            $aRow['detail2'] = array();

            foreach ($detail2 as $value) {
                if($aRow['noaksa']==$value['noaksa']){
                    $aRow['detail2'][]= $value;
                }
            }

            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('aksa/edit/'.$aRow['noaksa'])."\">Edit</a>";
            $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['noaksa']."');\">Hapus</button>";

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['noaksa']) ){
            if($_GET['noaksa']){
                $noaksa = $_GET['noaksa'];
            }else{
                $noaksa = '';
            }
        }else{
            $noaksa = '';
        }

        $aColumns = array('nmaks',
                          'qty',
                          'nilai',
                          'kdaks',
                          'nourut',
                          'noaksa');
	    $sIndexColumn = "noaksa";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select nmaks, qty, nilai, kdaks, nourut, noaksa from pzu.v_aksa_d where noaksa = '".$noaksa."' ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }/*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nourut']==$value['nourut']){
                    $aRow['detail'][]= $value;
                }
            }*/
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['kdaks']."','".$aRow['nmaks']."'
            ,'".$aRow['qty']."','".$aRow['nilai']."','".$aRow['nourut']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['noaksa']."','".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }

    public function addDetail() {
        $noaksa = $this->input->post('noaksa');
        $tglaksa = $this->apps->dateConvert($this->input->post('tglaksa'));
        $ket = $this->input->post('ket');
        $nourut = $this->input->post('nourut');
        if($nourut==''){
          $nourut=0;
        }
        $kdaks = $this->input->post('kdaks');
        $qty = $this->input->post('qty');
        $harga = $this->input->post('harga');

        $q = $this->db->query("select noaksa,title,msg,tipe from pzu.aksa_d_ins('".$noaksa."',
                                                                            '".$tglaksa."',
                                                                            '".$this->session->userdata('data')['kddiv']."',
                                                                            '".$ket."',
                                                                            '".$this->session->userdata("username")."' ,
                                                                            ".$nourut.",
                                                                            ".$kdaks.",
                                                                            ".$qty.",
                                                                            '".$harga."')");

      //  echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function detaildeleted() {
        $noaksa = $this->input->post('noaksa');
        $nourut = $this->input->post('nourut');
        $q = $this->db->query("select title,msg,tipe from pzu.aksa_d_del('".$noaksa."',". $nourut . ")");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function delete() {
        $noaksa = $this->input->post('noaksa');
        $q = $this->db->query("select title,msg,tipe from pzu.aksa_del('". $noaksa ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function submit() {
        $noaksa = $this->input->post('noaksa');
        $tglaksa = $this->apps->dateConvert($this->input->post('tglaksa'));
        $ket = $this->input->post('ket');
        $q = $this->db->query("select title,msg,tipe from pzu.aksa_ins( '". $noaksa ."',
                                                                        '". $tglaksa ."',
                                                                        '".$this->session->userdata('data')['kddiv']."',
                                                                        '". $ket ."',
                                                                        '".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Blokbpkb_qry
 *
 * @author adi
 */
class Blokbpkb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function set_do() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vl_do where nodo =  '".$nodo."' and tk = 'T'");
      // echo $this->db->last_query();
      // $res = $q->result_array();
      // return json_encode($res);
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    } 

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, coalesce(SUBSTRING(max(b.nodo),5,6)::int+1,1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function carispk() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select noso from pzu.t_so where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }    

    public function select_data() {
        $nodo = $this->uri->segment(3); 
        $this->db->select('*');
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vl_do");
        $res = $q->result_array();
        return $res;
    }       

    public function submit() { 
      $nodo             = $this->input->post('nodo'); 
      $fblokbpkb        = $this->input->post('fblokbpkb');
      $blokbpkb_nama    = $this->input->post('blokbpkb_nama');
      $blokbpkb_alamat  = $this->input->post('blokbpkb_alamat');
      $blokbpkb_noktp   = $this->input->post('blokbpkb_noktp');
      $blokbpkb_nohp    = $this->input->post('blokbpkb_nohp');
      $blokbpkb_ket     = $this->input->post('blokbpkb_ket'); 

        $q = $this->db->query("select title,msg,tipe from pzu.blokir_bpkb_w('" . $nodo . "',
                                                                          '" . $fblokbpkb . "', 
                                                                          '" .  $blokbpkb_nama . "',
                                                                          '" . $blokbpkb_alamat . "',
                                                                          '" . $blokbpkb_noktp . "',
                                                                          '" . $blokbpkb_nohp . "', 
                                                                          '" . $blokbpkb_ket . "')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Qrins_qry
 *
 * @author adi
 */
class Qrins_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function select_data($kdaks) {
        $this->db->select("*");
        $this->db->where('kdaks',$kdaks);
        $query = $this->db->get('pzu.v_aks');
        return $query->result_array();
    }

    public function getKategori() {
        $this->db->select("*");
        $this->db->order_by('nmaksgr');
        $query = $this->db->get('pzu.v_aks_gr');
        return $query->result_array();
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdaks']) ){
            $id = $_GET['kdaks'];
        }else{
            $id = '';
        }

        $aColumns = array('no',
                            'nmaks',
                            'nmaksgr',
                            'faktifx',
                            'kdaks');
	$sIndexColumn = "kdaks";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT '' as no, kdaks, nmaks, nmaksgr, faktifx
                        FROM pzu.v_aks ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[4] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('qrins/edit/'.$aRow['kdaks'])."\">Edit</a>";
                $row[5] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdaks']."');\">Hapus</button>";
		$output['aaData'][] = $row;
	}
	echo  json_encode( $output );
    }

    public function submit() {
      $nmaks = $this->input->post('nmaks');
      $kdaksgr = $this->input->post('kdaksgr');
      $q = $this->db->query("select title,msg,tipe from pzu.aks_ins('" . $nmaks . "'," . $kdaksgr . ",'".$this->session->userdata("username")."')");

      //echo $this->db->last_query();
      if($q->num_rows()>0){
          $res = $q->result_array();
      }else{
          $res = "";
      }

      return json_encode($res);
    }

    public function update() {
      $kdaks = $this->input->post('kdaks');
      $kdaksgr = $this->input->post('kdaksgr');
      $nmaks = $this->input->post('nmaks');
      $faktif= $this->input->post('faktif');
      $q = $this->db->query("select title,msg,tipe from pzu.aks_upd(" . $kdaks . ",'" . $nmaks . "'," . $kdaksgr . ",'" . $faktif . "','".$this->session->userdata("username")."')");

      //echo $this->db->last_query();
      if($q->num_rows()>0){
          $res = $q->result_array();
      }else{
          $res = "";
      }

      return json_encode($res);
    }

    public function delete() {
        $kdaks = $this->input->post('kdaks');
        $q = $this->db->query("select title,msg,tipe from pzu.aks_del('". $kdaks ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

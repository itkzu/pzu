<?php
/*
 * ***************************************************************
 * Script : adh.php
 * Version :
 * Date : Nov 21, 2017 10:11:14 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
	.dt-buttons{
		margin-bottom: 10px;
	}
</style>
<!-- Main row -->
<div class="row">
	<div class="col-lg-6">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-calendar"></i> Transaksi Harian
			</div>
			<div class="box-body">
				<!--
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<?php
							echo form_label($form['periode_awal']['placeholder']);
							echo form_input($form['periode_awal']);
							echo form_error('periode_awal', '<div class="note">', '</div>');
							?>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<?php
							echo form_label($form['periode_akhir']['placeholder']);
							echo form_input($form['periode_akhir']);
							echo form_error('periode_akhir', '<div class="note">', '</div>');
							?>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group" style="padding-top: 25px;">
							<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
						</div>
					</div>
				</div>
			-->
				<div id="" style="min-height: 300px;  overflow-y: auto;overflow-x: hidden;" align="center">
					<table class="table table-hover table-bordered trn_harian" style="margin-top: 0px !important;">
						<thead>
							<tr>
								<th  style="width:10px;">No.</th>
								<th  style="width:10px;">Periode</th>
								<th>Keterangan</th>
								<th style="width: 35px; text-align:right;">Qty</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th  style="width:10px;">No.</th>
								<th  style="width:10px;">Periode</th>
								<th>Item</th>
								<th style="width: 25px;">Qty</th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-balance-scale"></i> Posisi Kas / Bank
			</div>
			<div class="box-body">
				<div id="" style="min-height: 300px; overflow-y: auto;overflow-x: hidden;" align="center">
					<table class="table table-hover table-bordered trn_kas_bank" style="margin-top: 0px !important;">
						<thead>
							<tr>
								<th  style="width:10px;">No.</th>
								<th>Item Rekening</th>
								<th style="width: 100px;">Periode Aktif</th>
							</tr>
						</thead>
						<!--
						<tfoot>
							<tr>
								<th  style="width:10px;">No.</th>
								<th>Item Rekening</th>
								<th style="width: 100px;">Periode Aktif</th>
							</tr>
						</tfoot>
						-->
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-tags"></i> Daftar DO Belum Terima PO Leasing
			</div>
			<div class="box-body">
				<div id="" style="overflow-x: hidden;" align="center">
					<div class="table-responsive">
						<table class="table table-hover table-bordered trn_blm_trm_po_leasing" style="margin-top: 0px !important;">
							<thead>
								<tr>
									<th style="width:10px;text-align: center;">No.</th>
									<th style="text-align: center;">Leasing</th>
									<th style="width: 90px;text-align: center;" class="sum">Jumlah DO</th>
									<th style="width: 90px;text-align: center;">Detail</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr>
									<th style="text-align: right;" colspan="2">TOTAL</th>
									<th style="width: 90px;text-align: right;">0</th>
									<th style="width: 90px;text-align: right;">&nbsp;</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-line-chart"></i> Progres BBN
			</div>
			<div class="box-body">
				<div id="" style="overflow-x: hidden;" align="center">
					<table class="table table-hover table-bordered trn_prog_bbn" style="margin-top: 0px !important;">
						<thead>
							<tr>
								<th style="width:10px;">No.</th>
								<th>Item</th>
								<th style="width: 25px; text-align:right;">Qty</th>
							</tr>
						</thead>
						<!--
						<tfoot>
							<tr>
								<th  style="width:10px;">No.</th>
								<th>Item</th>
								<th style="width: 25px;">Qty</th>
							</tr>
						</tfoot>
						-->
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row (main row) -->


<!-- Modal Chart -->
<div id="modal-detail-po-leasing" class="modal fade" role="dialog" style="overflow-y: scroll;">
	<div class="modal-dialog" style="width: 99%;">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Daftar DO Belum Terima PO Leasing</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-hover table-bordered trn_blm_trm_po_leasing_detail" style="margin-top: 0px !important;">
						<thead>
							<tr>
								<th style="width: 5px;text-align: center;">No.</th>
								<th style="width: 20px;text-align: center;">Leasing</th>
								<th style="text-align: left;">Program</th>
								<th style="width: 100px;text-align: right;" class="sum">Pelunasan</th>
								<th style="width: 100px;text-align: left;">No. DO</th>
								<th style="width: 100px;text-align: left;">Tgl DO</th>
								<th style="text-align: left;">Nama STNK</th>
								<th style="text-align: left;">Tipe Unit</th>
								<th style="width: 140px;text-align: left;">Warna</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th style="text-align: right;" colspan="3">TOTAL</th>
								<th style="text-align: right;">0</th>
								<th style="text-align: right;" colspan="5">&nbsp;</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="close">Tutup</button>
			</div>
		</div>

	</div>
</div>


<!-- Modal Chart -->
<div id="modal-progres-bbn" class="modal fade" role="dialog" style="overflow-y: scroll;">
    <div class="modal-dialog" style="width: 99%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title">Title</h4>
      </div>
      <div class="modal-body">
          <div class="table-responsive">
              <div class="table_prog_bbn"></div>
									<table class="table table-hover table-bordered table_prog_bbn_detail" style="margin-top: 0px !important;"></table>

              <div class="table_prog_bbn_leasing"></div>
									<table class="table table-hover table-bordered table_prog_bbn_detail_leas" style="margin-top: 0px !important;"></table>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn-close">Tutup</button>
      </div>
    </div>

  </div>
</div>

<!-- ChartJS -->
<script src="<?= base_url('assets/plugins/chartjs/Chart.bundle.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/chartjs/utils.js'); ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>

<script>
	$(function () {
		$(".btn-tampil").click(function () {
			trn_harian.ajax.reload();
			trn_kas_bank.ajax.reload();
			trn_prog_bbn.ajax.reload();
		});


		trn_harian = $('.trn_harian').DataTable({
			"aoColumnDefs": [
				{
					"aTargets": [3],
					"mRender": function (data, type, full) {
						return type === 'export' ? data : numeral(data).format('0,0');
						// return type === 'export' ? data : numeral(data).format('0,0.00');
						// return formmatedvalue;
					},
					"sClass": "right"
				},

				{
					"aTargets": [ 0 ],
					"searchable": false,
					"orderable": false
				}

				],
			"fixedColumns": {
				leftColumns: 2
			},
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"aaSorting": [],
			"fnServerData": function (sSource, aoData, fnCallback) {
				/*
				aoData.push(
						{"name": "key", "value": "pzu"}
				, {"name": "periode_awal", "value": $("#periode_awal").val()}
				, {"name": "periode_akhir", "value": $("#periode_akhir").val()}
				);
				*/
				$.ajax({
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			'rowCallback': function (row, data, index) {
				//if(data[23]){
				//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?= site_url('dashadh/getTrnHarian'); ?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win) {
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
		});

		$('.trn_harian').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.trn_harian tfoot th').each(function () {
			var title = $('.trn_harian thead th').eq($(this).index()).text();
			if (title !== "Edit" && title !== "Delete" && title !== "No." && title !== "Qty") {
				$(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
			} else {
				$(this).html('');
			}
		});

		trn_harian.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
				that
						.search(this.value)
						.draw();
				//}
			});
		});


		trn_kas_bank = $('.trn_kas_bank').DataTable({
			"aoColumnDefs": [
				{
					"aTargets": [2],
					"mRender": function (data, type, full) {
						return type === 'export' ? data : data; //numeral(data).format('0,0');
						// return type === 'export' ? data : numeral(data).format('0,0.00');
						// return formmatedvalue;
					}
				}],
			"fixedColumns": {
				leftColumns: 2
			},
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"aaSorting": [],
			"fnServerData": function (sSource, aoData, fnCallback) {
				aoData.push({"name": "key", "value": "pzu"}
				);
				$.ajax({
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			'rowCallback': function (row, data, index) {
				//if(data[23]){
				//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?= site_url('dashadh/getTrnKasBank'); ?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win) {
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-12 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
		});

		$('.trn_kas_bank').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});


		$('.trn_kas_bank tfoot th').each(function () {
			var title = $('.trn_kas_bank thead th').eq($(this).index()).text();
			if (title !== "Edit" && title !== "Delete" && title !== "Periode Aktif" && title !== "No.") {
				$(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
			} else {
				$(this).html('');
			}
		});


		trn_kas_bank.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
				that
						.search(this.value)
						.draw();
				//}
			});
		});


		trn_prog_bbn = $('.trn_prog_bbn').DataTable({
			"aoColumnDefs": [
				{
					"aTargets": [2],
					"mRender": function (data, type, full) {
						return type === 'export' ? data : numeral(data).format('0,0');
						//return type === 'export' ? data : data; //numeral(data).format('0,0');
						// return type === 'export' ? data : numeral(data).format('0,0.00');
						// return formmatedvalue;
					},
					"sClass": "right"
				},
			{
				"aTargets": [1],
			//	"mData" : "ket",
				"mRender": function (data, type, row) {
					var btn = '<a onclick="detail_prog_bbn(\'' + row[0] + '\',\'' + row[1] + '\')"' +
										' href="javascript:void(0);">' + data + ' </a>';
					return btn;
				}
			}],
			"fixedColumns": {
				leftColumns: 2
			},
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"aaSorting": [],
			"fnServerData": function (sSource, aoData, fnCallback) {
				aoData.push({"name": "key", "value": "pzu"}
				);
				$.ajax({
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			'rowCallback': function (row, data, index) {
				//if(data[23]){
				//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?= site_url('dashadh/getTrnProgBBN'); ?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win) {
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-12 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
		});

		$('.trn_prog_bbn').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.trn_prog_bbn tfoot th').each(function () {
			var title = $('.trn_prog_bbn thead th').eq($(this).index()).text();
			if (title !== "Edit" && title !== "Delete" && title !== "SALDO") {
				$(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
			} else {
				$(this).html('');
			}
		});

		trn_prog_bbn.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
				that
						.search(this.value)
						.draw();
				//}
			});
		});



		trn_blm_trm_po_leasing = $('.trn_blm_trm_po_leasing').DataTable({
			"ordering": false,
			"columns": [
				{"data": null},
				{"data": "kdleasing"},
				{"data": "count", "sClass":"right"},
				{"data": null,
					render: function (data, type, row) {
						var btn = '<center>' +
								'<a class="btn btn-xs btn-info"' +
								' onclick="detail_po(\'' + row.cabang + '\', \'' + row.kdleasing + '\')"' +
								' href="javascript:void(0);">' +
								'Detail' +
								'</a>' +
								'</center>';
						return btn;
					}
				}
			],
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": false,
			"bServerSide": false,
			"bDestroy": true,
			"bAutoWidth": false,
			"aaSorting": [],
			'rowCallback': function (row, data, index) {
				//if(data[23]){
				//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"footerCallback": function (row, data, start, end, display) {
				var api = this.api();

				api.columns('.sum', {
					page: 'current'
				}).every(function () {
					var sum = this
							.data()
							.reduce(function (a, b) {
								var x = parseFloat(a) || 0;
								var y = parseFloat(b) || 0;
								return x + y;
							}, 0);
					$(this.footer()).html(numeral(sum).format('0,0'));
				});
			},
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
				},
				{extend: 'print',
					customize: function (win) {
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'> > "
		});
		trn_blm_trm_po_leasing.on('order.dt search.dt', function () {
			trn_blm_trm_po_leasing.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();

		trn_blm_trm_po_leasing_detail = $('.trn_blm_trm_po_leasing_detail').DataTable({
			"ordering": false,
			"columns": [
				{"data": null},
				{"data": "kdleasing"},
				{"data": "nmprogleas"},
				{"data": "pl",
					render: function (data, type, row) {
						return type === 'export' ? data : numeral(data).format('0,0');
					}
				},
				{"data": "nodo"},
				{"data": "tgldo",
					render: function (data, type, row) {
						return type === 'export' ? data : moment(data).format('L');
					}
				},
				{"data": "nama_s"},
				{"data": "nmtipe"},
				{"data": "nmwarna"}
			],
			"columnDefs": [
				{className: "text-right", "targets": [3]}
			],
			"lengthMenu": [[5, 10, 15, 20, 25, 50, -1], [5, 10, 15, 20, 25, 50, "Semua Data"]],
			"bProcessing": false,
			"bServerSide": false,
			"bDestroy": true,
			"bAutoWidth": false,
			"aaSorting": [],
			'rowCallback': function (row, data, index) {
				//if(data[23]){
				//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"footerCallback": function (row, data, start, end, display) {
				var api = this.api();

				api.columns('.sum', {
					page: 'current'
				}).every(function () {
					var sum = this
							.data()
							.reduce(function (a, b) {
								var x = parseFloat(a) || 0;
								var y = parseFloat(b) || 0;
								return x + y;
							}, 0);
					$(this.footer()).html(numeral(sum).format('0,0'));
				});
			},
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
				},
				{extend: 'print',
					customize: function (win) {
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
		});
		trn_blm_trm_po_leasing_detail.on('order.dt search.dt', function () {
			trn_blm_trm_po_leasing_detail.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();

		get_blm_trm_po_leasing();

		setInterval(function () {
			trn_harian.ajax.reload();
			trn_kas_bank.ajax.reload();
			trn_prog_bbn.ajax.reload();
		}, 60000);


	});

	function get_blm_trm_po_leasing() {
		var periode = $("#periode_awal").val();
		$.ajax({
			type: "POST",
			url: "<?= site_url('dashadh/get_blm_trm_po_leasing'); ?>",
			data: {"periode": periode},
			beforeSend: function () {

			},
			success: function (resp) {
				var obj = jQuery.parseJSON(resp);
				trn_blm_trm_po_leasing.clear().draw();
				trn_blm_trm_po_leasing.rows.add(obj).draw();
			},
			error: function (event, textStatus, errorThrown) {
				console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		});
	}

	function detail_po(cabang, kdleasing) {
		$.ajax({
			type: "POST",
			url: "<?= site_url('dashadh/get_blm_trm_po_leasing_detail'); ?>",
			data: {"cabang": cabang
				,"kdleasing": kdleasing},
			beforeSend: function () {
				trn_blm_trm_po_leasing_detail.clear().draw();
				$("#modal-detail-po-leasing").modal("show");
			},
			success: function (resp) {
				var obj = jQuery.parseJSON(resp);
				trn_blm_trm_po_leasing_detail.rows.add(obj).draw();
			},
			error: function (event, textStatus, errorThrown) {
				console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		});
	}

	function detail_prog_bbn(nourut,ket) {
//		alert(nourut);
//		alert(ket);
		$.ajax({
			type: "POST",
			url: "<?= site_url('dashadh/detail_prog_bbn'); ?>",
			data: {"no": nourut
						,"ket": ket},
			beforeSend: function () {
				clear();
				$("#modal-progres-bbn").modal("show");
				$(".title").html(ket)
			},
			success: function (resp) {
				if(nourut==13){
					$(".table_prog_bbn").html( '<h2 style="text-align:center"> LEASING </h2>');
					$(".table_prog_bbn_detail").html( '<thead>' +
																	'<tr>' +
																			'<th style="text-align: center;width: 7.5%;">Nama</th>' +
																			'<th style="text-align: center;width: 7.5%;">Alamat</th>' +
																			'<th style="text-align: center;width: 7.5%;">Kelurahan</th>' +
																			'<th style="text-align: center;width: 7.5%;">Kecamatan</th>' +
																			'<th style="text-align: center;width: 7.5%;">Kota</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Hp</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tipe Unit</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Mesin</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Rangka</th>' +
																			'<th style="text-align: center;width: 7.5%;">Leasing</th>' +
																			'<th style="text-align: center;width: 7.5%;">Sales</th>' +
																			'<th style="text-align: center;width: 7.5%;">No DO</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl DO</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl AJU FA</th>' +
																			'<th style="text-align: center;width: 7.5%;">Nama Supplier</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl Terbit FA</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl Terima FA</th>' +
																			'<th style="text-align: center;width: 7.5%;">AJU BBN BJ</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima Tagihan</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Polisi</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima Notice</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah Notice</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima STNK</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah STNK</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima TNKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah TNKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima BPKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah BPKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">No. BPKB</th>' +
																	'</tr>' +
																	'</thead>' );
														$(".table_prog_bbn_detail").append( '<tbody></tbody>');
					$(".table_prog_bbn_leasing").html( '<h2 style="text-align:center"> KONSUMEN </h2>');
					$(".table_prog_bbn_detail_leas").html( '<thead>' +
																										'<tr>' +
																												'<th style="text-align: center;width: 7.5%;">Nama</th>' +
																												'<th style="text-align: center;width: 7.5%;">Alamat</th>' +
																												'<th style="text-align: center;width: 7.5%;">Kelurahan</th>' +
																												'<th style="text-align: center;width: 7.5%;">Kecamatan</th>' +
																												'<th style="text-align: center;width: 7.5%;">Kota</th>' +
																												'<th style="text-align: center;width: 7.5%;">No Hp</th>' +
																												'<th style="text-align: center;width: 7.5%;">Tipe Unit</th>' +
																												'<th style="text-align: center;width: 7.5%;">No Mesin</th>' +
																												'<th style="text-align: center;width: 7.5%;">No Rangka</th>' +
																												'<th style="text-align: center;width: 7.5%;">Leasing</th>' +
																												'<th style="text-align: center;width: 7.5%;">Sales</th>' +
																												'<th style="text-align: center;width: 7.5%;">No DO</th>' +
																												'<th style="text-align: center;width: 7.5%;">Tgl DO</th>' +
																												'<th style="text-align: center;width: 7.5%;">Tgl AJU FA</th>' +
																												'<th style="text-align: center;width: 7.5%;">Nama Supplier</th>' +
																												'<th style="text-align: center;width: 7.5%;">Tgl Terbit FA</th>' +
																												'<th style="text-align: center;width: 7.5%;">Tgl Terima FA</th>' +
																												'<th style="text-align: center;width: 7.5%;">AJU BBN BJ</th>' +
																												'<th style="text-align: center;width: 7.5%;">Terima Tagihan</th>' +
																												'<th style="text-align: center;width: 7.5%;">No Polisi</th>' +
																												'<th style="text-align: center;width: 7.5%;">Terima Notice</th>' +
																												'<th style="text-align: center;width: 7.5%;">Serah Notice</th>' +
																												'<th style="text-align: center;width: 7.5%;">Terima STNK</th>' +
																												'<th style="text-align: center;width: 7.5%;">Serah STNK</th>' +
																												'<th style="text-align: center;width: 7.5%;">Terima TNKB</th>' +
																												'<th style="text-align: center;width: 7.5%;">Serah TNKB</th>' +
																												'<th style="text-align: center;width: 7.5%;">Terima BPKB</th>' +
																												'<th style="text-align: center;width: 7.5%;">Serah BPKB</th>' +
																												'<th style="text-align: center;width: 7.5%;">No. BPKB</th>' +
																										'</tr>' +
																										'</thead>');
																							$(".table_prog_bbn_detail_leas").append('<tbody></tbody>');

														tabelprogBBNdet(nourut);
														tabelprogBBNdet_leas();
				} else {
					$(".table_prog_bbn_detail").html( '<thead>' +
																	'<tr>' +
																			'<th style="text-align: center;width: 7.5%;">Nama</th>' +
																			'<th style="text-align: center;width: 7.5%;">Alamat</th>' +
																			'<th style="text-align: center;width: 7.5%;">Kelurahan</th>' +
																			'<th style="text-align: center;width: 7.5%;">Kecamatan</th>' +
																			'<th style="text-align: center;width: 7.5%;">Kota</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Hp</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tipe Unit</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Mesin</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Rangka</th>' +
																			'<th style="text-align: center;width: 7.5%;">Leasing</th>' +
																			'<th style="text-align: center;width: 7.5%;">Sales</th>' +
																			'<th style="text-align: center;width: 7.5%;">No DO</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl DO</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl AJU FA</th>' +
																			'<th style="text-align: center;width: 7.5%;">Nama Supplier</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl Terbit FA</th>' +
																			'<th style="text-align: center;width: 7.5%;">Tgl Terima FA</th>' +
																			'<th style="text-align: center;width: 7.5%;">AJU BBN BJ</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima Tagihan</th>' +
																			'<th style="text-align: center;width: 7.5%;">No Polisi</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima Notice</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah Notice</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima STNK</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah STNK</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima TNKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah TNKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">Terima BPKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">Serah BPKB</th>' +
																			'<th style="text-align: center;width: 7.5%;">No. BPKB</th>' +
																	'</tr>' +
																	'</thead>');
														$(".table_prog_bbn_detail").append('<tbody></tbody>');


														tabelprogBBNdet(nourut);
				}
											},
			error: function (event, textStatus, errorThrown) {
				console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
		});
	}

	function tabelprogBBNdet(nourut){

			var column = [];
			if(nourut==1){
        column.push({
	            "aTargets": [ 13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
			} else if(nourut==2){
        column.push({
	            "aTargets": [ 15,16,17,18,19,20,21,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==3){
        column.push({
	            "aTargets": [ 14,17,18,19,20,21,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==4){
        column.push({
	            "aTargets": [ 14,17,18,19,20,21,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==5){
        column.push({
	            "aTargets": [ 14,18,19,20,21,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==6){
        column.push({
	            "aTargets": [ 14,21,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==7){
        column.push({
	            "aTargets": [ 14,21,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==8){
        column.push({
	            "aTargets": [ 14,21,23,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==9){
        column.push({
	            "aTargets": [ 14,21,23,25,27 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==10){
        column.push({
	            "aTargets": [ 14,22,23,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==11){
        column.push({
	            "aTargets": [ 14,24,25,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==12){
        column.push({
	            "aTargets": [ 14,26,27,28 ],
							"visible": false,
            	"searchable": false
        });
 			} else if(nourut==13){
        column.push({
	            "aTargets": [ 14 ],
							"visible": false,
            	"searchable": false
        });
 			}
			column.push({
					"aTargets": [ 12,13,15,16,17,18,20,21,22,23,24,25,26 ],
					"mRender": function (data, type, full) {
						return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
				},
				"sClass": "center"
				});

			table_prog_bbn_detail = $('.table_prog_bbn_detail').DataTable({
			"aoColumnDefs": column,
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "Semua Data"]],
		 "bPaginate": true,
			"bProcessing": true,
			"bServerSide": true,
		 	"bDestroy": true,
			"order": [[ 11, 'asc' ]],
			"bAutoWidth": false,
			"ordering": true,
			"fnServerData": function (sSource, aoData, fnCallback) {
				aoData.push({"name": "nourut", "value": nourut}
				);
					$.ajax({
							"dataType": 'json',
							"type": "GET",
							"url": sSource,
							"data": aoData,
							"success": fnCallback
					});
			},
			'rowCallback': function (row, data, index) {
			},
			"sAjaxSource": "<?= site_url('dashadh/getProgBBNDetail'); ?>",
			"oLanguage": {
					"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
					{extend: 'copy',
							exportOptions: {orthogonal: 'export'}},
					{extend: 'csv',
							exportOptions: {orthogonal: 'export'}},
					{extend: 'excel',
							exportOptions: {orthogonal: 'export'}},
					{extend: 'pdf',
							orientation: 'landscape',
							pageSize: 'A3'
					},
					{extend: 'print',
							customize: function (win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size', '10px');
									$(win.document.body).find('table')
																			.addClass('compact')
																			.css('font-size', 'inherit');
							}
					}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
			});

			$('.table_prog_bbn_detail').tooltip({
					selector: "[data-toggle=tooltip]",
					container: "body"
			}); 

			$('.table_prog_bbn_detail tfoot th').each(function () {
					var title = $('.table_prog_bbn_detail thead th').eq($(this).index()).text();
					if (title !== "Edit" && title !== "Delete" && title !== "No." && title !== "Qty") {
					} else {
						$(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
							$(this).html('');
					}
			});

			table_prog_bbn_detail.columns().every(function () {
					var that = this;
					$('input', this.footer()).on('keyup change', function (ev) {
					//if (ev.keyCode == 13) { //only on enter keypress (code 13)
							that
							.search(this.value)
							.draw();
					//}
					});
			});
	}

	function tabelprogBBNdet_leas(){
			var column = [];
        column.push({
	            "aTargets": [ 14 ],
							"visible": false,
            	"searchable": false
        });


			table_prog_bbn_detail_leas = $('.table_prog_bbn_detail_leas').DataTable({
			"aoColumnDefs": column,
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "Semua Data"]],
		 "bPaginate": true,
		 "bDestroy": true,
			"bServerSide": true,
			"order": [[ 11, 'asc' ]],
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": true,
			"aaSorting": [],
			"fnServerData": function (sSource, aoData, fnCallback) {
					$.ajax({
							"dataType": 'json',
							"type": "GET",
							"url": sSource,
							"data": aoData,
							"success": fnCallback
					});
			},
			'rowCallback': function (row, data, index) {
			},
			"sAjaxSource": "<?= site_url('dashadh/getProgBBNDetail_leas'); ?>",
			"oLanguage": {
					"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
					{extend: 'copy',
							exportOptions: {orthogonal: 'export'}},
					{extend: 'csv',
							exportOptions: {orthogonal: 'export'}},
					{extend: 'excel',
							exportOptions: {orthogonal: 'export'}},
					{extend: 'pdf',
							orientation: 'landscape',
							pageSize: 'A3'
					},
					{extend: 'print',
							customize: function (win) {
									$(win.document.body).addClass('white-bg');
									$(win.document.body).css('font-size', '10px');
									$(win.document.body).find('table')
																			.addClass('compact')
																			.css('font-size', 'inherit');
							}
					}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
			});

			$('.table_prog_bbn_detail_leas').tooltip({
					selector: "[data-toggle=tooltip]",
					container: "body"
			});

			$('.table_prog_bbn_detail_leas tfoot th').each(function () {
					var title = $('.table_prog_bbn_detail_leas thead th').eq($(this).index()).text();
					if (title !== "Edit" && title !== "Delete" && title !== "No." && title !== "Qty") {
							$(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
					} else {
							$(this).html('');
					}
			});

			table_prog_bbn_detail_leas.columns().every(function () {
					var that = this;
					$('input', this.footer()).on('keyup change', function (ev) {
					//if (ev.keyCode == 13) { //only on enter keypress (code 13)
							that
							.search(this.value)
							.draw();
					//}
					});
			});
	}

	function clear(){

			$(".table_prog_bbn").html('');
			$(".table_prog_bbn_detail").html('');
			$(".table_prog_bbn_leasing").html('');
			$(".table_prog_bbn_detail_leas").html('');
	}

</script>

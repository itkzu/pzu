<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'target' => '_blank'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_akhir']['placeholder']);
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                    <button type="submit" class="btn btn-default" name="submit" >
                        <i class="fa fa-print"></i> Cetak
                    </button>
                    <button type="submit" class="btn btn-success" name="submit" value="excel">
                        <i class="fa fa-file-excel-o"></i> Excel
                    </button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover data-table">
                        <thead>
                            <tr>
                                <th style="text-align: center;">NO</th>
                                <th style="text-align: center;">TGL SPK</th>
                                <th style="text-align: center;">NO. SPK</th>
                                <th style="text-align: center;">NAMA PEMBELI</th>
                                <th style="text-align: center;">ALAMAT</th>
                                <th style="text-align: center;">NOMOR HP</th>
                                <th style="text-align: center;">TIPE</th>
                                <th style="text-align: center;">SALESMAN</th>
                                <th style="text-align: center;">SALDO TITIPAN</th>
                            </tr>
                        </thead>
                        <tbody class="body-data"></tbody>
                    </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            get_data();
        });
    });
    
    function get_data(){
        if ( $.fn.DataTable.isDataTable('.data-table') ) {
          $('.data-table').DataTable().destroy();
          $(".body-data").html('');
        }
        var periode_akhir = $("#periode_akhir").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rptadhtpkonsumen/get_data');?>",
            data: {"periode_akhir":periode_akhir},
            beforeSend: function(){
                $(".body-data").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){  
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var sub_total = 0;
                    var no = 1;
                    $.each(obj, function(key, data){
                    $(".body-data").append('<tr>' + 
                                '<td style="text-align: center;">'+no+'</td>' + 
                                '<td style="text-align: center;">'+data.tglso+'</td>' + 
                                '<td style="text-align: center;">'+data.noso+'</td>' + 
                                '<td style="text-align: left;">'+data.nama+'</td>' + 
                                '<td style="text-align: left;">'+data.alamat+'</td>' + 
                                '<td style="text-align: left;">'+data.nohp+'</td>' + 
                                '<td style="text-align: left;">'+data.nmtipe+'</td>' + 
                                '<td style="text-align: left;">'+data.nmsales+'</td>' + 
                                '<td style="text-align: right;">'+numeral(Number(data.byr_ar)).format('0,0')+'</td>' + 
                            '</tr>');
                    sub_total = Number(sub_total) + Number(data.byr_ar);
                    no++;
                    });
                        
                    $(".body-data").append('<tr style="background-color: #dadada;">' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">TOTAL</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' + 
                                '<td style="text-align: right;">'+numeral(Number(sub_total)).format('0,0')+'</td>' + 
                            '</tr>');
                }    
              $(".data-table").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    },
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false
                });                
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",false);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
</script>
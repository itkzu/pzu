<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Fkpo
 *
 * @author
 */
class Fkpo extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(    
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('fkpo/submit'),
            'add' => site_url('fkpo/add'),
            'edit' => site_url('fkpo/edit'),
            'reload' => site_url('fkpo'),
        );
        $this->load->model('fkpo_qry');
        $supplier = $this->fkpo_qry->getDataSupplier();
        foreach ($supplier as $value) {
            $this->data['kdsup'][$value['kdsup']] = $value['nmsup'];
        }
        /*
        $kdaks = $this->fkpo_qry->getKodefkpo();
        $this->data['kdaks'] = array(
            "" => "-- Pilih Kode Aksesoris --",
          );
        foreach ($kdaks as $value) {
            $this->data['kdaks'][$value['kdaks']] = $value['kdaks']." - ".$value['nmaks'];
          }
          */

        $this->data['statppn'] = array(
            "I" => "INCLUDE",
            "E" => "EXCLUDE",
            "N" => "NON-PPN",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 

    public function json_dgview() {
        echo $this->fkpo_qry->json_dgview();
    }

    public function set_nopo() {
        echo $this->fkpo_qry->set_nopo();
    }

    public function set_ket() {
        echo $this->fkpo_qry->set_ket();
    }

    public function getdetPO() {
        echo $this->fkpo_qry->getdetPO();
    }   

    public function json_dgview_detail() {
        echo $this->fkpo_qry->json_dgview_detail();
    }   

    public function addDetail() {
        echo $this->fkpo_qry->addDetail();
    }   

    public function submit() {
        echo $this->fkpo_qry->submit();
    }

    public function batal() {
        echo $this->fkpo_qry->batal();
    }
    private function _init_add(){ 

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
            'nopo'     => array(
                    'placeholder' => 'No. Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nopo',
                    'name'        => 'nopo',
                    'value'       => set_value('nopo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),
            'nmsup'     => array(
                    'placeholder' => 'Supplier',
                    //'type'        => 'hidden',
                    'id'          => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => set_value('nmsup'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'tglpo'     => array(
                    'placeholder' => 'Tgl Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'tgltrm'     => array(
                    'placeholder' => 'Tgl Terima',
                    //'type'        => 'hidden',
                    'id'          => 'tgltrm',
                    'name'        => 'tgltrm',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'tipebayar'     => array(
                    'placeholder' => 'Pembayaran',
                    //'type'        => 'hidden',
                    'id'          => 'tipebayar',
                    'name'        => 'tipebayar',
                    'value'       => set_value('tipebayar'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'jth_tmp'     => array(
                    'placeholder' => 'Jatuh Tempo',
                    //'type'        => 'hidden',
                    'id'          => 'jth_tmp',
                    'name'        => 'jth_tmp',
                    'value'       => set_value('jth_tmp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tgltmp'     => array(
                    'placeholder' => 'Hari',
                    //'type'        => 'hidden',
                    'id'          => 'tgltmp',
                    'name'        => 'tgltmp',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),  
            'sub_tot'     => array(
                    'placeholder' => 'Sub Total',
                    //'type'        => 'hidden',
                    'id'          => 'sub_tot',
                    'name'        => 'sub_tot',
                    'value'       => set_value('sub_tot'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'potongan'     => array(
                    'placeholder' => '(-) Potongan',
                    //'type'        => 'hidden',
                    'id'          => 'potongan',
                    'name'        => 'potongan',
                    'value'       => set_value('potongan'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'disc_top'     => array(
                    'placeholder' => '(-) Discount TOP',
                    //'type'        => 'hidden',
                    'id'          => 'disc_top',
                    'name'        => 'disc_top',
                    'value'       => set_value('disc_top'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'dpp'     => array(
                    'placeholder' => 'Dasar Pengenaan Pajak',
                    //'type'        => 'hidden',
                    'id'          => 'dpp',
                    'name'        => 'dpp',
                    'value'       => set_value('dpp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'ppn'     => array(
                    'placeholder' => 'Pajak Pertambahan Nilai',
                    //'type'        => 'hidden',
                    'id'          => 'ppn',
                    'name'        => 'ppn',
                    'value'       => set_value('ppn'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'total'     => array(
                    'placeholder' => 'TOTALS',
                    //'type'        => 'hidden',
                    'id'          => 'total',
                    'name'        => 'total',
                    'value'       => set_value('total'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'banner'     => array( 
                    //'type'        => 'hidden',
                    'id'          => 'banner',
                    'name'        => 'banner',
                    'value'       => set_value('banner'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'ket'     => array( 
                    // 'type'        => 'hidden',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nopo = $this->uri->segment(3);
        }
        $this->_check_id($nopo);
        $this->data['form'] = array(    
            'nopo'     => array(
                    'placeholder' => 'No. Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nopo',
                    'name'        => 'nopo',
                    'value'       => $this->val[0]['nopox'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'nmsup'     => array(
                    'placeholder' => 'Supplier',
                    //'type'        => 'hidden',
                    'id'          => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => $this->val[0]['nmsup'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'tglpo'     => array(
                    'placeholder' => 'Tgl Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => $this->val[0]['tglpo'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'tgltrm'     => array(
                    'placeholder' => 'Tgl Terima',
                    //'type'        => 'hidden',
                    'id'          => 'tgltrm',
                    'name'        => 'tgltrm',
                    'value'       => $this->val[0]['tglterima'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'tipebayar'     => array(
                    'placeholder' => 'Pembayaran',
                    //'type'        => 'hidden',
                    'id'          => 'tipebayar',
                    'name'        => 'tipebayar',
                    'value'       => $this->val[0]['jnsbayar'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),
            'jth_tmp'     => array(
                    'placeholder' => 'Jatuh Tempo',
                    //'type'        => 'hidden',
                    'id'          => 'jth_tmp',
                    'name'        => 'jth_tmp',
                    'value'       => $this->val[0]['njtempo'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'tgltmp'     => array(
                    'placeholder' => 'Hari',
                    //'type'        => 'hidden',
                    'id'          => 'tgltmp',
                    'name'        => 'tgltmp',
                    'value'       => $this->val[0]['tgljt'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),  
            'sub_tot'     => array(
                    'placeholder' => 'Sub Total',
                    //'type'        => 'hidden',
                    'id'          => 'sub_tot',
                    'name'        => 'sub_tot',
                    'value'       => set_value('sub_tot'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'potongan'     => array(
                    'placeholder' => '(-) Potongan',
                    //'type'        => 'hidden',
                    'id'          => 'potongan',
                    'name'        => 'potongan',
                    'value'       => set_value('potongan'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    // 'readonly' => '',
            ), 
            'disc_top'     => array(
                    'placeholder' => '(-) Discount TOP',
                    //'type'        => 'hidden',
                    'id'          => 'disc_top',
                    'name'        => 'disc_top',
                    'value'       => set_value('disc_top'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    // 'readonly' => '',
            ), 
            'dpp'     => array(
                    'placeholder' => 'Dasar Pengenaan Pajak',
                    //'type'        => 'hidden',
                    'id'          => 'dpp',
                    'name'        => 'dpp',
                    'value'       => set_value('dpp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'ppn'     => array(
                    'placeholder' => 'Pajak Pertambahan Nilai',
                    //'type'        => 'hidden',
                    'id'          => 'ppn',
                    'name'        => 'ppn',
                    'value'       => set_value('ppn'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'total'     => array(
                    'placeholder' => 'TOTAL',
                    //'type'        => 'hidden',
                    'id'          => 'total',
                    'name'        => 'total',
                    'value'       => set_value('total'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'onkeyup'     => 'sum();',
                    'readonly' => '',
            ), 
            'banner'     => array( 
                    //'type'        => 'hidden',
                    'id'          => 'banner',
                    'name'        => 'banner',
                    'value'       => set_value('banner'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'ket'     => array( 
                    //'type'        => 'hidden',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  

            //detail
            'kode'     => array( 
                    'placeholder' => 'Kode',
                    //'type'        => 'hidden',
                    'id'          => 'kode',
                    'name'        => 'kode',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'kdtipe'     => array( 
                    'placeholder' => 'Kode Tipe',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'nmtipe'     => array( 
                    'placeholder' => 'Nama Tipe',
                    //'type'        => 'hidden',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'qty'     => array( 
                    'placeholder' => 'Quantity',
                    //'type'        => 'hidden',
                    'id'          => 'qty',
                    'name'        => 'qty',
                    'value'       => set_value('qty'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'warna'     => array( 
                    'placeholder' => 'Warna',
                    //'type'        => 'hidden',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => set_value('warna'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ), 
            'kdwarna'     => array( 
                    'placeholder' => '',
                    // 'type'        => 'hidden',
                    'id'          => 'kdwarna',
                    'name'        => 'kdwarna',
                    'value'       => set_value('kdwarna'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly' => '',
            ),  
            'harga'     => array( 
                    'placeholder' => 'Harga Per Unit',
                    //'type'        => 'hidden',
                    'id'          => 'harga',
                    'name'        => 'harga',
                    'value'       => set_value('harga'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'nourut'     => array( 
                    // 'placeholder' => 'Harga'
                    'type'        => 'hidden',
                    'id'          => 'nourut',
                    'name'        => 'nourut',
                    'value'       => set_value('nourut'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ),  
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->fkpo_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

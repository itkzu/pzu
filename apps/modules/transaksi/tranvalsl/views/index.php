<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <div class="table-responsive">
                <table class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No. Faktur</th>
                            <th style="text-align: center;width: 100px;">Tgl. PO</th>
                            <th style="text-align: center;width: 50px;">Validasi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">No. Faktur</th>
                            <th style="text-align: center;width: 100px;">Tgl. PO</th>
                            <th style="text-align: center;width: 50px;">Validasi</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<!-- Modal Detail Validasi-->
<div id="detail-validasi" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-detail-validasi">Validasi Nomor Faktur</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 nopo">No. Faktur: </div>
                    <div class="col-lg-6 filedo text-right">File DO: </div>
                    <div class="col-lg-6 tglpo">Tgl PO: </div>
                    <div class="col-lg-6 filesl text-right">File SL: </div>
                    <div class="col-lg-12" style="padding-top: 10px;padding-bottom: 10px;">
                        <label>Pilih Tanggal Terima</label>
                        <input type="hidden" class="form-control" id="nopo" name="nopo" />
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" value="<?php echo date('d-m-Y');?>" 
                                   class="form-control calendar" id="tglterima" name="tglterima" />
                        </div> 
                    </div>
                </div>
                <table class="table table-hover table-detail-validasi">
                    <thead>
                        <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Mesin</th>
                            <th style="text-align: center;">No. Rangka</th>
                            <th style="width: 100px;text-align: center;">Kode Unit</th>
                            <th style="width: 100px;text-align: center;">Kode Warna</th>
                            <th style="width: 70px;text-align: center;">Tahun</th>
                        </tr>
                    </thead>
                    <tbody class="body-detail-validasi"></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-submit">Proses</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-submit").click(function(){
            submit();
        });
        
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        table = $('.dataTable').DataTable({     
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( 
                            //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            //,{ "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(0)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('tranvalsl/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'excel'},
                {extend: 'pdf', 
                    orientation: 'potrait',
                    pageSize: 'legal'
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" && title!=="Validasi" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
    
    function getDetail(nopo){
        if ( $.fn.DataTable.isDataTable('.table-top-sales') ) {
          $('.table-top-sales').DataTable().destroy();
        } 
        $.ajax({
            type: "POST",
            url: "<?=site_url('tranvalsl/getdetail');?>",
            data: {"nopo":nopo},
            success: function(resp){   
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){       
                        $("#nopo").val(data.nopo);
                        $(".nopo").html('No. Faktur : ' + data.nopo);
                        $(".filedo").html('File DO : ' + data.filedo);
                        $(".filesl").html('File SL : ' + data.filesl);
                        $(".tglpo").html('Tgl PO : ' + data.tglpo);
                        $(".body-detail-validasi").append('<tr>' + 
                                                  '<td style="text-align: center;">'+data.nourut+'</td>' + 
                                                  '<td style="text-align: center;">'+data.nosin+'</td>' + 
                                                  '<td style="text-align: center;">'+data.nora+'</td>' + 
                                                  '<td style="text-align: center;">'+data.kode+'</td>' + 
                                                  '<td style="text-align: center;">'+data.kdwarna+'</td>' + 
                                                  '<td style="text-align: center;">'+data.tahun+'</td>' + 
                                              '</tr>');
                  });   
                    
                }  
                
                $(".table-detail-validasi").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right' f>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });    
        $("#detail-validasi").modal({
           show: true, 
           backdrop: 'static'
        });
        $(".title-detail-validasi").html("Validasi Nomor Faktur : " +  nopo);
    }
    
    function submit(){
        swal({
            title: "Konfirmasi Validasi Shipping List !",
            text: "Data yang divalidasi akan masuk ke PO!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nopo =  $("#nopo").val();
            var tglterima =  $("#tglterima").val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("tranvalsl/submit");?>",
                    data: {"nopo":nopo
                            ,"tglterima":tglterima},
                    success: function(resp){   
                        var obj = jQuery.parseJSON(resp);
                        console.log(obj.state);
                        if(obj.state==="1"){
                            swal({
                                title: "Terupdate",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                location.reload();
                            });
                        }else{
                            swal("Error", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
</script>
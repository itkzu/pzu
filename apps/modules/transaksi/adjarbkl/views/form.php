<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-header">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div> 

          <div class="col-xs-12"> 
                <div style="border-top: 1px solid #ddd; height: 10px;"></div> 
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12">
                    <div class="row"> 

                      <div class="col-md-2">
                          <?php 
                            echo form_label($form['noaradjbkl']['placeholder']);
                          ?> 
                      </div>  

                      <div class="col-md-4"> 
                        <div class="form-group">
                          <?php  
                            echo form_input($form['noaradjbkl']);
                            echo form_error('noaradjbkl','<div class="note">','</div>');
                          ?>  
                        </div> 
                      </div>  
                      <div class="col-md-2"> 
                          <?php 
                            echo form_label($form['tglaradjbkl']['placeholder']); 
                          ?>  
                      </div> 
                      <div class="col-md-4">
                        <div class="form-group">
                          <?php  
                            echo form_input($form['tglaradjbkl']);
                            echo form_error('tglaradjbkl','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>  

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                      <div class="col-md-2"> 
                          <?php 
                            echo form_label($form['nosv']['placeholder']); 
                          ?>  
                      </div> 

                      <div class="col-md-4">
                        <div class="form-group">
                          <?php  
                            echo form_input($form['nosv']);
                            echo form_error('nosv','<div class="note">','</div>');
                          ?> 
                        </div> 
                      </div> 

                      <div class="col-md-2"> 
                          <?php 
                            echo form_label($form['tglsv']['placeholder']); 
                          ?>  
                      </div> 

                      <div class="col-md-4">
                        <div class="form-group">
                          <?php  
                            echo form_input($form['tglsv']);
                            echo form_error('tglsv','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 
                    </div>
                  </div> 

                  <div class="col-md-12">
                    <div class="form-group">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row">

                        <div class="col-md-4 ">  
                            <?php 
                              echo form_label($form['nama']['placeholder']);  
                            ?>  
                        </div>

                        <div class="col-md-8 "> 
                          <div class="form-group">
                            <?php  
                              echo form_input($form['nama']);
                              echo form_error('nama','<div class="note">','</div>');
                            ?> 
                          </div>
                        </div>

                        <div class="col-md-4 ">  
                            <?php 
                              echo form_label($form['nama_s']['placeholder']);  
                            ?>  
                        </div>

                        <div class="col-md-8 "> 
                          <div class="form-group">
                            <?php  
                              echo form_input($form['nama_s']);
                              echo form_error('nama_s','<div class="note">','</div>');
                            ?> 
                          </div>
                        </div>

                        <div class="col-md-4 ">  
                            <?php 
                              echo form_label($form['alamat']['placeholder']);  
                            ?>  
                        </div> 

                        <div class="col-md-8 "> 
                          <div class="form-group">
                            <?php  
                              echo form_input($form['alamat']);
                              echo form_error('alamat','<div class="note">','</div>');
                            ?> 
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row">

                      <!-- <div class="col-md-12 "> 
                        <div class="form-group">
                          <?php 
                            echo form_label($form['jnsbayar']['placeholder']); 
                            echo form_input($form['jnsbayar']);
                            echo form_error('jnsbayar','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> -->

                      <div class="col-md-4 ">  
                          <?php 
                            echo form_label($form['jenis']['placeholder']);  
                          ?>  
                      </div> 

                      <div class="col-md-8 "> 
                        <div class="form-group">
                          <?php  
                            echo form_dropdown( $form['jenis']['name'], $form['jenis']['data'], $form['jenis']['value'], $form['jenis']['attr']);
                            echo form_error('jenis','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 

                      <div class="col-md-4 ">  
                          <?php 
                            echo form_label($form['nilai_ar']['placeholder']);  
                          ?>  
                      </div> 

                      <div class="col-md-8 "> 
                        <div class="form-group">
                          <?php  
                            echo form_input($form['nilai_ar']);
                            echo form_error('nilai_ar','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 

                      <div class="col-md-4 "> 
                          <?php 
                            echo form_label($form['nilai_byr']['placeholder']);  ?>
                      </div> 

                      <div class="col-md-8 "> 
                        <div class="form-group">
                          <?php  
                            echo form_input($form['nilai_byr']);
                            echo form_error('nilai_byr','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 

                      <div class="col-md-4 ">  
                          <?php 
                            echo form_label($form['nilai_adj']['placeholder']);  
                          ?>  
                      </div> 

                      <div class="col-md-8 "> 
                        <div class="form-group">
                          <?php  
                            echo form_input($form['nilai_adj']);
                            echo form_error('nilai_adj','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 

                      <div class="col-md-4 ">  
                          <?php 
                            echo form_label($form['saldo']['placeholder']);  
                          ?>  
                      </div> 

                      <div class="col-md-8 "> 
                        <div class="form-group">
                          <?php  
                            echo form_input($form['saldo']);
                            echo form_error('saldo','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 

                    </div>
                  </div> 

                  <div class="col-md-2">  
                      <?php 
                        echo form_label($form['ket']['placeholder']); 
                      ?>  
                  </div>  

                  <div class="col-md-10"> 
                    <div class="form-group">
                      <?php  
                        echo form_input($form['ket']);
                        echo form_error('ket','<div class="note">','</div>');
                      ?> 
                    </div> 
                  </div>  
  
                  <!-- <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-2 col-md-2 ">
                          <div class="row"> 

                              <div class="col-md-12"> 
                                    <?php
                                        echo form_input($form['status']);
                                        echo form_error('status','<div class="note">','</div>');
                                    ?> 
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>  -->

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-md-12">
                          <label>&nbsp;</label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div> 
                    </div>
                  </div> 
                </div>

              </div>
          </div>
           <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>          
 




<script type="text/javascript">
    $(document).ready(function () {
        reset(); 

        $("#noaradjbkl").attr("readonly",true); 
        $("#tglaradjbkl").attr("disabled",true); 
        $("#nosv").attr("readonly",false);  
        $("#jenis").attr("disabled",true);  
        $("#nilai_adj").attr("disabled",true);  
        $("#ket").attr("disabled",true);  

        $("#nosv").keyup(function(){
            set_nosv();
        }); 

        $('.btn-batal').click(function(){ 
            batal();    
        });

    });

    function set_nosv() {
        var nosv = $('#nosv').val();

        $.ajax({
            type: "POST",
            url: "<?=site_url("adjarbkl/set_nosv");?>",
            data: {"nosv":nosv},
            success: function(resp){ 

              // if(resp==='"empty"'){ 
              //     empty2(); 
              // }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){ 
                      $("#tglsv").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgl_nota_servis))); 
                      $("#nama").val(data.nama_pembawa);
                      $("#nama_s").val(data.nama_pemilik);
                      $("#alamat").val(data.alamat); 

                      $("#jenis").attr('disabled',false); 
                      $("#nilai_adj").attr('disabled',false); 
                      $("#ket").attr('disabled',false); 
                      set_harga(); 
                  });
              // }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_harga() {
        var nosv = $('#nosv').val();
        var jenis = $('#jenis').val();

        $.ajax({
            type: "POST",
            url: "<?=site_url("adjarbkl/set_harga");?>",
            data: {"nosv":nosv, "jenis":jenis},
            success: function(resp){ 

              // if(resp==='"empty"'){ 
              //     empty2(); 
              // }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){ 
                      $("#tglsv").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgl_nota_servis))); 
                      $("#nama").val(data.nama_pembawa);
                      $("#nama_s").val(data.nama_pemilik);
                      $("#alamat").val(data.alamat); 

                      $("#jenis").attr('disabled',false); 
                      $("#nilai_adj").attr('disabled',false); 
                      $("#ket").attr('disabled',false); 
                      set_harga(); 
                  });
              // }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function reset(){
      // $("#noabj").val('ABJ-____-____');
      $("#tglabj").val($.datepicker.formatDate('dd-mm-yy', new Date()));  
    } 

    function batal(){ 
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
                window.location.href = '<?=site_url('adjarbkl');?>';  
            }); 
    }
</script>

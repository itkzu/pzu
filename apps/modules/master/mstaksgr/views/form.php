<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_input($form['kdaksgr']);

                              echo form_label($form['nmaksgr']['placeholder']);
                              echo form_input($form['nmaksgr']);
                              echo form_error('nmaksgr','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['kdakun']['placeholder']);
                              echo form_dropdown( $form['kdakun']['name'],
                                                  $form['kdakun']['data'] ,
                                                  $form['kdakun']['value'] ,
                                                  $form['kdakun']['attr']);
                              echo form_error('kdakun','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-3">
                  <div class="row">
                      <div class="form-group">
                          <?=form_label($form['faktif']['placeholder']);?>
                          <div class="checkbox">
            							    <?php
                                  echo form_checkbox($form['faktif']);
            							    ?>
            					    </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        if(!$('#kdaksgr').val()){
            $('#faktif').hide();
            $('.btn-submit').click(function(){
                submit();
            });
        } else if($('#kdaksgr').val()){
            $('#faktif').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            });
            $('#faktif').show();
            $('.btn-submit').click(function(){
            update();
            });
        }
    });

    function submit(){
        var kdakun = $("#kdakun").val();
        var nmaksgr = $("#nmaksgr").val().toUpperCase();
        //alert(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstaksgr/submit");?>",
      		data: {"nmaksgr":nmaksgr,"kdakun":kdakun},
      		beforeSend: function() {

      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              if (data.tipe==="success"){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      window.location.href = '<?=site_url('mstaksgr');?>';
                  });
              }else{
                  refresh();
              }
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function update(){
        var kdaksgr = $("#kdaksgr").val();
        var kdakun = $("#kdakun").val();
        var nmaksgr = $("#nmaksgr").val().toUpperCase();
        if ($("#faktif").prop("checked")){
          var faktif = 't';
        } else {
          var faktif = 'f';
        }
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstaksgr/update");?>",
      		data: {"kdaksgr":kdaksgr,"nmaksgr":nmaksgr,"kdakun":kdakun,"faktif":faktif},
      		beforeSend: function() {

      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              if (data.tipe==="success"){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      window.location.href = '<?=site_url('mstaksgr');?>';
                  });
              }else{
                  refresh();
              }
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function refresh(){
      window.location.reload();
    }
</script>

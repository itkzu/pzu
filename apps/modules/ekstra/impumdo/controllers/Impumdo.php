<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Impumdo
 *
 * @author adi
 */
class Impumdo extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('impumdo/submit'),
            'add' => site_url('impumdo/add'),
            'edit' => site_url('impumdo/edit'),
            'reload' => site_url('impumdo'),
        );
        $this->load->model('impumdo_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $res = $this->impumdo_qry->submit();
        if($res['state']=="0"){
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }else{
            redirect("impumdo");
        }
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <!-- <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>  -->

            <div class="box-header">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div>
            <div class="box-body body">
                <div class="row">

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['nodo']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['nodo']);
                                  echo form_error('nodo','<div class="note">','</div>');
                              ?>
                          </div>

                          <div class="col-xs-1"> 
                          </div>

                          <div class="col-xs-2 ">
                              <?php echo form_label($form['noso']['placeholder']); ?>
                          </div>

                          <div class="col-xs-2 ">
                              <?php
                                  echo form_input($form['noso']);
                                  echo form_error('noso','<div class="note">','</div>');
                              ?>
                          </div> 

                      </div>
                  </div>

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['tgldo']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['tgldo']);
                                  echo form_error('tgldo','<div class="note">','</div>');
                              ?>
                          </div>  

                          <div class="col-xs-1"> 
                          </div>


                          <div class="col-xs-2">
                              <?php echo form_label($form['tglso']['placeholder']); ?>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['tglso']);
                                  echo form_error('tglso','<div class="note">','</div>');
                              ?>
                          </div>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">
                          <br>
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['nopoleasing']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['nopoleasing']);
                                  echo form_error('nopoleasing','<div class="note">','</div>');
                              ?>
                          </div>   

                          <div class="col-xs-2">
                              <?php echo form_label($form['nama_p']['placeholder']); ?>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['nama_p']);
                                  echo form_error('nama_p','<div class="note">','</div>');
                              ?>
                          </div>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['tglterbitpo']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['tglterbitpo']);
                                  echo form_error('tglterbitpo','<div class="note">','</div>');
                              ?>
                          </div>

                          <div class="col-xs-1"> 
                          </div>   

                          <div class="col-xs-2">
                              <?php echo form_label($form['ket_ar_leas']['placeholder']); ?>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['ket_ar_leas']);
                                  echo form_error('ket_ar_leas','<div class="note">','</div>');
                              ?>
                          </div>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['tgltrmpo']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['tgltrmpo']);
                                  echo form_error('tgltrmpo','<div class="note">','</div>');
                              ?>
                          </div> 
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['tgltagihleas']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['tgltagihleas']);
                                  echo form_error('tgltagihleas','<div class="note">','</div>');
                              ?>
                          </div>

                          <div class="col-xs-1"> 
                          </div>   

                          <div class="col-xs-2">
                              <?php echo form_label($form['nama']['placeholder']); ?>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['nama']);
                                  echo form_error('nama','<div class="note">','</div>');
                              ?>
                          </div>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-5"> 
                          </div>   

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['nama_s']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['nama_s']);
                                  echo form_error('nama_s','<div class="note">','</div>');
                              ?>
                          </div>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['nmleasing']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['nmleasing']);
                                  echo form_error('nmleasing','<div class="note">','</div>');
                              ?>
                          </div>  

                          <div class="col-xs-2">
                              <?php echo form_label($form['alamat']['placeholder']); ?>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['alamat']);
                                  echo form_error('alamat','<div class="note">','</div>');
                              ?>
                          </div>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['nmprogleas']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-3">
                              <?php
                                  echo form_input($form['nmprogleas']);
                                  echo form_error('nmprogleas','<div class="note">','</div>');
                              ?>
                          </div>   
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row"> 
                          <br>  
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['pl']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['pl']);
                                  echo form_error('pl','<div class="note">','</div>');
                              ?>
                          </div>   
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['jp']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['jp']);
                                  echo form_error('jp','<div class="note">','</div>');
                              ?>
                          </div>   
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['matriks']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['matriks']);
                                  echo form_error('matriks','<div class="note">','</div>');
                              ?>
                          </div>   
                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php echo form_label($form['total']['placeholder']); ?>
                              </div>
                          </div>

                          <div class="col-xs-2">
                              <?php
                                  echo form_input($form['total']);
                                  echo form_error('total','<div class="note">','</div>');
                              ?>
                          </div>   
                      </div>
                  </div>  

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                </div>
            </div>
            <!-- /.box-body -->
            <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function () {
        $('input').prop('required',true);
        // sales_ins();
        autonum();  
        sum();
        $('.btn-submit').click( function () {
          submit()
        });

        $('.btn-batal').click( function () {
          batal();
        });

    }); 

    function autonum(){

      //baris 2
        $('#pl').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#jp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#matriks').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#total').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'}); 
    } 

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('trmpoleas');?>';
        });
    }

    function submit(){ 
        swal({
            title: "Transaksi Terima PO Leasing akan disimpan. Lanjutkan?",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var noso          = $("#noso").val();
            var nopoleasing   = $("#nopoleasing").val();
            var tglterbitpo   = $("#tglterbitpo").val();
            var tgltrmpo      = $("#tgltrmpo").val();
            var tgltagihleas  = $("#tgltagihleas").val();
            var nama_p        = $("#nama_p").val();
            var ket_ar_leas   = $("#ket_ar_leas").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("trmpoleas/save");?>",
                data: {"noso":noso
                      ,"nopoleasing":nopoleasing
                      ,"tglterbitpo":tglterbitpo
                      ,"tgltrmpo":tgltrmpo
                      ,"tgltagihleas":tgltagihleas
                      ,"nama_p":nama_p
                      ,"ket_ar_leas":ket_ar_leas },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){ 
                          // ctk_lr(nodo);x
                            window.location.href = "<?=site_url('trmpoleas');?>";
                        }
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function sum(){

      //baris 2
        var pl      = $('#pl').autoNumeric('get');
        var jp      = $('#jp').autoNumeric('get');
        var matriks = $('#matriks').autoNumeric('get');

        var total = parseInt(pl) + parseInt(jp) + parseInt(matriks);
        if(!isNaN(total)){
            $('#total').autoNumeric('set', total);
        } 
    } 
</script>

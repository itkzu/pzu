<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
	td.details-control {
		background: url('<?= base_url('assets/plugins/dtables/resource/details_open.png'); ?>') no-repeat center center;
		cursor: pointer;
	}

	tr.shown td.details-control {
		background: url('<?= base_url('assets/plugins/dtables/resource/details_close.png'); ?>') no-repeat center center;
	}

	.tab-content {
		padding:5px;
		border-left:1px solid #DDD;
		border-bottom:1px solid #DDD;
		border-right:1px solid #DDD;
	}

	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}

	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
						, 'enctype' => 'multipart/form-data'
						, 'target' => '_blank'
						, 'data-validate' => 'parsley');
					echo form_open($submit,$attributes);
				?>
				<div class="box-body">
					<div class="form-group">
						<?php
							echo '<div class="radio">';
							echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Periode Bulan <small>per bulan</small>');
							echo '</div>';
							echo form_input($form['periode']);
							echo form_error('akun_multi','<div class="note">','</div>');
						?>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<?php
									echo '<div class="radio">';
									echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Periode Awal');
									echo '</div>';
									echo form_input($form['periode_awal']);
									echo form_error('periode_awal','<div class="note">','</div>');
								?>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<?php
									echo form_label('Pilih Periode Akhir');
									echo form_input($form['periode_akhir']);
									echo form_error('periode_akhir','<div class="note">','</div>');
								 ?>
							</div>

						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
					</div>

					<hr>
					<div class="table-responsive">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#example2-tab1" aria-controls="example2-tab1" role="tab" data-toggle="tab">Tabel</a></li>
							<li role="presentation"><a href="#example2-tab2" aria-controls="example2-tab2" role="tab" data-toggle="tab">Tabel 2</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="example2-tab1">
								<table class="table table-striped table-bordered table-condensed dataTable" style="width:100%" id="myTable1">
									<thead>
										<tr>
											<th style="width: 1%;text-align: center;">D</th>
											<th style="width: 10%;text-align: center;">No. Pengeluaran</th>
											<th style="width: 7%;text-align: center;">Tanggal</th>
											<th style="width: 7%;text-align: center;">No. DO</th>
											<th style="width: 13%;text-align: center;">Showroom/Pos</th>
											<th style="width: 7%;text-align: center;">Keterangan</th>
											<th style="width: 6%;text-align: center;">Quantity</th>
											<th style="width: 7%;text-align: center;">Total</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th style="text-align: center;">DETAIL</th>
											<th style="text-align: center;"></th>
											<th style="text-align: center;"></th>
											<th style="text-align: center;"></th>
											<th style="text-align: center;"></th>
											<th style="text-align: center;"></th>
											<th style="text-align: center;"></th>
											<th style="text-align: center;"></th>
										</tr>
									</tfoot>
									<tbody></tbody>
								</table>
							</div>

							<div role="tabpanel" class="tab-pane fade" id="example2-tab2">
								<table id="myTable2" class="table table-striped table-bordered table-condensed" cellspacing="0">
									<thead>
										<tr>
											<th style="width: 20%;text-align: center;">No. Pengeluaran</th>
											<th style="text-align: center;">Tanggal</th>
											<th style="width: 5%;text-align: center;">No. DO</th>
											<th style="width: 15%;text-align: center;">Showroom/Pos</th>
											<th style="text-align: center;">Keterangan</th>
											<th style="width: 15%;text-align: center;">Nama Barang</th>
											<th style="text-align: center;">Quantity</th>
											<th style="text-align: center;">Harga</th>
											<th style="text-align: center;">Sub Total</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th style="text-align: center;">No. Pengeluaran</th>
											<th style="text-align: center;">Tanggal</th>
											<th style="text-align: center;">No. DO</th>
											<th style="text-align: center;">Showroom/Pos</th>
											<th style="text-align: center;">Keterangan</th>
											<th style="text-align: center;">Nama Barang</th>
											<th style="text-align: center;">Quantity</th>
											<th style="text-align: center;">Harga</th>
											<th style="text-align: center;">Sub Total</th>
										</tr>
									</tfoot>
									<tbody></tbody>
								</table>
							</div>
						</div>

						<?php echo form_close(); ?>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->

		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		disable();

		$("#rdakun_multi").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});

		$("#rdakun_between").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});

		$('#periode').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$(".btn-tampil").click(function(){
			$(".dataTable").show();
			tampilData();
		});

		$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
			$($.fn.dataTable.tables(true)).DataTable()
				.columns.adjust()
				.responsive.recalc();
		});
	});



	function tampilData(){

		var periode = $("#periode").val();
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val();
		var periode_d = $('#rdakun_multi').is(':checked');

		if(periode_d){
			var ket = "periode_multi";
		} else {
			var ket = "periode_between";
		}


		var column = [];

		column.push({
			"aTargets": [2],
			"mRender": function (data, type, full) {
				return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});

		column.push({
			"aTargets": [ 7 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
			},
			"sClass": "right"
		});

		column.push({
			"aTargets": [ 6 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "center"
		});


		$('.dataTableDetail').DataTable();

		table = $('#myTable1').DataTable({
			"aoColumnDefs": column,
			"order": [[ 1, "asc" ]],
			"columns": [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{"data": "noakso"},
				{"data": "tglakso"},
				{"data": "nodo"},
				{"data": "nmdiv"},
				{"data": "ket"},
				{"data": "qty"},
				{"data": "total"}
			],
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function (sSource, aoData, fnCallback) {
				aoData.push({"name": "periode", "value": periode}
							, {"name": "periode_awal", "value": periode_awal}
							, {"name": "periode_akhir", "value": periode_akhir}
							, {"name": "ket", "value": ket});

				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptakso/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('#myTable1').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		// Add event listener for opening and closing details
		$('#myTable1 tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				//format(row.data());
				tr.addClass('shown');
			}
		} );

		$('#myTable1 tfoot th').each( function () {
			var title = $('#myTable1 thead th').eq( $(this).index() ).text();
			if(title!=="D" && title!=="EDIT" && title!=="DELETE"){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
				that
					.search( this.value )
					.draw();
				//}
			} );
		});




	//tabel ke dua
		var column2 = [];

		column2.push({
			"aTargets": [ 1 ],
			"mRender": function (data, type, full) {
			return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});

		column2.push({
			"aTargets": [ 7,8 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
			},
			"sClass": "right"
		});

		column2.push({
			"aTargets": [ 6 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "center"
		});

		table2 = $('#myTable2').DataTable({
			"aoColumnDefs": column2,
			"order": [[ 1, "asc" ]],
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"aaSorting": [],
			"fnServerData": function (sSource, aoData, fnCallback) {
				aoData.push({"name": "periode", "value": periode}
							, {"name": "periode_awal", "value": periode_awal}
							, {"name": "periode_akhir", "value": periode_akhir}
							, {"name": "ket", "value": ket});

				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(0)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptakso/json_dgview_2');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('#myTable2').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('#myTable2 tfoot th').each( function () {
			var title = $('#myTable2 thead th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" ){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table2.columns().every( function () {
		var that = this;
		$( 'input', this.footer() ).on( 'keyup change', function (ev) {
			//if (ev.keyCode == 13) { //only on enter keypress (code 13)
				that
					.search( this.value )
					.draw();
			//}
			} );
		});
	}

	function format ( d ) {
		//console.log(d);
		var tdetail = '<table class="table table-bordered table-hover dataTableDetail">'+
		'<thead>' +
			'<tr style="background-color: #d73925;color: #fff;">'+
			'<th style="text-align: center;width:16px;"></th>'+
			'<th style="text-align: center;width:10px;">NO.</th>'+
			'<th style="text-align: center;">Nama Barang</th>'+
			'<th style="text-align: center;">Quantity</th>'+
			'<th style="text-align: center;">Harga</th>'+
			'<th style="text-align: center;">Sub Total</th>'+
			'</tr>'+
		'</thead><tbody>';
		var no = 1;
		var total = 0;
		if(d.detail.length>0){
		$.each(d.detail, function(key, data){
			//console.log(data);
			tdetail+='<tr>'+
			'<td style="text-align: center;"></td>'+
			'<td style="text-align: center;">'+no+'</td>'+
			'<td style="text-align: left;">'+data.nmaks+'</td>'+
			'<td style="text-align: center;">'+numeral(data.qty).format('0,0')+'</td>'+
			'<td style="text-align: right;">'+numeral(data.harga).format('0,0.00')+'</td>'+
			'<td style="text-align: right;">'+numeral(data.total).format('0,0.00')+'</td>'+
			'</tr>';
			no++;
		});

		}
		/*
		tdetail += '<tfoot>' +
			'<tr>'+
			'<th style="text-align: right;" colspan="8">TOTAL</th>'+
			'<th style="text-align: right;">'+numeral(total).format('0,0.00')+'</th>'+
			'</tr>'+
			'</tfoot>';
		*/
		tdetail += '</tbody></table>';
		return tdetail;
	}

	function disable(){
		$(".dataTable").hide();
		$("#periode").prop("disabled", true);
		$(".btn-tampil").prop("disabled", true);
		$("#periode_awal").prop("disabled", true);
		$("#periode_akhir").prop("disabled", true);
	}

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#periode").prop("disabled", false);
			$("#periode_awal").prop("disabled", true);
			$("#periode_akhir").prop("disabled", true);
		}else{
			$("#periode").prop("disabled", true);
			$("#periode_awal").prop("disabled", false);
			$("#periode_akhir").prop("disabled", false);
		}
	}

</script>

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Poskb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getref(){
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->order_by('nmkb');
        $q = $this->db->get("pzu.kasbank");
        return $q->result_array();
    }

    public function getkdrefkb() {
      $this->db->where("faktif","true");
      $q = $this->db->get("pzu.refkb_m");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function det_kb() {
      $nokb = $this->input->post('nokb');
      $nourut = $this->input->post('nourut');
      $kdrefkb = $this->input->post('kdrefkb');
      $query = $this->db->query("select a.*, case when b.nourut is null then 0 else b.nourut end as nourut2 from pzu.v_kb_posting a left join pzu.t_kb_ar b on a.nokb = b.nokb  where a.nokb = '".$nokb."' and a.nourut = '".$nourut."' and a.kdrefkb = '".$kdrefkb."'");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdkb']) ){
            if($_GET['kdkb']){
                $kdkb = $_GET['kdkb'];
            }else{
                $kdkb = '';
            }
        }else{
            $kdkb = '';
        }

        $aColumns = array('no',
                            'nmkb',
                            'nocetak',
                            'tglkb',
                            'nmrefkb',
                            'drkpd',
                            'ket',
                            'debet',
                            'kredit',
                            'nokb',
                            'nourut',
                            'kdrefkb'
                        );
        $sIndexColumn = "tglkb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        // if($this->perusahaan[0]['kddiv']===$this->session->userdata('data')['kddiv']){
          // $sTable = " (SELECT nokb,nocetak,tglkb,kdkb,nmkb,kdakun,dk,debet,kredit,ket,drkpd,nofaktur,nourut,kdrefkb,nmrefkb,fposting,nilai
          //                  FROM bkl.v_kb_posting order by nokb , nourut asc where kddiv_head = '".$kddiv."'
          //               ) AS a";
        // } else {
          $sTable = " (SELECT ''::varchar as no, a.*, b.nourut as nourut2 FROM pzu.v_kb_posting a left join pzu.t_kb_ar b on a.nokb = b.nokb where a.kdkb = '".$kdkb."' order by a.tglkb 
                      ) AS a";
        // }
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                            ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
               $sOrder = "";
            }
        }
        $sWhere = "where fposting=0";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " and (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if($aRow[ $aColumns[$i] ]===null){
                    $aRow[ $aColumns[$i] ] = '';
                }
                if(($i>=7 && $i<=7)){
                    $row[] = (float) $aRow[ $aColumns[$i] ];

                }else{
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
                //23 - 28
            $row[7] = (float)$aRow['debet']; //number_format($saldo, 2);
            $row[8] = (float)$aRow['kredit']; //number_format($saldo, 2);
            $row[9] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs btn-proses \" onclick=\"posting('".$aRow['nokb']."',".$aRow['nourut'].",".$aRow['kdrefkb'].");\">Posting</button>";
            $output['aaData'][] = $row;
        }
        echo  json_encode( $output );
    }

    public function det_json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        if( isset($_GET['jenis']) ){
            if($_GET['jenis']){
                $jenis = $_GET['jenis'];
            }else{
                $jenis = '';
            }
        }else{
            $jenis = '';
        }
        if( isset($_GET['nourut']) ){
            if($_GET['nourut']){
                $nourut = $_GET['nourut'];
            }else{
                $nourut = 0;
            }
        }else{
            $nourut = '';
        }
        if( isset($_GET['kdrefkb']) ){
            if($_GET['kdrefkb']){
                $kdrefkb = $_GET['kdrefkb'];
            }else{
                $kdrefkb = 0;
            }
        }else{
            $kdrefkb = '';
        }

        $aColumns = array('no',
                            'nmakun',
                            'debet',
                            'kredit'
                        );
        $sIndexColumn = "nokb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 
          $sTable = " (SELECT '' as no, * FROM pzu.v_detkb_posting('".$nokb."',".$nourut.",".$kdrefkb.",'".$jenis."') ORDER BY kdakun asc) AS a"; 
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                            ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
               $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " and (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere"; //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function det_k_json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        if( isset($_GET['jenis']) ){
            if($_GET['jenis']){
                $jenis = $_GET['jenis'];
            }else{
                $jenis = '';
            }
        }else{
            $jenis = '';
        }
        if( isset($_GET['kdrefkb']) ){
            if($_GET['kdrefkb']){
                $kdrefkb = $_GET['kdrefkb'];
            }else{
                $kdrefkb = 0;
            }
        }else{
            $kdrefkb = '';
        }
        if( isset($_GET['nourut']) ){
            if($_GET['nourut']){
                $nourut = $_GET['nourut'];
            }else{
                $nourut = 0;
            }
        }else{
            $nourut = '';
        }
        // echo $nourut;
        $aColumns = array('no',
                            'nmakun',
                            'debet',
                            'kredit'
                        );
        $sIndexColumn = "nokb";
        $sLimit = "";
        // echo $nourut;
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        } 
          $sTable = " (SELECT '' as no, * FROM pzu.v_detkb_posting('".$nokb."',".$nourut.",0,'".$jenis."') ORDER BY kdakun asc) AS a"; 
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                            ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
               $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " and (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere"; //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function proses() {
        $user = $this->session->userdata("username"); 
        $kddiv = $this->session->userdata('data')['kddiv'];
        $jenis = $this->input->post('jenis');
        $nourut = $this->input->post('nourut');
        $kdrefkb = $this->input->post('kdrefkb');
        if($kdrefkb===''){
            $kdrefkb=0;
        }
        
        $nokb = $this->input->post('nokb');

        if($jenis==='UM'){
            $q = $this->db->query("select code,title,msg,tipe from pzu.kb_posting_web('". $kddiv ."','". $nokb ."',". $nourut .",". $kdrefkb .",'". $user ."')");
        } else {
            $q = $this->db->query("select title,msg,tipe from pzu.kb_posting_web('". $kddiv ."','". $nokb ."','". $jenis ."',". $nourut .",". $kdrefkb .",'". $user ."')");
        }

        
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

}

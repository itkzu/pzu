<?php
/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>

<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->

			<?php
			$attributes = array(
				'role=' => 'form'
				, 'id' => 'form_add'
				, 'name' => 'form_add'
				, 'enctype' => 'multipart/form-data'
				, 'target' => '_blank'
				, 'data-validate' => 'parsley');
			echo form_open($submit, $attributes);
			?>

			<div class="box-body">
				<div class="row">

					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_label($form['kddiv']['placeholder']);
								echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
								echo form_error('kddiv', '<div class="note">', '</div>');
								?>
							</div>
					</div>

					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_label('Pilih Bulan &nbsp;');
								echo form_dropdown($form['bulan_awal']['name'],$form['bulan_awal']['data'] ,$form['bulan_awal']['value'] ,$form['bulan_awal']['attr']);
								echo form_error('bulan_awal','<div class="note">','</div>');
							?>
						</div>
					</div>

						<div class="col-lg-2">
							<div class="form-group">
								<?php
										echo form_label('Pilih Tahun&nbsp; ');
										echo form_dropdown($form['tahun']['name'],$form['tahun']['data'] ,$form['tahun']['value'] ,$form['tahun']['attr']);
										echo form_error('tahun','<div class="note">','</div>');
								?>
							</div>
						</div>

					<!-- <div class="col-lg-3">
						<div class="form-group">

							<?php
								echo form_label($form['periode_awal']['placeholder']);
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal', '<div class="note">', '</div>');
							?>

						</div>
					</div>  -->

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_input($form['nmdiv']);
								echo form_error('nmdiv', '<div class="note">', '</div>');
							?>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-12">
								<div class="checkbox">
									<label>
										<?php
											$data_chk_myd = array(
												'name' => 'compare_myd',
												'id' => 'compare_myd',
												'value' => 'true',
												'checked' => FALSE,
											);
											echo form_checkbox($data_chk_myd) . " Bandingkan dengan periode sebelumnya";
										?>
									</label>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="checkbox">
									<label>
										<?php
											$data_chk_nol = array(
												'name' => 'is_nol',
												'id' => 'is_nol',
												'value' => 'true',
												'checked' => FALSE,
											);
											echo form_checkbox($data_chk_nol) . " Tampilkan akun bernilai nol";
										?>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="submit" value="html">
						<i class="fa fa-print"></i> Cetak
					</button>
					<button type="submit" class="btn btn-success" name="submit" value="excel">
						<i class="fa fa-file-excel-o"></i> Export Ke Excel
					</button>
					<a href="<?php echo $reload; ?>" class="btn btn-default">
						Batal
					</a>
				</div>
<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		$('#periode_awal').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		var nmdiv = $('option:selected', this).html();
		$('#nmdiv').val(nmdiv);

		$("#kddiv").change(function(){
			nmdiv = $('option:selected', this).html();
			$('#nmdiv').val(nmdiv);
		}); 
	});

</script>

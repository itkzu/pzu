<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Trmbbn
 *
 * @author adi
 */
class Trmbbn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('trmbbn/submit'),
            'add' => site_url('trmbbn/add'),
            'edit' => site_url('trmbbn/edit'),
            'ctk'      => site_url('trmbbn/ctk'),
            'reload' => site_url('trmbbn'),
        );
        $this->load->model('trmbbn_qry');
        $kota = $this->trmbbn_qry->getkota();
        foreach ($kota as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk() {
        $nokps = $this->uri->segment(3);   
        $this->data['data'] = $this->trmbbn_qry->ctk($nokps);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function getNoID() {
        echo $this->trmbbn_qry->getNoID();
    }

    public function set_notbj() {
        echo $this->trmbbn_qry->set_notbj();
    }

    public function set_nosin() {
        echo $this->trmbbn_qry->set_nosin();
    }

    public function submit() {
        echo $this->trmbbn_qry->submit();
    }

    public function tambah() {
        echo $this->trmbbn_qry->tambah();
    }

    public function batal() {
        echo $this->trmbbn_qry->batal();
    }

    public function delDetail() {
        echo $this->trmbbn_qry->delDetail();
    }

    public function json_dgview() {
        echo $this->trmbbn_qry->json_dgview();
    }

    public function json_dgview_det() {
        echo $this->trmbbn_qry->json_dgview_det();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
           'notbj'=> array(
                    'placeholder' => 'No. Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'notbj',
                    'name'        => 'notbj',
                    'value'       => set_value('notbj'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgltbj'=> array(
                    'placeholder' => 'Tgl Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'tgltbj',
                    'name'        => 'tgltbj',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
           'total'=> array(
                    'placeholder' => 'Sub Total',
                    //'type'        => 'hidden',
                    'id'          => 'total',
                    'name'        => 'total',
                    'value'       => set_value('total'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ), 
           'disc'=> array(
                    'placeholder' => '(-) Discount',
                    //'type'        => 'hidden',
                    'id'          => 'disc',
                    'name'        => 'disc',
                    'value'       => set_value('disc'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum();',
                    'style'       => 'text-align: right;',
            ), 
           'pph21'=> array(
                    'placeholder' => '(-) PPH 21',
                    //'type'        => 'hidden',
                    'id'          => 'pph21',
                    'name'        => 'pph21',
                    'value'       => set_value('pph21'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum();',
                    'style'       => 'text-align: right;',
            ),
           'alltot'=> array( 
                    'placeholder' => 'Total',
                    //'type'        => 'hidden',
                    'id'          => 'alltot',
                    'name'        => 'alltot',
                    'value'       => set_value('alltot'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ), 
           'banner'=> array( 
                    //'type'        => 'hidden',
                    'id'          => 'banner',
                    'name'        => 'banner',
                    'value'       => set_value('banner'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),

           //detail
           'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => '',
            ),
           'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Nama Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'alamat'=> array(
                    'placeholder' => 'Alamat Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'kota'=> array(
                    'placeholder' => 'Kota',
                    //'type'        => 'hidden',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    //'type'        => 'hidden',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => set_value('warna'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tahun'=> array(
                    'placeholder' => 'Tahun',
                    //'type'        => 'hidden',
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'value'       => set_value('tahun'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'bbn'=> array(
                    'placeholder' => 'Biaya BBN',
                    //'type'        => 'hidden',
                    'id'          => 'bbn',
                    'name'        => 'bbn',
                    'value'       => set_value('bbn'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum1();',
                    'style'       => 'text-align: right;',
                    'required'    => '',
            ),
           'jasa'=> array(
                    'placeholder' => 'Jasa',
                    //'type'        => 'hidden',
                    'id'          => 'jasa',
                    'name'        => 'jasa',
                    'value'       => set_value('jasa'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum1();',
                    'style'       => 'text-align: right;',
                    'required'    => '',
            ),
           'tot_by'=> array( 
                    'placeholder' => 'Total Biaya',
                    //'type'        => 'hidden',
                    'id'          => 'tot_by',
                    'name'        => 'tot_by',
                    'value'       => set_value('tot_by'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ), 
        );
    }
     private function _init_edit($no = null){
        if(!$no){
            $nokb = $this->uri->segment(3);
        }
        $this->_check_id($nokb);
        $this->data['form'] = array(
           'notbj'=> array(
                    'placeholder' => 'No. Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'notbj',
                    'name'        => 'notbj',
                    'value'       => $this->val[0]['nokps'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgltbj'=> array(
                    'placeholder' => 'Tgl Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'tgltbj',
                    'name'        => 'tgltbj',
                    'value'       => $this->val[0]['tglkps'],
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
           'total'=> array(
                    'placeholder' => 'Sub Total',
                    //'type'        => 'hidden',
                    'id'          => 'total',
                    'name'        => 'total',
                    'value'       => set_value('total'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ), 
           'disc'=> array(
                    'placeholder' => '(-) Discount',
                    //'type'        => 'hidden',
                    'id'          => 'disc',
                    'name'        => 'disc',
                    'value'       => set_value('disc'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum();',
                    'style'       => 'text-align: right;',
            ), 
           'pph21'=> array(
                    'placeholder' => '(-) PPH 21',
                    //'type'        => 'hidden',
                    'id'          => 'pph21',
                    'name'        => 'pph21',
                    'value'       => set_value('pph21'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum();',
                    'style'       => 'text-align: right;',
            ),
           'alltot'=> array( 
                    'placeholder' => 'Total',
                    //'type'        => 'hidden',
                    'id'          => 'alltot',
                    'name'        => 'alltot',
                    'value'       => set_value('alltot'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ), 
           'banner'=> array( 
                    //'type'        => 'hidden',
                    'id'          => 'banner',
                    'name'        => 'banner',
                    'value'       => set_value('banner'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),

           //detail
           'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => '',
            ),
           'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Nama Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'alamat'=> array(
                    'placeholder' => 'Alamat Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'kota'=> array(
                    'placeholder' => 'Kota',
                    //'type'        => 'hidden',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    //'type'        => 'hidden',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => set_value('warna'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tahun'=> array(
                    'placeholder' => 'Tahun',
                    //'type'        => 'hidden',
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'value'       => set_value('tahun'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'bbn'=> array(
                    'placeholder' => 'Biaya BBN',
                    //'type'        => 'hidden',
                    'id'          => 'bbn',
                    'name'        => 'bbn',
                    'value'       => set_value('bbn'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum1();',
                    'style'       => 'text-align: right;',
                    'required'    => '',
            ),
           'jasa'=> array(
                    'placeholder' => 'Jasa',
                    //'type'        => 'hidden',
                    'id'          => 'jasa',
                    'name'        => 'jasa',
                    'value'       => set_value('jasa'),
                    'class'       => 'form-control',
                    'onkeyup'     => 'sum1();',
                    'style'       => 'text-align: right;',
                    'required'    => '',
            ),
           'tot_by'=> array( 
                    'placeholder' => 'Total Biaya',
                    //'type'        => 'hidden',
                    'id'          => 'tot_by',
                    'name'        => 'tot_by',
                    'value'       => set_value('tot_by'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ), 
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label('Pilih Cabang');
                                      echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                      echo form_error('kddiv','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <?php
                                      echo form_label($form['tglkb']['placeholder']);
                                      echo form_input($form['tglkb']);
                                      echo form_error('tglkb','<div class="note">','</div>');
                                  ?>
                               </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-md-12 harga" name="harga">

                      <div class="col-md-12 col-lg-12">
                          <div class="row">

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_label($form['aktiva']['placeholder']);
                                          echo form_input($form['aktiva']);
                                          echo form_error('aktiva','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                      </div>

                      <div class="col-md-12 col-lg-12">
                          <div class="row">

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_label($form['pasiva']['placeholder']);
                                          echo form_input($form['pasiva']);
                                          echo form_error('pasiva','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                      </div>

                      <div class="col-md-12 col-lg-12">
                          <div class="row">

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_label($form['lr']['placeholder']);
                                          echo form_input($form['lr']);
                                          echo form_error('lr','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                      </div>

                  </div>

                  <div class="col-md-12 table" name="table">

                      <div class="table-responsive">
                          <table class="table table-bordered dataTable">
                          <thead>
                              <tr>
                                  <th style="text-align: center;">Jumlah</th>
                                  <th style="text-align: center;">Keterangan</th>
                              </tr>
                          </thead>
                          <tbody></tbody>
                      </table>
                      </div>

                  </div>

                  <div class="col-md-12">
                      <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                  </div>

                  <div class="col-md-6 col-lg-6">
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <button type="button" class="btn btn-primary btn-proses">Proses</button>
                              </div>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
      $(".harga").hide();
      $(".table").hide();
      clear();
      autoNum();
      tglkb();



      $('#kddiv').change(function () {
          tglkb();
          // setNilai();
          // setStat();
      });

      $('#so_fisik').keyup(function(){
         status();
      });

      $('#so_kasbon').keyup(function(){
         status();
      });

      $('.btn-proses').click(function(){
         proses();
      });
    });

    function blmpost(){

            var column = [];

            column.push({ "aDataSort": [ 0,1 ], "aTargets": [ 0 ] });


            table = $('.dataTable').DataTable({
                "aoColumnDefs": column,
                "columns": [
                    { "data": "jml" },
                    { "data": "ket" }
                ],
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bAutoWidth": false,
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                    aoData.push(
                                    { "name": "kddiv", "value": $("#kddiv").val() },
                                    { "name": "tglkb", "value": $("#tglkb").val() }
                                );
                    $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                },
                'rowCallback': function(row, data, index){
                    //if(data[23]){
                        //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                    //}
                },
                "sAjaxSource": "<?=site_url('closegl/json_dgview');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                //dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                ],
                "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            });

            $('.dataTable').tooltip({
                selector: "[data-toggle=tooltip]",
                container: "body"
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                    //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                        that
                            .search( this.value )
                            .draw();
                    //}
                } );
        });
    }

    function status(){
      so_fisik = $("#so_fisik").autoNumeric('get');
      so_kasbon = $("#so_kasbon").autoNumeric('get');
      //  tot = $("#total").val('');
      so_akhir =   parseFloat(so_fisik) + parseFloat(so_kasbon);
        if (!isNaN(so_akhir)){
            $('#so_akhir').autoNumeric('set',so_akhir);
            sel_kb = parseFloat(so_fisik) - parseFloat(so_akhir);
            if (!isNaN(sel_kb)){
              $('#sel_kb').autoNumeric('set',sel_kb);
            }
        }
    }

    function autoNum() {
      $('#aktiva').autoNumeric();
      $('#pasiva').autoNumeric();
      $('#lr').autoNumeric();
    }

    function clear() {
      $('#aktiva').val('');
      $('#pasiva').val('');
      $('#lr').val('');
    }

    function setAktiva() {
      var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("closegl/setAktiva");?>",
            data: {"kddiv":kddiv },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  $('#aktiva').autoNumeric('set',data.nilai);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function setPasiva() {
      var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("closegl/setPasiva");?>",
            data: {"kddiv":kddiv },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  $('#pasiva').autoNumeric('set',data.nilai);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function setLR() {
      var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("closegl/setLR");?>",
            data: {"kddiv":kddiv },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  $('#lr').autoNumeric('set',data.nilai);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function tglkb(){
      // alert($("#kdkb").val());
      var kddiv = $("#kddiv").val();
      // alert(kdkb);
      // if (kddiv == ''){
      //   $("#tglkb").val('');
      //   $(".btn-proses").attr('disabled',true);
      //   $(".harga").hide();
      // } else {
      //   $(".btn-proses").attr('disabled',false);
      //   $(".harga").show();
          $.ajax({
              type: "POST",
              url: "<?=site_url("closegl/tglkb");?>",
              data: {"kddiv":kddiv },
              success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                  if(data.tglkb.substring(5,8)==='13'){
                    $("#tglkb").val(data.tglkb.substring(0,5)+data.tglkb.substring(5,8));
                  } else  {
                    $("#tglkb").val($.datepicker.formatDate('yy-mm', new Date(data.tglkb)));
                  } 
                });
                if (kddiv == ''){
                  $("#tglkb").val('');
                    $(".harga").hide();
                    $(".table").hide();
                } else {
                  cekPost();
                }
              },
              error:function(event, textStatus, errorThrown) {
                  swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
              }
          });
      // }
    }

    function cekPost(){
      // alert($("#kdkb").val());
      var kddiv = $("#kddiv").val();
      var tglkb = $("#tglkb").val();
      // alert(tglkb);
        $.ajax({
            type: "POST",
            url: "<?=site_url("closegl/cekpost");?>",
            data: {"kddiv":kddiv,
                   "tglkb":tglkb},
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                  if(data.jml>0){
                    // alert("lebih dari 0");
                    $(".harga").hide();
                    $(".table").show();
                    $(".btn-proses").attr('disabled',true);
                    blmpost();
                  } else {
                    // alert("jml = 0");
                    $(".harga").show();
                    $(".table").hide();
                    $(".btn-proses").attr('disabled',false);
                    setAktiva();
                    setPasiva();
                    setLR();
                  }
              });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function proses(){
        aktiva =  $('#aktiva').autoNumeric('get');
        pasiva =  $('#pasiva').autoNumeric('get');
        lr =  $('#lr').autoNumeric('get');
        if (aktiva = pasiva){
          swal({
              title: "Konfirmasi Tutup Akuntansi Keuangan Bengkel!",
              text: "Data yang akan ditutup !",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#c9302c",
              confirmButtonText: "Ya, Lanjutkan!",
              cancelButtonText: "Batalkan!",
              closeOnConfirm: false
          }, function () {
              var kddiv = $("#kddiv").val();
              $.ajax({
                  type: "POST",
                  url: "<?=site_url("closegl/process");?>",
                  data: {"kddiv":kddiv},
                  success: function(resp){
                      var obj = JSON.parse(resp); 
                        if(obj.skbbn_kons_ins=1){
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                                window.location.href = '<?=site_url('closegl');?>';
                            }); 
                        } 
                      clear();
                      setNilai();
                      tglkb(); 
                  },
                  error:function(event, textStatus, errorThrown) {
                      swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  }
              });
          });
        } else {
          swal({
            title: "Nilai Tidak Balance",
            text: "Nilai aktiva tidak sama dengan nilai pasiva!",
            type: "error",
          })
        }

    }
</script>

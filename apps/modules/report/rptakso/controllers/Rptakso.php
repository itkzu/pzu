<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptakso
 *
 * @author adi
 */
class Rptakso extends MY_Controller {

	protected $data = '';
	protected $val = '';

	public function __construct() {
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			'submit' => site_url('rptakso/submit'),
			'add' => site_url('rptakso/add'),
			'edit' => site_url('rptakso/edit'),
			'reload' => site_url('rptakso'),
		);
		$this->load->model('rptakso_qry');

		//$this->load->database('bengkel', true);
	}

	public function index() {
		$this->_init_add();
		$this->template
				->title($this->data['msg_main'], $this->apps->name)
				->set_layout('main')
				->build('index', $this->data);
	}

	public function json_dgview() {
		echo $this->rptakso_qry->json_dgview();
	}

	public function json_dgview_2() {
		echo $this->rptakso_qry->json_dgview_2();
	}

	public function submit() {
	    if($this->validate() == TRUE){
	        $res = $this->rptakso_qry->submit();
	        var_dump($res);
	    }else{
	        $this->_init_add();
	        $this->template
	            ->title($this->data['msg_main'],$this->apps->name)
	            ->set_layout('main')
	            ->build('index',$this->data);
	      }
	}

	private function _init_add(){
	    $this->data['form'] = array(
					'periode'=> array(
									 'placeholder' => 'Periode',
									 'id'          => 'periode',
									 'name'        => 'periode',
									 'value'       => date('Y-m'),
									 'class'       => 'form-control',
									 'style'       => 'margin-left: 5px;',
									 'required'    => '',
					),
				 'periode_awal'=> array(
									'placeholder' => 'Periode Awal',
									'id'          => 'periode_awal',
									'name'        => 'periode_awal',
									'value'       => date('d-m-Y'),
									'class'       => 'form-control',
					),
				 'periode_akhir'=> array(
									'placeholder' => 'Periode Akhir',
									'id'          => 'periode_akhir',
									'name'        => 'periode_akhir',
									'value'       => date('d-m-Y'),
									'class'       => 'form-control',
					),
	    );
	}

	private function validate() {
	    $config = array(
	        array(
	              'field' => 'periode_awal',
	              'label' => 'Periode Awal',
	              'rules' => 'required|max_length[20]',
	        ),
	        array(
	              'field' => 'periode_akhir',
	              'label' => 'Periode Akhir',
	              'rules' => 'required|max_length[20]',
	              ),
	        );

			$this->form_validation->set_rules($config);
	    if ($this->form_validation->run() == FALSE)
	       	{
	            return false;
	        }else{
	            return true;
	        }
	    }
	}

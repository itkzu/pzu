<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Tkbbkl
 *
 * @author
 */
class Tkbbkl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('tkbbkl/submit'),
            'add' => site_url('tkbbkl/add'),
            'edit' => site_url('tkbbkl/edit'),
            'reload' => site_url('tkbbkl'),
        );
        $this->load->model('tkbbkl_qry');
        $rek = $this->tkbbkl_qry->getDataRek();
        foreach ($rek as $value) {
            $this->data['kdkb'][$value['kdkb']] = $value['nmkb'];
        }
        $jnstrx = $this->tkbbkl_qry->getDataTrx();
        foreach ($jnstrx as $value) {
            $this->data['dk'][$value['dk']] = $value['jnstrx'];
        }
        // $refkb = $this->tkbbkl_qry->getDataRef();
        // foreach ($refkb as $value) {
        //     $this->data['kdrefkb'][$value['kdrefkb']] = $value['nmrefkb'];
        // }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function submit() {
        // $nokb = $this->uri->segment(3);
        // $this->data['data'] = $this->tkbbkl_qry->cetak($nokb);
        // $this->template
        //     ->title($this->data['msg_main'],$this->apps->name)
        //     ->set_layout('print-layout')
        //     ->build('html',$this->data);
        // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
        // $this->template
        //     ->title($this->data['msg_main'],$this->apps->name)
        //     ->set_layout('print-layout')
        //     ->build('html',$this->sum);
        //

        $nokb = $this->uri->segment(3);

        $this->load->library('dompdf_lib');

        $this->dompdf_lib->filename = "Kas/Bank Bengkel.pdf";
        $customPaper = array(0,0,360,360);

        $this->data['data'] = $this->tkbbkl_qry->cetak($nokb);
        // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
        //
        // $this->dompdf_lib->load_view('sample_view', $this->sum );
        $this->dompdf_lib->load_view('html', $this->data );
    }

    public function json_dgview() {
        echo $this->tkbbkl_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->tkbbkl_qry->json_dgview_detail();
    }

    public function addDetail() {
        echo $this->tkbbkl_qry->addDetail();
    }

    public function getKdRef() {
        echo $this->tkbbkl_qry->getKdRef();
    }

    public function detaildeleted() {
        echo $this->tkbbkl_qry->detaildeleted();
    }

    public function delete() {
        echo $this->tkbbkl_qry->delete();
    }

    public function save() {
        echo $this->tkbbkl_qry->save();
    }

    public function tglkb() {
        echo $this->tkbbkl_qry->tglkb();
    }

    private function _init_add(){

        $this->data['form'] = array(
           'nokb'=> array(
                    'placeholder' => 'No. Transaksi',
                    //'type'        => 'hidden',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglkb'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => ''
            ),
           'kdkb'=> array(
                   'attr'        => array(
                       'id'    => 'kdkb',
                       'class' => 'form-control  select',
                   ),
                   'data'     =>  $this->data['kdkb'],
                   'value'    => set_value('kdkb'),
                   'name'     => 'kdkb',
                   'required'    => '',
                   'placeholder' => 'Rekening',
            ),
           'jnstrx'=> array(
                   'attr'        => array(
                       'id'    => 'jnstrx',
                       'class' => 'form-control  select',
                   ),
                   'data'     =>  $this->data['dk'],
                   'value'    => set_value('jnstrx'),
                   'name'     => 'jnstrx',
                   'required'    => '',
                   'placeholder' => 'Jenis Transaki',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'readonly'    => '',
            ),

            //modal
            'kdrefkb'=> array(
                    'attr'        => array(
                        'id'    => 'kdrefkb',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdrefkb'),
                    'name'     => 'kdrefkb',
                    'required'    => '',
                    'placeholder' => 'Jenis Transaki',
             ),
            'darike'=> array(
                    'placeholder' => 'Dari/Kepada',
                    'id'    => 'darike',
                    'class' => 'form-control',
                    'value'    => set_value('darike'),
                    'name'     => 'darike',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => ''
            ),
            'nofaktur'=> array(
                    'placeholder' => 'No. Faktur/Nota',
                    'id'    => 'nofaktur',
                    'class' => 'form-control',
                    'value'    => set_value('nofaktur'),
                    'name'     => 'nofaktur',
                    'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'    => 'ket',
                    'class' => 'form-control',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => ''
            ),
            'nilai'=> array(
                    'placeholder' => 'Nominal',
                    'id'    => 'nilai',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nilai'),
                    'name'     => 'nilai',
                    'required'    => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nokb = $this->uri->segment(3);
        }
        $this->_check_id($nokb);
        $this->data['form'] = array(
               'nokb'=> array(
                        'placeholder' => 'No. Transaksi',
                        //'type'        => 'hidden',
                        'id'          => 'nokb',
                        'name'        => 'nokb',
                        'value'       => $this->val[0]['nokb'],
                        'class'       => 'form-control',
                        'style'       => '',
                        'readonly'    => '',
                ),
               'tglkb'=> array(
                        'placeholder' => 'Tanggal Transaksi',
                        'id'          => 'tglkb',
                        'name'        => 'tglkb',
                        'value'       => $this->apps->dateConvert($this->val[0]['tglkb']),
                        'class'       => 'form-control',
                        'style'       => '',
                        'readonly'    => ''
                ),
               'kdkb'=> array(
                       'attr'        => array(
                           'id'    => 'kdkb',
                           'class' => 'form-control  select',
                       ),
                       'data'     =>  $this->data['kdkb'],
                       'value'    => $this->val[0]['kdkb'],
                       'name'     => 'kdkb',
                       'required'    => '',
                       'placeholder' => 'Rekening',
                ),
               'jnstrx'=> array(
                       'attr'        => array(
                           'id'    => 'jnstrx',
                           'class' => 'form-control  select',
                       ),
                       'data'     =>  $this->data['dk'],
                       'value'    => $this->val[0]['dk'],
                       'name'     => 'jnstrx',
                       'required'    => '',
                       'placeholder' => 'Jenis Transaki',
                ),
                'nourut'=> array(
                        'id'    => 'nourut',
                        'type'    => 'hidden',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nourut'),
                        'name'     => 'nourut',
                        'readonly'    => '',
                ),

                //modal
                'kdrefkb'=> array(
                        'attr'        => array(
                            'id'    => 'kdrefkb',
                            'class' => 'form-control  select',
                        ),
                        'data'     =>  '',
                        'value'    => set_value('kdrefkb'),
                        'name'     => 'kdrefkb',
                        'required'    => '',
                        'placeholder' => 'Jenis Transaki',
                 ),
                'darike'=> array(
                        'placeholder' => 'Dari/Kepada',
                        'id'    => 'darike',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('darike'),
                        'name'     => 'darike',
                        'required'    => '',
                        'style'       => 'text-transform: uppercase;',
                        'required'    => ''
                ),
                'nofaktur'=> array(
                        'placeholder' => 'No. Faktur/Nota',
                        'id'    => 'nofaktur',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nofaktur'),
                        'name'     => 'nofaktur',
                        'style'       => 'text-transform: uppercase;',
                ),
                'ket'=> array(
                        'placeholder' => 'Keterangan',
                        'id'    => 'ket',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('ket'),
                        'name'     => 'ket',
                        'required'    => '',
                        'style'       => 'text-transform: uppercase;',
                        'required'    => ''
                ),
                'nilai'=> array(
                        'placeholder' => 'Nominal',
                        'id'    => 'nilai',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nilai'),
                        'name'     => 'nilai',
                        'required'    => '',
                ),
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

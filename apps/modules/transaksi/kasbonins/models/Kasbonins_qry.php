<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Kasbonins_qry
 *
 * @author adi
 */
class Kasbonins_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function set_kasbon() {
        $nokasbon   = $this->input->post('nokasbon');
        $query  = $this->db->query("select * from pzu.t_kasbon where nokasbon = '" . $nokasbon . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function nokasbon() { 
        $refkb   = $this->input->post('refkb');
        $query  = $this->db->query("select * from pzu.get_nokasbon('".$refkb."')");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getKB() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        $this->db->order_by('kdkb');
        $this->db->where('faktif',true);
        $this->db->where('kdkb',"KAS01");
        $q = $this->db->get("pzu.kasbank");
        return $q->result_array();
    }

    public function getjnstrans() {
        $this->db->select("kdjkasbon,nmjkasbon");
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->order_by('kdjkasbon'); 
        $q = $this->db->get("pzu.kasbon_jenis");
        return $q->result_array();
    }

    public function proses_dk() {
        $jnstrans = $this->input->post('jnstrans');
        $this->db->select("jnstrx,nourut,case when jnstrx like '%(D)%' THEN 'D' ELSE 'K' END AS kd");
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('jnstrx',$jnstrans);
        $this->db->order_by('nourut');
        $this->db->group_by('jnstrx,nourut');
        $q = $this->db->get("pzu.kasbank_trx");
          if($q->num_rows()>0){
              $res = $q->result_array();
          }else{
              $res = "empty";
          }
          return json_encode($res); 
    }

    public function getKdjenis() {
        $ket = $this->input->post('ket');
        $this->db->select('*');
        $this->db->where('faktif',true);
        $this->db->where('dk',$ket);
        $this->db->order_by('kdrefkb');
        $q = $this->db->get("pzu.refkb_m"); 
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    }

    public function getKdleas() {
      $q = $this->db->query("
     select * from pzu.leasing where faktif = 't'  and kdleasing not in ( select kdleasing from pzu.t_kasbon_sub_tmp where userentry = '".$this->session->userdata("username")."' )");

      // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "empty";
        }
        return json_encode($res);
    }

    public function getjnskredit() {
        $this->db->select('kdrefkb as kdkredit,nmrefkb,dk,faktif');
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->where('dk','K');
        $this->db->order_by('kdrefkb');
        $q = $this->db->get("pzu.refkb_m");
        return $q->result_array();
    }

    public function getleas() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        $this->db->where('faktif',true);
        $this->db->order_by('kdleasing'); 
        $q = $this->db->get("pzu.leasing");
        return $q->result_array();
    }

    public function set_nocetak() {
      $kdkb = $this->input->post('refkb');
      $query = $this->db->query("select max(nocetak),pzu.set_nocetak(max(nocetak)) as nocetak from pzu.t_kb a inner join pzu.t_kb_ar b on a.nokb=b.nokb inner join pzu.kasbank c on a.kdkb=c.kdkb and a.tglkb=c.tglkb where a.kdkb='".$kdkb."' and (b.jenis='PC' or b.jenis='DP') ");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_tbj() {
        $notbj = $this->input->post('notbj'); 
        $query = $this->db->query("select * from pzu.vm_kasbon_bbn where nobbn_trm = '".$notbj."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_tglbbn() {
        $notbj = $this->input->post('notbj'); 
        $query = $this->db->query("select * from pzu.t_bbn_trm where nobbn_trm = '".$notbj."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_jmlctk() {
      $max = $this->input->post('max');
      $query = $this->db->query("select count(nocetak) from pzu.t_kb where nocetak='".$max."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function get_pkons() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(noso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vm_kb_kons');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'alamat' => $value['alamat'],
                    'nama' => $value['nama']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function get_pleas() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(noso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vm_kb_leas');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'alamat' => $value['alamat'],
                    'nama' => $value['nama']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function get_blkkons() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(noso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama) like '".strtolower($q)."' then 1
                                when LOWER(nama) like '".strtolower($q)."%' then 2
                                when LOWER(nama) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama");
        $query = $this->db->get('pzu.vl_ar_kons');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['noso'],
                    'text' => $value['nodo'],
                    'alamat' => $value['alamat'],
                    'nama' => $value['nama']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'alamat' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function jenis() {
      $kdjkasbon = $this->input->post('kdjkasbon');
      $query = $this->db->query("select * from pzu.kasbon_jenis where kdjkasbon = '" . $kdjkasbon . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pkonsnodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vm_kb_kons where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pleasnoso() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from pzu.vm_kb_leas where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pleasnodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vm_kb_leas where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_blkkonsnoso() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from pzu.vl_ar_kons where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_blkkonsnodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vl_ar_kons where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getNoID() { 
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function ctk($no) {
        $this->db->select("*");
        $this->db->where('nobbn_trm',$no);
        $q = $this->db->get("pzu.vb_lr_bbn");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function set_pkons() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_pkons where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_pleas() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_pleas where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_rleas() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_rleas where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_blkkons() {
      $nokb = $this->input->post('nokb');
      $query = $this->db->query("select * from pzu.vm_kb_blkkons where nokb = '" . $nokb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function refkb() {
      $kdkb = $this->input->post('refkb');
      $query = $this->db->query("select * from pzu.kasbank where kdkb = '" . $kdkb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    } 

    public function set_kbm() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        $aColumns = array('no',
                             'nmrefkb',
                             'darike',
                             'nofaktur',
                             'ket',
                             'nilai' );
      $sIndexColumn = "nourut";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             kdrefkb,
                             nmrefkb,
                             darike,
                             nofaktur,
                             ket,
                             nilai,
                             nokb,
                             nourut  from pzu.vm_kb_d 
                        where nokb = '".$nokb."' order by nourut) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        // $sWhere = "";

        // if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        // {
        // $sWhere = " WHERE (";
        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //   $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        // }
        // $sWhere = substr_replace( $sWhere, "", -3 );
        // $sWhere .= ')';
        // }

        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //     if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        //     {
        //         $ix = $i - 1;
        //         if ( $sWhere == "" )
        //         {
        //             $sWhere = " WHERE ";
        //         }
        //         else
        //         {
        //             $sWhere .= " AND ";
        //         }
        //         //
        //         $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
        //         //echo $sWhere;
        //     }
        // }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable 
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable ";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable ";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            // $aRow['detail'] = array();

            // foreach ($detail as $value) {
            //     if($aRow['nopo']==$value['nopo']){
            //         $aRow['detail'][]= $value;
            //     }
            // } 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokasbon']) ){
            if($_GET['nokasbon']){
                $nokasbon = $_GET['nokasbon'];
            }else{
                $nokasbon = '';
            }
        }else{
            $nokasbon = '';
        }
        $aColumns = array('cl',
                             'no',
                             'kdleasing',
                             'ket',
                             'nilai',
                             'nokasbon');
      $sIndexColumn = "nokasbon";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no, '' as cl,
                             kdleasing,
                             ket,
                             nilai,
                             nokasbon from pzu.t_kasbon_sub 
                        where nokasbon = '".$nokasbon."' order by idsub) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //     if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        //     {
        //         $ix = $i - 1;
        //         if ( $sWhere == "" )
        //         {
        //             $sWhere = " WHERE ";
        //         }
        //         else
        //         {
        //             $sWhere .= " AND ";
        //         }
        //         //
        //         $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
        //         //echo $sWhere;
        //     }
        // }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            // $aRow['detail'] = array();

            // foreach ($detail as $value) {
            //     if($aRow['nopo']==$value['nopo']){
            //         $aRow['detail'][]= $value;
            //     }
            // } 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function det_json_dgview() {
        error_reporting(-1); 

        $aColumns = array('nomor','no', 
                             'kdleasing',
                             'ket',
                             'nilai'
        );

        $sIndexColumn = "nomor";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //row_number() over(order by nodo) as no
        $sTable = " (select '1'::varchar as no, 
                             nomor,
                             ket,
                             kdleasing,
                             nilai from pzu.t_kasbon_sub_tmp where userentry = '".$this->session->userdata("username")."' order by nomor) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //     if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        //     {
        //         $ix = $i - 1;
        //         if ( $sWhere == "" )
        //         {
        //             $sWhere = " WHERE ";
        //         }
        //         else
        //         {
        //             $sWhere .= " AND ";
        //         }
        //         //
        //         $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
        //         //echo $sWhere;
        //     }
        // }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail(); 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            // $aRow['detail'] = array();

            // foreach ($detail as $value) {
            //     if($aRow['nopo']==$value['nopo']){
            //         $aRow['detail'][]= $value;
            //     }
            // } 
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function kb_json_dgview() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }
        if( isset($_GET['jnstrans']) ){
            if($_GET['jnstrans']){
                $jnstrans = $_GET['jnstrans'];
            }else{
                $jnstrans = '';
            }
        }else{
            $jnstrans = '';
        }
        $aColumns = array('no',
                             'nourut',
                             'nmrefkb',
                             'darike',
                             'nofaktur',
                             'ket',
                             'nilai');
      $sIndexColumn = "nourut";
        $sLimit = "";
        $dk = '';
        if ($jnstrans==='K/B MASUK - UMUM (D)') {
          $dk = 'D';
        } else {
          $dk = 'K';
        }
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select '' as no,
                             a.nourut,
                             a.kdrefkb,
                             b.nmrefkb,
                             a.darike,
                             a.nofaktur,
                             a.ket,
                             a.nilai from pzu.t_kb_d_tmp a LEFT JOIN pzu.refkb_m b ON a.kdrefkb = b.kdrefkb 
                        where a.nokb = '".$this->session->userdata("username")."'||substring('".$nokb."',6,10) and a.dk = '".$dk."' and a.userentry = '".$this->session->userdata("username")."' order by a.nourut) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function tambah() { 

        try {   
            $kdleasing = $this->input->post('kdleasing');  
            $cat = $this->input->post('cat'); 
            $det_nilai = $this->input->post('det_nilai'); 

            $q = $this->db->query("insert into pzu.t_kasbon_sub_tmp(kdleasing,nilai,ket,userentry) values ('".$kdleasing."',".$det_nilai.",'".$cat."','".$this->session->userdata("username")."')");

                // echo $this->db->last_query();
            if (!$q) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "success";
                $this->state = "1";
            } 
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);  
    } 

    public function submit() {  
        $tglkasbon  = $this->apps->dateConvert($this->input->post('tglkasbon'));
        $nilai      = $this->input->post('nilai');   
        $refkb       = $this->input->post('refkb');
        $penerima       = $this->input->post('penerima');
        $ket        = $this->input->post('ket'); 
        $tglpks     = $this->apps->dateConvert($this->input->post('tglpks'));
        $kdjkasbon  = $this->input->post('kdjkasbon');
        $nobbn_trm  = $this->input->post('notagbbn'); 
        $q = $this->db->query("select * from pzu.kasbon_ins('".$tglkasbon."',".$nilai.",'".$refkb."','".$penerima."','".$ket."','".$tglpks."',".$kdjkasbon.",'".$nobbn_trm."','".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function kbm_del() {  
        $nokb   = $this->input->post('nokb');  
        $nourut   = $this->input->post('nourut');    
        $q = $this->db->query("select * from pzu.tmpkb_del('".$nokb."',".$nourut.",'".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function kbsubmit() {   
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb    = $this->input->post('kdkb');
        $jnstrans    = $this->input->post('jnstrans');
        $dk = $this->input->post('dk');
        $ket  = $this->input->post('ket'); 
        $q = $this->db->query("select * from pzu.kb_ins_w('".$tglkb."','".$kdkb."','".$jnstrans."','".$dk."','".$ket."','".$this->session->userdata("username")."')");
       // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function delDetail() { 

        try {   
            $nomor = $this->input->post('nomor');   

            $q = $this->db->query("delete from pzu.t_kasbon_sub_tmp where nomor = ".$nomor." and userentry = '".$this->session->userdata("username")."'");

                // echo $this->db->last_query();
            if (!$q) {
                $err = $this->db->error();
                $this->res = " Error : " . $this->apps->err_code($err['message']);
                $this->state = "0";
            } else {
                $this->res = "success";
                $this->state = "1";
            } 
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);  
        
        // $nomor         = $this->input->post('nomor'); 
        // $q = $this->db->query("select title,msg,tipe from pzu.tbj_del_d( '". $nobbn_trm ."',
        //                                                                 '". $nomor ."')");
        // // echo $this->db->last_query();
        // if($q->num_rows()>0){
        //     $res = $q->result_array();
        // }else{
        //     $res = "";
        // }

        // return json_encode($res);
    } 

    public function pkons_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $noso           = $this->input->post('noso');
        $nilaibyr       = $this->input->post('nilaibyr');
        $jenis          = $this->input->post('jenis');
        $nocetak        = $this->input->post('nocetak');   
        $q = $this->db->query("select * from pzu.kb_arkons_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $noso ."',
                                                                ". $nilaibyr .",
                                                                '". $jenis ."',
                                                                '". $nocetak ."', 
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function pleas_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $noso           = $this->input->post('noso');
        $nilai_pl       = $this->input->post('nilai_pl');
        $nilai_jp       = $this->input->post('nilai_jp'); 
        $nocetak        = $this->input->post('nocetak');   
        $q = $this->db->query("select * from pzu.kb_arleas_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $noso ."',
                                                                ". $nilai_pl .", 
                                                                ". $nilai_jp .", 
                                                                '". $nocetak ."', 
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function blkkons_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $noso           = $this->input->post('noso');
        $nilaibyr       = $this->input->post('nilaibyr');  
        $q = $this->db->query("select * from pzu.kb_retkons_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $noso ."',
                                                                ". $nilaibyr .",   
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function rleas_submit() {    
        $tglkb          = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb           = $this->input->post('kdkb');
        $jnstrx         = $this->input->post('jnstrx');
        $dk             = $this->input->post('dk');
        $nilai          = $this->input->post('nilai');
        $ket            = $this->input->post('ket');
        $jenis          = $this->input->post('jenis');
        $kdleasing      = $this->input->post('kdleasing');
        $nilaibyr       = $this->input->post('nilaibyr'); 
        $ket_det        = $this->input->post('ket_det');   
        $q = $this->db->query("select * from pzu.kb_leasing_ins( '". $tglkb ."', 
                                                                '". $kdkb ."',
                                                                '". $jnstrx ."',
                                                                '". $dk ."',
                                                                ". $nilai .",
                                                                '". $ket ."',
                                                                '". $jenis ."',
                                                                '". $kdleasing ."', 
                                                                ". $nilaibyr .", 
                                                                '". $ket_det ."', 
                                                                '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function batal() {     
        $q = $this->db->query("select * from pzu.kb_btl('".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

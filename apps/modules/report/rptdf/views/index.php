<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : 
 * Email : 
 * Description :
 * ***************************************************************
 */
?>

<style type="text/css">
	td.details-control {
		background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
	}
</style>

<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
						, 'class' => "form-inline");
					echo form_open($submit,$attributes);
				?>
				<div class="form-group">
					<?php
						echo form_label($form['periode_awal']['placeholder']);
						echo form_input($form['periode_awal']);
						echo form_error('periode_awal','<div class="note">','</div>');
					?>
				</div>
				<div class="form-group">
					<?php
						echo form_label($form['periode_akhir']['placeholder']);
						echo form_input($form['periode_akhir']);
						echo form_error('periode_akhir','<div class="note">','</div>');
					?>
				</div>
				<button type="button" class="btn btn-primary btn-tampil">Tampil</button>

				<?php echo form_close(); ?>
				<hr>
				<div class="table-responsive">
					<table  class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="text-align: center;width: 5px;"></th>
								<th style="text-align: center;width: 80px;">No. Rencana Bayar DF</th>
								<th style="text-align: center;width: 50px;">Tgl Transaksi</th>
								<th style="text-align: center;width: 50px;">Rencana Bayar</th>
								<th style="text-align: center;width: 50px;">Pokok Hutang</th>
								<th style="text-align: center;width: 50px;">Bunga</th>
								<th style="text-align: center;width: 50px;">Total Hutang</th>
								<th style="text-align: center;">Cabang</th>
								<th style="text-align: center;width: 50px;">No. Kas/Bank</th>
								<th style="text-align: center;width: 50px;">Tgl Bayar</th>
								<th style="text-align: center;width: 50px;">Pembayaran Via</th>
							</tr>
						</thead>						
						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".btn-tampil").click(function(){
			table.ajax.reload();
		});

		var column = [];

		column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });

		column.push({
			"aTargets": [ 0 ],
			"searchable": false,
			"orderable": false,
		});

		column.push({
			"aTargets": [ 4,5,6 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
				// return formmatedvalue;
			},
			"sClass" : "right"
		});

		column.push({
			"aTargets": [ 2,3,9 ],
			"mRender": function (data, type, full) {
				return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});

		$('.dataTableDetail').DataTable();
		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{
					"className" : 'details-control',
					"data" : null,
					"defaultContent" : ''
				},
				{ "data": "nodf" },
				{ "data": "tgldf" },
				{ "data": "tglrnb" },
				{ "data": "nap" },
				{ "data": "nint" },
				{ "data": "total" },
				{ "data": "nmdiv" },
				{ "data": "nokb" },
				{ "data": "tglkb" },
				{ "data": "nmkb" }
			],
			//"lengthMenu": [[ -1], [ "Semua Data"]],
			"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
							,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptdf/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},

			/*
			// jumlah TOTAL
			'footerCallback': function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// computing column Total of the complete result
				var nap = api
					.column( 4 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var nint = api
					.column( 5 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var nsel = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				// Update footer by showing the total with the reference of the column index
				$( api.column( 1 ).footer() ).html('Total');
				$( api.column( 2 ).footer() ).html('');
				$( api.column( 3 ).footer() ).html('');
				$( api.column( 4 ).footer() ).html(numeral(nap).format('0,0.00'));
				$( api.column( 5 ).footer() ).html(numeral(nint).format('0,0.00'));
				$( api.column( 6 ).footer() ).html(numeral(nsel).format('0,0.00'));
				$( api.column( 7 ).footer() ).html('');
				$( api.column( 8 ).footer() ).html('');
				$( api.column( 9 ).footer() ).html('');
				$( api.column( 10 ).footer() ).html('');
			},
			*/


			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print', footer: true,
					customize: function (win){
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');
							$(win.document.body).find('table')
									.addClass('compact')
									.css('font-size', 'inherit');
					}
				}
			],

			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		// Add event listener for opening and closing details
		$('.dataTable tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				//format(row.data());
				tr.addClass('shown');
			}
		} );

		
		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable thead th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete"  && title!==""){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
		


		function format ( d ) {
			//console.log(d);
			var tdetail =  '<table class="table table-bordered table-hover dataTableDetail">'+
				'<thead>' +
					'<tr style="background-color: #d73925;color: #fff;">'+
						'<th style="text-align: center;width:16px;"></th>'+
						'<th style="text-align: center;width:10px;">No.</th>'+
						'<th style="text-align: center;">No. Faktur</th>'+
						'<th style="text-align: center;">Tanggal Faktur</th>'+
						'<th style="text-align: center;">Pokok Hutang</th>'+
						'<th style="text-align: center;">Bunga</th>'+
						'<th style="text-align: center;">Total</th>'+
						'<th style="text-align: center;">Cabang</th>'+
					'</tr>'+
				'</thead><tbody>';
			var no = 1;
			var total = 0;
			if(d.detail.length>0){
				$.each(d.detail, function(key, data){
					//console.log(data);
					tdetail+='<tr>'+
						'<td style="text-align: center;"></td>'+
						'<td style="text-align: center;">'+no+'</td>'+
						'<td style="text-align: center;">'+data.nopo+'</td>'+
						'<td style="text-align: center;">'+moment(data.tglpo).format('L')+'</td>'+
						'<td style="text-align: right;">'+numeral(data.nap).format('0,0.00')+'</td>'+
						'<td style="text-align: right;">'+numeral(data.nint).format('0,0.00')+'</td>'+
						'<td style="text-align: right;">'+numeral(data.total).format('0,0.00')+'</td>'+
						'<td style="text-align: center;">'+data.nmdiv+'</td>'+
					'</tr>';
					no++;
				});

			}

			/*
			tdetail +=	'<tfoot>' +
							'<tr>'+
								'<th style="text-align: right;" colspan="8">TOTAL</th>'+
								'<th style="text-align: right;">'+numeral(total).format('0,0.00')+'</th>'+
							'</tr>'+
						'</tfoot>';
			*/

			tdetail += '</tbody></table>';
			return tdetail;

		}
	});


</script>
<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            -->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">

                    <div class="form-import main-form">

                        <div class="col-xs-8">

                            <div class="form-group">
                                <label for="exampleInputFile">Data Pembelian</label>

                                <div class="fileinputBeli fileinput-new input-group" name="fileinputBeli" id="fileinputBeli" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>

                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Cari...</span>
                                        <span class="fileinput-exists">Ubah</span>
                                        <input type="file" name="fileBeli" id="fileBeli" data-original-value="" required accept=".xls, .xlsx">
                                    </span>

                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                    <span class="input-group-btn fileinput-exists">
                                        <button class="btn btn-primary" name="importBeli" id="importBeli"><i class="fa fa-cloud-upload"></i> Import</button>
                                    </span>

                                </div>
                            </div>

                        </div>

                        <div class="col-xs-4">
                            <label>&nbsp;</label>
                            <div><a href="<?=site_url('imptrxbkladmin/download_template_po');?>" name="d_po" id="d_po" target="_blank">
                                <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                    Download Template
                                </button></a>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>




                        <div class="col-xs-8">
                            <div class="form-group">
                                <label for="exampleInputFile">Data Service</label>

                                <div class="fileinputServise fileinput-new input-group" name="fileinputServise" id="fileinputServise" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>

                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Cari...</span>
                                        <span class="fileinput-exists">Ubah</span>
                                        <input type="file" name="fileServis" id="fileServis" data-original-value="" required accept=".xls, .xlsx">
                                    </span>

                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                    <span class="input-group-btn fileinput-exists">
                                        <button class="btn btn-primary" name="importServis" id="importServis"><i class="fa fa-cloud-upload"></i> Import</button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <label>&nbsp;</label>
                            <div>
                                <a href="<?=site_url('imptrxbkladmin/download_template_sv');?>" name="d_sv" id="d_sv" target="_blank">
                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                        Download Template
                                    </button>
                                </a>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>





                        <div class="col-xs-8">
                            <div class="form-group">
                                <label for="exampleInputFile">Data Penjualan</label>

                                <div class="fileinputJual fileinput-new input-group" name="fileinputJual" id="fileinputJual" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>

                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Cari...</span>
                                        <span class="fileinput-exists">Ubah</span>
                                        <input type="file" name="fileJual" id="fileJual" data-original-value="" required accept=".xls, .xlsx">
                                    </span>

                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                    <span class="input-group-btn fileinput-exists">
                                        <button class="btn btn-primary" name="importJual" id="importJual"><i class="fa fa-cloud-upload"></i> Import</button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <label>&nbsp;</label>
                            <div>
                                <a href="<?=site_url('imptrxbkladmin/download_template_so');?>" name="d_so" id="d_so" target="_blank">
                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                        Download Template
                                    </button>
                                </a>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-xs-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                    </div>
                </div>


                <span id="status" class="hidden"></span>
                <div class="form-group" id="process" style="display:none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" id="file-progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->


            <!--
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 00%"></div>
                  </div>
              </div>


            <div class="box-footer">
                <p><b>Syarat & Ketentuan Import data :</b></p>
                <p>- </p>
                <p>- </p>
                <p>- </p>
            </div>
            -->

            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();

        $(".btn-reset").click(function(){
            //disabled();
            window.location.reload();
        });

        $("#fileinputKB").click(function(){
            $(".upload_kb").show();
        });





        //import data Pembelian
        $('#importBeli').click(function(event){
            $("#load").show();
            event.preventDefault();
            var formData = new FormData();
            formData.append('file', $('input[type=file]')[0].files[0]);
            formData.append('kddiv',  $("#kddiv").val());

            $.ajax({
                url:"<?php echo base_url(); ?>imptrxbkladmin/importBeli",
                method:"POST",
                data:formData,
                dataType:'json',
                contentType:false,
                cache:false,
                processData:false,
                success:function(resp){
                    // alert(resp);
                    // console.log(resp);
                    var obj = resp;
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputBeli').fileinput('clear');
                                    $('#importBeli').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputBeli').fileinput('clear');
                                    $('#importBeli').attr('disabled', false);
                                });

                            }
                        });
                }
            })
        });




        //import data Servis
        $('#importServis').click(function(event){
            $("#load").show();
            event.preventDefault();

            var formData = new FormData();
            formData.append('file', $('input[type=file]')[1].files[0]);
            formData.append('kddiv',  $("#kddiv").val());
            //formData.append('file', $('.fileinputON').files[0]);

            $.ajax({
                url:"<?php echo base_url(); ?>imptrxbkladmin/importServis",
                method:"POST",
                //data:new FormData(this),
                data:formData,
                contentType:false,
                cache:false,
                processData:false,
                success:function(resp){
                    // if($("#status").html('ok')){
                        var obj = JSON.parse(resp);
                        $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputServise').fileinput('clear');
                                    $('#importServis').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputServise').fileinput('clear');
                                    $('#importServis').attr('disabled', false);
                                });

                            }
                        });
                    // }
                }
            })
        });




        //import data Penjualan
        $('#importJual').click(function(e){
            $("#load").show();
            e.preventDefault();

            var formData = new FormData();
            formData.append('file', $('input[type=file]')[2].files[0]);
            formData.append('kddiv',  $("#kddiv").val());
            //formData.append('file', $('#fileinputOFF').files[0]);
            $.ajax({
                url:"<?php echo base_url(); ?>imptrxbkladmin/importJual",
                method:"POST",
                //data:new FormData(this),
                data:formData,
                contentType:false,
                cache:false,
                processData:false,
                success:function(resp){

                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputJual').fileinput('clear');
                                    $('#importJual').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputJual').fileinput('clear');
                                    $('#importJual').attr('disabled', false);
                                });

                            }
                        }); 
                }
            })
        });


        function progress_bar_process(percentage, timer){

            $('.progress-bar').css('width', percentage + '%');
            if(percentage > 100){
                clearInterval(timer);
                $('#process').css('display', 'none');
                $('.progress-bar').css('width', '0%');

                $('#importBeli').attr('disabled', false);
                $('#importServis').attr('disabled', false);
                $('#importJual').attr('disabled', false);
                $('#importKB').attr('disabled', false);

                setTimeout(function(){
                    }, 5000);
                $('#status').html("ok");
            }
        }

        function disabled(){
            $(".upload_kb").hide();
            $("#kddiv").attr("disabled",false);
            $(".btn-set").show();
            $(".btn-reset").hide();
            $('.form-import').hide();
            //$("#fileKB").remove();
            //$("#fileBeli").remove();
            //$("#fileJual").remove();
            //$("#fileServis").remove();
        }

        function enabled(){
            $("#kddiv").attr("disabled",true);
            $(".btn-set").hide();
            $(".btn-reset").show();
            $('.form-import').show();
        }


    });



    // function getkb(){
    //
    //     var kddiv = $("#kddiv").val();
    //
    //
    //     var select = document.getElementById("kb");
    //     var length = select.options.length;
    //     for (i = length-1; i >= 0; i--) {
    //         select.options[i] = null;
    //     }
    //
    //     var select = document.getElementById("kb");
    //     var options = [];
    //     var option = document.createElement('option');
    //
    //     $.ajax({
    //      type: "POST",
    //          url: "<?=site_url("imptrxbkladmin/getkb");?>",
    //          data: {"kddiv":kddiv},
    //          beforeSend: function() {
    //      },
    //          success: function(resp){
    //              var obj = jQuery.parseJSON(resp);
    //              $.each(obj, function(key, value){
    //                 //$('#dk').attr("disabled",false);
    //
    //                 option.value = value.kdkb;
    //                 option.text = value.nmkb;
    //                 options.push(option.outerHTML);
    //             });
    //
    //             select.insertAdjacentHTML('beforeEnd', options.join('\n'));
    //
    //         },
    //         error:function(event, textStatus, errorThrown) {
    //             swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //         }
    //     });
    // }

</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style>  
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-xs-12 box-header box-view">
              <div class="col-xs-3">
                <div class="row">
                    <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a> 
                </div>
              </div>
              <div class="col-xs-2">
                <div class="row"> 
                </div>
              </div>
              <div class="col-xs-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak Nota</button>
                </div>
              </div>
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-md-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['nokb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-7">  
                                      <?php
                                          echo form_input($form['nokb']);
                                          echo form_error('nokb','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['refkb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-8"> 
                                      <?php
                                        echo form_dropdown($form['refkb']['name'],
                                                          $form['refkb']['data'],
                                                          $form['refkb']['value'],
                                                          $form['refkb']['attr']);
                                        echo form_error('refkb','<div class="note">','</div>');
                                      ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3"> 
                                      <?php
                                          echo form_input($form['day']);
                                          echo form_error('day','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-md-4"> 
                                      <?php
                                          echo form_input($form['periode']);
                                          echo form_error('periode','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['periode']['placeholder']); ?>
                                  </div>
                              </div> 

                          </div>
                        </div>   

                    </div>
                  </div> 

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">  
                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['jnstrans']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-8">
                                      <?php
                                        echo form_dropdown($form['jnstrans']['name'],
                                                          $form['jnstrans']['data'],
                                                          $form['jnstrans']['value'],
                                                          $form['jnstrans']['attr']);
                                        echo form_error('jnstrans','<div class="note">','</div>');
                                      ?>
                              </div>

                          </div>
                        </div> 

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <!-- <div class="col-xs-4">
                                  <div class="form-group">
                                      <button type="button" class="btn btn-warning btn-set"> SET </button>
                                  </div>
                              </div>  -->

                          </div>
                        </div> 

                    </div>
                  </div> 

                  <!--PEMBAYARAN KONSUMEN-->
                  <div class="pkons">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>PEMBAYARAN KONSUMEN (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_noso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['byrkons_noso']);
                                            echo form_error('byrkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nodo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['byrkons_nodo']);
                                            echo form_error('byrkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nmsales']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_nmsales']);
                                            echo form_error('byrkons_nmsales','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_tglso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['byrkons_tglso']);
                                            echo form_error('byrkons_tglso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_tgldo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['byrkons_tgldo']);
                                            echo form_error('byrkons_tgldo','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nmspv']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_nmspv']);
                                            echo form_error('byrkons_nmspv','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12"> 
                      <br>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nama']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['byrkons_nama']);
                                            echo form_error('byrkons_nama','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nosin']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['byrkons_nosin']);
                                            echo form_error('byrkons_nosin','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['byrkons_nora']);
                                            echo form_error('byrkons_nora','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_alamat']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['byrkons_alamat']);
                                            echo form_error('byrkons_alamat','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_kdtipe']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['byrkons_kdtipe']);
                                            echo form_error('byrkons_kdtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['byrkons_nmtipe']);
                                            echo form_error('byrkons_nmtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['byrkons_kdleas']);
                                            echo form_error('byrkons_kdleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_warna']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-6"> 
                                        <?php
                                            echo form_input($form['byrkons_warna']);
                                            echo form_error('byrkons_warna','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['byrkons_tahun']);
                                            echo form_error('byrkons_tahun','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_kdprogleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['byrkons_kdprogleas']);
                                            echo form_error('byrkons_kdprogleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_status']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['byrkons_status']);
                                            echo form_error('byrkons_status','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12"> 
                      <br>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_hjnet']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_hjnet']);
                                            echo form_error('byrkons_hjnet','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_um']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_um']);
                                            echo form_error('byrkons_um','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div>

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_pelunasan']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_pelunasan']);
                                            echo form_error('byrkons_pelunasan','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_byr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_byr1']);
                                            echo form_error('byrkons_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div>

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_byr2']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_byr2']);
                                            echo form_error('byrkons_byr2','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nbyr']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_nbyr']);
                                            echo form_error('byrkons_nbyr','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_saldoum']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_saldoum']);
                                            echo form_error('byrkons_saldoum','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div>

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_saldo_pelunasan']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['byrkons_saldo_pelunasan']);
                                            echo form_error('byrkons_saldo_pelunasan','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                      <br>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-6">
                            <label>No. Cetak</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pkons_lanjut'),'true') . '<b>&nbsp; Lanjutkan </b>');
                              echo '</div>'; 
                          ?>
                        </div>
                        <div class="col-md-3"> 
                          <?php  
                              echo form_input($form['byrkons_nocetak']);
                              echo form_error('byrkons_nocetak','<div class="note">','</div>');
                          ?>
                        </div>
                        <div class="col-md-2"> 
                          <?php  
                              echo form_label($form['byrkons_jmltrans']['placeholder']); 
                          ?>
                        </div>
                        <div class="col-md-1"> 
                          <?php  
                              echo form_input($form['byrkons_jmltrans']);
                              echo form_error('byrkons_jmltrans','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pkons_baru'),'true') . '<b>&nbsp; No. Baru </b>');
                              echo '</div>'; 
                          ?>
                        </div> 
                      </div>
                    </div>  
                  </div> 

                  <!--PEMBAYARAN LEASING-->
                  <div class="pleas">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>PEMBAYARAN LEASING (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_noso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['pleas_noso']);
                                            echo form_error('pleas_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nodo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['pleas_nodo']);
                                            echo form_error('pleas_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nmsales']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_nmsales']);
                                            echo form_error('pleas_nmsales','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_tglso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['pleas_tglso']);
                                            echo form_error('pleas_tglso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_tgldo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['pleas_tgldo']);
                                            echo form_error('pleas_tgldo','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nmspv']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_nmspv']);
                                            echo form_error('pleas_nmspv','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12"> 
                      <br>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nama']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['pleas_nama']);
                                            echo form_error('pleas_nama','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nosin']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['pleas_nosin']);
                                            echo form_error('pleas_nosin','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['pleas_nora']);
                                            echo form_error('pleas_nora','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_alamat']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['pleas_alamat']);
                                            echo form_error('pleas_alamat','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_kdtipe']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['pleas_kdtipe']);
                                            echo form_error('pleas_kdtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['pleas_nmtipe']);
                                            echo form_error('pleas_nmtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['pleas_kdleas']);
                                            echo form_error('pleas_kdleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_warna']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-6"> 
                                        <?php
                                            echo form_input($form['pleas_warna']);
                                            echo form_error('pleas_warna','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['pleas_tahun']);
                                            echo form_error('pleas_tahun','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_kdprogleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['pleas_kdprogleas']);
                                            echo form_error('pleas_kdprogleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_status']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['pleas_status']);
                                            echo form_error('pleas_status','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12"> 
                      <br>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_pelunasan']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_pelunasan']);
                                            echo form_error('pleas_pelunasan','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_jp']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_jp']);
                                            echo form_error('pleas_jp','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_byr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_byr1']);
                                            echo form_error('pleas_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_byr2']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_byr2']);
                                            echo form_error('pleas_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nbyr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_nbyr1']);
                                            echo form_error('pleas_nbyr1','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nbyr2']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['pleas_nbyr2']);
                                            echo form_error('pleas_nbyr2','<div class="note">','</div>');
                                        ?> 
                                </div> 
                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                      <br>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-6">
                            <label>No. Cetak</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pleas_lanjut'),'true') . '<b>&nbsp; Lanjutkan </b>');
                              echo '</div>'; 
                          ?>
                        </div>
                        <div class="col-md-3"> 
                          <?php  
                              echo form_input($form['pleas_nocetak']);
                              echo form_error('pleas_nocetak','<div class="note">','</div>');
                          ?>
                        </div>
                        <div class="col-md-2"> 
                          <?php  
                              echo form_label($form['pleas_jmltrans']['placeholder']); 
                          ?>
                        </div>
                        <div class="col-md-1"> 
                          <?php  
                              echo form_input($form['pleas_jmltrans']);
                              echo form_error('pleas_jmltrans','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pleas_baru'),'true') . '<b>&nbsp; No. Baru </b>');
                              echo '</div>'; 
                          ?>
                        </div> 
                      </div>
                    </div>  
                  </div> 

                  <!--REFUND LEASING-->
                  <div class="rleas">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>REFUND LEASING (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                          echo form_dropdown($form['rleas_kdleas']['name'],
                                                            $form['rleas_kdleas']['data'],
                                                            $form['rleas_kdleas']['value'],
                                                            $form['rleas_kdleas']['attr']);
                                          echo form_error('rleas_kdleas','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_jenis']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-6"> 
                                        <?php
                                          echo form_dropdown($form['rleas_jenis']['name'],
                                                            $form['rleas_jenis']['data'],
                                                            $form['rleas_jenis']['value'],
                                                            $form['rleas_jenis']['attr']);
                                          echo form_error('rleas_jenis','<div class="note">','</div>');  
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>  
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_nilai']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['rleas_nilai']);
                                            echo form_error('rleas_nilai','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_ket']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-6"> 
                                        <?php
                                            echo form_input($form['rleas_ket']);
                                            echo form_error('rleas_ket','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>   
                  </div> 

                  <!--PENCAIRAN SUBSIDI-->
                  <div class="psub">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>PENCAIRAN SUBSIDI (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                          echo form_dropdown($form['psub_kdleas']['name'],
                                                            $form['psub_kdleas']['data'],
                                                            $form['psub_kdleas']['value'],
                                                            $form['psub_kdleas']['attr']);
                                          echo form_error('psub_kdleas','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                                <div class="col-md-3">  
                                </div>  

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_ncair']['placeholder']); ?>
                                    </div>
                                </div>

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['psub_ncair']);
                                          echo form_error('psub_ncair','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_selisih']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['psub_selisih']);
                                          echo form_error('psub_selisih','<div class="note">','</div>'); 
                                        ?> 
                                </div>   

                                <div class="col-md-6">  
                                </div>   

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_ket']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-6"> 
                                        <?php
                                            echo form_input($form['psub_ket']);
                                          echo form_error('psub_ket','<div class="note">','</div>');   
                                        ?> 
                                </div>  

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_totsub']['placeholder']); ?>
                                    </div>
                                </div>

                            </div>
                          </div>  

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row">  

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['psub_totsub']);
                                          echo form_error('psub_totsub','<div class="note">','</div>');   
                                        ?> 
                                </div>  

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_selsbg']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                          echo form_dropdown($form['psub_selsbg']['name'],
                                                            $form['psub_selsbg']['data'],
                                                            $form['psub_selsbg']['value'],
                                                            $form['psub_selsbg']['attr']);
                                          echo form_error('psub_selsbg','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div> 

                    <div class="col-md-12">
                    <!-- <p><a id="add" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-del"><i class="fa fa-minus"></i> Hapus</a></p> -->
                    <p id="alldata" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered tbl_subkasbon display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Kasbon</th> 
                            <th style="text-align: center;">Tgl Kasbon</th> 
                            <th style="text-align: center;">Keterangan Kasbon</th> 
                            <th style="text-align: center;">Catatan</th> 
                            <th style="text-align: center;">Nilai Subsidi</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div> 

                  <!--KAS/BANK MASUK UMUM-->
                  <div class="kbm_umum">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>K/B MASUK - UMUM (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-md-12"> 
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered tbl_kbm display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Jenis Penerimaan</th> 
                            <th style="text-align: center;">Diterima Dari</th> 
                            <th style="text-align: center;">No. Faktur/Nota</th> 
                            <th style="text-align: center;">Keterangan</th> 
                            <th style="text-align: center;">Nominal</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div> 

                  <!--PENGEMBALIAN KONSUMEN-->
                  <div class="balik_kons">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>PENGEMBALIAN KONSUMEN (K)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_noso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['blkkons_noso']);
                                            echo form_error('blkkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nodo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['blkkons_nodo']);
                                            echo form_error('blkkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nmsales']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_nmsales']);
                                            echo form_error('blkkons_nmsales','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_tglso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['blkkons_tglso']);
                                            echo form_error('blkkons_tglso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_tgldo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['blkkons_tgldo']);
                                            echo form_error('blkkons_tgldo','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nmspv']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_nmspv']);
                                            echo form_error('blkkons_nmspv','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12"> 
                      <br>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nama']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['blkkons_nama']);
                                            echo form_error('blkkons_nama','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nosin']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['blkkons_nosin']);
                                            echo form_error('blkkons_nosin','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['blkkons_nora']);
                                            echo form_error('blkkons_nora','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_alamat']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['blkkons_alamat']);
                                            echo form_error('blkkons_alamat','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_kdtipe']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['blkkons_kdtipe']);
                                            echo form_error('blkkons_kdtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-5"> 
                                        <?php
                                            echo form_input($form['blkkons_nmtipe']);
                                            echo form_error('blkkons_nmtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['blkkons_kdleas']);
                                            echo form_error('blkkons_kdleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_warna']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-6"> 
                                        <?php
                                            echo form_input($form['blkkons_warna']);
                                            echo form_error('blkkons_warna','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-md-3"> 
                                        <?php
                                            echo form_input($form['blkkons_tahun']);
                                            echo form_error('blkkons_tahun','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_kdprogleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-9"> 
                                        <?php
                                            echo form_input($form['blkkons_kdprogleas']);
                                            echo form_error('blkkons_kdprogleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_status']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-4"> 
                                        <?php
                                            echo form_input($form['blkkons_status']);
                                            echo form_error('blkkons_status','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12"> 
                      <br>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_hjnet']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_hjnet']);
                                            echo form_error('blkkons_hjnet','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_pl']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_pl']);
                                            echo form_error('blkkons_pl','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_byr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_byr1']);
                                            echo form_error('blkkons_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_pengembalian']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_pengembalian']);
                                            echo form_error('blkkons_pengembalian','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_lbhbyr']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-md-7"> 
                                        <?php
                                            echo form_input($form['blkkons_lbhbyr']);
                                            echo form_error('blkkons_lbhbyr','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div> 
                  
                    <div class="col-md-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 "> 
                              <div class="form-group">
                                  <?php echo form_label('<small><i> * Nilai Pengembalian tidak boleh melebihi Nilai Terbayar</i></small>'); ?>
                              </div> 

                            </div>
                          </div>  

                      </div>
                    </div>  
                  </div> 

                  <!--KAS/BANK KELUAR UMUM-->
                  <div class="kbk_umum">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>K/B KELUAR - UMUM (K)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-md-12"> 
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered tbl_kbk display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Jenis Penerimaan</th> 
                            <th style="text-align: center;">Diserahkan Kepada</th> 
                            <th style="text-align: center;">No. Faktur/Nota</th> 
                            <th style="text-align: center;">Keterangan</th> 
                            <th style="text-align: center;">Nominal</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div> 

                  <!--REALISASI KASBON-->
                  <div class="rkasbon">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-md-12">
                            <label>REALISASI KASBON (K)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link " id="list-tfa" name="list-tfa" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Transaksi</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="input-tfa" name="input-tfa" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Kasbon</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-tfa">

                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered  tbl_tkasbon display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">Jenis Pengeluaran</th>
                                                    <th style="text-align: center;">Diserahkan Kepada</th>
                                                    <th style="text-align: center;">No. Faktur/Nota</th> 
                                                    <th style="text-align: center;">Keterangan</th> 
                                                    <th style="text-align: center;">Nominal</th> 
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-tfa"> 

                                <!-- <div class="col-md-12">
                                    <br>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_nokasbon']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <?php
                                                echo form_input($form['rkasbon_nokasbon']);
                                                echo form_error('rkasbon_nokasbon','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_nama']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <?php
                                                echo form_input($form['rkasbon_nama']);
                                                echo form_error('rkasbon_nama','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div>   

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_tglkasbon']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <?php
                                                echo form_input($form['rkasbon_tglkasbon']);
                                                echo form_error('rkasbon_tglkasbon','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_ket']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <?php
                                                echo form_input($form['rkasbon_ket']);
                                                echo form_error('rkasbon_ket','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_kb']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <?php
                                                echo form_input($form['rkasbon_kb']);
                                                echo form_error('rkasbon_kb','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_nkasbon']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <?php
                                                echo form_input($form['rkasbon_nkasbon']);
                                                echo form_error('rkasbon_nkasbon','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div>   -->

                                <div class="col-md-12">
                                    <div class="row"> 
                                        <br>
                                    </div>
                                </div>   

                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered  tbl_kasbon display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">No. Kasbon</th>
                                                    <th style="text-align: center;">Tgl Kasbon</th>
                                                    <th style="text-align: center;">Nilai Kasbon</th> 
                                                    <th style="text-align: center;">Penerima</th> 
                                                    <th style="text-align: center;">Keterangan</th> 
                                                    <th style="text-align: center;">Kas/Bank</th> 
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                  </div>

                </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        reset(); 

        $('#nokb').mask('*****-****-****'); 
        disabled(); 
        autoNum(); 
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.psub').hide();
        $('.kbm_umum').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();

        

        $('.btn-ctk').click(function(){
            var nokb = $('#nokb').val().toUpperCase();
            var tglkb = $('#periode').val();
            // alert(tglkb);
            // window.open('kbins/kbins/html/'+nokb, '_blank');
            // window.open('kbins/kbins/pdf/'+nokb, '_blank');
            window.open('kbins/kbins/pdf/'+nokb+'/'+tglkb, '_blank');
        });

        //nocetak
          $('#pkons_lanjut').click(function(){  pkons_nocetak(); });
          $('#pkons_baru').click(function(){    pkons_nocetak(); });
          $('#pleas_lanjut').click(function(){  pleas_nocetak(); });
          $('#pleas_baru').click(function(){    pleas_nocetak(); });
          $('.btn-set').click(function(){       settrx(); });

        //nokb
          $('#nokb').keyup(function(){
            var jml = $('#nokb').val(); 
            var nokb = jml.replace("-","");
            var nokb = nokb.replace("-","");
            var nokb = nokb.replace("-","");
            var nokb = nokb.replace("_",""); 
            var n = nokb.length;
                // alert(nokb);    
            
            if(n===13){
                // alert(n);    
                set_kb();    
                // day();
            } else {
                c_pkons();
                c_pleas();
                c_rleas();
                c_psub();
                c_blkkons();
                c_rkasbon();
                $('.btn-ctk').attr('disabled',true);
            }
          });
    });

    function day(tgl){
      var n = tgl.getDay();
      // var n = new Date(tgl);
      // alert(n);
      if(n>=7){
        var nd = 'MINGGU';
      } else if(n>=6){
        var nd = 'SABTU';
      } else if(n>=5){
        var nd = 'JUMAT';
      } else if(n>=4){
        var nd = 'KAMIS';
      } else if(n>=3){
        var nd = 'RABU';
      } else if(n>=2){
        var nd = 'SELASA';
      } else if(n>=1){
        var nd = 'SENIN';
      }
      $('#day').val(nd);
    }

    function settrx(){
      // alert
      var nokb = $('#nokb').val();
      if ($('#jnstrans').val()==='PEMBAYARAN KONSUMEN (D)') {
        $('.pkons').show();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_pkons();
        }
      } else if ($('#jnstrans').val()==='PEMBAYARAN LEASING (D)') {
        $('.pkons').hide();
        $('.pleas').show();
        $('.rleas').hide();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_pleas();
        }
      } else if ($('#jnstrans').val()==='REFUND LEASING (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').show();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_rleas();
        }
      } else if ($('#jnstrans').val()==='PENGEMBALIAN KONSUMEN (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.balik_kons').show();
        $('.psub').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_blkkons();
        }
      } else if ($('#jnstrans').val()==='REALISASI KASBON (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.psub').hide();
        $('.rkasbon').show();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_rkasbon();
        }
      } else if ($('#jnstrans').val()==='K/B KELUAR - UMUM (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.kbk_umum').show();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.psub').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_kaskeluar(nokb);
        }
      } else if ($('#jnstrans').val()==='K/B MASUK - UMUM (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.kbk_umum').hide();
        $('.balik_kons').hide();
        $('.psub').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').show();
        if($('.kbm_umum').show()){
          // alert('1');
          set_kasmasuk(nokb);
        }
      } else if ($('#jnstrans').val()==='PENCAIRAN SUBSIDI (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.psub').show();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_psub();
        }
      } 
    }

    function pkons_nocetak(){
      var check = $('#pkons_lanjut').is(':checked');
      if(check){ 
        $("#byrkons_nocetak").prop("disabled", false);
        $("#byrkons_jmltrans").prop("disabled", false); 
      }else{ 
        $("#byrkons_nocetak").prop("disabled", true);
        $("#byrkons_jmltrans").prop("disabled", true); 

      }
    }

    function pkons_nocetak(){
      var check = $('#pleas_lanjut').is(':checked');
      if(check){ 
        $("#pleas_nocetak").prop("disabled", false);
        $("#pleas_jmltrans").prop("disabled", false); 
      }else{ 
        $("#pleas_nocetak").prop("disabled", true);
        $("#pleas_jmltrans").prop("disabled", true); 

      }
    }

    function disabled(){  
      $("#refkb").prop('disabled',true); 
      $("#jnstrans").prop('disabled',true);  
      $(".btn-add").attr('disabled',false); 
      $(".btn-ctk").attr('disabled',true);
      // $(".btn-batal").hide();

      $("#byrkons_nocetak").prop("disabled", true);
      $("#byrkons_jmltrans").prop("disabled", true); 
      $("#pkons_lanjut").prop("disabled", true);
      $("#pkons_baru").prop("disabled", true); 
      $("#pleas_lanjut").prop("disabled", true);
      $("#pleas_baru").prop("disabled", true); 
      $("#rleas_kdleas").prop("disabled", true); 
      $("#rleas_jenis").prop("disabled", true); 
    }

    function enabled(){  
      $("#tgltbj").attr('disabled',true); 
      $("#disc").attr('disabled',true); 
      $("#pph21").attr('disabled',true); 
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',false);
      $(".btn-del").attr('disabled',false);
      $(".btn-ctk").attr('disabled',false); 
    }

    function autoNum(){   
      //detail pembayaran konsumen
          //nominal pembayaran konsumen
          $('#byrkons_hjnet').autoNumeric('init',{ currencySymbol : 'Rp. '}); 
          $('#byrkons_um').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_pelunasan').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_byr1').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_byr2').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_nbyr').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_saldoum').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_saldo_pelunasan').autoNumeric('init',{ currencySymbol : 'Rp. '});   

      //detail pembayaran leasing  
          //nominal pembayaran leasing
          $('#pleas_pelunasan').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_jp').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_byr1').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_byr2').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_nbyr1').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_nbyr2').autoNumeric('init',{ currencySymbol : 'Rp. '});     

      //detail refund leasing 
          
          $('#rleas_nilai').autoNumeric('init',{ currencySymbol : 'Rp. '});      

      //detail pencairan subsidi
            
          $('#psub_selisih').autoNumeric('init',{ currencySymbol : 'Rp. '}); 
          $('#psub_totsub').autoNumeric('init',{ currencySymbol : 'Rp. '});    
          
          //more detail pencairan subsidi  
          $('#psub_nsubkasbon').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#psub_ncairkasbon').autoNumeric('init',{ currencySymbol : 'Rp. '});      

      //detail pengembalian konsumen 
          //nominal pembayaran konsumen
          $('#blkkons_hjnet').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_pl').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_byr1').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_pengembalian').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_lbhbyr').autoNumeric('init',{ currencySymbol : 'Rp. '});    

      //detail realisasi kasbon 
          
          $('#rkasbon_nkasbon').autoNumeric('init',{ currencySymbol : 'Rp.'});    
    } 

    function c_pkons(){ 
      //detail pembayaran konsumen
          $('#byrkons_noso').val('');  
          $('#byrkons_nodo').val('');  
          $('#byrkons_tglso').val('');  
          $('#byrkons_tgldo').val('');  
          $('#byrkons_nmsales').val('');  
          $('#byrkons_nmspv').val('');  
          $('#byrkons_nama').val('');  
          $('#byrkons_alamat').val('');  
          $('#byrkons_kdleas').val('');  
          $('#byrkons_kdprogleas').val('');  
          $('#byrkons_nosin').val('');  
          $('#byrkons_nora').val('');  
          $('#byrkons_kdtipe').val('');  
          $('#byrkons_nmtipe').val('');  
          $('#byrkons_warna').val('');  
          $('#byrkons_tahun').val('');  
          $('#byrkons_status').val('');  

          //nominal pembayaran konsumen
          $('#byrkons_hjnet').autoNumeric('set',0);  
          $('#byrkons_um').autoNumeric('set',0);  
          $('#byrkons_pelunasan').autoNumeric('set',0);  
          $('#byrkons_byr1').autoNumeric('set',0);  
          $('#byrkons_byr2').autoNumeric('set',0);  
          $('#byrkons_nbyr').autoNumeric('set',0);  
          $('#byrkons_saldoum').autoNumeric('set',0);  
          $('#byrkons_saldo_pelunasan').autoNumeric('set',0);  
          $('#byrkons_nocetak').val('');  
          $('#byrkons_jmltrans').val('');  
    }

    function c_pleas(){ 
      //detail pembayaran leasing
          $('#pleas_noso').val('');   
          $('#pleas_nodo').val('');   
          $('#pleas_tglso').val('');   
          $('#pleas_tgldo').val('');   
          $('#pleas_nmsales').val('');   
          $('#pleas_nmspv').val('');   
          $('#pleas_nama').val('');   
          $('#pleas_alamat').val('');   
          $('#pleas_kdleas').val('');   
          $('#pleas_kdprogleas').val('');   
          $('#pleas_nosin').val('');   
          $('#pleas_nora').val('');   
          $('#pleas_kdtipe').val('');   
          $('#pleas_nmtipe').val('');   
          $('#pleas_warna').val('');   
          $('#pleas_tahun').val('');   
          $('#pleas_status').val('');    

          //nominal pembayaran leasing
          $('#pleas_pelunasan').autoNumeric('set',0);     
          $('#pleas_jp').autoNumeric('set',0);     
          $('#pleas_byr1').autoNumeric('set',0);  
          $('#pleas_byr2').autoNumeric('set',0);  
          $('#pleas_nbyr1').autoNumeric('set',0);  
          $('#pleas_nbyr2').autoNumeric('set',0);  
          $('#pleas_nocetak').val('');    
          $('#pleas_jmltrans').val('');  
    }

    function c_rleas(){ 
      //detail refund leasing
          $('#rleas_kdleas').val('');   
          $('#rleas_jenis').val('');   
          $('#rleas_nilai').val('');   
          $('#rleas_ket').val('');    
    }

    function c_psub(){
      //detail pencairan subsidi
          $('#psub_kdleas').val('');   
          $('#psub_selisih').autoNumeric('set',0);   
          $('#psub_ket').val('');   
          $('#psub_totsub').autoNumeric('set',0);   
          $('#psub_selsbg').val('');   
          
          //more detail pencairan subsidi
          $('#psub_nokasbon').val('');    
          $('#psub_tglkasbon').val('');    
          $('#psub_ketkasbon').val('');    
          $('#psub_catkasbon').val('');    
          $('#psub_nsubkasbon').autoNumeric('set',0);    
          $('#psub_ncairkasbon').autoNumeric('set',0);   
    }

    function c_blkkons(){
      //detail pengembalian konsumen
          $('#blkkons_noso').val('');     
          $('#blkkons_nodo').val('');     
          $('#blkkons_tglso').val('');     
          $('#blkkons_tgldo').val('');     
          $('#blkkons_nmsales').val('');     
          $('#blkkons_nmspv').val('');     
          $('#blkkons_nama').val('');     
          $('#blkkons_alamat').val('');     
          $('#blkkons_kdleas').val('');     
          $('#blkkons_kdprogleas').val('');     
          $('#blkkons_nosin').val('');     
          $('#blkkons_nora').val('');     
          $('#blkkons_kdtipe').val('');     
          $('#blkkons_nmtipe').val('');     
          $('#blkkons_warna').val('');     
          $('#blkkons_tahun').val('');     
          $('#blkkons_status').val('');    

          //nominal pembayaran konsumen
          $('#blkkons_hjnet').autoNumeric('set',0);
          $('#blkkons_pl').autoNumeric('set',0);
          $('#blkkons_byr1').autoNumeric('set',0);  
          $('#blkkons_pengembalian').autoNumeric('set',0);
          $('#blkkons_lbhbyr').autoNumeric('set',0);    
    }

    function c_rkasbon(){
      //detail realisasi kasbon
          $('#rkasbon_nokasbon').val('');  
          $('#rkasbon_tglkasbon').val('');  
          $('#rkasbon_kb').val('');  
          $('#rkasbon_nama').val('');  
          $('#rkasbon_ket').val('');  
          $('#rkasbon_nkasbon').autoNumeric('set',0); 
    }

    function reset(){   

      $('#nokb').val('');
      $('#refkb').val('');
      $('#jnstrans').val(''); 
      $('#periode').val('');  
      $('#day').val('');  
      // $('#periode').val($.datepicker.formatDate('dd-mm-yy', new Date()));  

      //detail pembayaran konsumen
          $('#byrkons_noso').val('');  
          $('#byrkons_nodo').val('');  
          $('#byrkons_tglso').val('');  
          $('#byrkons_tgldo').val('');  
          $('#byrkons_nmsales').val('');  
          $('#byrkons_nmspv').val('');  
          $('#byrkons_nama').val('');  
          $('#byrkons_alamat').val('');  
          $('#byrkons_kdleas').val('');  
          $('#byrkons_kdprogleas').val('');  
          $('#byrkons_nosin').val('');  
          $('#byrkons_nora').val('');  
          $('#byrkons_kdtipe').val('');  
          $('#byrkons_nmtipe').val('');  
          $('#byrkons_warna').val('');  
          $('#byrkons_tahun').val('');  
          $('#byrkons_status').val('');  

          //nominal pembayaran konsumen
          $('#byrkons_hjnet').val('');  
          $('#byrkons_um').val('');  
          $('#byrkons_pelunasan').val('');  
          $('#byrkons_byr1').val('');  
          $('#byrkons_byr2').val('');  
          $('#byrkons_nbyr').val('');  
          $('#byrkons_saldoum').val('');  
          $('#byrkons_saldo_pelunasan').val('');  
          $('#byrkons_nocetak').val('');  
          $('#byrkons_jmltrans').val('');   

      //detail pembayaran leasing
          $('#pleas_noso').val('');   
          $('#pleas_nodo').val('');   
          $('#pleas_tglso').val('');   
          $('#pleas_tgldo').val('');   
          $('#pleas_nmsales').val('');   
          $('#pleas_nmspv').val('');   
          $('#pleas_nama').val('');   
          $('#pleas_alamat').val('');   
          $('#pleas_kdleas').val('');   
          $('#pleas_kdprogleas').val('');   
          $('#pleas_nosin').val('');   
          $('#pleas_nora').val('');   
          $('#pleas_kdtipe').val('');   
          $('#pleas_nmtipe').val('');   
          $('#pleas_warna').val('');   
          $('#pleas_tahun').val('');   
          $('#pleas_status').val('');    

          //nominal pembayaran leasing
          $('#pleas_pelunasan').val('');    
          $('#pleas_jp').val('');    
          $('#pleas_byr1').val('');    
          $('#pleas_byr2').val('');    
          $('#pleas_nbyr1').val('');    
          $('#pleas_nbyr2').val('');    
          $('#pleas_nocetak').val('');    
          $('#pleas_jmltrans').val('');  

      //detail refund leasing
          $('#rleas_kdleas').val('');   
          $('#rleas_jenis').val('');   
          $('#rleas_nilai').val('');   
          $('#rleas_ket').val('');    

      //detail pencairan subsidi
          
          $('#psub_kdleas').val('');   
          $('#psub_selisih').val('');   
          $('#psub_ket').val('');   
          $('#psub_totsub').val('');   
          $('#psub_selsbg').val('');   


          
          //more detail pencairan subsidi
          $('#psub_nokasbon').val('');    
          $('#psub_tglkasbon').val('');    
          $('#psub_ketkasbon').val('');    
          $('#psub_catkasbon').val('');    
          $('#psub_nsubkasbon').val('');    
          $('#psub_ncairkasbon').val('');     

      //detail pengembalian konsumen
          $('#blkkons_noso').val('');     
          $('#blkkons_nodo').val('');     
          $('#blkkons_tglso').val('');     
          $('#blkkons_tgldo').val('');     
          $('#blkkons_nmsales').val('');     
          $('#blkkons_nmspv').val('');     
          $('#blkkons_nama').val('');     
          $('#blkkons_alamat').val('');     
          $('#blkkons_kdleas').val('');     
          $('#blkkons_kdprogleas').val('');     
          $('#blkkons_nosin').val('');     
          $('#blkkons_nora').val('');     
          $('#blkkons_kdtipe').val('');     
          $('#blkkons_nmtipe').val('');     
          $('#blkkons_warna').val('');     
          $('#blkkons_tahun').val('');     
          $('#blkkons_status').val('');    

          //nominal pembayaran konsumen
          $('#blkkons_hjnet').val('');  
          $('#blkkons_pl').val('');  
          $('#blkkons_byr1').val('');  
          $('#blkkons_pengembalian').val('');  
          $('#blkkons_lbhbyr').val('');   

      //detail realisasi kasbon
          $('#rkasbon_nokasbon').val('');  
          $('#rkasbon_tglkasbon').val('');  
          $('#rkasbon_kb').val('');  
          $('#rkasbon_nama').val('');  
          $('#rkasbon_ket').val('');  
          $('#rkasbon_nkasbon').val('');   
    } 

    function set_kb(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_kb");?>",
            data: {"nokb":nokb },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
                  $(".btn-add").attr('disabled',false); 
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                    $("#refkb").val(data.kdkb); 
                    $("#jnstrans").val(data.jnstrx); 
                    $("#periode").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb)));  
                    var tgl = new Date(data.tglkb);
                    day(tgl);  
                    settrx();
                    $(".btn-ctk").attr('disabled',false);
                    // if(data.jnstrx === 'PEMBAYARAN KONSUMEN (D)'){
                    //   set_pkons();
                    // } else if(data.jnstrx === 'PEMBAYARAN LEASING (D)'){
                    //   set_pleas();
                    // }  else if(data.jnstrx === 'REFUND LEASING (D)'){
                    //   set_rleas();
                    // }  else if(data.jnstrx === 'K/B MASUK - UMUM (D)'){
                    //   set_kasmasuk();
                    // }  else if(data.jnstrx === 'PENGEMBALIAN KONSUMEN (K)'){
                    //   set_blkkons();
                    // }  else if(data.jnstrx === 'REALISASI KASBON (K)'){
                    //   set_rkasbon();
                    // }  else if(data.jnstrx === 'K/B KELUAR - UMUM (K)'){
                    //   set_kaskeluar();
                    // } 
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pkons(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pkons");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#byrkons_noso").val(data.noso); 
                    $("#byrkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                    $("#byrkons_nodo").val(data.nodo); 
                    $("#byrkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                    $("#byrkons_nmsales").val(data.nmsales); 
                    $("#byrkons_nmspv").val(data.nmsales_header); 
                    $("#byrkons_nama").val(data.nama); 
                    $("#byrkons_alamat").val(data.alamat); 
                    $("#byrkons_kdleas").val(data.jnsbayar); 
                    $("#byrkons_kdprogleas").val(data.nmprogleas); 
                    $("#byrkons_nosin").val(data.nosin); 
                    $("#byrkons_nora").val(data.nora); 
                    $("#byrkons_kdtipe").val(data.kdtipe); 
                    $("#byrkons_nmtipe").val(data.nmtipe); 
                    $("#byrkons_warna").val(data.warna); 
                    $("#byrkons_tahun").val(data.tahun); 
                    $("#byrkons_status").val(data.status_otr); 

                    $("#byrkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                    $("#byrkons_um").autoNumeric('set',data.ar_um); 
                    $("#byrkons_pelunasan").autoNumeric('set',data.ar_pl); 
                    $("#byrkons_byr1").autoNumeric('set',data.byr_um); 
                    $("#byrkons_byr2").autoNumeric('set',data.byr_pl); 
                    $("#byrkons_nbyr").autoNumeric('set',data.nilai_byr); 
                    $("#byrkons_saldoum").autoNumeric('set',data.saldo_um); 
                    $("#byrkons_saldo_pelunasan").autoNumeric('set',data.saldo_pl); 

                    $('#pkons_lanjut').prop("checked",true);
                    $("#byrkons_nocetak").val(data.nocetak); 
                    $("#byrkons_jmltrans").val(data.jmltrans);  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pleas(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pleas");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#pleas_noso").val(data.noso); 
                    $("#pleas_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                    $("#pleas_nodo").val(data.nodo); 
                    $("#pleas_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                    $("#pleas_nmsales").val(data.nmsales); 
                    $("#pleas_nmspv").val(data.nmsales_header); 
                    $("#pleas_nama").val(data.nama); 
                    $("#pleas_alamat").val(data.alamat); 
                    $("#pleas_kdleas").val(data.jnsbayar); 
                    $("#pleas_kdprogleas").val(data.nmprogleas); 
                    $("#pleas_nosin").val(data.nosin); 
                    $("#pleas_nora").val(data.nora); 
                    $("#pleas_kdtipe").val(data.kdtipe); 
                    $("#pleas_nmtipe").val(data.nmtipe); 
                    $("#pleas_warna").val(data.warna); 
                    $("#pleas_tahun").val(data.tahun); 
                    $("#pleas_status").val(data.status_otr); 

                    $("#pleas_pelunasan").autoNumeric('set',data.pl); 
                    $("#pleas_jp").autoNumeric('set',data.jp); 
                    $("#pleas_byr1").autoNumeric('set',data.pl_bayar); 
                    $("#pleas_byr2").autoNumeric('set',data.jp_bayar); 
                    $("#pleas_nbyr1").autoNumeric('set',data.nbyr_pl); 
                    $("#pleas_nbyr2").autoNumeric('set',data.nbyr_jp);  

                    $('#pleas_lanjut').prop("checked",true);
                    $("#pleas_nocetak").val(data.nocetak); 
                    $("#pleas_jmltrans").val(data.jmltrans);  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_blkkons(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_blkkons");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#blkkons_noso").val(data.noso); 
                    $("#blkkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                    $("#blkkons_nodo").val(data.nodo); 
                    $("#blkkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                    $("#blkkons_nmsales").val(data.nmsales); 
                    $("#blkkons_nmspv").val(data.nmsales_header); 
                    $("#blkkons_nama").val(data.nama); 
                    $("#blkkons_alamat").val(data.alamat); 
                    $("#blkkons_kdleas").val(data.jnsbayar); 
                    $("#blkkons_kdprogleas").val(data.nmprogleas); 
                    $("#blkkons_nosin").val(data.nosin); 
                    $("#blkkons_nora").val(data.nora); 
                    $("#blkkons_kdtipe").val(data.kdtipe); 
                    $("#blkkons_nmtipe").val(data.nmtipe); 
                    $("#blkkons_warna").val(data.warna); 
                    $("#blkkons_tahun").val(data.tahun); 
                    $("#blkkons_status").val(data.status_otr); 

                    $("#blkkons_hjnet").autoNumeric('set',data.harga_jual_net);  
                    $("#blkkons_pl").autoNumeric('set',data.ar_pl); 
                    $("#blkkons_byr1").autoNumeric('set',data.byr_pl); 
                    $("#blkkons_pengembalian").autoNumeric('set',Math.abs(data.saldo_pl)); 
                    $("#blkkons_lbhbyr").autoNumeric('set',data.saldo_pl);   

                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_rleas(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_rleas");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#rleas_kdleas").val(data.kdleasing); 
                    $("#rleas_jenis").val(data.jenis); 
                    $("#rleas_nilai").autoNumeric('set',data.nilai); 
                    $("#rleas_ket").val(data.ket);  

                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_kasmasuk(nokb){  
      var nokb = nokb.toUpperCase();
      tabel_kbm = $('.tbl_kbm').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [ {
                    "aTargets": [ 5 ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"
                    
                }],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nmrefkb" },
                { "data": "darike" },
                { "data": "nofaktur"},
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nokb", "value": nokb  }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('kbins/set_kbm');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.tbl_kbm').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        tabel_kbm.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        tabel_kbm.on( 'draw.dt', function () {
            var PageInfo = $('.tbl_kbm').DataTable().page.info();
            tabel_kbm.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }

    function set_kaskeluar(nokb){  
      var nokb = nokb.toUpperCase();
      tabel_kbk = $('.tbl_kbk').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [ {
                    "aTargets": [ 5 ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"
                    
                }],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nmrefkb" },
                { "data": "darike" },
                { "data": "nofaktur"},
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nokb", "value": nokb  }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('kbins/set_kbm');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.tbl_kbk').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        tabel_kbk.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        tabel_kbk.on( 'draw.dt', function () {
            var PageInfo = $('.tbl_kbk').DataTable().page.info();
            tabel_kbk.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }

    function sum(){
        var total = $('#total').autoNumeric('get'); 
        var disc  = $('#disc').autoNumeric('get'); 
        var pph21 = $('#pph21').autoNumeric('get'); 

        var alltot = parseInt(total) - (parseInt(disc) + parseInt(pph21));
        if(!isNaN(alltot)){
            $('#alltot').autoNumeric('set',alltot); 
            var terbilang = penyebut(alltot);
            if(alltot==='0'){
              $('#banner').val(terbilang);  
            } else {
              $('#banner').val(terbilang+" RUPIAH");  
            }
            
        }
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 
</script>

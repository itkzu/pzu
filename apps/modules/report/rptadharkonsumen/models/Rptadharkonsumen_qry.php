<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptadharkonsumen_qry
 *
 * @author adi
 */
class Rptadharkonsumen_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }    
    public function get_data() {
        $periode_akhir = $this->input->post('periode_akhir');
        $str = "SELECT * FROM (select * from pzu.pl_ar_kons('".$this->apps->dateConvert($periode_akhir)."')) A";
        //echo $str;
        $q = $this->db->query($str);
        if($q->num_rows()>0){
            $arr = $q->result_array();
            $leas = array();
            foreach ($arr as $dts) {
                $leas[$dts['jnsbayarx']] = $dts['jnsbayarx'];
            }
            $dt_arr = array();
            foreach ($leas as $v) {
                $no = 1;
                foreach ($arr as $data) {
                    if($v==$data['jnsbayarx']){
                        $dt = $data + array('no'=>$no);
                        $dt_arr[] = $dt;
                        $no++;
                    }
                }
            }
            $res = array(
                'konsumen' => $leas,
                'data' => $dt_arr,
            );
        }else{
            $res = null;
        }
        return json_encode($res);
    }

}

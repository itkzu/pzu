<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Adjarbkl
 *
 * @author adi
 */
class Adjarbkl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('adjarbkl/submit'),
            'add' => site_url('adjarbkl/add'),
            'edit' => site_url('adjarbkl/edit'),
            'reload' => site_url('adjarbkl'),
        );
        $this->load->model('adjarbkl_qry');

        $this->data['jenis'] = array(
            "PC" => "PEMBAYARAN KONSUMEN"
        );
    }
        

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function set_nosv() {
        echo $this->adjarbkl_qry->set_nosv();
    }

    public function set_harga() {
        echo $this->adjarbkl_qry->set_harga();
    }

    public function set_nodo() {
        echo $this->adjarbkl_qry->set_nodo();
    }

    public function set_noaradj() {
        echo $this->adjarbkl_qry->set_noaradj();
    }

    public function getjenis() {
        echo $this->adjarbkl_qry->getjenis();
    }

    public function simpan() {
        echo $this->adjarbkl_qry->simpan();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'noaradjbkl'=> array(
                    'placeholder' => 'No. Adjustment',
                    //'type'        => 'hidden',
                    'id'          => 'noaradjbkl',
                    'name'        => 'noaradjbkl',
                    'value'       => set_value('noaradjbkl'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglaradjbkl'=> array(
                    'placeholder' => 'Tanggal Adjustment',
                    'id'          => 'tglaradjbkl',
                    'name'        => 'tglaradjbkl',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nosv'=> array(
                    'placeholder' => 'No. Nota Service',
                    //'type'        => 'hidden',
                    'id'          => 'nosv',
                    'name'        => 'nosv',
                    'value'       => set_value('nosv'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglsv'=> array(
                    'placeholder' => 'Tanggal Nota Service',
                    'id'          => 'tglsv',
                    'name'        => 'tglsv',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ), 
           'nama'=> array(
                    'placeholder' => 'Nama Pemesan',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'nama_s'=> array(
                    'placeholder' => 'Nama Pemilik',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'alamat'=> array(
                     'placeholder' => 'Alamat STNK',
                     'id'          => 'alamat',
                     'name'        => 'alamat',
                     'value'       => set_value('alamat'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'jnsbayar'=> array(
                    'placeholder' => 'Tunai/Kredit',
                    'value'       => set_value('jnsbayar'),
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'jenis'=> array(
                    'attr'        => array(
                        'id'    => 'jenis',
                        'class' => 'form-control',
                    ),
                    'data'     =>  $this->data['jenis'],
                    'value'    => set_value('jenis'),
                    'name'     => 'jenis',
                    'placeholder' => 'Jenis AR',
                    'required' => ''
            ), 
            'nilai_ar'=> array(
                    'placeholder' => 'Total AR',
                    'value'       => set_value('nilai_ar'),
                    'id'          => 'nilai_ar',
                    'name'        => 'nilai_ar',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
            'nilai_byr'=> array(
                    'placeholder' => 'Total Pembayaran',
                    'value'       => set_value('nilai_byr'),
                    'id'          => 'nilai_byr',
                    'name'        => 'nilai_byr',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
            'nilai_adj'=> array(
                    'placeholder' => 'Nilai Penyesuaian',
                    'value'       => set_value('nilai_adj'),
                    'id'          => 'nilai_adj',
                    'name'        => 'nilai_adj',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-align: right;',
                    // 'readonly'    => '',
            ),
            'saldo'=> array(
                    'placeholder' => 'Saldo AR',
                    'value'       => set_value('saldo'),
                    'id'          => 'saldo',
                    'name'        => 'saldo',
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'status'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('status'),
                    'id'          => 'status',
                    'name'        => 'status',
                    'class'       => 'form-control',
                    'style'       => '',
                    'type'        => 'hidden',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $noaksi = $this->uri->segment(3);
        }
        $this->_check_id($noaksi);


        $this->data['form'] = array(
           'nobklaradj'=> array(
                    'placeholder' => 'No. Adjustment',
                    //'type'        => 'hidden',
                    'id'          => 'nobklaradj',
                    'name'        => 'nobklaradj',
                    'value'       => $this->val[0]['nobklaradj'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglbklaradj'=> array(
                    'placeholder' => 'Tanggal Adjustment',
                    'id'          => 'tglbklaradj',
                    'name'        => 'tglbklaradj',
                    'value'       => $this->val[0]['tglbklaradj'],
                    'class'       => 'form-control calendar',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
           'nosv'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'nosv',
                    'name'        => 'nosv',
                    'value'       => $this->val[0]['nosv'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'tglsv'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglsv',
                    'name'        => 'tglsv',
                    'value'       => $this->val[0]['tglsv'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Nama Pemesan',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nama_s'=> array(
                    'placeholder' => 'Nama. STNK',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'alamat'=> array(
                     'placeholder' => 'Alamat STNK',
                     'id'          => 'alamat',
                     'name'        => 'alamat',
                     'value'       => set_value('alamat'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'jnsbayar'=> array(
                    'placeholder' => 'Tunai/Kredit',
                    'value'       => set_value('jnsbayar'),
                    'id'          => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'kdjns'=> array( 
                    'value'       => set_value('kdjns'),
                    'id'          => 'kdjns',
                    'name'        => 'kdjns',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'jenis'=> array(
                    'attr'        => array(
                        'id'    => 'jenis',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('jenis'),
                    'name'     => 'jenis',
                    'placeholder' => 'Jenis AR',
                    'required' => ''
            ), 
            'nilai_ar'=> array(
                    'placeholder' => 'Total AR',
                    'value'       => set_value('nilai_ar'),
                    'id'          => 'nilai_ar',
                    'name'        => 'nilai_ar',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
            'nilai_byr'=> array(
                    'placeholder' => 'Total Pembayaran',
                    'value'       => set_value('nilai_byr'),
                    'id'          => 'nilai_byr',
                    'name'        => 'nilai_byr',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
            'nilai_adj'=> array(
                    'placeholder' => 'Nilai Penyesuaian',
                    'value'       => set_value('nilai_adj'),
                    'id'          => 'nilai_adj',
                    'name'        => 'nilai_adj',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-align: right;',
                    // 'readonly'    => '',
            ),
            'saldo'=> array(
                    'placeholder' => 'Saldo AR',
                    'value'       => set_value('saldo'),
                    'id'          => 'saldo',
                    'name'        => 'saldo',
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'status'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('status'),
                    'id'          => 'status',
                    'name'        => 'status',
                    'class'       => 'form-control',
                    'style'       => '',
                    'type'        => 'hidden',
            ),


        // $this->data['form'] = array(
        //    'noaksi'=> array(
        //             'placeholder' => 'No. Transaksi',
        //             //'type'        => 'hidden',
        //             'id'          => 'noaksi',
        //             'name'        => 'noaksi',
        //             'value'       => ,
        //             'class'       => 'form-control',
        //             'style'       => '',
        //             'readonly'    => '',
        //     ),
        //    'tglaksi'=> array(
        //             'placeholder' => 'Tanggal Transaksi',
        //             'id'          => 'tglaksi',
        //             'name'        => 'tglaksi',
        //             'value'       => $this->apps->dateConvert($this->val[0]['tglaksi']),
        //             'class'       => 'form-control calendar',
        //             'style'       => '',
        //     ),
        //     'kdsup'=> array(
        //             'attr'        => array(
        //                 'id'    => 'kdsup',
        //                 'class' => 'form-control  select',
        //             ),
        //             'data'     =>  $this->data['kdsup'],
        //             'value'    => $this->val[0]['kdsup'],
        //             'name'     => 'kdsup',
        //             'required'    => '',
        //             'placeholder' => 'Supplier',
        //     ),
        //    'nmsup'=> array(
        //             'placeholder' => 'Supplier',
        //             'id'          => 'nmsup',
        //             'name'        => 'nmsup',
        //     //        'value'       => $this->val[0]['nmsup'],
        //             'class'       => 'form-control',
        //             'required'    => '',
        //     //        'style'       => 'text-transform: uppercase;',
        //     ),
        //     'ket'=> array(
        //             'placeholder' => 'Keterangan',
        //             'id'      => 'ket',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => $this->val[0]['ket'],
        //             'name'     => 'ket',
        //     //        'required'    => '',
        //     //        'onkeyup'     => 'myFunction()',
        //     //        'style'       => 'text-transform: uppercase;',
        //     ),
        //        'stathrg'=> array(
        //         'placeholder' => 'Harga Sudah diketahui',
        //                 'id'          => 'stathrg',
        //                 'value'       => $this->val[0]['ffinal'],
        //                 'checked'     => '',
        //                 'class'       => 'filled-in',
        //                 'name'        => 'faktif'
        //         ),
        //     'nourut'=> array(
        //             'id'    => 'nourut',
        //             'type'    => 'hidden',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => set_value('nourut'),
        //             'name'     => 'nourut',
        //             'required'    => ''
        //     ),
        //     'kdaks'=> array(
        //             'attr'        => array(
        //                 'id'    => 'kdaks',
        //                 'class' => 'form-control',
        //             ),
        //             'data'     =>  '',
        //             'value'    => set_value('kdaks'),
        //             'name'     => 'kdaks',
        //             'placeholder' => 'Cari Barang',
        //             'required' => ''
        //     ),
        //     'nmaks'=> array(
        //             'attr'        => array(
        //                 'id'    => 'nmaks',
        //                 'class' => 'form-control',
        //             ),
        //             'data'     => '',
        //             'class' => 'form-control',
        //             'value'    => set_value('nmaks'),
        //             'name'     => 'nmaks',
        //             'readonly' => '',
        //     ),
        //     'qty'=> array(
        //             'placeholder' => 'Quantity',
        //             'id'    => 'qty',
        //             'class' => 'form-control',
        //             'value'    => set_value('qty'),
        //             'name'     => 'qty',
        //             'required'    => '',
        //     ),
        //     'harga'=> array(
        //             'id'    => 'harga',
        //             'class' => 'form-control',
        //             'value'    => set_value('harga'),
        //             'name'     => 'harga',
        //     ),
        //     'total'=> array(
        //             'placeholder' => 'Total',
        //             'id'    => 'total',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => set_value('total'),
        //             'name'     => 'total',
        //             'readonly' => ''
        //     ),
        //     'diskon'=> array(
        //       //      'placeholder' => 'Diskon <small>dalam rupiah </small>(-)',
        //             'id'    => 'diskon',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => $this->val[0]['disc'],
        //             'name'     => 'diskon',
        //             'onkeyup' => 'status()'
        //     ),
        //     'biaya_lain'=> array(
        //             'placeholder' => 'Biaya Lainnya (+)',
        //             'id'    => 'biaya_lain',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => $this->val[0]['bylain'],
        //             'name'     => 'biaya_lain',
        //             'onkeyup' => 'status()'
        //     ),
        //     'total_net'=> array(
        //             'placeholder' => 'Total Net',
        //             'id'    => 'total_net',
        //             'class' => 'form-control',
        //             'value'    => set_value('total_net'),
        //             'name'     => 'total_net',
        //             'readonly' => ''
        //     ),
        //     'statppn'=> array(
        //             'placeholder' => 'Status PPN',
        //             'attr'        => array(
        //                 'id'    => 'statppn',
        //                 'class' => 'form-control',
        //             ),
        //             'data'     => $this->data['statppn'],
        //             'class' => 'form-control',
        //             'value'    => $this->val[0]['sppn'],
        //             'name'     => 'statppn',
        //             'readonly' => '',
        //     ),
        //     'dpp'=> array(
        //             'placeholder' => 'DPP',
        //             'id'    => 'dpp',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => set_value('dpp'),
        //             'name'     => 'dpp',
        //             'readonly' => ''
        //     ),
        //     'ppn'=> array(
        //             'placeholder' => 'PPN',
        //             'id'    => 'ppn',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => set_value('ppn'),
        //             'name'     => 'ppn',
        //             'readonly' => ''
        //     ),
        //     'totppn'=> array(
        //             'placeholder' => 'Total',
        //             'id'    => 'totppn',
        //             'class' => 'form-control',
        //             'data'     => '',
        //             'value'    => set_value('totppn'),
        //             'name'     => 'totppn',
        //             'readonly' => ''
        //     )
        );
    }
    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
	td.details-control {
		background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
	}
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?>
        <div class="box box-danger">
          <div class="box-header">
            <div class="col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <?php
                                echo form_label($form['imp']['placeholder']);
                                echo form_dropdown($form['imp']['name'],$form['imp']['data'] ,$form['imp']['value'] ,$form['imp']['attr']);
                                echo form_error('imp','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <?php
                                echo form_input($form['jml_spk']);
                                echo form_error('jml_spk','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <?php
                                echo form_input($form['jml_sl']);
                                echo form_error('jml_sl','<div class="note">','</div>');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                          <button type='button' class="btn btn-primary btn-pull">Tarik Data Asist</button>
                          <button type='button' class="btn btn-default btn-imp"><a class="fa fa-arrow-down"></a>Import</button>
                          <button type='button' class="btn btn-default btn-batal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
          </div>

          <?php echo form_close(); ?>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="tbl-spk">
              <div class="table-responsive">
                <table class="dataTable-spk table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                          <th style="width: 19%;text-align: center;">ID SPK</th>
                          <th style="width: 19%;text-align: center;">Nama Customer</th>
                          <th style="width: 30%;text-align: center;">No KTP</th>
                          <th style="width: 19%;text-align: center;">Alamat</th>
                          <th style="width: 19%;text-align: center;">Kode Provinsi</th>
                          <th style="width: 19%;text-align: center;">Kode Kota</th>
                          <th style="width: 19%;text-align: center;">Kode Kecamatan</th>
                          <th style="width: 19%;text-align: center;">Kode Kelurahan</th>
                          <th style="width: 19%;text-align: center;">Kode Pos</th>
                          <th style="width: 19%;text-align: center;">NPWP</th>
                          <th style="width: 19%;text-align: center;">No. KK</th>
                          <th style="width: 19%;text-align: center;">Fax</th>
                          <th style="width: 19%;text-align: center;">Email</th>
                          <th style="width: 19%;text-align: center;">Id Sales</th>
                          <th style="width: 19%;text-align: center;">Tgl Psn</th>
                          <th style="width: 19%;text-align: center;">Status SPK</th>
                          <th style="width: 19%;text-align: center;">Kode Tipe Unit</th>
                          <th style="width: 19%;text-align: center;">Kode Warna</th>
                          <th style="width: 19%;text-align: center;">Quantity</th>
                          <th style="width: 19%;text-align: center;">Harga Jual</th>
                          <th style="width: 19%;text-align: center;">Diskon</th>
                          <th style="width: 19%;text-align: center;">Kode PPN</th>
                          <th style="width: 19%;text-align: center;">DP</th>
                          <th style="width: 19%;text-align: center;">FP</th>
                          <th style="width: 19%;text-align: center;">Tipe Pembayaran</th>
                          <th style="width: 19%;text-align: center;">Jumlah Tanda Jadi</th>
                          <th style="width: 19%;text-align: center;">Tgl Pengiriman</th>
                          <th style="width: 19%;text-align: center;">Id Sales Prog</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th style="width: 19%;text-align: center;">ID SPK</th>
                          <th style="width: 19%;text-align: center;">Nama Customer</th>
                          <th style="width: 30%;text-align: center;">No KTP</th>
                          <th style="width: 19%;text-align: center;">Alamat</th>
                          <th style="width: 19%;text-align: center;">Kode Provinsi</th>
                          <th style="width: 19%;text-align: center;">Kode Kota</th>
                          <th style="width: 19%;text-align: center;">Kode Kecamatan</th>
                          <th style="width: 19%;text-align: center;">Kode Kelurahan</th>
                          <th style="width: 19%;text-align: center;">Kode Pos</th>
                          <th style="width: 19%;text-align: center;">NPWP</th>
                          <th style="width: 19%;text-align: center;">No. KK</th>
                          <th style="width: 19%;text-align: center;">Fax</th>
                          <th style="width: 19%;text-align: center;">Email</th>
                          <th style="width: 19%;text-align: center;">Id Sales</th>
                          <th style="width: 19%;text-align: center;">Tgl Psn</th>
                          <th style="width: 19%;text-align: center;">Status SPK</th>
                          <th style="width: 19%;text-align: center;">Kode Tipe Unit</th>
                          <th style="width: 19%;text-align: center;">Kode Warna</th>
                          <th style="width: 19%;text-align: center;">Quantity</th>
                          <th style="width: 19%;text-align: center;">Harga Jual</th>
                          <th style="width: 19%;text-align: center;">Diskon</th>
                          <th style="width: 19%;text-align: center;">Kode PPN</th>
                          <th style="width: 19%;text-align: center;">DP</th>
                          <th style="width: 19%;text-align: center;">FP</th>
                          <th style="width: 19%;text-align: center;">Tipe Pembayaran</th>
                          <th style="width: 19%;text-align: center;">Jumlah Tanda Jadi</th>
                          <th style="width: 19%;text-align: center;">Tgl Pengiriman</th>
                          <th style="width: 19%;text-align: center;">Id Sales Prog</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
              </div>
            </div>
            <div class="tbl-sl">
              <div class="table-responsive">
                <table class="dataTable-sl table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th style="width: 1%;text-align: center;">Detail</th>
                            <th style="width: 19%;text-align: center;">No. Shipping List</th>
                            <th style="width: 19%;text-align: center;">Tanggal Terima</th>
                            <th style="width: 19%;text-align: center;">Main Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 1%;text-align: center;">Detail</th>
                            <th style="width: 19%;text-align: center;">No. Shipping List</th>
                            <th style="width: 19%;text-align: center;">Tanggal Terima</th>
                            <th style="width: 19%;text-align: center;">Main Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Status</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
      $('.btn-batal').hide();
      list();
      get_jmlspk();
      get_jmlsl();
      $("#imp").change(function(){
        list();
      });

      $(".btn-pull").click(function(){
        get_spk();
        // get_sl();
      });

      //-- S P K --
      var columnspk = [];
      columnspk.push({
          "aTargets": [ 21,34 ],
          "mRender": function (data, type, full) {
              return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
          },
          "sClass": "center"
      });

      columnspk.push({
          "aTargets": [ 27,28,29,30,31,33 ],
          "mRender": function (data, type, full) {
            return type === 'export' ? data : numeral(data).format('0,0.00');
          },
          "sClass": "center"
      });

      columnspk.push({
          "aTargets": [ 0,5 ],
        "searchable": false,
          "bSortable": false,
          "sClass": "center"
      });

      // $('.dataTableDetail').DataTable({});
      tablespk = $('.dataTable-spk').DataTable({
          "aoColumnDefs": columnspk,
          "order": [[ 1, "asc" ]],
          "columns": [
              { "data": "idspk" },
              { "data": "nmcustomer" },
              { "data": "noktp" },
              { "data": "alamat" },
              { "data": "kodepropinsi" },
              { "data": "kdkota" },
              { "data": "kdkec" },
              { "data": "kdlurah"},
              { "data": "kdpos" },
              { "data": "npwp" },
              { "data": "nokk" },
              { "data": "fax" },
              { "data": "email" },
              { "data": "idsalespeople" },
              { "data": "tglpsn" },
              { "data": "statusspk" },
              { "data": "kdtipeunit" },
              { "data": "kodewarna" },
              { "data": "qty" },
              { "data": "hrgjual" },
              { "data": "diskon" },
              { "data": "kdppn" },
              { "data": "downpayment" },
              { "data": "fakturpajak" },
              { "data": "tppembayaran" },
              { "data": "jmltandajadi" },
              { "data": "tglpengiriman" },
              { "data": "idsalesprog" },
          ],
          //"lengthMenu": [[ -1], [ "Semua Data"]],
          "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
          "bProcessing": true,
          "bServerSide": true,
          "bDestroy": true,
          "bAutoWidth": false,
          "fnServerData": function ( sSource, aoData, rowCallback ) {
              aoData.push(
                              //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                          );
              $.ajax( {
                  "dataType": 'json',
                  "type": "GET",
                  "url": sSource,
                  "data": aoData,
                  "success": rowCallback
              } );
          },
          'rowCallback': function(row, data, index){
              // alert(data[2]);
              // if(data[3]){
              //     $(row).find('td:eq(23)').css('background-color', '#ff9933');
              // } else {
              //   alert('1');
              // }
          },
          "sAjaxSource": "<?=site_url('imphso/json_dgview_spk');?>",
          "oLanguage": {
              "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
          },
          buttons: [
          ],
          "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
      });
      $('.dataTable-spk').tooltip({
          selector: "[data-toggle=tooltip]",
          container: "body"
      });
      $('.dataTable-spk tfoot th').each( function () {
          var title = $('.dataTable-spk thead th').eq( $(this).index() ).text();
          if(title!=="Detail" && title!=="Edit" && title!=="Delete"){
              $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
          }else{
              $(this).html( '' );
          }
      } );
      tablespk.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function (ev) {
              //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                  that
                      .search( this.value )
                      .draw();
              //}
          } );
      });

      //-- SHIPPING LIST --
      var columnsl = [];
      columnsl.push({
          "aTargets": [ 2 ],
          "mRender": function (data, type, full) {
              return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
          },
          "sClass": "center"
      });

      columnsl.push({
          "aTargets": [ 0,5 ],
        "searchable": false,
          "bSortable": false,
          "sClass": "center"
      });

      $('.dataTableDetail').DataTable({});
      tablesl = $('.dataTable-sl').DataTable({
          "aoColumnDefs": columnsl,
          "order": [[ 1, "asc" ]],
          "columns": [
              {
                  "className":      'details-control',
                  "data":           null,
                  "defaultContent": ''
              },
              { "data": "noshippinglist" },
              { "data": "tanggalterima" },
              { "data": "maindealerid" },
              { "data": "noinvoice" },
              { "data": "statusshippinglist" }
          ],
          //"lengthMenu": [[ -1], [ "Semua Data"]],
          "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
          "bProcessing": true,
          "bServerSide": true,
          "bDestroy": true,
          "bAutoWidth": false,
          "fnServerData": function ( sSource, aoData, fnCallback ) {
              aoData.push(
                              //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                          );
              $.ajax( {
                  "dataType": 'json',
                  "type": "GET",
                  "url": sSource,
                  "data": aoData,
                  "success": fnCallback
              } );
          },
          'rowCallback': function(row, data, index){
              //if(data[23]){
                  //$(row).find('td:eq(23)').css('background-color', '#ff9933');
              //}
          },
          "sAjaxSource": "<?=site_url('imphso/json_dgview_sl');?>",
          "oLanguage": {
              "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
          },
          buttons: [
          ],
          "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
      });
      $('.dataTable-sl').tooltip({
          selector: "[data-toggle=tooltip]",
          container: "body"
      });
      // Add event listener for opening and closing details
      $('.dataTable-sl tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = tablesl.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              //format(row.data());
              tr.addClass('shown');
          }
      } );
      $('.dataTable-sl tfoot th').each( function () {
          var title = $('.dataTable-sl thead th').eq( $(this).index() ).text();
          if(title!=="Detail" && title!=="Edit" && title!=="Delete"){
              $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
          }else{
              $(this).html( '' );
          }
      } );
      tablesl.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function (ev) {
              //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                  that
                      .search( this.value )
                      .draw();
              //}
          } );
      });
      function format ( d ) {
          //console.log(d);
          var tdetail =  '<table class="table table-bordered table-hover dataTableDetail">'+
              '<thead>' +
                  '<tr style="background-color: #d73925;color: #fff;">'+
                      '<th style="text-align: center;width:16px;"></th>'+
                      '<th style="text-align: center;width:15px;">No.</th>'+
                      '<th style="text-align: center;">Kode Tipe Unit</th>'+
                      '<th style="text-align: center;">Kode Warna</th>'+
                      '<th style="text-align: center;">Kuantitas Terkirim</th>'+
                      '<th style="text-align: center;">Kuantitas Diterima</th>'+
                      '<th style="text-align: center;">No. Mesin</th>'+
                      '<th style="text-align: center;">No. Rangka</th>'+
                      '<th style="text-align: center;">Status RFS</th>'+
                      '<th style="text-align: center;">PO ID</th>'+
                      '<th style="text-align: center;">Kelengkapan Unit</th>'+
                      '<th style="text-align: center;">No Goods Receipt</th>'+
                      '<th style="text-align: center;">Doc NRFS ID</th>'+
                  '</tr>'+
              '</thead><tbody>';
          var no = 1;
          var total = 0;
          if(d.detail.length>0){
              $.each(d.detail, function(key, data){
                  tdetail+='<tr>'+
                      '<td style="text-align: center;"></td>'+
                      '<td style="text-align: center;">'+no+'</td>'+
                      '<td style="text-align: ">'+data.kodetipeunit+'</td>'+
                      '<td style="text-align: ">'+data.kodewarna+'</td>'+
                      '<td style="text-align: ">'+data.kuantitasterkirim+'</td>'+
                      '<td style="text-align: ">'+data.kuantitasditerima+'</td>'+
                      '<td style="text-align: ">'+data.nomesin+'</td>'+
                      '<td style="text-align: ">'+data.norangka+'</td>'+
                      '<td style="text-align: ">'+data.statusrfs+'</td>'+
                      '<td style="text-align: ">'+data.poid+'</td>'+
                      '<td style="text-align: ">'+data.kelengkapanunit+'</td>'+
                      '<td style="text-align: ">'+data.nogoodsreceipt+'</td>'+
                      '<td style="text-align: ">'+data.docnrfsid+'</td>'+
                  '</tr>';
                  no++;
              });
          }
          tdetail += '</tbody></table>';
          return tdetail;
      }
    });

    function list(){
      var nmlist = $('#imp').val();
      if(nmlist==='SL'){
        $('.tbl-sl').show();
        $('.tbl-spk').hide();
      } else {
        $('.tbl-sl').hide();
        $('.tbl-spk').show();
      }
    }

    function get_jmlspk(){
        $.ajax({
            type: "POST",
            url: "<?=site_url("imphso/get_jmlspk");?>",
            data: { },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $("#jml_spk").val(data.jml);
                    if(data.jml>0){
                      $('.btn-batal').show();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function get_jmlsl(){
        $.ajax({
            type: "POST",
            url: "<?=site_url("imphso/get_jmlsl");?>",
            data: { },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $("#jml_sl").val(data.jml);
                    if(data.jml>0){
                      $('.btn-batal').show();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function get_sl(){
      var data = '';
      var date = new Date();
      var rdate = moment(date).format('YYYY-MM-DD hh:mm:ss');
      var date7 = new Date(Date.now() - 7*24*60*60*1000).toISOString().slice(0,10);
      var rdate7 = moment(date7).format('YYYY-MM-DD hh:mm:ss');

      var token = sha256('dgi-key-live:4D023875-C265-4CE6-A388-2676022816CEdgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA1578283522');

      // // new sha256('dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE + dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA + 1578283522');
      var options = {
        'method': 'POST',
        'url': 'https://astraapps.astra.co.id/dmsahassapi/dgi-api/v1/uinb/read',
        'timeout': 0,
        'cors': true ,
        'secure': true ,
        'headers': {
					'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'X-Request-Time': '1578283522',
          'DGI-Api-Key': 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE',
          'DGI-API-Token': token
        },
        'data': JSON.stringify({"fromTime":rdate7,"toTime":rdate,"dealerId":"10091","poId":"","noShippingList":""})

      };
      //
      $.ajax(options).done(function (response) {
        ins_tmp_po(response);
      });

      // var settings = {
      //   "url": "https://devproxy.astra.co.id/api_gateway/astra-api-developer/dmsahassapi-qa/dgi-api/v1/uinb/read",
      //   "method": "POST",
      //   "timeout": 0,
      //   "headers": {
      //     "Content-Type": "application/json",
      //     "X-Request-Time": "1578283522",
      //     "DGI-Api-Key": "dgi-key-live:52ADFCEE-18BE-470E-9772-4E76EB0CDF00",
      //     "DGI-API-Token": "7b55542a2a8b60303673d6ceebd77f37b52681128ca46878396bc6d27ad372a2"
      //   },
      //   "data": JSON.stringify({"fromTime":"2017-12-15 00:00:00","toTime":"2017-12-19 00:00:00","dealerId":"04700","poId":"","noShippingList":""}),
      // };
      //
      // $.ajax(settings).done(function (response) {
      //   // ins_tmp_po(json_encode(response));
      //   ins_tmp_po(response);
      // });
    }

    function get_spk(){
      var data = '';
      var date = new Date();
      var rdate = moment(date).format('YYYY-MM-DD hh:mm:ss');
      var date7 = new Date(Date.now() - 7*24*60*60*1000).toISOString().slice(0,10);
      var rdate7 = moment(date7).format('YYYY-MM-DD hh:mm:ss');

     //  var token = sha256('dgi-key-live:4D023875-C265-4CE6-A388-2676022816CEdgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA1578283522');

     //  $.ajax({
					// // method : 'POST',
     //      'type': "POST",
     //      'url': 'https://astraapps.astra.co.id/dmsahassapi/dgi-api/v1/spk/read',
	    //     'timeout': 0,
	    //     'cors': true ,
	    //     'secure': true ,
	    //     'async':true,
	    //     'crossDomain':true,
     //        'dataType': 'json',
     //      'data': JSON.stringify({"fromTime":rdate7,"toTime":rdate,"dealerId":"10091","idProspect":"","idSalesPeople":""}),
					// 'headers': {

    	// 			'Access-Control-Allow-Origin': '*',
    	// 			'Access-Control-Allow-Headers': 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method',
    	// 			'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, DELETE',
					// 	// 'Access-Control-Allow-Credentials': true,
					// 	// 'Access-Control-Allow-Methods': 'OPTIONS, GET, POST',
					// 	// 'Access-Control-Allow-Headers': 'Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control',
					// 	// 'Access-Control-Allow-Origin': '*',
	    //       'Content-Type': 'application/json',
	    //       'X-Request-Time': '1578283522',
	    //       'DGI-Api-Key': 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE',
	    //       'DGI-API-Token': token
	    //     },
     //      success: function(resp){
     //              ins_tmp_po(resp);
     //      },
     //      error:function(event, textStatus, errorThrown) {
     //          swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
     //      }
     //  });

      // var options = {
      //   'method': 'POST',
      //   'url': 'https://astraapps.astra.co.id/dmsahassapi/dgi-api/v1/spk/read',
      //   'cors': true ,
      //   'secure': true ,
      //   'headers': {
			// 		'Access-Control-Allow-Origin': '*',
      //     'Content-Type': 'application/json',
      //     'X-Request-Time': '1578283522',
      //     'DGI-Api-Key': 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE',
      //     'DGI-API-Token': token
      //   },
      //   "data": JSON.stringify({"fromTime":rdate7,"toTime":rdate,"dealerId":"10091","idProspect":"","idSalesPeople":""})
      // };
			//
      // $.ajax(options).done(function (response) {
      //   // console.log(response);
      //   ins_tmp_po(response);
        // if(response){
        //   ins_tmp_spk(response);
        // } else {
        //   swal({
        //     title: "Data Tidak Ada!",
        //     text: "Data yang ditarik tidak ada!",
        //     type: "error",
        //   })
        // }
      // });
    }

    function ins_tmp_po(info){
      // var info = json_decode(info);
      $.ajax({
          type: "POST",
          url: "<?=site_url("imphso/sl_tmp");?>",
          data: { "info":info },
          success: function(resp){
              if (resp==='success'){
                  get_jmlsl();
                  tablesl.ajax.reload();
              }
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function ins_tmp_spk(info){
      // var info = json_decode(info);
      $.ajax({
          type: "POST",
          url: "<?=site_url("imphso/spk_tmp");?>",
          data: { "info":info },
          success: function(resp){
              if (resp==='success'){
                  get_jmlspk();
                  tablespk.ajax.reload();
              }
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    function sha256(ascii) {
	     function rightRotate(value, amount) {
		       return (value>>>amount) | (value<<(32 - amount));
	     };

	     var mathPow = Math.pow;
	     var maxWord = mathPow(2, 32);
	     var lengthProperty = 'length'
	     var i, j; // Used as a counter across the whole file
	     var result = ''

	     var words = [];
	     var asciiBitLength = ascii[lengthProperty]*8;

      	//* caching results is optional - remove/add slash from front of this line to toggle
      	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
      	// (we actually calculate the first 64, but extra values are just ignored)
	     var hash = sha256.h = sha256.h || [];
	      // Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	     var k = sha256.k = sha256.k || [];
	     var primeCounter = k[lengthProperty];
      	/*/
      	var hash = [], k = [];
      	var primeCounter = 0;
      	//*/

    	var isComposite = {};
    	for (var candidate = 2; primeCounter < 64; candidate++) {
    		if (!isComposite[candidate]) {
    			for (i = 0; i < 313; i += candidate) {
    				isComposite[i] = candidate;
    			}
    			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
    			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
    		}
    	}

    	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
    	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
    	for (i = 0; i < ascii[lengthProperty]; i++) {
    		j = ascii.charCodeAt(i);
    		if (j>>8) return; // ASCII check: only accept characters in range 0-255
    		words[i>>2] |= j << ((3 - i)%4)*8;
    	}
    	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
    	words[words[lengthProperty]] = (asciiBitLength)

    	// process each chunk
    	for (j = 0; j < words[lengthProperty];) {
    		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
    		var oldHash = hash;
    		// This is now the undefinedworking hash", often labelled as variables a...g
    		// (we have to truncate as well, otherwise extra entries at the end accumulate
    		hash = hash.slice(0, 8);

    		for (i = 0; i < 64; i++) {
    			var i2 = i + j;
    			// Expand the message into 64 words
    			// Used below if
    			var w15 = w[i - 15], w2 = w[i - 2];

    			// Iterate
    			var a = hash[0], e = hash[4];
    			var temp1 = hash[7]
    				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
    				+ ((e&hash[5])^((~e)&hash[6])) // ch
    				+ k[i]
    				// Expand the message schedule if needed
    				+ (w[i] = (i < 16) ? w[i] : (
    						w[i - 16]
    						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
    						+ w[i - 7]
    						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
    					)|0
    				);
    			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
    			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
    				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

    			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
    			hash[4] = (hash[4] + temp1)|0;
    		}

    		for (i = 0; i < 8; i++) {
    			hash[i] = (hash[i] + oldHash[i])|0;
    		}
    	}

    	for (i = 0; i < 8; i++) {
    		for (j = 3; j + 1; j--) {
    			var b = (hash[i]>>(j*8))&255;
    			result += ((b < 16) ? 0 : '') + b.toString(16);
    		}
    	}
    	return result;
    }
</script>

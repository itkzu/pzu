<?php

/* 
 * ***************************************************************
 * Script : html.php
 * Version : 
 * Date : Oct 17, 2017 10:32:23 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php
$file = $this->uri->segment(4);
if($file==="excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap Referensi Posting Kas_Bank " .date('Y-m-d H:i:s').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$this->load->library('table');
$caption = "<b>Rekap Referensi Posting Kas/Bank</b>"
        . "<br>"
        . "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->session->userdata('data')['cabang'])."</b>"
        . "<br><br>";
$namaheader = array(
    array('data' => 'No.'
                        , 'style' => 'text-align: center; width: 5%; font-size: 12px;'),
    array('data' => 'Nama Referensi'
                        , 'colspan' => 2
                        , 'style' => 'text-align: center; width: 35%; font-size: 12px;'),
    array('data' => 'Debit/Kredit'
                        , 'style' => 'text-align: center; width: 15%; font-size: 12px;'),
    array('data' => 'Keterangan'
                            , 'colspan' => 2
                        , 'style' => 'text-align: center; width: 35%; font-size: 12px;'),
    array('data' => 'Status'
                            , 'colspan' => 2
                        , 'style' => 'text-align: center; width: 10%; font-size: 12px;'),
);
// Caption text
$this->table->set_caption($caption);
$this->table->add_row($namaheader);
$no = 1;
foreach ($data as $value) {
    //var_dump($value['detail']);
    $header_data = array(
        array('data' => $no
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['nmrefkb']
                            , 'colspan' => 2
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $value['dk']
                            , 'colspan' => 2
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['ket']
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $value['faktif']
                            , 'colspan' => 2
                            , 'style' => 'text-align: center; font-size: 12px;'),
    );  
    $this->table->add_row($header_data);
    if(count($value['detail'])>0){
        $detail_header = array(
            array('data' => '&nbsp'
                            , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
            array('data' => 'No.'
                                , 'style' => 'text-align: center; width: 5%; font-size: 12px;background-color: #ddd;'),
            array('data' => 'Kode Akun'
                                , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
            array('data' => 'Nama Akun'
                                , 'colspan' => 3
                                , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
            
            array('data' => 'Persentase'
                                , 'style' => 'text-align: center; font-size: 12px;background-color: #ddd;'),
        );  
        $this->table->add_row($detail_header);
        $dno = 1;
        foreach ($value['detail'] as $v_detail) {
            $detail_data = array(
                array('data' => '&nbsp'
                                , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $dno
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $v_detail['kdakun']
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $v_detail['nmakun']
                                , 'colspan' => 3
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                
                array('data' => $v_detail['mpl']
                                    , 'style' => 'text-align: center; font-size: 12px;'),
            );  
            $this->table->add_row($detail_data);
            $dno++;
        }
//        $detail_footer = array(
//            array('data' => '&nbsp'
//                            , 'colspan' => 8
//                            , 'style' => 'text-align: center; font-size: 12px;background-color: #fff;'),
//        );  
//        $this->table->add_row($detail_footer);
    }
    $no++;
}

$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
$this->table->set_template($template);
echo $this->table->generate();    
<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nopo']['placeholder']);
                                            echo form_input($form['nopo']);
                                            echo form_error('nopo','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglpo']['placeholder']);
                                            echo form_input($form['tglpo']);
                                            echo form_error('tglpo','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>


                              </div>
                            </div>
                          <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nmsup']['placeholder']);
                                            echo form_input($form['nmsup']);
                                            echo form_error('nmsup','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['alamat']['placeholder']);
                                            echo form_input($form['alamat']);
                                            echo form_error('alamat','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nofaktur']['placeholder']);
                                            echo form_input($form['nofaktur']);
                                            echo form_error('nofaktur','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['grno']['placeholder']);
                                        echo form_input($form['grno']);
                                        echo form_error('grno','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['total']['placeholder']);
                                        echo form_input($form['total']);
                                        echo form_error('total','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['tot_disc']['placeholder']);
                                        echo form_input($form['tot_disc']);
                                        echo form_error('tot_disc','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['total_net']['placeholder']);
                                        echo form_input($form['total_net']);
                                        echo form_error('total_net','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>

                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['bkuno']['placeholder']);
                                        echo form_input($form['bkuno']);
                                        echo form_error('bkuno','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['dpp']['placeholder']);
                                        echo form_input($form['dpp']);
                                        echo form_error('dpp','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['ppn']['placeholder']);
                                        echo form_input($form['ppn']);
                                        echo form_error('ppn','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%;text-align: center;">Kode Part</th>
                                            <th style="text-align: center;">Nama Part</th>
                                            <th style="text-align: center;">Kode Grup</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                                <th style="text-align: center;"></th>
                                                <th style="text-align: center;"></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-save">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Part Nomor</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kdpart']['placeholder']);
                                    echo form_input($form['kdpart']);
                                    echo form_error('kdpart','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['nmpart']['placeholder']);
                                    echo form_input($form['nmpart']);
                                    echo form_error('nmpart','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label('Jenis Group');
                                    echo form_dropdown($form['kdgrup']['name'],$form['kdgrup']['data'] ,$form['kdgrup']['value'] ,$form['kdgrup']['attr']);
                                    echo form_error('kdgrup','<div class="note">','</div>');
                                ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {

          getKdGrup();
        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                jnstrx : "required"
            },
            messages : {
                jnstrx : ""
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdrefkb  : "required",
                nilai  : "required",
                ket  : "required",
                darike  : "required"
            },
            messages : {
                kdrefkb  : "Masukkan Data Transaki",
                nilai  : "Masukkan Nilai",
                ket  : "Masukkan Keterangan",
                darike  : "required"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var column = [];

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "kdpart" },
                { "data": "nmpart" },
                { "data": "kdgrup" },
                { "data": "edit" }
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nopo", "value": $("#nopo").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('pobkl/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });


        $('.btn-simpan').click(function(){
          //alert($('#kdaks').val());
            if ($("#modalform").valid()) {
                addDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-save").click(function(){
            if ($("#myform").valid()) {
              save();
            }
        });
    });

    function clear(){
        $("#kdpart").val('');
        $("#nmpart").val('');
        $("#kdgrup").val('');
    }

    function edit(kdpart,nmpart,kdgrup){
        $("#kdpart").val(kdpart);
        $("#nmpart").val(nmpart);
        $("#kdgrup").val(kdgrup);
        //$("#kdakun").val(nmakun);
        $("#kdgrup").trigger("change");
        $('#kdgrup').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });
        $('#modal_transaksi').modal('toggle');
    }

    function getKdGrup(){
        var kdpart = $("#kdpart").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("pobkl/getKdGrup");?>",
            data: {"kdpart":kdpart},
            beforeSend: function() {
                $('#kdgrup').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kode Group --'));
                $("#kdgrup").trigger("change.chosen");
                if ($('#kdgrup').hasClass("chosen-hidden-accessible")) {
                    $('#kdgrup').select2('destroy');
                    $("#kdgrup").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdgrup')
                        .append($('<option>', { value : value.kdgrup })
                        .html("<b style='font-size: 14px;'>" + value.kdgrup +" - " + value.ket + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.kdgrup + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }


    function addDetail(){
        var kdpart = $("#kdpart").val();
        var nmpart = $("#nmpart").val();
        var kdgrup = $("#kdgrup").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("pobkl/addDetail");?>",
            data: {"kdpart":kdpart
                    ,"nmpart":nmpart
                    ,"kdgrup":kdgrup },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                clear();

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    // $("#nokb").val(data.nokb);
                    if (data.tipe==="success"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                        });
                    }else{
                        //refresh();
                    };
                        refresh();
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Inputan!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('pobkl');?>';
        });
    }

    function save(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
                }, function () {
                  window.location.href = '<?=site_url('pobkl');?>';
                });
    }

</script>

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Impunit_qry
 *
 * @author 
 */

class Impunit_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    /*
    public function upload_file($filename){
        $this->load->library('upload'); // Load librari upload    

        $config['upload_path'] = './excel/';    
        $config['allowed_types'] = 'xlsx';    
        $config['max_size']  = '2048';    
        $config['overwrite'] = true;    
        $config['file_name'] = $filename;      
        
        $this->upload->initialize($config); // Load konfigurasi uploadnya    
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil      
            // Jika berhasil :      
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');      
            return $return;    
        }else{      
            // Jika gagal :      
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());      
            return $return;    
        }  
    }    
    */

    public function insertBeli($data)
    {
        $this->db->empty_table('pzu.unit_imp');
        $this->db->insert_batch('pzu.unit_imp', $data);
        if ($this->db->affected_rows() > 0){
            $this->db->get('pzu.unit_imp_hrgbeli()');
            return 'Data Harga Beli Unit sukses di Import';
        } else{
            return 'Error! '.$this->db->_error_message();
        }
        
    }

    public function insertONTR($data)
    {
        $this->db->empty_table('pzu.unit_imp');
        $this->db->insert_batch('pzu.unit_imp', $data);
        if ($this->db->affected_rows() > 0){
            $this->db->get('pzu.unit_imp_hrgon()');
            return 'Data Harga ON-The Road Unit sukses di Import';
        } else{
            return 'Error! '.$this->db->_error_message();
        }        
    }

    public function insertOFFTR($data)
    {
        $this->db->empty_table('pzu.unit_imp');
        $this->db->insert_batch('pzu.unit_imp', $data);
        if ($this->db->affected_rows() > 0){

            try {
                $this->db->get('pzu.unit_imp_hrgoff()');
                return 'Data Harga OFF-The Road Unit sukses di Import';
                
                //$this->db->_error_message();
            } catch (Exception $e) {
                //return 'Error! '.$this->db->_error_message();
                return 'Error! '.$e->getMessage();
                //log_message('error: ',$e->getMessage());
                //return;
            }            
        } else{
            return 'Error! '.$this->db->_error_message();
        }
    }
}

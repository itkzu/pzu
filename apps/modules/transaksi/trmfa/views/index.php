<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
    .pass-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    } 
    #load{
        width: 80%;
        height: 80%;
        position: fixed;
        text-indent: 100%;
        background: #ebebeb url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>

<div id="load">Loading...</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="box box-danger">   
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?>            -->

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-md-1">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                                </div>
                            </div>

                            <div class="col-md-3">
                              <div class="box-header box-view">
                                <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()" > Ubah</button> 
                              </div>
                            </div>

                            <div class="col-md-3"> 
                                <div class="box-header">
                                    <button type="button" class="btn btn-primary btn-imp">Import Assist</button> 
                                </div>
                            </div>

                            <!-- <div class="col-md-2">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-cetak"><i class="fa fa-print"></i> Cetak L/R</button>
                                 </div>
                            </div> -->

                            <!-- <div class="col-md-3">
                                <div class="box-header form-group">
                                    <?php
                                        echo form_input($form['ket']);
                                        echo form_error('ket','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link " id="list-tfa" name="list-tfa" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Daftar Faktur Kendaraan Bermotor yang belum diterima</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="input-tfa" name="input-tfa" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Input Terima Faktur Kendaraan Bermotor</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-tfa">

                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">No. Mesin</th>
                                                    <th style="text-align: center;">No. Rangka</th>
                                                    <th style="text-align: center;">Tipe Unit</th> 
                                                    <th style="text-align: center;">Nama Konsumen</th> 
                                                    <th style="text-align: center;">Alamat</th> 
                                                    <th style="text-align: center;">Kelurahan</th> 
                                                    <th style="text-align: center;">Kecamatan</th> 
                                                    <th style="text-align: center;">Kota</th> 
                                                    <th style="text-align: center;">No. HP</th> 
                                                    <th style="text-align: center;">No. DO</th> 
                                                    <th style="text-align: center;">Tgl. DO</th> 
                                                    <th style="text-align: center;">Tgl. Aju Faktur</th> 
                                                    <th style="text-align: center;">Leasing</th> 
                                                    <th style="text-align: center;">Sales</th> 
                                                    <th style="text-align: center;">Supplier</th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-tfa"> 

                                <div class="col-md-12">
                                    <br>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nosin']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <?php
                                                echo form_input($form['nosin']);
                                                echo form_error('nosin','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2 ">
                                            <?php echo form_label($form['tgltrmfa']['placeholder']); ?>
                                        </div>

                                        <div class="col-md-3 ">
                                            <?php
                                                echo form_input($form['tgltrmfa']);
                                                echo form_error('tgltrmfa','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 


                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nama']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nama']);
                                                        echo form_error('nama','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nofaktur']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['nofaktur']);
                                                        echo form_error('nofaktur','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>      

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['alamat']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['alamat']);
                                                        echo form_error('alamat','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tglfaktur']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['tglfaktur']);
                                                        echo form_error('tglfaktur','<div class="note">','</div>');
                                                    ?>
                                                </div> 

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_label("<small><i>Tanggal Terbit Faktur</i></small>");
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>   

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-1">  
                                                </div>
                                                <div class="col-md-1"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kel']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_input($form['kel']);
                                                        echo form_error('kel','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                                <div class="col-md-1"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kec']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_input($form['kec']);
                                                        echo form_error('kec','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['harga']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['harga']);
                                                        echo form_error('harga','<div class="note">','</div>');
                                                    ?>
                                                </div> 

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_label("<small><i>Harga yg tertera di Faktur</i></small>");
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>  

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-1">  
                                                </div>
                                                <div class="col-md-1"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kota']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-4"> 
                                                    <?php 
                                                        echo form_dropdown($form['kota']['name'],
                                                                           $form['kota']['data'],
                                                                           $form['kota']['value'],
                                                                           $form['kota']['attr']);
                                                        echo form_error('kota', '<div class="note">', '</div>');  
                                                    ?>
                                                </div>  
                                            </div>
                                        </div>  
                                    </div>
                                </div>   

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['noktp']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['noktp']);
                                                        echo form_error('noktp','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nodo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['nodo']);
                                                        echo form_error('nodo','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 
                                        <br>
                                    </div>
                                </div>     

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['kdtipe']);
                                                        echo form_error('kdtipe','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tgldo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['tgldo']);
                                                        echo form_error('tgldo','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tahun']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-3"> 
                                                    <?php
                                                        echo form_input($form['tahun']);
                                                        echo form_error('tahun','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['cc']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-3"> 
                                                    <?php
                                                        echo form_input($form['cc']);
                                                        echo form_error('cc','<div class="note">','</div>');
                                                    ?>
                                                </div> 

                                                <div class="col-md-1"> 
                                                    <?php
                                                        echo form_label('CC');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>   
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['warna']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['warna']);
                                                        echo form_error('warna','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['notpt']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['notpt']);
                                                        echo form_error('notpt','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nora2']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nora2']);
                                                        echo form_error('nora2','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nosut']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nosut']);
                                                        echo form_error('nosut','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nosin2']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nosin2']);
                                                        echo form_error('nosin2','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div>
              </div>
          </div>
            <!-- /.box-body -->
        <?php echo form_close(); ?>
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
        upd_DOCH();
        // var date7 = new Date(Date.now() - 3*24*60*60*1000).toISOString().slice(0,10);
        // var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
        // var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
        // var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
        // get_DOCH(rdate1,rdate7);
        reset();
        autonum();
        $("#kota").attr("disabled",true);
        $('#nosin').mask('***-***-***-***');
        $('.btn-ubah').hide(); 

        $('#btn-refresh').show();  

        $("#nosin").change(function(){
            set_nosin(); 
        });

        $('.btn-refresh').click(function(){
            table.ajax.reload();
        });

        $('#input-tfa').click(function(){
            $('.btn-ubah').show();  
            $('.btn-ubah').attr("disabled",true);  
            $('.btn-refresh').hide();
        });

        $('#list-tfa').click(function(){
            $('.btn-ubah').hide();   
            $('.btn-refresh').show();
        }); 

        $('.btn-imp').click(function () {
            var date7 = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            get_DOCH(rdate1,rdate7);
        });

        table = $('.dataTable').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mData" : "nodo",
                    "mData" : "nosin2",
                    "mRender": function (data, type, row) {
                        var btn = '<a href="trmfa/edit/' + row.nodo +'">' + row.nosin2 + ' </a>';
                        return btn;
                    }
                },
                {
                    "aTargets": [ 11,12 ],
                    "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                    },
                    "sClass": "center"
                    
                }
                // {
                //     "aTargets": [ 6 ],
                //     "mRender": function (data, type, full) {
                //         return type === 'export' ? data : numeral(data).format('0,0.00');
                //     },
                //     "sClass": "right"
                    
                // } 
                ],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nosin2" },
                { "data": "nora2" },
                { "data": "kdtipe"},
                { "data": "nama" },
                { "data": "alamat"},
                { "data": "kel"},
                { "data": "kec"},
                { "data": "kota"},
                { "data": "nohp"},
                { "data": "nodo"},
                { "data": "tgldo"},
                { "data": "tgl_aju_fa"},
                { "data": "jnsbayarx"},
                { "data": "nmsales"},
                { "data": "nmsup"}, 
                { "data": "edit"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('trmfa/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="No." ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
    });

    // DOCH
    function get_DOCH(rdate1,rdate7){ 
        $("#load").show();
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_DOCH'); ?>",
            data: { "rdate1" : rdate1 , "rdate7" : rdate7 }, 
            success: function (resp) {  
                ins_DOCH(resp); 
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_DOCH(resp){ 
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_DOCH'); ?>",
            data: {"info" : resp}, 
            success: function (resp) {
                save_DOCH(); 
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_DOCH(){   
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_DOCH'); ?>",
            data: {}, 
            success: function (resp) {
                $("#load").fadeOut(); 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    upd_DOCH()
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }   

    function upd_DOCH(){   
        var status = 'AJU_FA';
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/upd_DOCH'); ?>",
            data: {"status":status}, 
            success: function (resp) {
                // $("#load").fadeOut(); 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    table.ajax.reload();
                });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }   

    function autonum(){
        $('#harga').autoNumeric('init');
        $('#cc').autoNumeric('init');
    }

    function set_nosin(){
      // alert(ket);
      var sin = $("#nosin").val();
      var nosin = sin.toUpperCase(); 
      // alert(sinx);
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmfa/set_nosin");?>",
            data: {"nosin":nosin},
            success: function(resp){ 
              if(resp==='"empty"'){
                  swal({
                      title: "No Mesin "+nosin+" belum proses Pengajuan Faktur atau sudah melewati proses Kwitansi STNK",
                      text: "Silahkan Periksa Kembali",
                      type: "warning"
                  })
                  clear();  
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                    // baris 1 
                      $("#tgltrmfa").val($.datepicker.formatDate('dd-mm-yy', new Date()));
                      $("#tglfaktur").val($.datepicker.formatDate('dd-mm-yy', new Date()));
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                      $("#nama").val(data.nama);
                      $("#alamat").val(data.alamat_s);
                      $("#kel").val(data.kel_s);
                      $("#kec").val(data.kec_s);
                      $("#kota").val(data.kota_s);
                      $("#noktp").val(data.noktp);
                      $("#noktp").mask('**.**.**.******.****');
                      $("#kdtipe").val(data.kdtipe2);
                      $("#tahun").val(data.tahun);
                      $("#warna").val(data.nmwarna);
                      $("#tahun").val(data.tahun);
                      $("#nora2").val(data.nora);
                      $("#nosin2").val(data.nosin);
                      $("#notpt").val(data.notpt);
                      $("#nosut").val(data.nosut);
                      $("#nodo").val(data.nodo); 
                      $("#nofaktur").val(data.nofaktur); 


                      $("#cc").autoNumeric('set',data.cc);
                      $("#harga").autoNumeric('set',data.harga); 
                      // $("#potongan").val(data.disc);
                      $(".btn-ubah").prop("disabled",false); 
                      // set_ket(nopo);
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    } 

    function ubah(){
      var nodo = $('#nodo').val();
      // alert('trmfa/edit/'+nodo);
      window.location.href = 'trmfa/edit/'+nodo;
    } 

    function clear(){
        $('#nopo').val('');
        $('#tipebayar').val('');
        $('#nmsup').val('');
        $("#tglpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#jth_tmp").val('15');
        $('#tgltmp').val(dateback($("#tglpo").val()));

        //nominal
        $('#sub_tot').autoNumeric('set',0);
        $('#potongan').autoNumeric('set',0); 
        $('#disc_top').autoNumeric('set',0); 
        $('#dpp').autoNumeric('set',0);
        $('#ppn').autoNumeric('set',0);
        $('#total').autoNumeric('set',0);

        $('#banner').val(''); 
    }

    function reset(){
        $('#nosin').val('');
        $('#nama').val('');
        $('#alamat').val('');
        $('#nofaktur').val('');
        $('#kel').val('');
        $('#kec').val('');
        $('#kota').val('');
        $('#noktp').val('');
        $('#kdtipe').val('');
        $('#nodo').val('');
        $('#tahun').val('');
        $('#nora2').val('');
        $('#nosin2').val('');
        $('#notpt').val('');
        $('#nosut').val('');
        $("#tgltrmfa").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tglfaktur").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tglpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#cc").val('');
        $("#harga").val(''); 
    }

    function dateback(tgl) {
      var less = $("#jth_tmp").val();
      var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var res = day + '-' + month + '-' + year;
        return res;
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
      } 
</script>

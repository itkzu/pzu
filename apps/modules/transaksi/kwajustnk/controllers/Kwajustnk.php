<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Kwajustnk
 *
 * @author adi
 */
class Kwajustnk extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('kwajustnk/submit'),
            'add' => site_url('kwajustnk/add'),
            'edit' => site_url('kwajustnk/edit'),
            'ctk'      => site_url('kwajustnk/ctk'),
            'reload' => site_url('kwajustnk'),
        );
        $this->load->model('kwajustnk_qry');
        $kota = $this->kwajustnk_qry->getkota();
        foreach ($kota as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk() {
        $nokps = $this->uri->segment(3);  
                        $no = str_replace("+","/",$nokps);  
        $this->data['data'] = $this->kwajustnk_qry->ctk($no);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function getNoID() {
        echo $this->kwajustnk_qry->getNoID();
    }

    public function set_noaju() {
        echo $this->kwajustnk_qry->set_noaju();
    }

    public function set_nosin() {
        echo $this->kwajustnk_qry->set_nosin();
    }

    public function submit() {
        echo $this->kwajustnk_qry->submit();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
           'noajustnk'=> array(
                    'placeholder' => 'No. Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'noajustnk',
                    'name'        => 'noajustnk',
                    'value'       => set_value('noajustnk'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglajustnk'=> array(
                    'placeholder' => 'Tgl Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'tglajustnk',
                    'name'        => 'tglajustnk',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin/Rk',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'nora'=> array( 
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'kdtipe'=> array(
                     'placeholder' => 'Tipe Unit',
                     'id'          => 'kdtipe',
                     'name'        => 'kdtipe',
                     'value'       => set_value('kdtipe'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => set_value('warna'),
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'harga_beli'=> array(
                    'placeholder' => 'Harga Beli Unit',
                    'value'       => set_value('harga_beli'),
                    'id'          => 'harga_beli',
                    'name'        => 'harga_beli',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right',
                    // 'readonly'    => '',
            ),
            'nama'=> array(
                    'placeholder' => 'Nama',
                    'value'       => set_value('nama'),
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly'    => '',
            ),  
            'alamat'=> array(
                    'placeholder' => 'Alamat',
                    'value'       => set_value('alamat'),
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kel'=> array(
                     'placeholder' => 'Kel.',
                     'id'          => 'kel',
                     'name'        => 'kel',
                     'value'       => set_value('kel'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kec'=> array(
                     'placeholder' => 'Kec.',
                     'id'          => 'kec',
                     'name'        => 'kec',
                     'value'       => set_value('kec'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kota'     => array(
                    'placeholder' => 'Kota.',
                    //'type'        => 'hidden',
                    'attr'        => array(
                       'id'    => 'kota',
                       'class' => 'form-control',
                     ),
                    'data'        => $this->data['kota'],
                    'name'        => 'kota',
                    'value'       => set_value('kota'), 
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'nohp'=> array(
                     'placeholder' => 'No HP',
                     'id'          => 'nohp',
                     'name'        => 'nohp',
                     'value'       => set_value('nohp'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'notelp'=> array(
                     'placeholder' => 'Telp.',
                     'id'          => 'notelp',
                     'name'        => 'notelp',
                     'value'       => set_value('notelp'),
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'ket'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => '',
                    'type'        => 'hidden',
            ),
        );
    }
     private function _init_edit($no = null){
        if(!$no){
            $nokb = $this->uri->segment(3);
        }
        $this->_check_id($nokb);
        $this->data['form'] = array(
           'noajustnk'=> array(
                    'placeholder' => 'No. Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'noajustnk',
                    'name'        => 'noajustnk',
                    'value'       => $this->val[0]['nokps'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglajustnk'=> array(
                    'placeholder' => 'Tgl Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'tglajustnk',
                    'name'        => 'tglajustnk',
                    'value'       => $this->val[0]['tglkps'],
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => $this->val[0]['noso'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => $this->val[0]['tglso'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => $this->val[0]['nodo'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => $this->val[0]['tgldo'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin/Rk',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => $this->val[0]['nosin'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'nora'=> array( 
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => $this->val[0]['nora'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'kdtipe'=> array(
                     'placeholder' => 'Tipe Unit',
                     'id'          => 'kdtipe',
                     'name'        => 'kdtipe',
                    'value'       => $this->val[0]['kdtipe'],
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'value'       => $this->val[0]['nmtipe'],
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna/Tahun',
                    'value'       => $this->val[0]['warna'],
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    'value'       => $this->val[0]['tahun'],
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'harga_beli'=> array(
                    'placeholder' => 'Harga Beli Unit',
                    'value'       => $this->val[0]['harga'],
                    'id'          => 'harga_beli',
                    'name'        => 'harga_beli',
                    'class'       => 'form-control',
                    'style'       => 'text-align: right',
                    // 'readonly'    => '',
            ),
            'nama'=> array(
                    'placeholder' => 'Nama',
                    'value'       => $this->val[0]['nama_s'],
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly'    => '',
            ),  
            'alamat'=> array(
                    'placeholder' => 'Alamat',
                    'value'       => $this->val[0]['alamat_s'],
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kel'=> array(
                     'placeholder' => 'Kel.',
                     'id'          => 'kel',
                     'name'        => 'kel',
                    'value'       => $this->val[0]['kel_s'],
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kec'=> array(
                     'placeholder' => 'Kec.',
                     'id'          => 'kec',
                     'name'        => 'kec',
                    'value'       => $this->val[0]['kec_s'],
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kota'     => array(
                    'placeholder' => 'Kota.',
                    //'type'        => 'hidden',
                    'attr'        => array(
                       'id'    => 'kota',
                       'class' => 'form-control',
                     ),
                    'data'        => $this->data['kota'],
                    'name'        => 'kota',
                    'value'       => $this->val[0]['kota_s'],
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly' => '',
            ), 
            'nohp'=> array(
                     'placeholder' => 'No HP',
                     'id'          => 'nohp',
                     'name'        => 'nohp',
                    'value'       => $this->val[0]['nohp_s'],
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'notelp'=> array(
                     'placeholder' => 'Telp.',
                     'id'          => 'notelp',
                     'name'        => 'notelp',
                    'value'       => $this->val[0]['notelp_s'],
                     'class'       => 'form-control',
                     'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'ket'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => '',
                    'type'        => 'hidden',
            ),
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

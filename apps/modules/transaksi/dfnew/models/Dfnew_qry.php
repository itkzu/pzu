<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dfnew_qry
 *
 * @author adi
 */
class Dfnew_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.get_divisi()");
        return $q->result_array();
    }

    public function getDetailPO() {
        $this->db->select("nopo, to_char(tglpo,'DD-MM-YYYY') AS tglpo, nap"
                . " , 0 as ndef, '' as nourut ");
        $nopo = $this->input->post('nopo');
        $kddiv = $this->input->post('kddiv');
        $this->db->where("kddiv",$kddiv);
        $this->db->where("nopo",$nopo);
        $this->db->where("nodf IS NULL");
        $q = $this->db->get("pzu.v_df_po");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    private function getdetail(){
        $output = array();
        $str = "SELECT
                      nodf
                    ,  nopo
                    , tglpo
                    , nap
                    , nint
                    , nsel
                    , total
                      FROM pzu.v_df_d";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
	    }
        return $output;
    }

    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('nodf', 'tgldf', 'tglrnb', 'nmdiv', 'nap', 'nint', 'total', 'ket');
    	$sIndexColumn = "nodf";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT nodf
                        , tgldf
                        , tglrnb
                        , nmdiv
                        , nap
                        , nint
                        , total
                        , ket
                        FROM pzu.vm_df WHERE nokb is null) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        
        if(empty($sOrder)){
            $sOrder = " order by nodf ";
        }
        

        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['nodf']==$value['nodf']){
                    $aRow['detail'][]= $value;
                }
            }

            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('dfnew/edit/'.str_replace('/','-',$aRow['nodf']))."\">Edit</a>";
            $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['nodf']."');\">Hapus</button>";

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['nodf']) ){
            if($_GET['nodf']){
                $nodf = $_GET['nodf'];
            }else{
                $nodf = '';
            }
        }else{
            $nodf = '';
        }

        $aColumns = array('nopo',
                          'tglpo',
                          'nap',
                          'nint',
                          'total',
                          'nodf',
                          'nourut');
	    $sIndexColumn = "nodf";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT nopo, nourut,
                          nap,
                          nint,
                          total,
                          nodf,
                          to_char(tglpo,'dd-mm-yyyy') as tglpo
                      FROM pzu.v_df_d
                      WHERE nodf = '".$nodf."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nodf']==$value['nodf']){
                    $aRow['detail'][]= $value;
                }
            }
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nodf']."','".$aRow['nourut']."','".$aRow['nopo']."','".$aRow['tglpo']."',
            '".$aRow['nap']."','".$aRow['nint']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['nodf']."','".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );

    }

    public function addDetail() {
        $nodf = $this->input->post('nodf');
        $nourut = $this->input->post('nourut');
        $nopo = $this->input->post('nopo');
        $tgldf = $this->apps->dateConvert($this->input->post('tgldf'));
        $ket = $this->input->post('ket');
        $kddiv = $this->input->post('kddiv');
        $nap = $this->input->post('nap');
        $nint = $this->input->post('nint');
      //  $nsel = $this->input->post('nsel');

        $q = $this->db->query("select nodf,title,msg,tipe from pzu.df_d_ins('" . $nodf . "'," . $nourut . ",'" . $tgldf . "','" .  $ket . "','" .  $kddiv . "','" . $nopo . "'," . $nint . ",'". $this->session->userdata('username') ."' )");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function detaildeleted() {
        $nodf = $this->input->post('nodf');
        $nourut = $this->input->post('nourut');
        $q = $this->db->query("select title,msg,tipe,rnodf from pzu.df_d_del('". $nodf ."',". $nourut . ",'". $this->session->userdata('username') ."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function delete() {
        $nodf = $this->input->post('nodf');
        $q = $this->db->query("select title,msg,tipe from pzu.df_del('". $nodf ."','". $this->session->userdata('username') ."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function submit() {
        $nodf = $this->input->post('nodf');
        $tgldf = $this->apps->dateConvert($this->input->post('tgldf'));
        $ket = $this->input->post('ket');
        $q = $this->db->query("select title,msg,tipe from pzu.df_ins('". $nodf ."','". $tgldf ."','".$this->session->userdata('data')['kddiv']."',0,'". $tgldf ."','". $ket ."','". $this->session->userdata('username') ."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

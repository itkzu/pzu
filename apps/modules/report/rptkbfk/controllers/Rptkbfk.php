<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptkbfk
 *
 * @author adi
 */
class Rptkbfk extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptkbfk/submit'),
            'add' => site_url('rptkbfk/add'),
            'edit' => site_url('rptkbfk/edit'),
            'reload' => site_url('rptkbfk'),
        );
        $this->load->model('rptkbfk_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->rptkbfk_qry->json_dgview();
    }
    
    public function gettgl() {
        echo $this->rptkbfk_qry->gettgl();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptkbfk_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode'=> array(
                    'placeholder' => 'Pilih Tanggal',
                    'id'          => 'periode',
                    'name'        => 'periode',
                    //'value'       => set_value('periode_awal'),
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ), 
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'periode',
                    'label' => 'Periode',
                    'rules' => 'required|max_length[20]',
                ), 
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

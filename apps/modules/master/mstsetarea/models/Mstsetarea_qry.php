<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstsetarea_qry
 *
 * @author adi
 */
class Mstsetarea_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->apps->kd_cabang;
    }
    public function getPos($kddiv = null) {
        if(!$kddiv){
            $kddiv = $this->session->userdata('data')['kddiv'];
        }
        $this->db->select("nmdiv,status,kddiv");
        $this->db->where("kddiv LIKE '".$kddiv."%'"); 
        $this->db->where("nmdiv LIKE '%%'"); 
        $this->db->where("status","POS"); 
        $this->db->where("faktif","true");
        $this->db->order_by("kddiv","ASC");
        $query = $this->db->get("mst.divisi");
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;  
    }
    
    public function select_data($param) { 
        $this->db->select("id, nama, area, kddiv");
        $this->db->where('id',$param);
        $query = $this->db->get('mstringarea');
        return $query->result_array();
    }
    
    public function getarea() {
        $this->db->select("kec_s");
        $this->db->order_by("kec_s","ASC");
        $this->db->group_by("kec_s");
        $query = $this->db->get('pzu.vl_do_real');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }
    
    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['id']) ){
            $id = $_GET['id'];
        }else{
            $id = '';
        }        
        
        $aColumns = array('nmdiv',
                            'nama',
                            'area',
                            'id',);
	$sIndexColumn = "id";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT nmdiv, nama, replace(area,',',' , ') as area, id
                        FROM
                        pzu.mstringarea
                        JOIN mst.divisi ON divisi.kddiv = mstringarea.kddiv
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
        //echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[3] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('mstsetarea/edit/'.$aRow['id'])."\">Edit</a>";
                $row[4] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['id']."');\">Hapus</button>";
		$output['aaData'][] = $row;
	}
	echo  json_encode( $output );  
    }
    
    public function submit() {
        try {
            $array = $this->input->post();  
            if(empty($array['stat'])){
                $array['area'] = implode(",", $array['area']);
            }
            if(empty($array['id'])){    
                unset($array['id']);
                //$array['nama'] = $this->getnamaarea();
                $resl = $this->db->insert('mstringarea',$array);
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && empty($array['stat'])){
                $this->db->where('id', $array['id']);
                $resl = $this->db->update('mstringarea', $array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && !empty($array['stat'])){                
                $this->db->where('id', $array['id']);
                $resl = $this->db->delete('mstringarea');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }             
            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

    public function getnamaarea() {
        $kddiv = $this->input->post("kddiv");
        $id = $this->input->post("id");
        $this->db->where('kddiv',$kddiv);
        if($id){
            // Jika mengedit data maka muncul data ring area sesuai dengan editnya
            $this->db->where('id',$id);
            $this->db->select("substring(nama,10,length(nama)) as kode");
            $q = $this->db->get("pzu.mstringarea");
            if($q->num_rows()>0){
                $res = $q->result_array();
                $kode = $res[0]['kode'];
            }else{
                $kode = 1;
            }

            $result = "RING AREA ".$kode;            
        }else{
            $this->db->select("max(substring(nama,10,length(nama))) as kode");
            $q = $this->db->get("pzu.mstringarea");
            if($q->num_rows()>0){
                $res = $q->result_array();
                $kode = $res[0]['kode'] + 1;
            }else{
                $kode = 1;
            }

            $result = "RING AREA ".$kode;            
        }
        return $result;
    }
}

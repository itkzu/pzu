<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Revbpkb_qry
 *
 * @author adi
 */
class Revbpkb_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getkota() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.bbn_tarif");
        return $q->result_array();
    }

    public function getDtbpkb() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nobpkb)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nama_s)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nosin)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama_s) like '".strtolower($q)."' then 1
                                when LOWER(nama_s) like '".strtolower($q)."%' then 2
                                when LOWER(nama_s) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama_s");
        $query = $this->db->get('pzu.v_bpkb_revisi');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['nobpkb'],
                    'text' => $value['nodo'],
                    'nosin' => $value['nosin'],
                    'nama' => $value['nama_s']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'nosin' => '',
                'nama' => 'nama', 
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 

    public function ctk($nokps) {
        $this->db->select("*");
        $this->db->where('nokps',$nokps);
        $q = $this->db->get("pzu.vb_kps");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function set_nosin() {
      $nosin = $this->input->post('nosin');
      $query = $this->db->query("select * from pzu.vm_kps where nosin = '" . $nosin . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function setbpkb() {
      $nobpkb = $this->input->post('nobpkb');
      $query = $this->db->query("select * from pzu.v_bpkb_revisi where nobpkb = '" . $nobpkb . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    } 

    public function submit() {      
        $nodo = $this->input->post('nodo');
        $ket = $this->input->post('ket');  
        $q = $this->db->query("select * from pzu.bpkb_rev_ins( '". $nodo ."',
                                                                        '". $ket ."', 
                                                                        '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

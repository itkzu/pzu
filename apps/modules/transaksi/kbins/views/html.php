  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style> 
      caption {
          padding-top: 1px;
          padding-bottom: 1px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto; 
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }

      tr#r1{
        border: 2px solid black;
      }

      td#r1{
        border: 2px solid black;
      }

      th#r1{
        border: 2px solid black;
      }

      /*table, td, th {
        border: 3px solid black;
      }*/
  </style>
  <?php 

      function bulan($tgl) {
        $year = substr($tgl,0,4);
        $month = substr($tgl,5,2);
        $day = substr($tgl,8,2);
        $temp = '';
        if ($month === '01') {
          $temp = "Januari";
        } else if ($month === '02') {
          $temp = "Februari";
        } else if ($month === '03') {
          $temp = "Maret";
        } else if ($month ==='04') {
          $temp = "April";
        } else if ($month === '05') {
          $temp = "Mei";
        } else if ($month === '06') {
          $temp = "Juni";
        } else if ($month === '07') {
          $temp = "Juli";
        } else if ($month === '08') {
          $temp = "Agustus";
        } else if ($month === '09') {
          $temp = "September";
        } else if ($month === '10') {
          $temp = "Oktober";
        } else if ($month === '12') {
          $temp = "November";
        } else if ($month === '13') {
          $temp = "Desember";
        }
        return $day.' '.$temp.' '.$year;
      } 

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 
  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $namaheader = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 22
                          , 'style' => 'text-align: left; width: 88%; font-size: 12px;'),    
      array('data' => '&nbsp;'.$data[0]['nocetak']
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 12%; font-size: 12px;'), 
  ); 
  $this->table->add_row($namaheader);   
  $tglheader = array( 
      array('data' => '&nbsp;'
                          , 'colspan' => 22
                          , 'style' => 'text-align: left; width: 88%; font-size: 12px;'),  
      array('data' => '&nbsp;'.date_format(date_create($data[0]['tglkb']),"d/m/Y")
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 12%; font-size: 12px;'),  
  );
  $this->table->add_row($tglheader);
  $empty = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),    
  );
  $this->table->add_row($empty);
  $empty = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),    
  );
  $this->table->add_row($empty);
  $empty = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),    
  );
  $this->table->add_row($empty);
  foreach ($data as $value) {

      $body = array(
          array('data' => '&nbsp;'.$value['nourut2']
                              , 'colspan' => 2
                              , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),   
          array('data' => '&nbsp;'.$value['darike']
                              , 'colspan' => 6
                              , 'style' => 'text-align: left; width: 24%; font-size: 12px;'),  
          array('data' => '&nbsp;'.$value['nofaktur']
                              , 'colspan' => 5
                              , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),  
          array('data' => '&nbsp;'.$value['ket_det']
                              , 'colspan' => 7
                              , 'style' => 'text-align: left; width: 28%; font-size: 12px;'),  
          array('data' => '&nbsp;'.number_format($value['nilai_det'],0,",",".")
                              , 'colspan' => 3
                              , 'style' => 'text-align: right; width: 12%; font-size: 12px;'),  
          array('data' => '&nbsp;'
                              , 'colspan' => 2
                              , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),   
      );
      $this->table->add_row($body);  
  }
  $total = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 20
                          , 'style' => 'text-align: left; width: 80%; font-size: 12px;'), 
          array('data' => '&nbsp;'.number_format($data[0]['nilai'],0,",",".")
                              , 'colspan' => 3
                              , 'style' => 'text-align: right; width: 12%; font-size: 12px;'),  
          array('data' => '&nbsp;'
                              , 'colspan' => 2
                              , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),     
  );
  $this->table->add_row($total);


  $empty = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),    
  );
  $this->table->add_row($empty);


  $footer = array(
      array('data' => '&nbsp;'.penyebut($data[0]['nilai'])
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; width: 100%; font-size: 12px;'),    
  );
  $this->table->add_row($footer);
  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();     
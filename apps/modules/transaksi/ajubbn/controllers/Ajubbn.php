<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Ajubbn
 *
 * @author adi
 */
class Ajubbn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('ajubbn/submit'),
            'add' => site_url('ajubbn/add'),
            'edit' => site_url('ajubbn/edit'),
            'ctk'      => site_url('ajubbn/ctk'),
            'reload' => site_url('ajubbn'),
        );
        $this->load->model('ajubbn_qry');
        $kota = $this->ajubbn_qry->getkota();
        foreach ($kota as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk() {
        $nokps = $this->uri->segment(3);   
        $this->data['data'] = $this->ajubbn_qry->ctk($nokps);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function getNoID() {
        echo $this->ajubbn_qry->getNoID();
    }

    public function set_noabj() {
        echo $this->ajubbn_qry->set_noabj();
    }

    public function set_nosin() {
        echo $this->ajubbn_qry->set_nosin();
    }

    public function submit() {
        echo $this->ajubbn_qry->submit();
    }

    public function tambah() {
        echo $this->ajubbn_qry->tambah();
    }

    public function batal() {
        echo $this->ajubbn_qry->batal();
    }

    public function delDetail() {
        echo $this->ajubbn_qry->delDetail();
    }

    public function json_dgview() {
        echo $this->ajubbn_qry->json_dgview();
    }

    public function json_dgview_det() {
        echo $this->ajubbn_qry->json_dgview_det();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
           'noabj'=> array(
                    'placeholder' => 'No. Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'noabj',
                    'name'        => 'noabj',
                    'value'       => set_value('noabj'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglabj'=> array(
                    'placeholder' => 'Tgl Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'tglabj',
                    'name'        => 'tglabj',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
        );
    }
     private function _init_edit($no = null){
        if(!$no){
            $nokb = $this->uri->segment(3);
        }
        $this->_check_id($nokb);
        $this->data['form'] = array(
           'noajustnk'=> array(
                    'placeholder' => 'No. Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'noajustnk',
                    'name'        => 'noajustnk',
                    'value'       => $this->val[0]['nokps'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglajustnk'=> array(
                    'placeholder' => 'Tgl Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'tglajustnk',
                    'name'        => 'tglajustnk',
                    'value'       => $this->val[0]['tglkps'],
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

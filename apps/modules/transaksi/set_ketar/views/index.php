<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">
          <div class="col-xs-1">
          </div>
          <div class="col-xs-2 box-header box-view">
            <button type="button" class="btn btn-primary btn-edit">Ubah</button>
              <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
          </div>
          <div class="col-xs-12 box-header box-new">
            <button type="button" class="btn btn-primary btn-save">Simpan</button>
            <button type="button" class="btn btn-danger btn-cancel">Batal</button>
          </div>

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-5 ">
                                  <div class="form-group">
                                      <?php echo form_label($form['nodo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['nodo']);
                                          echo form_error('nodo','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['noso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['noso']);
                                        echo form_error('noso','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-5 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['tgldo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['tgldo']);
                                        echo form_error('tgldo','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                    <?php echo form_label($form['tglso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['tglso']);
                                        echo form_error('tglso','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-6">
                          <label>&nbsp;</label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                      <div class="col-xs-6">
                          <label> Identitas Pemesan </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3">
                                    <?php echo "<b>No. Mesin/RK</b>"; ?>
                              </div>

                              <div class="col-xs-4">
                                    <?php
                                        echo form_input($form['nosin']);
                                        echo form_error('nosin','<div class="note">','</div>');
                                    ?>
                              </div>

                              <div class="col-xs-5">
                                    <?php
                                        echo form_input($form['nora']);
                                        echo form_error('nora','<div class="note">','</div>');
                                    ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">

                              <div class="col-xs-3 ">
                                    <?php echo form_label($form['nama_p']['placeholder']); ?>
                              </div>

                              <div class="col-xs-9">
                                    <?php
                                        echo form_input($form['nama_p']);
                                        echo form_error('nama_p','<div class="note">','</div>');
                                    ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                    <?php echo form_label($form['kdtipe']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                    <?php
                                        echo form_input($form['kdtipe']);
                                        echo form_error('kdtipe','<div class="note">','</div>');
                                    ?>
                              </div>

                              <div class="col-xs-5">
                                    <?php
                                        echo form_input($form['nmtipe']);
                                        echo form_error('nmtipe','<div class="note">','</div>');
                                    ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">

                              <div class="col-xs-3 ">
                                    <?php echo form_label($form['nama_s']['placeholder']); ?>
                              </div>

                              <div class="col-xs-9">
                                    <?php
                                        echo form_input($form['nama_s']);
                                        echo form_error('nama_s','<div class="note">','</div>');
                                    ?>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo "<b>Warna / Tahun</b>"; ?>
                                  </div>
                              </div>

                              <div class="col-xs-6">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['warna']);
                                        echo form_error('warna','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>

                              <div class="col-xs-3">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['tahun']);
                                        echo form_error('tahun','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['alamat_k']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['alamat_k']);
                                        echo form_error('alamat_k','<div class="note">','</div>');
                                    ?>
                                    <div>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                    <?php echo form_label($form['nmleasing']['placeholder']); ?>
                              </div>

                              <div class="col-xs-6">
                                    <?php
                                        echo form_input($form['nmleasing']);
                                        echo form_error('nmleasing','<div class="note">','</div>');
                                    ?>
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                    <?php echo form_label($form['harga_otr']['placeholder']); ?>
                              </div>

                              <div class="col-xs-4">
                                    <?php
                                        echo form_input($form['harga_otr']);
                                        echo form_error('harga_otr','<div class="note">','</div>');
                                    ?>
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                  <div class="form-group">
                                    <?php echo form_label($form['um']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['um']);
                                        echo form_error('um','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-6">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                      <div class="col-xs-6">
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6 ">
                          <div class="row">

                              <div class="col-xs-3 ">
                                    <?php echo form_label($form['ket_ar']['placeholder']); ?>
                              </div>

                              <div class="col-xs-9">
                                    <?php
                                        echo form_input($form['ket_ar']);
                                        echo form_error('ket_ar','<div class="note">','</div>');
                                    ?>
                              </div>

                          </div>
                        </div>

                    </div>
                  </div>


              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#ket").val('0');
        // $("#npwp").mask("s99-999999");
        clear();
        autonum();
        disabled();
        set_kode();

        $('#nodo').change(function () {
            set_nodo();
        });

        $('.btn-batal').click(function () {
            clear();
            disabled();
        });

        $('.btn-edit').click(function () {
          $("#ket").val('2');
            enabled();
            $("#nodo").attr('disabled',true); 
        });

        $('.btn-save').click(function () {
            ubah_ketdo();
        });

        $('.btn-cancel').click(function () {
          swal({
              title: "Input data akan dibatalkan!",
              text: "Data tidak akan Tersimpan!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#c9302c",
              confirmButtonText: "Ya, Lanjutkan!",
              cancelButtonText: "Batalkan!",
              closeOnConfirm: false
          }, function () {
            window.location.reload();
          });
        });
    });

    function disabled(){
      $(".box-new").hide();
      $(".box-view").show();
      $("#ket_ar").attr("disabled",true);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){
      $(".box-view").hide();
      $(".box-new").show();
      $("#ket_ar").attr("disabled",false);
      $(".btn-edit").prop('disabled',true);
      // $(".btn-batal").show();
    }

    function clear(){
      $("#nodo").val('');
      $("#noso").val('');
      $("#nosin").val('');
      $("#nora").val('');
      $("#kdtipe").val('');
      $("#nmtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#nmleasing").val('');
      $("#harga_otr").val('');
      $("#um").val('');
      $("#ket_ar").val('');
      $("#nama_p").val('');
      $("#nama_s").val('');
      $("#alamat_k").val('');
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
    }
    function autonum(){
      $('#harga_otr').autoNumeric('init',{ currencySymbol : 'Rp.'});
      $('#um').autoNumeric('init',{ currencySymbol : 'Rp.'});
    }

    function set_kode(){
        var tg = new Date();
        var tgl = tg.toString();
        var tanggal = tgl.substring(13,15);
        $.ajax({
            type: "POST",
            url: "<?=site_url("set_ketar/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#nodo").mask(data.kode+"99-999999");
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_nodo(){

      var nodo = $("#nodo").val();
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("set_ketar/set_nodo");?>",
            data: {"nodo":nodo },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#noso").val(data.noso);
                      $("#nosin").val(data.nosin2);
                      $("#nora").val(data.nora2);
                      $("#kdtipe").val(data.kdtipex);
                      $("#nmtipe").val(data.nmtipe);
                      $("#warna").val(data.warna);
                      $("#tahun").val(data.tahun);
                      $("#nama_p").val(data.nama);
                      $("#alamat_k").val(data.alamatktp_s2);
                      $("#nama_s").val(data.nama_s);
                      $("#nmleasing").val(data.nmleasingx);
                      $('#harga_otr').autoNumeric('set',data.harga_otr);
                      $('#um').autoNumeric('set',data.um);
                      $("#ket_ar").val(data.ket_ar);
                      $(".btn-edit").attr('disabled',false);
                      $(".btn-batal").show();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function ubah_ketdo(){
      var nodo = $("#nodo").val();
      var nodo = nodo.toUpperCase();
      var ket_ar = $("#ket_ar").val();
      var ket_ar = ket_ar.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("set_ketar/ubah_ketdo");?>",
            data: {"nodo":nodo ,"ket_ar":ket_ar },
            success: function(resp){
              if (resp='success'){
                    swal({
                        title: "Data Berhasil Diubah",
                        text: "Data Berhasil",
                        type: "success"
                    }, function(){
                        window.location.reload();
                    });
              } else {
                    swal({
                        title: "Data Gagal Dirubah",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
</script>

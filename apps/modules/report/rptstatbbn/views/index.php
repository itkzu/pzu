<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<div class="row">


					<div class="col-xs-12">
						<div class="form-group has-error">
							<?php
							echo form_label('Masukkan Kata Kunci <small><i>( Nama Pemilik / No. Polisi / No. DO / No. BPKB )</i></small>');
							echo form_dropdown($form['nama']['name']
								,$form['nama']['data']
								,$form['nama']['value']
								,$form['nama']['attr']);
							echo form_error('nama','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<?php
							echo form_label($form['alamat']['placeholder']);
							echo form_textarea($form['alamat']);
							echo form_error('alamat','<div class="note">','</div>');
							?>
						</div>
					</div>





					<div class="col-xs-12">
						<div class="row">

							<div class="col-xs-6 col-md-6 ">
								<div class="row">

									<div class="col-xs-12">
										<div class="form-group">
											<?php
											echo form_label($form['pemilik']['placeholder']);
											echo form_input($form['pemilik']);
											echo form_error('pemilik','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['nodo']['placeholder']);
											echo form_input($form['nodo']);
											echo form_error('nodo','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['nmtipe']['placeholder']);
											echo form_input($form['nmtipe']);
											echo form_error('nmtipe','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['nmsales']['placeholder']);
											echo form_input($form['nmsales']);
											echo form_error('nmsales','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="form-group">
											<?php
											echo form_label($form['nopol']['placeholder']);
											echo form_input($form['nopol']);
											echo form_error('nopol','<div class="note">','</div>');
											?>
										</div>
									</div>

								</div>
							</div>



							<div class="col-xs-6 col-md-6 ">
								<div class="row">

									<div class="col-xs-12">
										<div class="form-group">
											<?php
											echo form_label($form['nohp']['placeholder']);
											echo form_input($form['nohp']);
											echo form_error('nohp','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="form-group">
											<?php
											echo form_label($form['tgldo']['placeholder']);
											echo form_input($form['tgldo']);
											echo form_error('tgldo','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['jnsbayarx']['placeholder']);
											echo form_input($form['jnsbayarx']);
											echo form_error('jnsbayarx','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="form-group">
											<?php
											echo form_label($form['status_otrx']['placeholder']);
											echo form_input($form['status_otrx']);
											echo form_error('status_otrx','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12">
										<div class="form-group">
											<?php
											echo form_label($form['nobpkb']['placeholder']);
											echo form_input($form['nobpkb']);
											echo form_error('nobpkb','<div class="note">','</div>');
											?>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>


					<div class="col-xs-12">
						<hr>
					</div>



					<div class="col-xs-12">
						<div class="row">

							<div class="col-xs-6 col-md-6 ">
								<div class="row">

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_trm_notis']['placeholder']);
											echo form_input($form['tgl_trm_notis']);
											echo form_error('tgl_trm_notis','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_trm_stnk']['placeholder']);
											echo form_input($form['tgl_trm_stnk']);
											echo form_error('tgl_trm_stnk','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_trm_tnkb']['placeholder']);
											echo form_input($form['tgl_trm_tnkb']);
											echo form_error('tgl_trm_tnkb','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_trm_bpkb']['placeholder']);
											echo form_input($form['tgl_trm_bpkb']);
											echo form_error('tgl_trm_bpkb','<div class="note">','</div>');
											?>
										</div>
									</div>
								</div>
							</div>



							<div class="col-xs-6 col-md-6 ">
								<div class="row">

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_srh_notis']['placeholder']);
											echo form_input($form['tgl_srh_notis']);
											echo form_error('tgl_srh_notis','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_srh_stnk']['placeholder']);
											echo form_input($form['tgl_srh_stnk']);
											echo form_error('tgl_srh_stnk','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_srh_tnkb']['placeholder']);
											echo form_input($form['tgl_srh_tnkb']);
											echo form_error('tgl_srh_tnkb','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-xs-12 ">
										<div class="form-group">
											<?php
											echo form_label($form['tgl_srh_bpkb']['placeholder']);
											echo form_input($form['tgl_srh_bpkb']);
											echo form_error('tgl_srh_bpkb','<div class="note">','</div>');
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">
$(document).ready(function () {
	init_nama();
	$("#nama").change(function(){
		getData();
	});
});
function getData(){
	var nama = $("#nama").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptstatbbn/getData");?>",
		data: {"nama":nama},
		beforeSend: function() {

		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				$("#pemilik").val(value.nama);
				$("#nohp").val(value.nohp);
				$("#nopol").val(value.nopol);
				$("#nobpkb").val(value.nobpkb);
				$("#nmtipe").val(value.nmtipe);
				$("#nodo").val(value.nodo);


				$("#tgldo").val(moment(value.tgldo).format('LL'));
										//$("#tgldo").val(value.tgldo);
										$("#alamat").val(value.alamat2);

										$("#jnsbayarx").val(value.jnsbayarx);
										$("#nmsales").val(value.nmsales);
										$("#status_otrx").val(value.status_otrx);

										$("#tgl_srh_bpkb").val(moment(value.tgl_srh_bpkb).format('LL'));
										setBgColour(value.tgl_srh_bpkb,"tgl_srh_bpkb");

										$("#tgl_srh_notis").val(moment(value.tgl_srh_notis).format('LL'));
										setBgColour(value.tgl_srh_notis,"tgl_srh_notis");

										$("#tgl_srh_stnk").val(moment(value.tgl_srh_stnk).format('LL'));
										setBgColour(value.tgl_srh_stnk,"tgl_srh_stnk");

										$("#tgl_srh_tnkb").val(moment(value.tgl_srh_tnkb).format('LL'));
										setBgColour(value.tgl_srh_tnkb,"tgl_srh_tnkb");

										$("#tgl_trm_bpkb").val(moment(value.tgl_trm_bpkb).format('LL'));
										setBgColour(value.tgl_trm_bpkb,"tgl_trm_bpkb");

										$("#tgl_trm_notis").val(moment(value.tgl_trm_notis).format('LL'));
										setBgColour(value.tgl_trm_notis,"tgl_trm_notis");

										$("#tgl_trm_stnk").val(moment(value.tgl_trm_stnk).format('LL'));
										setBgColour(value.tgl_trm_stnk,"tgl_trm_stnk");

										$("#tgl_trm_tnkb").val(moment(value.tgl_trm_tnkb).format('LL'));
										setBgColour(value.tgl_trm_tnkb,"tgl_trm_tnkb");
									});
},
error:function(event, textStatus, errorThrown) {
	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
}
});
}

function setBgColour(val,object){
	if(val){
		$("#"+object).css("background-color", "#fff");
	}else{
		$("#"+object).css("background-color", "#eee");
	}
}

function init_nama(){
	$("#nama").select2({
		ajax: {
			url: "<?=site_url('rptstatbbn/getNama');?>",
			type: 'post',
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term,
					page: params.page
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total_count
					}
				};
			},
			cache: true
		},
		placeholder: 'Masukkan Nama Pemilik ...',
		dropdownAutoWidth : false,
					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					minimumInputLength: 3,
					templateResult: format_nama, // omitted for brevity, see the source of this page
					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
				});
}

function format_nama_terpilih (repo) {
	return repo.full_name || repo.text;
}

function format_nama (repo) {
	if (repo.loading) return "Mencari data ... ";


	var markup = "<div class='select2-result-repository clearfix'>" +
	"<div class='select2-result-repository__meta'>" +
	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

	if (repo.alamat) {
		markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.alamat + " || " + repo.kel + "</div>";
	}

	markup += "<div class='select2-result-repository__statistics'>" +
	"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nobpkb + " | <i class=\"fa fa-tag\"></i> " + repo.nopol + " | <i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " </div>" +
	"<div class='select2-result-repository__stargazers'> <i class=\"fa fa-file-o\"></i> " + repo.nodo + " | <i class=\"fa fa-calendar\"></i> " + repo.tgldo + "</div>" +
	"</div>" +
	"</div>" +
	"</div>";
	return markup;
}
</script>

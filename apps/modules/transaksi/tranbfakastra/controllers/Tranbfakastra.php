<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Tranbfakastra
 *
 * @author adi
 */
class Tranbfakastra extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('tranbfakastra/submit'),
            'add' => site_url('tranbfakastra/add'),
            'edit' => site_url('tranbfakastra/edit'),
            'reload' => site_url('tranbfakastra'),
        );
        $this->load->model('tranbfakastra_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function getData() {
        echo $this->tranbfakastra_qry->getData();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->tranbfakastra_qry->submit();
            echo $res;
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'nodo'=> array(
                    'placeholder' => 'Nomor DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'iddo'=> array(
                    'placeholder' => 'ID DO',
                    'type'        => 'hidden',
                    'id'          => 'iddo',
                    'name'        => 'iddo',
                    'value'       => set_value('iddo'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => set_value('tgldo'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Nama Konsumen',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'alamat'=> array(
                    'placeholder' => 'Alamat Konsumen',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    'style'       => 'min-height: 108px;height: 108px; resize: vertical;'
            ),
           'kel'=> array(
                    'placeholder' => 'Kelurahan',
                    'id'          => 'kel',
                    'name'        => 'kel',
                    'value'       => set_value('kel'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'kec'=> array(
                    'placeholder' => 'Kecamatan',
                    'id'          => 'kec',
                    'name'        => 'kec',
                    'value'       => set_value('kec'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'kota'=> array(
                    'placeholder' => 'Kota',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'nohp'=> array(
                    'placeholder' => 'No. HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'kdtipe'=> array(
                    'placeholder' => 'Kode Tipe Unit',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe Unit',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
            
        // nodo, tgldo, nama, alamat, kel, kec, kota, nohp, kdtipe, nmtipe, nosin, nora
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'nodo',
                    'label' => 'No. DO',
                    'rules' => 'required|max_length[10]',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

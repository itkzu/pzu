<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('Pilih Cabang');?>
                            <div class="input-group">
                                <?php
                                    echo form_dropdown($form['cabang']['name'],$form['cabang']['data'] ,$form['cabang']['value'] ,$form['cabang']['attr']);
                                    echo form_error('cabang','<div class="note">','</div>'); 
                                ?>
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-info btn-flat btn-show">Tampilkan</button>
                                </span>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-stock">
                                <caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-table-stock">
                                    DATA STOK UNIT
                                </caption>
                                <thead>
                                    <tr>
                                        <th style="width:10px;">Kode</th>
                                        <th style="width:100px;">Kode Tipe</th>
                                        <th style="">Nama Tipe</th>
                                        <th style="width:10px;">Kode Warna</th>
                                        <th style="width:100px;">Nama Warna</th>
                                        <th style="width:10px;">Tgl Jual Akhir</th>
                                        <th style="">Ket Jual Akhir</th>
                                        <th style="width:10px;">Harga Jual OTR</th>
                                        <th style="width:10px;">Jumlah Stok</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="width:10px;">Kode</th>
                                        <th style="width:100px;">Kode Tipe</th>
                                        <th style="">Nama Tipe</th>
                                        <th style="width:10px;">Kode Warna</th>
                                        <th style="width:100px;">Nama Warna</th>
                                        <th style="width:10px;">Tgl Jual Akhir</th>
                                        <th style="">Ket Jual Akhir</th>
                                        <th style="width:10px;">Harga Jual OTR</th>
                                        <th style="width:10px;">Jumlah Stok</th>
                                    </tr>
                                </tfoot>
                                <tbody class="body-table-stock"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        table = $(".table-stock").DataTable();
        $(".btn-show").click(function(){
            getCabang();
        });
    });
    function getCabang(){
        var cabang = $("#cabang").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("android/getStokAllUnit");?>",
            data: {"request":"true","url":cabang},          
            beforeSend: function() {
                $(".btn-show").attr("disabled",true);
                if ( $.fn.DataTable.isDataTable('.table-stock') ) {
                  $('.table-stock').DataTable().destroy();
                  $(".body-table-stock").html('');
                }     
            }, 
            success: function(resp){   
                try{
                    $(".btn-show").attr("disabled",false);
                    var obj = jQuery.parseJSON(resp);
                    var no = 1;
                    $.each(obj, function(key, data){
                        if(Number(data.jml_stok)!==0){
                            $(".body-table-stock").append('<tr class="row-gp-'+no+'" style="font-size: 12px;"></tr>');
                        }else{
                            $(".body-table-stock").append('<tr class="row-gp-'+no+'" style="font-size: 12px;color: #ff6363;"></tr>');
                        }
                        $.each(data, function(key, val){
                            $(".row-gp-"+no).append('<td>'+val+'</tr>');
                        });           
                        no++;
                    });
                    var column = [];
                    for (i = 7; i <= 7; i++) { 
                        column.push({ 
                            "aTargets": [ i ],
                            "mRender": function (data, type, full) {
                                return type === 'export' ? data : numeral(data).format('0,0.00');
                                } 
                            });
                    }   

                    table = $(".table-stock").DataTable({
                          "aoColumnDefs": column,
                          "lengthMenu": [[-1], ["Semua Data"]],
                          "bProcessing": false,
                          "bServerSide": false,
                          "bDestroy": true,
                          "bAutoWidth": false,
                            buttons: [
                                {extend: 'copy',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'csv',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'excel',
                                    exportOptions: {orthogonal: 'export'}},
                                {extend: 'pdf', 
                                    orientation: 'landscape',
                                    pageSize: 'A0'
                                },
                                {extend: 'print',
                                    customize: function (win){
                                           $(win.document.body).addClass('white-bg');
                                           $(win.document.body).css('font-size', '10px');
                                           $(win.document.body).find('table')
                                                   .addClass('compact')
                                                   .css('font-size', 'inherit');
                                   }
                                }
                            ],
                          "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right '> B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
                      });                    
                    $('.table-stock').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });

                    $('.table-stock tfoot th').each( function () {
                        var title = $('.dataTable thead th').eq( $(this).index() ).text();
                        if(title!=="Edit" && title!=="Delete" ){
                            $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
                        }else{
                            $(this).html( '' );
                        }
                    } );

                    table.columns().every( function () {
                        var that = this;
                        $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                            //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                                that
                                    .search( this.value )
                                    .draw();
                            //}
                        } );
                    });   
                }
                catch(e){
                    swal("Error !", resp, "error");
                }               
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                $(".btn-show").attr("disabled",false);
            }
        });
    }
</script>
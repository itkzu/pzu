<?php

/*
 * ***************************************************************
 * Script : pdf
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto;
    }
</style>
<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();

$template = array(
        'table_open'            => '<table class="table-responsive" style="padding: 0px 2px;min-width: 850px;border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);

$this->load->library('table');
// $dec = explode('',$decode);
$caption = "<b>LAPORAN HARIAN KAS/BANK BENGKEL</b>"
        . "<br>"
        . "<b>". strtoupper($this->apps->title)." - ".$this->cabbkl[0]['nmcabang']."</b>"
        . "<br>"
        . "<b>Rekening : ". $decode[0]['nmkb']."  </b>"
        . "<br>"
        . "<b> Tanggal : " .$array['periode_awal']." </b>"
        . "<br><br>";

// Caption text
$this->table->set_caption($caption);

$namaheader = array(
    array('data' => 'BUKTI'
                        , 'align' => 'center'
                        , 'width' => '10px'
                        , 'style' => ' width: 15px; font-size: 12px;'),
    array('data' => 'DARI/KE'
                        , 'align' => 'center'
                        , 'style' => 'width: 200px; font-size: 12px;'),
    array('data' => 'NO. BG/CEK'
                        , 'align' => 'center'
                        , 'style' => 'width: 10px; font-size: 12px;'),
    array('data' => 'KETERANGAN'
                        , 'align' => 'center'
                        , 'style' => ' width: 200px; font-size: 12px;'),
    array('data' => 'DEBET'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
    array('data' => 'KREDIT'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
    array('data' => 'SALDO'
                        , 'align' => 'center'
                        , 'style' => ' width: 90px; font-size: 12px;'),
);
$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
// Caption text
//$this->table->set_caption($caption);
$this->table->add_row($namaheader);
$spasi = '&nbsp;';
$res = array();

$saldo = 0;
foreach ($decode as $data) {

  $saldo = $saldo + ($data['so_awal']+$data['debet'] - $data['kredit']);
        $akun_d = array(
            array('data' => $spasi.$data['nocetak']
                                , 'style' => 'text-align: left; font-size: 12px;'),
            array('data' => $spasi.$data['drkpd']
                                , 'style' => 'text-align: left; font-size: 12px;'),
            array('data' => $spasi.$data['nofaktur']
                                , 'style' => 'text-align: left; font-size: 12px;'),
            array('data' => $spasi.$data['ket']
                                , 'style' => 'text-align: left; font-size: 12px;'),
            array('data' => number_format($data['debet'], 0,',','.').$spasi
                                , 'style' => 'text-align: right; font-size: 12px;'),
            array('data' => number_format($data['kredit'], 0,',','.').$spasi
                                , 'style' => 'text-align: right; font-size: 12px;'),
            array('data' => number_format($saldo, 0,',','.').$spasi
                                , 'style' => 'text-align: right; font-size: 12px;'),
        );
        $this->table->add_row($akun_d);
    }
$total =  $jml[0]['debet'] + $jml[0]['saldo'] - $jml[0]['kredit'];
$g_total = array(
    array('data' => '<b>&nbsp; GRAND TOTAL</b>'
                        , 'colspan' => '4'
                        , 'style' => 'text-align: left; font-size: 12px;'),
    array('data' => '<b>'.number_format($jml[0]['debet'], 0,',','.').$spasi.'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' => '<b>'.number_format($jml[0]['kredit'], 0,',','.').$spasi.'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
    array('data' => '<b>'.number_format($total, 0,',','.').$spasi.'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'),
);
$this->table->add_row($g_total);

$desc_1 = array(
    array('data' => '&nbsp;'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => 'DIPERIKSA OLEH'
                        , 'colspan' => '2'
                        , 'style' => 'text-align: center; font-size: 12px;border: none;'),
    array('data' => 'MENGETAHUI'
                        , 'colspan' => '3'
                        , 'style' => 'text-align: CENTER; font-size: 12px;border: none;'),
    array('data' => 'DIBUAT OLEH'
                        , 'colspan' => '5'
                        , 'style' => 'text-align: center; font-size: 12px;border: none;'),
);
$this->table->add_row($desc_1);
for($i=1;$i<=5;$i++){
  $this->table->add_row(array(
        array('data' => '&nbsp;'
                            , 'colspan' => '11'
                            , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    ));
}
$desc_2 = array(
    array('data' => '&nbsp;'
                        , 'style' => 'text-align: left; font-size: 12px;border: none;'),
    array('data' => $this->cabbkl[0]['nmadh']
                        , 'colspan' => '2'
                        , 'style' => 'text-align: center; font-size: 12px;border: none;'),
    array('data' => $this->cabbkl[0]['nmkacab']
                        , 'colspan' => '3'
                        , 'style' => 'text-align: center; font-size: 12px;border: none;'),
    array('data' => $this->cabbkl[0]['nmkasir']
                        , 'colspan' => '5'
                        , 'style' => 'text-align: center; font-size: 12px;border: none;'),
);
$this->table->add_row($desc_2);

$this->table->set_template($template);
echo $this->table->generate();

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Srhbpkb
 *
 * @author adi
 */
class Srhbpkb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('srhbpkb/submit'),
            'add' => site_url('srhbpkb/add'),
            'edit' => site_url('srhbpkb/edit'),
            'ctk'      => site_url('srhbpkb/ctk'),
            'reload' => site_url('srhbpkb'),
        );
        $this->load->model('srhbpkb_qry');
        $kota = $this->srhbpkb_qry->getleas();
        foreach ($kota as $value) {
            $this->data['kdleasing'][$value['kdleasing']] = $value['kdleasing'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk() {
        $nokps = $this->uri->segment(3);   
        $this->data['data'] = $this->srhbpkb_qry->ctk($nokps);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function getNoID() {
        echo $this->srhbpkb_qry->getNoID();
    }

    public function set_nosrh() {
        echo $this->srhbpkb_qry->set_nosrh();
    }

    public function set_nosin() {
        echo $this->srhbpkb_qry->set_nosin();
    }

    public function submit() {
        echo $this->srhbpkb_qry->submit();
    }

    public function tambah() {
        echo $this->srhbpkb_qry->tambah();
    }

    public function batal() {
        echo $this->srhbpkb_qry->batal();
    }

    public function delDetail() {
        echo $this->srhbpkb_qry->delDetail();
    }

    public function json_dgview() {
        echo $this->srhbpkb_qry->json_dgview();
    }

    public function json_dgview_det() {
        echo $this->srhbpkb_qry->json_dgview_det();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
            'nosrh'=> array(
                    'placeholder' => 'No. Penyerahan',
                    //'type'        => 'hidden',
                    'id'          => 'nosrh',
                    'name'        => 'nosrh',
                    'value'       => set_value('nosrh'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'tglsrh'=> array(
                    'placeholder' => 'Tgl Penyerahan',
                    //'type'        => 'hidden',
                    'id'          => 'tglsrh',
                    'name'        => 'tglsrh',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'kdleasing' => array(
                  'placeholder' => 'Leasing',
                  'attr'        => array(
                      'id'      => 'kdleasing',
                      'class'   => 'form-control',
                  ),
                  'data'        => $this->data['kdleasing'],
                  'class'       => 'form-control',
                  'value'       => set_value('kdleasing'),
                  'name'        => 'kdleasing',
                    // 'readonly' => '',
            ),
        );
    }
     private function _init_edit($no = null){
        if(!$no){
            $nokb = $this->uri->segment(3);
        }
        $this->_check_id($nokb);
        $this->data['form'] = array(
           'noajustnk'=> array(
                    'placeholder' => 'No. Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'noajustnk',
                    'name'        => 'noajustnk',
                    'value'       => $this->val[0]['nokps'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglajustnk'=> array(
                    'placeholder' => 'Tgl Pengajuan',
                    //'type'        => 'hidden',
                    'id'          => 'tglajustnk',
                    'name'        => 'tglajustnk',
                    'value'       => $this->val[0]['tglkps'],
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'kdleasing' => array(
                  'placeholder' => 'Leasing',
                  'attr'        => array(
                      'id'      => 'kdleasing',
                      'class'   => 'form-control',
                  ),
                  'data'        => $this->data['kdleasing'],
                  'class'       => 'form-control',
                  'value'       => set_value('kdleasing'),
                  'name'        => 'kdleasing',
                    // 'readonly' => '',
            ),
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

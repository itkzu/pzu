<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Tkbbkl_qry
 *
 * @author adi
 */
class Tkbbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataRek() {
      $kddiv = $this->session->userdata('data')['kddiv'];
      if ($kddiv==="ZPH01.02S") {
        $kddiv = "";
      } else {
        $kddiv = "where kddiv ='".$this->session->userdata('data')['kddiv']."'  and now()::date >= tglkb::date";
      }
        $q = $this->db->query("select kdkb, nmkb, tglkb from bkl.v_kasbank ".$kddiv."");
        return $q->result_array();
    }

    public function getKdRef() {
        $jnstrx = $this->input->post('jnstrx');
          $kddiv = $this->session->userdata('data')['kddiv'];
          if ($kddiv==="ZPH01.02S") {
            $kddiv = "";
          } else {
            $kddiv = "where kddiv = substring('".$this->session->userdata('data')['kddiv']."',0,10)";
          }
        $q = $this->db->query("select kdrefkb, nmrefkb from bkl.v_refkb ".$kddiv." and dk = '".$jnstrx."'");
                // echo $this->db->last_query();
        $res = $q->result_array();
        return json_encode($res);
    }

    public function getDataTrx() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.v_kasbank_trx");
        return $q->result_array();
    }

    public function tglkb() {
      $kddiv = $this->session->userdata('data')['kddiv'];
      if ($kddiv==="ZPH01.02S") {
        $kddiv = "";
      } else {
        $kddiv = "and kddiv ='".$this->session->userdata('data')['kddiv']."'";
      }
        $kdkb = $this->input->post('kdkb');
        $q = $this->db->query("select kdkb, nmkb, tglkb from bkl.v_kasbank where kdkb = '".$kdkb."' ".$kddiv."");
        $res = $q->result_array();
        return json_encode($res);
    }

    public function cetak($nokb) {
        $this->db->select("nocetak,nocetakx,nokb,tglkb,kdkb,nmkb,jenis,dk,ket,nilai_det,nilai,nmrefkb,nofaktur,darike,ket_det,nourut,kode");
        $this->db->where('nokb',$nokb);
        $this->db->order_by('nourut asc');
        $q = $this->db->get("bkl.vb_kb");
        $res = $q->result_array();
        return $res;
    }

    public function sum($nokb) {
        $this->db->select("sum(nilai_det) as tot_nilai,nocetak,tglkb,nokb, count(nokb) as jml");
        $this->db->where('nokb',$nokb);
        $this->db->group_by('nocetak');
        $this->db->group_by('tglkb');
        $this->db->group_by('nokb');
        $q = $this->db->get("bkl.vb_kb");
        $res = $q->result_array();
        return $res;
    }

    public function select_data() {
        $nokb = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->where('nokb',$nokb);
        $q = $this->db->get("bkl.vm_tkb");
        $res = $q->result_array();
        return $res;
    }

    private function getdetail(){
        $output = array();
        $str = "SELECT  nokb,
                        jnstrx,
                        nmrefkb,
                        darike,
                        nofaktur,
                        ket,
                        nilai,
                        nourut
                         FROM bkl.v_tkb_d order by nourut asc";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
	    }
        return $output;
    }

    private function getdetail2(){
        $output2 = array();
        $str2 = "SELECT  noaksi,sum(total) as tot_all
                        FROM pzu.v_aksi_d group by noaksi";
        $q2 = $this->db->query($str2);
        $res2 = $q2->result_array();

        foreach ( $res2 as $aRow2 ){
            foreach ($aRow2 as $key2 => $value2) {
                if(is_numeric($value2)){
                    $aRow2[$key2] = (float) $value2;
                }else{
                    $aRow2[$key2] = $value2;
                }
            }
           $output2[] = $aRow2;
	    }
        return $output2;
    }

    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('nmkb', 'nokb', 'tglkb', 'nocetak', 'jnstrx', 'nominal');
    	$sIndexColumn = "nokb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT nmkb
                              , nokb
                              , tglkb
                              , nocetak
                              , jnstrx
                              , nominal
                              FROM bkl.vm_tkb where kddiv = '".$this->session->userdata('data')['kddiv']."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "  ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        // $detail2 = $this->getdetail2();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['nokb']==$value['nokb']){
                    $aRow['detail'][]= $value;
                }
            }

            // $aRow['detail2'] = array();
            //
            // foreach ($detail2 as $value) {
            //     if($aRow['noaksi']==$value['noaksi']){
            //         $aRow['detail2'][]= $value;
            //     }
            // }

            $aRow['cetak'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('tkbbkl/submit/'.$aRow['nokb'])."\">Cetak</a>";
            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('tkbbkl/edit/'.$aRow['nokb'])."\">Edit</a>";
            $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['nokb']."');\">Hapus</button>";
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['nokb']) ){
            if($_GET['nokb']){
                $nokb = $_GET['nokb'];
            }else{
                $nokb = '';
            }
        }else{
            $nokb = '';
        }

        $aColumns = array('jnstrx',
                          'nmrefkb',
                          'darike',
                          'nofaktur',
                          'ket',
                          'nilai',
                          'nourut',
                          'nokb',
                        'kdrefkb');
      $sIndexColumn = "nokb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select jnstrx, kdrefkb, nmrefkb, darike, nofaktur, ket, nilai, nourut, nokb from bkl.v_tkb_d where nokb = '".$nokb."' order by nourut ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }/*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nourut']==$value['nourut']){
                    $aRow['detail'][]= $value;
                }
            }*/
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['kdrefkb']."','".$aRow['darike']."','".$aRow['ket']."'
            ,'".$aRow['nofaktur']."','".$aRow['nilai']."','".$aRow['nourut']."','".$aRow['nokb']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['nokb']."','".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }

    public function addDetail() {
        $nokb = $this->input->post('nokb');
        $tglkb = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb = $this->input->post('kdkb');
        $jnstrx = $this->input->post('jnstrx');
        $kdrefkb = $this->input->post('kdrefkb');
        $darike = $this->input->post('darike');
        $nofaktur = $this->input->post('nofaktur');
        $ket = $this->input->post('ket');
        $nourut = $this->input->post('nourut');
        if($nourut==''){
          $nourut=0;
        }
        $nilai = $this->input->post('nilai');

        $q = $this->db->query("select nokb,title,msg,tipe from bkl.tkb_d_ins('".$nokb."',
                                                                            '".$tglkb."',
                                                                            '".$kdkb."',
                                                                            '".$jnstrx."',
                                                                            ".$kdrefkb.",
                                                                            '".$darike."',
                                                                            '".$nofaktur."',
                                                                            '".$ket."',
                                                                            '".$this->session->userdata("username")."' ,
                                                                            ".$nourut.",
                                                                            ".$nilai.")");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function detaildeleted() {
        $nokb = $this->input->post('nokb');
        $nourut = $this->input->post('nourut');
        $q = $this->db->query("select title,msg,tipe from bkl.tkb_d_del('".$nokb."',". $nourut . ")");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function delete() {
        $nokb = $this->input->post('nokb');
        $q = $this->db->query("select title,msg,tipe from bkl.tkb_del('". $nokb ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function save() {
        $nokb = $this->input->post('nokb');
        $tglkb = $this->apps->dateConvert($this->input->post('tglkb'));
        $kdkb = $this->input->post('kdkb');
        $jnstrx = $this->input->post('jnstrx');
        $q = $this->db->query("select title,msg,tipe from bkl.tkb_ins( '". $nokb ."',
                                                                        '". $tglkb ."',
                                                                        '". $kdkb ."',
                                                                        '". $jnstrx ."',
                                                                        '".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

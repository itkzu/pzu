<?php defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mst_bklsuboli
 *
 * @author
 */
class Mst_bklsuboli extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mst_bklsuboli/submit'),
            'add' => site_url('mst_bklsuboli/add'),
            'edit' => site_url('mst_bklsuboli/edit'),
            'reload' => site_url('mst_bklsuboli'),
        );
        $this->load->model('mst_bklsuboli_qry');

        $this->data['kdcab'] = array(
            // "" => "-- SEMUA CABANG --",
            "ZPH01.01B" => "-- PZU AYANI --",
            "ZPH01.02B" => "-- PZU PATI --",
            "ZPH02.01B" => "-- PZU KUDUS --",
            // "ZPH02.02B" => "-- PZU PURWODADI --",
            "ZPH03.01B" => "-- PZU BREBES --",
            "ZPH04.01B" => "-- PZU SETIABUDI --",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    // List Data Vendor
    public function json_dgview() {
        echo $this->mst_bklsuboli_qry->json_dgview();
    }



    public function submit() { 
        echo $this->mst_bklsuboli_qry->submit(); 
    }

    public function update() { 
        echo $this->mst_bklsuboli_qry->update(); 
    }

    public function delete() { 
        echo $this->mst_bklsuboli_qry->delete(); 
    } 

    private function _init_add(){

		if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 'OK'){
    		$faktif = TRUE;
    	} else{
    		$faktif = FALSE;
    	}

        $this->data['form'] = array( 
            'kdcab'=> array(
                    'attr'        => array(
                        'id'    => 'kdcab',
                        'class' => 'form-control  select2',
                    ),
                    'data'        =>  $this->data['kdcab'],
                    'value'       => set_value('kdcab'),
                    'name'        => 'kdcab',
                    // 'required'    => '',
                    'placeholder' => 'Khusus Cabang',
            ),
            'kdsupplier'=> array(
                    'placeholder' => 'Kode Vendor',
                    'id'          => 'kdsupplier',
                    'name'        => 'kdsupplier',
                    'value'       => set_value('kdsupplier'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'type'        => 'hidden',
            ),
            'nmsupplier'=> array(
                    'placeholder' => '*Nama Vendor',
                    'id'          => 'nmsupplier',
                    'name'        => 'nmsupplier',
                    'value'       => set_value('nmsupplier'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'alamat'=> array(
                    'placeholder' => '*Alamat Vendor',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'npwp'=> array(
                    'placeholder' => 'NPWP',
                    'id'          => 'npwp',
                    'class'       => 'form-control', 
                    'value'       => set_value('npwp'),
                    'name'        => 'npwp',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
                    'style'       => 'text-transform: uppercase;',
            ),
      		'nohp'=> array(
                    'placeholder' => 'No HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    // 'required'    => '',
                    'style'       => 'text-transform: uppercase;',
      		), 
            'faktif'=> array(
                    'placeholder' => '',
                    'id'          => 'faktif',
                    'value'       => 't',
                    'checked'     => $faktif,
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif',
                    'type'        => 'checkbox',
            ),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdsupplier = $this->uri->segment(3);
        }

        $this->_check_id($kdsupplier);

        if($this->val[0]['faktif'] == 't'){
            $faktifx = true;
        } else {
            $faktifx = false;
        }

        if($this->val[0]['kddiv'] == null){
            $cab = '';
        } else {
            $cab = $this->data['kdcab'];
        }

        $this->data['form'] = array(
            'kdcab'=> array(
                    'attr'        => array(
                        'id'    => 'kdcab',
                        'class' => 'form-control  select',
                    ),
                    'data'        => $this->data['kdcab'],
                    'value'       => $cab,
                    'name'        => 'kdcab',
                    'required'    => '',
                    'placeholder' => 'Khusus Cabang',
            ),
            'kdsupplier'=> array(
                    'placeholder' => 'Kode Vendor',
                    'id'          => 'kdsupplier',
                    'name'        => 'kdsupplier',
                    'value'       => $this->val[0]['kdsupplier'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'type'        => 'hidden',
            ),
            'nmsupplier'=> array(
                    'placeholder' => '*Nama Vendor',
                    'id'          => 'nmsupplier',
                    'name'        => 'nmsupplier',
                    'value'       => $this->val[0]['nmsupplier'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'alamat'=> array(
                    'placeholder' => '*Alamat Vendor',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => $this->val[0]['alamat'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'npwp'=> array(
                    'placeholder' => 'NPWP',
                    'id'          => 'npwp',
                    'class'       => 'form-control', 
                    'value'       => $this->val[0]['npwp'],
                    'name'        => 'npwp',
            //        'required'    => '', 
                    'style'       => 'text-transform: uppercase;',
            ),
            'nohp'=> array(
                    'placeholder' => 'No HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => $this->val[0]['nohp'],
                    'class'       => 'form-control',
                    // 'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
            'faktif'=> array(
                    'placeholder' => '',
                    'id'          => 'faktif',
                    'value'       => 't',
                    'checked'     => $faktifx,
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif',
                    'type'        => 'checkbox',
            ), 
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->mst_bklsuboli_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

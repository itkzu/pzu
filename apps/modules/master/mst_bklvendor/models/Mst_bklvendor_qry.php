<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mst_bklvendor_qry
 *
 * @author adi
 */
class Mst_bklvendor_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function select_data($kdsupplier) {
        $this->db->select("*");
        $this->db->where('kdsupplier',$kdsupplier);
        $query = $this->db->get('bkl.supplier');
        return $query->result_array();
    } 

    public function json_dgview() {
        error_reporting(-1); 
        $aColumns = array('no',
                            'nmsupplier',
                            'alamat',
                            'npwp',
                            'nohp',
                            'faktifx',
                            'kdsupplier',
                            'kdsupplier');
        $sIndexColumn = "kdsupplier";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        if($this->session->userdata("data")["kddiv"]==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, kdsupplier, upper(nmsupplier) as nmsupplier, upper(alamat) as alamat, npwp, nohp, CASE WHEN faktif = true THEN 'AKTIF' ELSE 'TIDAK AKTIF' END AS faktifx FROM bkl.supplier ) AS a";    
        } else {
            $sTable = " ( SELECT '' as no, kdsupplier, upper(nmsupplier) as nmsupplier, upper(alamat) as alamat, npwp, nohp, CASE WHEN faktif = true THEN 'AKTIF' ELSE 'TIDAK AKTIF' END AS faktifx FROM bkl.supplier WHERE kddiv = substring('".$this->session->userdata("data")["kddiv"]."',1,9) OR kddiv is null) AS a";                
        }
        

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " Where (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */

        if(empty($sOrder)){
            $sOrder = "ORDER BY kdsupplier ASC";
        }

        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[5] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('mst_bklvendor/edit/'.$aRow['kdsupplier'])."\">Edit</a>";
                $row[6] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdsupplier']."');\">Hapus</button>";
                $output['aaData'][] = $row;
        }
       echo  json_encode( $output );
    }

    public function submit() {
      $nmsupplier   = $this->input->post('nmsupplier');
      $alamat       = $this->input->post('alamat');
      $npwp         = $this->input->post('npwp');
      $nohp         = $this->input->post('nohp');
      $kdcab         = $this->input->post('kdcab');
      $q = $this->db->query("select title,msg,tipe from bkl.mvdr_ins('" . $nmsupplier . "','" . $alamat . "','" . $npwp . "','" . $nohp . "','" . $kdcab . "','".$this->session->userdata("username")."')");

      //echo $this->db->last_query();
      if($q->num_rows()>0){
          $res = $q->result_array();
      }else{
          $res = "";
      }

      return json_encode($res);
    }

    public function update() {
      $kdsupplier   = $this->input->post('kdsupplier');
      $nmsupplier   = $this->input->post('nmsupplier');
      $alamat       = $this->input->post('alamat');
      $npwp         = $this->input->post('npwp');
      $nohp         = $this->input->post('nohp');
      $kdcab        = $this->input->post('kdcab');
      $faktifx      = $this->input->post('faktif');
      $q = $this->db->query("select title,msg,tipe from bkl.mvdr_upd('" . $kdsupplier . "','" . $nmsupplier . "','" . $alamat . "','" . $npwp . "','" . $nohp . "','" . $kdcab . "','" . $faktifx . "','".$this->session->userdata("username")."')");

      //echo $this->db->last_query();
      if($q->num_rows()>0){
          $res = $q->result_array();
      }else{
          $res = "";
      }

      return json_encode($res);
    }

    public function delete() {
        $kdsupplier = $this->input->post('kdsupplier');
        $q = $this->db->query("select title,msg,tipe from bkl.mvdr_del('". $kdsupplier ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : 
 *  Email : 
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Jurnalbkl
 *
 * @author 
 */
class Jurnalbkl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('jurnalbkl/submit'),
            'add' => site_url('jurnalbkl/add'),
            'edit' => site_url('jurnalbkl/edit'),
            'reload' => site_url('jurnalbkl'),
        );
        $this->load->model('jurnalbkl_qry');
        $pos = $this->jurnalbkl_qry->getDataPos();
        foreach ($pos as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }
        $cabang = $this->jurnalbkl_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kdcab'][$value['kdcab']] = $value['nmcab'];
        }

        /*
        $kdakun = $this->jurnalbkl_qry->getKodeAkun();
        $this->data['kdakun'] = array(
            "" => "-- Pilih Kode Akun --",
          );
        foreach ($kdakun as $value) {
            $this->data['kdakun'][$value['kdakun']] = $value['kdakun']." - ".$value['nmakun'];
          }*/

        $this->data['dk'] = array(
            "D" => "DEBIT",
            "K" => "KREDIT",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('edit',$this->data);
    }

    public function getData() {
        echo $this->jurnalbkl_qry->getData();
    }

    public function getAkun() {
        echo $this->jurnalbkl_qry->getAkun();
    }

    public function setnojurnal() {
        echo $this->jurnalbkl_qry->setnojurnal();
    }

    public function getKodeAkun() {
        echo $this->jurnalbkl_qry->getKodeAkun();
    }

    public function getKodeAkunEdit() {
        echo $this->jurnalbkl_qry->getKodeAkunEdit();
    }

    public function json_dgview() {
        echo $this->jurnalbkl_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->jurnalbkl_qry->json_dgview_detail();
    }

    public function json_dgview_detail_edit() {
        echo $this->jurnalbkl_qry->json_dgview_detail_edit();
    }

    public function addDetail() {
        echo $this->jurnalbkl_qry->addDetail();
    }

    public function updateDetail() {
        echo $this->jurnalbkl_qry->updateDetail();
    }

    public function addDetailEdit() {
        echo $this->jurnalbkl_qry->addDetailEdit();
    }

    public function updateDetailEdit() {
        echo $this->jurnalbkl_qry->updateDetailEdit();
    }

    public function detaildeleted() {
        echo $this->jurnalbkl_qry->detaildeleted();
    }

    public function detaildeleted_edit() {
        echo $this->jurnalbkl_qry->detaildeleted_edit();
    }

    public function delete() {
        echo $this->jurnalbkl_qry->delete();
    }

    public function submit() {
        echo $this->jurnalbkl_qry->submit();
    }

    public function del() {
        echo $this->jurnalbkl_qry->del();
    }

    public function batal() {
        echo $this->jurnalbkl_qry->batal();
    }

    public function batalEdit() {
        echo $this->jurnalbkl_qry->batalEdit();
    }

    public function submitedit() {
        echo $this->jurnalbkl_qry->submitedit();
    }


    private function _init_add(){
        $this->data['form'] = array(

            'kddiv2'=> array(
                   'attr'        => array(
                       'id'    => 'kddiv2',
                       'class' => 'form-control  select',
                   ),
                   'data'     =>  $this->data['kdcab'],
                   'value'    => set_value('kddiv2'),
                   'name'     => 'kddiv2',
                   'required'    => '',
                   'placeholder' => 'Cabang',
            ),
           'nojurnal'=> array(
                    'placeholder' => 'No. Jurnal',
                    //'type'        => 'hidden',
                    'id'          => 'nojurnal',
                    'name'        => 'nojurnal',
                    'value'       => set_value('nojurnal'),
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
           'tgljurnal'=> array(
                    'placeholder' => 'Tanggal Jurnal',
                    'id'          => 'tgljurnal',
                    'name'        => 'tgljurnal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
           'noref'=> array(
                    'placeholder' => 'No. Referensi',
                    'id'          => 'noref',
                    'name'        => 'noref',
                    'value'       => set_value('noref'),
                    'class'       => 'form-control',
                    'required'    => '',
          					'autofocus'   => '',
                    // 'onkeyup'     => 'myFunction()',
                    'style'       => 'text-transform: uppercase;',
            ),
            'trans'=> array(
                    'placeholder' => 'Transaksi',
                    'id'      => 'trans',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('trans'),
                    'name'     => 'trans',
                    'required'    => '',
                    // 'onkeyup'     => 'myFunction()',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '',
                    'placeholder' => 'Cabang',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => ''
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdakun'),
                    'name'     => 'kdakun',
                    'required'    => '',
            ),
            'nmakun'=> array(
                    'attr'        => array(
                        'id'    => 'nmakun',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmakun'),
                    'name'     => 'nmakun',
                    'readonly' => '',
            ),
            'dk'=> array(
                    'placeholder' => 'Posisi (D/K)',
                        'attr'        => array(
                        'id'    => 'dk',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['dk'],
                    'value'    => set_value('dk'),
                    'name'     => 'dk',
                    'required'    => '',
            ),
            'nominal'=> array(
                    'placeholder' => 'Nominal',
                    'id'    => 'nominal',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nominal'),
                    'name'     => 'nominal',
                    'required'    => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'    => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nojurnal = $this->uri->segment(3);
        }
        $this->_check_id($nojurnal);
        $this->data['form'] = array(
           'nojurnal'=> array(
                    'placeholder' => 'No. Jurnal',
                    //'type'        => 'hidden',
                    'id'          => 'nojurnal',
                    'name'        => 'nojurnal',
                    'value'       => $this->val[0]['nojurnal'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tgljurnal'=> array(
                    'placeholder' => 'Tanggal Jurnal',
                    'id'          => 'tgljurnal',
                    'name'        => 'tgljurnal',
                    'value'       => $this->apps->dateConvert($this->val[0]['tgljurnal']),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
           'noref'=> array(
                    'placeholder' => 'No. Referensi',
                    'id'          => 'noref',
                    'name'        => 'noref',
                    'value'       => $this->val[0]['noref'],
                    'class'       => 'form-control',
                    'required'    => '',
          					'autofocus'   => '',
                    // 'onkeyup'     => 'myFunction()',
                    'style'       => 'text-transform: uppercase;',
            ),
            'trans'=> array(
                    'id'          => 'trans',
                    'class'       => 'form-control',
                    'value'       => $this->val[0]['ket'],
                    'name'        => 'trans',
                    'required'    => '',
                    'placeholder' => 'Keterangan',
                    // 'onkeyup'     => 'myFunction()',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kddiv'=> array(
                    'id'    => 'kddiv',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'value'       => $this->val[0]['kddiv'],
                    'name'     => 'kddiv',
                    'placeholder' => 'Cabang',
                    'readonly'      => '',
            ),
            'nmdiv'=> array(
                    'id'    => 'nmdiv',
                    'class' => 'form-control',
                    'data'     => '',
                    'class' => 'form-control',
                    'value'       => $this->val[0]['nmdiv'],
                    'name'     => 'nmakun',
                    'readonly' => '',
                    'placeholder' => 'Cabang',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => '',
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdakun'),
                    'name'     => 'kdakun',
                    'required'    => '',
            ),
            'nmakun'=> array(
                    'attr'        => array(
                        'id'    => 'nmakun',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmakun'),
                    'name'     => 'nmakun',
                    'readonly' => '',
            ),
            'dk'=> array(
                    'placeholder' => 'Posisi (D/K)',
                    'attr'        => array(
                        'id'    => 'dk',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['dk'],
                    'value'    => set_value('dk'),
                    'name'     => 'dk',
                    'required'    => '',
            ),
            'nominal'=> array(
                    'placeholder' => 'Nominal',
                    'id'    => 'nominal',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nominal'),
                    'name'     => 'nominal',
                    'required'    => '',
            ),
            'ket'=> array(
                    'id'          => 'ket',
                    'class'       => 'form-control',
                    'value'       => set_value('ket'),
                    'name'        => 'ket',
                    'required'    => '',
                    'placeholder' => 'Keterangan',
                    // 'onkeyup'     => 'myFunction()',
                    'style'       => 'text-transform: uppercase;',
            ),
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->jurnalbkl_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'noref',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

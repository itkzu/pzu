<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbklsohr
 *
 * @author adi
 */
class Rptbklsohr extends MY_Controller {

	protected $data = '';
	protected $val = '';

	public function __construct() {
		parent::__construct();
		$this->data = array(
			'msg_main' => $this->msg_main,
			'msg_detail' => $this->msg_detail,
			'submit' => site_url('rptbklsohr/submit'),
			'add' => site_url('rptbklsohr/add'),
			'edit' => site_url('rptbklsohr/edit'),
			'reload' => site_url('rptbklsohr'),
		);
		$this->load->model('rptbklsohr_qry');
		$cabang = $this->rptbklsohr_qry->getDataCabang();
		foreach ($cabang as $value) {
				$this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
		}

		//$this->load->database('bengkel', true);
	}

	public function index() {
		$this->_init_add();
		$this->template
				->title($this->data['msg_main'], $this->apps->name)
				->set_layout('main')
				->build('index', $this->data);
	}

	public function json_dgview() {
		echo $this->rptbklsohr_qry->json_dgview();
	}

	public function json_dgview_2() {
		echo $this->rptbklsohr_qry->json_dgview_2();
	}

	public function submit() {
	    if($this->validate() == TRUE){
	        $res = $this->rptbklsohr_qry->submit();
	        var_dump($res);
	    }else{
	        $this->_init_add();
	        $this->template
	            ->title($this->data['msg_main'],$this->apps->name)
	            ->set_layout('main')
	            ->build('index',$this->data);
	      }
	}

	private function _init_add(){
	    $this->data['form'] = array(
					'kddiv'=> array(
									'attr'        => array(
									'id'    => 'kddiv',
									'class' => 'form-control  select',
							),
							'data'     =>  $this->data['kddiv'],
							'value'    => set_value('kddiv'),
							'name'     => 'kddiv',
							'required'    => '',
							'placeholder' => 'Cabang ',
					),
	        'periode_awal'=> array(
	              'placeholder' => 'Tanggal',
	              'id'          => 'periode_awal',
	              'name'        => 'periode_awal',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control calendar',
                  'style'       => 'margin-left: 5px;',
                  'required'    => '',
	        ),
	        'periode_akhir'=> array(
	              'placeholder' => 'Periode Akhir',
	              'id'          => 'periode_akhir',
	              'name'        => 'periode_akhir',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control calendar',
                  'style'       => 'margin-left: 5px;',
                  'required'    => '',
	        )
	    );
	}

	private function validate() {
	    $config = array(
	        array(
	              'field' => 'periode_awal',
	              'label' => 'Periode Awal',
	              'rules' => 'required|max_length[20]',
	        ),
	        array(
	              'field' => 'periode_akhir',
	              'label' => 'Periode Akhir',
	              'rules' => 'required|max_length[20]',
	              ),
	        );

			$this->form_validation->set_rules($config);
	    if ($this->form_validation->run() == FALSE)
	       	{
	            return false;
	        }else{
	            return true;
	        }
	    }
	}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-header">
				<!--<a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
				<a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>-->
				<!--
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-wrench"></i></button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo $add;?>" >Tambah Data Baru</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="btn-refersh">Refresh</a>
							</li>
						</ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
				-->

			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<p><a class="btn btn-primary btn-refersh"><i class="fa fa-refresh" aria-hidden="true"></i>
 Refresh</a>
				<a id="save" class="btn btn-primary btn-simpan"><i class="fa fa-floppy-o" aria-hidden="true"></i>
 Simpan</a></p>
				<p id="nodo" class="kata"></p>
				<p id="no" class="kata"></p>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover js-basic-example dataTable" class="display select">
							<thead>
								<tr>
									<th style="width: 1%;text-align: center;">No.</th>
									<th style="width: 5%;text-align: center;"></th>
									<th style="width: 7%;text-align: center;">No. Mesin</th>
									<th style="width: 5%;text-align: center;">No. Rangka</th>
									<th style="text-align: center;">Nama Konsumen</th>
									<th style="text-align: center;">Alamat Konsumen</th>
									<th style="text-align: center;">Tipe Unit</th>
									<th style="width: 5%;text-align: center;">No. DO</th>
									<th style="width: 5%;text-align: center;">Tgl. DO</th> 

								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>No.</th>
									<th>Cek.</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>

								</tr>
							</tfoot>
							<tbody></tbody>
						</table>
					</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {

		$(".btn-refersh").click(function(){
			location.reload(); 
		});


		$(".btn-simpan").click(function(){
			simpan();
		});  

		//var rows_selected = [];
		var column = [];

		column.push({
			"aTargets": [ 2,3,4,5,6,7,8 ],
			"orderable": true,
			"searchable": true,
		});

		column.push({
			"aTargets":  [0] ,
			"searchable": false,
			"orderable": false, 

		});

		column.push({
			"aTargets":  [1] ,
			"searchable": false,
			"orderable": false,
			"checkboxes": {
				'selectRow': true
			}

		});

		// column.push({
		// 	"aTargets": [ 4,5,6 ],
		// 	"mRender": function (data, type, full) {
		// 		return type === 'export' ? data : numeral(data).format('0,0');
		// 	},
		// 	"sClass": "right"
		// });

		column.push({
			"aTargets": [ 8 ],
			"mRender": function (data, type, full) {
				return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
			},
			"sClass": "center"
		});


		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"bProcessing": true,
			"select":{
				style: 'multi',
			},
			"fixedColumns": {
				leftColumns: 2
			},
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Semua Data"]],
			"bPaginate": true,
			"bDestroy": true,
			"bServerSide": true,
			"order": [[ 0, 'asc' ],[ 1, 'asc' ]],
			"rowCallback": function(row, data, dataIndex){
				/*
		        // Get row ID
		        var rowId = data[0];

		        // If row ID is in the list of selected row IDs
		        if($.inArray(rowId, rows_selected) !== -1){
					$(row).find('input[type="checkbox"]').prop('checked', true);
					$(row).addClass('selected');
		        }
		        */
		    },

			// "pagingType": "simple",
			"sAjaxSource": "<?=site_url('ajufa/json_dgview');?>",
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
			}
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Cek." && title!=="No."){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});

        // row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
	});



	function simpan(){  
			var rows_selected = table.column(1).checkboxes.selected();
			$('#nodo').val(rows_selected.join(","))
			var allkd = $("#nodo").val(); 
                    
            var count = table.rows( { selected: true } ).count(); 
			$.ajax({
				type: "POST",
				url: "<?=site_url('ajufa/simpan');?>",
				data: { "nodo":allkd, "jml":count },
				success: function(resp){ 
					location.reload(); 
				},
				error: function(event, textStatus, errorThrown) {
					swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				}
			}); 
	} 
</script>

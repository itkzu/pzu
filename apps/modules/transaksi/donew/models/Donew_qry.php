<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Donew_qry
 *
 * @author adi
 */
class Donew_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function set_nodo() {
      $noso = $this->input->post('noso');
      $nodo = $this->input->post('nodo');
      $nospk = $this->input->post('nospk');
      $ket = $this->input->post('ket');
      if($ket==='nospk'){
        $query = $this->db->query("select * from pzu.vl_jual where nosohso =  '".$nospk."'");
      } else if($ket==='noso'){
        $query = $this->db->query("select * from pzu.vl_jual where noso =  '".$noso."'");
      } else {
        $query = $this->db->query("select * from pzu.vl_jual where nodo =  '".$nodo."'");
      }
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_akso() { 
      $nodo = $this->input->post('nodo');   
      $query = $this->db->query("select * from pzu.v_do_akso('".$nodo."')");
      
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_spk() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from pzu.vm_spk a left join pzu.t_do b on a.noso = b.noso
                                                                      left join api.v_so c on a.nosohso = c.idspk where a.noso =  '".$noso."'");
      // echo $this->db->last_query();
      // $res = $q->result_array();
      // return json_encode($res);
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function setket() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select progres,tgl_aju_fa from pzu.t_do where nodo =  '".$nodo."'");
      // echo $this->db->last_query();
      // $res = $q->result_array();
      // return json_encode($res);
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function carispk() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select noso from pzu.vl_jual where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "";
      }
      return json_encode($res);
    }

    public function set_apispk() {
      $nospk = $this->input->post('nospk');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from api.v_so a left join pzu.t_do b on a.noso = b.noso where a.idspk =  '".$nospk."' and b.nodo is null");//
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_apispk_ubh() {
      $nospk = $this->input->post('nospk');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from api.v_so a left join pzu.t_do b on a.noso = b.noso where a.idspk =  '".$nospk."' ");//
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function full_leas() {
      $query = $this->db->query("select * from pzu.program_leasing where faktif = 't'");//
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function set_leas() {
      $kdprogleas = $this->input->post('kdleasing');
      $query = $this->db->query("select * from pzu.program_leasing where kdleasing =  '".$kdprogleas."' and faktif = 't'");//
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function set_nosin() {
      $nosin = $this->input->post('nosin');
      $query = $this->db->query("select * from pzu.vm_smh_avail where nosin =  '".$nosin."'");//
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getKodeSales() {
      $noso = $this->input->post('noso');
      $this->db->where("faktif","true");
      $q = $this->db->get("pzu.v_sales");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getAks1() { 
      $data1 = $this->input->post('data1');  
      $query = $this->db->query("select * from pzu.vm_stock_aks where sisa > 0 union
                                 select * from pzu.vm_stock_aks where kdaks in ('".$data1."')");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getAks2() { 
      $data1 = $this->input->post('data1'); 
      $data2 = $this->input->post('data2');  
      $query = $this->db->query("select * from pzu.vm_stock_aks where sisa > 0 union
                                 select * from pzu.vm_stock_aks where kdaks in ('".$data1."','".$data2."')");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getAks3() { 
      $data1 = $this->input->post('data1');  
      $data2 = $this->input->post('data2'); 
      $data3 = $this->input->post('data3'); 
      $query = $this->db->query("select * from pzu.vm_stock_aks where sisa > 0 union
                                 select * from pzu.vm_stock_aks where kdaks in ('".$data1."','".$data2."','".$data3."')");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getAks4() { 
      $data1 = $this->input->post('data1');  
      $data2 = $this->input->post('data2'); 
      $data3 = $this->input->post('data3'); 
      $data4 = $this->input->post('data4'); 
      $query = $this->db->query("select * from pzu.vm_stock_aks where sisa > 0 union
                                 select * from pzu.vm_stock_aks where kdaks in ('".$data1."','".$data2."','".$data3."','".$data4."')");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getKdAks1() { 
      $this->db->where("faktif","true");
      $q = $this->db->get("pzu.vm_stock_aks");
      // echo $this->db->last_query();
      $res = $q->result_array();  
      return json_encode($res); 
    }

    public function getKdAks2() {  
      $kdaks1 = $this->input->post('kdaks1'); 
      $query = $this->db->query("select * from pzu.vm_stock_aks where kdaks != '".$kdaks1."' and sisa > 0");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getKdAks3() { 
      $kdaks1 = $this->input->post('kdaks1'); 
      $kdaks2 = $this->input->post('kdaks2'); 
      $query = $this->db->query("select * from pzu.vm_stock_aks where kdaks != '".$kdaks1."' and kdaks != '".$kdaks2."' and sisa > 0");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getKdAks4() { 
      $kdaks1 = $this->input->post('kdaks1'); 
      $kdaks2 = $this->input->post('kdaks2'); 
      $kdaks3 = $this->input->post('kdaks3'); 
      $query = $this->db->query("select * from pzu.vm_stock_aks where kdaks != '".$kdaks1."' and kdaks != '".$kdaks2."' and kdaks != '".$kdaks3."' and sisa > 0");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getKdAks5() { 
      $kdaks1 = $this->input->post('kdaks1'); 
      $kdaks2 = $this->input->post('kdaks2'); 
      $kdaks3 = $this->input->post('kdaks3'); 
      $kdaks4 = $this->input->post('kdaks4'); 
      $query = $this->db->query("select * from pzu.vm_stock_aks where kdaks != '".$kdaks1."' and kdaks != '".$kdaks2."' and kdaks != '".$kdaks3."' and kdaks != '".$kdaks4."' and sisa > 0");
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res); 
    }

    public function getNoAssist() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nosohso)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nama_s)', strtolower($q));
        $this->db->or_like('LOWER(noktp)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nosohso) like '".strtolower($q)."' then 1
                                when LOWER(nama_s) like '".strtolower($q)."%' then 2
                                when LOWER(noktp) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama_s");
        $query = $this->db->get('api.v_so_do');
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['nosohso'],
                    'text' => $value['nama_s'],
                    'noktp' => $value['noktp'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'noktp' => 'noktp',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }

    public function getNoSPK() {
      $nospk = $this->input->post('nospk');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from api.v_so a left join pzu.t_do b on a.noso = b.noso where b.nodo is null UNION select * from api.v_so where noso = '" .$noso. "'");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getSPKAssist() {
        $q = $this->input->post('q');
        $nospk = $this->input->post('nospk');
        $noso = $this->input->post('noso');
        $query = $this->db->query("select x.* from (select a.* from api.v_so a left join pzu.t_do b on a.noso = b.noso where b.nodo is null UNION select * from api.v_so where noso = '" .$noso. "') as x
                                    WHERE LOWER(x.idspk) LIKE '%".$q."%' ESCAPE '!' OR LOWER(x.nmbpkb) LIKE '%".$q."%' ESCAPE '!'
                                    OR LOWER(x.noktp) LIKE '%".$q."%' ESCAPE '!' ORDER BY case
                                      when LOWER(x.idspk) like '".$q."'
                                      then 1
                                      when LOWER(x.nmbpkb) like '".$q."%'
                                      then 2
                                      when LOWER(x.noktp) like '%".$q."'
                                      then 3
                                      else 4 end, x.nmbpkb");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['idspk'],
                    'text' => $value['nmbpkb'],
                    'noktp' => $value['noktp'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'noktp' => 'noktp',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $ayani  = 'ZPH01.01S';
      $pati   = 'ZPH01.02S';
      $kudus  = 'ZPH02.01S';
      $pwd    = 'ZPH02.02S';
      $brebes = 'ZPH03.01S';
      $stbd   = 'ZPH04.01S'; 
      if($this->session->userdata('data')['kddiv']===$stbd){
        $kode = 'S';
      } else if ($this->session->userdata('data')['kddiv']===$brebes) {
        $kode = 'B';
      } else if ($this->session->userdata('data')['kddiv']===$pwd) {
        $kode = 'P';
      } else if ($this->session->userdata('data')['kddiv']===$kudus) {
        $kode = 'K';
      } else if ($this->session->userdata('data')['kddiv']===$pati) {
        $kode = 'P';
      } else if ($this->session->userdata('data')['kddiv']===$ayani) {
        $kode = 'S';
      }

      $query = $this->db->query("select '".$kode."' as kode, coalesce(SUBSTRING(max(nodo),5,6)::int+1,1) as jml from pzu.t_do where left(periode,4) = '".$tanggal."'");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      // echo $this->session->userdata('data')['kddiv'].$kode;
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getNoDO() {


      $query = $this->db->query("select a.* from api.v_so a left join pzu.t_do b on a.noso = b.noso where b.nodo is null UNION select * from api.v_so where noso = '" .$noso. "'");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getNoDOHso() {
      $query = $this->db->query("select a.idspk from api.v_so a left join pzu.t_so b on b.nosohso = a.idspk left join pzu.t_do c on b.noso = c.noso where c.nodo is not null group by a.idspk");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    } 

    public function getTipeUnit() {
      $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.v_tipe");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKdWarna() {
      $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.warna");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKodeSalesHeader() {
      $kdsales = $this->input->post('kdsales');
      // $this->db->where("faktif","true");
      // $this->db->where("kdsales",$kdsales);
      $q = $this->db->query("select * from pzu.v_sales where faktif = 'true' and kdsales = ".$kdsales);
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKodeTipeHeader() {
      $kdtipe = $this->input->post('kdtipe');
      // $this->db->where("faktif","true");
      $this->db->where("kdtipe",$kdtipe);
      $q = $this->db->get("pzu.v_tipe");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKdWarnaHeader() {
      $kdwarna = $this->input->post('kdwarna');
      // $this->db->where("faktif","true");
      $this->db->where("kdwarna",$kdwarna);
      $q = $this->db->get("pzu.warna");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function gethrg_otr() {
      $nosin = $this->input->post('nosin');
      $stat_otr = $this->input->post('stat_otr');
      // $this->db->where("faktif","true");
      $this->db->where("nosin",$nosin);
      $q = $this->db->get("pzu.vm_smh_avail");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function gethrg_otr_edt() {
      $nosin = $this->input->post('nosin');
      $stat_otr = $this->input->post('stat_otr');
      // $this->db->where("faktif","true");
      $this->db->where("nosin",$nosin);
      $q = $this->db->get("pzu.vm_smh_sold");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.get_divisi()");
        return $q->result_array();
    }

    public function getleas() {
        $this->db->select('kdleasing,nmleasing');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.v_leasing");
        return $q->result_array();
    }

    public function getkdkerja() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
       $this->db->where('faktif',true);
        $q = $this->db->get("pzu.m_pekerjaan");
      $res = $q->result_array();
      return json_encode($res); 
    }

    public function getkdsalesins() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.sales_ins");
        return $q->result_array();
    }
 
    public function getkdkota() {
      // $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("api.v_kota");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res); 
    }

    public function bbn_tarif() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.bbn_tarif");
        $res = $q->result_array();
        return json_encode($res);
    }

    public function getDetailPO() {
        $this->db->select("nopo, to_char(tglpo,'DD-MM-YYYY') AS tglpo, nap"
                . " , 0 as ndef, '' as nourut ");
        $nopo = $this->input->post('nopo');
        $kddiv = $this->input->post('kddiv');
        $this->db->where("kddiv",$kddiv);
        $this->db->where("nopo",$nopo);
        $this->db->where("nodf IS NULL");
        $q = $this->db->get("pzu.v_df_po");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function adddo() {

      $noso            = $this->input->post('noso');
      $tglso            = $this->apps->dateConvert($this->input->post('tglso'));
      $nosin            = $this->input->post('nosin');
      $nodo             = $this->input->post('nodo');
      $kdsales          = $this->input->post('kdsales');
      $jnsbayar         = $this->input->post('jnsbayar');
      $kdleasing        = $this->input->post('kdleasing');
      $kdprogleas       = $this->input->post('kdprogleas');
      $stat_otr         = $this->input->post('stat_otr');
      $hrg_otr          = $this->input->post('hrg_otr');
      $um               = $this->input->post('um');
      $disc             = $this->input->post('disc');
      $disc_leasing     = $this->input->post('disc_leasing');
      $nama             = $this->input->post('nama');
      $nama_s           = $this->input->post('nama_s');
      $alamat           = $this->input->post('alamat');
      $alamat_s         = $this->input->post('alamat_s');
      $alamatktp_s      = $this->input->post('alamatktp_s');
      $kel              = $this->input->post('kel');
      $kel_s            = $this->input->post('kel_s');
      $nohp             = $this->input->post('nohp');
      $nohp_s           = $this->input->post('nohp_s');
      $kec              = $this->input->post('kec');
      $kec_s            = $this->input->post('kec_s');
      $notelp           = $this->input->post('notelp');
      $notelp_s         = $this->input->post('notelp_s');
      $npwp             = $this->input->post('npwp');
      $noktp            = $this->input->post('noktp');
      $kota             = $this->input->post('kota');
      $kota_s           = $this->input->post('kota_s');
      $kdkerja           = $this->input->post('kdkerja');
      $angsuran         = $this->input->post('angsuran');
      $tenor            = $this->input->post('tenor');
      $ket              = $this->input->post('ket');
      $tgldo            = $this->apps->dateConvert($this->input->post('tgldo'));
      $kirim_tagih      = $this->input->post('kirim_tagih');
      $nilai_kt         = $this->input->post('nilai_kt');
      $insentif         = $this->input->post('insentif');
      $ket_astra        = $this->input->post('ket_astra');
      $nospk        = $this->input->post('nospk');
      $kdaks1        = $this->input->post('kdaks1');
      $kdaks2        = $this->input->post('kdaks2');
      $kdaks3        = $this->input->post('kdaks3');
      $kdaks4        = $this->input->post('kdaks4');
      $qtyaks1        = $this->input->post('qtyaks1');
      $qtyaks2        = $this->input->post('qtyaks2');
      $qtyaks3        = $this->input->post('qtyaks3');
      $qtyaks4        = $this->input->post('qtyaks4');
      $promo        = $this->input->post('promo');


      if($disc_leasing===''){      $disc_leasing = 0;    } else {      $disc_leasing = $disc_leasing;    }
 
      if($kdaks1===''){    $kdaks1 = 0;  $qtyaks1 = 0; } else {    $kdaks1 = $kdaks1;  $qtyaks1 = $qtyaks1;  }
      if($kdaks2===''){    $kdaks2 = 0;  $qtyaks2 = 0; } else {    $kdaks2 = $kdaks2;  $qtyaks2 = $qtyaks2;  }
      if($kdaks3===''){    $kdaks3 = 0;  $qtyaks3 = 0; } else {    $kdaks3 = $kdaks3;  $qtyaks3 = $qtyaks3;  }
      if($kdaks4===''){    $kdaks4 = 0;  $qtyaks4 = 0; } else {    $kdaks4 = $kdaks4;  $qtyaks4 = $qtyaks4;  }

      if($disc===''){        $disc = 0;      } else {        $disc = $disc;      }

      if($um===''){        $um = 0;      } else {        $um = $um;      }

      if($npwp==='.'){        $npwp = '00.000.000.0-000.000';      } else {        $npwp = $npwp;      }

        $q = $this->db->query("select title,msg,tipe from pzu.do_ins_w('".$noso."'::varchar,
                                                                         '".$tglso."'::date,
                                                                         '".$nospk."'::varchar,
                                                                         '".$nosin."'::varchar,
                                                                         '".$nodo."'::varchar,
                                                                          ".$kdsales."::integer,
                                                                         '".$jnsbayar."'::varchar,
                                                                         '".$ket_astra."'::varchar,
                                                                         '".$kdleasing."'::varchar,
                                                                          ".$kdprogleas."::integer,
                                                                         '".$stat_otr."'::varchar,
                                                                          ".$hrg_otr."::numeric,
                                                                          ".$um."::numeric,
                                                                          ".$disc."::numeric,
                                                                          ".$disc_leasing."::numeric,
                                                                         '".$nama."'::varchar,
                                                                         '".$alamat."'::varchar,
                                                                         '".$kel."'::varchar,
                                                                         '".$kec."'::varchar,
                                                                         '".$kota."'::varchar,
                                                                         '".$nohp."'::varchar,
                                                                         '".$notelp."'::varchar,
                                                                         '".$npwp."'::varchar,
                                                                         '".$nama_s."'::varchar,
                                                                         '".$alamat_s."'::varchar,
                                                                         '".$alamatktp_s."'::varchar,
                                                                         '".$kel_s."'::varchar,
                                                                         '".$kec_s."'::varchar,
                                                                         '".$kota_s."'::varchar,
                                                                         '".$nohp_s."'::varchar,
                                                                         '".$notelp_s."'::varchar,
                                                                         '".$noktp."'::varchar,
                                                                          ".$angsuran."::numeric,
                                                                          ".$tenor."::numeric,
                                                                         '".$ket."'::varchar,
                                                                         '".$tgldo."'::date,
                                                                         '".$kirim_tagih."'::boolean,
                                                                          ".$nilai_kt."::numeric,
                                                                          ".$insentif."::integer,
                                                                         '".$this->session->userdata('username')."',
                                                                         ".$kdkerja."::integer ,
                                                                         ".$kdaks1."::integer ,
                                                                         ".$kdaks2."::integer ,
                                                                         ".$kdaks3."::integer ,
                                                                         ".$kdaks4."::integer ,
                                                                         ".$qtyaks1."::integer ,
                                                                         ".$qtyaks2."::integer ,
                                                                         ".$qtyaks3."::integer ,
                                                                         ".$qtyaks4."::integer ,
                                                                         '".$this->session->userdata('data')['kddiv']."',
                                                                         '".$promo."'::varchar)");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updatedo() {

      $noso            = $this->input->post('noso');
      $tglso            = $this->apps->dateConvert($this->input->post('tglso'));
      $nosin            = $this->input->post('nosin');
      $nodo             = $this->input->post('nodo');
      $kdsales          = $this->input->post('kdsales');
      $jnsbayar         = $this->input->post('jnsbayar');
      $kdleasing        = $this->input->post('kdleasing');
      $kdprogleas       = $this->input->post('kdprogleas');
      $stat_otr         = $this->input->post('stat_otr');
      $hrg_otr          = $this->input->post('hrg_otr');
      $um               = $this->input->post('um');
      $disc             = $this->input->post('disc');
      $disc_leasing     = $this->input->post('disc_leasing');
      $nama             = $this->input->post('nama');
      $nama_s           = $this->input->post('nama_s');
      $alamat           = $this->input->post('alamat');
      $alamat_s         = $this->input->post('alamat_s');
      $alamatktp_s      = $this->input->post('alamatktp_s');
      $kel              = $this->input->post('kel');
      $kel_s            = $this->input->post('kel_s');
      $nohp             = $this->input->post('nohp');
      $nohp_s           = $this->input->post('nohp_s');
      $kec              = $this->input->post('kec');
      $kec_s            = $this->input->post('kec_s');
      $notelp           = $this->input->post('notelp');
      $notelp_s         = $this->input->post('notelp_s');
      $npwp             = $this->input->post('npwp');
      $noktp            = $this->input->post('noktp');
      $kota             = $this->input->post('kota');
      $kota_s           = $this->input->post('kota_s');
      $angsuran         = $this->input->post('angsuran');
      $tenor            = $this->input->post('tenor');
      $ket              = $this->input->post('ket');
      $tgldo            = $this->apps->dateConvert($this->input->post('tgldo'));
      $kirim_tagih      = $this->input->post('kirim_tagih');
      $nilai_kt         = $this->input->post('nilai_kt');
      $insentif         = $this->input->post('insentif');
      $ket_astra        = $this->input->post('ket_astra');
      $nospk        = $this->input->post('nospk');
      $kdkerja        = $this->input->post('kdkerja');
      $kdaks1        = $this->input->post('kdaks1');
      $kdaks2        = $this->input->post('kdaks2');
      $kdaks3        = $this->input->post('kdaks3');
      $kdaks4        = $this->input->post('kdaks4');
      $qtyaks1        = $this->input->post('qtyaks1');
      $qtyaks2        = $this->input->post('qtyaks2');
      $qtyaks3        = $this->input->post('qtyaks3');
      $qtyaks4        = $this->input->post('qtyaks4');
      $promo        = $this->input->post('promo');

      if($disc_leasing===''){
        $disc_leasing = 0;
      } else {
        $disc_leasing = $disc_leasing;
      }

      if($disc===''){
        $disc = 0;
      } else {
        $disc = $disc;
      }

      if($um===''){
        $um = 0;
      } else {
        $um = $um;
      }

      if($npwp==='.'){
        $npwp = '00.000.000.0-000.000';
      } else {
        $npwp = $npwp;
      }
      
      if($kdaks1===''){    $kdaks1 = 0;  $qtyaks1 = 0; } else {    $kdaks1 = $kdaks1;  $qtyaks1 = $qtyaks1;  }
      if($kdaks2===''){    $kdaks2 = 0;  $qtyaks2 = 0; } else {    $kdaks2 = $kdaks2;  $qtyaks2 = $qtyaks2;  }
      if($kdaks3===''){    $kdaks3 = 0;  $qtyaks3 = 0; } else {    $kdaks3 = $kdaks3;  $qtyaks3 = $qtyaks3;  }
      if($kdaks4===''){    $kdaks4 = 0;  $qtyaks4 = 0; } else {    $kdaks4 = $kdaks4;  $qtyaks4 = $qtyaks4;  }

        $q = $this->db->query("select title,msg,tipe from pzu.do_upd_w('".$noso."'::varchar,
                                                                         '".$tglso."'::date,
                                                                         '".$nospk."'::varchar,
                                                                         '".$nosin."'::varchar,
                                                                         '".$nodo."'::varchar,
                                                                          ".$kdsales."::integer,
                                                                         '".$jnsbayar."'::varchar,
                                                                         '".$ket_astra."'::varchar,
                                                                         '".$kdleasing."'::varchar,
                                                                          ".$kdprogleas."::integer,
                                                                         '".$stat_otr."'::varchar,
                                                                          ".$hrg_otr."::numeric,
                                                                          ".$um."::numeric,
                                                                          ".$disc."::numeric,
                                                                          ".$disc_leasing."::numeric,
                                                                         '".$nama."'::varchar,
                                                                         '".$alamat."'::varchar,
                                                                         '".$kel."'::varchar,
                                                                         '".$kec."'::varchar,
                                                                         '".$kota."'::varchar,
                                                                         '".$nohp."'::varchar,
                                                                         '".$notelp."'::varchar,
                                                                         '".$npwp."'::varchar,
                                                                         '".$nama_s."'::varchar,
                                                                         '".$alamat_s."'::varchar,
                                                                         '".$alamatktp_s."'::varchar,
                                                                         '".$kel_s."'::varchar,
                                                                         '".$kec_s."'::varchar,
                                                                         '".$kota_s."'::varchar,
                                                                         '".$nohp_s."'::varchar,
                                                                         '".$notelp_s."'::varchar,
                                                                         '".$noktp."'::varchar,
                                                                          ".$angsuran."::numeric,
                                                                          ".$tenor."::numeric,
                                                                         '".$ket."'::varchar,
                                                                         '".$tgldo."'::date,
                                                                         '".$kirim_tagih."'::boolean,
                                                                          ".$nilai_kt."::numeric,
                                                                          ".$insentif."::integer,
                                                                         '".$this->session->userdata('username')."'::varchar, 
                                                                         ".$kdkerja."::integer ,
                                                                         ".$kdaks1."::integer ,
                                                                         ".$kdaks2."::integer ,
                                                                         ".$kdaks3."::integer ,
                                                                         ".$kdaks4."::integer ,
                                                                         ".$qtyaks1."::integer ,
                                                                         ".$qtyaks2."::integer ,
                                                                         ".$qtyaks3."::integer ,
                                                                         ".$qtyaks4."::integer ,
                                                                         '".$this->session->userdata('data')['kddiv']."',
                                                                         '".$promo."'::varchar)");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function deletedo() {
        $noso = $this->input->post('noso');
        $ulasan = 'BATAL';
        $q = $this->db->query("select title,msg,tipe from pzu.do_del_w('". $noso ."','". $ulasan ."','". $this->session->userdata('username') ."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    //cetak
    public function ctk_do($nodo) {
        $q = $this->db->query("select to_char(now(),'HH:MI:SS') as wkt, a.*, c.totaks, b.* from pzu.vb_do a
                               LEFT JOIN (select * from pzu.v_do_akso('".$nodo."')) as b ON a.nodo = b.nodo 
                               LEFT JOIN (select count(g.nourut) as totaks, f.nodo from pzu.t_akso f JOIN pzu.t_akso_d g ON f.noakso = g.noakso GROUP BY f.nodo) as c ON a.nodo = c.nodo 
                               WHERE a.nodo = upper('".$nodo."') ");
        // $this->db->select("to_char(now(),'HH:MI:SS') as wkt, *");
        // $this->db->where('nodo',upper('.$nodo.'));
        // $q = $this->db->get("pzu.vb_do");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function ctk_um($nodo) { 
        $q = $this->db->query("select * from pzu.vb_kwitansi_um where nodo = upper('".$nodo."')");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function ctk_stiker($nodo,$margin_up,$alamat_ctk) {
        $this->db->select("nodo,tgldo,alamat,notelp,nama,".$margin_up." as mrg,replace('".$alamat_ctk."','%20',' ') as alamat_ctk");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get('pzu.vl_jual');
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function ctk_stkkpb($nodo,$margin_up) {
        $this->db->select("nodo,tgldo,alamat,kode,".$margin_up." as mrg,kpb,kpb1,kpb2,kpb3,kpb4,notelp");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get('pzu.vl_sesikpb');
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function ctk_kredit($nodo) {
        $this->db->select("*,to_char(tgldo,'DD-MM-YYYY') as tgl,to_char(now(),'DD-MM-YYYY') as wkt");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get('pzu.vl_jual');
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

}

    <?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-header">
            <a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
                <div class="table-responsive">
                <table class="dataTable table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th style="width: 100px;text-align: center;">Nama Lengkap</th>
                            <th style="text-align: center;">Jabatan</th>
                            <th style="text-align: center;">Atasan</th>
                            <th style="text-align: center;">Pos/Cabang</th>
                            <th style="text-align: center;">Tgl Masuk</th>
                            <th style="text-align: center;">Status Aktif</th>
                            <th style="width: 10px;text-align: center;">Set Keluar</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 100px;text-align: center;">Nama Lengkap</th>
                            <th style="text-align: center;">Jabatan</th>
                            <th style="text-align: center;">Atasan</th>
                            <th style="text-align: center;">Pos/Cabang</th>
                            <th style="text-align: center;">Tgl Masuk</th>
                            <th style="text-align: center;">Status Aktif</th>
                            <th style="width: 10px;text-align: center;">Set Keluar</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>
<!-- Modal Sales Oute-->
<div id="sales-out" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 30%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-sales-out">Sales Keluar</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <?php 
                    echo form_input($form['kdsales']);
                    echo form_label($form['tgl_out']['placeholder']);
                    echo form_input($form['tgl_out']);
                    echo form_error('tgl_out','<div class="note">','</div>'); 
                ?>
            </div>
        </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-submit">Proses</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-refersh").click(function(){
            table.ajax.reload();
        });
        $(".btn-submit").click(function(){
            swal({
                title: "Konfirmasi Keluar Sales !",
                text: "Data yang sales yang keluar akan di non aktifakan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
                var kdsales =  $("#kdsales").val();
                var tgl_out =  $("#tgl_out").val();
                var submit = "<?php echo $submit;?>"; 
                    $.ajax({
                        type: "POST",
                        url: submit,
                        data: {"kdsales":kdsales
                                ,"tgl_out":tgl_out
                                ,"stat":"delete"},
                        success: function(resp){   
                            var obj = jQuery.parseJSON(resp);
                            if(obj.state==="1"){
                                swal({
                                    title: "Terupdate",
                                    text: obj.msg,
                                    type: "success"
                                }, function(){
                                    location.reload();
                                });
                            }else{
                                swal("Error", obj.msg, "error");
                            }
                        },
                        error:function(event, textStatus, errorThrown) {
                            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                        }
                    });
            });
        });
        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],
            "columnDefs": [
                { "orderable": false, "targets": 6 }
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('transalesout/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            }
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="Set Keluar"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
    
    function refresh(){
        table.ajax.reload();
    }
    
    function deleted(kdsales){
          $("#sales-out").modal({
             show: true, 
             backdrop: 'static'
          });
          $("#kdsales").val(kdsales);
    }
    
    
</script>
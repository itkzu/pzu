<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Tranvalsl_qry
 *
 * @author adi
 */
class Tranvalsl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getDetail() {
        $nopo = $this->input->post('nopo');
        $str = "SELECT
                        t_umdo.nopo
                        ,t_umdo.nmfile as filedo
                        ,t_umsl.nmfile as filesl
                        ,to_char(t_umdo.tglpo,'DD-MM-YYYY') AS tglpo
                        ,t_umsl_d.nourut
                        ,t_umsl_d.nosin
                        ,t_umsl_d.nora
                        ,t_umsl_d.kode
                        ,t_umsl_d.kdwarna
                        ,t_umsl_d.tahun
                  FROM pzu.t_umdo
                JOIN pzu.t_umsl ON t_umdo.noumdo = t_umsl.noumdo
                JOIN pzu.t_umsl_d ON t_umsl.noumsl = t_umsl_d.noumsl
                WHERE t_umdo.nopo = '".$nopo."'";
        $q = $this->db->query($str);
        if($q->num_rows()>0){
            $res = $q->result_array();
            return json_encode($res);
        }else{
            return "";
        }
    }
    
    public function json_dgview() {
        error_reporting(-1);
        $aColumns = array('nopo'
                        ,'tglpo'
                        ,'id');
	$sIndexColumn = "nopo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT nopo, tglpo, nopo as id FROM pzu.v_umdo_blm_po) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {   
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }
        
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {   
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
            }
        }
        

        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";
        
//        echo $sQuery;
        
        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()
        
        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                //23 - 28
                $row[2] = '<button class="btn btn-primary btn-sm" onclick="getDetail(\''.$aRow['id'].'\');">Validasi</button>';
               $output['aaData'][] = $row;
	}
	echo  json_encode( $output );  
    }
    
    public function submit() {
        try {
            $array = $this->input->post();     
            $array['tglterima'] = $this->apps->dateConvert($array['tglterima']);
            $kddiv = $this->session->userdata('data')['kddiv'];
            $userentry = $this->session->userdata('username');
            $str = "SELECT pzu.po_ins_umsl('".$array['nopo']."', '".$array['tglterima']."', '".$kddiv."', '".$userentry."') AS ins_uml";
            $q = $this->db->query($str);
            if($this->db->trans_status() === FALSE){  
                $err = $this->db->error();
                $this->res = " Error : ". $this->apps->err_code($err['message']);
                $this->state = "0";
                
            }else{        
                $this->res = "Proses Validasi Berhasil";
                $this->state = "1";
            }
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        //$this->session->set_flashdata('statsubmit', json_encode($arr));
        return $arr;
    }

}

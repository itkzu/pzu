<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Revinvso_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getid() {
      $ayani  = 'ZPH01.01S';
      $pati   = 'ZPH01.02S';
      $kudus  = 'ZPH02.01S';
      $pwd    = 'ZPH02.02S';
      $brebes = 'ZPH03.01S';
      $stbd   = 'ZPH04.01S'; 
      if($this->session->userdata('data')['kddiv']===$stbd){
        $kode = 'S';
      } else if ($this->session->userdata('data')['kddiv']===$brebes) {
        $kode = 'B';
      } else if ($this->session->userdata('data')['kddiv']===$pwd) {
        $kode = 'P';
      } else if ($this->session->userdata('data')['kddiv']===$kudus) {
        $kode = 'K';
      } else if ($this->session->userdata('data')['kddiv']===$pati) {
        $kode = 'P';
      } else if ($this->session->userdata('data')['kddiv']===$ayani) {
        $kode = 'S';
      }

      $query = $this->db->query("select '".$kode."' as kode"); 
      $res = $query->result_array();
      return json_encode($res);
    }



    public function select_data($param) {
		$q = $this->db->query("select nodo, to_char(tgldo,'DD-MM-YYYY') AS tgldo, noso, nosin, nora, nmtipe, nmwarna, nama, alamat, jnsbayar, round(ppn_unit) + round(dpp_unit) as harga, round(ppn_unit) + round(dpp_unit) + bbn as tot_hrg, tot_disc, dpp_unit - tot_disc_dpp as dpp_unit, ppn_unit - tot_disc_ppn as ppn_unit, noktp, bbn from pzu.vb_invoice_so where nodo = '".$param."'");   
		// echo $this->db->last_query();
        return $q->result_array();
    }

    public function getData() {
		error_reporting(-1);

        //$nokb = $this->input->post('nokb');

		if( isset($_GET['nodo']) ){
			if($_GET['nodo']){
				$nodo = $_GET['nodo'];
			}else{
				$nodo = '';
			}
    	}else{
      		$nodo = '';
    	}

		//var_dump($periode_awal, $periode_awal);
		$aColumns = array('no',
                      	'nodo',
                      	'tgldo',
                      	'noso',
                      	'nmtipe',
                      	'nama',
                      	'alamat',
                      	'jnsbayar',
                      	'harga',
                      	'tot_disc',
						'dpp_unit',
						'ppn_unit',
						'nodo'
                        );


		$sIndexColumn = "nodo";

		$sLimit = "";
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}

		$sTable = " (SELECT '' as no, nodo, to_char(tgldo,'DD-MM-YYYY') AS tgldo, noso, nosin, nora, nmtipe, nmwarna, nama, alamat, jnsbayar, round(ppn_unit) + round(dpp_unit) as harga, tot_disc, round(dpp_unit) as dpp_unit, round(ppn_unit) as ppn_unit
					    FROM pzu.vb_invoice_so WHERE nodo = '". $nodo ."'
					) AS a";


		//$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
		$sWhere = "";

		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = " ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == " ORDER BY" )
			{
					$sOrder = "";
			}
		}

		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere =  "AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}

		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{

			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
			}
		}


		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";

        //echo $sQuery;

		$rResult = $this->db->query( $sQuery);

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";	//SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);

		foreach ( $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if($aRow[ $aColumns[$i] ]===null){
					$aRow[ $aColumns[$i] ] = '';
				}
				if(is_numeric($aRow[ $aColumns[$i] ])){
					$row[] = floatval($aRow[ $aColumns[$i] ]);
				}else{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$row[12] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs btn-edit \" href=\"".site_url('revinvso/edit/'.$aRow['nodo'])."\">Edit</a>";

      $output['aaData'][] = $row;
		}
		echo json_encode( $output );
	}

    public function proses() {
        $nodo = $this->input->post('nodo');
        $dpp_unit = $this->input->post('dpp_unit');
        $ppn_unit = $this->input->post('ppn_unit');
        $diskon = $this->input->post('diskon');
        $bbn = $this->input->post('bbn');
		$q = $this->db->query("select title,msg,tipe from pzu.tmp_inv_ins('". $nodo ."',".$dpp_unit.",".$ppn_unit.",".$diskon.",".$bbn.")");
		// echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
    
    public function submit() {
        try {
            $array = $this->input->post();
            $array['nokb'] = $this->nokb;
            $array['nourut'] = $this->nourut;
            if(!empty($array['nokb']) && empty($array['nourut'])){
      				//echo "<script> console.log('post qry: ". json_encode($array) ."');</script>";

      				$this->db->where('nourut', $array['nokb']);
      				unset($array['nokb']);
      				$resl = $this->db->update('v_kb_d', $array);
      				if( ! $resl){
      					$err = $this->db->error();
      					$this->res = " Error : ". $this->apps->err_code($err['message']);
      					$this->state = "0";
      				}else{
      					$this->res = "Data Berhasil Diupdate";
      					$this->state = "1";
      				}

      			}

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => 'form-inline'
                        , 'target' => 'blank');
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_awal']['placeholder']);
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_akhir']['placeholder']);
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                    <button type="submit" class="btn btn-success" name="submit" value="excel">
                        <i class="fa fa-file-excel-o"></i> Export Excel
                    </button>
                <?php echo form_close(); ?>
                <hr>
                <div  class="table-responsive">
                    <table style="width: 1900px" class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr class="tr-head">
                            </tr>
                            <tr class="tr-head-tk">
                            </tr>
                        </thead>
                        <tfoot class="tr-foot"></tfoot>
                        <tbody class="tr-body"></tbody>
                    </table>                    
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {
    $(".table-responsive").hide();
    $(".btn-tampil").click(function(){
        getPenjualanPerLeasing();
    });
    
    function getPenjualanPerLeasing(){
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rptpenjualanleasing/getPenjualanPerLeasing');?>",
            data: {"periode_awal":periode_awal
                    ,"periode_akhir":periode_akhir},
            beforeSend: function(msg){
                $(".table-responsive").hide();
                $(".tr-head").html('');
                $(".tr-head-tk").html('');
            },
            success: function(resp){   
                $(".table-responsive").show();
                $(".tr-head").html('<th style="text-align: center;width: 10px;" rowspan="2">NO</th>'+
                                 '<th style="text-align: center;" rowspan="2">DESKRIPSI</th>'+
                                 '<th style="text-align: center;" rowspan="2">TIPE</th>'+
                                 '<th style="text-align: center;" rowspan="2">KODE</th>'+
                                 '<th style="text-align: center;" rowspan="2">T</th>');
                var obj = jQuery.parseJSON(resp);
                // Menampilkan kolom header start
                var col_num = 1;
                $.each(obj.header, function(key, data){
                    //console.log(col_num);
                    $(".tr-head").append('<th style="text-align: center;width: 80px;" colspan="4">'+data.nmleasing+'</th>');
                    $(".tr-head-tk").append('<th style="text-align: center;width: 40px;">1 TH</th>'+
                                            '<th style="text-align: center;width: 40px;">2 TH</th>'+
                                            '<th style="text-align: center;width: 40px;">3 TH</th>'+
                                            '<th style="text-align: center;width: 40px;">4 TH</th>');
                    col_num++;
                });
                $(".tr-head").append('<th style="text-align: center;width: 10px;" rowspan="2">TOTAL</th>');
                // Menampilkan kolom header end
                setDetailPenjualanLeasing(obj.detail);
                setFooterPenjualanLeasing(obj.footer_leasing,obj.footer);
            },
            error:function(event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function setDetailPenjualanLeasing(detail){
        // Menampilkan kolom detail start
        var tr_name = 1;
        $(".tr-body").html('');
        $.each(detail, function(key, data){
            $(".tr-body").append('<tr class="row'+tr_name+'">');
            $(".row"+tr_name).append('<td>'+tr_name+'</td>');
            var coldetail = 1;
            var subtotal = 0;
            $.each(data, function(key, value){
                $(".row"+tr_name).append('<td>'+value+'</td>');
                if(!isNaN(value)){
                    subtotal = subtotal+Number(value);
                }
                coldetail++;
            });
            $(".row"+tr_name).append('<td>'+subtotal+'</td>');
            $(".tr-body").append('</tr>');
            tr_name++;
        });
        // Menampilkan kolom detail end
    }

    function setFooterPenjualanLeasing(footer_leasing,footer){
        
        // Menampilkan kolom footer 1 start
        //console.log(footer_leasing);
        var tr_name = 'leasing';
        $(".tr-foot").html('');
        $.each(footer_leasing, function(key, data){
            $(".tr-foot").append('<tr class="row'+tr_name+'">');
            $(".row"+tr_name).append('<td colspan="4" style="background-color: #f9d4d4;font-weight: bold ;">TOTAL PER LEASING</td>');
            var coldetail = 1;
            var subtotal = 0;
            $.each(data, function(key, value){
                if(coldetail>1){
                    $(".row"+tr_name).append('<td colspan="4" style="text-align:center;background-color: #f9d4d4;font-weight: bold ;">'+value+'</td>');
                }else{
                    $(".row"+tr_name).append('<td style="text-align:center;background-color: #f9d4d4;font-weight: bold ;">'+value+'</td>');
                }
                if(!isNaN(value)){
                    subtotal = subtotal+Number(value);
                }
                coldetail++;
            });
            $(".row"+tr_name).append('<td style="text-align:center;background-color: #f9d4d4;font-weight: bold ;">'+subtotal+'</td>');
            $(".tr-foot").append('</tr>');
            tr_name++;
        });
        // Menampilkan kolom footer 1 end 
    
        // Menampilkan kolom footer 2 start
        tr_name = 'total';
        $.each(footer, function(key, data){
            $(".tr-foot").append('<tr class="row'+tr_name+'">');
            $(".row"+tr_name).append('<td colspan="4" style="background-color: #d5d4f9;font-weight: bold ;">TOTAL</td>');
            var coldetail = 1;
            var subtotal = 0;
            $.each(data, function(key, value){
                $(".row"+tr_name).append('<td style="text-align:center;background-color: #d5d4f9;font-weight: bold ;">'+value+'</td>');
                if(!isNaN(value)){
                    subtotal = subtotal+Number(value);
                }
                coldetail++;
            });
            $(".row"+tr_name).append('<td style="text-align:center;background-color: #d5d4f9;font-weight: bold ;">'+subtotal+'</td>');
            $(".tr-foot").append('</tr>');
            tr_name++;
        });
        // Menampilkan kolom footer 2 end    

    }
});
</script>
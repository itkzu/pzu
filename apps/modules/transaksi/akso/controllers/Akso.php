<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Akso
 *
 * @author
 */
class Akso extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('akso/submit'),
            'add' => site_url('akso/add'),
            'edit' => site_url('akso/edit'),
            'reload' => site_url('akso'),
        );
        $this->load->model('akso_qry');
        /*
        $nodo = $this->akso_qry->getDataDo();
        foreach ($nodo as $value) {
            $this->data['nodo'][$value['nodo']] = $value['nodo'];
        }
        $kdaks = $this->akso_qry->getKodeakso();
        $this->data['kdaks'] = array(
            "" => "-- Pilih Kode Aksesoris --",
          );
        foreach ($kdaks as $value) {
            $this->data['kdaks'][$value['kdaks']] = $value['kdaks']." - ".$value['nmaks'];
          }
          */
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function getKodeAkso() {
        echo $this->akso_qry->getKodeAkso();
    }

    public function getNama() {
        echo $this->akso_qry->getNama();
    }

    public function getData() {
        echo $this->akso_qry->getData();
    }

    public function json_dgview() {
        echo $this->akso_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->akso_qry->json_dgview_detail();
    }

    public function addDetail() {
        echo $this->akso_qry->addDetail();
    }

    public function detaildeleted() {
        echo $this->akso_qry->detaildeleted();
    }
    public function delete() {
        echo $this->akso_qry->delete();
    }

    public function submit() {
        echo $this->akso_qry->submit();
    }

    private function _init_add(){

        if(isset($_POST['ketnondo']) && strtoupper($_POST['ketnondo']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'noakso'=> array(
                    'placeholder' => 'No. Transaksi',
                    //'type'        => 'hidden',
                    'id'          => 'noakso',
                    'name'        => 'noakso',
                    'value'       => set_value('noakso'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglakso'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tglakso',
                    'name'        => 'tglakso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
      		   'ketnondo'=> array(
                'placeholder' => 'Pengeluaran Non DO',
      					'id'          => 'ketnondo',
      					'value'       => 't',
      					'checked'     => $faktif,
      					'class'       => 'filled-in',
      					'name'		  => 'ketnondo'
      			),
            'ketnodo'=> array(
                    'id'    => 'ketnodo',
                    'class' => 'form-control',
                    'value'    => set_value('nodo'),
                    'name'     => 'ketnodo',
                    'type'    => 'hidden'
            ),
            'nodo'=> array(
                    'attr'        => array(
                        'id'    => 'nodo',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('nodo'),
                    'name'     => 'nodo',
                    'required'    => '',
                    'placeholder' => 'No. DO'
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'      => 'tgldo',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('tgldo'),
                    'name'     => 'tgldo',
                    'readonly' => ''
            ),
            'nospk'=> array(
                    'placeholder' => 'No SPK',
                    'id'      => 'nospk',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nospk'),
                    'name'     => 'nospk',
                    'readonly' => ''
            ),
            'nmunit'=> array(
                    'placeholder' => 'Nama Unit',
                    'id'      => 'nmunit',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nmunit'),
                    'name'     => 'nmunit',
                    'readonly' => ''
            ),
            'nmcust'=> array(
                    'placeholder' => 'Nama Konsumen',
                    'id'      => 'nmcust',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nmcust'),
                    'name'     => 'nmcust',
                    'readonly' => ''
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    'id'      => 'tahun',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('tahun'),
                    'name'     => 'tahun',
                    'readonly' => ''
            ),
            'kdunit'=> array(
                    'placeholder' => 'Kode Unit',
                    'id'      => 'kdunit',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('kdunit'),
                    'name'     => 'kdunit',
                    'readonly' => ''
            ),
            'warna'=> array(
                    'placeholder' => 'Warna',
                    'id'      => 'warna',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('warna'),
                    'name'     => 'warna',
                    'readonly' => ''
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
            //        'required'    => ''
            ),
            'kdaks'=> array(
                    'attr'        => array(
                        'id'    => 'kdaks',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kdaks'),
                    'name'     => 'kdaks',
                    'placeholder' => 'Kode Barang'
            ),
            'nmaks'=> array(
                    'attr'        => array(
                        'id'    => 'nmaks',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmaks'),
                    'name'     => 'nmaks',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            )
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nojurnal = $this->uri->segment(3);
        }
        $this->_check_id($nojurnal);

        if($this->val[0]['nodo'] == ''){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
               'noakso'=> array(
                        'placeholder' => 'No. Transaksi',
                        //'type'        => 'hidden',
                        'id'          => 'noakso',
                        'name'        => 'noakso',
                        'value'       => $this->val[0]['noakso'],
                        'class'       => 'form-control',
                        'style'       => '',
                        'readonly'    => '',
                ),
               'tglakso'=> array(
                        'placeholder' => 'Tanggal Transaksi',
                        'id'          => 'tglakso',
                        'name'        => 'tglakso',
                        'value'       => $this->apps->dateConvert($this->val[0]['tglakso']),
                        'class'       => 'form-control calendar',
                        'style'       => '',
                ),
          		   'ketnondo'=> array(
                    'placeholder' => 'Pengeluaran Non DO',
          					'id'          => 'ketnondo',
          					'value'       => 't',
          					'checked'     => $faktifx,
          					'class'       => 'filled-in',
          					'name'		  => 'ketnondo'
          			),
                'ketnodo'=> array(
                        'id'    => 'ketnodo',
                        'class' => 'form-control',
                        'value'    => $this->val[0]['nodo'],
                        'name'     => 'ketnodo',
                        'type'    => 'hidden'
                ),
                'nodo'=> array(
                        'attr'        => array(
                            'id'    => 'nodo',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                        'value'    => '',
                        'name'     => 'nodo',
                        'required'    => '',
                        'placeholder' => 'No. DO',
                ),
                'ket'=> array(
                        'placeholder' => 'Keterangan',
                        'id'      => 'ket',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => $this->val[0]['ket'],
                        'name'     => 'ket',
                //        'required'    => '',
                //        'onkeyup'     => 'myFunction()',
                //        'style'       => 'text-transform: uppercase;',
                ),
                'tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        'id'      => 'tgldo',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('tgldo'),
                        'name'     => 'tgldo',
                        'readonly' => ''
                ),
                'nospk'=> array(
                        'placeholder' => 'No SPK',
                        'id'      => 'nospk',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nospk'),
                        'name'     => 'nospk',
                        'readonly' => ''
                ),
                'nmunit'=> array(
                        'placeholder' => 'Nama Unit',
                        'id'      => 'nmunit',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nmunit'),
                        'name'     => 'nmunit',
                        'readonly' => ''
                ),
                'nmcust'=> array(
                        'placeholder' => 'Nama Konsumen',
                        'id'      => 'nmcust',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nmcust'),
                        'name'     => 'nmcust',
                        'readonly' => ''
                ),
                'tahun'=> array(
                        'placeholder' => 'Tahun',
                        'id'      => 'tahun',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('tahun'),
                        'name'     => 'tahun',
                        'readonly' => ''
                ),
                'kdunit'=> array(
                        'placeholder' => 'Kode Unit',
                        'id'      => 'kdunit',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('kdunit'),
                        'name'     => 'kdunit',
                        'readonly' => ''
                ),
                'warna'=> array(
                        'placeholder' => 'Warna',
                        'id'      => 'warna',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('warna'),
                        'name'     => 'warna',
                        'readonly' => ''
                ),
                'nourut'=> array(
                        'id'    => 'nourut',
                        'type'    => 'hidden',
                        'class' => 'form-control',
                        'data'     => '',
                        'value'    => set_value('nourut'),
                        'name'     => 'nourut',
                //        'required'    => ''
                ),
                'kdaks'=> array(
                        'attr'        => array(
                            'id'    => 'kdaks',
                            'class' => 'form-control',
                        ),
                        'data'     =>  '',
                        'value'    => set_value('kdaks'),
                        'name'     => 'kdaks',
                        'placeholder' => 'Kode Barang'
                ),
                'nmaks'=> array(
                        'attr'        => array(
                            'id'    => 'nmaks',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                        'class' => 'form-control',
                        'value'    => set_value('nmaks'),
                        'name'     => 'nmaks',
                        'readonly' => '',
                ),
                'qty'=> array(
                        'placeholder' => 'Quantity',
                        'id'    => 'qty',
                        'class' => 'form-control',
                        'value'    => set_value('qty'),
                        'name'     => 'qty',
                        'required'    => '',
                )
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->akso_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'noref',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

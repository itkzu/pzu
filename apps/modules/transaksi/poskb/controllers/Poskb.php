<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Poskb
 *
 * @author
 */

class Poskb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('poskb/submit'),
            'add' => site_url('poskb/add'),
            'edit' => site_url('poskb/edit'),
            'reload' => site_url('poskb'),
        );
        $this->load->model('poskb_qry');
        $getref = $this->poskb_qry->getref();
        foreach ($getref as $value) {
            $this->data['kdkb'][$value['kdkb']] = $value['nmkb'];
        }     

        $this->data['jenis'] = array(
            "D" => "DEBET",
            "K" => "KREDIT"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->poskb_qry->json_dgview();
    }

    public function det_json_dgview() {
        echo $this->poskb_qry->det_json_dgview();
    }

    public function det_k_json_dgview() {
        echo $this->poskb_qry->det_k_json_dgview();
    }

    public function proses(){
        echo $this->poskb_qry->proses();
    }

    public function getkdrefkb(){
        echo $this->poskb_qry->getkdrefkb();
    }

    public function det_kb(){
        echo $this->poskb_qry->det_kb();
    }



    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->poskb_qry->submit();
            echo $res;
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    private function _init_add(){
        $this->data['form'] = array( 
            'kdkb' => array(
                  'placeholder' => 'Ref. Kas/Bank',
                  'attr'        => array(
                      'id'      => 'kdkb',
                      'class'   => 'form-control',
                  ),
                  'data'        => $this->data['kdkb'],
                  'class'       => 'form-control',
                  'value'       => set_value('kdkb'),
                  'name'        => 'kdkb',
                    // 'readonly' => '',
            ),
            
           'nokb'=> array(
                    'placeholder' => ' No KB',
                    'type'        => 'hidden',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    'required'    => '',
            ), 
           'nourut'=> array(
                    'placeholder' => ' No Urut',
                    // 'type'        => 'hidden',
                    'id'          => 'nourut',
                    'name'        => 'nourut',
                    'value'       => set_value('nourut'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'nmkb'=> array(
                    'placeholder' => ' Ref. Kas/Bank',
                    'id'          => 'nmkb',
                    'name'        => 'nmkb',
                    'value'       => set_value('nmkb'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'nocetak'=> array(
                    'placeholder' => ' No. Bukti',
                    'id'          => 'nocetak',
                    'name'        => 'nocetak',
                    'value'       => set_value('nocetak'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'tglkb'=> array(
                    'placeholder' => ' Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => set_value('tglkb'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'drkpd'=> array(
                    'placeholder' => ' Dari/Kepada',
                    'id'          => 'drkpd',
                    'name'        => 'drkpd',
                    'value'       => set_value('drkpd'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
                    // 'style'       => 'min-height: 108px;height: 108px; resize: vertical;'
            ),
           'ket'=> array(
                    'placeholder' => ' Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ), 
            'jenis' => array(
                    'placeholder' => ' Debet/Kredit',
                    'attr'        => array(
                        'id'      => 'jenis',
                        'class'   => 'form-control',
                    ),
                    'data'        => $this->data['jenis'],
                    'class'       => 'form-control',
                    'value'       => set_value('jenis'),
                    'name'        => 'jenis',
                    'required'    => '',
                    'readonly'    => '', 
            ), 
           'nilai'=> array(
                    'placeholder' => ' Nilai',
                    'id'          => 'nilai',
                    'name'        => 'nilai',
                    'value'       => set_value('nilai'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ), 
            'kdrefkb'=> array(
                    'placeholder' => ' Jenis Transaksi',
                    'attr'        => array(
                        'id'    => 'kdrefkb',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdrefkb'),
                    'name'     => 'kdrefkb',
                    'style'       => 'text-transform: uppercase;',
            ), 
            'nmrefkb'=> array(
                    'placeholder' => ' Jenis Transaksi',
                    'id'          => 'nmrefkb',
                    'name'        => 'nmrefkb',
                    'value'       => set_value('nmrefkb'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ), 
            'stat'=> array(
                    'placeholder' => ' Status',
                    'type'        => 'hidden',
                    'id'          => 'stat',
                    'name'        => 'stat',
                    'value'       => set_value('stat'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ), 
            'jmemo'=> array(
                    'placeholder' => ' Jurnal Memo',
                    'id'          => 'jmemo',
                    // 'value'       => 't',
                    'checked'     => false,
                    'class'       => 'custom-control-input',
                    'name'        => 'jmemo',
                    'type'        => 'checkbox',
                ),
            
        // nodo, tgldo, nama, alamat, kel, kec, kota, nohp, kdtipe, nmtipe, nosin, nora
        );
    }

    private function validate() {
        $config = array(
            array(
                    'field' => 'nodo',
                    'label' => 'No. DO',
                    'rules' => 'required|max_length[10]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptunitentry_qry
 *
 * @author adi
 */
class Rptunitentry_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['tahun']) ){
			if($_GET['tahun']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$tahun = $_GET['tahun'];//$tgl1[1].$tgl1[0];
			}else{
				$tahun = '';
			} 
		}else{
			$tahun = '';
		}        
		

//echo "<script> console.log('PHP: ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";

//		$aColumns = array('dt','pk','no_ktp','nm','adr','subtotsrv','subtotitem','subtot','distrs','tottrs');

							//, 'nmtipe'
		$aColumns = array('bl'
							, 'qty_day'
							, 'qty_tot'
							, 'qty_ue'
							, 'qty_reg'
							, 'qty_kpb'
							, 'qty_pnj'
		);


/*
		$aColumns = array('pk'
							, 'dt'
							, 'kpbto'
							, 'distrs'
		);
*/

		$sIndexColumn = "bl";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}


		$sTable = "(

SELECT y.bl
	, count(y.bl) as qty_day
	, sum(y.qty_tot) AS qty_tot
	, sum(y.qty_ue) AS qty_ue
	, sum(y.qty_reg) AS qty_reg
	, sum(y.qty_kpb) AS qty_kpb
	, sum(y.qty_pnj) AS qty_pnj
FROM
(
SELECT x.bl
	, x.mt
	, sum(x.reg + x.kpb+pnj) AS qty_tot
	, sum(x.reg + x.kpb) AS qty_ue
	, sum(x.reg) AS qty_reg
	, sum(x.kpb) AS qty_kpb
	, sum(x.pnj) AS qty_pnj
FROM
(
SELECT 
	 format(dt,'mm') AS mt
	 , format(dt,'ddmm') AS dtx
	 , iif(format(dt,'mm')='01','JANUARI',
	 iif(format(dt,'mm')='02','FEBRUARI',
	 iif(format(dt,'mm')='03','MARET',
	 iif(format(dt,'mm')='04','APRIL',
	 iif(format(dt,'mm')='05','MEI',
	 iif(format(dt,'mm')='06','JUNI',
	 iif(format(dt,'mm')='07','JULI',
	 iif(format(dt,'mm')='08','AGUSTUS',
	 iif(format(dt,'mm')='09','SEPTEMBER',
	 iif(format(dt,'mm')='10','OKTOBER',
	 iif(format(dt,'mm')='11','NOVEMBER',
	 iif(format(dt,'mm')='12','DESEMBER','')))))))))))) AS bl

	 , iif(kpbto='',1,0) as reg
 	, iif(kpbto='',0,1) as kpb
 	, 0 AS pnj
FROM t_wi_hd
WHERE format(dt,'yyyy')='{$tahun}'

UNION ALL

SELECT 
	 format(dt,'mm') AS mt
	 , format(dt,'ddmm') AS dtx 
	 , iif(format(dt,'mm')='01','JANUARI',
	 iif(format(dt,'mm')='02','FEBRUARI',
	 iif(format(dt,'mm')='03','MARET',
	 iif(format(dt,'mm')='04','APRIL',
	 iif(format(dt,'mm')='05','MEI',
	 iif(format(dt,'mm')='06','JUNI',
	 iif(format(dt,'mm')='07','JULI',
	 iif(format(dt,'mm')='08','AGUSTUS',
	 iif(format(dt,'mm')='09','SEPTEMBER',
	 iif(format(dt,'mm')='10','OKTOBER',
	 iif(format(dt,'mm')='11','NOVEMBER',
	 iif(format(dt,'mm')='12','DESEMBER','')))))))))))) AS bl
	, 0 as reg
 	, 0 as kpb
 	, 1 AS pnj
FROM t_si_hd
WHERE format(dt,'yyyy')='{$tahun}'
) AS x
GROUP BY x.bl, x.mt, x.dtx
) AS y
GROUP BY y.bl, y.mt
ORDER BY y.mt
					) AS A ";



		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
//        echo $sQuery;
		
		$rResult = $dbbengkel->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $dbbengkel->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $dbbengkel->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}
	
	public function submit(){

	}
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    #modal_kbm .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
        <div class="box box-danger">
            <!-- <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="col-xs-12 box-header box-view">
              <div class="col-xs-4"> 
                <button type="button" class="btn btn-primary btn-submit"> Simpan </button>
                <button type="button" class="btn btn-default btn-batal"> Batal </button> 
              </div>
              <div class="col-xs-2">
                <div class="row"> 
                </div>
              </div>
              <div class="col-xs-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak Nota</button>
                </div>
              </div>
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['nokb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7">  
                                      <?php
                                          echo form_input($form['nokb']);
                                          echo form_error('nokb','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['refkb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                        echo form_dropdown($form['refkb']['name'],
                                                          $form['refkb']['data'],
                                                          $form['refkb']['value'],
                                                          $form['refkb']['attr']);
                                        echo form_error('refkb','<div class="note">','</div>');
                                      ?>
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3"> 
                                      <?php
                                          echo form_input($form['day']);
                                          echo form_error('day','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['periode']);
                                          echo form_error('periode','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['periode']['placeholder']); ?>
                                  </div>
                              </div> 

                          </div>
                        </div>   

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">  

                              <div class="col-xs-12">  
                                      <?php
                                          echo form_input($form['dk']);
                                          echo form_error('dk','<div class="note">','</div>');
                                      ?> 
                              </div>
                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['jnstrans']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8">
                                      <?php
                                        echo form_dropdown($form['jnstrans']['name'],
                                                          $form['jnstrans']['data'],
                                                          $form['jnstrans']['value'],
                                                          $form['jnstrans']['attr']);
                                        echo form_error('jnstrans','<div class="note">','</div>');
                                      ?>
                              </div>

                          </div>
                        </div> 

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <button type="button" class="btn btn-primary btn-set"> SET </button>
                                      <button type="button" class="btn btn-warning btn-unset"> UNSET </button>
                                  </div>
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div> 

                  <!--PEMBAYARAN KONSUMEN-->
                  <div class="pkons">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>PEMBAYARAN KONSUMEN (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                   
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_cari']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_dropdown($form['byrkons_cari']['name'],
                                                              $form['byrkons_cari']['data'],
                                                              $form['byrkons_cari']['value'],
                                                              $form['byrkons_cari']['attr']);
                                            echo form_error('byrkons_cari','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_noso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['byrkons_noso']);
                                            echo form_error('byrkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nodo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['byrkons_nodo']);
                                            echo form_error('byrkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nmsales']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_nmsales']);
                                            echo form_error('byrkons_nmsales','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_tglso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['byrkons_tglso']);
                                            echo form_error('byrkons_tglso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_tgldo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['byrkons_tgldo']);
                                            echo form_error('byrkons_tgldo','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nmspv']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_nmspv']);
                                            echo form_error('byrkons_nmspv','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12"> 
                      <br>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nama']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['byrkons_nama']);
                                            echo form_error('byrkons_nama','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nosin']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['byrkons_nosin']);
                                            echo form_error('byrkons_nosin','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['byrkons_nora']);
                                            echo form_error('byrkons_nora','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_alamat']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['byrkons_alamat']);
                                            echo form_error('byrkons_alamat','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_kdtipe']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['byrkons_kdtipe']);
                                            echo form_error('byrkons_kdtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['byrkons_nmtipe']);
                                            echo form_error('byrkons_nmtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['byrkons_kdleas']);
                                            echo form_error('byrkons_kdleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_warna']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_input($form['byrkons_warna']);
                                            echo form_error('byrkons_warna','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['byrkons_tahun']);
                                            echo form_error('byrkons_tahun','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_kdprogleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['byrkons_kdprogleas']);
                                            echo form_error('byrkons_kdprogleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_status']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['byrkons_status']);
                                            echo form_error('byrkons_status','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12"> 
                      <br>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_hjnet']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_hjnet']);
                                            echo form_error('byrkons_hjnet','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_um']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_um']);
                                            echo form_error('byrkons_um','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div>

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_pelunasan']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_pelunasan']);
                                            echo form_error('byrkons_pelunasan','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_byr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_byr1']);
                                            echo form_error('byrkons_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div>

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_byr2']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_byr2']);
                                            echo form_error('byrkons_byr2','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_nbyr']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_nbyr']);
                                            echo form_error('byrkons_nbyr','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_saldoum']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_saldoum']);
                                            echo form_error('byrkons_saldoum','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div>

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['byrkons_saldo_pelunasan']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['byrkons_saldo_pelunasan']);
                                            echo form_error('byrkons_saldo_pelunasan','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                      <br>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-6">
                            <label>No. Cetak</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pkons_lanjut'),'true') . '<b>&nbsp; Lanjutkan </b>');
                              echo '</div>'; 
                          ?>
                        </div>
                        <div class="col-xs-2"> 
                          <?php  
                              echo form_input($form['byrkons_nocetak']);
                              echo form_error('byrkons_nocetak','<div class="note">','</div>');
                          ?>
                        </div>
                        <div class="col-xs-2"> 
                          <?php  
                              echo form_label($form['byrkons_jmltrans']['placeholder']); 
                          ?>
                        </div>
                        <div class="col-xs-1"> 
                          <?php  
                              echo form_input($form['byrkons_jmltrans']);
                              echo form_error('byrkons_jmltrans','<div class="note">','</div>');
                          ?>
                        </div>
                        <div class="col-xs-3"> 
                          <?php  
                              echo form_input($form['byrkons_noctk']);
                              echo form_error('byrkons_noctk','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pkons_baru'),'true') . '<b>&nbsp; No. Baru </b>');
                              echo '</div>'; 
                          ?>
                        </div> 
                      </div>
                    </div>  
                  </div> 

                  <!--PEMBAYARAN LEASING-->
                  <div class="pleas">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>PEMBAYARAN LEASING (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_cari']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_dropdown($form['pleas_cari']['name'],
                                                              $form['pleas_cari']['data'],
                                                              $form['pleas_cari']['value'],
                                                              $form['pleas_cari']['attr']);
                                            echo form_error('pleas_cari','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_noso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['pleas_noso']);
                                            echo form_error('pleas_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nodo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['pleas_nodo']);
                                            echo form_error('pleas_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nmsales']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_nmsales']);
                                            echo form_error('pleas_nmsales','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_tglso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['pleas_tglso']);
                                            echo form_error('pleas_tglso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_tgldo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['pleas_tgldo']);
                                            echo form_error('pleas_tgldo','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nmspv']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_nmspv']);
                                            echo form_error('pleas_nmspv','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12"> 
                      <br>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nama']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['pleas_nama']);
                                            echo form_error('pleas_nama','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nosin']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['pleas_nosin']);
                                            echo form_error('pleas_nosin','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['pleas_nora']);
                                            echo form_error('pleas_nora','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_alamat']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['pleas_alamat']);
                                            echo form_error('pleas_alamat','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_kdtipe']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['pleas_kdtipe']);
                                            echo form_error('pleas_kdtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['pleas_nmtipe']);
                                            echo form_error('pleas_nmtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['pleas_kdleas']);
                                            echo form_error('pleas_kdleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_warna']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_input($form['pleas_warna']);
                                            echo form_error('pleas_warna','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['pleas_tahun']);
                                            echo form_error('pleas_tahun','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_kdprogleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['pleas_kdprogleas']);
                                            echo form_error('pleas_kdprogleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_status']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['pleas_status']);
                                            echo form_error('pleas_status','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12"> 
                      <br>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_pelunasan']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_pelunasan']);
                                            echo form_error('pleas_pelunasan','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_jp']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_jp']);
                                            echo form_error('pleas_jp','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_byr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_byr1']);
                                            echo form_error('pleas_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_byr2']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_byr2']);
                                            echo form_error('pleas_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nbyr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_nbyr1']);
                                            echo form_error('pleas_nbyr1','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['pleas_nbyr2']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['pleas_nbyr2']);
                                            echo form_error('pleas_nbyr2','<div class="note">','</div>');
                                        ?> 
                                </div> 
                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                      <br>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-6">
                            <label>No. Cetak</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pleas_lanjut'),'true') . '<b>&nbsp; Lanjutkan </b>');
                              echo '</div>'; 
                          ?>
                        </div>
                        <div class="col-xs-3"> 
                          <?php  
                              echo form_input($form['pleas_nocetak']);
                              echo form_error('pleas_nocetak','<div class="note">','</div>');
                          ?>
                        </div>
                        <div class="col-xs-2"> 
                          <?php  
                              echo form_label($form['pleas_jmltrans']['placeholder']); 
                          ?>
                        </div>
                        <div class="col-xs-1"> 
                          <?php  
                              echo form_input($form['pleas_jmltrans']);
                              echo form_error('pleas_jmltrans','<div class="note">','</div>');
                          ?>
                        </div>
                        <div class="col-xs-3"> 
                          <?php  
                              echo form_input($form['pleas_noctk']);
                              echo form_error('pleas_noctk','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>  
                    
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-2"> 
                          <?php 
                              echo '<div class="radio">';
                              echo form_label(form_radio(array('name' => 'nocetak','id'=>'pleas_baru'),'true') . '<b>&nbsp; No. Baru </b>');
                              echo '</div>'; 
                          ?>
                        </div> 
                      </div>
                    </div>  
                  </div> 

                  <!--REFUND LEASING-->
                  <div class="rleas">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>REFUND LEASING (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                          echo form_dropdown($form['rleas_kdleas']['name'],
                                                            $form['rleas_kdleas']['data'],
                                                            $form['rleas_kdleas']['value'],
                                                            $form['rleas_kdleas']['attr']);
                                          echo form_error('rleas_kdleas','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_jenis']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                          echo form_dropdown($form['rleas_jenis']['name'],
                                                            $form['rleas_jenis']['data'],
                                                            $form['rleas_jenis']['value'],
                                                            $form['rleas_jenis']['attr']);
                                          echo form_error('rleas_jenis','<div class="note">','</div>');  
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>  
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_nilai']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['rleas_nilai']);
                                            echo form_error('rleas_nilai','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  
                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['rleas_ket']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_input($form['rleas_ket']);
                                            echo form_error('rleas_ket','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>   
                  </div> 

                  <!--PENCAIRAN SUBSIDI-->
                  <div class="psub">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>PENCAIRAN SUBSIDI (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                          echo form_dropdown($form['psub_kdleas']['name'],
                                                            $form['psub_kdleas']['data'],
                                                            $form['psub_kdleas']['value'],
                                                            $form['psub_kdleas']['attr']);
                                          echo form_error('psub_kdleas','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                                <div class="col-xs-3">  
                                </div>  

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_ncair']['placeholder']); ?>
                                    </div>
                                </div>

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['psub_ncair']);
                                          echo form_error('psub_ncair','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_selisih']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['psub_selisih']);
                                          echo form_error('psub_selisih','<div class="note">','</div>'); 
                                        ?> 
                                </div>   

                                <div class="col-xs-6">  
                                </div>   

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_ket']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_input($form['psub_ket']);
                                          echo form_error('psub_ket','<div class="note">','</div>');   
                                        ?> 
                                </div>  

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_totsub']['placeholder']); ?>
                                    </div>
                                </div>

                            </div>
                          </div>  

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row">  

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['psub_totsub']);
                                          echo form_error('psub_totsub','<div class="note">','</div>');   
                                        ?> 
                                </div>  

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['psub_selsbg']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                          echo form_dropdown($form['psub_selsbg']['name'],
                                                            $form['psub_selsbg']['data'],
                                                            $form['psub_selsbg']['value'],
                                                            $form['psub_selsbg']['attr']);
                                          echo form_error('psub_selsbg','<div class="note">','</div>'); 
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div> 

                    <div class="col-xs-12">
                    <!-- <p><a id="add" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-del"><i class="fa fa-minus"></i> Hapus</a></p> -->
                    <p id="alldata" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered tbl_subkasbon dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Kasbon</th> 
                            <th style="text-align: center;">Tgl Kasbon</th> 
                            <th style="text-align: center;">Keterangan Kasbon</th> 
                            <th style="text-align: center;">Catatan</th> 
                            <th style="text-align: center;">Nilai Subsidi</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div> 

                  <!--KAS/BANK MASUK UMUM-->
                  <div class="kbm_umum">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>K/B MASUK - UMUM (D)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-xs-12"> 
                    <p><a id="add" class="btn btn-primary btn-addkbm"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-delkbm"><i class="fa fa-minus"></i> Hapus</a></p>
                    <p id="kbm_urut" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable tbl_kbm display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">CL</th>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Jenis Penerimaan</th> 
                            <th style="text-align: center;">Diterima Dari</th> 
                            <th style="text-align: center;">No. Faktur/Nota</th> 
                            <th style="text-align: center;">Keterangan</th> 
                            <th style="text-align: center;">Nominal</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div> 

                  <!--PENGEMBALIAN KONSUMEN-->
                  <div class="balik_kons">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>PENGEMBALIAN KONSUMEN (K)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>  
                   
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_cari']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_dropdown($form['blkkons_cari']['name'],
                                                              $form['blkkons_cari']['data'],
                                                              $form['blkkons_cari']['value'],
                                                              $form['blkkons_cari']['attr']);
                                            echo form_error('blkkons_cari','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_noso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['blkkons_noso']);
                                            echo form_error('blkkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nodo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['blkkons_nodo']);
                                            echo form_error('blkkons_noso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nmsales']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_nmsales']);
                                            echo form_error('blkkons_nmsales','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_tglso']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['blkkons_tglso']);
                                            echo form_error('blkkons_tglso','<div class="note">','</div>');
                                        ?> 
                                </div> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_tgldo']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['blkkons_tgldo']);
                                            echo form_error('blkkons_tgldo','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                          <div class="col-xs-5 col-md-5 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nmspv']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_nmspv']);
                                            echo form_error('blkkons_nmspv','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12"> 
                      <br>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nama']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blkkons_nama']);
                                            echo form_error('blkkons_nama','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_nosin']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['blkkons_nosin']);
                                            echo form_error('blkkons_nosin','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['blkkons_nora']);
                                            echo form_error('blkkons_nora','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_alamat']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blkkons_alamat']);
                                            echo form_error('blkkons_alamat','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_kdtipe']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['blkkons_kdtipe']);
                                            echo form_error('blkkons_kdtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['blkkons_nmtipe']);
                                            echo form_error('blkkons_nmtipe','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_kdleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blkkons_kdleas']);
                                            echo form_error('blkkons_kdleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_warna']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-6"> 
                                        <?php
                                            echo form_input($form['blkkons_warna']);
                                            echo form_error('blkkons_warna','<div class="note">','</div>');
                                        ?> 
                                </div>  

                                <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['blkkons_tahun']);
                                            echo form_error('blkkons_tahun','<div class="note">','</div>');
                                        ?> 
                                </div>  
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_kdprogleas']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blkkons_kdprogleas']);
                                            echo form_error('blkkons_kdprogleas','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-6 col-md-6 ">
                            <div class="row"> 

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_status']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['blkkons_status']);
                                            echo form_error('blkkons_status','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12"> 
                      <br>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_hjnet']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_hjnet']);
                                            echo form_error('blkkons_hjnet','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_arum']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_arum']);
                                            echo form_error('blkkons_arum','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_byr1']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_byr1']);
                                            echo form_error('blkkons_byr1','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div>
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_pengembalian']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_pengembalian']);
                                            echo form_error('blkkons_pengembalian','<div class="note">','</div>');
                                        ?> 
                                </div>  

                            </div>
                          </div> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <?php echo form_label($form['blkkons_lbhbyr']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-7"> 
                                        <?php
                                            echo form_input($form['blkkons_lbhbyr']);
                                            echo form_error('blkkons_lbhbyr','<div class="note">','</div>');
                                        ?> 
                                </div>   
                            </div>
                          </div> 

                      </div>
                    </div> 
                  
                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-4 col-md-4 ">
                            <div class="row"> 

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <?php echo form_label('<small><i> * Nilai Pengembalian tidak boleh melebihi Nilai Terbayar</i></small>'); ?>
                                    </div>
                                </div>  

                            </div>
                          </div>  

                      </div>
                    </div> 
                  </div> 

                  <!--KAS/BANK KELUAR UMUM-->
                  <div class="kbk_umum">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>K/B KELUAR - UMUM (K)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-xs-12"> 
                    <p><a id="add" class="btn btn-primary btn-addkbk"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-delkbk"><i class="fa fa-minus"></i> Hapus</a></p>
                    <p id="kbk_urut" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable tbl_kbk display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">CL</th>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Jenis Penerimaan</th> 
                            <th style="text-align: center;">Diterima Dari</th> 
                            <th style="text-align: center;">No. Faktur/Nota</th> 
                            <th style="text-align: center;">Keterangan</th> 
                            <th style="text-align: center;">Nominal</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div> 

                  <!--REALISASI KASBON-->
                  <div class="rkasbon">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>REALISASI KASBON (K)</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-xs-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link " id="list-tfa" name="list-tfa" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Transaksi</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="input-tfa" name="input-tfa" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Kasbon</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-tfa">

                                <div class="col-xs-12">
                                  <br>
                                  <p><a id="add" class="btn btn-primary btn-addtkasbon"><i class="fa fa-plus"></i> Tambah</a>
                                  <a id="del" class="btn btn-danger btn-deltkasbon"><i class="fa fa-minus"></i> Hapus</a></p>
                                  <p id="tkasbon_urut" class="kata"></p>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable tbl_tkasbon display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">CL</th>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">Jenis Pengeluaran</th>
                                                    <th style="text-align: center;">Diserahkan Kepada</th>
                                                    <th style="text-align: center;">No. Faktur/Nota</th> 
                                                    <th style="text-align: center;">Keterangan</th> 
                                                    <th style="text-align: center;">Nominal</th> 
                                                </tr>
                                            </thead> 
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-tfa"> 

                                <div class="col-xs-12">
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_nokasbon']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <?php
                                                echo form_input($form['rkasbon_nokasbon']);
                                                echo form_error('rkasbon_nokasbon','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_nama']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-5">
                                            <?php
                                                echo form_input($form['rkasbon_nama']);
                                                echo form_error('rkasbon_nama','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div>   

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_tglkasbon']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <?php
                                                echo form_input($form['rkasbon_tglkasbon']);
                                                echo form_error('rkasbon_tglkasbon','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_ket']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-5">
                                            <?php
                                                echo form_input($form['rkasbon_ket']);
                                                echo form_error('rkasbon_ket','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_kb']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <?php
                                                echo form_input($form['rkasbon_kb']);
                                                echo form_error('rkasbon_kb','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['rkasbon_nkasbon']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <?php
                                                echo form_input($form['rkasbon_nkasbon']);
                                                echo form_error('rkasbon_nkasbon','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div>   

                                <div class="col-xs-12">
                                  <br>
                                  <p><a id="add" class="btn btn-primary btn-addrkasbon"><i class="fa fa-plus"></i> Tambah</a>
                                  <a id="del" class="btn btn-danger btn-delrkasbon"><i class="fa fa-minus"></i> Hapus</a></p>
                                  <p id="rkasbon_urut" class="kata"></p>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable tbl_kasbon display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">CL</th>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">No. Kasbon</th>
                                                    <th style="text-align: center;">Tgl Kasbon</th>
                                                    <th style="text-align: center;">Nilai Kasbon</th> 
                                                    <th style="text-align: center;">Penerima</th> 
                                                    <th style="text-align: center;">Keterangan</th> 
                                                    <th style="text-align: center;">Kas/Bank</th> 
                                                </tr>
                                            </thead> 
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                  </div>

                </div>
          </div>

        </form>
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- modal dialog -->
<div id="modal_trans" class="modal fade">
    <div class="modal-dialog"  style="width: 75%;">
        <div class="modal-content">
            <form id="modalf" name="modalf" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Detail Tagihan BBN</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nosin']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['nosin']);
                                          echo form_error('nosin','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nodo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['nodo']);
                                          echo form_error('nodo','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nora']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['nora']);
                                          echo form_error('nora','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgldo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['tgldo']);
                                          echo form_error('tgldo','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row"> 
                        <br>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nama']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['nama']);
                                          echo form_error('nama','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['alamat']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['alamat']);
                                          echo form_error('alamat','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['kota']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['kota']);
                                          echo form_error('kota','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['bbn']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['bbn']);
                                          echo form_error('bbn','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['kdtipe']);
                                          echo form_error('kdtipe','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['jasa']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['jasa']);
                                          echo form_error('jasa','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['warna']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-5"> 
                                      <?php
                                          echo form_input($form['warna']);
                                          echo form_error('warna','<div class="note">','</div>');
                                      ?> 
                              </div> 

                              <div class="col-xs-3"> 
                                      <?php
                                          echo form_input($form['tahun']);
                                          echo form_error('tahun','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tot_by']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['tot_by']);
                                          echo form_error('tot_by','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>

<!-- modal dialog -->
<div id="modal_kbm" class="modal fade">
    <div class="modal-dialog"  style="width: 50%;">
        <div class="modal-content">
            <form id="modal_kbm" name="modal_kbm" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Data Kas / Bank</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-12 col-md-12">
                            <div class="row">  

                                <div class="col-xs-8"> 
                                        <?php
                                            echo form_input($form['kbm_nourut']);
                                            echo form_error('kbm_nourut','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-xs-12">
                      <div class="row">

                          <div class="col-xs-12 col-md-12">
                            <div class="row"> 

                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php echo form_label($form['kbm_jns']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-8"> 
                                        <?php
                                          echo form_dropdown($form['kbm_jns']['name'],
                                                            $form['kbm_jns']['data'],
                                                            $form['kbm_jns']['value'],
                                                            $form['kbm_jns']['attr']);
                                          echo form_error('kbm_jns','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-12 col-md-12">
                            <div class="row"> 

                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php echo form_label($form['kbm_drkpd']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-8"> 
                                        <?php
                                            echo form_input($form['kbm_drkpd']);
                                            echo form_error('kbm_drkpd','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-12 col-md-12">
                            <div class="row"> 

                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php echo form_label($form['kbm_nofaktur']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-8"> 
                                        <?php
                                            echo form_input($form['kbm_nofaktur']);
                                            echo form_error('kbm_nofaktur','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-12 col-md-12">
                            <div class="row"> 

                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php echo form_label($form['kbm_ket']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-8"> 
                                        <?php
                                            echo form_input($form['kbm_ket']);
                                            echo form_error('kbm_ket','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                      </div>
                    </div>

                    <div class="col-xs-12">
                      <div class="row"> 

                          <div class="col-xs-12 col-md-12">
                            <div class="row"> 

                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php echo form_label($form['kbm_nilai']['placeholder']); ?>
                                    </div>
                                </div>

                                <div class="col-xs-8"> 
                                        <?php
                                            echo form_input($form['kbm_nilai']);
                                            echo form_error('kbm_nilai','<div class="note">','</div>');
                                        ?> 
                                </div> 

                            </div>
                          </div> 

                      </div>
                    </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-kbmcancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-kbmsimpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>
            <?php echo form_close(); ?>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        autoNum();
        hide();
        GetNoID();
        init_pkons_cari();
        init_pleas_cari();
        init_blkkons_cari();

        $('.btn-unset').hide();
        $('.btn-set').show();
        // $("#notbj").val('ABJ-____-____');   
        $(".btn-ctk").attr('disabled',true);   
        $("#refkb").val('');  
        $("#jnstrans").val('');   

        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                jnstrx : "required"
            },
            messages : {
                jnstrx : ""
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalf').validate({
            errorClass: 'errors',
            rules : {
                nosin  : "required",
                bbn  : "required",
                jasa  : "required"
            },
            messages : {
                nosin  : "Masukkan Nomer Mesin",
                bbn  : "Masukkan Nilai BBN",
                jasa  : "Masukkan Nilai Jasa"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });


        $('#refkb').change(function () { 
            refkb();
            set_nokb();
            set_nocetak();
        }); 

        $('#byrkons_noso').keyup(function () { 
            var jml = $('#byrkons_noso').val();
            var noso = jml.replace("-",""); 
            var noso = noso.replace("_","");
            var n = noso.length;
            
                // console.log(n); 
            if(n===9){
                // console.log(n);    
                set_pkons_noso();
            } else {
                c_pkons('noso');
            }
        }); 

        $('#byrkons_nodo').keyup(function () { 
            var jml = $('#byrkons_nodo').val();
            var nodo = jml.replace("-",""); 
            var nodo = nodo.replace("_","");
            var n = nodo.length;
            
                // console.log(n); 
            if(n===9){
                // console.log(n);    
                set_pkons_nodo();
            } else {
                c_pkons('nodo');
            }
        }); 

        $('#pleas_noso').keyup(function () { 
            var jml = $('#pleas_noso').val();
            var noso = jml.replace("-",""); 
            var noso = noso.replace("_","");
            var n = noso.length;
            
                // console.log(n); 
            if(n===9){
                // console.log(n);    
                set_pleas_noso();
            } else {
                c_pleas('noso');
            }
        }); 

        $('#pleas_nodo').keyup(function () { 
            var jml = $('#pleas_nodo').val();
            var nodo = jml.replace("-",""); 
            var nodo = nodo.replace("_","");
            var n = nodo.length;
            
                // console.log(n); 
            if(n===9){
                // console.log(n);    
                set_pleas_nodo();
            } else {
                c_pleas('nodo');
            }
        }); 

        $('#blkkons_noso').keyup(function () { 
            var jml = $('#blkkons_noso').val();
            var noso = jml.replace("-",""); 
            var noso = noso.replace("_","");
            var n = noso.length;
            
                // console.log(n); 
            if(n===9){
                // console.log(n);    
                set_blkkons_noso();
            } else {
                c_blkkons('noso');
            }
        }); 

        $('#blkkons_nodo').keyup(function () { 
            var jml = $('#blkkons_nodo').val();
            var nodo = jml.replace("-",""); 
            var nodo = nodo.replace("_","");
            var n = nodo.length;
            
                // console.log(n); 
            if(n===9){
                // console.log(n);    
                set_blkkons_nodo();
            } else {
                c_blkkons('nodo');
            }
        }); 

        $('#byrkons_cari').change(function(){
            set_pkons(); 
        });

        $('.btn-ctk').click(function(){
            var nokb = $('#nokb').val();
            var tglkb = $('#periode').val();
            // window.open('kbins/pdf/'+nokb, '_blank');
            window.open('kbins/pdf/'+nokb+'/'+tglkb, '_blank');
        });

        $('#pleas_cari').change(function(){
            set_pleas(); 
        });

        $('#blkkons_cari').change(function(){
            set_blkkons(); 
        });

        $('.btn-batal').click(function () { 
            batal();
        });   

        $('.btn-submit').click(function () { 
            if ($('#jnstrans').val()==='PEMBAYARAN KONSUMEN (D)') {
                pkons_submit();
            } else if ($('#jnstrans').val()==='PEMBAYARAN LEASING (D)') {
                pleas_submit();
            } else if ($('#jnstrans').val()==='REFUND LEASING (D)') {
                rleas_submit();
            } else if ($('#jnstrans').val()==='PENGEMBALIAN KONSUMEN (K)') {
                blkkons_submit();
            } else if ($('#jnstrans').val()==='REALISASI KASBON (K)') {
                rkasbon_submit();
            } else if ($('#jnstrans').val()==='K/B KELUAR - UMUM (K)') {
                kbsubmit();
            } else if ($('#jnstrans').val()==='K/B MASUK - UMUM (D)') {
                kbsubmit();
            } else if ($('#jnstrans').val()==='PENCAIRAN SUBSIDI (D)') {
                psub_submit();
            } 
        });  

        $('.btn-del').click(function () { 
            var nodo = table.row('.selected').data()['nodo']; 
            delDetail(nodo);
        });  

        $('.btn-set').click(function(){
            settrx(); 
            $('#jnstrans').prop('disabled',true);
            $('.btn-set').hide();
            $('.btn-unset').show();
        });

        $('.btn-unset').click(function(){ 
            hide();
            reset();
            $('#jnstrans').prop('disabled',false);
            $('.btn-set').show();
            $('.btn-unset').hide();
        });


        $('.btn-add').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            $('#modal_trans').modal('toggle');
            r_modal();
            autoNum_modal();
        });


        $('.btn-addkbm').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            r_modalkbm();
            getKdjenis();
            $('#kbm_nilai').autoNumeric('init');
            $('#modal_kbm').modal('toggle');
            // autoNum_modal();
        });


        $('.btn-delkbm').click(function(){ 
            kbm_del();
        });


        $('.btn-addkbk').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            r_modalkbm();
            getKdjenis();
            $('#kbm_nilai').autoNumeric('init');
            $('#modal_kbm').modal('toggle');
            // autoNum_modal();
        }); 


        $('.btn-addtkasbon').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            r_modalkbm();
            getKdjenis();
            $('#kbm_nilai').autoNumeric('init');
            $('#modal_kbm').modal('toggle');
            // autoNum_modal();
        });


        $('.btn-addrkasbon').click(function(){  
            rkasbon_ins(); 
        });



        $('.btn-delkbk').click(function(){ 
            kbk_del();
        }); 

        $('.btn-deltkasbon').click(function(){ 
            tkasbon_del();
        });

        $('.btn-delrkasbon').click(function(){ 
            kasbon_del();
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            // $("#nourut").val('');
            r_modal();
        });

        $('.btn-simpan').click(function () { 
            // console.log($('#modelf').valid());
            if ($("#modalf").valid()) { 
                tambah();
                // alert('1');
            }
        });

        $('#jnstrans').change(function () { 
                proses_dk();
        });

        $('#nosin').keyup(function () {
            var jml = $('#nosin').val();
            var nosin = jml.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("_","");
            var n = nosin.length;
            
            if(n===12){
                // console.log(n);    
                set_nosin();   
            }
            // set_nosin();
        }); 

        $('#rkasbon_nokasbon').keyup(function () {
            var jml = $('#rkasbon_nokasbon').val();
            var jml = jml.replaceAll("_","");
            var n = jml.length;
                // alert(n); 
            
            if(n===13){
                // console.log(n);    
                set_nokasbon();  
                // alert(n); 
            }
            // set_nosin();
        }); 

        $('.btn-kbmsimpan').click(function(){
            if($('#jnstrans').val()==='K/B MASUK - UMUM (D)'){
              kbm_ins();
            } else if($('#jnstrans').val()==='K/B KELUAR - UMUM (K)') {
              kbk_ins();
            } else {
              tkasbon_ins();
            }
        }); 

        $('.btn-kbmcancel').click(function(){
            $("#modal_trans").modal("hide");
            r_modalkbm();
        });
    });

    // no id
    function GetNoID(){ 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/getNoID");?>",
            data: {},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#byrkons_noso').mask(value.kode+"99-999999"); 
                  $('#byrkons_nodo').mask(value.kode+"99-999999"); 
                  $('#pleas_noso').mask(value.kode+"99-999999"); 
                  $('#pleas_nodo').mask(value.kode+"99-999999"); 
                  $('#blkkons_noso').mask(value.kode+"99-999999"); 
                  $('#blkkons_nodo').mask(value.kode+"99-999999"); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    // no id
    function proses_dk(){ 
        //alert(kddiv);
        var jnstrans = $('#jnstrans').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/proses_dk");?>",
            data: { "jnstrans":jnstrans},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#dk').val(value.kd); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function day(tgl){
      var n = tgl.getDay();
      // var n = new Date(tgl);
      // alert(n);
      if(n>=6){
        var nd = 'SABTU';
      } else if(n>=5){
        var nd = 'JUMAT';
      } else if(n>=4){
        var nd = 'KAMIS';
      } else if(n>=3){
        var nd = 'RABU';
      } else if(n>=2){
        var nd = 'SELASA';
      } else if(n>=1){
        var nd = 'SENIN';
      } else if(n>=0){
        var nd = 'MINGGU';
      }
      $('#day').val(nd);
    }

    function hide(){
          $('.pkons').hide();
          $('.pleas').hide();
          $('.rleas').hide();
          $('.psub').hide();
          $('.kbm_umum').hide();
          $('.balik_kons').hide();
          $('.kbk_umum').hide();
          $('.rkasbon').hide();
    }

    function settrx(){
      // alert
      var nokb = $('#nokb').val();
      if ($('#jnstrans').val()==='PEMBAYARAN KONSUMEN (D)') {
        $('.pkons').show();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide(); 
      } else if ($('#jnstrans').val()==='PEMBAYARAN LEASING (D)') {
        $('.pkons').hide();
        $('.pleas').show();
        $('.rleas').hide();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide(); 
      } else if ($('#jnstrans').val()==='REFUND LEASING (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').show();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.kbm_umum').hide(); 
      } else if ($('#jnstrans').val()==='PENGEMBALIAN KONSUMEN (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.balik_kons').show();
        $('.psub').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide(); 
      } else if ($('#jnstrans').val()==='REALISASI KASBON (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.psub').hide();
        $('.rkasbon').show();
        $('.kbm_umum').hide(); 
        $('#rkasbon_nokasbon').mask('BON-9999-9999'); 
        tbl_kbbon();
        tb_kasbon();
      } else if ($('#jnstrans').val()==='K/B KELUAR - UMUM (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.kbk_umum').show();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.psub').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide(); 
        tbl_kbk();
      } else if ($('#jnstrans').val()==='K/B MASUK - UMUM (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.kbk_umum').hide();
        $('.balik_kons').hide();
        $('.psub').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').show(); 
        tbl_kbm();
      } else if ($('#jnstrans').val()==='PENCAIRAN SUBSIDI (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.psub').show();
        $('.kbm_umum').hide(); 
      } 
    }

    function tbl_kbm(){

        //
        var column = []; 



        

        column.push({
          "aTargets":  0 ,
          "searchable": false,
          "orderable": false,
          "checkboxes": {
            'selectRow': true
          }

        });

        column.push({
            "aTargets": [ 6 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 


        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });  

        tb_kbm = $('.tbl_kbm').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nourut"},
                { "data": "no"},
                { "data": "nmrefkb" },
                { "data": "darike" },
                { "data": "nofaktur"},
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": $('#nokb').val()},
                            { "name": "jnstrans", "value": $('#jnstrans').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('kbins/kb_json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        tb_kbm.on( 'draw.dt', function () {
        var PageInfo = $('.tbl_kbm').DataTable().page.info();
                tb_kbm.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.tbl_kbm tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                tb_kbm.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });
    }

    function tbl_kbk(){

        //
        var column = []; 

        column.push({
          "aTargets":  0 ,
          "searchable": false,
          "orderable": false,
          "checkboxes": {
            'selectRow': true
          }

        });

        column.push({
            "aTargets": [ 6 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 


        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });  

        tb_kbk = $('.tbl_kbk').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nourut"},
                { "data": "no"},
                { "data": "nmrefkb" },
                { "data": "darike" },
                { "data": "nofaktur"},
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": $('#nokb').val()},
                            { "name": "jnstrans", "value": $('#jnstrans').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('kbins/kb_json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        tb_kbk.on( 'draw.dt', function () {
        var PageInfo = $('.tbl_kbk').DataTable().page.info();
                tb_kbk.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.tbl_kbk tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                tb_kbk.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });
    }

    function tbl_kbbon(){

        //
        var column = []; 

        column.push({
          "aTargets":  0 ,
          "searchable": false,
          "orderable": false,
          "checkboxes": {
            'selectRow': true
          }

        });

        column.push({
            "aTargets": [ 6 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 


        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });  

        tbl_tkasbon = $('.tbl_tkasbon').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nourut"},
                { "data": "no"},
                { "data": "nmrefkb" },
                { "data": "darike" },
                { "data": "nofaktur"},
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": $('#nokb').val()},
                            { "name": "jnstrans", "value": $('#jnstrans').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('kbins/kb_json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        tbl_tkasbon.on( 'draw.dt', function () {
        var PageInfo = $('.tbl_tkasbon').DataTable().page.info();
                tbl_tkasbon.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.tbl_tkasbon tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                tbl_tkasbon.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });
    }

    function tb_kasbon(){

        //
        var column = []; 

        column.push({
          "aTargets":  0 ,
          "searchable": false,
          "orderable": false,
          "checkboxes": {
            'selectRow': true
          }

        });

        column.push({
            "aTargets": [ 4 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 


        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });  

        tbl_kasbon = $('.tbl_kasbon').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nokasbon"},
                { "data": "no"},
                { "data": "nokasbon" },
                { "data": "tglkasbon" },
                { "data": "nilai"},
                { "data": "nama" },
                { "data": "ket"},
                { "data": "kdkb"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": $('#nokb').val()},
                            { "name": "jnstrans", "value": $('#jnstrans').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('kbins/kasbon_json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        tbl_kasbon.on( 'draw.dt', function () {
        var PageInfo = $('.tbl_kasbon').DataTable().page.info();
                tbl_kasbon.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.tbl_kasbon tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                tbl_kasbon.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });
    }

    function pkons_nocetak(){
      var check = $('#pkons_lanjut').is(':checked');
      if(check){ 
        $("#byrkons_nocetak").prop("disabled", false);
        $("#byrkons_jmltrans").prop("disabled", false); 
      }else{ 
        $("#byrkons_nocetak").prop("disabled", true);
        $("#byrkons_jmltrans").prop("disabled", true); 

      }
    }

    function pkons_nocetak(){
      var check = $('#pleas_lanjut').is(':checked');
      if(check){ 
        $("#pleas_nocetak").prop("disabled", false);
        $("#pleas_jmltrans").prop("disabled", false); 
      }else{ 
        $("#pleas_nocetak").prop("disabled", true);
        $("#pleas_jmltrans").prop("disabled", true); 

      }
    }

    function r_modalkbm(){
          $('#kbm_jns').val('');  
          $("#kbm_jns").trigger("chosen:updated");
          $('#kbm_jns').select2({
              dropdownAutoWidth : true,
              width: '100%'
          });
          $('#kbm_drkpd').val('');  
          $('#kbm_nofaktur').val('');  
          $('#kbm_ket').val('');  
          $('#kbm_nilai').val('');  

    }

    function r_modalkbk(){
          $('#kbk_jns').val('');  
          $("#kbk_jns").trigger("chosen:updated");
          $('#kbk_jns').select2({
              dropdownAutoWidth : true,
              width: '100%'
          });
          $('#kbk_drkpd').val('');  
          $('#kbk_nofaktur').val('');  
          $('#kbk_ket').val('');  
          $('#kbk_nilai').val('');  

    }

    function reset(){    
      // $('#periode').val($.datepicker.formatDate('dd-mm-yy', new Date()));  

      //detail pembayaran konsumen
          $('#byrkons_cari').val('');  
          $("#byrkons_cari").trigger("chosen:updated"); 
          $('#byrkons_noso').val('');  
          $('#byrkons_nodo').val('');  
          $('#byrkons_tglso').val('');  
          $('#byrkons_tgldo').val('');  
          $('#byrkons_nmsales').val('');  
          $('#byrkons_nmspv').val('');  
          $('#byrkons_nama').val('');  
          $('#byrkons_alamat').val('');  
          $('#byrkons_kdleas').val('');  
          $('#byrkons_kdprogleas').val('');  
          $('#byrkons_nosin').val('');  
          $('#byrkons_nora').val('');  
          $('#byrkons_kdtipe').val('');  
          $('#byrkons_nmtipe').val('');  
          $('#byrkons_warna').val('');  
          $('#byrkons_tahun').val('');  
          $('#byrkons_status').val('');  

          //nominal pembayaran konsumen
          $('#byrkons_hjnet').val('');  
          $('#byrkons_um').val('');  
          $('#byrkons_pelunasan').val('');  
          $('#byrkons_byr1').val('');  
          $('#byrkons_byr2').val('');  
          $('#byrkons_nbyr').val('');  
          $('#byrkons_saldoum').val('');  
          $('#byrkons_saldo_pelunasan').val('');  
          $('#byrkons_nocetak').val('');  
          $('#byrkons_jmltrans').val('');   

      //detail pembayaran leasing
          $('#pleas_cari').val('');  
          $("#pleas_cari").trigger("chosen:updated"); 
          $('#pleas_noso').val('');   
          $('#pleas_nodo').val('');   
          $('#pleas_tglso').val('');   
          $('#pleas_tgldo').val('');   
          $('#pleas_nmsales').val('');   
          $('#pleas_nmspv').val('');   
          $('#pleas_nama').val('');   
          $('#pleas_alamat').val('');   
          $('#pleas_kdleas').val('');   
          $('#pleas_kdprogleas').val('');   
          $('#pleas_nosin').val('');   
          $('#pleas_nora').val('');   
          $('#pleas_kdtipe').val('');   
          $('#pleas_nmtipe').val('');   
          $('#pleas_warna').val('');   
          $('#pleas_tahun').val('');   
          $('#pleas_status').val('');    

          //nominal pembayaran leasing
          $('#pleas_pelunasan').val('');    
          $('#pleas_jp').val('');    
          $('#pleas_byr1').val('');    
          $('#pleas_byr2').val('');    
          $('#pleas_nbyr1').val('');    
          $('#pleas_nbyr2').val('');    
          $('#pleas_nocetak').val('');    
          $('#pleas_jmltrans').val('');  

      //detail refund leasing
          $('#rleas_kdleas').val('');   
          $('#rleas_jenis').val('');   
          $('#rleas_nilai').val('');   
          $('#rleas_ket').val('');    

      //detail pencairan subsidi
          
          $('#psub_kdleas').val('');   
          $('#psub_selisih').val('');   
          $('#psub_ket').val('');   
          $('#psub_totsub').val('');   
          $('#psub_selsbg').val('');   


          
          //more detail pencairan subsidi
          $('#psub_nokasbon').val('');    
          $('#psub_tglkasbon').val('');    
          $('#psub_ketkasbon').val('');    
          $('#psub_catkasbon').val('');    
          $('#psub_nsubkasbon').val('');    
          $('#psub_ncairkasbon').val('');     

      //detail pengembalian konsumen
          $('#blkkons_cari').val('');  
          $("#blkkons_cari").trigger("chosen:updated"); 
          $('#blkkons_noso').val('');     
          $('#blkkons_nodo').val('');     
          $('#blkkons_tglso').val('');     
          $('#blkkons_tgldo').val('');     
          $('#blkkons_nmsales').val('');     
          $('#blkkons_nmspv').val('');     
          $('#blkkons_nama').val('');     
          $('#blkkons_alamat').val('');     
          $('#blkkons_kdleas').val('');     
          $('#blkkons_kdprogleas').val('');     
          $('#blkkons_nosin').val('');     
          $('#blkkons_nora').val('');     
          $('#blkkons_kdtipe').val('');     
          $('#blkkons_nmtipe').val('');     
          $('#blkkons_warna').val('');     
          $('#blkkons_tahun').val('');     
          $('#blkkons_status').val('');    

          //nominal pembayaran konsumen
          $('#blkkons_hjnet').val('');  
          $('#blkkons_arum').val('');  
          $('#blkkons_byr1').val('');  
          $('#blkkons_pengembalian').val('');  
          $('#blkkons_lbhbyr').val('');   

      //detail realisasi kasbon
          $('#rkasbon_nokasbon').val('');  
          $('#rkasbon_tglkasbon').val('');  
          $('#rkasbon_kb').val('');  
          $('#rkasbon_nama').val('');  
          $('#rkasbon_ket').val('');  
          $('#rkasbon_nkasbon').val('');   
    }

    function c_pkons(ket){ 
      //detail pembayaran konsumen
          if(ket==='noso'){ 
            $('#byrkons_nodo').val('');  
          } else {
            $('#byrkons_noso').val('');
          }  
          $('#byrkons_tglso').val('');  
          $('#byrkons_tgldo').val('');  
          $('#byrkons_nmsales').val('');  
          $('#byrkons_nmspv').val('');  
          $('#byrkons_nama').val('');  
          $('#byrkons_alamat').val('');  
          $('#byrkons_kdleas').val('');  
          $('#byrkons_kdprogleas').val('');  
          $('#byrkons_nosin').val('');  
          $('#byrkons_nora').val('');  
          $('#byrkons_kdtipe').val('');  
          $('#byrkons_nmtipe').val('');  
          $('#byrkons_warna').val('');  
          $('#byrkons_tahun').val('');  
          $('#byrkons_status').val('');  

          //nominal pembayaran konsumen
          $('#byrkons_hjnet').autoNumeric('set',0);  
          $('#byrkons_um').autoNumeric('set',0);  
          $('#byrkons_pelunasan').autoNumeric('set',0);  
          $('#byrkons_byr1').autoNumeric('set',0);  
          $('#byrkons_byr2').autoNumeric('set',0);  
          $('#byrkons_nbyr').autoNumeric('set',0);  
          $('#byrkons_saldoum').autoNumeric('set',0);  
          $('#byrkons_saldo_pelunasan').autoNumeric('set',0);  
          $('#byrkons_nocetak').val('');  
          $('#byrkons_jmltrans').val('');  
    }

    function c_pleas(ket){ 
      //detail pembayaran leasing
          if(ket==='noso'){ 
            $('#pleas_nodo').val('');  
          } else {
            $('#pleas_noso').val('');
          }  
          $('#pleas_tglso').val('');   
          $('#pleas_tgldo').val('');   
          $('#pleas_nmsales').val('');   
          $('#pleas_nmspv').val('');   
          $('#pleas_nama').val('');   
          $('#pleas_alamat').val('');   
          $('#pleas_kdleas').val('');   
          $('#pleas_kdprogleas').val('');   
          $('#pleas_nosin').val('');   
          $('#pleas_nora').val('');   
          $('#pleas_kdtipe').val('');   
          $('#pleas_nmtipe').val('');   
          $('#pleas_warna').val('');   
          $('#pleas_tahun').val('');   
          $('#pleas_status').val('');    

          //nominal pembayaran leasing
          $('#pleas_pelunasan').autoNumeric('set',0);     
          $('#pleas_jp').autoNumeric('set',0);     
          $('#pleas_byr1').autoNumeric('set',0);  
          $('#pleas_byr2').autoNumeric('set',0);  
          $('#pleas_nbyr1').autoNumeric('set',0);  
          $('#pleas_nbyr2').autoNumeric('set',0);  
          $('#pleas_nocetak').val('');    
          $('#pleas_jmltrans').val('');  
    }

    function c_rleas(){ 
      //detail refund leasing
          $('#rleas_kdleas').val('');   
          $('#rleas_jenis').val('');   
          $('#rleas_nilai').val('');   
          $('#rleas_ket').val('');    
    }

    function c_psub(){
      //detail pencairan subsidi
          $('#psub_kdleas').val('');   
          $('#psub_selisih').autoNumeric('set',0);   
          $('#psub_ket').val('');   
          $('#psub_totsub').autoNumeric('set',0);   
          $('#psub_selsbg').val('');   
          
          //more detail pencairan subsidi
          $('#psub_nokasbon').val('');    
          $('#psub_tglkasbon').val('');    
          $('#psub_ketkasbon').val('');    
          $('#psub_catkasbon').val('');    
          $('#psub_nsubkasbon').autoNumeric('set',0);    
          $('#psub_ncairkasbon').autoNumeric('set',0);   
    }

    function c_blkkons(ket){
      //detail pengembalian konsumen
          if(ket==='noso'){ 
            $('#blkkons_nodo').val('');  
          } else {
            $('#blkkons_noso').val('');
          }      
          $('#blkkons_tglso').val('');     
          $('#blkkons_tgldo').val('');     
          $('#blkkons_nmsales').val('');     
          $('#blkkons_nmspv').val('');     
          $('#blkkons_nama').val('');     
          $('#blkkons_alamat').val('');     
          $('#blkkons_kdleas').val('');     
          $('#blkkons_kdprogleas').val('');     
          $('#blkkons_nosin').val('');     
          $('#blkkons_nora').val('');     
          $('#blkkons_kdtipe').val('');     
          $('#blkkons_nmtipe').val('');     
          $('#blkkons_warna').val('');     
          $('#blkkons_tahun').val('');     
          $('#blkkons_status').val('');    

          //nominal pembayaran konsumen
          $('#blkkons_hjnet').autoNumeric('set',0);
          $('#blkkons_pl').autoNumeric('set',0);
          $('#blkkons_byr1').autoNumeric('set',0);  
          $('#blkkons_pengembalian').autoNumeric('set',0);
          $('#blkkons_lbhbyr').autoNumeric('set',0);    
    }

    function c_rkasbon(){
      //detail realisasi kasbon
          $('#rkasbon_nokasbon').val('');  
          $('#rkasbon_tglkasbon').val('');  
          $('#rkasbon_kb').val('');  
          $('#rkasbon_nama').val('');  
          $('#rkasbon_ket').val('');  
          $('#rkasbon_nkasbon').autoNumeric('set',0); 
    }

    function autoNum(){   

      //detail pembayaran konsumen
          //nominal pembayaran konsumen
          $('#byrkons_hjnet').autoNumeric('init',{ currencySymbol : 'Rp. '}); 
          $('#byrkons_um').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_pelunasan').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_byr1').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_byr2').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_nbyr').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_saldoum').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#byrkons_saldo_pelunasan').autoNumeric('init',{ currencySymbol : 'Rp. '});   

      //detail pembayaran leasing  
          //nominal pembayaran leasing
          $('#pleas_pelunasan').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_jp').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_byr1').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_byr2').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_nbyr1').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#pleas_nbyr2').autoNumeric('init',{ currencySymbol : 'Rp. '});     

      //detail refund leasing 
          
          $('#rleas_nilai').autoNumeric('init',{ currencySymbol : 'Rp. '});      

      //detail pencairan subsidi
            
          $('#psub_selisih').autoNumeric('init',{ currencySymbol : 'Rp. '}); 
          $('#psub_totsub').autoNumeric('init',{ currencySymbol : 'Rp. '});    
          
          //more detail pencairan subsidi  
          $('#psub_nsubkasbon').autoNumeric('init',{ currencySymbol : 'Rp. '});     
          $('#psub_ncairkasbon').autoNumeric('init',{ currencySymbol : 'Rp. '});      

      //detail pengembalian konsumen 
          //nominal pembayaran konsumen
          $('#blkkons_hjnet').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_arum').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_byr1').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_pengembalian').autoNumeric('init',{ currencySymbol : 'Rp. '});   
          $('#blkkons_lbhbyr').autoNumeric('init',{ currencySymbol : 'Rp. '});    

      //detail realisasi kasbon 
          
          $('#rkasbon_nkasbon').autoNumeric('init',{ currencySymbol : 'Rp.'});    
    }

    function set_nosin(){   
        var nosin = $("#nosin").val();  
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/set_nosin');?>",
            data: { "nosin":nosin},
            success: function(resp){
              // $("#modal_trans").modal("hide"); 
              if(resp==='"empty"'){
                  swal({
                    title: 'No. Mesin tidak ditemukan atau sudah masuk Detail Tagihan BBN',
                    text: '',
                    type: 'error'
                  },function (){
                    c_modal();
                  });
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                  // $('#ket').show();  
                      // alert(data.nobbn_trm);
                      if(data.nobbn_trm===null){
                        $('#nora').val(data.nora);
                        $('#nodo').val(data.nodo);
                        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo))); 
                        $('#nama').val(data.nama);
                        $('#alamat').val(data.alamat);
                        $('#kota').val(data.kota);
                        $('#kdtipe').val(data.kdtipe);
                        $('#warna').val(data.nmwarna);
                        $('#tahun').val(data.tahun);
                      } else {
                        swal({
                          title: 'No. Mesin sudah pernah input Tagihan BBN',
                          text: '',
                          type: 'error'
                        },function (){
                          c_modal();
                        });
                      }
                  });
              } 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function set_nokasbon(){   
        var nokasbon = $("#rkasbon_nokasbon").val();  
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/set_nokasbon');?>",
            data: { "nokasbon":nokasbon},
            success: function(resp){
              // $("#modal_trans").modal("hide"); 
              if(resp==='"empty"'){
                  swal({
                    title: 'No. kasbon tidak ditemukan atau sudah digunakan ',
                    text: '',
                    type: 'error'
                  });
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                  // $('#ket').show();  
                      // alert(data.nobbn_trm); 
                        $('#rkasbon_nama').val(data.nama);
                        $('#rkasbon_ket').val(data.ket);
                        $("#rkasbon_tglkasbon").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkasbon))); 
                        $('#rkasbon_kb').val(data.kdkb);
                        $('#rkasbon_nkasbon').autoNumeric('set',data.nilai);  
                  });
              } 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function refkb(){   
        var refkb = $("#refkb").val();   
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/refkb');?>",
            data: { "refkb":refkb},
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#periode").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb)));  
                    var tgl = new Date(data.tglkb);
                    // alert(tgl);
                    day(tgl);  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function set_nocetak(){   
        var refkb = $("#refkb").val();   
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/set_nocetak');?>",
            data: { "refkb":refkb},
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){   
                    var jnstrans = $('#jnstrans').val();
                    if(jnstrans==='PEMBAYARAN KONSUMEN (D)'){
                        $('#pkons_lanjut').prop("checked",true);
                        $('#pkons_baru').prop("checked",false);
                        $('#pkons_lanjut').prop("disabled",true);
                        $('#pkons_baru').prop("disabled",true);
                        $("#byrkons_nocetak").val(data.nocetak);
                        $("#byrkons_noctk").val(data.max);
                        set_jmlctk(jnstrans,data.max);
                    } else if(jnstrans==='PEMBAYARAN LEASING (D)') {
                        $('#pleas_lanjut').prop("checked",true);
                        $('#pleas_baru').prop("checked",false);
                        $('#pleas_lanjut').prop("disabled",true);
                        $('#pleas_baru').prop("disabled",true);
                        $("#pleas_nocetak").val(data.nocetak);
                        $("#pleas_noctk").val(data.max);
                        set_jmlctk(jnstrans,data.max);
                    }
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function set_jmlctk(jnstrans,max){   
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/set_jmlctk');?>",
            data: { "max":max},
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    if(jnstrans==='PEMBAYARAN KONSUMEN (D)'){ 
                      if(data.count<9){
                        $('#pkons_lanjut').prop("checked",true);
                        $('#pkons_baru').prop("checked",false);
                        $('#pkons_lanjut').prop("disabled",true);
                        $('#pkons_baru').prop("disabled",true); 
                        $("#byrkons_jmltrans").val(data.count);
                      } else {
                        $('#pkons_lanjut').prop("checked",false);
                        $('#pkons_baru').prop("checked",true);
                        $('#pkons_lanjut').prop("disabled",true);
                        $('#pkons_baru').prop("disabled",true); 
                        $("#byrkons_nocetak").val('');
                        $("#byrkons_jmltrans").val('');
                      }
                    } else if(jnstrans==='PEMBAYARAN LEASING (D)') {
                      if(data.count<9){
                        $('#pleas_lanjut').prop("checked",true);
                        $('#pleas_baru').prop("checked",false);
                        $('#pleas_lanjut').prop("disabled",true);
                        $('#pleas_baru').prop("disabled",true); 
                        $("#pleas_jmltrans").val(data.count);
                      } else {
                        $('#pleas_lanjut').prop("checked",false);
                        $('#pleas_baru').prop("checked",true);
                        $('#pleas_lanjut').prop("disabled",true);
                        $('#pleas_baru').prop("disabled",true); 
                        $("#pleas_nocetak").val('');
                        $("#pleas_jmltrans").val('');
                      }  
                    }
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function set_nokb(){   
        var kdkb = $("#refkb").val();   
        var tglkb = $("#periode").val();   
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/set_nokb');?>",
            data: { "kdkb":kdkb,"tglkb":tglkb},
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){   
                    $("#nokb").val(data.kb_setnokb); 
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function set_pkons(){

      var noso = $("#byrkons_cari").val();
      var noso = noso.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pkonsnoso");?>",
            data: {"noso":noso },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'noso';
                c_pkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#byrkons_noso").val(data.noso); 
                      $("#byrkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#byrkons_nodo").val(data.nodo); 
                      $("#byrkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#byrkons_nmsales").val(data.nmsales); 
                      $("#byrkons_nmspv").val(data.nmsales_header); 
                      $("#byrkons_nama").val(data.nama); 
                      $("#byrkons_alamat").val(data.alamat); 
                      $("#byrkons_kdleas").val(data.jnsbayar); 
                      $("#byrkons_kdprogleas").val(data.nmprogleas); 
                      $("#byrkons_nosin").val(data.nosin); 
                      $("#byrkons_nora").val(data.nora); 
                      $("#byrkons_kdtipe").val(data.kdtipe); 
                      $("#byrkons_nmtipe").val(data.nmtipe); 
                      $("#byrkons_warna").val(data.warna); 
                      $("#byrkons_tahun").val(data.tahun); 
                      $("#byrkons_status").val(data.status_otr); 

                      $("#byrkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#byrkons_um").autoNumeric('set',data.ar_um); 
                      $("#byrkons_pelunasan").autoNumeric('set',data.ar_pl); 
                      $("#byrkons_byr1").autoNumeric('set',data.byr_um); 
                      $("#byrkons_byr2").autoNumeric('set',data.byr_pl); 
                      $("#byrkons_nbyr").autoNumeric('set',data.nilai_byr); 
                      $("#byrkons_saldoum").autoNumeric('set',data.saldo_um); 
                      $("#byrkons_saldo_pelunasan").autoNumeric('set',data.saldo_pl); 

                      set_nocetak();  
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pkons_noso(){

      var noso = $("#byrkons_noso").val();
      var noso = noso.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pkonsnoso");?>",
            data: {"noso":noso },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'noso';
                c_pkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#byrkons_noso").val(data.noso); 
                      $("#byrkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#byrkons_nodo").val(data.nodo); 
                      $("#byrkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#byrkons_nmsales").val(data.nmsales); 
                      $("#byrkons_nmspv").val(data.nmsales_header); 
                      $("#byrkons_nama").val(data.nama); 
                      $("#byrkons_alamat").val(data.alamat); 
                      $("#byrkons_kdleas").val(data.jnsbayar); 
                      $("#byrkons_kdprogleas").val(data.nmprogleas); 
                      $("#byrkons_nosin").val(data.nosin); 
                      $("#byrkons_nora").val(data.nora); 
                      $("#byrkons_kdtipe").val(data.kdtipe); 
                      $("#byrkons_nmtipe").val(data.nmtipe); 
                      $("#byrkons_warna").val(data.warna); 
                      $("#byrkons_tahun").val(data.tahun); 
                      $("#byrkons_status").val(data.status_otr); 

                      $("#byrkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#byrkons_um").autoNumeric('set',data.ar_um); 
                      $("#byrkons_pelunasan").autoNumeric('set',data.ar_pl); 
                      $("#byrkons_byr1").autoNumeric('set',data.byr_um); 
                      $("#byrkons_byr2").autoNumeric('set',data.byr_pl); 
                      $("#byrkons_nbyr").autoNumeric('set',data.nilai_byr); 
                      $("#byrkons_saldoum").autoNumeric('set',data.saldo_um); 
                      $("#byrkons_saldo_pelunasan").autoNumeric('set',data.saldo_pl); 

                      set_nocetak();  
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pkons_nodo(){

      var nodo = $("#byrkons_nodo").val();
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pkonsnodo");?>",
            data: {"nodo":nodo },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'nodo';
                c_pkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#byrkons_noso").val(data.noso); 
                      $("#byrkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#byrkons_nodo").val(data.nodo); 
                      $("#byrkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#byrkons_nmsales").val(data.nmsales); 
                      $("#byrkons_nmspv").val(data.nmsales_header); 
                      $("#byrkons_nama").val(data.nama); 
                      $("#byrkons_alamat").val(data.alamat); 
                      $("#byrkons_kdleas").val(data.jnsbayar); 
                      $("#byrkons_kdprogleas").val(data.nmprogleas); 
                      $("#byrkons_nosin").val(data.nosin); 
                      $("#byrkons_nora").val(data.nora); 
                      $("#byrkons_kdtipe").val(data.kdtipe); 
                      $("#byrkons_nmtipe").val(data.nmtipe); 
                      $("#byrkons_warna").val(data.warna); 
                      $("#byrkons_tahun").val(data.tahun); 
                      $("#byrkons_status").val(data.status_otr); 

                      $("#byrkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#byrkons_um").autoNumeric('set',data.ar_um); 
                      $("#byrkons_pelunasan").autoNumeric('set',data.ar_pl); 
                      $("#byrkons_byr1").autoNumeric('set',data.byr_um); 
                      $("#byrkons_byr2").autoNumeric('set',data.byr_pl); 
                      $("#byrkons_nbyr").autoNumeric('set',data.nilai_byr); 
                      $("#byrkons_saldoum").autoNumeric('set',data.saldo_um); 
                      $("#byrkons_saldo_pelunasan").autoNumeric('set',data.saldo_pl); 

                      set_nocetak(); 
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pleas(){

      var noso = $("#pleas_cari").val(); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pleasnoso");?>",
            data: {"noso":noso },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'noso';
                c_pkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#pleas_noso").val(data.noso); 
                      $("#pleas_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#pleas_nodo").val(data.nodo); 
                      $("#pleas_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#pleas_nmsales").val(data.nmsales); 
                      $("#pleas_nmspv").val(data.nmsales_header); 
                      $("#pleas_nama").val(data.nama); 
                      $("#pleas_alamat").val(data.alamat); 
                      $("#pleas_kdleas").val(data.jnsbayar); 
                      $("#pleas_kdprogleas").val(data.nmprogleas); 
                      $("#pleas_nosin").val(data.nosin); 
                      $("#pleas_nora").val(data.nora); 
                      $("#pleas_kdtipe").val(data.kdtipe); 
                      $("#pleas_nmtipe").val(data.nmtipe); 
                      $("#pleas_warna").val(data.warna); 
                      $("#pleas_tahun").val(data.tahun); 
                      $("#pleas_status").val(data.status_otr); 

                      $("#pleas_pelunasan").autoNumeric('set',data.pl); 
                      $("#pleas_jp").autoNumeric('set',data.jp); 
                      $("#pleas_byr1").autoNumeric('set',data.pl_bayar); 
                      $("#pleas_byr2").autoNumeric('set',data.jp_bayar); 
                      $("#pleas_nbyr1").autoNumeric('set',0); 
                      $("#pleas_nbyr2").autoNumeric('set',0); 

                      set_nocetak();  
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pleas_noso(){

      var noso = $("#pleas_noso").val();
      var noso = noso.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pleasnoso");?>",
            data: {"noso":noso },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'noso';
                c_pleas(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      // $("#pleas_noso").val(data.noso); 
                      $("#pleas_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#pleas_nodo").val(data.nodo); 
                      $("#pleas_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#pleas_nmsales").val(data.nmsales); 
                      $("#pleas_nmspv").val(data.nmsales_header); 
                      $("#pleas_nama").val(data.nama); 
                      $("#pleas_alamat").val(data.alamat); 
                      $("#pleas_kdleas").val(data.jnsbayar); 
                      $("#pleas_kdprogleas").val(data.nmprogleas); 
                      $("#pleas_nosin").val(data.nosin); 
                      $("#pleas_nora").val(data.nora); 
                      $("#pleas_kdtipe").val(data.kdtipe); 
                      $("#pleas_nmtipe").val(data.nmtipe); 
                      $("#pleas_warna").val(data.warna); 
                      $("#pleas_tahun").val(data.tahun); 
                      $("#pleas_status").val(data.status_otr); 

                      $("#pleas_pelunasan").autoNumeric('set',data.pl); 
                      $("#pleas_jp").autoNumeric('set',data.jp); 
                      $("#pleas_byr1").autoNumeric('set',data.pl_bayar); 
                      $("#pleas_byr2").autoNumeric('set',data.jp_bayar); 
                      $("#pleas_nbyr1").autoNumeric('set',0); 
                      $("#pleas_nbyr2").autoNumeric('set',0); 

                      set_nocetak();  
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pleas_nodo(){

      var nodo = $("#pleas_nodo").val();
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_pleasnodo");?>",
            data: {"nodo":nodo },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'nodo';
                c_pleas(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#pleas_noso").val(data.noso); 
                      $("#pleas_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      // $("#pleas_nodo").val(data.nodo); 
                      $("#pleas_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#pleas_nmsales").val(data.nmsales); 
                      $("#pleas_nmspv").val(data.nmsales_header); 
                      $("#pleas_nama").val(data.nama); 
                      $("#pleas_alamat").val(data.alamat); 
                      $("#pleas_kdleas").val(data.jnsbayar); 
                      $("#pleas_kdprogleas").val(data.nmprogleas); 
                      $("#pleas_nosin").val(data.nosin); 
                      $("#pleas_nora").val(data.nora); 
                      $("#pleas_kdtipe").val(data.kdtipe); 
                      $("#pleas_nmtipe").val(data.nmtipe); 
                      $("#pleas_warna").val(data.warna); 
                      $("#pleas_tahun").val(data.tahun); 
                      $("#pleas_status").val(data.status_otr); 

                      $("#pleas_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#pleas_um").autoNumeric('set',data.ar_um); 
                      $("#pleas_pelunasan").autoNumeric('set',data.ar_pl); 
                      $("#pleas_byr1").autoNumeric('set',data.byr_um); 
                      $("#pleas_byr2").autoNumeric('set',data.byr_pl); 
                      $("#pleas_nbyr").autoNumeric('set',data.nilai_byr); 
                      $("#pleas_saldoum").autoNumeric('set',data.saldo_um); 
                      $("#pleas_saldo_pelunasan").autoNumeric('set',data.saldo_pl); 

                      set_nocetak(); 
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_blkkons(){

      var noso = $("#blkkons_cari").val();
      var noso = noso.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_blkkonsnoso");?>",
            data: {"noso":noso },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'noso';
                c_blkkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#blkkons_noso").val(data.noso); 
                      $("#blkkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#blkkons_nodo").val(data.nodo); 
                      $("#blkkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#blkkons_nmsales").val(data.nmsales); 
                      $("#blkkons_nmspv").val(data.nmsales_header); 
                      $("#blkkons_nama").val(data.nama); 
                      $("#blkkons_alamat").val(data.alamat); 
                      $("#blkkons_kdleas").val(data.jnsbayarx); 
                      $("#blkkons_kdprogleas").val(data.nmprogleas); 
                      $("#blkkons_nosin").val(data.nosin); 
                      $("#blkkons_nora").val(data.nora); 
                      $("#blkkons_kdtipe").val(data.kdtipe); 
                      $("#blkkons_nmtipe").val(data.nmtipe); 
                      $("#blkkons_warna").val(data.warna); 
                      $("#blkkons_tahun").val(data.tahun); 
                      $("#blkkons_status").val(data.status_otr); 

                      $("#blkkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#blkkons_arum").autoNumeric('set',data.tot_ar); 
                      $("#blkkons_byr1").autoNumeric('set',data.byr_ar); 
                      var total = parseInt(data.byr_ar - data.tot_ar);
                      $("#blkkons_pengembalian").autoNumeric('set',Math.abs(total)); 
                      $("#blkkons_lbhbyr").autoNumeric('set',Math.abs(total)); 
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_blkkons_noso(){

      var noso = $("#blkkons_noso").val();
      var noso = noso.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_blkkonsnoso");?>",
            data: {"noso":noso },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'noso';
                c_blkkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#blkkons_noso").val(data.noso); 
                      $("#blkkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#blkkons_nodo").val(data.nodo); 
                      $("#blkkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#blkkons_nmsales").val(data.nmsales); 
                      $("#blkkons_nmspv").val(data.nmsales_header); 
                      $("#blkkons_nama").val(data.nama); 
                      $("#blkkons_alamat").val(data.alamat); 
                      $("#blkkons_kdleas").val(data.jnsbayarx); 
                      $("#blkkons_kdprogleas").val(data.nmprogleas); 
                      $("#blkkons_nosin").val(data.nosin); 
                      $("#blkkons_nora").val(data.nora); 
                      $("#blkkons_kdtipe").val(data.kdtipe); 
                      $("#blkkons_nmtipe").val(data.nmtipe); 
                      $("#blkkons_warna").val(data.warna); 
                      $("#blkkons_tahun").val(data.tahun); 
                      $("#blkkons_status").val(data.status_otr); 

                      $("#blkkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#blkkons_arum").autoNumeric('set',data.tot_ar); 
                      $("#blkkons_byr1").autoNumeric('set',data.byr_ar); 
                      var total = parseInt(data.byr_ar - data.tot_ar);
                      $("#blkkons_pengembalian").autoNumeric('set',Math.abs(total)); 
                      $("#blkkons_lbhbyr").autoNumeric('set',Math.abs(total)); 
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_blkkons_nodo(){

      var nodo = $("#blkkons_nodo").val();
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/set_blkkonsnodo");?>",
            data: {"nodo":nodo },
            success: function(resp){ 
              if(resp==='"empty"'){ 
                var ket = 'nodo';
                c_blkkons(ket);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                      $("#blkkons_noso").val(data.noso); 
                      $("#blkkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                      $("#blkkons_nodo").val(data.nodo); 
                      $("#blkkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                      $("#blkkons_nmsales").val(data.nmsales); 
                      $("#blkkons_nmspv").val(data.nmsales_header); 
                      $("#blkkons_nama").val(data.nama); 
                      $("#blkkons_alamat").val(data.alamat); 
                      $("#blkkons_kdleas").val(data.jnsbayarx); 
                      $("#blkkons_kdprogleas").val(data.nmprogleas); 
                      $("#blkkons_nosin").val(data.nosin); 
                      $("#blkkons_nora").val(data.nora); 
                      $("#blkkons_kdtipe").val(data.kdtipe); 
                      $("#blkkons_nmtipe").val(data.nmtipe); 
                      $("#blkkons_warna").val(data.warna); 
                      $("#blkkons_tahun").val(data.tahun); 
                      $("#blkkons_status").val(data.status_otr); 

                      $("#blkkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                      $("#blkkons_arum").autoNumeric('set',data.tot_ar); 
                      $("#blkkons_byr1").autoNumeric('set',data.byr_ar); 
                      var total = parseInt(data.byr_ar - data.tot_ar);
                      $("#blkkons_pengembalian").autoNumeric('set',Math.abs(total)); 
                      $("#blkkons_lbhbyr").autoNumeric('set',Math.abs(total));  
                  }); 
              } 
            }, 
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

  //   function sum(){
  //       var total = $('#total').autoNumeric('get'); 
  //       var disc  = $('#disc').autoNumeric('get'); 
  //       if(disc===''){
  //         var disc = 0;
  //       }
  //       var pph21 = $('#pph21').autoNumeric('get'); 
  //       if(pph21===''){
  //         var pph21 = 0;
  //       }

  //       var alltot = parseInt(total) - (parseInt(disc) + parseInt(pph21));
  //       if(!isNaN(alltot)){
  //           $('#alltot').autoNumeric('set',alltot); 
  //           var terbilang = penyebut(alltot);
  //           if(alltot==='0'){
  //             $('#banner').val(terbilang);  
  //           } else {
  //             $('#banner').val(terbilang+" RUPIAH");  
  //           }
            
  //       }
  //   }

  // function sum1(){
  //     var bbn = $('#bbn').autoNumeric('get');
  //     if(bbn===''){
  //       var bbn = 0;
  //     }
  //     var jasa = $('#jasa').autoNumeric('get');
  //     if(jasa===''){
  //       var jasa = 0;
  //     }
  //     var tot_by = parseInt(bbn) + parseInt(jasa);
  //     if(!isNaN(tot_by)){
  //       $('#tot_by').autoNumeric('set',tot_by);
  //     }
  // }

  // function tambah(){   ; 
  //     var nobbn_trm = $("#notbj").val(); 
  //     var tglbbn_trm = $("#tgltbj").val();  
  //     var nodo = $("#nodo").val();  
  //     var nilai = $("#bbn").autoNumeric('get');  
  //     var jasa = $("#jasa").autoNumeric('get');  
  //     // alert(count);

  //     $.ajax({
  //         type: "POST",
  //         url: "<?=site_url('kbins/tambah');?>",
  //         data: { "nobbn_trm":nobbn_trm, "tglbbn_trm":tglbbn_trm, "nodo":nodo, "nilai":nilai, "jasa":jasa },
  //         success: function(resp){
  //           $("#modal_trans").modal("hide");
  //           var obj = jQuery.parseJSON(resp);
  //           $.each(obj, function(key, data){
  //             $('#notbj').val(data.nobbn_trm);
  //               swal({
  //                 title: data.title,
  //                 text: data.msg,
  //                 type: data.tipe
  //               }, function(){
  //                 table.ajax.reload();
  //               });
  //             });
  //         },
  //         error:function(event, textStatus, errorThrown) {
  //           swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
  //         }
  //     });
  // } 

    function delDetail(nodo){     
      
        var nobbn_trm = $('#notbj').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('kbins/delDetail');?>",
            data: {"nobbn_trm":nobbn_trm
                    ,"nodo":nodo 
            },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    table.ajax.reload(); 
                });
            },
            error: function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function batal(){ 
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
            // var jnstrans = $("#jnstrans").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/batal");?>",
                data: { },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                      swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                      }, function() {
                        window.location.href = '<?=site_url('kbins');?>';  
                      }); 
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
            }); 
    }

    function pkons_submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var tglkb = $("#periode").val();
            var kdkb = $("#refkb").val(); 
            var jnstrx = $("#jnstrans").val(); 
            var dk = 'D'; 

            var nilai = $("#byrkons_nbyr").autoNumeric('get'); 
            var ket = ''; 

            var noso = $("#byrkons_noso").val(); 
            var nilaibyr = $("#byrkons_nbyr").autoNumeric('get');
            var jenis = 'PC'; 
            var noso = $("#byrkons_noso").val(); 
            var nocetak = $("#byrkons_noctk").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/pkons_submit");?>",
                data: {"tglkb":tglkb
                        ,"kdkb":kdkb
                        ,"jnstrx":jnstrx
                        ,"dk":dk
                        ,"nilai":nilai
                        ,"ket":ket
                        ,"noso":noso
                        ,"nilaibyr":nilaibyr
                        ,"jenis":jenis 
                        ,"nocetak":nocetak},
                success: function(resp){
                    var obj = JSON.parse(resp);
                        if(obj.kb_arkons_ins=1){
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                              $.each(obj, function(key, data){
                                  // window.open('kbins/pdf/'+data.kb_arkons_ins, '_blank');
                                  // var tglkb = $('#periode').val();
                                  // window.open('kbins/pdf/'+nokb, '_blank');
                                  window.open('kbins/pdf/'+data.kb_arkons_ins+'/'+tglkb, '_blank');
                              });
                                
                                window.location.href = '<?=site_url('kbins');?>';
                            }); 
                        }else{ 
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        }  
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

    function pleas_submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var tglkb = $("#periode").val();
            var kdkb = $("#refkb").val(); 
            var jnstrx = $("#jnstrans").val(); 
            var dk = 'D'; 

            var n1 = $("#pleas_nbyr1").autoNumeric('get'); 
            var n2 = $("#pleas_nbyr2").autoNumeric('get'); 
            var nilai = parseInt(n1) + parseInt(n2); 
            var ket = ''; 

            var noso = $("#pleas_noso").val(); 
            var nilai_pl = $("#pleas_nbyr1").autoNumeric('get');
            var nilai_jp = $("#pleas_nbyr2").autoNumeric('get'); 
            var nocetak = $("#byrkons_noctk").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/pleas_submit");?>",
                data: {"tglkb":tglkb
                        ,"kdkb":kdkb
                        ,"jnstrx":jnstrx
                        ,"dk":dk
                        ,"nilai":nilai
                        ,"ket":ket
                        ,"noso":noso
                        ,"nilai_pl":nilai_pl
                        ,"nilai_jp":nilai_jp 
                        ,"nocetak":nocetak},
                success: function(resp){
                    var obj = JSON.parse(resp);
                        if(obj.kb_arkons_ins=1){
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                                $.each(obj, function(key, data){
                                    // window.open('kbins/pdf/'+data.kb_arkons_ins, '_blank');
                                  window.open('kbins/pdf/'+data.kb_arkons_ins+'/'+tglkb, '_blank');
                                });
                                   
                                window.location.href = '<?=site_url('kbins');?>';
                            }); 
                        }else{ 
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        }  
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

    function rleas_submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nokb = $("#nokb").val();
            var tglkb = $("#periode").val();
            var kdkb = $("#refkb").val(); 
            var jnstrx = $("#jnstrans").val(); 
            var dk = 'D'; 

            var nilai = $("#rleas_nilai").autoNumeric('get'); 
            var ket = ''; 

            var jenis = $("#rleas_jenis").val(); 
            var kdleasing = $("#rleas_kdleas").val(); 
            var nilaibyr = $("#rleas_nilai").autoNumeric('get'); 
            var ket_det = $("#rleas_ket").val(); 
            var ket_det = ket_det.toUpperCase();
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/rleas_submit");?>",
                data: {"tglkb":tglkb
                        ,"kdkb":kdkb
                        ,"jnstrx":jnstrx
                        ,"dk":dk
                        ,"nilai":nilai
                        ,"ket":ket
                        ,"jenis":jenis
                        ,"kdleasing":kdleasing
                        ,"nilaibyr":nilaibyr 
                        ,"ket_det":ket_det},
                success: function(resp){
                    var obj = JSON.parse(resp); 
                        if(obj.kb_arkons_ins===''){
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        }else{ 
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                                $.each(obj, function(key, data){
                                    // window.open('kbins/pdf/'+data.kb_arkons_ins, '_blank');
                                    
                                  window.open('kbins/pdf/'+data.kb_arkons_ins+'/'+tglkb, '_blank');
                                });
                                window.location.href = '<?=site_url('kbins');?>';
                            }); 
                        }  
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

    function blkkons_submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nokb = $("#nokb").val();
            var tglkb = $("#periode").val();
            var kdkb = $("#refkb").val(); 
            var jnstrx = $("#jnstrans").val(); 
            var dk = 'K'; 

            var nilai = $("#blkkons_pengembalian").autoNumeric('get'); 
            var ket = ''; 

            var noso = $("#blkkons_noso").val();  
            var nilaibyr = $("#blkkons_pengembalian").autoNumeric('get');  
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/blkkons_submit");?>",
                data: {"tglkb":tglkb
                        ,"kdkb":kdkb
                        ,"jnstrx":jnstrx
                        ,"dk":dk
                        ,"nilai":nilai
                        ,"ket":ket
                        ,"noso":noso 
                        ,"nilaibyr":nilaibyr },
                success: function(resp){
                    var obj = JSON.parse(resp);
                        // alert(obj.kb_arkons_ins);
                        if(obj.kb_retkons_ins===''){
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        }else{ 
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                                $.each(obj, function(key, data){
                                    // window.open('kbins/pdf/'+data.kb_retkons_ins, '_blank');
                                  window.open('kbins/pdf/'+data.kb_retkons_ins+'/'+tglkb, '_blank');
                                }); 
                                window.location.href = '<?=site_url('kbins');?>';
                            }); 
                        }  
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

    function kbm_ins(){  

            var nokb = $("#nokb").val().toUpperCase();
            var jnstrans = $("#jnstrans").val();
            var nourut = $("#kbm_nourut").val();
            var kdrefkb = $("#kbm_jns").val();
            var drkpd = $("#kbm_drkpd").val().toUpperCase();
            var nofaktur = $("#kbm_nofaktur").val().toUpperCase();
            var ket = $("#kbm_ket").val().toUpperCase();
            var nilai = $("#kbm_nilai").autoNumeric('get'); 
 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/kbm_submit");?>",
                data: {"nokb":nokb
                        ,"nourut":nourut
                        ,"kdrefkb":kdrefkb
                        ,"drkpd":drkpd
                        ,"nofaktur":nofaktur
                        ,"ket":ket
                        ,"nilai":nilai
                        ,"jnstrans":jnstrans },
                success: function(resp){
                  $("#modal_kbm").modal("hide"); 
                  var obj = jQuery.parseJSON(resp);
                  $.each(obj, function(key, data){
                      // alert(data.tipe);
                      tbl_kbm(); 
                  });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    } 



  function kbm_del(){  

    // var rows_selected = tb_kbm.column(0).selected();
    // alert(nourut);
      // $('#kbm_urut').val(rows_selected.join(","))

      var nourut = tb_kbm.row('.selected').data()['nourut'];
      // var nourut = $("#kbm_urut").val();
      // alert(nourut);
      var nokb = $("#nokb").val();
      $.ajax({
        type: "POST",
        url: "<?=site_url('kbins/kbm_del');?>",
        data: {"nourut":nourut
            ,"nokb":nokb
        },
        success: function(resp){
          var obj = JSON.parse(resp);
          $.each(obj, function(key, data){ 
              tb_kbm.ajax.reload(); 
          });
        },
        error: function(event, textStatus, errorThrown) {
          swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
        }
      }); 
  }  

  function kbk_del(){  

    // var rows_selected = tb_kbm.column(0).selected();
    // alert(nourut);
      // $('#kbm_urut').val(rows_selected.join(","))

      var nourut = tb_kbk.row('.selected').data()['nourut'];
      // var nourut = $("#kbm_urut").val();
      // alert(nourut);
      var nokb = $("#nokb").val();
      $.ajax({
        type: "POST",
        url: "<?=site_url('kbins/kbm_del');?>",
        data: {"nourut":nourut
            ,"nokb":nokb
        },
        success: function(resp){
          var obj = JSON.parse(resp);
          $.each(obj, function(key, data){ 
              tb_kbk.ajax.reload(); 
          });
        },
        error: function(event, textStatus, errorThrown) {
          swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
        }
      }); 
  }



  function tkasbon_del(){  

    // var rows_selected = tb_kbm.column(0).selected();
    // alert(nourut);
      // $('#kbm_urut').val(rows_selected.join(","))

      var nourut = tbl_tkasbon.row('.selected').data()['nourut'];
      // var nourut = $("#kbm_urut").val();
      // alert(nourut);
      var nokb = $("#nokb").val();
      $.ajax({
        type: "POST",
        url: "<?=site_url('kbins/kbm_del');?>",
        data: {"nourut":nourut
            ,"nokb":nokb
        },
        success: function(resp){
          var obj = JSON.parse(resp);
          $.each(obj, function(key, data){ 
              tbl_tkasbon.ajax.reload(); 
          });
        },
        error: function(event, textStatus, errorThrown) {
          swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
        }
      }); 
  }



  function kasbon_del(){  

    // var rows_selected = tb_kbm.column(0).selected();
    // alert(nourut);
      // $('#kbm_urut').val(rows_selected.join(","))

      var nokasbon = tbl_kasbon.row('.selected').data()['nokasbon'];
      // var nourut = $("#kbm_urut").val();
      // alert(nourut); 
      $.ajax({
        type: "POST",
        url: "<?=site_url('kbins/kasbon_del');?>",
        data: {"nokasbon":nokasbon 
        },
        success: function(resp){
          var obj = JSON.parse(resp);
          $.each(obj, function(key, data){ 
              if(obj.state=1){ 
                  tbl_kasbon.ajax.reload();
              }else{ 
                  swal({
                      title: 'Data Gagal Dihapus',
                      text: obj.msg,
                      type: 'error'
                  }); 
              }  
          });
        },
        error: function(event, textStatus, errorThrown) {
          swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
        }
      }); 
  }

    function kbk_ins(){  

            var nokb = $("#nokb").val().toUpperCase();
            var jnstrans = $("#jnstrans").val();
            var nourut = $("#kbm_nourut").val();
            var kdrefkb = $("#kbm_jns").val();
            var drkpd = $("#kbm_drkpd").val().toUpperCase();
            var nofaktur = $("#kbm_nofaktur").val().toUpperCase();
            var ket = $("#kbm_ket").val().toUpperCase();
            var nilai = $("#kbm_nilai").autoNumeric('get'); 
 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/kbm_submit");?>",
                data: {"nokb":nokb
                        ,"nourut":nourut
                        ,"kdrefkb":kdrefkb
                        ,"drkpd":drkpd
                        ,"nofaktur":nofaktur
                        ,"ket":ket
                        ,"nilai":nilai
                        ,"jnstrans":jnstrans },
                success: function(resp){
                  $("#modal_kbm").modal("hide"); 
                  var obj = jQuery.parseJSON(resp);
                  $.each(obj, function(key, data){
                      // alert(data.tipe);
                      tbl_kbk(); 
                  });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    } 

    function tkasbon_ins(){  

            var nokb = $("#nokb").val().toUpperCase();
            var jnstrans = $("#jnstrans").val();
            var nourut = $("#kbm_nourut").val();
            var kdrefkb = $("#kbm_jns").val();
            var drkpd = $("#kbm_drkpd").val().toUpperCase();
            var nofaktur = $("#kbm_nofaktur").val().toUpperCase();
            var ket = $("#kbm_ket").val().toUpperCase();
            var nilai = $("#kbm_nilai").autoNumeric('get'); 
 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/kbm_submit");?>",
                data: {"nokb":nokb
                        ,"nourut":nourut
                        ,"kdrefkb":kdrefkb
                        ,"drkpd":drkpd
                        ,"nofaktur":nofaktur
                        ,"ket":ket
                        ,"nilai":nilai
                        ,"jnstrans":jnstrans },
                success: function(resp){
                  $("#modal_kbm").modal("hide"); 
                  var obj = jQuery.parseJSON(resp);
                  $.each(obj, function(key, data){
                      // alert(data.tipe);
                      tbl_kbbon(); 
                  });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    } 

    function rkasbon_ins(){  
 
            var nokasbon = $("#rkasbon_nokasbon").val(); 
            var nama = $("#rkasbon_nama").val(); 
            var tglkb = $("#rkasbon_tglkasbon").val(); 
            var kdkb = $("#rkasbon_kb").val(); 
            var ket = $("#rkasbon_ket").val(); 
            var nilai = $("#rkasbon_nkasbon").autoNumeric('get'); 
 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/rkasbon_ins");?>",
                data: {"nokasbon":nokasbon
                      ,"nama":nama
                      ,"tglkb":tglkb
                      ,"kdkb":kdkb 
                      ,"ket":ket
                      ,"nilai":nilai  },
                // success: function(resp){  
                //   var obj = jQuery.parseJSON(resp);
                //   $.each(obj, function(key, data){
                //       // alert(data.tipe);
                //       tbl_kbbon(); 
                //   });
                // },
                success: function(resp){
                    var obj = JSON.parse(resp);
                        if(obj.state=1){ 
                                tbl_kasbon.ajax.reload();
                        }else{ 
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        }  
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    } 

    function kbsubmit(){  
 
            var jnstrans = $("#jnstrans").val(); 
            var kdkb = $("#refkb").val();
            var tglkb = $("#periode").val();
            var dk = $("#dk").val().toUpperCase();
            var ket = '';
 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/kbsubmit");?>",
                data: {"tglkb":tglkb
                        ,"kdkb":kdkb
                        ,"jnstrans":jnstrans
                        ,"dk":dk
                        ,"ket":ket },
                success: function(resp){ 
                  var obj = jQuery.parseJSON(resp);
                  $.each(obj, function(key, data){
                      // alert(data.tipe);
                      
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                              if(data.tipe==='success'){
                                // window.open('kbins/pdf/'+data.nokb, '_blank');
                                  window.open('kbins/pdf/'+data.nokb+'/'+tglkb, '_blank');
                                window.location.href = '<?=site_url('kbins');?>';
                              } else {
                                tb_kbm.ajax.reload();
                              }
                            }); 
                  });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    } 

    function rkasbon_submit(){  
 
            var jnstrans = $("#jnstrans").val(); 
            var kdkb = $("#refkb").val();
            var tglkb = $("#periode").val();
            var dk = $("#dk").val().toUpperCase();
            var ket = '';
 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kbins/rkasbon_submit");?>",
                data: {"tglkb":tglkb
                        ,"kdkb":kdkb
                        ,"jnstrans":jnstrans
                        ,"dk":dk
                        ,"ket":ket },
                success: function(resp){ 
                  var obj = jQuery.parseJSON(resp);
                  $.each(obj, function(key, data){
                      // alert(data.tipe);
                      
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                              if(data.tipe==='success'){
                                // window.open('kbins/pdf/'+data.nokb, '_blank');
                                  window.open('kbins/pdf/'+data.nokb+'/'+tglkb, '_blank');
                                window.location.href = '<?=site_url('kbins');?>';
                              } else {
                                tb_kbm.ajax.reload();
                              }
                            }); 
                  });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    } 

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 

    function init_pkons_cari(){
        $("#byrkons_cari").select2({
            ajax: {
                url: "<?=site_url('kbins/get_pkons');?>",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
          processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan No DO / No. SPK / Nama Konsumen ...',
            dropdownAutoWidth : true, 
            width: '100%',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 3,
                        templateResult: pkons_format_nama, // omitted for brevity, see the source of this page
                        templateSelection: pkons_format_nama_terpilih // omitted for brevity, see the source of this page
                    });
    }

    function pkons_format_nama_terpilih (repo) {
        return repo.full_name || repo.id;
    }

    function pkons_format_nama (repo) {
        if (repo.loading) return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.nama + "</b></div>";

        markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'> No. SPK <i class=\"fa fa-book\"></i> " + repo.id +
        "</div>" +
        "<div class='select2-result-repository__forks'> No. DO <i class=\"fa fa-book\"></i> " + repo.text +
        "</div>" +
        "</div>" +
        "</div>";
        return markup;
    }

    function init_pleas_cari(){
        $("#pleas_cari").select2({
            ajax: {
                url: "<?=site_url('kbins/get_pleas');?>",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
          processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan No DO / No. SPK / Nama Konsumen ...',
            dropdownAutoWidth : true, 
            width: '100%',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 3,
                        templateResult: pleas_format_nama, // omitted for brevity, see the source of this page
                        templateSelection: pleas_format_nama_terpilih // omitted for brevity, see the source of this page
                    });
    }

    function pleas_format_nama_terpilih (repo) {
        return repo.full_name || repo.id;
    }

    function pleas_format_nama (repo) {
        if (repo.loading) return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.nama + "</b></div>";

        markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'> No. SPK <i class=\"fa fa-book\"></i> " + repo.id +
        "</div>" +
        "<div class='select2-result-repository__forks'> No. DO <i class=\"fa fa-book\"></i> " + repo.text +
        "</div>" +
        "</div>" +
        "</div>";
        return markup;
    }

    function init_blkkons_cari(){
        $("#blkkons_cari").select2({
            ajax: {
                url: "<?=site_url('kbins/get_blkkons');?>",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
          processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan No DO / No. SPK / Nama Konsumen ...',
            dropdownAutoWidth : true, 
            width: '100%',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 3,
                        templateResult: blkkons_format_nama, // omitted for brevity, see the source of this page
                        templateSelection: blkkons_format_nama_terpilih // omitted for brevity, see the source of this page
                    });
    }

    function blkkons_format_nama_terpilih (repo) {
        return repo.full_name || repo.id;
    }

    function blkkons_format_nama (repo) {
        if (repo.loading) return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.nama + "</b></div>";

        markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'> No. SPK <i class=\"fa fa-book\"></i> " + repo.id +
        "</div>" +
        "<div class='select2-result-repository__forks'> No. DO <i class=\"fa fa-book\"></i> " + repo.text +
        "</div>" +
        "</div>" +
        "</div>";
        return markup;
    }

    function getKdjenis(){
        var jnstrans = $("#jnstrans").val();
        if(jnstrans==='K/B MASUK - UMUM (D)'){
          var ket = 'D';
        } else {
          var ket = 'K';
        }
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("kbins/getKdjenis");?>",
            data: {"ket":ket},
            beforeSend: function() {
                $('#kbm_jns').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis --'));
                $("#kbm_jns").trigger("change.chosen");
                if ($('#kbm_jns').hasClass("chosen-hidden-accessible")) {
                    $('#kbm_jns').select2('destroy');
                    $("#kbm_jns").chosen({ width: '100%' });
                }
                $('#kbk_jns').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis --'));
                $("#kbk_jns").trigger("change.chosen");
                if ($('#kbk_jns').hasClass("chosen-hidden-accessible")) {
                    $('#kbk_jns').select2('destroy');
                    $("#kbk_jns").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kbm_jns')
                      .append($('<option>', { value : value.kdrefkb })
                      .html("<b style='font-size: 12px;'>" + value.nmrefkb + " </b>"));
                  
                  $('#kbk_jns')
                      .append($('<option>', { value : value.kdrefkb })
                      .html("<b style='font-size: 12px;'>" + value.nmrefkb + " </b>"));
                });

                $('#kbm_jns').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                });

                $('#kbk_jns').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
</script>

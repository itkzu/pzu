<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstunit
 *
 * @author
 */
class Mstunit extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstunit/submit'),
            'add' => site_url('mstunit/add'),
            'edit' => site_url('mstunit/edit'),
            'reload' => site_url('mstunit'),
        );
        $this->load->model('mstunit_qry');

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('edit',$this->data);

    }


    public function getDetail() {
        echo $this->mstunit_qry->getDetail();
    }

    public function ListKodeUnit() {
        echo $this->mstunit_qry->ListKodeUnit();
    }

    public function SimpanDetail() {
        echo $this->mstunit_qry->SimpanDetail();
    }

    public function HapusDetail() {
        echo $this->mstunit_qry->HapusDetail();
    }

    public function proses_setaktif(){
        echo $this->mstunit_qry->proses_setaktif();

    }



    public function getDetailEd() {
        echo $this->mstunit_qry->getDetailEd();
    }

    public function ListKodeUnitEd() {
        echo $this->mstunit_qry->ListKodeUnitEd();
    }

    public function SimpanDetailEd() {
        echo $this->mstunit_qry->SimpanDetailEd();
    }

    public function HapusDetailEd() {
        echo $this->mstunit_qry->HapusDetailEd();
    }



    public function json_dgview() {
        echo $this->mstunit_qry->json_dgview();
    }

    public function submit() {
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');

        if($this->validate($id,$stat) == TRUE){
            $res = $this->mstunit_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->set_layout('main')->build('form', $this->data);
                    }else{
                        $this->_check_id($id);
                        $this->template->set_layout('main')->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($id)){
                $this->_init_add();
                $this->template->set_layout('main')->build('form', $this->data);
            }else{
                $this->_check_id($id);
                $this->template->set_layout('main')->build('form', $this->data);
            }
        }
    }

    private function _init_add(){

        $this->data['form'] = array(
           'kdgift'=> array(
                'type'        => 'hidden',
                'placeholder' => 'ID',
                'id'          => 'kdgift',
                'name'        => 'kdgift',
                'value'       => '',
                'class'       => 'form-control',
                'readonly'    => '',
            ),
           'nmgift'=> array(
                'placeholder' => 'Nama Program Hadiah',
                'id'          => 'nmgift',
                'name'        => 'nmgift',
                'value'       => set_value('nmgift'),
                'class'       => 'form-control',
                'autofocus'   => '',
                'required'    => '',
            ),
            'tglawal'=> array(
                'placeholder' => 'Tanggal Mulai',
                'id'          => 'tglawal',
                'name'        => 'tglawal',
                'value'       => set_value('tglawal'),
                'class'       => 'form-control calendar',
                'required'    => '',
            ),
           'tglakhir'=> array(
                'placeholder' => 'Tanggal Berakhir',
                'id'          => 'tglakhir',
                'name'        => 'tglakhir',
                'value'       => set_value('tglakhir'),
                'class'       => 'form-control calendar',
                'required'    => '',
            ),
           'ket'=> array(
                'placeholder' => 'Keterangan Hadiah',
                'id'          => 'ket',
                'name'        => 'ket',
                'value'       => set_value('ket'),
                'class'       => 'form-control',
                'required'    => '',
                'style'       => 'resize: vertical; height: 115px; min-height: 35px;'
            ),
        );
    }

    private function _init_edit(){
        $groupid = $this->uri->segment(3);
        $this->_check_id($groupid);

        echo "<script> console.log('PHP: ". json_encode($this->val) ."');</script>";

        $this->data['form'] = array(
           'kdgift'=> array(
                'type'        => 'hidden',
                'placeholder' => 'ID',
                'id'          => 'kdgift',
                'name'        => 'kdgift',
                'value'       => $this->val[0]['kdgift'],
                'class'       => 'form-control',
                'readonly'    => ''
            ),
           'nmgift'=> array(
                'placeholder' => 'Nama Program Hadiah',
                'id'          => 'nmgift',
                'name'        => 'nmgift',
                'value'       => $this->val[0]['nmgift'],
                'class'       => 'form-control',
                'autofocus'   => '',
                'required'    => '',
            ),
           'tglawal'=> array(
                'placeholder' => 'Tanggal Mulai',
                'id'          => 'tglawal',
                'name'        => 'tglawal',
                'value'       => $this->val[0]['tglawal'],
                'class'       => 'form-control calendar',
                'required'    => '',
            ),
           'tglakhir'=> array(
                'placeholder' => 'Tanggal Berakhir',
                'id'          => 'tglakhir',
                'name'        => 'tglakhir',
                'value'       => $this->val[0]['tglakhir'],
                'class'       => 'form-control calendar',
                'required'    => '',
            ),
           'ket'=> array(
                'placeholder' => 'Keterangan Hadiah',
                'id'          => 'ket',
                'name'        => 'ket',
                'value'       => $this->val[0]['ket'],
                'class'       => 'form-control',
                'required'    => '',
                'style'       => 'resize: vertical; height: 115px; min-height: 35px;'
            ),
        );
    }

    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }

        $this->val = $this->mstunit_qry->select_data($id);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nmgift',
                    'label' => 'Nama Hadiah',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

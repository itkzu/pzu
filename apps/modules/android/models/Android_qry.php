<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Android_qry
 *
 * @author adi
 */

require_once APPPATH ."/libraries/myjwt/JWT.php";
require_once APPPATH ."/libraries/myjwt/ExpiredException.php";
require_once APPPATH ."/libraries/myjwt/BeforeValidException.php";
require_once APPPATH ."/libraries/myjwt/SignatureInvalidException.php";
use \Firebase\JWT\JWT;
class Android_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getStokHargaUnit() {
        $query = $this->db->get("pzu.v_stock_unit_all");
        return json_encode($query->result_array());
    }
    
    public function submit() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');   

        if(strtoupper($username)=="SADMIN" && $password=="3335"){
            $this->db->join('lokasi','user.kdlokasi = lokasi.kdlokasi');
            $this->db->limit(1);
            $q_login = $this->db->get('user'); 
            if($q_login->num_rows()>0){
                $dbrbac = $this->load->database('rbac', TRUE);
                $dbrbac->where("lower(user_id)",  strtolower($username));
                $dbrbac->join('groups','groups.rowid = users_groups.group_id');
                $q_ug = $dbrbac->get('users_groups');
                $d_ug = $q_ug->result();
                $this->load->database('rbac', FALSE);            

                $d_login = $q_login->result();
                $res = array(
                    'state' => '1',
                    'logged_in' => '1',
                    'title' => 'Login Berhasil',
                    'msg' => 'Login Berhasil, Selamat Datang '.strtoupper($username),
                    'groupname' => $d_ug[0]->name,
                    'username'  => strtoupper($username),
                    'nmuser'  => ucwords(strtolower($username)),
                    'kddiv'  => $d_login[0]->kddiv,
                    'nmlokasi'  => $d_login[0]->nmlokasi,
                    'jenis'  => $d_login[0]->jenis,
                    "exp" => time() + JWT::$leeway + 300,
                );
                $res['token'] = JWT::encode($res, $this->rbac->key);
            }
            else{
                $res = array(
                    'state' => '0',
                    'logged_in' => '0',
                    'title' => 'Login Gagal',
                    'msg' => 'Username atau Password anda salah',
                );
            }
        }else{
            $this->db->join('lokasi','user.kdlokasi = lokasi.kdlokasi');
            $q_login = $this->db->get_where('user',array('kduser' => strtoupper($username),'pwd' => $password)); 
            if($q_login->num_rows()>0){

                $dbrbac = $this->load->database('rbac', TRUE);
                $dbrbac->where("lower(user_id)",  strtolower($username));
                $dbrbac->join('groups','groups.rowid = users_groups.group_id');
                $q_ug = $dbrbac->get('users_groups');
                $d_ug = $q_ug->result();
                $this->load->database('rbac', FALSE);            

                $d_login = $q_login->result();
                $res = array(
                    'state' => '1',
                    'logged_in' => '1',
                    'title' => 'Login Berhasil',
                    'msg' => 'Login Berhasil, Selamat Datang '.strtoupper($username),
                    'groupname' => $d_ug[0]->name,
                    'username'  => strtoupper($username),
                    'nmuser'  => ucwords(strtolower($username)),
                    'kddiv'  => $d_login[0]->kddiv,
                    'nmlokasi'  => $d_login[0]->nmlokasi,
                    'jenis'  => $d_login[0]->jenis,
                    "exp" => time() + JWT::$leeway + 300,
                );
                $res['token'] = JWT::encode($res, $this->rbac->key);
            }
            else{
                $res = array(
                    'state' => '0',
                    'logged_in' => '0',
                    'title' => 'Login Gagal',
                    'msg' => 'Username atau Password anda salah',
                );
            }
        }
        return $res;
    }
    
    // Service GLR_H Start
    public function getGlrhAkunDiv() {
        try{
            
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT akun_div.kddiv, akun_div.kdakun, akun_div.parent, 
                            akun_div.sawal, akun_div.debet, akun_div.kredit, 
                            akun_div.periode, akun_div.faktif, akun_div.par_div, 
                            akun_div.par_head
                       FROM glr_h.akun_div
                       JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.akun_div {$where_periode}) AS period
                        ON akun_div.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    
    public function getGlrhNeraca() {
        try{
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT neraca.kddiv, neraca.kdheader, neraca.nmheader, neraca.parent, 
                            neraca.jenis, neraca.level, neraca.par_div, neraca.par_head, 
                            neraca.nominal, neraca.fshow, neraca.periode
                       FROM glr_h.neraca
                       JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.neraca {$where_periode}) AS period
                        ON neraca.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    
    public function getGlrhTJurnal() {
        try{
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT t_jurnal.nojurnal, t_jurnal.tgljurnal, t_jurnal.kddiv, 
                    t_jurnal.noref, t_jurnal.posting, t_jurnal.ket, t_jurnal.periode, 
                    t_jurnal.tglentry, t_jurnal.userentry, t_jurnal.ismemorial, t_jurnal.jenis
                       FROM glr_h.t_jurnal
                       JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.t_jurnal {$where_periode}) AS period
                        ON t_jurnal.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    
    public function getGlrhTJurnalD() {
        try{
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT t_jurnal_d.nojurnal, t_jurnal_d.nourut, t_jurnal_d.kddiv, 
                            t_jurnal_d.kdakun, t_jurnal_d.dk, t_jurnal_d.nominal, 
                            t_jurnal_d.ket, t_jurnal.periode
                      FROM glr_h.t_jurnal_d 
                        JOIN glr_h.t_jurnal 
                            ON t_jurnal.nojurnal = t_jurnal_d.nojurnal
                        JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.t_jurnal {$where_periode}) AS period
                            ON t_jurnal.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    // Service GLR_H End

    // Service target penjualan start
    public function gettargetbulan($periode) {
        /*
        if(!$periode){
            $periode = date('Y-m');
        }
        */


        /*
         $query = $this->db->get_where('mstcptarget'
                , array(
                    "to_char(periode,'YYYY-MM')" => $periode
                , "kd_cabang" => $this->apps->kd_cabang
                )); 
		*/

		//$this->db->where('kd_cabang',$this->apps->kd_cabang);	
    /*
		$this->db->where("to_char(periode,'YYYY-MM')",$periode);			
		$this->db->order_by('periode','desc');	
		$this->db->limit(1);
		$query = $this->db->get('mstcptarget');
        if($query->num_rows()>0){
            $res = $query->result_array();
            return (int) $res[0]['cp_target_total'];
        }else{
            return (int) '0';
        }
    */

        $str = "select * from pzu.dba_target_do_gp('". $periode ."')";
        // echo $str;
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }

    // ALL CABANG    
    public function getJualAllCab($periode) {
        $str = "select * from pzu.dba_rekap_do('". $periode ."')";
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    } 
    
    public function getJualAllCabLeas($periode) {
        $str = "select  kddiv , mtdadr || '[' || round(mtdadr / total * 100) || '%]' as mtdadr
                              , mtdfif || '[' || round(mtdfif / total * 100) || '%]' as mtdfif
                              , mtdbca || '[' || round(mtdbca / total * 100) || '%]' as mtdbca
                              , mtdoto || '[' || round(mtdoto / total * 100) || '%]' as mtdoto
                              , mtdmuf || '[' || round(mtdmuf / total * 100) || '%]' as mtdmuf
                              , mtdothers || '[' || round(mtdothers / total * 100) || '%]' as mtdothers
                              from pzu.dba_rekap_leas('". $periode ."')";
                              // , ytdadr , ytdfif , ytdbca , ytdoto , ytdmuf , ytdothers from pzu.dba_rekap_leas('". $periode ."')
        $query = $this->db->query($str);
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }

    public function getJualAllCabtipex($periode) {
        $tgl = $this->apps->dateConvert($periode);
        $str = "select * from pzu.dba_rekap_tipex('". $tgl ."')";
        $query = $this->db->query($str);
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    } 

    public function getJualAllCabtipe($periode) {
        $tgl = $this->apps->dateConvert($periode);
        $str = "select * from pzu.dba_rekap_tipe('". $tgl ."')";
        $query = $this->db->query($str);
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    } 

    public function getJualAllCabtipeyear($periode) {
        $tgl = $this->apps->dateConvert($periode);
        $str = "select * from pzu.dba_rekap_tipey('". $tgl ."')";
        $query = $this->db->query($str);
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    } 

    public function getStokAllCabtipe($periode) {
        $tgl = $this->apps->dateConvert($periode);
        $str = "select * from pzu.dba_rekap_stok('". $tgl ."')";
        $query = $this->db->query($str);
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    } 
    
    public function getblmtrmpoleasing($nm_cabang, $periode) {
        if(!$periode){
            $periode = date('Y-m');
        }
        $str = "select '{$nm_cabang}' as cabang
                            , kdleasing
                            , count(nodo) 
                    from pzu.vm_trm_po_leasing 
                            where tgltrmpo is null 
                    group by kdleasing 
                    order by kdleasing";
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }
    
    public function getblmtrmpoleasingdetail($kdleasing, $periode) {
        if(!$periode){
            $periode = date('Y-m');
        }
        $str = "select kdleasing
                , nmprogleas
                , pl
                , nodo
                , tgldo
                , nama_s
                , nmtipe
                , nmwarna 
                from pzu.vm_trm_po_leasing 
                where tgltrmpo is null 
                    and kdleasing = '{$kdleasing}'
                order by nodo";
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }
    // Service target penjualan end

    public function getBeliAstra() { 
        $array = $this->input->post();
    
            $dt = array();
            foreach (getallheaders() as $name => $value) {
                $dt[$name] = $value;
                //echo "$name: $value\n";            
            }

            //$array['rdjenis'] = $dt['key'];
            $array['kddiv'] = $dt['kddiv'];
            $array['periode_akhir'] = $dt['tahun']."-".$dt['bulan_akhir'];
            $array['periode_awal'] = $dt['tahun']."-".$dt['bulan_awal'];

            if ($dt['key'] == 'pzu'){
                $res = $this->rpt_po_astra($array);
            } else{
                $res = "";
            }
            return json_encode($res);              
    }

    public function getJualAll() { 
        $array = $this->input->post();
    
            $dt = array();
            foreach (getallheaders() as $name => $value) {
                $dt[$name] = $value;
                //echo "$name: $value\n";            
            }

            //$array['rdjenis'] = $dt['key'];
            $array['kddiv'] = $dt['kddiv'];
            $array['periode_akhir'] = $dt['tahun']."-".$dt['bulan_akhir'];
            $array['periode_awal'] = $dt['tahun']."-".$dt['bulan_awal'];

            if ($dt['key'] == 'pzu'){
                $res = $this->rpt_so_all($array);
            } else{
                $res = "";
            }
            return json_encode($res);             
    }

    public function getJualAllPersen() { 
        $array = $this->input->post();
    
            $dt = array();
            foreach (getallheaders() as $name => $value) {
                $dt[$name] = $value;
                //echo "$name: $value\n";            
            }

            //$array['rdjenis'] = $dt['key'];
            $array['kddiv'] = $dt['kddiv'];
            $array['periode_akhir'] = $dt['tahun']."-".$dt['bulan_akhir'];
            $array['periode_awal'] = $dt['tahun']."-".$dt['bulan_awal'];

            if ($dt['key'] == 'pzu'){
                $res = $this->rpt_so_all_persen($array);
            } else{
                $res = "";
            }
            return json_encode($res);             
    }

    public function getJualAllPersenK() { 
        $array = $this->input->post();
    
            $dt = array();
            foreach (getallheaders() as $name => $value) {
                $dt[$name] = $value;
                //echo "$name: $value\n";            
            }

            //$array['rdjenis'] = $dt['key'];
            $array['kddiv'] = $dt['kddiv'];
            $array['periode_akhir'] = $dt['tahun']."-".$dt['bulan_akhir'];
            $array['periode_awal'] = $dt['tahun']."-".$dt['bulan_awal'];

            if ($dt['key'] == 'pzu'){
                $res = $this->rpt_so_all_persenK($array);
            } else{
                $res = "";
            }
            return json_encode($res);             
    }
    
    private function rpt_po_astra($array) {

        if (!isset($array['kddiv'])){
            $array['kddiv'] = $this->session->userdata('data')['kddiv'];
        } 
            
        $str = "SELECT count(nosin) as jml , nmsup, periode FROM pzu.vl_po_astra WHERE nmsup like '%ASTRA%' and to_char(tglpo::date, 'YYYY-MM') between '".$array['periode_awal']."' and '".$array['periode_akhir']."' group by nmsup,periode";   

        $q_periode = $this->db->query("SELECT prd.periode FROM (".$str.") as prd GROUP BY prd.periode ORDER BY prd.periode ASC");
        // echo  $this->db->last_query();
        $data_periode = $q_periode->result_array();

            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", sum(CASE WHEN periode = '".$value['periode']."' THEN jml "
                        . " ELSE 0 END)::integer AS \"" . $value['periode']."\"";
            }

            $q = $this->db->query("SELECT nmsup " .$str_periode_nominal.", '".$array['periode_akhir']."' as periode_aktif FROM (".$str.") AS akn GROUP BY nmsup ORDER BY nmsup");       
        // echo  $this->db->last_query();
        if($q->num_rows()>0){  
            $data = $q->result_array(); 
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => 'ZPH01.02S',
              'periode_aktif' => $array['periode_akhir'],
            );
            return $data[0];
        }else{
            return false;
        } 
    }
    
    private function rpt_so_all($array) {

        if (!isset($array['kddiv'])){
            $array['kddiv'] = $this->session->userdata('data')['kddiv'];
        } 
            
        $str = "SELECT count(a.nodo) as jml, substring(b.kddiv,1,8) as kddiv, a.periode FROM pzu.t_do a JOIN pzu.t_so b ON a.noso = b.noso WHERE to_char(a.tgldo::date, 'YYYY-MM') between '".$array['periode_awal']."' and '".$array['periode_akhir']."' group by substring(b.kddiv,1,8), a.periode";   

        $q_periode = $this->db->query("SELECT prd.periode FROM (".$str.") as prd GROUP BY prd.periode ORDER BY prd.periode ASC");
        // echo  $this->db->last_query();
        $data_periode = $q_periode->result_array();
        // echo "<script> console_log(".$data_periode.") </scrip";
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", sum(CASE WHEN periode = '".$value['periode']."' THEN jml "
                        . " ELSE 0 END)::integer AS \"" . $value['periode']."\"";
            }

            $q = $this->db->query("SELECT kddiv " .$str_periode_nominal.", '".$array['periode_akhir']."' as periode_aktif FROM (".$str.") AS akn GROUP BY kddiv");       
            // echo  $this->db->last_query();
        if($q->num_rows()>0){  
            $data = $q->result_array(); 
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => 'ZPH01.02S',
              'periode_aktif' => $array['periode_akhir'],
            );
            return $data[0];
        }else{
            return false;
        } 
    }
    
    private function rpt_so_all_persen($array) {

        if (!isset($array['kddiv'])){
            $array['kddiv'] = $this->session->userdata('data')['kddiv'];
        } 
            
        $str = "SELECT count(a.nodo) as jml, substring(b.kddiv,1,8) as kddiv, a.periode, 'T'::text as jenis FROM pzu.t_do a JOIN pzu.t_so b ON a.noso = b.noso WHERE to_char(a.tgldo::date, 'YYYY-MM') between '".$array['periode_awal']."' and '".$array['periode_akhir']."' AND b.jnsbayar = 'T' group by substring(b.kddiv,1,8), a.periode UNION SELECT count(a.nodo) as jml, substring(b.kddiv,1,8) as kddiv, a.periode, 'K'::text as jenis  FROM pzu.t_do a JOIN pzu.t_so b ON a.noso = b.noso WHERE to_char(a.tgldo::date, 'YYYY-MM') between '".$array['periode_awal']."' and '".$array['periode_akhir']."' AND b.jnsbayar = 'K' group by substring(b.kddiv,1,8), a.periode ";   

        $q_periode = $this->db->query("SELECT prd.periode FROM (".$str.") as prd GROUP BY prd.periode ORDER BY prd.periode ASC");
        // echo  $this->db->last_query();
        $data_periode = $q_periode->result_array();
        // echo "<script> console_log(".$data_periode.") </scrip";
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", sum(CASE WHEN periode = '".$value['periode']."' AND jenis = 'T' THEN jml "
                        . " ELSE 0 END)::integer AS \"" . $value['periode']."-t\"";
            }

            $q = $this->db->query("SELECT kddiv " .$str_periode_nominal.", '".$array['periode_akhir']."' as periode_aktif FROM (".$str.") AS akn GROUP BY kddiv");       
            // echo  $this->db->last_query();
        if($q->num_rows()>0){  
            $data = $q->result_array(); 
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => 'ZPH01.02S',
              'periode_aktif' => $array['periode_akhir'],
            );
            return $data[0];
        }else{
            return false;
        } 
    }
    
    private function rpt_so_all_persenK($array) {

        if (!isset($array['kddiv'])){
            $array['kddiv'] = $this->session->userdata('data')['kddiv'];
        } 
            
        $str = "SELECT count(a.nodo) as jml, substring(b.kddiv,1,8) as kddiv, a.periode, 'K'::text as jenis  FROM pzu.t_do a JOIN pzu.t_so b ON a.noso = b.noso WHERE to_char(a.tgldo::date, 'YYYY-MM') between '".$array['periode_awal']."' and '".$array['periode_akhir']."' AND b.jnsbayar = 'K' group by substring(b.kddiv,1,8), a.periode ";   

        $q_periode = $this->db->query("SELECT prd.periode FROM (".$str.") as prd GROUP BY prd.periode ORDER BY prd.periode ASC");
        // echo  $this->db->last_query();
        $data_periode = $q_periode->result_array();
        // echo "<script> console_log(".$data_periode.") </scrip";
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", sum(CASE WHEN periode = '".$value['periode']."' AND jenis = 'K' THEN jml "
                        . " ELSE 0 END)::integer AS \"" . $value['periode']."-k\"";
            }

            $q = $this->db->query("SELECT kddiv " .$str_periode_nominal.", '".$array['periode_akhir']."' as periode_aktif FROM (".$str.") AS akn GROUP BY kddiv");       
            // echo  $this->db->last_query();
        if($q->num_rows()>0){  
            $data = $q->result_array(); 
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => 'ZPH01.02S',
              'periode_aktif' => $array['periode_akhir'],
            );
            return $data[0];
        }else{
            return false;
        } 
    }

    public function getJualLeas() { 
        $array = $this->input->post();
    
            $dt = array();
            foreach (getallheaders() as $name => $value) {
                $dt[$name] = $value;
                //echo "$name: $value\n";            
            }

            //$array['rdjenis'] = $dt['key'];
            $array['kddiv'] = $dt['kddiv'];
            $array['periode_akhir'] = $dt['tahun']."-".$dt['bulan_akhir'];
            $array['periode_awal'] = $dt['tahun']."-".$dt['bulan_awal'];

            if ($dt['key'] == 'pzu'){
                $res = $this->rpt_do_leas($array);
            } else{
                $res = "";
            }
            return json_encode($res); 
    }
    
    private function rpt_do_leas($array) {

        if (!isset($array['kddiv'])){
            $array['kddiv'] = $this->session->userdata('data')['kddiv'];
        } 
            
        $str = "SELECT count(nodo) as jml , kdleasing, substring(kddiv,1,9) as kddiv, periode FROM pzu.vl_jual WHERE tk = 'K' and to_char(tglpo::date, 'YYYY-MM') between '".$array['periode_awal']."' and '".$array['periode_akhir']."' group by substring(kddiv,1,9),kdleasing,periode";   

        $q_periode = $this->db->query("SELECT prd.periode FROM (".$str.") as prd GROUP BY prd.periode ORDER BY prd.periode ASC");
        // echo  $this->db->last_query();
        $data_periode = $q_periode->result_array();

            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", sum(CASE WHEN periode = '".$value['periode']."' THEN coalesce(jml,0) ELSE 0 END)::integer AS \"" . $value['periode']."\"";
            }

            $q = $this->db->query("SELECT kddiv " .$str_periode_nominal.", '".$array['periode_akhir']."' as periode_aktif FROM (".$str.") AS akn GROUP BY kddiv ORDER BY kddiv");       
        // echo  $this->db->last_query();
        if($q->num_rows()>0){  
            $data = $q->result_array(); 
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => 'ZPH01.02S',
              'periode_aktif' => $array['periode_akhir'],
            );
            return $data[0];
        }else{
            return false;
        } 
    }

    public function getJualPerBulanKredit($periode) { 
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_do_real GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_do_real.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') BETWEEN SUBSTRING('".$periode."',1,4)||'-01' AND SUBSTRING('".$periode."',1,4)||'-'||SUBSTRING('".$periode."',6,2))) AS vl_do_real");

        // echo  $this->db->last_query();
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
            return $res; 
    }

    public function getJualYTD($periode) {
        $this->db->select("COUNT(nodo) AS jml, vl_do_real.tk");
        $this->db->group_by("vl_do_real.tk");
        $this->db->order_by("vl_do_real.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') BETWEEN SUBSTRING('".$periode."',1,4)||'-01' AND SUBSTRING('".$periode."',1,4)||'-'||SUBSTRING('".$periode."',6,2))) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }

        if($query->num_rows()>0){
            return $res;
        }else{
            return null;
        }
    }

    public function getJualMTD($periode) {
        $this->db->select("coalesce(COUNT(nodo),0) AS jml, vl_do_real.tk");
        $this->db->group_by("vl_do_real.tk");
        $this->db->order_by("vl_do_real.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_do_real WHERE tk = 'K' AND (to_char(tgldo,'YYYY-MM') = SUBSTRING('".$periode."',1,4)||'-'||SUBSTRING('".$periode."',6,2))) AS vl_do_real");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        } 

        if($query->num_rows()>0){
            return $res;
        }else{
            return null;
        }
    }
}

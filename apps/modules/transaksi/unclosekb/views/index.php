<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <!-- /.box-header -->
          <div class="box-body">
                <div class="table-responsive">
                <table class="dataTable table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th style="width: 2%;text-align: center;">No.</th>
                            <th style="width: 15%;text-align: center;">Kode K/B</th>
                            <th style="width: 25%;text-align: center;">Nama K/B</th>
                            <th style="width: 15%;text-align: center;">Tanggal K/B</th>
                            <th style="width: 25%;text-align: center;">Nama Akun</th>
                            <th style="width: 15%;text-align: center;">Status</th>
                            <th style="width: 2%;text-align: center;">
            									<i class="fa fa-th-large"></i></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 2%;text-align: center;">No.</th>
                            <th style="width: 15%;text-align: center;"></th>
                            <th style="width: 25%;text-align: center;"></th>
                            <th style="width: 15%;text-align: center;"></th>
                            <th style="width: 20%;text-align: center;"></th>
                            <th style="width: 20%;text-align: center;"></th>
                            <th style="width: 2%;text-align: center;">Unclose</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

      var column = [];

      column.push({
      		"aTargets": [ 3 ],
      		"mRender": function (data, type, full) {
      			return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
      		},
      		"sClass": "center"
      });

      column.push({
      		"aTargets": [  ],
      		"mRender": function (data, type, full) {
      				return type === 'export' ? data : numeral(data).format('0,0.00');
      		},
      		"sClass": "right"
      });

        table = $('.dataTable').DataTable({
            "columnDefs": [
                { "targets": 0 , "data": null,"sortable": false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "targets": 3,
            		  "mRender": function (data, type, full) {
            			    return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            		  },
            		  "sClass": "center"
                },
                { "orderable": false, "targets": 6 }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('unclosekb/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            }
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Unclose" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });

    function refresh(){
        table.ajax.reload();
    }

    function unclosing(kdkb){
        swal({
            title: "Konfirmasi",
            text: "Proses Unclosing Kas & Bank akan dilakukan, data akan dibuka kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            },

            function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('unclosekb/unclosing');?>",
                    data: {"kdkb":kdkb},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                refresh();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }
</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_input($form['kdlokasi']);

                              echo form_label($form['nmlokasi']['placeholder']);
                              echo form_input($form['nmlokasi']);
                              echo form_error('nmlokasi','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['jenis']['placeholder']);
                              echo form_dropdown( $form['jenis']['name'],
                                                  $form['jenis']['data'] ,
                                                  $form['jenis']['value'] ,
                                                  $form['jenis']['attr']);
                              echo form_error('jenis','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-3">
                  <div class="row">
                      <div class="form-group">
                          <?=form_label($form['faktif']['placeholder']);?>
                          <div class="checkbox">
            							    <?php
                                  echo form_checkbox($form['faktif']);
            							    ?>
            					    </div>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php  
                              // echo form_label($form['status']['placeholder']);
                              echo form_input($form['status']);
                              echo form_error('status','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

      if(!$('#kdlokasi').val()){
        $('#faktif').hide();
      } else if($('#kdlokasi').val()){
        $('#faktif').show();
        $('#faktif').bootstrapToggle({
          on: 'AKTIF',
          off: 'TIDAK AKTIF',
          onstyle: 'success',
          offstyle: 'danger'
        }); 
      }
      
      $('.btn-submit').click(function(){
        submit();
      });
      
      $('#jenis').change(function(){
        if($('#jenis').val()==='PAMERAN'){
          $('#status').val('E');
        } else {
          $('#status').val('I');
        }
      });
        //getKategori();
    }); 

    function submit(){
        var kdlokasi = $("#kdlokasi").val(); 
        var nmlokasi = $("#nmlokasi").val(); 
        var jenis = $("#jenis").val(); 
        var status = $("#status").val();  
        if ($("#faktif").prop("checked")){
          var faktif = 't';
        } else {
          var faktif = 'f';
        }
        // alert(faktif);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstlokasi/submit");?>",
      		data: {"kdlokasi":kdlokasi,"nmlokasi":nmlokasi,"jenis":jenis,"status":status,"faktif":faktif}, 
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
            if(obj.state==='1'){
              swal({
                title: 'Data '+nmlokasi+' Berhasil Disimpan',
                text: obj.msg,
                type: 'success'
              }, function(){
                window.location.href = '<?=site_url('mstlokasi');?>';
              }); 
            }else{ 
              swal({
                title: 'Data '+nmlokasi+' Gagal Disimpan',
                text: obj.msg,
                type: 'error'
              }); 
            }
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    } 

    function refresh(){
      window.location.reload();
    }
</script>

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of
 *
 * @author
 */

class Postdo_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }


    public function json_dgview() {
        error_reporting(-1);

        $aColumns = array('nodo',
                            'tgldo',
                            'noso',
                            'nama',
                            'nama_s',
                            'nmleasingx',
                            'nmprogleas',
                            'kddiv',
                        );
	    $sIndexColumn = "nodo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT nodo,tgldo,noso,nama,nama_s,nmleasingx,nmprogleas,progres,fposting,kddiv
                       FROM pzu.vl_jual
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                            ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
               $sOrder = "";
            }
        }
        $sWhere = "where progres>0 and fposting=0";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " and (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $row[] = $aRow[ $aColumns[$i] ];
            }
            $row[7] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs btn-proses \" onclick=\"posting('".$aRow['nodo']."');\">Posting</button>";
		    $output['aaData'][] = $row;
	    }
	    echo  json_encode( $output );
    }

    public function proses() {
        $kddiv = $this->session->userdata('kddiv');
        $nodo = $this->input->post('nodo');

        $q = $this->db->query("select title,msg,tipe from pzu.do_post('". $kddiv ."','". $nodo ."')");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    /*
    public function getData() {
        $this->db->select("nodo, to_char(tgldo,'DD-MM-YYYY') AS tgldo, nama"
                . " , alamat, kel, kec, kota, nohp, noso, to_char(tglso,'DD-MM-YYYY') AS tglso, kode"
                . " , kdtipe, nmtipe, nosin, nora, kdwarna, nmwarna, tahun");
        $nodo = $this->input->post('nodo');
        $this->db->where("nodo",$nodo);
        $q = $this->db->get("pzu.vl_jual");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function proses() {
        $nodo = $this->input->post('nodo');
        $q = $this->db->query("select code,title,msg,tipe from pzu.do_unpost('". $nodo ."')");
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function submit() {
        try {
            $array = $this->input->post();
            if(!empty($array['nodo']) && !empty($array['stat'])){
                $this->db->where('nodo', $array['nodo']);
                $this->db->set('tgl_aju_fa',NULL);
                $resl = $this->db->update('pzu.t_do');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }
    */
}

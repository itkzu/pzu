<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstaset_qry
 *
 * @author adi
 */
class Mstaset_qry extends CI_Model {

    //put your code here
    protected $res = "";
    protected $delete = "";
    protected $state = "";
    protected $kd_cabang = "";

    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getKlpAsetDep() {
        $kdjaset = $this->input->post('kdjaset');
        $this->db->select("aset_klp_dep.*");
        $this->db->where('aset_jenis.kdjaset', $kdjaset);
        $this->db->where('aset_klp_dep.faktif', 'true');
        $this->db->join('aset_jenis', 'aset_jenis.kdasetkat = aset_klp_dep.kdasetkat');
        $query = $this->db->get('aset_klp_dep');
        $res = $query->result_array();
        echo json_encode($res);
    }

    public function getLokasi() {
        $this->db->where('faktif', 'true');
        $this->db->order_by('nmlokasi','ASC');
        $query = $this->db->get('lokasi');
        return $query->result_array();
    }

    public function getKlpAset($kdklpaset = null) {
        if ($kdklpaset) {
            $this->db->where('kdklpaset', $kdklpaset);
        }
        $this->db->where('flgaktif', 't');
        $query = $this->db->get('aset_klp');
        return $query->result_array();
    }

    public function getJAset() {
        $this->db->where('faktif', 't');
        $this->db->order_by('nmjaset','ASC');
        $query = $this->db->get('aset_jenis');
        return $query->result_array();
    }

    public function select_data($param) {
        $this->db->select("kdaset, nmaset, umur, harga, to_char(tglbeli,'DD-MM-YYYY') AS tglbeli, 
                            dprno, kdjaset, kddiv, kdakun, ket_aset, ket_posisi, 
                            periode, kdasetga, kdlokaset, kdklpdep");
        $this->db->where('kdaset', $param);
        $query = $this->db->get('v_aset');
        return $query->result_array();
    }

    public function json_dgview() {
        error_reporting(-1);
        if (isset($_GET['kdaset'])) {
            $kdaset = $_GET['kdaset'];
        } else {
            $kdaset = '';
        }

        $aColumns = array(
            'no',
            'kdasetga',
            'nmaset',
            'tglbeli',
            'ket_aset',
            'ket_posisi',
            'kdaset',);
        $sIndexColumn = "kdaset";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() over(order by tglbeli,kdasetga) as no,
                        kdaset, nmaset, tglbeli, ket_aset, ket_posisi, kdasetga
                        FROM pzu.v_aset order by tglbeli
                    ) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );


        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $row[6] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"" . site_url('mstaset/edit/' . $aRow['kdaset']) . "\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
            $row[6] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('" . $aRow['kdaset'] . "');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }


//            $row[5] = "<a style=\"margin-right: 2px;\" class=\"btn btn-primary btn-xs \" href=\"".site_url('groups/edit/'.$aRow['kdgroups'])."\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Edit Data\"><i class='fa fa-pencil'></i></a>";
//            $row[5] .= "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdgroups']."');\" data-toggle=\"tooltip\" data-placement=\"auto\" title=\"Hapus Data\"><i class='fa fa-trash'></i></button>";

    public function submit() {
        try {
            $array = $this->input->post();
            $user = $this->session->userdata('username');
            if (empty($array['id'])) {
                $str = "SELECT pzu.aset_ins(
                            '" . strtoupper($array['nmaset']) . "',
                            '{$array['kdjaset']}',
                            '{$array['jumlah']}',
                            '{$array['harga']}',
                            '{$this->apps->dateConvert($array['tglbeli'])}',
                            '{$array['ket_aset']}',
                            '{$array['ket_posisi']}',
                            '" . strtoupper($array['kdlokaset']) . "',
                            '" . strtoupper($array['kdklpdep']) . "',
                            '{$user}'
                        );";
//                unset($array['kdaset']);
                $resl = $this->db->query($str);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->res = " Error : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }
            } elseif (!empty($array['id']) && empty($array['stat'])) {
                $str = "SELECT pzu.aset_trm_upd(
                            '" . strtoupper($array['nmaset']) . "',
                            '{$array['kdjaset']}',
                            '{$array['jumlah']}',
                            '{$array['harga']}',
                            '{$this->apps->dateConvert($array['tglbeli'])}',
                            '{$array['ket_aset']}',
                            '{$array['ket_posisi']}',
                            '" . strtoupper($array['kdlokaset']) . "',
                            '" . strtoupper($array['kdklpdep']) . "',
                            '{$user}'
                        );";
//                unset($array['kdaset']);
                $resl = $this->db->query($str);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->res = " Error : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }
            } elseif (!empty($array['id']) && !empty($array['stat'])) {
                $str = "SELECT pzu.aset_trm_del('" . $array['id'] . "');";
//                unset($array['kdaset']);
                $resl = $this->db->query($str);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->res = " Error : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }
            } else {
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
        );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mst_bklpart
 *
 * @author adi
 */
class Mst_bklpart extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mst_bklpart/submit'),
            'add' => site_url('mst_bklpart/add'),
            'edit' => site_url('mst_bklpart/edit'),
            'reload' => site_url('mst_bklpart'),
        );
        $this->load->model('mst_bklpart_qry');
        $kategori = $this->mst_bklpart_qry->getKategori();
        foreach ($kategori as $value) {
            $this->data['kdgrup'][$value['kdgrup']] = $value['kdgrup'];
        }

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mst_bklpart_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->mst_bklpart_qry->getKategori();
    }*/

    public function submit() {
        echo $this->mst_bklpart_qry->submit();
    }

    public function update() {
        echo $this->mst_bklpart_qry->update();
    }

    public function delete() {
        echo $this->mst_bklpart_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdpart'=> array(
                    // 'type'        => 'hidden',
                    'placeholder' => 'Kode Part',
                    'id'          => 'kdpart',
                    'name'        => 'kdpart',
                    'value'       => set_value('kdpart'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
            ),
           'nmpart'=> array(
                    'placeholder' => 'Nama Part',
                    'id'      => 'nmpart',
                    'name'        => 'nmpart',
                    'value'       => set_value('nmpart'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdgrup'=> array(
                    'attr'        => array(
                        'id'    => 'kdgrup',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdgrup'],
                    'value'    => set_value('kdgrup'),
                    'name'     => 'kdgrup',
                    'placeholder' => 'Pilih Kategori',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => '',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktif,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdpart = $this->uri->segment(3);
        }
        $this->_check_id($kdpart);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdpart'=> array(
                    // 'type'        => 'hidden',
                    'placeholder' => 'Kode Aksesoris',
                    'id'          => 'kdpart',
                    'name'        => 'kdpart',
                    'value'       => $this->val[0]['kdpart'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmpart'=> array(
                    'placeholder' => 'Nama Aksesoris',
                    'id'          => 'nmpart',
                    'name'        => 'nmpart',
                    'value'       => $this->val[0]['nmpart'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'kdgrup'=> array(
                    'attr'        => array(
                        'id'    => 'kdgrup',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdgrup'],
                    'value'       => $this->val[0]['kdgrup'],
                    'name'     => 'kdgrup',
                    'placeholder' => 'Pilih Kategori',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => 'Status Aksesoris',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktifx,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdpart){
        if(empty($kdpart)){
            redirect($this->data['add']);
        }

        $this->val= $this->mst_bklpart_qry->select_data($kdpart);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdpart,$stat,$nmpart,$faktif) {
        if(!empty($kdpart) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdpart',
                    'label' => 'Kode Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmpart',
                    'label' => 'Nama Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Imphso
 *
 * @author adi
 */
class Imphso extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('imphso/submit'),
            'add' => site_url('imphso/add'),
            'edit' => site_url('imphso/edit'),
            'reload' => site_url('imphso'),
        );
        $this->load->model('imphso_qry');

        $this->data['imp'] = array(
            "SL" => "SHIPPING LIST",
            "SPK" => "SPK"
        );

    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview_spk() {
        echo $this->imphso_qry->json_dgview_spk();
    }

    public function json_dgview_sl() {
        echo $this->imphso_qry->json_dgview_sl();
    }

    public function submit() {
        echo $this->imphso_qry->submit();
    }

    public function update() {
        echo $this->imphso_qry->update();
    }

    public function get_jmlspk() {
        echo $this->imphso_qry->get_jmlspk();
    }

    public function sl_tmp() {
        echo $this->imphso_qry->sl_tmp();
    }

    public function spk_tmp() {
        echo $this->imphso_qry->spk_tmp();
    }

    public function get_jmlsl() {
        echo $this->imphso_qry->get_jmlsl();
    }

    public function delete() {
        echo $this->imphso_qry->delete();
    }

    private function _init_add(){
        $this->data['form'] = array(
            'jml_sl'=> array(
                    'id'    => 'jml_sl',
                    'class' => 'form-control',
                    'value'    => set_value('jml_sl'),
                    'name'     => 'jml_sl',
                    // 'type'    => 'hidden'
            ),
            'jml_spk'=> array(
                    'id'    => 'jml_spk',
                    'class' => 'form-control',
                    'value'    => set_value('jml_spk'),
                    'name'     => 'jml_spk',
                    // 'type'    => 'hidden'
            ),
            'imp'=> array(
                    'attr'        => array(
                        'id'    => 'imp',
                        'class' => 'form-control',
                    ),
                    'data'     =>  $this->data['imp'],
                    'value'    => set_value('imp'),
                    'name'     => 'imp',
                    'placeholder' => 'Pilih Data yang akan di Import ',
                    'required' => ''
            ),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdaksgr'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Kelompok Aksesoris',
                    'id'          => 'kdaksgr',
                    'name'        => 'kdaksgr',
                    'value'       => $this->val[0]['kdaksgr'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmaksgr'=> array(
                    'placeholder' => 'Nama Kelompok Aksesoris',
                    'id'          => 'nmaksgr',
                    'name'        => 'nmaksgr',
                    'value'       => $this->val[0]['nmaksgr'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdakun'],
                    'value'       => $this->val[0]['kdakun'],
                    'name'     => 'kdakun',
                    'placeholder' => 'Pilih Akun',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => 'Status Kelompok Aksesoris',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktifx,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->imphso_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaksgr',
                    'label' => 'Kode Kelompok Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmaksgr',
                    'label' => 'Nama Kelompok Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}


      <?php

      //   function penyebut($nilai) {
      //   $nilai = abs($nilai);
      //   $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
      //   $temp = "";
      //   if ($nilai < 12) {
      //     $temp = " ". $huruf[$nilai];
      //   } else if ($nilai <20) {
      //     $temp = penyebut($nilai - 10). " Belas";
      //   } else if ($nilai < 100) {
      //     $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
      //   } else if ($nilai < 200) {
      //     $temp = " Seratus" . penyebut($nilai - 100);
      //   } else if ($nilai < 1000) {
      //     $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
      //   } else if ($nilai < 2000) {
      //     $temp = " Seribu" . penyebut($nilai - 1000);
      //   } else if ($nilai < 1000000) {
      //     $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
      //   } else if ($nilai < 1000000000) {
      //     $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
      //   } else if ($nilai < 1000000000000) {
      //     $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
      //   } else if ($nilai < 1000000000000000) {
      //     $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
      //   }
      //   return $temp;
      // }

      function bulan($tgl) {
        $tgl1 = $tgl.substr(0,2);
        $tgl2 = $tgl.substr(7,4);
        $tgldo = $tgl.substr(3,2);
        $temp = '';
        if ($tgldo = '01') {
          $temp = "Januari";
        } else if ($tgldo = '02') {
          $temp = "Februari";
        } else if ($tgldo = '03') {
          $temp = "Maret";
        } else if ($tgldo = '04') {
          $temp = "April";
        } else if ($tgldo = '05') {
          $temp = "Mei";
        } else if ($tgldo = '06') {
          $temp = "Juni";
        } else if ($tgldo = '07') {
          $temp = "Juli";
        } else if ($tgldo = '08') {
          $temp = "Agustus";
        } else if ($tgldo = '09') {
          $temp = "September";
        } else if ($tgldo = '10') {
          $temp = "Oktober";
        } else if ($tgldo = '11') {
          $temp = "November";
        } else if ($tgldo = '12') {
          $temp = "Desember";
        }
        return $temp;
      }
      ?>
      <style type="text/css">
        table { font-family: Tahoma; }
        body { font-family: Tahoma; }
        *{margin: 35px 25px 10px 30px; padding: 0}
      </style>
          <table border="0" style="width:750px;"  >
            <thead>
              <tr>
                <td style="font-size:16px;width:100%;text-align: center" colspan="24" height="24px"><b>SURAT KETERANGAN PENJUALAN KREDIT</b></td>
              </tr>
            </thead>
            <tbody>
              <tr> <td style="width:100%;text-align: center"  height="24px" colspan="24"></td> </tr>
              <tr> <td style="width:100%;text-align: center"  height="24px" colspan="24"></td> </tr>
              <tr style="font-size:14px;width:100%;text-align: left">
                  <td colspan="8" height="24px" align='left'><b>Data Customer :</b></td>
                  <td colspan="16" height="24px"></td>
              </tr>
              <tr> <td style="width:100%;text-align: center"  height="2px" colspan="24"></td> </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Nama Customer</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo $data[0]['nama'];?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Alamat Customer</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo $data[0]['alamat']. "&nbsp; KEL. ".$data[0]['kel']."&nbsp; KEC. ".$data[0]['kec']."&nbsp;-&nbsp;".$data[0]['kota'];?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr> <td style="width:100%;text-align: center"  height="24px" colspan="24"></td> </tr>
              <tr style="font-size:14px;width:100%;text-align: left">
                  <td colspan="8" height="24px" align='left' valign="top"><b>Data Penjualan :</b></td>
                  <td colspan="16" height="24px"></td>
              </tr>
              <tr> <td style="width:100%;text-align: center"  height="2px" colspan="24"></td> </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Tanggal Penjualan</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo substr($data[0]['tgl'],0,2)."&nbsp;".bulan($data[0]['tgl'])."&nbsp;".substr($data[0]['tgl'],6,4)?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Tipe Unit</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo $data[0]['nmtipe'];?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> No. Mesin / No. Rangka</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo $data[0]['nosin']."&nbsp;/&nbsp;".$data[0]['nora'];?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Harga OTR</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top">Rp. <?php echo number_format($data[0]['harga_otr'],2,",",".");?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Jangka Waktu Kredit</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo number_format($data[0]['tenor'],0,",",".")." bulan / Rp. ".number_format($data[0]['angsuran'],2,",",".");?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Nama Finance Company</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top"><?php echo $data[0]['nmleasing'];?></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Uang Muka Gross</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top">Rp.</td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Kontribusi Program (Diskon Asuransi & Administrasi)</td>
                 <td colspan="1" height="24px" valign="bottom">:</td>
                 <td colspan="12" height="24px" valign="bottom">Rp.</td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="5px" valign="top"></td>
                 <td colspan="1" height="5px" valign="bottom"></td>
                 <td colspan="12" height="5px" valign="top"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
                 <td colspan="2" height="24px"></td>
              </tr>
              <tr style="font-size:13px;width:100%;text-align: left">
                 <td colspan="2" height="24px"></td>
                 <td colspan="7" height="24px" valign="top"> Uang Muka Nett</td>
                 <td colspan="1" height="24px" valign="top">:</td>
                 <td colspan="12" height="24px" valign="top">Rp. </td>
                 <td colspan="3" height="24px"></td>
              </tr>
              <tr> <td style="width:100%;text-align: center"  height="24px" colspan="24"></td> </tr>
              <tr> <td style="width:100%;text-align: center"  height="24px" colspan="24"></td> </tr>
              <tr>
                 <td colspan="18" height="24px"</td>
                 <td colspan="6" height="24px" align="right">*No. DO : <?php echo $data[0]['nodo'];?></td>
              </tr>
              <tr>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
                <td colspan="1" height="23px" valign="top"></td>
              </tr>
          </tbody>
            <tfoot style="font-size:13px;width:100%;text-align: left">
              <tr>
                <td colspan="3" height="23px"></td>
                <td colspan="6" height="23px" align="center"></td>
                <td colspan="5" height="23px"></td>
                <td colspan="8" height="23px" align="center"><?php echo substr($this->session->userdata('data')['kota'],0,1)."".strtolower(substr($this->session->userdata('data')['kota'],1,10))." , ".substr($data[0]['wkt'],0,2)."&nbsp;".bulan($data[0]['wkt'])."&nbsp;".substr($data[0]['wkt'],6,4) ?></td>
                <td colspan="2" height="23px"></td>
              </tr>
              <tr>
                <td colspan="3" height="23px"></td>
                <td colspan="6" height="23px" align="center">Disetujui oleh,</td>
                <td colspan="5" height="23px"></td>
                <td colspan="8" height="23px" align="center">Dibuat oleh,</td>
                <td colspan="2" height="23px"></td>
              </tr>
              <tr> <td style="width:100%;text-align: center"  height="23px" colspan="24"></td> </tr>
              <tr> <td style="width:100%;text-align: center"  height="23px" colspan="24"></td> </tr>
              <tr> <td style="width:100%;text-align: center"  height="23px" colspan="24"></td> </tr>
              <tr> <td style="width:100%;text-align: center"  height="23px" colspan="24"></td> </tr>
              <tr>
                <td colspan="3" height="23px"></td>
                <td colspan="6" height="23px" align="center"><?php echo $data[0]['nama'];?></td>
                <td colspan="5" height="23px"></td>
                <td colspan="8" height="23px" align="center">PT. PRIMA ZIRANG UTAMA</td>
                <td colspan="2" height="23px"></td>
              </tr>
            </tfoot>
          </table>
      </div>

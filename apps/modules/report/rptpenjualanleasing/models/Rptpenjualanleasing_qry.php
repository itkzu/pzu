<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptpenjualanleasing_qry
 *
 * @author adi
 */
class Rptpenjualanleasing_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getPenjualanPerLeasing() {
        $periode_awal = $this->apps->dateConvert($this->input->post('periode_awal'));
        $periode_akhir = $this->apps->dateConvert($this->input->post('periode_akhir'));
        $header = $this->_header($periode_awal, $periode_akhir);
        $res = array(
            'header' => $header,
            'detail' => $this->_detail($periode_awal, $periode_akhir,$header),
            'footer' => $this->_footer($periode_awal, $periode_akhir,$header),
            'footer_leasing' => $this->_footer_leasing($periode_awal, $periode_akhir,$header),
        );
        return json_encode($res);
    }
    
    
    
    public function getArrayPenjualanPerLeasing() {
        $periode_awal = $this->apps->dateConvert($this->input->post('periode_awal'));
        $periode_akhir = $this->apps->dateConvert($this->input->post('periode_akhir'));
        $header = $this->_header($periode_awal, $periode_akhir);
        $res = array(
            'header' => $header,
            'detail' => $this->_detail($periode_awal, $periode_akhir,$header),
            'footer' => $this->_footer($periode_awal, $periode_akhir,$header),
            'footer_leasing' => $this->_footer_leasing($periode_awal, $periode_akhir,$header),
        );
        return $res;
    }
    
    private function _header($periode_awal, $periode_akhir){
        $str = "SELECT kdleasing,nmleasing FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' )
                        AND kdleasing<> ''
                    GROUP BY kdleasing,nmleasing
                    ORDER BY kdleasing;";
        $qry = $this->db->query($str);
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }

    private function _detail($periode_awal, $periode_akhir, $header) {
        $column = "";
        foreach ($header as $value) {
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 1 AND tenor <=17) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_1";
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 21 AND tenor <=29) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_2";
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 33 AND tenor <=39) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_3";
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 47) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_4";
        }
        
        $str = "SELECT nmtipe ,kdtipe,kode,SUM(CASE WHEN kdleasing IS NULL THEN 1 ELSE 0 END) AS T ".$column." 
                        FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' )
                    GROUP BY nmtipe ,kdtipe,kode
                    ORDER BY nmtipe";
        $qry = $this->db->query($str);
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }
    
    private function _footer($periode_awal, $periode_akhir, $header) {
        $column = "";
        foreach ($header as $value) {
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 1 AND tenor <=17) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_1";
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 21 AND tenor <=29) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_2";
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 33 AND tenor <=39) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_3";
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 47) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_4";
        }
        //$resColumn = substr($column, 1,  strlen($column));
        $str = "SELECT SUM(CASE WHEN kdleasing IS NULL THEN 1 ELSE 0 END) AS T ".$column." 
                        FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' );";
        $qry = $this->db->query($str);
        //// echo $this->db->last_query();
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }
    
    private function _footer_leasing($periode_awal, $periode_akhir, $header) {
        $column = "";
        foreach ($header as $value) {
            $column.=", SUM(CASE WHEN kdleasing = '".$value['kdleasing']."' "
                    . " AND (tenor >= 1) "
                    . " THEN 1 ELSE 0 END) AS ".$value['kdleasing']."_total";
        }
        //$resColumn = substr($column, 1,  strlen($column));
        $str = "SELECT SUM(CASE WHEN kdleasing IS NULL THEN 1 ELSE 0 END) AS T ".$column." 
                        FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' );";
        $qry = $this->db->query($str);
        //// echo $this->db->last_query();
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }
}

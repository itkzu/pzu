<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->
                <div class="box-header">
                    <button type="button" class="btn btn-primary btn-submit">
                       Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                       Batal
                    </button>
                </div>
                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-3">
                                            <div class="row">
                                                <div class="col-xs-5"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nokb']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-7"> 
                                                    <?php
                                                        echo form_input($form['nokb']);
                                                        echo form_error('nokb','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-4">
                                            <div class="row">
                                                <div class="col-xs-4"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nmkb']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['nmkb']);
                                                        echo form_error('nmkb','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['nilai']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['nilai']);
                                                        echo form_error('nilai','<div class="note">','</div>');
                                            ?>
                                        </div>     
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-3">
                                            <div class="row">
                                                <div class="col-xs-5"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tglkb']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-7"> 
                                                    <?php
                                                        echo form_input($form['tglkb']);
                                                        echo form_error('tglkb','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-4">
                                            <div class="row">
                                                <div class="col-xs-4"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nmleasing']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['nmleasing']);
                                                        echo form_error('nmleasing','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['tot_ar_jp']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['tot_ar_jp']);
                                                echo form_error('tot_ar_jp','<div class="note">','</div>');
                                            ?>
                                        </div>    
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-3"> 
                                        </div> 

                                        <div class="col-xs-4">
                                            <div class="row">
                                                <div class="col-xs-4"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['ket']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['ket']);
                                                        echo form_error('ket','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['selisih']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['selisih']);
                                                echo form_error('selisih','<div class="note">','</div>');
                                            ?>
                                        </div>       
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row"> 
                                        <div class="col-xs-5">  
                                        </div> 

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['kdleasing']);
                                                echo form_error('kdleasing','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['ket_selisih']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <?php
                                                echo form_input($form['ket_selisih']);
                                                echo form_error('ket_selisih','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label> Detail Pembayaran PPN JP </label>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12">
                                    <p><a id="add" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                                    <a id="del" class="btn btn-danger btn-del"><i class="fa fa-minus"></i> Hapus</a></p>
                                    <p id="alldata" class="kata"></p>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" class="display select">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">No. DO</th>
                                                    <th style="text-align: center;">Tgl DO</th>
                                                    <th style="text-align: center;">No. SPK</th> 
                                                    <th style="text-align: center;">Tgl SPK</th> 
                                                    <th style="text-align: center;">Atas Nama PO</th> 
                                                    <th style="text-align: center;">Nama Pemesan</th> 
                                                    <th style="text-align: center;">Nama STNK</th> 
                                                    <th style="text-align: center;">Alamat</th> 
                                                    <th style="text-align: center;">AR PPN JP</th> 
                                                </tr>
                                            </thead> 
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div> 
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
        </form>
        <!-- /.box -->
    </div>
</div> 

<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Data AR PPN JP Outstanding</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kunci']['placeholder']);  
                                    echo form_dropdown($form['kunci']['name']
                                                                        ,$form['kunci']['data']
                                                                        ,$form['kunci']['value']
                                                                        ,$form['kunci']['attr']);
                                    echo form_error('kunci','<div class="note">','</div>');
                                  ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['nodo']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-3"> 
                                <?php
                                    echo form_input($form['nodo']);
                                    echo form_error('nodo','<div class="note">','</div>');
                                ?>
                            </div> 

                            <div class="col-xs-1">  
                            </div> 

                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['noso']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-3"> 
                                <?php
                                    echo form_input($form['noso']);
                                    echo form_error('noso','<div class="note">','</div>');
                                ?>
                            </div> 
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row"> 
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['tgldo']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-3"> 
                                <?php
                                    echo form_input($form['tgldo']);
                                    echo form_error('tgldo','<div class="note">','</div>');
                                ?>
                            </div> 

                            <div class="col-xs-1">  
                            </div> 
                            
                            <div class="col-xs-2"> 
                                <div class="form-group">
                                    <?php echo form_label($form['tglso']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-3"> 
                                <?php
                                    echo form_input($form['tglso']);
                                    echo form_error('tglso','<div class="note">','</div>');
                                ?>
                            </div> 
                        </div>
                    </div> 

                    <div class="col-xs-12">
                        <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div> 

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['nama_p']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-9"> 
                                <?php
                                    echo form_input($form['nama_p']);
                                    echo form_error('nama_p','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['nama']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-9"> 
                                <?php
                                    echo form_input($form['nama']);
                                    echo form_error('nama','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['nama_s']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-9"> 
                                <?php
                                    echo form_input($form['nama_s']);
                                    echo form_error('nama_s','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['alamat']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-9"> 
                                <?php
                                    echo form_input($form['alamat']);
                                    echo form_error('alamat','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>   

                    <div class="col-xs-12">
                        <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div> 

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['jp']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-4"> 
                                <?php
                                    echo form_input($form['jp']);
                                    echo form_error('jp','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>   

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['jp_bayar']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-4"> 
                                <?php
                                    echo form_input($form['jp_bayar']);
                                    echo form_error('jp_bayar','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>   

                    <div class="col-xs-12">
                        <div class="row">
                            
                            <div class="col-xs-3"> 
                                <div class="form-group">
                                    <?php echo form_label($form['jp_saldo']['placeholder']); ?>
                                </div>
                            </div>

                            <div class="col-xs-4"> 
                                <?php
                                    echo form_input($form['jp_saldo']);
                                    echo form_error('jp_saldo','<div class="note">','</div>');
                                ?>
                            </div>  
                        </div>
                    </div>    

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div>
    <?php echo form_close(); ?>
</div>
 


<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>
<script type="text/javascript">
    $(document).ready(function () { 
        $(".dataTable tr").css('cursor', 'pointer');
        $("#tot_ar_jp").val('');
        $("#selisih").val('');
        // reset();
        autoNum();  
        sum();   

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdakun  : "required",
                kdaks : "required"
            },
            messages : {
                kdakun  : "Masukkan Kode Akun",
                kdaks : "Masukkan Nama Barang"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var nokb = $("#nokb").val();
        var nokb = nokb.toUpperCase();

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 9 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        column.push({
            "aTargets": [ 2,4 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });  

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nodo" },
                { "data": "tgldo" },
                { "data": "noso"},
                { "data": "tglso" },
                { "data": "nama_p"},
                { "data": "nama"},
                { "data": "nama_s"},
                { "data": "alamat"},
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nokb", "value": nokb});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('jpppn/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 9 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                var nilai = $('#nilai').autoNumeric('get'); 
                if(nilai===''){
                    $('#nilai').autoNumeric('set',0);
                    var nilai = 0;
                } 

                if(!isNaN(nilai)){ 
                    $('#tot_ar_jp').autoNumeric('set',total);
                    var selisih = parseInt(nilai) - parseInt(total);
                    if(!isNaN(selisih)){
                        $('#selisih').autoNumeric('set',selisih);
                    } else {
                        $('#selisih').autoNumeric('set',0);
                        var selisih = 0;
                    }
                } 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.dataTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }

            // $(this).removeClass('selected');
            // var data = table.row( this ).data();
            // $('.btn-del').click(function(){ 
            //     var datax = '';
            //     var datax = data['nodo'];
            //     delDetail(datax);
            // }); 
        });

        $('#kunci').change(function(){
            set_do();
        });

        $('.btn-del').click(function(){  
            var nodo = table.row('.selected').data()['nodo'];
            var nilai = table.row('.selected').data()['nilai'];
            // alert(nilai);
            delDetail(nodo,nilai);
        }); 


        $('.btn-add').click(function(){

        	  // getKodejpppn();
            validator.resetForm();
            init_key();
          	$('#modal_transaksi').modal('toggle'); 
        });

        $('.btn-simpan').click(function(){
            addDetail(); 
        }); 

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal(); 
        });

        $(".btn-submit").click(function(){ 
            var ket_selisih = $('#ket_selisih').val(); 
            if(ket_selisih===''){
                swal({
                    title: "Data tidak boleh kosong",
                    text: "",
                    type: "error"
                }, function(){
                    $('#ket_selisih').focus();
                    $('#ket_selisih').css("border", "2px solid red");
                });
            } else {
                $('#ket_selisih').blur();
                $('#ket_selisih').css("border", "1px solid gainsboro");
                submit(); 
            }  
        });
    });

    function autoNum(){
        $('#nilai').autoNumeric('init');
        $('#tot_ar_jp').autoNumeric('init');
        $('#selisih').autoNumeric('init'); 

        $('#jp').autoNumeric('init',{ currencySymbol : 'Rp.'}); 
        $('#jp_bayar').autoNumeric('init',{ currencySymbol : 'Rp.'}); 
        $('#jp_saldo').autoNumeric('init',{ currencySymbol : 'Rp.'});   
    } 

    function sum(){ 
        var nilai = $('#nilai').autoNumeric('get');
        var selisih = $('#selisih').autoNumeric('get'); 

        var tot_ar_jp = parseInt(nilai) - parseInt(selisih);
        if(!isNaN(tot_ar_jp)){
            $('#tot_ar_jp').autoNumeric('set',tot_ar_jp);
        } else {
            $('#tot_ar_jp').autoNumeric('set',0);
            var tot_ar_jp = 0;
        }   
    } 

    function clear(){
        $("#nourut").val('');
    } 

    function setBgColour(val,object){
        if(val){
            $("#"+object).css("background-color", "#fff");
        }else{
            $("#"+object).css("background-color", "#eee");
        }
    }

    function init_key(){
        var kdleasing = $("#kdleasing").val(); 
        $("#kunci").select2({
            ajax: {
                url: "<?=site_url('jpppn/getKey');?>", 
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                        return {
                            q: params.term, 
                            kdleasing: $("#kdleasing").val(),
                            page: params.page
                        }; 
                },
                // data: {"kdleasing":kdleasing},
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan Kata Kunci ...',
            dropdownAutoWidth : true,
            width: '100%',
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: format_nama, // omitted for brevity, see the source of this page
            templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
        });
    }

    function format_nama_terpilih (repo) {
        return repo.full_name || repo.text;
    }

    function format_nama (repo) {
        if (repo.loading) return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.id + "-" + repo.text + "</b></div>";

        if (repo.alamat) {
            markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.alamat + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nama_s + " | <i class=\"fa fa-tag\"></i> " + repo.nama_p   + " </div>" +
        "</div>" +
        "</div>";
        return markup;
    } 

    function set_do(){
        var kunci = $('#kunci').val();    
        $.ajax({
            type: "POST",
            url: "<?=site_url("jpppn/set_do");?>",
            data: {"nodo":kunci },
            success: function(resp){ 
                var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){
                        $('#nodo').val(data.nodo);
                        $('#noso').val(data.noso);
                        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                        $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                        $('#nama').val(data.nama);
                        $('#nama_s').val(data.nama_s);
                        $('#nama_p').val(data.nama_p);
                        $('#alamat').val(data.alamat);

                        //nominal
                        $('#jp').autoNumeric('set',data.nilai_ar);
                        $('#jp_bayar').autoNumeric('set',data.byr_ar);
                        $('#jp_saldo').autoNumeric('set',data.saldo_ar);
                    });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                // refresh();
            }
        });
    } 


    function addDetail(){

        var nokb = $("#nokb").val();
        var nokb = nokb.toUpperCase();  

        var nodo = $("#nodo").val();
        var jp = $("#jp").autoNumeric('get'); 
        // var jp = $("#harga").autoNumeric('get'); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("jpppn/addDetail");?>",
            data: {"nokb":nokb
                    ,"nodo":nodo 
                    ,"nilai":jp },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, data){
                    // alert(data.tipe);
                    refresh(); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    } 

    function delDetail(nodo,nilai){     
            // alert(nodo);
        swal({
            title: 'Konfirmasi untuk Hapus DO '+nodo+' di AR JP PPN',
            text: "Apakah Anda Yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Lanjutkan!',
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
        }, function (){ 
            var nokb = $('#nokb').val();
            $.ajax({
                type: "POST",
                url: "<?=site_url('jpppn/delDetail');?>",
                data: {"nokb":nokb
                      ,"nodo":nodo
                      ,"nilai":nilai
                },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                        table.ajax.reload(); 
                    });
                },
                error: function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        })
    }

    function deleted(nojpppn,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("jpppn/detaildeleted");?>",
                data: {"nojpppn":nojpppn ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){

        var nokb = $("#nokb").val(); 
        var nokb = nokb.toUpperCase();   
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            // window.location.href = '<?=site_url('jpppn');?>';

            $.ajax({
                type: "POST",
                url: "<?=site_url("jpppn/batal");?>",
                data: {"nokb":nokb },
                success: function(resp){ 
                    var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            window.location.href = '<?=site_url('jpppn');?>';
                            // refresh();
                        });
                    }); 
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    refresh();
                }
            });
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () { 
            var nokb = $("#nokb").val(); 
            var nokb = nokb.toUpperCase();   
            var ket_selisih = $("#ket_selisih").val(); 
            var ket_selisih = ket_selisih.toUpperCase(); 

            var selisih = $("#selisih").autoNumeric('get'); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("jpppn/submit");?>",
                data: {"nokb":nokb
                    ,"selisih":selisih
                    ,"ket_selisih":ket_selisih},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('jpppn');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : 
 * Email : 
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
						, 'class' => "form-inline");
					echo form_open($submit,$attributes);
				?>
				<div class="form-group">
					<?php
						echo form_label($form['periode_akhir']['placeholder']);
						echo form_input($form['periode_akhir']);
						echo form_error('periode_akhir','<div class="note">','</div>');
					?>
				</div>

				<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				<?php echo form_close(); ?>
				<hr>

				<div class="table-responsive">
					<table style="" class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="text-align: center;width: 5px;">No.</th>
								<th style="text-align: center;width: 50px;">No. Perkiraan</th>
								<th style="text-align: center;width: 100px;">Nama Perkiraan</th>
								<th colspan="2" style="text-align: center;">Saldo Awal</th>
								<th colspan="2" style="text-align: center;">Mutasi</th>
								<th colspan="2" style="text-align: center;">Saldo Akhir</th>
							</tr>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th style="text-align: center;width:">Debet</th>
								<th style="text-align: center;width:">Kredit</th>
								<th style="text-align: center;width:">Debet</th>
								<th style="text-align: center;width:">Kredit</th>
								<th style="text-align: center;width:">Debet</th>
								<th style="text-align: center;width:">Kredit</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {


		$('#periode_akhir').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$(".btn-tampil").click(function(){
			table.ajax.reload();
		});

		if ( $.fn.DataTable.isDataTable('.data-table') ) {
		  $('.data-table').DataTable().destroy();
		  $(".body-data").html('');
		}

		var periode = $("#periode_akhir").val();
		var column = [];

		column.push({
			"aTargets": [ 3,4,5,6,7,8 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0.00');
				// return formmatedvalue;
			  },
			  "sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"aaSorting": [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
							{ "name": "periode_akhir", "value": $("#periode_akhir").val() }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(0)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rpttb/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			// jumlah TOTAL
			'footerCallback': function ( row, data, start, end, display ) {
				var api = this.api(), data;

				  // converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// computing column Total of the complete result
				var sa_debet = api
					.column( 3 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var sa_kredit = api
					.column( 4 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var m_debet = api
					.column( 5 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				var m_kredit = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					  }, 0 );

				 var s_akhir_debet = api
					.column( 7 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				 var s_akhir_kredit = api
					.column( 8 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );

				// Update footer by showing the total with the reference of the column index
				$( api.column( 2 ).footer() ).html('Total');
				$( api.column( 3 ).footer() ).html(numeral(sa_debet).format('0,0.00'));
				$( api.column( 4 ).footer() ).html(numeral(sa_kredit).format('0,0.00'));
				$( api.column( 5 ).footer() ).html(numeral(m_debet).format('0,0.00'));
				$( api.column( 6 ).footer() ).html(numeral(m_kredit).format('0,0.00'));
				$( api.column( 7 ).footer() ).html(numeral(s_akhir_debet).format('0,0.00'));
				$( api.column( 8 ).footer() ).html(numeral(s_akhir_kredit).format('0,0.00'));
			},

			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print', footer: true, header: true,
					customize: function (win){
					   $(win.document.body).addClass('white-bg');
					   $(win.document.body).css('font-size', '10px');
					   $(win.document.body).find('table')
						   .addClass('compact')
						   .css('font-size', 'inherit');
				   }
				}
			],
			"sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {

		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
	});

</script>

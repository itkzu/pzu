<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Msttrk_hso_qry
 *
 * @author adi
 */
class Msttrk_hso_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    private function getdetail_uinb(){
        $output = array();
        $str = "SELECT nogoodsreceipt,
                        kodetipeunit,
                        kodewarna,
                        kuantitasterkirim,
                        kuantitasditerima,
                        nomesin,
                        norangka,
                        statusrfs,
                        poid,
                        kelengkapanunit,
                        docnrfsid
                         FROM api.vm_po_d GROUP BY nogoodsreceipt,
                        kodetipeunit,
                        kodewarna,
                        kuantitasterkirim,
                        kuantitasditerima,
                        nomesin,
                        norangka,
                        statusrfs,
                        poid,
                        kelengkapanunit,
                        docnrfsid";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
        }
        return $output;
    }

    private function getdetail_doch(){
        $output = array();
        $str = "SELECT nomorfakturstnk,
                        tanggalpengajuanstnkkebiro,
                        statusfakturstnk,
                        nomorstnk,
                        nomorrangka,
                        tanggalpenerimaanstnkdaribiro,
                        platnomor,
                        nomorbpkb ,
                        tanggalpenerimaanbpkbdaribiro ,
                        tanggalterimastnkolehkonsumen ,
                        tanggalterimabpkbolehkonsumen ,
                        namapenerimastnk     ,
                        jenisidpenerimastnk ,
                        noidpenerimastnk,
                        namapenerimabpkb ,
                        jenisidpenerimabpkb ,
                        noidpenerimabpkb
                         FROM api.vm_doch2_d";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
        }
        return $output;
    }

    private function getdetail_SV(){
        $output = array();
        $str = "SELECT idjob, jenis, namapekerjaan, jenispekerjaan, partsnumber, kuantitas, harga, diskon, dispersen, totalharga, noworkorder
                         FROM api.vm_pkb_d GROUP BY idjob, jenis, namapekerjaan, jenispekerjaan, partsnumber, kuantitas, harga, diskon, dispersen, totalharga, noworkorder";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
        }
        return $output;
    }

    private function getdetail_PO(){
        $output = array();
        $str = "SELECT nopenerimaan, nopo, jenisorder, idwarehouse, partsnumber, kuantitas
                         FROM api.vm_bpo_d GROUP BY nopenerimaan, nopo, jenisorder, idwarehouse, partsnumber, kuantitas";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
        }
        return $output;
    }

    private function getdetail_SO(){
        $output = array();
        $str = "SELECT noso, partsnumber, kuantitas, hargaparts, discamount, discpercentage 
                         FROM api.vm_bso_d GROUP BY noso, partsnumber, kuantitas, hargaparts, discamount, discpercentage ";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
        }
        return $output;
    }

    public function uinb_dgview() {
        error_reporting(-1);
        $aColumns = array('noshippinglist', 'tanggalterima', 'maindealerid', 'dealerid', 'noinvoice', 'statusshippinglist', 'nogoodsreceipt');
        $sIndexColumn = "nogoodsreceipt";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT noshippinglist
                              , tanggalterima
                              , maindealerid
                              , dealerid
                              , noinvoice
                              , statusshippinglist
                              , nogoodsreceipt
                              FROM api.vm_po group by noshippinglist
                              , tanggalterima
                              , maindealerid
                              , dealerid
                              , noinvoice
                              , statusshippinglist
                              , nogoodsreceipt ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by nogoodsreceipt ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_uinb();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['nogoodsreceipt']==$value['nogoodsreceipt']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function spk_dgview() {
        error_reporting(-1);
        $aColumns = array('idspk',
              'idprospect',
              'nmcustomer',
              'noktp',
              'alamat',
              'kodepropinsi',
              'kdkota',
              'kdkec',
              'kdlurah',
              'kdpos',
              'nokontak',
              'nmbpkb',
              'noktpbpkb',
              'lat',
              'longi',
              'npwp',
              'nokk',
              'fax',
              'email',
              'idsalespeople',
              'idevent',
              'tglpsn',
              'statusspk',
              'kdtipeunit',
              'anggotakk',
              'kodewarna',
              'qty',
              'hrgjual',
              'diskon',
              'ppnamount',
              'downpayment',
              'fakturpajak',
              'tppembayaran',
              'jmltandajadi',
              'tglpengiriman',
              'idsalesprog',
              'idappreal');
        $sIndexColumn = "kdtipeunit";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT idspk ,
                              idprospect ,
                              nmcustomer ,
                              noktp ,
                              alamat ,
                              kodepropinsi ,
                              kdkota ,
                              kdkec ,
                              kdlurah ,
                              kdpos ,
                              nokontak ,
                              nmbpkb ,
                              noktpbpkb ,
                              alamatbpkb ,
                              kdpropbpkb ,
                              kdkotabpkb ,
                              kdkecbpkb ,
                              kdlurahbpkb ,
                              kdposbpkb ,
                              lat ,
                              longi ,
                              npwp ,
                              nokk ,
                              alamatkk ,
                              kdpropkk ,
                              kdkotakk ,
                              kdkeckk ,
                              kdlurahkk ,
                              kdposkk ,
                              fax ,
                              email ,
                              idsalespeople ,
                              idevent ,
                              tglpsn ,
                              statusspk ,
                              createdtime ,
                              kdtipeunit ,
                              anggotakk , 
                              kodewarna ,
                              qty ,
                              hrgjual ,
                              diskon ,
                              ppnamount ,
                              downpayment ,
                              fakturpajak ,
                              tppembayaran ,
                              jmltandajadi ,
                              tglpengiriman ,
                              idsalesprog ,
                              idappreal from api.vm_so group by idspk ,
                              idprospect ,
                              nmcustomer ,
                              noktp ,
                              alamat ,
                              kodepropinsi ,
                              kdkota ,
                              kdkec ,
                              kdlurah ,
                              kdpos ,
                              nokontak ,
                              nmbpkb ,
                              noktpbpkb ,
                              alamatbpkb ,
                              kdpropbpkb ,
                              kdkotabpkb ,
                              kdkecbpkb ,
                              kdlurahbpkb ,
                              kdposbpkb ,
                              lat ,
                              longi ,
                              npwp ,
                              nokk ,
                              alamatkk ,
                              kdpropkk ,
                              kdkotakk ,
                              kdkeckk ,
                              kdlurahkk ,
                              kdposkk ,
                              fax ,
                              email ,
                              idsalespeople ,
                              idevent ,
                              tglpsn ,
                              statusspk ,
                              createdtime ,
                              kdtipeunit ,
                              anggotakk , 
                              kodewarna ,
                              qty ,
                              hrgjual ,
                              diskon ,
                              ppnamount ,
                              downpayment ,
                              fakturpajak ,
                              tppembayaran ,
                              jmltandajadi ,
                              tglpengiriman ,
                              idsalesprog ,
                              idappreal ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by kdtipeunit ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function doch_dgview() {
        error_reporting(-1);
        $aColumns = array('idso',
                          'idspk',
                          'nomorrangka');
        $sIndexColumn = "nomorrangka";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT idso,
                          idspk , 
                          nomorrangka  FROM api.vm_doch2) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by idspk ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_doch();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['nomorrangka']==$value['nomorrangka']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function sv_dgview() {
        error_reporting(-1);
        $aColumns = array('noworkorder', 'tanggalservis', 'nopolisi', 'nomesin', 'norangka', 'namapembawa', 'dealerid');
        $sIndexColumn = "noworkorder";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT noworkorder, tanggalservis, nopolisi, nomesin, norangka, namapembawa, dealerid
                              FROM api.vm_pkb group by noworkorder, tanggalservis, nopolisi, nomesin, norangka, namapembawa, dealerid ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noworkorder ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_SV();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['noworkorder']==$value['noworkorder']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function so_dgview() {
        error_reporting(-1);
        $aColumns = array('noso', 'tglso', 'idcustomer', 'namacustomer', 'discso', 'totalhargaso', 'dealerid');
        $sIndexColumn = "noso";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT noso, tglso, idcustomer, namacustomer, discso, totalhargaso, dealerid
                              FROM api.vm_bso group by noso, tglso, idcustomer, namacustomer, discso, totalhargaso, dealerid ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noso ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_SO();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['noso']==$value['noso']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function po_dgview() {
        error_reporting(-1);
        $aColumns = array('nopenerimaan', 'tglpenerimaan', 'noshippinglist');
        $sIndexColumn = "nopenerimaan";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT nopenerimaan, tglpenerimaan, noshippinglist
                              FROM api.vm_bpo group by nopenerimaan, tglpenerimaan, noshippinglist ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by nopenerimaan ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_PO();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['nopenerimaan']==$value['nopenerimaan']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function get_UINB() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);
        $curl = curl_init();

        if($this->session->userdata('data')['kddiv']==='ZPH01.01S'){
            $dealerId = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01S'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02S'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01S'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01S'){
            $dealerId = '15544';        
        }

            curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/uinb/read',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_SSL_VERIFYHOST => 0,
                  CURLOPT_SSL_VERIFYPEER => 0,
                  CURLOPT_POSTFIELDS =>'{
                  "fromTime": "'.$rdate7.'",
                  "toTime": "'.$rdate1.'",
                  "dealerId":"'.$dealerId.'",
                  "poId": "",
                  "noShippingList": ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=D9ADz8zIVfe0BM_dJbrdXiupv7k.zlH8PLq0jOwckmE-1680686556-0-AYBlBkYs8F5ppQwj5g5tGdQKqnxoj2SmSfv/a7+rYJxj4mdIe0+vmY277jbtz2ka/XHsPWpPvo568/ypDjRvZAM=; BIGipServerPool_AHASS_DC=1042294794.20480.0000'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;  
    }    

    public function get_SPK() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);
        $curl = curl_init();

        if($this->session->userdata('data')['kddiv']==='ZPH01.01S'){
            $dealerId = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01S'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02S'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01S'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01S'){
            $dealerId = '15544';        
        }

            curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/spk/read',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_SSL_VERIFYHOST => 0,
                  CURLOPT_SSL_VERIFYPEER => 0,
                  CURLOPT_POSTFIELDS =>'{
                  "fromTime": "'.$rdate7.'",
                  "toTime": "'.$rdate1.'",
                  "dealerId":"'.$dealerId.'",
                  "idProspect": "",
                  "idSalesPeople": ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=D9ADz8zIVfe0BM_dJbrdXiupv7k.zlH8PLq0jOwckmE-1680686556-0-AYBlBkYs8F5ppQwj5g5tGdQKqnxoj2SmSfv/a7+rYJxj4mdIe0+vmY277jbtz2ka/XHsPWpPvo568/ypDjRvZAM=; BIGipServerPool_AHASS_DC=1042294794.20480.0000'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;  
    } 

    public function get_DOCH() {  
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 
        $curl = curl_init();

        if($this->session->userdata('data')['kddiv']==='ZPH01.01S'){
            $dealerId = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $dealerId = '12603'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01S'){
            $dealerId = '13435'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02S'){
            $dealerId = '13528'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01S'){
            $dealerId = '14031'; 
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01S'){
            $dealerId = '15544';        
        }

            curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/doch/read',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_SSL_VERIFYHOST => 0,
                  CURLOPT_SSL_VERIFYPEER => 0,
                  CURLOPT_POSTFIELDS =>'{
                  "fromTime": "'.$rdate7.'",
                  "toTime": "'.$rdate1.'",
                  "dealerId":"'.$dealerId.'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: 1578283522',
                'DGI-Api-Key: dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE',
                'DGI-API-Token: b750cadd2f07874ce208953d56d2f4d11df4f7f9dbc165c627828a795b5c37e5',
                'Cookie: __cf_bm=D9ADz8zIVfe0BM_dJbrdXiupv7k.zlH8PLq0jOwckmE-1680686556-0-AYBlBkYs8F5ppQwj5g5tGdQKqnxoj2SmSfv/a7+rYJxj4mdIe0+vmY277jbtz2ka/XHsPWpPvo568/ypDjRvZAM=; BIGipServerPool_AHASS_DC=1042294794.20480.0000'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;  
    } 

    public function get_SV() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7');  

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }
            
        } else {
            $kddiv = $this->input->post('kddiv'); 
        }
        if ($this->input->post('nopkb')===''){
            $nopkb = '';
        } else {
            $nopkb = $this->input->post('nopkb'); 
        }

        $hash = hash('sha256', $api_key.$secret_key.$req_time);
        $curl = curl_init();

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/pkb/read',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "fromTime": "'.$rdate7.'",
                    "toTime": "'.$rdate1.'",
                    "dealerId":"'.$dealerId.'", 
                    "noWorkOrder": "'.$nopkb.'"

                    // "fromTime": "2024-05-01 00:00:00",
                    // "toTime": "2024-05-17 23:00:00",
                    // "dealerId":"13435", 
                    // "noWorkOrder": "13435-PKB-2400821"                    

                    // "noWorkOrder": ""

                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'X-Request-Time: '.$req_time,
                    'DGI-Api-Key: '.$api_key,
                    'DGI-API-Token: '.$hash,
                    // 'Cookie: __cf_bm=gpjtKmTr6KDwBLAxf4lOu6ruvEo9aK3EQbQyM0ZFsX0-1698372601-0-AU4+WCj7eMLQpfxVdkdaJplsM1xMmpaSE9nD7ArDKjhj+flnf7sQ/c7SV1p1DX/ODzqq4MR8jtkL6zdrb0C39Ko='
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;  
            // echo $rdate7 . $rdate1 . $dealerId . $nopkb . $req_time . $api_key . $hash ;  jangan di hidupin
    }   

    public function get_SV_zpx() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7');  

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }
            
        } else {
            $kddiv = $this->input->post('kddiv'); 
        }
        if ($this->input->post('nopkb')===''){
            $nopkb = '';
        } else {
            $nopkb = $this->input->post('nopkb'); 
        }

        $hash = hash('sha256', $api_key.$secret_key.$req_time);
        $curl = curl_init();

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/pkb/read',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POSTFIELDS =>'{
                    "fromTime": "'.$rdate7.'",
                    "toTime": "'.$rdate1.'",
                    "dealerId":"'.$dealerId.'", 
                    "noWorkOrder": "" 
                    
                }',
				// "noWorkOrder": "'.$nopkb.'"
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'X-Request-Time: '.$req_time,
                    'DGI-Api-Key: '.$api_key,
                    'DGI-API-Token: '.$hash,
                    // 'Cookie: __cf_bm=gpjtKmTr6KDwBLAxf4lOu6ruvEo9aK3EQbQyM0ZFsX0-1698372601-0-AU4+WCj7eMLQpfxVdkdaJplsM1xMmpaSE9nD7ArDKjhj+flnf7sQ/c7SV1p1DX/ODzqq4MR8jtkL6zdrb0C39Ko='
                ),
            )); 
            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;  
            // echo $rdate7 . $rdate1 . $dealerId . $nopkb . $req_time . $api_key . $hash . $secret_key ;  
    }      
    public function ins_UINB() {    
        $info    = $this->input->post('info'); 
        $q = $this->db->query("insert into api.po_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        // echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function ins_SPK() {    
        $info   = $this->input->post('info'); 
        $info   = str_replace("'","''",$info);
        $q = $this->db->query("insert into api.so_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        // echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function ins_DOCH() {    
        $info    = $this->input->post('info'); 
        $q = $this->db->query("insert into api.doch2_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        // echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";  

        return json_encode($res);
    }

    public function ins_SV() {    
        $info    = $this->input->post('info'); 
        // $info   = str_replace("'","''",$info);
        $info   = str_replace("'","''",$info);                
        $q = $this->db->query("insert into api.pkb_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function save_UINB() { 
        $q = $this->db->query("select title,msg,tipe from api.in_po()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }   

    public function save_SPK() { 
        $q = $this->db->query("select title,msg,tipe from api.in_so()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function save_DOCH() { 
        $q = $this->db->query("select title,msg,tipe from api.in_doch2()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function save_SV() { 
        $q = $this->db->query("select title,msg,tipe from api.in_pkb()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }   

    public function upd_DOCH() { 
        $status    = $this->input->post('status'); 
        $q = $this->db->query("select title,msg,tipe from api.upd_doch2('".$status."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function save_DOCH_trm() { 
        $q = $this->db->query("select title,msg,tipe from api.in_doch2_trm()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    //import
    public function get_PO() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }  
        }

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/pinb/read',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "fromTime": "'.$rdate7.'",
                "toTime": "'.$rdate1.'",
                "dealerId":"'.$dealerId.'",
                "noPO" : ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=yibWElurH59qWQNssvTYCZzwSaxRMYs1kH_WT_v3QYY-1698196857-0-AU80JsC+DN7RnicjIxWqPepjs7Lkv1J9B8YzWY+FfzshqpoddUkEKQzhDQMaHaQlglJayvb8Z02btLqh1JKyNw8='
            ),
        ));   

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response; 
    } 

    public function ins_PO() {    
        $info   = $this->input->post('info'); 
        $info   = str_replace("'","''",$info);
        $q = $this->db->query("insert into api.bpo_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        // echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function save_PO() { 
        $q = $this->db->query("select title,msg,tipe from api.in_bpo()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    //import
    public function get_SO() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }  
        }

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/prsl/read',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "fromTime": "'.$rdate7.'",
                "toTime": "'.$rdate1.'",
                "dealerId":"'.$dealerId.'",
                "noSO" : ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=Xi.HNu3sqsLkerdNZU.H9qZHR67P.35BMakyEhSO7Kk-1698760488-0-Acd1habkwOKW47ghkKJyFhp6Ighd9wvt/tBKjfI5EgKNB5pVqcnrcqIe2uh/UNnu+2NT6IXHAaYMHMGc6Q8s+2E='
            ),
        ));   

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response; 
    } 

    public function ins_SO() {    
        $info   = $this->input->post('info'); 
        $info   = str_replace("'","''",$info);
        $q = $this->db->query("insert into api.bso_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function save_SO() { 
        $q = $this->db->query("select title,msg,tipe from api.in_bso()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Tranvalsl
 *
 * @author adi
 */
class Tranvalsl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('tranvalsl/submit'),
            'add' => site_url('tranvalsl/add'),
            'edit' => site_url('tranvalsl/edit'),
            'reload' => site_url('tranvalsl'),
        );
        $this->load->model('tranvalsl_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->tranvalsl_qry->json_dgview();
    }
    
    public function getDetail() {
        echo $this->tranvalsl_qry->getDetail();
    }
    
    public function submit() {
        if($this->_validate() == TRUE){
            $res = $this->tranvalsl_qry->submit();
        }else{
            $res = array(
                'state' => 0,
                'msg' => validation_errors(),
            );
        }
        echo json_encode($res);
    }
    
    private function _validate() {
        $config = array(
            array(
                    'field' => 'nopo',
                    'label' => 'Nomor PO',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tglterima',
                    'label' => 'Tgl Terima',
                    'rules' => 'required',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

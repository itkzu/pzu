<style type="text/css">
  .{
    margin: -10em 0 0 -10em;
  }
  table th, table td {
    margin: 10000px 0 0 10000px;
/*    border:1px solid #000;  */
    padding:0;
    height: 12px;
    font-size: 12px;
    font-family: 'Nunito', sans-serif;
/*    font-weight: bold;*/
  }
</style>
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

      <?php

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      }

      function baris($jml) {
        $jml = abs($jml);
        $hasil = "";
        if($jml==0){
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==1) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==2) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==3) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } elseif ($jml==4) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==5) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==6) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==7) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==8) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr></tr>";
        }
        return $hasil;
      }

      ?>
          <table border="0" style="width:100%; height:400px; margin: 0 0 0 0;">
            <tbody>
              <!-- <tr>
                <td style="width:2%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:2%;text-align: center;"> - </td>
              </tr> -->
              <tr style="height:24px">
                <td style="width:50%" colspan="7"></td>
                <td style="width:50%;text-align: right;font-size:24px;" colspan="7" rowspan="2"> FAKTUR SUKU CADANG </td>
              </tr> 
              <tr>
                <td style="width:50%" colspan="7">PRIMA ZIRANG</td> 
              </tr> 
              <tr>
                <td style="width:50%" colspan="7"> <?php echo $data[0]['companyname'];?> </td> 
                <td style="width:8%;text-align: center;"></td> 
                <td style="width:8%;text-align: left;font-size: 18px; font-weight: bold;" rowspan="2"> Nomor </td>
                <td style="width:36%;text-align: left;font-size: 18px; font-weight: bold;" colspan="5" rowspan="2"> : <?php echo $data[0]['noref'];?></td>
              </tr>     
              <tr>
                <td style="width:50%" colspan="7"> <?php echo $data[0]['alamatperusahaan'];?> </td> 
              </tr>
              <tr>
                <td style="width:50%" colspan="7"> Telp. <?php echo $data[0]['notelperusahaan'];?>   Fax. <?php echo $data[0]['nofaxperusahaan'];?> </td> 
                <td style="width:8%;text-align: center;"></td> 
                <td style="width:8%;text-align: left;font-size: 18px;" rowspan="2"> Tanggal </td>
                <td style="width:36%;text-align: left;font-size: 18px;" colspan="5" rowspan="2"> : <?php echo date_format(date_create($data[0]['tanggal']),"d-m-Y"); ?></td>
              </tr>     
              <tr>
                <td style="width:50%" colspan="7"></td> 
              </tr>
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> Kode Customer </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['idcustomer'];?></td> 
                <td style="width:50%" colspan="7"></td> 
              </tr>  
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> Nama Customer </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['nama_pelanggan'];?></td> 
                <td style="width:50%" colspan="7"></td> 
              </tr>  
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> Alamat Customer </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['alamat_pelanggan'];?></td> 
                <td style="width:50%" colspan="7"></td> 
              </tr>  
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> KTP / NPWP Customer </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['nik_pelanggan'];?></td> 
                <td style="width:50%" colspan="7"></td> 
              </tr>
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr> 
              <tr>
                <td style="width:2%;text-align: center;"> * </td>
                <td style="width:24%;text-align: left;" colspan="3"> KODE SUKU CADANG </td>
                <td style="width:24%;text-align: left;" colspan="3"> NAMA SUKU CADANG </td>
                <td style="width:8%;text-align: right;" colspan="1"> HARGA </td>
                <td style="width:8%;text-align: right;" colspan="1"> SATUAN  </td>
                <td style="width:16%;text-align: right;" colspan="2"> POTONGAN </td> 
                <td style="width:16%;text-align: right;" colspan="2"> HARGA BERSIH  </td> 
                <td style="width:2%;text-align: center;"></td> 
              </tr> 
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr> 
              <?php
                if ($data[0]['noso']==='DEFAULT') {
                  $noso = "";
                } else {
                  $noso = $data[0]['noso'];
                }
              ?>
              <tr>
                <td style="width:2%;text-align: center;"> </td>
                <td style="width:16%;text-align: left;" colspan="2"> Sales Order </td>
                <td style="width:32%;text-align: left;" colspan="4"> : <?php echo $noso;?> </td>
                <td style="width:16%;text-align: left;" colspan="2"> PO/DO ATPM </td>
                <td style="width:32%;text-align: left;" colspan="4"> : </td>
                <td style="width:2%;text-align: center;"></td> 
              </tr>
              <tr>
                <td style="width:2%;text-align: center;"> </td>
                <td style="width:16%;text-align: left;" colspan="2"> PO Pelanggan </td>
                <td style="width:32%;text-align: left;" colspan="4"> : <?php echo $data[0]['bookingidreference'];?> </td>
                <td style="width:16%;text-align: left;" colspan="2"> Tipe Order </td>
                <td style="width:32%;text-align: left;" colspan="4"> : </td>
                <td style="width:2%;text-align: center;"></td> 
              </tr>
              <?php
              $no = 1;  
              $sum = [];
              $disc = [];
              foreach ($data as $value) {

                array_push($sum, $value['total']); 
                array_push($disc, $value['discount']); 
                $hasil = '<tr>
                            <td style="width:2%;text-align: center;"> * </td>
                            <td style="width:24%;text-align: left;" colspan="3"> '.$value["kodepart"].' </td>
                            <td style="width:24%;text-align: left;" colspan="3"> '.$value["kodepart"].' </td>
                            <td style="width:8%;text-align: right;" colspan="1"> '.number_format($value["harga"],2,",",".").' </td>
                            <td style="width:8%;text-align: right;" colspan="1"> '.$value["satuan"].' </td>
                            <td style="width:16%;text-align: right;" colspan="2"> '.number_format($value["discount"],2,",",".").' </td> 
                            <td style="width:16%;text-align: right;" colspan="2"> '.number_format($value["total"],2,",",".").' </td> 
                            <td style="width:2%;text-align: center;"></td> 
                          </tr>'; 
                echo $hasil;
                $no++;  
              } 
              $total = array_sum($sum);
              $totdisc = array_sum($disc);
              ?>
              <tr> 
                <td style="width:26%;text-align: center;" colspan="3"> Sub Total </td>
                <td style="width:48%;text-align: center;" colspan="6"> </td> 
                <td style="width:32%;text-align: right;" colspan="4"> <?php echo number_format($total,2,",",".") ?>  </td> 
                <td style="width:2%;text-align: center;"> </td> 
              </tr>
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px dashed black; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr> 
              <?php
                $jml = $total ;
                $dpp = $total / 1.11;
                $ppn = $total - $dpp;
              ?>
              <tr> 
                <td style="width:26%;text-align: center;" colspan="3"> Total (Termasuk PPN) </td>
                <td style="width:48%;text-align: center;" colspan="6"> </td> 
                <td style="width:32%;text-align: right;" colspan="4"> <?php echo number_format($jml,2,",",".") ?>  </td> 
                <td style="width:2%;text-align: center;"> </td> 
              </tr>
              <tr>
                <td style="width:2%;text-align: center;"> </td>
                <td style="width:16%;text-align: left;" colspan="2"> DPP </td>
                <td style="width:8%;text-align: right;" > :</td>
                <td style="width:64%;text-align: right;" colspan="8"> </td>
                <td style="width:16%;text-align: right;" ><?php echo number_format($dpp,2,",",".") ?>  </td>
                <td style="width:2%;text-align: center;" > </td>
              </tr>
              <tr>
                <td style="width:2%;text-align: center;"> </td>
                <td style="width:16%;text-align: left;" colspan="2"> PPN</td>
                <td style="width:8%;text-align: right;" > :</td>
                <td style="width:64%;text-align: right;" colspan="8"> </td>
                <td style="width:16%;text-align: right;" ><?php echo number_format($ppn,2,",",".") ?>  </td>
                <td style="width:2%;text-align: center;" > </td>
              </tr>
              <tr>
                <td style="width:2%;text-align: center;"> </td>
                <td style="width:16%;text-align: left;" colspan="2"> TOTAL</td>
                <td style="width:8%;text-align: right;" > :</td>
                <td style="width:56%;text-align: right;" colspan="7"> </td>
                <td style="width:24%;text-align: right;" colspan="2">IDR <?php echo number_format($dpp + $ppn,2,",",".") ?>  </td>
                <td style="width:2%;text-align: center;" > </td>
              </tr>
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr>  
              <tr>
                <td style="width:2%;text-align: center;" >  </td>
                <td style="width:64%;text-align: left; font-weight: bold;" colspan="8"> K e t e r a n g a n </td>
                <td style="width:32%;text-align: center;" colspan="4"> Kasir </td>
                <td style="width:2%;text-align: center;"> </td>
              </tr>
              <tr>
                <td style="width:2%;text-align: center;" >  </td>
                <td style="width:96%;text-align: left;" colspan="12"> 1. Faktur ini berlaku sebagai tanda terima pembayaran</td>
                <td style="width:2%;text-align: center;" >  </td>
              </tr>
              <tr>
                <td style="width:2%;text-align: center;" >  </td>
                <td style="width:96%;text-align: left;" colspan="12"> 2. Pembayaran tunai sah apabila Faktur sudah dicap <b>LUNAS</td>
                <td style="width:2%;text-align: center;" >  </td>
              </tr>
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"></td>
              </tr>
              <tr>
                <td style="width:66%;text-align: center;" colspan="9">  </td>
                <td style="width:32%;text-align: center;" colspan="4"> ( <?php echo $data[0]['nmkasir'];?> ) </td>
                <td style="width:2%;text-align: center;"> </td>
              </tr>
              <tr>
                <td style="width:66%;text-align: center;" colspan="9">  </td>
                <td style="width:32%;text-align: center;" colspan="4"> <hr style="border:1.4px solid black; padding: 0; margin: 0 0 0 0 ;"></td>
                <td style="width:2%;text-align: center;"> </td>
              </tr> 
             </tbody> 
            <tfoot>
            </tfoot>
          </table>
      </div>

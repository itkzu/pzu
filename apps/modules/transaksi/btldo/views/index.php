<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <?php 
                                echo form_label($form['nodo']['placeholder']);
                            ?>

                            <div class="input-group">
                                <?php 
                                    echo form_input($form['nodo']);
                                    echo form_error('nodo','<div class="note">','</div>'); 
                                ?>

                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-tampil" id="btn-tampil">Tampil</button>
                                    <button type="button" class="btn btn-success btn-reset">Reset</button> 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group keterangan">
                            <?php 
                                    echo form_label($form['ket_batal']['placeholder']);
                            ?>

                            <div class="input-group">
                                <?php 
                                    echo form_input($form['ket_batal']);
                                    echo form_error('ket_batal','<div class="note">','</div>'); 
                                ?> 

                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-danger btn-batal_do">Batal</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- detail transaksi via ajax -->
                <div id="detail"></div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //$("#nodo").mask("a99-999999");

        //$("#detail").hide();

        $(".btn-batal_do").hide();
        $(".btn-reset").hide();
        $(".keterangan").hide();

        $("#nodo").focus();

        $(".btn-tampil").click(function(){
            
            getData();
            //$(".btn-tampil").attr("disabled",true);
        });

        $(".btn-reset").click(function(){
            reset();
        });

        $(".btn-batal_do").click(function(){
            batal_do();
        });
        
        /*
        $(".btn-batal").click(function(){
            reset();
        });
        $(".btn-validasi").click(function(){
            batalFaktur();
        });
        */



        var input = document.getElementById("nodo");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("btn-tampil").click();
            }
        }); 

        
    });

    function buttonState(){
        $(".btn-tampil").toggle();  
        $(".btn-batal_do").toggle();  
        $(".btn-reset").toggle();  
        $(".keterangan").show();
    }

    function reset(){
        //$(".form-control").val("");
        buttonState();
        $("#detail").html('');

        //$("#nodo").val('');
        $("#nodo").prop("readonly", false);
        $("#nodo").focus(); $("#nodo").select();
        $(".keterangan").hide();
        $("#ket_batal").val('');
    }
    
    function getData(){ 
        var nodo = $("#nodo").val();
        $.ajax({
            type: "POST",

            //dataType: "html",

            url: "<?=site_url('btldo/getData');?>",
            data: {"nodo":nodo},
            success: function(resp){  
                if(resp){

                    var obj = JSON.parse(resp);
                    //alert(JSON.stringify(resp));

                    if (jQuery.isEmptyObject(obj)){
                        //alert('empty');
                        swal({
                            title: "Error",
                            text: "Data No. DO / DO Intern : " + nodo + " tidak ditemukan! ",
                            type: "error"
                        }, function(){
                            
                            $("#nodo").val('');
                            $("#nodo").focus();
                        });
                    } else{
                        $("#nodo").prop("readonly", true);
                        var detailhtml = "";
                        $.each(obj, function(key, data){
                            detailhtml = '<hr>'+
                                            '<div class="row">'+
                                            '    <div class="col-lg-6">'+
                                            '        <div class="form-group">'+
                                            '            <label>Tanggal DO</label><input type="text" name="tgldo" value="'+data.tgldo+'" placeholder="" id="tgldo" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>No. SPK</label><input type="text" name="kdtipe" value="'+data.noso+'" placeholder="" id="kdtipe" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Tanggal SPK</label><input type="text" name="kdtipe" value="'+data.tglso+'" placeholder="" id="kdtipe" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Nama Konsumen</label><input type="text" name="nama" value="'+data.nama+'" placeholder="" id="nama" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Alamat Konsumen</label><textarea name="alamat" placeholder="" id="alamat" class="form-control" required="" readonly="" style="min-height: 34px;height: 108px; resize: vertical;" >'+data.alamat+'&#013KEL. '+data.kel+'&#013KEC. '+data.kec+'&#013'+data.kota+'</textarea>'+
                                            '        </div>'+
                                            '    </div>'+

                                            '    <div class="col-lg-6">'+
                                            '        <div class="form-group">'+
                                            '            <label>Kode Unit</label><input type="text" name="kdtipe" value="'+data.kode+' - '+data.kdtipe+'" placeholder="" id="kdtipe" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Tipe Unit</label><input type="text" name="nmtipe" value="'+data.nmtipe+'" placeholder="" id="nmtipe" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Warna</label><input type="text" name="nmtipe" value="'+data.kdwarna+' - '+data.nmwarna+'" placeholder="" id="nmtipe" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>Tahun</label><input type="text" name="nmtipe" value="'+data.tahun+'" placeholder="" id="nmtipe" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>No. Mesin</label><input type="text" name="nosin" value="'+data.nosin+'" placeholder="" id="nosin" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '        <div class="form-group">'+
                                            '            <label>No. Rangka</label><input type="text" name="nora" value="'+data.nora+'" placeholder="" id="nora" class="form-control" required="" readonly=""  />'+
                                            '        </div>'+
                                            '    </div>'+

                                            '</div>'+
                                            ' ';
                        });

                        $("#detail").html(detailhtml);

                        buttonState();
                        //$(".btn-tampil").hide();
                        //$(".btn-batal_do").show();
                        //$(".btn-reset").show();
                    }

                /*
                    $.each(obj, function(key, data){
                        $("#iddo").val(data.nodo);
                        $("#nama").val(data.nama);
                        $("#tgldo").val(data.tgldo);
                        $("#alamat").val(data.alamat);
                        $("#kel").val(data.kel);
                        $("#kec").val(data.kec);
                        $("#kota").val(data.kota);
                        $("#nohp").val(data.nohp);
                        $("#kdtipe").val(data.kdtipe);
                        $("#nmtipe").val(data.nmtipe);
                        $("#nosin").val(data.nosin);
                        $("#nora").val(data.nora);
                        tgl_trm_fa = data.tgl_trm_fa;
                        //nodo, tgldo, nama, alamat, kel, kec, kota, nohp, kdtipe, nmtipe, nosin, nora
                    }
                    if(tgl_trm_fa===null){
                        $(".btn-validasi").attr("disabled",false);
                    }else if(tgl_trm_fa==="" && obj===""){
                        swal({
                            title: "Informasi",
                            text: "Data No. DO : " + nodo + " Tidak Ditemukan, Mungkin Nomor Pengajuan Faktur Astra Belum Di Input",
                            type: "info"
                        }, function(){
                            
                        });
                    }else if(tgl_trm_fa!==null && tgl_trm_fa!=="" ){
                        swal({
                            title: "Informasi",
                            text: "Data No. DO : " + nodo + " Tidak Bisa Batal Faktur, Karena Sudah Pernah Terima Faktur Tanggal " + tgl_trm_fa,
                            type: "info"
                        }, function(){
                            
                        });
                    }else{
                        swal({
                            title: "Informasi",
                            text: "Data No. DO : " + nodo + " Tidak Ditemukan, Alasan Tidak Diketahui",
                            type: "info"
                        }, function(){
                            
                        });
                    }
                */                    
                }else{
                    swal("Error", obj.msg, "error");
                }
            },
            error: function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });        
    }
    
    function batal_do(){
        swal({
            title: "Konfirmasi",
            text: "Proses Batal Transaksi Penjualan akan dilakukan, data tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            }, 
            
            function () {
                var nodo = $("#nodo").val();
                var ket_batal = $("#ket_batal").val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('btldo/proses');?>",
                    data: {"nodo":nodo,"ket_batal":ket_batal},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                $("#nodo").val('');
                                $("#ket_batal").val('');
                                reset();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }

    /*
    function batalFaktur(){
        swal({
            title: "Konfirmasi Pembatalan Faktur !",
            text: "Faktur Astra akan dibatalkan, data tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                var submit = "<?php echo $submit;?>"; 
                var nodo = $("#iddo").val();
                $.ajax({
                    type: "POST",
                    url: submit,
                    data: {"nodo":nodo,"stat":"cancel"},
                    success: function(resp){   
                        var obj = jQuery.parseJSON(resp);
                        if(obj.state==="1"){
                            swal({
                                title: "Pembatalan Berhasil",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                reset();
                            });
                        }else{
                            swal("Pembatalan Gagal", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
    */
</script>
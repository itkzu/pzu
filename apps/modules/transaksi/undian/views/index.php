<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : 
 * Email : 
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
	td.details-control {
		background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
	}
	#load{
		width: 100%;
		height: 100%;
		position: fixed;
		text-indent: 100%;
		background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
		z-index: 1;
		opacity: 0.6;
		background-size: 10%;
	}
	#spinner-div {
		position: fixed;
		display: none;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		text-align: center;
		background-color: rgba(255, 255, 255, 0.8);
		z-index: 2;
	}
</style>

<div id="load">Loading...</div> 
<div class="row">
	<!-- <div id="load" class="pt-5">
		<div class="spinner-border text-primary" role="status"></div>
	</div> -->
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-header"> 
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-wrench"></i></button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo $add;?>" >Tambah Data Baru</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="btn-refersh">Refresh</a>
							</li>
						</ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="col-xs-4">
					<div class="table-responsive">
						<table class="table table-bordered table-hover js-basic-example dataTable display nowrap tbl1">
						<thead>
							<tr>  
								<th style="text-align: center;">No.</th> 
								<th style="text-align: center;">No Polisi</th> 
								<th style="text-align: center;">Nama</th>  
							</tr>
						</thead>
						<tfoot> 
						</tfoot>
						<tbody></tbody>
					</table>
					</div>
				</div>
				<div class="col-xs-1">  
						<a class="btn btn-draw btn-primary">Draw</a> 
				</div>
				<div class="col-xs-7">
					<div class="table-responsive">
						<table class="table table-bordered table-hover js-basic-example dataTable display nowrap tbl2">
						<thead>
							<tr>   
								<th style="text-align: center;">No Polisi</th> 
								<th style="text-align: center;">Nama</th> 
								<th style="text-align: center;">Alamat</th> 
								<th style="text-align: center;">No. HP</th> 
								<th style="text-align: center;">Hapus</th> 
							</tr>
						</thead>
						<tfoot> 
						</tfoot>
						<tbody></tbody>
					</table>
					<div id="loading">
						<!-- <i class="fa fa-spinner fa-spin"></i> -->
					</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<!-- modal dialog -->
<div id="modal_trans" class="modal fade">
    <div class="modal-dialog"  style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                  <div class="col-xs-8"> 
            			<div id="load">Loading...</div> 
                  </div> 

                    <!-- .modal-footer -->
                    <div class="modal-footer"></div>
                    <!-- /.modal-footer -->
                </div>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function () { 
        $("#load").hide();
		// $("#spinner-div").hide();
		$(".btn-tampil").click(function(){
			table.ajax.reload();
		});
		var column = [];
		var column2 = [];

		/*
		for (i = 1 i <= 1; i++) {
			column.push({
				"aType":"numeric-comma",
				"aTargets": [ i ],
				"mRender": function (data, type, full) {
					return type === 'export' ? data : numeral(data).format('0,0.00');
					// return formmatedvalue;
					}
				});
		}
		*/

		//column.push({ "aDataSort": [ 1,2 ], "aTargets": [ 1 ] });
 
		table = $('.tbl1').DataTable({
			"aoColumnDefs": column,
			"order": [[ 0, "asc" ]],
			"columns": [ 
				{ "data": "nomor" },
				{ "data": "no_polisi" },
				{ "data": "nama_pemilik" },
			], 
            "scrollY": 400,
            "scrollCollapse": true,
            "scrollX": true,
            "filter": true,
        
            "deferRender": true,
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[ -1], [ "Semua Data"]],
            // "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
								//{ "name": "periode_awal", "value": $("#periode_awal").val() }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('undian/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				//{extend: 'copy'},
				//{extend: 'csv'},
				//{extend: 'excel'},
				{
					extend:    'excelHtml5',
					text:      'Export To Excel',
					titleAttr: 'Excel',
					"oSelectorOpts": { filter: 'applied', order: 'current' },
					"sFileName": "report.xls",
					action : function( e, dt, button, config ) {
						exportTableToCSV.apply(this, [$('.tbl1'), 'export.xls']);

					},
					exportOptions: {orthogonal: 'export'}

				},
				/*
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						   $(win.document.body).addClass('white-bg');
						   $(win.document.body).css('font-size', '10px');
						   $(win.document.body).find('table')
								   .addClass('compact')
								   .css('font-size', 'inherit');
				   }
				}
				*/
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.tbl1').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		// Add event listener for opening and closing details
		$('.tbl1 tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				//format(row.data());
				tr.addClass('shown');
			}
		} );

		$('.tbl1 tfoot th').each( function () {
			var title = $('.tbl1 thead th').eq( $(this).index() ).text();
			if(title!=="Detail" && title!=="Edit" && title!=="Delete"){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});

		//column.push({ "aDataSort": [ 1,2 ], "aTargets": [ 1 ] });
 
		event = $('.tbl2').DataTable({
			"aoColumnDefs": column2, 
			"columns": [  
				{ "data": "no_polisi" },
				{ "data": "nama_pemilik" },
				{ "data": "alamat" },
				{ "data": "no_hp" },
				{ "data": "del" },
			], 
            "scrollY": 400,
            "scrollCollapse": true,
            "scrollX": true,
            "filter": true,
        
            "deferRender": true, 
            "lengthMenu": [[ -1], [ "Semua Data"]],
            // "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": false, 
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
								//{ "name": "periode_awal", "value": $("#periode_awal").val() }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('undian/json_dgview_event');?>",
			// "oLanguage": {
			// 	"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			// },
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				//{extend: 'copy'},
				//{extend: 'csv'},
				//{extend: 'excel'},
				{
					extend:    'excelHtml5',
					text:      'Export To Excel',
					titleAttr: 'Excel',
					"oSelectorOpts": { filter: 'applied', order: 'current' },
					"sFileName": "report.xls",
					action : function( e, dt, button, config ) {
						exportTableToCSV.apply(this, [$('.tbl2'), 'export.xls']);

					},
					exportOptions: {orthogonal: 'export'}

				},
				/*
				{extend: 'pdf',
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						   $(win.document.body).addClass('white-bg');
						   $(win.document.body).css('font-size', '10px');
						   $(win.document.body).find('table')
								   .addClass('compact')
								   .css('font-size', 'inherit');
				   }
				}
				*/
			],
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
		});

		$('.tbl2').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});   

		$(".btn-draw").click(function(e){
			 
			// $('#load').fadeOut();
			e.preventDefault();
			// $('#modal_trans').modal('toggle');
			$("#load").show();
			// $('#loading').addClass('overlay');
			// $('#loading').html('<i class="fa fa-spinner fa-spin"></i>');
			setTimeout(RemoveClass, 5000);
			// draw(nopol[random]);
		});
	});

	function RemoveClass(){

        // $("#modal_transaksi").modal("hide");
        $("#load").fadeOut();
		// $('#loading').removeClass('overlay');
		// $('#loading').fadeOut();

			var nopol = table.column( 1 ).data().toArray();
			const random = Math.floor(Math.random() * nopol.length); 
			// alert(nopol[random]);
			draw(nopol[random]);
	}

    function draw(nopol){

        $.ajax({
            type: "POST",
            url: "<?=site_url("undian/submit");?>",
            data: {"nopol":nopol },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    if(data.tipe==="success"){
                        event.ajax.reload();
                    }else{
                    	swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            event.ajax.reload();
                        });}
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

	function deleted(nopol){

        $.ajax({
            type: "POST",
            url: "<?=site_url("undian/delete");?>",
            data: {"nopol":nopol },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    if(data.tipe==="success"){
                        event.ajax.reload();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

</script>

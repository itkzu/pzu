<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstasethapus_qry
 *
 * @author adi
 */
class Mstasethapus_qry extends CI_Model {

    //put your code here
    protected $res = "";
    protected $delete = "";
    protected $state = "";
    protected $kd_cabang = "";

    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function select_data($param) {
        $this->db->select("noashp, to_char(tglashp,'DD-MM-YYYY') as tglashp, ket, userentry, tglentry, periode");
        $this->db->where('noashp', $param);
        $query = $this->db->get('t_aset_hapus');
        return $query->result_array();
    }

    public function get_detail_ahp() {
        $noashp = $this->input->post('noashp');
        $this->db->select("kdaset
                            , nmaset
                            , ket_aset
                            , to_char(tglbeli,'DD FMMonth YYYY') as tglbeli
                            , ket_hapus
                            , kdasetx");
        $this->db->where('noashp', $noashp);
        $query = $this->db->get('vl_aset_hapus_d');
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            $nourut = 1;
            $data = array();
            foreach ($res as $key => $value) {
                $data[$key]['nourut'] = $nourut;
                $data[$key]['kdaset'] = $value['kdaset'];
                $data[$key]['nmaset'] = $value['nmaset'];
                $data[$key]['ket_aset'] = $value['ket_aset'];
                $data[$key]['tglbeli'] = $value['tglbeli'];
                $data[$key]['alasan'] = $value['ket_hapus'];
                $data[$key]['hapus'] = '<button class="btn btn-danger btn-xs btn-delete" type="button"><i class="fa fa-trash"></i></button>';
                $data[$key]['kdasetx'] = $value['kdasetx'];
                $nourut++;
            }
            $state = "1";
            $msg = "Data ditemukan";
        } else {
            $data = "";
            $state = "0";
            $msg = "Data tidak temukan";
        }
        $res = array(
            'state' => $state,
            'data' => $data,
            'msg' => $this->db->last_query(),
        );
        return json_encode($res);
    }

    public function json_dgview() {
        error_reporting(-1);

        $aColumns = array('noashp',
            'tglashp',
            'ket',);
        $sIndexColumn = "noashp";

        $sLimit = "";
        if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (
                    select noashp,tglashp,ket from pzu.t_aset_hapus
                    ) AS a";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            if ($_GET['iDisplayStart'] > 0) {
                $sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
                        intval($_GET['iDisplayStart']);
            }
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == " ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = " Where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "lower(" . $aColumns[$i] . ") LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        for ($i = 0; $i < count($aColumns); $i++) {

            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(" . $aColumns[$i] . ")  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query($sQuery);

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(" . $sIndexColumn . ") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "data" => array()
        );

        $detail = $this->getdetail();
        foreach ($rResult->result_array() as $aRow) {
            foreach ($aRow as $key => $value) {
                if (is_numeric($value)) {
                    $aRow[$key] = (float) $value;
                } else {
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if ($aRow['noashp'] == $value['noashp']) {
                    $aRow['detail'][] = $value;
                }
            }
            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"" . site_url('mstasethapus/edit/' . $aRow['noashp']) . "\">Edit</a>";
            $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('" . $aRow['noashp'] . "');\">Hapus</button>";
            $output['data'][] = $aRow;
        }
        echo json_encode($output);
    }

    private function getdetail() {
        $output = array();
        $str = "select * from pzu.vl_aset_hapus_d";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ($res as $aRow) {
            foreach ($aRow as $key => $value) {
                if (is_numeric($value)) {
                    $aRow[$key] = (float) $value;
                } else {
                    $aRow[$key] = $value;
                }
            }
            $output[] = $aRow;
        }
        return $output;
    }

    public function submit() {
        $this->data = array();
        try {
            $array = $this->input->post();
            if (!empty($array['kdaset']) && !empty($array['alasan'])) {
                $this->db->select("kdaset, kdasetx, nmaset, ket_aset, to_char(tglbeli,'DD FMMonth YYYY') as tglbeli, harga, nmjaset, nmlokasi, ket_posisi");
                $query = $this->db->get_where('v_aset_riel', array('kdasetx' => $array['kdaset']));
                if ($query->num_rows() > 0) {
                    $res = $query->result_array();
                    $data = array(
                        'alasan' => strtoupper($array['alasan']),
                    );
                    foreach ($res as $value) {
                        $data['kdaset'] = $value['kdaset'];
                        $data['kdasetx'] = $value['kdasetx'];
                        $data['nmaset'] = $value['nmaset'];
                        $data['ket_aset'] = $value['ket_aset'];
                        $data['tglbeli'] = $value['tglbeli'];
                        $data['hapus'] = '<button class="btn btn-danger btn-xs btn-delete" type="button"><i class="fa fa-trash"></i></button>';
                    }
                    $this->data = array('0' => $data,);
                } else {
                    throw new Exception("Error: Data aset [" . $array['kdaset'] . "] tidak ditemukan");
                }
                $this->res = "Data Ditemukan";
                $this->state = "1";
            } else {
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            'data' => $this->data,
        );
        return json_encode($arr);
    }

    public function proses() {
        $this->data = "";
        try {
            $array = $this->input->post();
            if (empty($array['kode']) && !empty($array['tglhapus']) && !empty($array['catatan'])) {
                $tglhapus = $this->apps->dateConvert($array['tglhapus']);
                $catatan = strtoupper($array['catatan']);
                $username = $this->session->userdata('username');
                $kdaset = json_brackets($array['kdaset']);
                $alasan = json_brackets($array['alasan']);

                $str = "SELECT * FROM pzu.aset_hapus_ins('" . $tglhapus . "'"
                        . " ,'" . $catatan . "'"
                        . " ,'" . $username . "'"
                        . " ,'" . $kdaset . "'"
                        . " ,'" . $alasan . "')";
                $query = $this->db->query($str);
                if (!$query) {
                    $err = $this->db->error();
                    throw new Exception(" Error : " . $this->apps->err_code($err['message']));
                } else {
                    if ($query->num_rows() > 0) {
                        $res = $query->result_array();
                        $this->data = $res;
                        $this->res = "Data Berhasil Diproses!";
                        $this->state = "1";
                    } else {
                        $this->res = "Data Tidak Dapat Diproses!";
                        $this->state = "0";
                    }
                }
            } elseif (!empty($array['kode']) && !empty($array['tglhapus']) && !empty($array['catatan']) && empty($array['stat'])) {
                $tglhapus = $this->apps->dateConvert($array['tglhapus']);
                $catatan = strtoupper($array['catatan']);
                $username = $this->session->userdata('username');
                $kdaset = json_brackets($array['kdaset']);
                $alasan = json_brackets($array['alasan']);

                $str = "SELECT * FROM pzu.aset_hapus_upd('" . $array['kode'] . "'"
                        . " ,'" . $tglhapus . "'"
                        . " ,'" . $catatan . "'"
                        . " ,'" . $username . "'"
                        . " ,'" . $kdaset . "'"
                        . " ,'" . $alasan . "')";
                $query = $this->db->query($str);
                if (!$query) {
                    $err = $this->db->error();
                    throw new Exception(" Error : " . $this->apps->err_code($err['message']));
                } else {
                    if ($query->num_rows() > 0) {
                        $this->data = array(
                            '0' => array(
                                'aset_hapus_ins' => $array['kode']
                            )
                        );
                        $this->res = "Data Berhasil Diproses!";
                        $this->state = "1";
                    } else {
                        $this->res = "Data Tidak Dapat Diproses!";
                        $this->state = "0";
                    }
                }
            } elseif (!empty($array['kode']) && !empty($array['stat'])) {
                $username = $this->session->userdata('username');

                $str = "SELECT * FROM pzu.aset_hapus_del('" . $array['kode'] . "'"
                        . " ,'" . $username . "')";
                $query = $this->db->query($str);
                if (!$query) {
                    $err = $this->db->error();
                    throw new Exception(" Error : " . $this->apps->err_code($err['message']));
                } else {
                    if ($query->num_rows() > 0) {
                        $this->data = array(
                            '0' => array(
                                'aset_hapus_ins' => $array['kode']
                            )
                        );
                        $this->res = "Data " . $array['kode'] . " Berhasil Diproses!";
                        $this->state = "1";
                    } else {
                        $this->res = "Data " . $array['kode'] . " Tidak Dapat Diproses!";
                        $this->state = "0";
                    }
                }
            } else {
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }
        } catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            'data' => $this->data,
        );
        return json_encode($arr);
    }

}

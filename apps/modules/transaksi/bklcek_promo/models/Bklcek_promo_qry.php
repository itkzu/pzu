<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Bklcek_promo_qry
 *
 * @author adi
 */
class Bklcek_promo_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function getDataUnit() {
        $nama = $this->input->post('nama');
        $this->db->where('LOWER(nosin)', strtolower($nama));
        $this->db->select("nodo,tgldo,nosin,nora,nmtipe,nama_s,jenis,promo");
        $query = $this->db->get('pzu.v_kpbtipe');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getDataKPB() {
        $nosin = $this->input->post('nosin');
        $this->db->where('LOWER(nosin)', strtolower($nosin));
        $this->db->select("nosin,nora,cx_kpb1,cx_kpb2,cx_kpb3,cx_kpb4,kpb1,kpb2,kpb3,kpb4");
        $query = $this->db->get('pzu.v_kpbreminder');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    } 

    public function getNama() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nosin)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nora)', strtolower($q));
        $this->db->or_like('LOWER(nodo)', strtolower($q));
        $this->db->or_like('LOWER(nama_s)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nama_s) like '".strtolower($q)."' then 1
                                when LOWER(nama_s) like '".strtolower($q)."%' then 2
                                when LOWER(nama_s) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nama_s"); 
        $query = $this->db->get('pzu.v_kpbtipe');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['nosin'],
                    'text' => $value['nama_s'], 
                    'nodo' => $value['nodo'],
                    'tgldo' => $value['tgldo'],
                    'nmtipe' => $value['nmtipe'],
                    'jenis' => $value['jenis'],
                    'nosin' => $value['nosin'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '', 
                'nodo' => '',
                'tgldo' => '',
                'nmtipe' => '',
                'jenis' => '',
                'nosin' => 'nosin',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    } 
    
    public function proses() { 
        $nosin = $this->input->post('nosin');
        $fkpb1 = $this->input->post('fkpb1');
        $tglkpb1 = $this->input->post('tglkpb1');
        $fkpb2 = $this->input->post('fkpb2');
        $tglkpb2 = $this->input->post('tglkpb2');
        $fkpb3 = $this->input->post('fkpb3');
        $tglkpb3 = $this->input->post('tglkpb3');
        $fkpb4 = $this->input->post('fkpb4');
        $tglkpb4 = $this->input->post('tglkpb4');
        $ket_batal = $this->input->post('ket_batal');
        $q = $this->db->query("select title,msg,tipe from pzu.promo_reminder('". $nosin ."','". $fkpb1 ."','". $tglkpb1 ."','". $fkpb2 ."','". $tglkpb2 ."','". $fkpb3 ."','". $tglkpb3 ."','". $fkpb4 ."','". $tglkpb4 ."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }
}

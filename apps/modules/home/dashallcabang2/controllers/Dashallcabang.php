<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashallcabang
 *
 * @author adi
 */
class Dashallcabang extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('dashallcabang/submit'),
            'add' => site_url('dashallcabang/add'),
            'edit' => site_url('dashallcabang/edit'),
            'reload' => site_url('dashallcabang'),
        );
        $this->load->model('dashallcabang_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'required'    => '',
            ),
        );
    }
    
    public function get_penjualan() {
        echo $this->dashallcabang_qry->get_penjualan();
    }
    
    public function get_target_penjualan() {
        echo $this->dashallcabang_qry->get_target_penjualan();
    }
    
    public function get_blm_trm_po_leasing() {
        $nama_cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        $periode = date('Y-m-d');
        $data = array();
        foreach ($nama_cabang as $value) {
            $res = $this->dashallcabang_qry->get_blm_trm_po_leasing($value, $periode);
            if($res){
                array_push($data, json_decode($res));
            }
        }
        echo json_encode($data);
    }
    
    public function get_blm_trm_po_leasing_detail() {
        $data = $this->dashallcabang_qry->get_blm_trm_po_leasing_detail();
        $arr = json_decode($data, true);
        $res = array();
        $no = 0;
        foreach ($arr as $dt) {
            foreach ($dt as $k => $val) {
                if(is_numeric($val)){
                    $res[$no][$k] = (float) $val;
                }else{
                    $res[$no][$k] = $val;
                }   
            }
            $no++;
        }
        echo json_encode($res);
    }


    //penjualan versi CURL
    public function get_penjualan_curl() {

        /*
        $cabang = array(
              'PZU A.YANI'
            , 'PZU BREBES'
            , 'PZU KUDUS'
            , 'PZU PATI'
            , 'PZU PURWODADI'
            , 'PZU SETIABUDI'
        );
        */

        $cabang = array(
              'pzusmg1.ip-dynamic.com'
            , 'pzubrebes1.ip-dynamic.com'
            , 'pzukudus1.ip-dynamic.com'
            , 'pzupati1.ip-dynamic.com'
            , 'pzupwd1.ip-dynamic.com'
            , 'pzusmg2.ip-dynamic.com'
        );


        $data = array();
        foreach ($cabang as $value) {
            $res = $this->dashallcabang_qry->get_blm_trm_po_leasing($value);
            if($res){
                array_push($data, json_decode($res));
            }
        }
        echo json_encode($data);
    }

}

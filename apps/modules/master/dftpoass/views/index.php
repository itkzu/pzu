<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable">
                    <thead>
                        <tr>
                            <th style="width: 1%;text-align: center;">Detail</th>
                            <th style="width: 19%;text-align: center;">No. Shipping List</th>
                            <th style="width: 19%;text-align: center;">Tanggal Terima</th>
                            <th style="width: 19%;text-align: center;">Main Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 1%;text-align: center;">Detail</th>
                            <th style="width: 19%;text-align: center;">No. Shipping List</th>
                            <th style="width: 19%;text-align: center;">Tanggal Terima</th>
                            <th style="width: 19%;text-align: center;">Main Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Dealer Id</th>
                            <th style="width: 19%;text-align: center;">Status</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var column = [];

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        column.push({
            "aTargets": [ 0,5 ],
      		"searchable": false,
            "bSortable": false,
            "sClass": "center"
        });

        $('.dataTableDetail').DataTable({});
        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "columns": [
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "noshippinglist" },
                { "data": "tanggalterima" },
                { "data": "maindealerid" },
                { "data": "noinvoice" },
                { "data": "statusshippinglist" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('dftpoass/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // Add event listener for opening and closing details
        $('.dataTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Detail" && title!=="Edit" && title!=="Delete"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',


                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


             download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover dataTableDetail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
												'<th style="text-align: center;">Kode Tipe Unit</th>'+
												'<th style="text-align: center;">Kode Warna</th>'+
												'<th style="text-align: center;">Kuantitas Terkirim</th>'+
												'<th style="text-align: center;">Kuantitas Diterima</th>'+
												'<th style="text-align: center;">No. Mesin</th>'+
												'<th style="text-align: center;">No. Rangka</th>'+
												'<th style="text-align: center;">Status RFS</th>'+
												'<th style="text-align: center;">PO ID</th>'+
												'<th style="text-align: center;">Kelengkapan Unit</th>'+
												'<th style="text-align: center;">No Goods Receipt</th>'+
                        '<th style="text-align: center;">Doc NRFS ID</th>'+
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
												'<td style="text-align: ">'+data.kodetipeunit+'</td>'+
												'<td style="text-align: ">'+data.kodewarna+'</td>'+
												'<td style="text-align: ">'+data.kuantitasterkirim+'</td>'+
												'<td style="text-align: ">'+data.kuantitasditerima+'</td>'+
												'<td style="text-align: ">'+data.nomesin+'</td>'+
												'<td style="text-align: ">'+data.norangka+'</td>'+
                        '<td style="text-align: ">'+data.statusrfs+'</td>'+
                        '<td style="text-align: ">'+data.poid+'</td>'+
                        '<td style="text-align: ">'+data.kelengkapanunit+'</td>'+
                        '<td style="text-align: ">'+data.nogoodsreceipt+'</td>'+
                        '<td style="text-align: ">'+data.docnrfsid+'</td>'+
                    '</tr>';
                    no++;
                });
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    });

</script>

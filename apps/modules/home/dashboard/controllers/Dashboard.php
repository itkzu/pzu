<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashboard
 *
 * @author adi
 */
class Dashboard extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
        );
        $this->load->model('dashboard_qry');
        $this->data['cabang'] = array(
            "" => "-- PILIH CABANG --",
            "http://pzusmg1.ddns.net" => "A. Yani Semarang",
            "http://pzusmg2.ddns.net" => "Setiabudi Semarang",
            "http://pzubrebes1.ddns.net" => "Brebes",
            "http://pzukudus1.ddns.net" => "Kudus",
            "http://pzupati1.ddns.net" => "Pati",
            "http://pzupwd1.ddns.net" => "Purwodadi",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){

        //echo "<script> console.log('PHP: ". $this->apps->logintag ."');</script>";
        //var_dump($this->apps->logintag);

        //$this->dashboard_qry->set_apps_var();

        $this->_init_add();


        $dashboard_view = $this->session->userdata('dashboard');
        if ($dashboard_view == '') {
            $dashboard_view = 'index';
        }

        $this->template
            ->title('Dashboard',$this->apps->name)
            ->set_layout('main')
            ->build($dashboard_view,$this->data);

        /*
        $groupname = $this->session->userdata('groupname');
        if($groupname==="SALES"
                || $groupname==="ADMIN"
                || $groupname==="ACCOUNTING" ){
            // Dashboard untuk SALES / ADMIN / ACCOUNTING
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('adh',$this->data);
        }elseif($groupname==="SPV"){
            // Dashboard untuk SPV
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('spv',$this->data);
        }elseif($groupname==="HRD"){
            // Dashboard untuk HRD
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('hrd',$this->data);
        }elseif($groupname==="SALES ONLINE"){
            // Dashboard untuk SALES ONLINE
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('sales_online',$this->data);
        }elseif($groupname==="ADH"){
            // Dashboard untuk ADH

            //$this->load->model('dashadh/dashadh_qry');

            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                //->build('dashadh/index',$this->data);
                ->build('adh',$this->data);
        }else{
            $this->template
                ->title('Dashboard',$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
        */
    }

    public function getJualPerTahun() {
        echo $this->dashboard_qry->getJualPerTahun();
    }

    public function getJualPerBulan() {
        echo $this->dashboard_qry->getJualPerBulan();
    }

    public function getJualTunaiKreditPerBulan() {
        echo $this->dashboard_qry->getJualTunaiKreditPerBulan();
    }

    public function getJualUnitTahun() {
        echo $this->dashboard_qry->getJualUnitTahun();
    }

    public function getDetailJualUnitTahun() {
        echo $this->dashboard_qry->getDetailJualUnitTahun();
    }

    public function getJualHeaderSalesTahun() {
        echo $this->dashboard_qry->getJualHeaderSalesTahun();
    }

    public function getJualSalesTahun() {
        echo $this->dashboard_qry->getJualSalesTahun();
    }

    public function getDetailJualSalesTahun() {
        echo $this->dashboard_qry->getDetailJualSalesTahun();
    }

    public function getJualTipe() {
        echo $this->dashboard_qry->getJualTipe();
    }

    public function getJualPerItemMTD() {
        echo $this->dashboard_qry->getJualPerItemMTD();
    }

    public function getJualTipeMTD() {
        echo $this->dashboard_qry->getJualTipeMTD();
    }

    public function getJualHarian() {
        echo $this->dashboard_qry->getJualHarian();
    }

    public function getJualHarianPerTipe() {
        echo $this->dashboard_qry->getJualHarianPerTipe();
    }

    public function getGPHarianPerTipe() {
        echo $this->dashboard_qry->getGPHarianPerTipe();
    }

    public function getJualLeasing() {
        echo $this->dashboard_qry->getJualLeasing();
    }

    public function getJualPerLeasingOthersYTD() {
        echo $this->dashboard_qry->getJualPerLeasingOthersYTD();
    }

    public function getJualPerLeasingOthers() {
        echo $this->dashboard_qry->getJualPerLeasingOthers();
    }

    public function getJualSalesHeader() {
        echo $this->dashboard_qry->getJualSalesHeader();
    }

    public function getJualSalesKoorMTD() {
        echo $this->dashboard_qry->getJualSalesKoorMTD();
    }

    public function getJualSalesMTD(){
        echo $this->dashboard_qry->getJualSalesMTD();
    }

    public function getJualPerPos() {
        echo $this->dashboard_qry->getJualPerPos();
    }

    public function getJualPerKecamatan() {
        echo $this->dashboard_qry->getJualPerKecamatan();
    }

    public function getJualPerSalesAktual() {
        echo $this->dashboard_qry->getJualPerSalesAktual();
    }

    public function getStokPerTipeAktual() {
        echo $this->dashboard_qry->getStokPerTipeAktual();
    }

    public function getDetailStok() {
        echo $this->dashboard_qry->getDetailStok();
    }

    // dashboard ADH

    public function getTrnHarian() {
        echo $this->dashboard_qry->adh_json_trn_harian();
    }

    public function getTrnKasBank() {
        echo $this->dashboard_qry->adh_json_trn_kas_bank();
    }

    public function getTrnKasBankbkl() {
        echo $this->dashboard_qry->adh_json_trn_kas_bank_bkl();
    }

    public function getTrnProgBBN() {
        echo $this->dashboard_qry->adh_json_trn_prog_bbn();
    }

    public function get_blm_trm_po_leasing() {
        $res = $this->dashboard_qry->get_blm_trm_po_leasing();
        echo json_encode($res);
    }

    public function get_blm_trm_po_leasing_detail() {
        $data = $this->dashboard_qry->get_blm_trm_po_leasing_detail();
        $res = array();
        $no = 0;
        foreach ($data as $dt) {
            foreach ($dt as $k => $val) {
                if(is_numeric($val)){
                    $res[$no][$k] = (float) $val;
                }else{
                    $res[$no][$k] = $val;
                }
            }
            $no++;
        }
        echo json_encode($res);
    }



    //bengkel

    public function getBklJualQty() {
        echo $this->dashboard_qry->getBklJualQty();
    }

    public function getBklJualNom() {
        echo $this->dashboard_qry->getBklJualNom();
    }




    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('Y-01'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('Y-m'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
            'cabang'=> array(
                    'attr'        => array(
                        'id'    => 'cabang',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['cabang'],
                    'value'    => set_value('cabang'),
                    'name'     => 'cabang',
                    'required'    => '',
            ),
        );
    }
}

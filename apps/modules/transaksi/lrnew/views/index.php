<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
    		outline: none;
    		border: none;
    }

    .radio {
    		margin-top: 0px;
    		margin-bottom: 0px;
    }

    .checkbox label, .radio label {
    		min-height: 20px;
    		padding-left: 20px;
    		margin-bottom: 5px;
    		font-weight: bold;
    		cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <div class="box-header box-view">
                            <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                          </div>
                        </div>

                        <div class="col-xs-3">
                          <div class="box-header box-view">
                            <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()"> Ubah</button>
                            <button type="button" class="btn btn-primary btn-hapus" disabled> Hapus</button>
                            <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
                          </div>
                        </div>

                        <div class="col-xs-2">
                          <div class="box-header box-view">
                            <button type="button" class="btn btn-cetak"><i class="fa fa-print"></i> Cetak L/R</button>
                          </div>
                        </div>

                        <div class="col-xs-3">
                          <div class="form-group">
                            <?php
                                echo form_input($form['ket']);
                                echo form_error('ket','<div class="note">','</div>');
                            ?>
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-12">
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

        					<div class="col-xs-12">
        							<ul class="nav nav-tabs" id="myTab" role="tablist">
        								<li class="nav-item active" role="presentation">
        										<a class="nav-link " id="list-do" name="list-do" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Daftar DO yang Belum diproses L/R</a>
        								</li>
        								<li class="nav-item" role="presentation">
        										<a class="nav-link" id="input-do" name="input-do" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Input L/R</a>
        								</li>
        							</ul>
        						<div class="tab-content" id="myTabContent">
        							<div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-do">

        								<div class="col-xs-12">
                            <div class="table-responsive">
                              <table class="table table-hover table-bordered dataTable display nowrap" style="width:3500px">
                                <thead>
                                    <tr>
                                        <th style="width: 1%;text-align: center;">No.</th>
                                        <th style="width: 3%;text-align: center;">No. DO</th>
                                        <th style="width: 3%;text-align: center;">Tgl. DO</th>
                                        <th style="width: 3%;text-align: center;">No. SPK</th>
                                        <th style="width: 3%;text-align: center;">Tgl. SPK</th>
                                        <th style="width: 8%;text-align: center;">Nama Pemesan</th>
                                        <th style="width: 8%;text-align: center;">Nama STNK</th>
                                        <th style="width: 8%;text-align: center;">Alamat STNK</th>
                                        <th style="width: 2%;text-align: center;">Leasing</th>
                                        <th style="width: 3%;text-align: center;">Program</th>
                                        <th style="width: 2%;text-align: center;">Kode</th>
                                        <th style="width: 8%;text-align: center;">Tipe</th>
                                        <th style="width: 8%;text-align: center;">Jenis</th>
                                        <th style="width: 2%;text-align: center;">Warna</th>
                                        <th style="width: 5%;text-align: center;">No. Mesin</th>
                                        <th style="width: 5%;text-align: center;">No. Rangka</th>
                                        <th style="width: 8%;text-align: center;">Sales</th>
                                        <th style="width: 1%;text-align: center;">Edit</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="width: 2%;text-align: center;">No.</th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="text-align: center;"></th>
                                        <th style="width: 10px;text-align: center;">Edit</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                          </div>
        								</div>
        							</div>

        							<div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-do">

        									<div class="col-xs-12">
        											<hr>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-1">
                                    <?php echo form_label($form['nodo']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2">
        														<?php
        																echo form_input($form['nodo']);
        																echo form_error('nodo','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo form_label($form['noso']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['noso']);
        																echo form_error('noso','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo "<b>No. Mesin/RK</b>"; ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['nosin']);
        																echo form_error('nosin','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-3 ">
        														<?php
        																echo form_input($form['norangka']);
        																echo form_error('norangka','<div class="note">','</div>');
        														?>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-1">
                                    <?php echo form_label($form['tgldo']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2">
        														<?php
        																echo form_input($form['tgldo']);
        																echo form_error('tgldo','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo form_label($form['nama']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['nama']);
        																echo form_error('nama','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo "<b>Tipe Unit</b>"; ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['kdtipe']);
        																echo form_error('kdtipe','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-3 ">
        														<?php
        																echo form_input($form['nmtipe']);
        																echo form_error('nmtipe','<div class="note">','</div>');
        														?>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-1">
                                    <?php echo form_label($form['nmsales']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2">
        														<?php
        																echo form_input($form['nmsales']);
        																echo form_error('nmsales','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo form_label($form['nmleasing']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['nmleasing']);
        																echo form_error('nmleasing','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo "<b>Warna/Tahun</b>"; ?>
        												</div>

        												<div class="col-xs-1 ">
        														<?php
        																echo form_input($form['nmwarna']);
        																echo form_error('nmwarna','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
        														<?php
        																echo form_input($form['tahun']);
        																echo form_error('tahun','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo form_label($form['nopo']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['nopo']);
        																echo form_error('nopo','<div class="note">','</div>');
        														?>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-1">
                                    <?php echo form_label($form['nmspv']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2">
        														<?php
        																echo form_input($form['nmspv']);
        																echo form_error('nmspv','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo form_label($form['progleas']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['progleas']);
        																echo form_error('progleas','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo "<b>STATUS OTR</b>"; ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['stat_otr']);
        																echo form_error('stat_otr','<div class="note">','</div>');
        														?>
        												</div>

        												<div class="col-xs-1 ">
                                    <?php echo form_label($form['tglpo']['placeholder']); ?>
        												</div>

        												<div class="col-xs-2 ">
        														<?php
        																echo form_input($form['tglpo']);
        																echo form_error('tglpo','<div class="note">','</div>');
        														?>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        											<hr>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['hrg_otr']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['hrg_otr']);
        																			echo form_error('hrg_otr','<div class="note">','</div>');
        																	?>
        															</div>

            													<div class="col-xs-3 ">
                                        <i>Beban Leasing</i>
            													</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

                                      <div class="col-xs-4 ">
                                          <?php echo form_label($form['hdd2']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['hdd2']);
        																			echo form_error('hdd2','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['disc_unit1']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['disc_unit1']);
        																			echo form_error('disc_unit1','<div class="note">','</div>');
        																	?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['disc_leasing']);
        																			echo form_error('disc_leasing','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

                                      <div class="col-xs-4 ">
                                          <?php echo form_label($form['cad_bbn']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['cad_bbn']);
        																			echo form_error('cad_bbn','<div class="note">','</div>');
        																	?>
        															</div>

        															<div class="col-xs-1">
        															</div>

        															<div class="col-xs-3">
                                          <i>AR Insentif Leasing</i>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_dealer1']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_dealer1']);
        																			echo form_error('sub_dealer1','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['hd_dpp']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['hd_dpp']);
        																			echo form_error('hd_dpp','<div class="note">','</div>');
        																	?>
        															</div>

        															<div class="col-xs-1" style="text-align:right">
                                          <i>JP</i>
        															</div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['jp']);
        																			echo form_error('jp','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_md1']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_md1']);
        																			echo form_error('sub_md1','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['sub_hso']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['sub_hso']);
        																			echo form_error('sub_hso','<div class="note">','</div>');
        																	?>
        															</div>

        															<div class="col-xs-1" style="text-align:right">
                                          <i>Matriks</i>
        															</div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['matriks']);
        																			echo form_error('matriks','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_ahm1']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_ahm1']);
        																			echo form_error('sub_ahm1','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['ins_leas_dpp']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['ins_leas_dpp']);
        																			echo form_error('ins_leas_dpp','<div class="note">','</div>');
        																	?>
        															</div>

        															<div class="col-xs-1" style="text-align:right">
                                          <i>Total</i>
        															</div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['total']);
        																			echo form_error('total','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_leas1']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_leas1']);
        																			echo form_error('sub_leas1','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['ins_ai']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['ins_ai']);
        																			echo form_error('ins_ai','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-2 ">
        															</div>

        															<div class="col-xs-5">
        																	<?php echo form_label($form['hdd1']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['hdd1']);
        																			echo form_error('hdd1','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-4 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['hj_net']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['hj_net']);
        																			echo form_error('hj_net','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['um_cust']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['um_cust']);
        																			echo form_error('um_cust','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['hbu_dpp']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['hbu_dpp']);
        																			echo form_error('hbu_dpp','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['disc_unit2']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['disc_unit2']);
        																			echo form_error('disc_unit2','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['jaket']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['jaket']);
        																			echo form_error('jaket','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_dealer2']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_dealer2']);
        																			echo form_error('sub_dealer2','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-1 ">
                                          <?php echo form_label($form['ins_sales']['placeholder']); ?>
                                      </div>

            													<div class="col-xs-2 ">
                                          <?php
                                              echo form_dropdown($form['insentif']['name'],$form['insentif']['data'] ,$form['insentif']['value'] ,$form['insentif']['attr']);
                                              echo form_error('insentif','<div class="note">','</div>');
                                          ?>
            													</div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['ins_sales']);
        																			echo form_error('ins_sales','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_md2']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_md2']);
        																			echo form_error('sub_md2','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['biops']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['biops']);
        																			echo form_error('biops','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_ahm2']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_ahm2']);
        																			echo form_error('sub_ahm2','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['komisi']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['komisi']);
        																			echo form_error('komisi','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-3 ">
        																	<?php echo form_label($form['sub_leas2']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['sub_leas2']);
        																			echo form_error('sub_leas2','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-1 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['oth']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['oth']);
        																			echo form_error('oth','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-2 ">
        															</div>

        															<div class="col-xs-5">
        																	<?php echo form_label($form['um']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['um']);
        																			echo form_error('um','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-4 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['hpp']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['hpp']);
        																			echo form_error('hpp','<div class="note">','</div>');
        																	?>
        															</div>

        															<div class="col-xs-2">
        																	<?php
        																			echo form_input($form['iris']);
        																			echo form_error('iris','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>

        									<div class="col-xs-12">
        										<div class="row">

        												<div class="col-xs-5 col-md-5 ">
        													<div class="row">

        															<div class="col-xs-2 ">
        															</div>

        															<div class="col-xs-5">
        																	<?php echo form_label($form['lunas']['placeholder']); ?>
        															</div>

        															<div class="col-xs-4">
        																	<?php
        																			echo form_input($form['lunas']);
        																			echo form_error('lunas','<div class="note">','</div>');
        																	?>
        															</div>

        													</div>
        												</div>

        												<div class="col-xs-7 col-md-7 ">
        													<div class="row">

        															<div class="col-xs-4 ">
        															</div>

                                      <div class="col-xs-3 ">
                                          <?php echo form_label($form['lr_unit']['placeholder']); ?>
                                      </div>

        															<div class="col-xs-3">
        																	<?php
        																			echo form_input($form['lr_unit']);
        																			echo form_error('lr_unit','<div class="note">','</div>');
        																	?>
        															</div>
        													</div>
        												</div>

        										</div>
        									</div>


        									<div class="col-xs-12">
        											<hr>
        									</div>
        							</div>
        						</div>
        					</div>
              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //input_lr

        $('.btn-ubah').hide();
        $('.btn-hapus').hide();
        $('#ket').hide();
        $('#btn-refresh').show();
        $('.btn-cetak').hide();
        clear();
        disabled();
        autonum();
{
                
            }

        //manggil kode do
        GetNoID();

        //daftar do yang blm input
        var column = [];

        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "orderCellsTop": true,
            "fixedHeader": true,
            // "scrollY": 500,
            // "scrollX": true,
            // "aoColumnDefs": column,
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                 "mData" : "nodo",
                    "mRender": function (data, type, row) {
                    var btn = '<a href="lrnew/edit/' + data +'">' + data + ' </a>';
                    return btn;
                }
                }],
            "columns": [
                { "data": "no"},
                { "data": "nodo"},
                { "data": "tgldo" },
                { "data": "noso"},
                { "data": "tglso" },
                { "data": "nama" },
                { "data": "nama_s" },
                { "data": "alamat_s" },
                { "data": "nmleasingx" },
                { "data": "nmprogleas" },
                { "data": "kode" },
                { "data": "kdtipe" },
                { "data": "nmtipe" },
                { "data": "nmwarna" },
                { "data": "nosin2" },
                { "data": "nora2" },
                { "data": "nmsales" },
                { "data": "edit"},
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('lrnew/json_dgview');?>",
      			"oLanguage": {
      				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
      			},
      			//dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                    .search( this.value )
                    .draw();
                //}
            });
        });

      	//row number
      	table.on( 'draw.dt', function () {
      	var PageInfo = $('.dataTable').DataTable().page.info();
      			table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
      					cell.innerHTML = i + 1 + PageInfo.start;
      			});
      	});

        //fungsi tombol
        $('.btn-refresh').click(function(){
            table.ajax.reload();
        });
        $('.btn-cetak').click(function(){

            var nodo = $('#nodo').val();
            window.open('lrnew/ctk_lr/'+nodo, '_blank');
            // window.location.href = 'lrnew/ctk_lr/'+nodo;

            // ctk_lr();
        });

        $('#input-do').click(function(){
            $('.btn-ubah').show();
            $('.btn-hapus').show();
            $('.btn-cetak').show();
            $('.btn-ubah').attr("disabled",true);
            $('.btn-cetak').attr("disabled",true);
            $('#ket').hide();
            $('.btn-refresh').hide();
        });

        $('#list-do').click(function(){
            $('.btn-ubah').hide();
            $('.btn-hapus').hide();
            $('.btn-cetak').hide();
            $('#ket').hide();
            $('.btn-refresh').show();
        });

        // $('.btn-ubah').click(function(){
        //     ubah();
        // });

        //fungsi input
        $('#nodo').change(function(){
            set_nodo();
        });
      });

      //fungsi sistem

      // no id
    function GetNoID(){
        var tg = new Date();
        var tgl = tg.toString();
        var tanggal = tgl.substring(13,15);
          //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("lrnew/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#nodo').mask(value.kode+"99-999999");
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function ctk_lr(){
        var nodo = $("#nodo").val();
        var nodo = nodo.toUpperCase();
          $.ajax({
              type: "POST",
              url: "<?=site_url("lrnew/ctk_labarugi");?>",
              data: {"nodo":nodo },
              success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                  });
              }
          });
    }


  function ubah(){
      var nodo = $('#nodo').val();
      window.location.href = 'lrnew/edit/'+nodo;
  }

     //memanggil data do

    function set_nodo(){
        var nodo = $("#nodo").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("lrnew/set_nodo");?>",
            data: {"nodo":nodo},
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    // baris 1
                    if (data.progres_po<1){
                        $('#ket').show();
                        $('#ket').val('BELUM INPUT FAKTUR PEMBELIAN');
                        $('.btn-ubah').attr("disabled",true);
                        $('.btn-cetak').attr("disabled",true); 
                    }else if (data.fposting<1){
                        $('#ket').show();
                        $('#ket').val('BELUM PROSES L/R');
                        $('.btn-ubah').attr("disabled",false);
                        $('.btn-cetak').attr("disabled",true);
                    } else {
                        $('#ket').show();
                        $('#ket').val('SUDAH PROSES POSTING');
                        $('.btn-ubah').attr("disabled",true);
                        $('.btn-cetak').attr("disabled",false);
                    }

                    $('#nodo').val(data.nodo);            $('#noso').val(data.noso);            $('#nosin').val(data.nosin);          $('#norangka').val(data.nora);
                    $('#tgldo').val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                    $('#nama').val(data.nama);            $('#kdtipe').val(data.kdtipe);        $('#nmtipe').val(data.nmtipe);
                    $('#nmsales').val(data.nmsales);      $('#nmleasing').val(data.nmleasing);  $('#nmwarna').val(data.warna);        $('#tahun').val(data.tahun);     $('#nopo').val(data.nopox);
                    $('#nmspv').val(data.nmsales_header); $('#progleas').val(data.nmprogleas);  $('#stat_otr').val(data.status_otr);
                    $('#tglpo').val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpo)));

                    //baris 2 kolom 1
                    $('#hrg_otr').autoNumeric('set',data.harga_otr);
                    $('#disc_unit1').autoNumeric('set',data.disc);              $('#disc_leasing').autoNumeric('set',data.disc_leasing);
                    $('#sub_dealer1').autoNumeric('set',data.sub_dealer);
                    $('#sub_md1').autoNumeric('set',data.sub_md);
                    $('#sub_ahm1').autoNumeric('set',data.sub_ahm);
                    $('#sub_leas1').autoNumeric('set',data.sub_fincoy);
                    var hdd = Math.floor(parseFloat(data.harga_otr) - (parseFloat(data.disc)+parseFloat(data.sub_dealer)+parseFloat(data.sub_md)+parseFloat(data.sub_ahm)+parseFloat(data.sub_fincoy)));
                            $('#hdd1').autoNumeric('set',hdd);

                    //baris 2 kolom 2
                    $('#hdd2').autoNumeric('set',hdd);
                    if(data.tgldo>'2022-03-31'){
                        var hd_dpp = Math.floor((parseFloat(hdd) - parseFloat(data.bbn))/1.11);
                    }else{
                        var hd_dpp = Math.floor((parseFloat(hdd) - parseFloat(data.bbn))/1.1);
                    }
                    $('#cad_bbn').autoNumeric('set',data.bbn);
                    if(data.nmleasing==='TUNAI'){
                          $('#jp').autoNumeric('set',0);
                          $('#matriks').autoNumeric('set',0);
                                var total = Math.floor(parseFloat(0) + parseFloat(0));
                                $('#total').autoNumeric('set',total);
                                $('#hd_dpp').autoNumeric('set',hd_dpp);
                                // var ins_leas_dpp = Math.floor(parseFloat(total/1.1));
                                // $('#ins_leas_dpp').autoNumeric('set',ins_leas_dpp.toFixed(0));
                                $('#ins_leas_dpp').autoNumeric('set',data.ins_leasing);
                                      $('#sub_hso').autoNumeric('set',data.sub_hso);
                                      $('#ins_ai').autoNumeric('set',data.ins_ai);
                                            var hj_net = Math.floor(parseFloat(hd_dpp)+parseFloat(data.sub_hso)+parseFloat(data.ins_leasing)+parseFloat(data.ins_ai));
                                            $('#hj_net').autoNumeric('set',hj_net);
                    } else {
                          $('#jp').autoNumeric('set',data.jp);
                          $('#matriks').autoNumeric('set',data.matriks);
                                var total = Math.floor(parseFloat(data.jp) + parseFloat(data.matriks));
                                $('#total').autoNumeric('set',total);
                                $('#hd_dpp').autoNumeric('set',hd_dpp);
                                // var ins_leas_dpp = Math.floor(parseFloat(total/1.1));
                                $('#ins_leas_dpp').autoNumeric('set',data.ins_leasing);
                                      $('#sub_hso').autoNumeric('set',data.sub_hso);
                                      $('#ins_ai').autoNumeric('set',data.ins_ai);
                                            var hj_net = Math.floor(parseFloat(hd_dpp)+parseFloat(data.sub_hso)+parseFloat(data.ins_leasing)+parseFloat(data.ins_ai));
                                            $('#hj_net').autoNumeric('set',hj_net);
                    }

                    //baris 3 kolom 1
                    $('#um_cust').autoNumeric('set',data.um);
                    $('#disc_unit2').autoNumeric('set',data.disc2);
                    $('#sub_dealer2').autoNumeric('set',data.sub_dealer);
                    $('#sub_md2').autoNumeric('set',data.sub_md);
                    $('#sub_ahm2').autoNumeric('set',data.sub_ahm);
                    $('#sub_leas2').autoNumeric('set',data.sub_fincoy);
                    var um = Math.floor(parseFloat(data.um) - parseFloat(data.disc_real));
                            $('#um').autoNumeric('set',um);
                    var lunas = Math.floor(parseFloat(hdd) - parseFloat(um));
                            $('#lunas').autoNumeric('set',lunas);

                    //baris 3 kolom 2
                    $('#hbu_dpp').autoNumeric('set',data.harga_beli);
                    $('#iris').autoNumeric('set',data.iris);
                    $('#jaket').autoNumeric('set',data.jaket);
                    $('#insentif').val(data.kdsales_ins);    $('#ins_sales').autoNumeric('set',data.ins_sales);
                    $('#biops').autoNumeric('set',data.biops);
                    $('#komisi').autoNumeric('set',data.komisi);
                    $('#oth').autoNumeric('set',data.topcover);
                              var hpp = Math.floor(parseFloat(data.harga_beli) + parseFloat(data.jaket) + parseFloat(data.ins_sales) + parseFloat(data.biops) + parseFloat(data.komisi) + parseFloat(data.topcover));
                              var lr_unit = Math.floor(parseFloat(hj_net-hpp));
                              $('#hpp').autoNumeric('set',hpp);
                              $('#lr_unit').autoNumeric('set',lr_unit);


                   });
             },
             error:function(event, textStatus, errorThrown) {
               swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
             }
         });

     }


      //fungsi tampilan

    function clear(){
      //baris 1
        $('#ket').val('');
        $('#nodo').val('');         $('#noso').val('');          $('#nosin').val('');                              $('#norangka').val('');
        $('#tgldo').val('');        $('#nama').val('');          $('#kdtipe').val('');                             $('#nmtipe').val('');
        $('#nmsales').val('');      $('#nmleasing').val('');       $('#nmwarna').val('');   $('#tahun').val('');     $('#nopo').val('');
        $('#nmspv').val('');        $('#progleas').val('');      $('#stat_otr').val('');                           $('#tglpo').val('');

      //baris 2
        $('#hrg_otr').val('');                                    $('#hdd2').val('');
        $('#disc_unit1').val('');   $('#disc_leasing').val('');   $('#cad_bbn').val('');
        $('#sub_dealer1').val('');                                      $('#hd_dpp').val('');                 $('#jp').val('');
        $('#sub_md1').val('');                                          $('#sub_hso').val('');                $('#matriks').val('');
        $('#sub_ahm1').val('');                                         $('#ins_leas_dpp').val('');           $('#total').val('');
        $('#sub_leas1').val('');                                        $('#ins_ai').val('');
                $('#hdd1').val('');                                             $('#hj_net').val('');

        $('#um_cust').val('');                                          $('#hbu_dpp').val('');
        $('#disc_unit2').val('');                                       $('#jaket').val('');
        $('#sub_dealer2').val('');                                      $('#insentif').val('1');    $('#ins_sales').val('');
        $('#sub_md2').val('');                                          $('#biops').val('');
        $('#sub_ahm2').val('');                                         $('#komisi').val('');
        $('#sub_leas2').val('');                                        $('#oth').val('');
                $('#um').val('');                                               $('#hpp').val('');
                $('#lunas').val('');                                            $('#lr_unit').val('');
                $('#iris').val('');
    }

    function autonum(){

      //baris 2
        $('#hrg_otr').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                             $('#hdd2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#disc_unit1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});   $('#disc_leasing').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});   $('#cad_bbn').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_dealer1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                         $('#hd_dpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                  $('#jp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_md1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                             $('#sub_hso').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                 $('#matriks').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_ahm1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                            $('#ins_leas_dpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});            $('#total').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_leas1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                           $('#ins_ai').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
              $('#hdd1').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                                  $('#hj_net').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});

        $('#um_cust').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                             $('#hbu_dpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#disc_unit2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                          $('#jaket').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_dealer2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                         $('#ins_sales').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_md2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                             $('#biops').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_ahm2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                            $('#komisi').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#sub_leas2').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                           $('#oth').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
              $('#um').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                                  $('#hpp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
              $('#lunas').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});                                                               $('#lr_unit').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
              $('#iris').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
    }

    function disabled(){
        $('#insentif').attr('disabled',true)
    }

    function enabled(){
        $('#insentif').attr('disabled',false)
    }


</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['kdprovinsi']['placeholder']);
                              echo form_dropdown( $form['kdprovinsi']['name'],
                                                  $form['kdprovinsi']['data'] ,
                                                  $form['kdprovinsi']['value'] ,
                                                  $form['kdprovinsi']['attr']);
                              echo form_error('kdprovinsi','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['kdkota']['placeholder']);
                              echo form_dropdown( $form['kdkota']['name'],
                                                  $form['kdkota']['data'] ,
                                                  $form['kdkota']['value'] ,
                                                  $form['kdkota']['attr']);
                              echo form_error('kdprovinsi','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['kdkec']['placeholder']);
                              echo form_dropdown( $form['kdkec']['name'],
                                                  $form['kdkec']['data'] ,
                                                  $form['kdkec']['value'] ,
                                                  $form['kdkec']['attr']);
                              echo form_error('kdprovinsi','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['kdlurah']['placeholder']);
                              echo form_input($form['kdlurah']);
                              echo form_error('kdlurah','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="row">
                      <div class="form-group">
                          <?php
                              echo form_label($form['nmlurah']['placeholder']);
                              echo form_input($form['nmlurah']);
                              echo form_error('nmlurah','<div class="note">','</div>');
                          ?>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#kdlurah').mask($('#kdprovinsi').val()+".99.99.9999");
        $('#kdkec').prop("disabled",true);

        getKdKota();
        getKdKec();


        $('#kdprovinsi').change(function(){
          getKdKota();
        });

        $('#kdkota').change(function(){
          $('#kdkec').prop("disabled",false);
          getKdKec();
        });

        if(!$('#kdlurah').val()){
            $('.btn-submit').click(function(){
                submit();
            });
        } else if($('#kdlurah').val()){
            $('.btn-submit').click(function(){
            update();
            });
        }



        //getKategori();
    });

    function getkdprovinsi(){
       //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("mstlok/getkdprovinsi");?>",
            data: {},
            beforeSend: function() {
                $('#kdprovinsi').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Provinsi --'));
                $("#kdprovinsi").trigger("change.chosen");
                $("#kdprovinsi").chosen({ width: '100%' });
                if ($('#kdprovinsi').hasClass("chosen-hidden-accessible")) {
                    $('#kdprovinsi').select2('destroy');
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdprovinsi')
                        .append($('<option>', { value : value.kode_provinsi })
                        .html("<b style='font-size: 14px;'>" + value.nmprov + " </b>"));
                });

                $('#kdprovinsi').select2({
                    placeholder: '-- Pilih Provinsi --',
                    dropdownAutoWidth : true,
                    width: '100%',
                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                  //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
                  //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmprov + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdKota(){
        var kdprov = $("#kdprovinsi").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("mstlok/getKdKota");?>",
            data: {"kdprov":kdprov},
            beforeSend: function() {
                $('#kdkota').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kota --'));
                $("#kdkota").trigger("change.chosen");
                if ($('#kdkota').hasClass("chosen-hidden-accessible")) {
                    $('#kdkota').select2('destroy');
                    $("#kdkota").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdkota')
                      .append($('<option>', { value : value.kdkota })
                      .html("<b style='font-size: 14px;'>" + value.nmkota + " </b>"));
                });

                $('#kdkota').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });



                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdKec(){
        var kdprov = $("#kdprovinsi").val();
        var kdkota = $("#kdkota").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("mstlok/getKdKec");?>",
            data: {"kdkota":kdkota,"kdprov":kdprov},
            beforeSend: function() {
                $('#kdkec').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kecamatan --'));
                $("#kdkec").trigger("change.chosen");
                if ($('#kdkec').hasClass("chosen-hidden-accessible")) {
                    $('#kdkec').select2('destroy');
                    $("#kdkec").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdkec')
                      .append($('<option>', { value : value.kdkec })
                      .html("<b style='font-size: 14px;'>" + value.nmkec + " </b>"));
                });

                $('#kdkec').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });



                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function submit(){
        var kdkota = $("#kdkota").val();
        var kdprovinsi = $("#kdprovinsi").val();
        var nmkota = $("#nmkota").val().toUpperCase();
        //alert(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstlok/submit");?>",
      		data: {"kdkota":kdkota,"nmkota":nmkota,"kdprovinsi":kdprovinsi},
      		beforeSend: function() {

      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              if (data.tipe==="success"){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      window.location.href = '<?=site_url('mstlok');?>';
                  });
              }else{
                  refresh();
              }
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function update(){
        var kdkota = $("#kdkota").val();
        var kdprovinsi = $("#kdprovinsi").val();
        var nmkota = $("#nmkota").val().toUpperCase();
        if ($("#faktif").prop("checked")){
          var faktif = 't';
        } else {
          var faktif = 'f';
        }
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstlok/update");?>",
      		data: {"kdkota":kdkota,"nmkota":nmkota,"kdprovinsi":kdprovinsi,"faktif":faktif},
      		beforeSend: function() {

      		},
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
              if (data.tipe==="success"){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                      window.location.href = '<?=site_url('mstlok');?>';
                  });
              }else{
                  refresh();
              }
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function refresh(){
      window.location.reload();
    }
</script>

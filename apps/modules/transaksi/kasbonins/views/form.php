<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    #modal_sub .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
        <div class="box box-danger">
            <!-- <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="col-xs-12 box-header box-view">
              <div class="col-xs-4"> 
                <button type="button" class="btn btn-primary btn-submit"> Simpan </button>
                <button type="button" class="btn btn-default btn-batal"> Batal </button> 
              </div>
              <div class="col-xs-2">
                <div class="row"> 
                </div>
              </div>
              <!-- <div class="col-xs-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak Nota</button>
                </div>
              </div> -->
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['nokasbon']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['nokasbon']);
                                echo form_error('nokasbon','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['tglkasbon']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['tglkasbon']);
                                echo form_error('tglkasbon','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                      <div class="col-xs-2 col-md-2 tagbbn">
                        <div class="form-group">
                          <?php echo form_label("Tagihan BBN"); ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                           <?php echo form_label($form['refkb']['placeholder']); ?>
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php
                              echo form_dropdown($form['refkb']['name'], $form['refkb']['data'],
                                                $form['refkb']['value'], $form['refkb']['attr']);
                              echo form_error('refkb','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                      <div class="col-xs-2 col-md-2 tagbbn">
                        <div class="form-group">
                          <?php echo form_label($form['notagbbn']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-3 col-md-3 tagbbn">
                        <div class="form-group">
                          <?php echo form_input($form['notagbbn']);
                                echo form_error('notagbbn','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                           <?php echo form_label($form['kdjkasbon']['placeholder']); ?>
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php
                              echo form_dropdown($form['kdjkasbon']['name'], $form['kdjkasbon']['data'], $form['kdjkasbon']['value'], $form['kdjkasbon']['attr']);
                              echo form_error('kdjkasbon','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                      <div class="col-xs-2 col-md-2 tagbbn">
                        <div class="form-group">
                          <?php echo form_label($form['tgltagbbn']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-3 col-md-3 tagbbn">
                        <div class="form-group">
                          <?php echo form_input($form['tgltagbbn']);
                                echo form_error('tgltagbbn','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['penerima']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['penerima']);
                                echo form_error('penerima','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['ket']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['ket']);
                                echo form_error('ket','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['nilai']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['nilai']);
                                echo form_error('nilai','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['tglpelaksanaan']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['tglpelaksanaan']);
                                echo form_error('tglpelaksanaan','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 
                    </div>
                  </div>

                  <!--KAS/BANK MASUK UMUM-->
                  <div class="kontribusi">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>Data Kontribusi</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-xs-12"> 
                    <p><a id="add" class="btn btn-primary btn-addkons"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-delkons"><i class="fa fa-minus"></i> Hapus</a></p>
                    <p id="kons_urut" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered DataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">CL</th>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Kontributor</th> 
                            <th style="text-align: center;">Catatan</th>  
                            <th style="text-align: center;">Nilai</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div>  

                </div>
          </div>

        </form>
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- modal dialog -->
<div id="modal_sub" class="modal fade">
    <div class="modal-dialog"  style="width: 75%;">
        <div class="modal-content">
            <form id="modalf" name="modalf" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Detail Kontribusi</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-12 col-md-12">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdleasing']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_dropdown($form['kdleasing']['name'], $form['kdleasing']['data'], $form['kdleasing']['value'], $form['kdleasing']['attr']);
                                          echo form_error('kdleasing','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-12 col-md-12">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['cat']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['cat']);
                                          echo form_error('cat','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-12 col-md-12">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['det_nilai']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['det_nilai']);
                                          echo form_error('det_nilai','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>     
                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
</div>


<?php echo form_close(); ?>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        autoNum();
        hide();
        // GetNoID(); 

        $('.btn-unset').hide();
        $('.btn-set').show();
        $('.kontribusi').hide();
        $('.tagbbn').hide();
        // $("#notbj").val('ABJ-____-____');   
        $(".btn-ctk").attr('disabled',true);   
        $("#refkb").val('');  
        $("#jnstrans").val('');    


        $('#refkb').change(function () { 
            // refkb(); 
            nokasbon();
        });    

        $('#notagbbn').keyup(function () {
            var jml = $('#notagbbn').val();
            var jml = jml.replaceAll("-","");
            var jml = jml.replaceAll("_","");
            // var jml = jml.replace("-","");
            // var nosin = nosin.replace("_","");
            var n = jml.length;
            // alert(n);
            if(n===11){ 
                set_tbj();   
            }
            // set_nosin();
        }); 

        $('#kdjkasbon').change(function(){
            jenis();
            // alert($('#kdjkasbon').val());
        }); 

        $('.btn-batal').click(function () { 
            batal();
        });   

        $('.btn-submit').click(function () {  
                submit(); 
        });  

        $('.btn-delkons').click(function () { 
            var nomor = table.row('.selected').data()['nomor']; 
            // alert(nomor);
            delDetail(nomor);
        });   

        $('.btn-addkons').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            r_modalkons();
            getKdleas();
            $('#det_nilai').autoNumeric('init');
            $('#modal_sub').modal('toggle');
            // autoNum_modal();
        }); 


        $('.btn-delkbk').click(function(){ 
            kbm_del();
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            // $("#nourut").val('');
            r_modal();
        });

        $('.btn-simpan').click(function () { 
            // console.log($('#modelf').valid());
            if ($("#modalf").valid()) { 
                tambah();
                // alert('1');
            }
        }); 
    });

    function reset(){    

      $('#nokasbon').val('BON-____-____');
      $('#tglkasbon').val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $('#refkb').val('');
      $('#kdjkasbon').val('');  
      $('#penerima').val('');    
      $('#ket').val('');    
      $('#nilai').val('');    
      $('#notagbbn').val('');    
      $('#tgltagbbn').val($.datepicker.formatDate('dd-mm-yy', new Date()));  
    }

    function autoNum(){    
          $('#nilai').autoNumeric('init',{ currencySymbol : 'Rp. '}); 
          $('#nilai_kont').autoNumeric('init',{ currencySymbol : 'Rp. '});    
    } 

    // no id
    function jenis(){ 
        //alert(kddiv);
        var kdjkasbon = $('#kdjkasbon').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/jenis");?>",
            data: {"kdjkasbon":kdjkasbon},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){

                    if(value.isbbn==='t'){
                      $('.kontribusi').hide();
                      $('.tagbbn').show(); 
                      $('#notagbbn').mask('TBJ-9999-9999');
                      $('#ket').prop('disabled',true);
                      $('#nilai').prop('disabled',true);
                      $('#tgltagbbn').prop('disabled',true); 
                    } else {
                      $('.kontribusi').show();
                      $('.tagbbn').hide();
                      $('#notagbbn').val('');
                      $('#ket').prop('disabled',false);
                      $('#nilai').prop('disabled',false); 
                      $('#tgltagbbn').prop('disabled',true);
                      tbl();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    } 

    function day(tgl){
      var n = tgl.getDay();
      // var n = new Date(tgl);
      // alert(n);
      if(n>=6){
        var nd = 'SABTU';
      } else if(n>=5){
        var nd = 'JUMAT';
      } else if(n>=4){
        var nd = 'KAMIS';
      } else if(n>=3){
        var nd = 'RABU';
      } else if(n>=2){
        var nd = 'SELASA';
      } else if(n>=1){
        var nd = 'SENIN';
      } else if(n>=0){
        var nd = 'MINGGU';
      }
      $('#day').val(nd);
    }

    function hide(){
          $('.pkons').hide();
          $('.pleas').hide();
          $('.rleas').hide();
          $('.psub').hide();
          $('.kbm_umum').hide();
          $('.balik_kons').hide();
          $('.kbk_umum').hide();
          $('.rkasbon').hide();
    } 

    function tbl(){

        //
        var column = []; 

        column.push({
          "aTargets":  0 ,
          "searchable": false,
          "orderable": false,
          "checkboxes": {
            'selectRow': true
          }

        });

        column.push({
            "aTargets": [ 4 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 


        // column.push({
        //     "aTargets": [ 9 ],
        //     "mRender": function (data, type, full) {
        //         return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
        //     },
        //     "sClass": "center"
        // });  

        table = $('.DataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nomor"},
                { "data": "no"},
                { "data": "kdleasing" },
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('kasbonins/det_json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },  
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.DataTable').DataTable().page.info();
                table.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.DataTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });
    }  

    function r_modalkons(){
          $('#kdleasing').val('');  
          $("#kdleasing").trigger("chosen:updated");
          $('#kdleasing').select2({
              dropdownAutoWidth : true,
              width: '100%'
          });
          $('#cat').val('');  
          $('#det_nilai').val('');   

    } 

    function c_kons(ket){ 
      //detail pembayaran konsumen 
          $('#kdleasing').val('');  
          $('#cat').val('');    
          //nominal pembayaran konsumen
          $('#det_nilai').autoNumeric('set',0);   
    }

    function set_tbj(){   
        var notbj = $("#notagbbn").val();  
        // alert(count);
        $.ajax({
            type: "POST",
            url: "<?=site_url('kasbonins/set_tbj');?>",
            data: { "notbj":notbj},
            success: function(resp){
              // $("#modal_sub").modal("hide"); 
              if(resp==='"empty"'){
                  swal({
                    title: 'No. Tagihan '+notbj+' tidak ditemukan atau sudah pernah dilakukan Kasbon',
                    text: '',
                    type: 'error'
                  },function (){
                    $('#notagbbn').val('');
                  });
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                  // $('#ket').show();  
                      // alert(data.nobbn_trm);  
                        $("#tgltagbbn").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglbbn_trm))); 
                        $('#ket').val(data.ket);
                        $('#nilai').autoNumeric('set',data.total);
                  });
              } 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    function nokasbon(){   
      var refkb = $("#refkb").val(); 
        $.ajax({
            type: "POST",
            url: "<?=site_url('kasbonins/nokasbon');?>",
            data: {"refkb":refkb},
            success: function(resp){ 
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                        $("#nokasbon").val(data.get_nokasbon);  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

  function tambah(){   ; 
      var kdleasing = $("#kdleasing").val(); 
      var cat = $("#cat").val().toUpperCase();   
      var det_nilai = $("#det_nilai").autoNumeric('get');   
            $.ajax({
                type: "POST",
                url: "<?=site_url("kasbonins/tambah");?>",
                data: {"kdleasing":kdleasing
                        ,"cat":cat
                        ,"det_nilai":det_nilai  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                        if(obj.state=1){ 
                                $("#modal_sub").modal("hide"); 
                                 tbl(); 
                        }else{ 
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        } 
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
  } 

    function delDetail(nomor){     
       
        $.ajax({
            type: "POST",
            url: "<?=site_url('kasbonins/delDetail');?>",
            data: {"nomor":nomor}, 
            success: function(resp){
                var obj = JSON.parse(resp);
                    if(obj.state=1){ 
                        table.ajax.reload(); 
                    }else{ 
                        swal({
                            title: 'Data Gagal Disimpan',
                            text: obj.msg,
                            type: 'error'
                        }); 
                    } 
            },
            error: function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        }); 
    }

    function batal(){ 
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
            // var jnstrans = $("#jnstrans").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kasbonins/batal");?>",
                data: { },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                      swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                      }, function() {
                        window.location.href = '<?=site_url('kasbonins');?>';  
                      }); 
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
            }); 
    }

    function submit(){
        var tglkasbon     = $("#tglkasbon").val();
        var nilai         = $("#nilai").autoNumeric('get'); 
        var refkb         = $("#refkb").val(); 

        var penerima          = $("#penerima").val().toUpperCase(); 
        var ket           = $("#ket").val().toUpperCase(); 
        var tglpks        = $("#tglpelaksanaan").val(); 
        // alert(nama);
        var kdjkasbon     = $("#kdjkasbon").val();
        var notagbbn      = $("#notagbbn").val(); 

        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () { 
            $.ajax({
                type: "POST",
                url: "<?=site_url("kasbonins/submit");?>",
                data: {"tglkasbon":tglkasbon
                        ,"nilai":nilai
                        ,"refkb":refkb
                        ,"penerima":penerima
                        ,"ket":ket
                        ,"tglpks":tglpks
                        ,"kdjkasbon":kdjkasbon
                        ,"notagbbn":notagbbn},
                success: function(resp){ 
                  var obj = jQuery.parseJSON(resp);
                  $.each(obj, function(key, data){
                      // alert(data.tipe); 
                      var nokasbon = data.nokasbon;
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){ 
                                window.location.href = '<?=site_url('kasbonins');?>';  
                            }); 
                  });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }  

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    }  

    function getKdleas(){ 
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/getKdleas");?>",
            data: {},
            beforeSend: function() {
                $('#kdleasing').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Leasing --'));
                $("#kdleasing").trigger("change.chosen");
                if ($('#kdleasing').hasClass("chosen-hidden-accessible")) {
                    $('#kdleasing').select2('destroy');
                    $("#kdleasing").chosen({ width: '100%' });
                } 
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdleasing')
                      .append($('<option>', { value : value.kdleasing })
                      .html("<b style='font-size: 12px;'>" + value.kdleasing + " </b>")); 
                });

                $('#kdleasing').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
</script>

<style type="text/css">
	.nopadding {
		padding: 0 !important;
		margin: 0 !important;
		border: none;
	}
	.box{
		border-radius: 0px;
		padding-left: 7px;
		padding-right: 7px;
		border: 1px solid #d2d6de;
		margin-bottom: 0px;
	}
	.content-header{
		display: none;
	}
	
	.content {
		min-height: 250px;
		padding: 0px;
		margin-right: auto;
		margin-left: auto;
		padding-left: 15px;
		padding-right: 15px;
	}
	.input-sm{
		height: 20px;
	}
	
	.table>tbody>tr>td
	, .table>tfoot>tr>td
	, .table>thead>tr>td
	{
		padding: 3px 8px;
	}
	.table{
		font-size: 12px;
	}
</style>
<!-- Main row -->
<div class="row">
	<!--
	<div class="box nopadding" style="border-top: 0px">
		<div class="box-body">
			<div class="col-lg-3">
				<div class="input-group input-group-sm">
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i> <?=$form['periode_akhir']['placeholder'];?>
					</span>
					<?php 
						echo form_input($form['periode_akhir']);
						echo form_error('periode_akhir','<div class="note">','</div>'); 
					?>
					<span class="input-group-btn">
						<button class="btn btn-primary btn-preview">
							Proses
						</button>
					</span>
				</div>      
			</div>
			<div class="col-lg-10">
				
			</div>
		</div>
	</div>   
	--> 
					<?php 
						echo form_input($form['periode_akhir']);
					?>

	<div class="col-lg-6 nopadding" style="display: none;">
		<div class="box box-danger">
			<div class="box-body">
				<div class="row"> 
					<div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
						<div id="" style="height: 450px; overflow-y: auto;overflow-x: hidden;padding: 10px 10px;" align="center">
							<table class="table table-hover table-bordered table-top-sales" style="width: 98%;font-size: 12px;margin-top: 0px !important;">
								<caption style="text-align: center; font-weight: 700;color: #ff6363;font-size: 12px;padding-top: 0px;" class="cap-sales-mtd">
									<div>
										<label class="label label-primary">Sales Productivity</label>
										<label class="label label-success">Counter Productivity</label>
									</div>
									Penjualan Per Sales MTD
								</caption>
								<thead>
									<tr>
										<th  style="width:10px;">No.</th>
										<th>Nama Sales</th>
										<th style="width: 25px;">Jml</th>
									</tr>
								</thead>
								<tfoot class="footSalesMTD"></tfoot>
								<tbody class="bodySalesMTD"></tbody>
								<tfoot class="footSalesMTD"></tfoot>
							</table>
						</div>                        
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-12 nopadding">
		<div class="box box-danger">
			<!-- <h2 class="box-title">Stok Aktual</h2> -->
			<div class="box-body">
				<div class="row">  
					<div class="col-lg-12 nopadding" style="border-bottom: solid 1px #ddd;">
						<div id="" style="height: 480px; overflow-y: auto;overflow-x: hidden;padding: 10px 10px;" align="center">
							<table class="table table-hover table-bordered table-stok-aktual" style="width: 98%;font-size: 12px;margin-top: 0px !important;">
								<!--
 								<caption style="text-align: center; font-weight: 700;font-size: 15px;color: #ff6363;padding-top: 0px;" class="cap-stok-mtd">
									Stok Aktual
								</caption>
								-->
								<thead>
									<tr>
										<th style="width:10px;">No.</th>
										<th>Tipe Unit</th>
										<th style="width: 25px;">Qty</th>
									</tr>
								</thead>
								<tfoot class="footStokMTD"></tfoot>
								<tbody class="bodyStokMTD"></tbody>
								<tfoot class="footStokMTD"></tfoot>
							</table>
						</div>             
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row (main row) -->

<!-- Modal Chart -->
<div id="chart-lv-1" class="modal fade" role="dialog" style="overflow-y: scroll;">
	<div class="modal-dialog" style="width: 99%;">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title title-lv-1">Title</h4>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<input type="hidden" id="nm" name="nm" />
			<input type="hidden" id="tahun" name="tahun" />
			<input type="hidden" id="func" name="func" />
			<div id="divLevel1a" class="col-lg-8" style="min-height:250px;">
				<div class="row">
					<div class="col-lg-12">
						<canvas id="chartLevel1" style="height:250px;"></canvas>
					</div>  
					<div class="col-lg-12 dataLevel1"></div>
				</div>
			</div>
			<div id="divLevel1b"  class="col-lg-4" style="min-height:250px;">
				<div class="table-responsive" style="width: 100%;">
					<div class="tableLevel1"></div>
				</div>
			</div>
			<div class="col-lg-12"><hr></div>
			<div id="divLevel2a" class="col-lg-8">
				<canvas id="chartLevel2" style="height:250px;"></canvas>
			</div>
			<div id="divLevel2b" class="col-lg-4">
				<div class="tableLevel2"></div>
			</div>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	  </div>
	</div>

  </div>
</div>

<!-- Modal Detail Stok-->
<div id="detail-stok" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 99%;">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title title-detail-stok">Detail Stok</h4>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-3">
				<table class="table table-hover table-bordered table-stok-aktual-perwarna">
					<caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-stok-perwarna">
						Stok Aktual Per Warna MTD 
					</caption>
					<thead>
						<tr>
							<th style="width:10px;">Kode</th>
							<th >Warna</th>
							<th style="width:10px;">Jumlah</th>
						</tr>
					</thead>
					<tbody class="bodyStokPerWarna"></tbody>
				</table>
			</div>
			<div class="col-lg-9" style="border-left: 1px solid #f4f4f4;">
				<div class="table-responsive" align="center">
					<table class="table table-hover table-bordered table-stok-aktual-pertipe">
						<caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-stok-pertipe">
							Stok Aktual MTD 
						</caption>
						<thead>
							<tr>
								<th style="width:5px;">No.</th>
								<th style="width:5px;">Kode</th>
								<th style="width:50px;">Warna</th>
								<th style="width:30px;">Tgl Terima</th>
								<th style="width:10px;">Umur (Hari)</th>
								<th style="width:50px;">Nomor Mesin</th>
								<th style="width:50px;">Nomor Rangka</th>
								<th>Lokasi</th>
							</tr>
						</thead>
						<tbody class="bodyStokPerTipe"></tbody>
					</table>
				</div> 
			</div>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	  </div>
	</div>

  </div>
</div>

<!-- Modal Detail Gross Profit-->
<div id="detail-gp" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 99%;">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title title-detail-gp">Detail Stok</h4>
	  </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-hover table-bordered table-detail-gp">
					<caption style="text-align: center; font-weight: 700;color: #ff6363;" class="cap-detail-gp">
						GROSS PROFIT DETAIL PERIODE
					</caption>
					<thead>
						<tr>
							<th style="width:45px;">Tgl Beli</th>
							<th >Nama Konsumen</th>
							<th style="width:10px;">T/K</th>
							<th >Tipe</th>
							<th >Leasing</th>
							<th style="width: 45px;">Prog. Leasing</th>
							<th style="width: 10px;">DP Gross</th>
							<th style="width: 10px;">DP Riil</th>
							<th style="width: 10px;">Angsuran</th>
							<th style="width: 10px;">Tenor /Bulan</th>
							<th style="width: 10px;">JP Leasing</th>
							<th style="width: 10px;">Gross Profit</th>
						</tr>
					</thead>
					<tbody class="body-detail-gp"></tbody>
				</table>
			</div>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	  </div>
	</div>

  </div>
</div>

<!-- ChartJS -->
<script src="<?=base_url('assets/plugins/chartjs/Chart.bundle.js');?>"></script>
<script src="<?=base_url('assets/plugins/chartjs/utils.js');?>"></script>
<!-- FastClick -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<script>
	$( document ).ready(function() {
  	$('#periode_akhir').hide();
  });

	$(function () {
		setInterval(function(){  
			getChart();
		}, 1800000);
		$('#periode_akhir').datepicker({
			startView: "year", 
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"      
		});
		getChart();
		$('.btn-preview').click(function(){
			getChart();
		});

		function getChart(){
			var periode = $('#periode_akhir').val();
			getJualPerSalesAktual(periode);
			getStokPerTipeAktual(periode);
		}

		$('#periode_akhir').on("changeDate", function(e) {
			getChart();
		});
		$('.overlay').hide();
		// Define a plugin to provide data labels
		Chart.plugins.register({
			beforeDraw: function (chart, easing) {
				if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
						var helpers = Chart.helpers;
						var ctx = chart.chart.ctx;
						var chartArea = chart.chartArea;

						ctx.save();
						ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
						ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
						ctx.restore();
				}
			},
			afterDatasetsDraw: function(chart, easing) {
				// To only draw at the end of animation, check for easing === 1
				var ctx = chart.ctx;

				chart.data.datasets.forEach(function (dataset, i) {
					var meta = chart.getDatasetMeta(i);
					if (!meta.hidden) {
						if(meta.type!=="line"){
							meta.data.forEach(function(element, index) {
								// Draw the text in black, with the specified font
								ctx.fillStyle = 'rgb(0, 0, 0)';

								var fontSize = 11;
								var fontStyle = 'normal';
								var fontFamily = 'Source Sans Pro';
								ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

								// Just naively convert to string for now
								var dataString = dataset.data[index].toString();

								// Make sure alignment settings are correct
								ctx.textAlign = 'center';
								ctx.textBaseline = 'middle';

								var padding = 5;
								var position = element.tooltipPosition();
								ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
							});    
						}
					}
				});
			} 
		}); 
		
		$("#chart-lv-1").on('hidden.bs.modal', function(){
			
			$('#divLevel1a').show();
			$('#divLevel1a').removeClass('col-lg-8 col-lg-12');
			$('#divLevel1a').addClass('col-lg-8');
			
			$('#divLevel1b').show();
			$('#divLevel1b').removeClass('col-lg-4 col-lg-12');
			$('#divLevel1b').addClass('col-lg-4');
			
			
			$('#divLevel2a').show();
			$('#divLevel2a').removeClass('col-lg-8 col-lg-12');
			$('#divLevel2a').addClass('col-lg-8');
			
			$('#divLevel2b').show();
			$('#divLevel2b').removeClass('col-lg-4 col-lg-12');
			$('#divLevel2b').addClass('col-lg-4');
			
			$(".dataLevel1").html('');
			$(".tableLevel2").html('');
			if(typeof mychartLevel2 != 'undefined' ){
				mychartLevel2.destroy();
			}    
		});
		$("#chart-lv-1").on('shown.bs.modal', function(){
			var nm = $("#nm").val();
			var tahun = $("#tahun").val();
			var func = $("#func").val();  
		});

		$("#detail-stok").on('shown.bs.modal', function(){
			var periode = $('#periode_akhir').val();
			var kode = $(".title-kode-stok").html();

			if ( $.fn.DataTable.isDataTable('.table-stok-aktual-pertipe') ) {
			  $('.table-stok-aktual-pertipe').DataTable().destroy();
			}    

			if ( $.fn.DataTable.isDataTable('.table-stok-aktual-perwarna') ) {
			  $('.table-stok-aktual-perwarna').DataTable().destroy();
			}   
			
			if ( $.fn.DataTable.isDataTable('.table-detail-jual') ) {
			  $('.table-detail-jual').DataTable().destroy();
			}
			$.ajax({
				type: "POST",
				url: "<?=site_url('dashboard/getDetailStok');?>",
				data: {"periode":periode
						,"kode":kode},
				success: function(resp){  
					var obj = jQuery.parseJSON(resp);

					$(".cap-stok-perwarna").html("Stok Aktual Per Warna MTD  " + periode);
					$(".bodyStokPerWarna").html('');
					$.each(obj.jmlwarna, function(key, data){
						  $(".bodyStokPerWarna").append('<tr>' + 
													  '<td style="text-align: left;">'+
														  data.kdwarna+
													  '</td>' + 
													  '<td style="text-align: left;">'+
														  data.nmwarna+
													  '</td>' + 
													  '<td style="text-align: right;">'+
														  data.jml+
													  '</td>' + 
												  '</tr>');
					});  

					var no = 1;
					$(".cap-stok-pertipe").html("Stok Aktual Per Warna Dan Lokasi MTD " + periode);
					$(".bodyStokPerTipe").html('');
					$.each(obj.detail, function(key, data){
						  $(".bodyStokPerTipe").append('<tr>' + 
													  '<td style="text-align: center;">'+no+'</td>' + 
													  '<td style="text-align: left;">'+
														  data.kdwarna+
													  '</td>' + 
													  '<td style="text-align: left;">'+
														  data.nmwarna+
													  '</td>' + 
													  '<td style="text-align: left;">'+
														  moment(data.tglterima).format('L')+
													  '</td>' + 
													  '<td style="text-align: right;">'+
														  data.umur+
													  '</td>' + 
													  '<td style="text-align: left;">'+
														  data.nosin+
													  '</td>' + 
													  '<td style="text-align: left;">'+
														  data.nora+
													  '</td>' + 
													  '<td style="text-align: left;">'+
														  data.nmlokasi+
													  '</td>' + 
												  '</tr>');
						  no++;
					});     
					$(".table-stok-aktual-pertipe").DataTable({
						  "lengthMenu": [[-1], ["Semua Data"]],
						  "bProcessing": false,
						  "bServerSide": false,
						  "bDestroy": true,
						  "bAutoWidth": false,
						  "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right' >r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'>> "
					  });   
					$(".table-stok-aktual-perwarna").DataTable({
						  "lengthMenu": [[-1], ["Semua Data"]],
						  "bProcessing": false,
						  "bServerSide": false,
						  "bDestroy": true,
						  "bAutoWidth": false,
						  "sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right' >r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'>> "
					  });
				},
				error:function(event, textStatus, errorThrown) {
					console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				}
			});
		});
	});
	
	// Detail Stok Start
	function getDetailStok(kode,nama){
		$("#detail-stok").modal({
			"show":true,
			"backdrop":"static"
		});
		$(".title-detail-stok").html(nama+' <small class="title-kode-stok">'+kode+'</small>');
	}
	// Detail Stok End

	// Tabel untuk penjualan per sales aktual start
	function getJualPerSalesAktual(tahun){
		  if ( $.fn.DataTable.isDataTable('.table-top-sales') ) {
			$('.table-top-sales').DataTable().destroy();
		  } 
		$.ajax({
			type: "POST",
			url: "<?=site_url('dashboard/getJualPerSalesAktual');?>",
			data: {"tanggal":tahun},
			success: function(resp){  
				var obj = jQuery.parseJSON(resp);
				var no = 1;
				var totalJualSL = 0;
				var totalJualCS = 0;
				
				var totalSL = 0;
				var totalCS = 0;
				var totalAll = 0;
				$(".bodySalesMTD").html('');
				$.each(obj.jual, function(key, data){
					  $(".bodySalesMTD").append('<tr>' + 
												  '<td style="text-align: center;">'+no+'</td>' + 
												  '<td style="text-align: left;">'+data.nmsales+'</td>' + 
												  '<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
											  '</tr>');
						if(data.status==="CS"){
							totalJualCS = Number(totalJualCS) + Number(data.jml);                            
						}else{
							totalJualSL = Number(totalJualSL) + Number(data.jml);
						}
						totalAll = Number(totalAll) + Number(data.jml);     
					  no++;
				});     
				$(".footSalesMTD").html('<tr>' + 
												'<td style="text-align: center;"></td>' + 
												'<td style="text-align: center;"><b>TOTAL</b></td>'+
												'<td style="text-align: right;"><b>'+numeral(totalAll).format('0,0')+'</b></td>' + 
											'</tr>');
				$(".table-top-sales").DataTable({
					"lengthMenu": [[-1], ["Semua Data"]],
					"bProcessing": false,
					"bServerSide": false,
					"bDestroy": true,
					"bAutoWidth": false,
					"sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
				});
				
				$.each(obj.jmlsales, function(key, data){
					if(data.status==="CS"){
						totalCS = Number(totalCS) + Number(data.jml);                            
					}else{
						totalSL = Number(totalSL) + Number(data.jml);
					}
				});
				
				var avgCS = (Number(totalJualCS)/Number(totalCS));
				var avgSL = (Number(totalJualSL)/Number(totalSL));
				
				$(".cap-sales-mtd").html('<div style="font-size:13px;">' +
												' <label class="label label-primary">Sales Productivity '+avgSL.toFixed(1)+'</label> ' +
												' | <label class="label label-success">Counter Productivity '+avgCS.toFixed(1)+'</label> ' +
											'</div>' +
											'Penjualan Per Sales MTD : ' + tahun);
			},
			error:function(event, textStatus, errorThrown) {
				console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			}
	  });
	}
	// Tabel untuk penjualan per sales aktual end

	// Tabel untuk stok per tipe aktual start
	function getStokPerTipeAktual(tahun){
	  if ( $.fn.DataTable.isDataTable('.table-stok-aktual') ) {
		$('.table-stok-aktual').DataTable().destroy();
	  }    
	  $.ajax({
		  type: "POST",
		  url: "<?=site_url('dashboard/getStokPerTipeAktual');?>",
		  data: {"tanggal":tahun},
		  success: function(resp){  
			  var obj = jQuery.parseJSON(resp);
			  var no = 1;
			  //$(".cap-stok-mtd").html("Stok Aktual MTD " + tahun);
			  $(".bodyStokMTD").html('');
			  var totalAll = 0;
			  $.each(obj, function(key, data){
					$(".bodyStokMTD").append('<tr>' + 
												'<td style="text-align: center;">'+no+'</td>' + 
												'<td style="text-align: left;">'+
													data.nmtipegrp+
													' <small style="font-size: 12px;float: right;"><label onclick="getDetailStok(\''+data.nmtipegrp+'\',\''+data.nmtipe+'\')" class="label label-info">Detail</label></small>'+
													'<br><small style="font-size: 10px;">'+data.nmtipe+'</small>'+
												'</td>' + 
												'<td style="text-align: right;">'+numeral(data.jml).format('0,0')+'</td>' + 
											'</tr>');
					totalAll = Number(totalAll) + Number(data.jml);                 
					no++;
			  });            
				$(".footStokMTD").html('<tr>' + 
												'<td style="text-align: center;"></td>' + 
												'<td style="text-align: right;"><b>TOTAL</b></td>'+
												'<td style="text-align: right;"><b>'+numeral(totalAll).format('0,0')+'</b></td>' + 
											'</tr>');
			  $(".table-stok-aktual").DataTable({
					"lengthMenu": [[-1], ["Semua Data"]],
					"oLanguage": {
						"url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
					},
					"bProcessing": false,
					"bServerSide": false,
					"bDestroy": true,
					"bAutoWidth": false,
					"sDom": "<'row'<'col-sm-6' f><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
				});
		  },
		  error:function(event, textStatus, errorThrown) {
			  console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		  }
	});
	}
	// Tabel untuk stok per tipe aktual end
</script>
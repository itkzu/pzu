<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptbb
 *
 * @author adi
 */
class Rptbb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptbb/submit'),
            'add' => site_url('rptbb/add'),
            'edit' => site_url('rptbb/edit'),
            'reload' => site_url('rptbb'),
        );
        $this->load->model('rptbb_qry');
        $akun = $this->rptbb_qry->getAkun();
        foreach ($akun as $value) {
            $this->data['akun'][$value['kdakun']] = $value['kdakun']." - ".$value['nmakun'];
        }
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $this->data['rptakun'] = $this->rptbb_qry->submit();
        if(empty($this->data['rptakun']['akun_h'])){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('debug/err_000',$this->data);
        }else{
            $array = $this->input->post();
            if($array['submit']==="html" || $array['submit']==="excel"){
                $this->template
                    ->title($this->data['msg_main'],$this->apps->name)
                    ->set_layout('print-layout')
                    ->build('html',$this->data);            
            }else{
                redirect("rptbb");            
            }    
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            'akun_multi'=> array(
                    'attr'        => array(
                        'id'    => 'akun_multi',
                        'class' => 'form-control',
                        'multiple' => '',
                    ),
                    'data'     => $this->data['akun'],
                    'value'    => '',
                    'name'     => 'akun_multi[]',
            ),
            'akun_awal'=> array(
                    'attr'        => array(
                        'id'    => 'akun_awal',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['akun'],
                    'value'    => '',
                    'name'     => 'akun_awal',
            ),
            'akun_akhir'=> array(
                    'attr'        => array(
                        'id'    => 'akun_akhir',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['akun'],
                    'value'    => '',
                    'name'     => 'akun_akhir',
            ),
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => $this->ddmmyyyy,
                    'class'       => 'form-control',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => $this->last_ddmmyyyy,
                    'class'       => 'form-control',
            ),
        );
    }
}

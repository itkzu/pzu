<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Lrnew_qry
 *
 * @author adi
 */
class Lrnew_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function set_nodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vm_labarugi where nodo =  '".$nodo."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "";
      }
      return json_encode($res);
    }

    public function refund_dpp() {
      $total = $this->input->post('total');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from pzu.refund_dpp(".$total.",'".$noso."')");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "";
      }
      return json_encode($res);
    }

    public function select_data() {
        $nodo = $this->uri->segment(3);
        // $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vm_labarugi");
        $res = $q->result_array();
        return $res;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.get_divisi()");
        return $q->result_array();
    }

    public function getkdsalesins() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.sales_ins");
        return $q->result_array();
    }

    public function ctk_labarugi($nodo) {
        $this->db->select("*");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vb_labarugi");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function sales_ins() {
        $insentif = $this->input->post('insentif');
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
        $this->db->where('kdsales_ins',$insentif);
        $q = $this->db->get("pzu.sales_ins");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function insentif() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.perusahaan");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    //cetak
    public function ctk_um($nodo) {
        $this->db->select("*");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vb_kwitansi_um");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

  public function json_dgview() {
      error_reporting(-1);
      if( isset($_GET['nodo']) ){
          $nodo = $_GET['nodo'];
      }else{
          $nodo = '';
      }

      $aColumns = array('no',
                              'nodo'      ,
                              'tgldo'     ,
                              'noso'      ,
                              'tglso'	    ,
                              'nama'      ,
                              'nama_s'    ,
                              'alamat_s'  ,
                              'nmleasingx',
                              'nmprogleas',
                              'kode'      ,
                              'kdtipe'    ,
                              'nmtipe'    ,
                              'nmwarna'   ,
                              'nosin2'    ,
                              'nora2'     ,
                              'nmsales'   );
$sIndexColumn = "nodo";

      $sLimit = "";
      if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
          $sLimit = " LIMIT " . $_GET['iDisplayLength'];
      }
      $sTable = " ( SELECT ''::varchar as no, nodo,	tgldo,	noso,	tglso,	nama,	nama_s,	alamat_s,	nmleasingx,	nmprogleas,	kode,	kdtipe,	nmtipe,	nmwarna,	nosin2,	nora2, nmsales
         					    FROM pzu.vl_do WHERE progres=0 ORDER BY tgldo ) AS a";
      if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
      {
          if($_GET['iDisplayStart']>0){
              $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                      intval( $_GET['iDisplayStart'] );
          }
      }

      $sOrder = "";
      if ( isset( $_GET['iSortCol_0'] ) )
      {
              $sOrder = " ORDER BY  ";
              for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
              {
                      if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                      {
                              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                      ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                      }
              }

              $sOrder = substr_replace( $sOrder, "", -2 );
              if ( $sOrder == " ORDER BY" )
              {
                      $sOrder = "";
              }
      }
      $sWhere = "";

      if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
      {
  $sWhere = " Where (";
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  }
  $sWhere = substr_replace( $sWhere, "", -3 );
  $sWhere .= ')';
      }

      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
              if ( $sWhere == "" )
              {
                  $sWhere = " WHERE ";
              }
              else
              {
                  $sWhere .= " AND ";
              }
              //echo $sWhere."<br>";
              $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
      }


      /*
       * SQL queries
       * QUERY YANG AKAN DITAMPILKAN
       */
      $sQuery = "
              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
              FROM   $sTable
              $sWhere
              $sOrder
              $sLimit
              ";

      //echo $sQuery;

      $rResult = $this->db->query( $sQuery);

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";    //SELECT FOUND_ROWS()

      $rResultFilterTotal = $this->db->query( $sQuery);
      $aResultFilterTotal = $rResultFilterTotal->result_array();
      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";
      $rResultTotal = $this->db->query( $sQuery);
      $aResultTotal = $rResultTotal->result_array();
      $iTotal = $aResultTotal[0]['jml'];

      $output = array(
              "sEcho" => intval($_GET['sEcho']),
              "iTotalRecords" => $iTotal,
              "iTotalDisplayRecords" => $iFilteredTotal,
              "aaData" => array()
      );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                }


                $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('lrnew/edit/'.$aRow['nodo'])."\">Edit</a>";

                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    }

    public function save() {
        $nodo         = $this->input->post('nodo');
        $disc         = $this->input->post('disc');
        $sud_dealer   = $this->input->post('sud_dealer');
        $sub_md       = $this->input->post('sub_md');
        $sub_ahm      = $this->input->post('sub_ahm');
        $sub_leas     = $this->input->post('sub_leas');
        $sub_hso      = $this->input->post('sub_hso');
        $ins_leasing  = $this->input->post('ins_leasing');
        $ins_ai       = $this->input->post('ins_ai');
        $harga_beli   = $this->input->post('harga_beli');
        $iris         = $this->input->post('iris');
        $bbn          = $this->input->post('bbn');
        $komisi       = $this->input->post('komisi');
        $topcover     = $this->input->post('topcover');
        $jaket        = $this->input->post('jaket');
        $ins_sales    = $this->input->post('ins_sales');
        $biops        = $this->input->post('biops');
        $jp           = $this->input->post('jp');
        $matriks      = $this->input->post('matriks');
        $kdsales_ins  = $this->input->post('kdsales_ins');
        $sales_ins    = $this->input->post('sales_ins');
        $q = $this->db->query("select title,msg,tipe from pzu.lr_ins_w( '". $nodo ."',
                                                                        ". $disc .",
                                                                        ". $sud_dealer .",
                                                                        ". $sub_md .",
                                                                        ". $sub_ahm .",
                                                                        ". $sub_leas .",
                                                                        ". $sub_hso .",
                                                                        ". $ins_leasing .",
                                                                        ". $ins_ai .",
                                                                        ". $harga_beli .",
                                                                        ". $iris .",
                                                                        ". $bbn .",
                                                                        ". $komisi .",
                                                                        ". $topcover .",
                                                                        ". $jaket .",
                                                                        ". $ins_sales .",
                                                                        ". $biops .",
                                                                        ". $jp .",
                                                                        ". $matriks .",
                                                                        ". $kdsales_ins .",
                                                                        ". $sales_ins .",
                                                                        '".$this->session->userdata("username")."')");
        echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style>
      caption {
          padding-top: 8px;
          padding-bottom: 8px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto;
          font-family: verdana;
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }

      @media print {
        footer {page-break-after: always;}
      }
  </style>
  <?php 

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 

  $namaheader = array(
      array('data' => '&nbsp; PT. PRIMA ZIRANG UTAMA'
                          , 'colspan' => 13
                          , 'style' => 'text-align: left; width: 55%; font-size: 12px;'),
      array('data' => '&nbsp; <b>FAKTUR PENJUALAN<b>'
                          , 'colspan' => 11
                          , 'rowspan' => 2
                          , 'style' => 'text-align: center; width: 45%; font-size: 24px;'),  
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);    
  $header1 = array(
      array('data' => '&nbsp; '.$data[0]['alamat_div']
                          , 'colspan' => 14
                          , 'style' => 'text-align: left; width: 55%; font-size: 12px;'),  
  );
  $this->table->add_row($header1);
  $header2 = array(
      array('data' => '&nbsp; '.$data[0]['notelp2']
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),  
  );
  $this->table->add_row($header2);
  $header3 = array(
      array('data' => '&nbsp; NPWP : 02.511.814.2-512.000'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; width: 100%; font-size: 12px;'),  
  );
  $this->table->add_row($header3); 

  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" rules="none" border="3px" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();    

  $body1 = array(
      array('data' => '&nbsp; <b>Kepada Yth :</b>'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; font-size: 12px;'),  
  );
  $this->table->add_row($body1); 
  $body2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 15
                          , 'style' => 'text-align: left; width: 54%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 8 
                          , 'style' => 'text-align: left; width: 45%; font-size: 12px; border-top: 3px solid #000; border-right: 3px solid #000; border-left: 3px solid #000;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body2); 
  $body3 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => 'Nama'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ': '.$data[0]['nama']
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;  border-left: 3px solid #000;'),    
      array('data' => '&nbsp; <b>No. Faktur</b>'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp; <b>Tanggal</b>'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px; border-right: 3px solid #000'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body3); 
  $body4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => 'Alamat'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ': '.$data[0]['alamat']
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 8 
                          , 'style' => 'text-align: left; width: 45%; font-size: 12px; border-right: 3px solid #000; border-left: 3px solid #000;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),   
  );
  $this->table->add_row($body4); 
  $body5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 15
                          , 'style' => 'text-align: center; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;  border-left: 3px solid #000;'),    
      array('data' => '&nbsp; DO  : '.$data[0]['nodo']
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp; '.date_format(date_create($data[0]['tgldo']),"d/m/Y") 
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px; border-right: 3px solid #000'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body5); 
  $body6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => 'Kota'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ': '.$data[0]['kota']
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 8 
                          , 'style' => 'text-align: left; width: 45%; font-size: 12px; border-right: 3px solid #000; border-left: 3px solid #000;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body6); 
  $body7 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => 'No. Telp'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ': '.$data[0]['notelp']
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;  border-left: 3px solid #000;'),    
      array('data' => '&nbsp; SPK : '.$data[0]['noso']
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px; border-right: 3px solid #000'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
    $this->table->add_row($body7); 
    if($data[0]['npwp']==='00.000.000.0-000.000'){
      $npwp = '';
    } else if($data[0]['npwp']==='.   .   . -   .'){
      $npwp = '';
    } else if($data[0]['npwp']===''){
      $npwp = '';
    } else {
      $npwp = $data[0]['npwp']; 
    }
  $body8 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => 'NPWP'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ': '.$npwp
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 8 
                          , 'style' => 'text-align: left; width: 45%; font-size: 12px; border-right: 3px solid #000; border-left: 3px solid #000; border-bottom: 3px solid #000;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body8);  
  $body10 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp; <b>No.</b>'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),   
      array('data' => '&nbsp; <b>Keterangan</b>'
                          , 'colspan' => 18
                          , 'style' => 'text-align: left; width: 80%; font-size: 12px;'),  
      array('data' => '&nbsp; <b>Nilai</b>'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 10%; font-size: 12px;'),  
      array('data' => '&nbsp; '
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body10);  

  $nilai = $data[0]['dpp_unit'] + $data[0]['ppn_unit'];
  $body12 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp; 1'
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),   
      array('data' => '&nbsp; 1 Unit '.$data[0]['nmtipe']
                          , 'colspan' => 18
                          , 'style' => 'text-align: left; width: 80%; font-size: 12px;'),  
      array('data' => '&nbsp; '.number_format($nilai,0,",",".").'&nbsp'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 10%; font-size: 12px;'),  
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($body12); 
  $body13 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp; '
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),   
      array('data' => '&nbsp; No Rangka '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 15%; font-size: 12px;'),  
      array('data' => '&nbsp;: '.$data[0]['nora']
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'),  
      array('data' => '&nbsp; '
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp; Warna '
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 10%; font-size: 12px;'),  
      array('data' => '&nbsp;: '.$data[0]['nmwarna']
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp; '
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 30%; font-size: 12px;'),  
  );
  $this->table->add_row($body13); 
  $body14 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp; '
                          , 'colspan' => 2
                          , 'style' => 'text-align: center; width: 8%; font-size: 12px;'),   
      array('data' => '&nbsp; No Mesin '
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 15%; font-size: 12px;'),  
      array('data' => '&nbsp;: '.$data[0]['nosin']
                          , 'colspan' => 5
                          , 'style' => 'text-align: left; width: 25%; font-size: 12px;'),  
      array('data' => '&nbsp; '
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp; Tahun '
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 10%; font-size: 12px;'),  
      array('data' => '&nbsp;: '.$data[0]['tahun']
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp; '
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 30%; font-size: 12px;'),      
  );
  $this->table->add_row($body14);  
  $template2 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" rules="none" border="3px" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template2); 
  echo $this->table->generate();     
  $footer2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 34%; font-size: 12px;'), 
      array('data' => '&nbsp; Harga Sebelum Diskon '
                          , 'colspan' => 10
                          , 'style' => 'text-align: right; width: 40%; font-size: 12px;'), 
      array('data' => '&nbsp;'.number_format($data[0]['dpp_unit'],0,",",".").'&nbsp;&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 20%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 5%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),   
  );
  $this->table->add_row($footer2); 
 
  $footer2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 34%; font-size: 12px;'), 
      array('data' => '&nbsp; Diskon '
                          , 'colspan' => 10
                          , 'style' => 'text-align: right; width: 40%; font-size: 12px;'), 
      array('data' => '&nbsp;'.number_format($data[0]['tot_disc_dpp'],0,",",".").'&nbsp;<b>-'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 20%; font-size: 12px; border-bottom: 3px solid #000;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 5%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),   
  );
  $this->table->add_row($footer2); 
 

  $nilai2 = $data[0]['dpp_unit'] - $data[0]['tot_disc_dpp'] ;
  $footer2 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 34%; font-size: 12px;'), 
      array('data' => '&nbsp; Harga Setelah Diskon (DPP)'
                          , 'colspan' => 10
                          , 'style' => 'text-align: right; width: 40%; font-size: 12px;'), 
      array('data' => '&nbsp;'.number_format($nilai2,0,",",".").'&nbsp;&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 20%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 5%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),  
  ); 
  $this->table->add_row($footer2);  
  $nil_ppn = $data[0]['ppn_unit'] - $data[0]['tot_disc_ppn'] ;
  $footer4 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 34%; font-size: 12px;'), 
      array('data' => '&nbsp; PPN'
                          , 'colspan' => 10
                          , 'style' => 'text-align: right; width: 40%; font-size: 12px;'), 
      array('data' => '&nbsp;'.number_format($nil_ppn,0,",",".").'&nbsp;&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 20%; font-size: 12px;'),  
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 5%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer4);
  $footer5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 34%; font-size: 12px;'),
      array('data' => '&nbsp; Pengurusan Surat Kendaraan (BBN)'
                          , 'colspan' => 10
                          , 'style' => 'text-align: right; width: 40%; font-size: 12px;'), 
      array('data' => '&nbsp;'.number_format($data[0]['bbn'],0,",",".").'+'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 20%; font-size: 12px; border-bottom: 3px solid #000;'),  
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 5%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer5);   
  $total = $nilai2 + $nil_ppn + $data[0]['bbn'];
  if($data[0]['bbn']>0){
    $otr = 'OTR';
  } else {
    $otr = 'OFTR';
  }
  $footer6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 10
                          , 'style' => 'text-align: left; width: 34%; font-size: 12px;'),
      array('data' => '&nbsp; <b>TOTAL HARGA </b>'
                          , 'colspan' => 10
                          , 'style' => 'text-align: right; width: 40%; font-size: 12px;'), 
      array('data' => '&nbsp;'.number_format($total,0,",",".").'&nbsp;&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 20%; font-size: 12px;'),   
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 5%; font-size: 12px;'), 
      array('data' => '&nbsp;' 
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer6);   
  $footer6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp;Mengetahui, ' 
                          , 'colspan' => 3
                          , 'style' => 'text-align: center; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer6);  
  $footer6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer6); 
  $footer6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer6);  
  $footer6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 1%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 12px;'),  
      array('data' => ''
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 46%; font-size: 12px;'),   
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),    
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 21%; font-size: 12px;'),    
      array('data' => '&nbsp; <b>'.$data[0]['nmadh'].'</b>'
                          , 'colspan' => 3
                          , 'style' => 'text-align: center; width: 21%; font-size: 12px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 1 
                          , 'style' => 'text-align: left; width: 1%; font-size: 12px;'),  
  );
  $this->table->add_row($footer6);  

  $footer7 = array(
      array('data' => '&nbsp; <b></b>'
                          , 'colspan' => 24
                          , 'style' => 'text-align: left; font-size: 12px;'),  
  );
  $this->table->add_row($footer7); 
  $template3 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" rules="none" border="3px" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template3); 
  echo $this->table->generate();    


  $space = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 15
                          , 'style' => 'text-align: center; font-size: 12px;'),  
  );

  $this->table->add_row($space); 

  $empty = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" rules="none" border="0px" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($empty); 
  echo $this->table->generate();    
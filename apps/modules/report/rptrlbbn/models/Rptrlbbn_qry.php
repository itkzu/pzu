<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptrlbbn_qry
 *
 * @author adi
 */
class Rptrlbbn_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }
    
    public function submit() {
        $array = $this->input->post();  
        $array['periode_awal'] = $this->apps->dateConvert($array['periode_awal']);
        $array['periode_akhir'] = $this->apps->dateConvert($array['periode_akhir']);
        $this->db->select("nobbn_trm, to_char(tglbbn_trm,'DD-MM-YYYY') AS tglbbn_trm, nama, nodo
                            , to_char(tgldo,'DD-MM-YYYY') AS tgldo, to_char(tgl_aju_bbn,'DD-MM-YYYY') AS tgl_aju_bbn, nilai
                            , jasa, disc, bbn, kode, kdtipe, nosin, nilai, byproses, jasa, pph21, bruto, netto, lr_bbn, periode
                            , kota, to_char(tgl_trm_notis,'DD-MM-YYYY') AS tgl_trm_notis, pajak_notis, pajak_tag, pajak_selisih");
        if(isset($array['tbj'])){
            $this->db->where("nobbn_trm",$array['tbj']);
        }else if(isset($array['do'])){
            $this->db->where("nodo",$array['do']);
        }else{
            $this->db->where("(tglbbn_trm BETWEEN '".$array['periode_awal']."' AND '".$array['periode_akhir']."')");
        }
        $q = $this->db->get("pzu.vb_lr_bbn");
        if($q->result_array()>0){
            $res = $q->result_array();
        }else{
            $res = null;
        }
        return $res;
    }
}

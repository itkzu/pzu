<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
    		outline: none;
    		border: none;
    }

    .radio {
    		margin-top: 0px;
    		margin-bottom: 0px;
    }

    .checkbox label, .radio label {
    		min-height: 20px;
    		padding-left: 20px;
    		margin-bottom: 5px;
    		font-weight: bold;
    		cursor: pointer;
    }
    .pass-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    } 
    #load{
        width: 80%;
        height: 80%;
        position: fixed;
        text-indent: 100%;
        background: #ebebeb url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              
      <?php
        $attributes = array(
          'role=' => 'form'
          , 'id' => 'form_add'
          , 'name' => 'form_add'
          , 'enctype' => 'multipart/form-data'
          , 'target' => '_blank'
          , 'data-validate' => 'parsley');
          echo form_open($submit,$attributes);
      ?> 
        <div class="box box-danger">

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-3">
                          <div class="box-header box-view">
                            <button type="button" class="btn btn-primary btn-add">Tambah</button>
                            <button type="button" class="btn btn-warning btn-edit">Ubah</button>
                            <button type="button" class="btn btn-danger btn-del">Hapus</button>
                            <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
                          </div>
                          <div class="box-header box-new">
                            <button type="button" class="btn btn-primary btn-save">Simpan</button>
                            <button type="button" class="btn btn-danger btn-cancel">Batal</button>
                          </div>
                        </div>

                        <div class="col-xs-1">
                          <div class="box-header "> 
                            <button type="button" class="btn btn-primary btn-imp">Import Assist</button>
                          </div>
                        </div>

                        <div class="col-xs-8">
                          <div class="box-header box-view"> 
                            <a class="btn btn-primary btn-ctk-do" target="_blank">
                              <i class="fa fa-print"></i> Cetak DO
                            </a>
                            <a class="btn btn-primary btn-ctk-um" target="_blank">
                              <i class="fa fa-print"></i> KW. UM
                            </a>
                            <button type="button" class="btn btn-primary btn-ctk-stkpb" name="ctkstkpb"><i class="fa fa-print"></i> Sticker KPB</button>
                            <!-- <a class="btn btn-primary btn-ctk-stkpb" target="_blank">
                              <i class="fa fa-print"></i> Sticker KPB
                            </a> -->
                            <a class="btn btn-primary btn-ctk-skkb" target="_blank">
                              <i class="fa fa-print"></i> SK Kredit
                            </a>
                            <button type="button" class="btn btn-primary btn-ctk-srv" name="ctksrv"><i class="fa fa-print"></i> Sticker Service</button>
                            <!-- <button type="button" class="btn btn-primary btn-ctk-do" name="ctkdo"><i class="fa fa-print"></i> Cetak DO</button> 
                            <button type="button" class="btn btn-primary btn-ctk-um" name="ctkum"><i class="fa fa-print"></i> KW. UM</button> -->
                            <!-- <button type="button" class="btn btn-primary btn-ctk-skkb" name="ctkskkb"><i class="fa fa-print"></i> SK Kredit</button>  -->
                            
                            <!-- <button type="button" class="btn btn-primary btn-ctk-bast" name="ctkbast"><i class="fa fa-print"></i> BAST Hadiah</button> -->
                            <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-12">
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-3 col-lg-3">
                              <div class="row">
                                  <div class="col-xs-5">
                                    <label>
                                      <?php echo form_label($form['tglso']['placeholder']); ?>
                                    </label>
                                  </div>

                                  <div class="col-xs-6">
                                          <?php
                                              echo form_input($form['tglso']);
                                              echo form_error('tglso','<div class="note">','</div>');
                                          ?>
                                  </div> 
                              </div> 
                          </div>

                          <div class="col-xs-3 col-lg-3"> 
                          </div>

                          <div class="col-xs-3 col-lg-3">
                              <div class="row">

                                  <div class="col-sm-5">
                                    <label>
                                      <?php echo form_label($form['tgldo']['placeholder']); ?>
                                    </label>
                                  </div>

                                  <div class="col-sm-6">
                                          <?php
                                              echo form_input($form['tgldo']);
                                              echo form_error('tgldo','<div class="note">','</div>');
                                          ?>
                                  </div> 
                              </div> 
                          </div>

                          <div class="col-xs-3 col-lg-3">
                              <div class="row"> 
                                  <div class="lr">
                                      <div class="col-sm-8">
                                              <?php
                                                  echo form_input($form['ket_lr']);
                                                  echo form_error('ket_lr','<div class="note">','</div>');
                                              ?>
                                      </div>
                                  </div>
                              </div> 
                          </div>
 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">
                          
                          <div class="col-xs-3 col-lg-3">
                              <div class="row">

                                  <div class="col-sm-5">
                                    <label>
                                      <?php echo form_label($form['noso']['placeholder']); ?>
                                    </label>
                                  </div>

                                  <div class="col-sm-6">
                                          <?php
                                              echo form_input($form['noso']);
                                              echo form_error('noso','<div class="note">','</div>');
                                          ?>
                                  </div> 
                              </div> 
                          </div>
                          
                          <div class="col-xs-3 col-lg-3">
                              <div class="row"> 

                                  <div class="col-sm-4">
                                    <label>
                                      <?php echo form_label($form['nospk']['placeholder']); ?>
                                    </label>
                                  </div>  
                                  

                                  <div class="col-sm-8">
                                      <?php
                                            echo form_dropdown($form['nospk']['name'],$form['nospk']['data'] ,$form['nospk']['value'] ,$form['nospk']['attr']);
                                            echo form_error('nospk','<div class="note">','</div>'); 
                                      ?>
                                  </div> 
                              </div> 
                          </div>
                          
                          <div class="col-xs-3 col-lg-3">
                              <div class="row"> 

                                  <div class="col-xs-5">
                                      <label>
                                        <?php echo form_label($form['nodo']['placeholder']); ?>
                                      </label>
                                  </div>

                                  <div class="col-xs-6">
                                      <?php
                                          echo form_input($form['nodo']);
                                          echo form_error('nodo','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 
                          </div>
                          
                          <div class="col-xs-3 col-lg-3">
                              <div class="row"> 

                                  <div class="bbn">
                                      <div class="col-sm-8">
                                          <?php
                                              echo form_input($form['ket_bbn']);
                                              echo form_error('ket_bbn','<div class="note">','</div>');
                                          ?>
                                      </div>
                                  </div>
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['ket']);
                                          echo form_error('ket','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div> 
                          </div> 

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">
                          <div class="col-xs-6">
                              <label> Identitas Pemesan </label>
                              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                          </div>
                          <div class="col-xs-6">
                              <label> Identitas STNK / BPKB </label>
                              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                          <?php echo form_label($form['nama1']['placeholder']); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-10">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['nama1']);
                                          echo form_error('nama1','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div> 
                          </div> 

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                          <?php echo form_label($form['nama2']['placeholder']); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-10">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['nama2']);
                                          echo form_error('nama2','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>   
                      </div> 
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-9"> 
                                  </div> 
                                
                                  <div class="col-sm-2">
                                      <button type="button" style="text-align: right" class="btn btn-secondary btn-copy">Copy Data =></button>
                                  </div> 
                              </div> 
                          </div> 

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-3">
                                      <label>
                                        <?php echo form_label('Alamat KTP'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-9">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['alamat2']);
                                          echo form_error('alamat2','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>  
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Alamat'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-10">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['alamat1']);
                                          echo form_error('alamat1','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div> 
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-3">
                                      <label>
                                        <?php echo form_label('Alamat T. Tinggal'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-9">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['alamattt']);
                                          echo form_error('alamattt','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>   
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Kel.'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['kel1']);
                                          echo form_error('kel1','<div class="note">','</div>');
                                      ?>
                                  </div> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('No. HP'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['nohp1']);
                                          echo form_error('nohp1','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>  
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Kel.'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['kel2']);
                                          echo form_error('kel2','<div class="note">','</div>');
                                      ?>
                                  </div> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('No. HP'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['nohp2']);
                                          echo form_error('nohp2','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>  
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">  
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Kec.'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['kec1']);
                                          echo form_error('kec1','<div class="note">','</div>');
                                      ?>
                                  </div> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('No. Telp'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['notelp']);
                                          echo form_error('notelp','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div> 
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Kec.'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['kec2']);
                                          echo form_error('kec2','<div class="note">','</div>');
                                      ?>
                                  </div> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('KTP'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['noktp']);
                                          echo form_error('noktp','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>   

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Kota'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                            echo form_dropdown($form['kota1']['name'],$form['kota1']['data'] ,$form['kota1']['value'] ,$form['kota1']['attr']);
                                            echo form_error('kota1','<div class="note">','</div>'); 
                                      ?>
                                  </div> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Pekerjaan'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                            echo form_dropdown($form['kdkerja']['name'],$form['kdkerja']['data'] ,$form['kdkerja']['value'] ,$form['kdkerja']['attr']);
                                            echo form_error('kdkerja','<div class="note">','</div>'); 
                                      ?>
                                  </div> 
                              </div>  
                          </div>   
                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Kota'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                            echo form_dropdown($form['kota2']['name'],$form['kota2']['data'] ,$form['kota2']['value'] ,$form['kota2']['attr']);
                                            echo form_error('kota2','<div class="note">','</div>'); 
                                      ?>
                                  </div> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('NPWP'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_input($form['npwp']);
                                          echo form_error('npwp','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>    

                      </div>
                  </div>

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 5px;"></div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Mesin / RK'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['nosin']);
                                          echo form_error('nosin','<div class="note">','</div>');
                                      ?>
                                  </div> 
                                
                                  <div class="col-sm-6">
                                      <?php
                                          // echo form_label($form['ket']['placeholder']);
                                          echo form_input($form['norangka']);
                                          echo form_error('norangka','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div> 

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Status'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php 
                                          echo form_dropdown($form['stat_otr']['name'],$form['stat_otr']['data'] ,$form['stat_otr']['value'] ,$form['stat_otr']['attr']);
                                          echo form_error('stat_otr','<div class="note">','</div>');
                                      ?>
                                  </div> 
                              </div>  
                          </div>   

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Tipe'); ?>
                                      </label>
                                  </div> 
                                
                                  <div class="col-sm-4">
                                      <?php
                                            echo form_dropdown($form['kdtipe']['name'],$form['kdtipe']['data'] ,$form['kdtipe']['value'] ,$form['kdtipe']['attr']);
                                            echo form_error('kdtipe','<div class="note">','</div>'); 
                                      ?>
                                  </div> 
                                
                                  <div class="col-sm-6">
                                      <?php
                                          echo form_input($form['nmtipe']);
                                          echo form_error('nmtipe','<div class="note">','</div>');
                                      ?>
                                  </div>  
                              </div>  
                          </div> 

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Harga'); ?>
                                      </label>
                                  </div>  
                                
                                  <div class="col-sm-5">
                                      <?php
                                          echo form_input($form['hrg_otr']);
                                          echo form_error('hrg_otr','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>   

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Warna'); ?>
                                      </label>
                                  </div>  
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_dropdown($form['kdwarna']['name'],$form['kdwarna']['data'] ,$form['kdwarna']['value'] ,$form['kdwarna']['attr']);
                                          echo form_error('kdwarna','<div class="note">','</div>');
                                      ?>
                                  </div>  
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_input($form['nmwarna']);
                                          echo form_error('nmwarna','<div class="note">','</div>');
                                      ?>
                                  </div>   
                                
                                  <div class="col-sm-2">
                                      <?php
                                          echo form_input($form['tahun']);
                                          echo form_error('tahun','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>   

                      </div>
                  </div>

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 5px;"></div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Pembayaran'); ?>
                                      </label>
                                  </div>  
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_dropdown( $form['kdleasing']['name'],
                                                              $form['kdleasing']['data'] ,
                                                              $form['kdleasing']['value'] ,
                                                              $form['kdleasing']['attr']);
                                          echo form_error('kdleasing','<div class="note">','</div>');
                                      ?>
                                  </div>  
                                
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Ket Astra'); ?>
                                      </label>
                                  </div>   
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_dropdown( $form['ket_astra']['name'],
                                                              $form['ket_astra']['data'] ,
                                                              $form['ket_astra']['value'] ,
                                                              $form['ket_astra']['attr']);
                                          echo form_error('ket_astra','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>    

                          
                          <div class="col-xs-6 col-lg-6">
                              <div class="row">  
                                
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Gratis Oli'); ?>
                                      </label>
                                  </div>   
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_dropdown( $form['promo']['name'],
                                                              $form['promo']['data'] ,
                                                              $form['promo']['value'] ,
                                                              $form['promo']['attr']);
                                          echo form_error('promo','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>   
                      </div>
                  </div>

                  <div class="khusus"> 
                  <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                          <div class="col-xs-3 col-lg-3">
                              <div class="row"> 
 
                                  <div class="col-sm-4">
                                      <label>
                                        <?php echo form_label('Program'); ?>
                                      </label>
                                  </div>  
                                
                                  <div class="col-sm-8">
                                      <?php
                                          echo form_dropdown( $form['progleas']['name'],
                                                              $form['progleas']['data'] ,
                                                              $form['progleas']['value'] ,
                                                              $form['progleas']['attr']);
                                          echo form_error('progleas','<div class="note">','</div>');
                                      ?>
                                  </div>    
                              </div>  
                          </div>

                          <div class="col-xs-3 col-lg-3">
                              <div class="row">  
                                
                                  <div class="col-sm-4">
                                      <label>
                                        <?php echo form_label('UM'); ?>
                                      </label>
                                  </div>   
                                
                                  <div class="col-sm-8">
                                      <?php
                                          echo form_input($form['um']);
                                          echo form_error('um','<div class="note">','</div>');
                                      ?>
                                  </div>    
                              </div>  
                          </div>    

                          <div class="col-xs-3 col-lg-3">
                              <div class="row">  
                                
                                  <div class="col-sm-6">
                                      <label>
                                        <?php echo form_label('Subsidi Leasing'); ?>
                                      </label>
                                  </div>  
                              </div>  
                          </div>    



                    </div>
                  </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">

                      <div class="khusus2">
                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
 
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Angsuran'); ?>
                                      </label>
                                  </div>  
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_input($form['angsuran']);
                                          echo form_error('angsuran','<div class="note">','</div>');
                                      ?>
                                  </div>  
                                
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Diskon'); ?>
                                      </label>
                                  </div>   
                                
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_input($form['diskon1']);
                                          echo form_error('diskon1  ','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>

                          <div class="col-xs-6 col-lg-6">
                              <div class="row">  
                                
                                  <div class="col-sm-6"> 
                                      <?php
                                          echo form_input($form['sub_leas']);
                                          echo form_error('sub_leas','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>   

                      <div class="col-xs-3 col-lg-3">
                          <div class="row"> 
 
                              <div class="col-sm-4">
                                  <label>
                                    <?php echo form_label('Tenor'); ?>
                                  </label>
                              </div>  
                                
                              <div class="col-sm-4">
                                  <?php
                                      echo form_input($form['tenor']);
                                      echo form_error('tenor','<div class="note">','</div>');
                                  ?>
                              </div>  
                                
                              <div class="col-sm-4">
                                  <label>
                                    <?php echo form_label('Bulan'); ?>
                                  </label>
                              </div>    
                          </div>  
                      </div>

                      <div class="col-xs-3 col-lg-3">
                          <div class="row"> 
 
                              <div class="col-sm-4">
                                  <label>
                                    <?php echo form_label('Total'); ?>
                                  </label>
                              </div>  
                                
                              <div class="col-sm-8">
                                  <?php
                                      echo form_input($form['total']);
                                      echo form_error('total','<div class="note">','</div>');
                                  ?>
                              </div>      
                          </div>  
                      </div>
                      </div>

                      <div class="umum">

                          <div class="col-xs-3 col-lg-3"> 
                          </div> 

                          <div class="col-xs-3 col-lg-3">
                              <div class="row"> 
     
                                  <div class="col-sm-4">
                                      <label>
                                        <?php echo form_label('Diskon'); ?>
                                      </label>
                                  </div>  
                                    
                                  <div class="col-sm-8">
                                      <?php
                                          echo form_input($form['diskon']);
                                          echo form_error('diskon','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div> 
                      </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
     
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Salesman'); ?>
                                      </label>
                                  </div>  
                                    
                                  <div class="col-sm-6">
                                      <?php
                                          echo form_dropdown($form['kdsales']['name'],$form['kdsales']['data'] ,$form['kdsales']['value'] ,$form['kdsales']['attr']);
                                          echo form_error('kdsales','<div class="note">','</div>');
                                      ?>
                                  </div> 
     
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Insentif'); ?>
                                      </label>
                                  </div>  
                                    
                                  <div class="col-sm-2">
                                      <?php
                                          echo form_dropdown($form['insentif']['name'],$form['insentif']['data'] ,$form['insentif']['value'] ,$form['insentif']['attr']);
                                          echo form_error('insentif','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>   

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
     
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Aksesoris'); ?>
                                      </label>
                                  </div>  
                                    
                                  <div class="col-sm-5">
                                      <?php
                                          echo form_dropdown($form['kdaks1']['name'],$form['kdaks1']['data'] ,$form['kdaks1']['value'] ,$form['kdaks1']['attr']);
                                          echo form_error('kdaks1','<div class="note">','</div>');
                                      ?>
                                  </div>  
                                    
                                  <div class="col-sm-2">
                                      <?php
                                          echo form_input($form['qtyaks1']);
                                          echo form_error('qtyaks1','<div class="note">','</div>');
                                      ?>
                                  </div>   

                                  <div class="col-xs-1 btn-add1"> 
                                          <button type="button" class="btn btn-success btn-add1"> + </button> 
                                  </div>   

                                  <div class="col-xs-1"> 
                                  </div>  
                              </div>  
                          </div>   

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
     
                                  <div class="col-sm-2">
                                      <label>
                                        <?php echo form_label('Superviser'); ?>
                                      </label>
                                  </div>  
                                    
                                  <div class="col-sm-4">
                                      <?php
                                          echo form_input($form['nmspv']);
                                          echo form_error('nmspv','<div class="note">','</div>');
                                      ?>
                                  </div>  
                                    
                                  <div class="col-sm-6">
                                      <?php
                                          echo form_input($form['nmpos']);
                                          echo form_error('nmpos','<div class="note">','</div>');
                                      ?>
                                  </div>   
                              </div>  
                          </div>   

                          <div class="col-xs-6 col-lg-6 aks2">
                              <div class="row"> 
     
                                  <div class="col-sm-2"> 
                                  </div>  
                                    
                                  <div class="col-sm-5">
                                      <?php
                                          echo form_dropdown($form['kdaks2']['name'],$form['kdaks2']['data'] ,$form['kdaks2']['value'] ,$form['kdaks2']['attr']);
                                          echo form_error('kdaks2','<div class="note">','</div>');
                                      ?>
                                  </div>      
                                    
                                  <div class="col-sm-2">
                                      <?php
                                          echo form_input($form['qtyaks2']);
                                          echo form_error('qtyaks2','<div class="note">','</div>');
                                      ?>
                                  </div>   

                                  <div class="col-xs-1 btn-add2"> 
                                          <button type="button" class="btn btn-success btn-add2"> + </button> 
                                  </div>  

                                  <div class="col-xs-1 btn-del2"> 
                                          <button type="button" class="btn btn-danger btn-del2"> - </button> 
                                  </div>  
                              </div>  
                          </div>    
 

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">  

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
     
                                  <div class="col-sm-3"> 
                                    <label><?php echo form_label('Kirim Tagih');?></label>
                                  </div>  

                                  <div class="col-xs-1">
                                      <div class="checkbox">
                                          <label>
                                              <?php echo form_checkbox($form['kirim_tagih']); ?>
                                          </label>
                                      </div>
                                  </div>

                                  <div class="col-xs-4">
                                    <?php echo form_label($form['nilai_kt']['placeholder']); ?>
                                  </div>

                                  <div class="col-xs-4">
                                        <?php
                                            echo form_input($form['nilai_kt']);
                                            echo form_error('nilai_kt','<div class="note">','</div>');
                                        ?>
                                  </div>  
                              </div>  
                          </div>   

                          <div class="col-xs-6 col-lg-6 aks3">
                              <div class="row"> 
     
                                  <div class="col-sm-2"> 
                                  </div>  
                                    
                                  <div class="col-sm-5">
                                      <?php
                                          echo form_dropdown($form['kdaks3']['name'],$form['kdaks3']['data'] ,$form['kdaks3']['value'] ,$form['kdaks3']['attr']);
                                          echo form_error('kdaks3','<div class="note">','</div>');
                                      ?>
                                  </div>      
                                    
                                  <div class="col-sm-2">
                                      <?php
                                          echo form_input($form['qtyaks3']);
                                          echo form_error('qtyaks3','<div class="note">','</div>');
                                      ?>
                                  </div>   

                                  <div class="col-xs-1 btn-add3"> 
                                          <button type="button" class="btn btn-success btn-add3"> + </button> 
                                  </div> 

                                  <div class="col-xs-1 btn-del3"> 
                                          <button type="button" class="btn btn-danger btn-del3"> - </button> 
                                  </div> 
                              </div>  
                          </div>  

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-6 col-lg-6">
                              <div class="row"> 
     
                                  <div class="col-sm-2"> 
                                        <?php echo form_label($form['keterangan']['placeholder']); ?>
                                  </div>  
                                    
                                  <div class="col-sm-10">
                                      <?php
                                          echo form_input($form['keterangan']);
                                          echo form_error('keterangan','<div class="note">','</div>');
                                      ?>
                                  </div>       
                              </div>  
                          </div>  

                          <div class="col-xs-6 col-lg-6 aks4">
                              <div class="row"> 
     
                                  <div class="col-sm-2"> 
                                  </div>  
                                    
                                  <div class="col-sm-5">
                                      <?php
                                          echo form_dropdown($form['kdaks4']['name'],$form['kdaks4']['data'] ,$form['kdaks4']['value'] ,$form['kdaks4']['attr']);
                                          echo form_error('kdaks4','<div class="note">','</div>');
                                      ?>
                                  </div>      
                                    
                                  <div class="col-sm-2">
                                      <?php
                                          echo form_input($form['qtyaks4']);
                                          echo form_error('qtyaks4','<div class="note">','</div>');
                                      ?>
                                  </div>    

                                  <div class="col-xs-1 btn-del4"> 
                                          <button type="button" class="btn btn-danger btn-del4"> - </button> 
                                  </div> 
                              </div>  
                          </div>   

                      </div>
                  </div>


              </div>
          </div>
          <!-- /.box-body -->
          <?php echo form_close(); ?> 
        </div>
        <!-- /.box -->

    </div>
</div>

<!-- modal dialog -->
<div id="modal_stiker" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">SETTING OFFSET STICKER LABEL KPB</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                  							<?php
                  								echo '<div class="radio">';
                  								echo form_label(form_radio(array('name' => 'ctk_stk','id'=>'ctk_stk_1'),'1',true) . ' Baris 1 & 2');
                  								echo '</div>';
                  							?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                  							<?php
                  								echo '<div class="radio">';
                  								echo form_label(form_radio(array('name' => 'ctk_stk','id'=>'ctk_stk_2'),'2') . ' Baris 3 & 4');
                  								echo '</div>';
                  							?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                  							<?php
                  								echo '<div class="radio">';
                  								echo form_label(form_radio(array('name' => 'ctk_stk','id'=>'ctk_stk_3'),'3') . ' Baris 5 & 6');
                  								echo '</div>';
                  							?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                  							<?php
                  								echo '<div class="radio">';
                  								echo form_label(form_radio(array('name' => 'ctk_stk','id'=>'ctk_stk_4'),'4') . ' Baris 7 & 8');
                  								echo '</div>';
                  							?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                <?php echo form_label($form['margin_up']['placeholder']); ?>
                              </div>

                              <div class="col-xs-8">
                                    <?php
                                        echo form_input($form['margin_up']);
                                        echo form_error('margin_up','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                <?php echo form_label($form['alamat_stnk']['placeholder']); ?>
                              </div>

                              <div class="col-xs-8">
                                    <?php
                                        echo form_input($form['alamat_stnk']);
                                        echo form_error('alamat_stnk','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                <?php echo form_label($form['alamat_ctk']['placeholder']); ?>
                              </div>

                              <div class="col-xs-8">
                                    <?php
                                        echo form_input($form['alamat_ctk']);
                                        echo form_error('alamat_ctk','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                    <?php
                                        echo form_input($form['nodo_ctk']);
                                        echo form_error('nodo_ctk','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-elevate btn-print"><i class="fa fa-print"></i>&nbsp; Print</button>
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-close">Batal</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div> 
</div>
<!-- modal dialog -->
<div id="modal_stiker2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">SETTING OFFSET STICKER LABEL JADWAL KPB</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                                <?php
                                  echo '<div class="radio">';
                                  echo form_label(form_radio(array('name' => 'ctk_stkkpb','id'=>'ctk_stkkpb_1'),'1',true) . ' Baris 1 & 2');
                                  echo '</div>';
                                ?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                                <?php
                                  echo '<div class="radio">';
                                  echo form_label(form_radio(array('name' => 'ctk_stkkpb','id'=>'ctk_stkkpb_2'),'2') . ' Baris 3 & 4');
                                  echo '</div>';
                                ?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                                <?php
                                  echo '<div class="radio">';
                                  echo form_label(form_radio(array('name' => 'ctk_stkkpb','id'=>'ctk_stkkpb_3'),'3') . ' Baris 5 & 6');
                                  echo '</div>';
                                ?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                                <?php
                                  echo '<div class="radio">';
                                  echo form_label(form_radio(array('name' => 'ctk_stkkpb','id'=>'ctk_stkkpb_4'),'4') . ' Baris 7 & 8');
                                  echo '</div>';
                                ?>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="row">

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                            <div class="col-xs-4">
                              <button type="button" class="btn" style="width:180px" disabled>&nbsp;</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                <?php echo form_label($form['margin_up2']['placeholder']); ?>
                              </div>

                              <div class="col-xs-8">
                                    <?php
                                        echo form_input($form['margin_up2']);
                                        echo form_error('margin_up2','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                <?php echo form_label($form['kdtipe_ctk']['placeholder']); ?>
                              </div>

                              <div class="col-xs-8">
                                    <?php
                                        echo form_input($form['kdtipe_ctk']);
                                        echo form_error('kdtipe_ctk','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div> 

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-xs-4">
                                    <?php
                                        echo form_input($form['nodo_ctk2']);
                                        echo form_error('nodo_ctk2','<div class="note">','</div>');
                                    ?>
                              </div>

                            </div>
                        </div>
                    </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-elevate btn-print2"><i class="fa fa-print"></i>&nbsp; Print</button>
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-close">Batal</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
      // Baru
      $("#nosin").mask("***-***-***-***");
      $("#norangka").mask("***-***-***-**-***-***");
      $("#noktp").mask("99.99.99.999999.9999");
      clear();

      $('.lr').hide();
      $('.bbn').hide();
      disabled();
      aksdisabled();
      GetNoID();
      $("#hrg_otr").autoNumeric();
      $('#angsuran').autoNumeric();
      $('#um').autoNumeric();
      $('#diskon1').autoNumeric();
      $('#diskon').autoNumeric();
      $('#sub_leas').autoNumeric();
      $('#total').autoNumeric();
      $('#nilai_kt').autoNumeric();
      $("#ket").val('0');
      $("#noso").val('');

      if ($("#kirim_tagih").prop("checked",false)) {
        $("#nilai_kt").attr("disabled",true);
      } else {
        $("#nilai_kt").attr("disabled",false);
      }

        if($("#kdleasing").val()==="T") {
          $(".khusus").hide();
          $(".khusus2").hide();
          $(".khusus3").hide();
          $(".umum").show();
        } else {
          $(".khusus").show();
          $(".khusus2").show();
          $(".khusus3").show();
          $(".umum").hide();
        }
        $('#kdleasing').change(function () {
            if($("#kdleasing").val()==="T") {
              $(".khusus").hide();
              $(".khusus2").hide();
              $(".khusus3").hide();
              $(".umum").show();
            } else {
              $(".khusus").show();
              $(".khusus2").show();
              $(".khusus3").show();
              set_leas();
              $(".umum").hide();
            }
        });

        //Baru
        // getNoDOHso();
        init_assist();
        full_leas();
        getKodeSales();
        getTipeUnit();
        getKdWarna();
        getKdKota();
        getKdKerja();
        // getAks();
        ctk_disabled();
        $('.btn-ctk-skkb').attr('disabled',true);
      // lama
        // clear();
        // disabled();
        // getNoSPK();

        $('#nospk').change(function () {
          if($('#ket').val()==='0'){
            var nospk = "nospk";
            set_nodo(nospk);
          } else if($('#ket').val()==='1'){
            set_apispk();
          } else {
            set_apispk_ubh();
          }
            // set_apispk();

        });

        $('#nodo').change(function () {
          if($('#ket').val()==='0'){
            var nodo = "nodo";
            set_nodo(nodo);
            // aksdisabled1();
            // alert('0');
          }
        });

        $('#noso').change(function () {
          if($('#ket').val()==='0'){
            var noso = "noso";
            set_nodo(noso);
            // aksdisabled1();
            // set_spk();
            // getNoSPK();
          } else if($('#ket').val()==='1'){
              set_spk();
              // if($('#nospk').val()===''){
              //   set_spk();
              // }else{
              //   carispk();
              // }
          }
        });

        $('#nosin').change(function () { 
          if($('#nosin').val()===''){
            dis_nosin();
          } else {
              $('#stat_otr').val('N');
              set_nosin(); 
          }
        });

        $('#ctk_stk_1').click(function () {
            set_margin();
        });

        $('#ctk_stk_2').click(function () {
            set_margin();
        });

        $('#ctk_stk_3').click(function () {
            set_margin();
        });

        $('#ctk_stk_4').click(function () {
            set_margin();
        });

        $('#ctk_stkkpb_1').click(function () {
            set_margin2();
        });

        $('#ctk_stkkpb_2').click(function () {
            set_margin2();
        });

        $('#ctk_stkkpb_3').click(function () {
            set_margin2();
        });

        $('#ctk_stkkpb_4').click(function () {
            set_margin2();
        });

        $('#kdsales').change(function () {
            getKodeSalesHeader();
        });

        $('#kdtipe').change(function () {
            getKodeTipeHeader();
        });

        $('#kdwarna').change(function () {
            getKdWarnaHeader();
        });

        $('#stat_otr').change(function () {
          if ($("#ket").val()==='2'){
            gethrg_otr_edt();
          } else {
            gethrg_otr();
          }

        });

        // $('#um').click(function () {
        //     gettotal();
        // });
        //
        // $('#diskon').click(function () {
        //     gettotal();
        // });


        $('.btn-batal').click(function () {
            clear();
            disabled();
        });

        $('.btn-ctk-stkpb').click(function () {
            $('#modal_stiker').modal('toggle');

            clear_modal();
            set_margin();
            alamat  = $("#alamat1").val();
            nodo   = $("#nodo").val();
            set_stiker(alamat,nodo);
        });

        $('.btn-ctk-srv').click(function () {
            $('#modal_stiker2').modal('toggle');

            clear_modal2();
            set_margin2(); 
            kdtipe  = $("#kdtipe").val();
            nodo   = $("#nodo").val();
            // alert(nodo);
            set_kpb(kdtipe,nodo);
        });

        $('.btn-ctk-do').click(function () {
            // clear();
            // ctk_do();
            var nodo = $('#nodo').val();  
            window.open('donew/ctk_do/'+nodo, '_blank');
            // window.location.href = 'donew/ctk_do/'+nodo; 
        });

        $('.btn-print').click(function () {
            // clear(); 
            var nodo = $('#nodo_ctk').val();
            var alamat = $('#alamat_ctk').val(); 
            var margin_up = $('#margin_up').val(); 
            var margin = margin_up - 6;
            window.open('donew/ctk_stk/'+nodo+'/'+alamat+'/'+margin, '_blank');
            // window.location.href = 'donew/ctk_stk/'+nodo+'/'+alamat+'/'+margin; 
            // ctk_stiker();
        });

        $('.btn-print2').click(function () {
            // clear(); 
            var nodo = $('#nodo_ctk2').val();
            var kdtipe = $('#kdtipe_ctk').val(); 
            var margin_up = $('#margin_up2').val(); 
            var margin = margin_up - 6;
            window.open('donew/ctk_stkkpb/'+nodo+'/'+margin, '_blank');
            // window.location.href = 'donew/ctk_stk/'+nodo+'/'+alamat+'/'+margin; 
            // ctk_stiker();
        });

        $('.btn-ctk-um').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            window.open('donew/ctk_um/'+nodo, '_blank');
            // window.location.href = 'donew/ctk_um/'+nodo;
            // ctk_um();
        });

        $('.btn-ctk-skkb').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            window.open('donew/ctk_krd/'+nodo, '_blank');
            // window.location.href = 'donew/ctk_krd/'+nodo;
            // ctk_kredit();
        });

        $('.btn-ctk-bast').click(function () {
            // clear();
            ctk_bast();
        });

        $('#tglso').change(function () {
            // clear();
            GetNoID_NOSO();
        });


        $('.btn-add').click(function () {
            clear();
            $("#ket").val('1');
            GetNoID_ADD();
            GetNoID_NOSO();
            // getNoSPK();
            init_spk();
            enabled();
            aksdisabled();
            getKdAks1(); 
            $("#npwp").mask('99.999.999.9-999.999'); 
            $("#kdleasing").val('T');
            $("#ket_astra").val('T');
            $(".khusus").hide();
            $(".khusus2").hide();
            $(".khusus3").hide();
            $(".umum").show();

        });

        $('#kirim_tagih').click(function (){
            if ($("#kirim_tagih").prop("checked")) {
              $("#nilai_kt").attr("disabled",false);
            } else {
              $("#nilai_kt").attr("disabled",true);
            }
        });

        $('#kirim_tagih').click(function (){
            if ($("#kirim_tagih").prop("checked")) {
              $("#nilai_kt").attr("disabled",false);
            } else {
              $("#nilai_kt").attr("disabled",true);
            }
        });

        $('.btn-edit').click(function () {
          $("#ket").val('2');
          init_spk();
          // set_akso_enab($('#nodo').val());

            $('#kdaks1').val('');
            $('#kdaks2').val('');
            $('#kdaks3').val('');
            $('#kdaks4').val('');
            $('#qtyaks1').val('');
            $('#qtyaks2').val('');
            $('#qtyaks3').val('');
            $('#qtyaks4').val('');
            getKdAks1();
            aksdisabled1();


            if($("#ket_bbn").val()==='BELUM BBN'){
              // alert('1');
                enabled();
                enabled1();
                enabled2(); 
                $("#npwp").mask('99.999.999.9-999.999'); 
                //bbn_tarif();
            } else if ($("#ket_bbn").val()==='SUDAH PROSES BBN') {
              // alert('0');
                $(".box-view").hide();
                $(".box-new").show();
              enabled2();
              $("#noso").attr('disabled',true);
              $("#nilai_kt").attr('disabled',true);
            }
            // $("#tglso").attr('disabled',true);
            // $("#noso").attr('disabled',true);
            // $("#kdtipe").attr('disabled',true);
            // $("#kdwarna").attr('disabled',true);
        });

        $('.btn-save').click(function () {
          var noktp = $('#noktp').val();
          var npwp = $('#npwp').val(); 
          if(noktp===''){
             if(npwp===''){
              swal({
                  title: "Data NPWP tidak boleh npwp",
                  text: "",
                  type: "error"
              }, function(){
                $('#npwp').focus();
                $('#npwp').css("border", "2px solid red");
              });
            } else {
              swal({
                  title: "Data No. KTP tidak boleh kosong",
                  text: "",
                  type: "error"
              }, function(){
                $('#noktp').focus();
                $('#noktp').css("border", "2px solid red");
              });
            }
          } else {
             if(npwp===''){
              swal({
                  title: "Data NPWP tidak boleh npwp",
                  text: "",
                  type: "error"
              }, function(){
                $('#npwp').focus();
                $('#npwp').css("border", "2px solid red");
              });
            } else {
              $('#noktp').blur();
              $('#noktp').css("border", "1px solid gainsboro");
              $('#npwp').blur();
              $('#npwp').css("border", "1px solid gainsboro");
              if($('#ket').val()==='1'){
                adddo();
              } else if($('#ket').val()==='2'){
                updatedo();
              };
            }
          }          
        });

        $('.btn-del').click(function () {
          deletedo();
        });

        $('.btn-add1').click(function () {
          var kdaks1 = $('#kdaks1').val();
          var qty1 = $('#qtyaks1').val();
          if(kdaks1===''){
            swal({
              title: "Tolong di isi Aksesorisnya",
              text: "Data tidak akan Tersimpan!",
              type: "warning"
            });
          } else {
            if(qty1==='0'){
              swal({
                title: "Mohon diisi selain 0",
                text: "Data tidak akan Tersimpan!",
                type: "warning"
              });
            } else {
              $('#kdaks1').attr("disabled",true);
              $('#qtyaks1').prop("disabled",true);
              $('.btn-add1').hide();
              aksdisabled2();
              getKdAks2(kdaks1);
            }
          }
        });

        $('.btn-add2').click(function () {
          var kdaks1 = $('#kdaks1').val(); 
          var kdaks2 = $('#kdaks2').val();
          var qty2 = $('#qtyaks2').val();
          if(kdaks2===''){
            swal({
              title: "Tolong di isi Aksesorisnya",
              text: "Data tidak akan Tersimpan!",
              type: "warning"
            });
          } else {
            if(qty2==='0'){
              swal({
                title: "Mohon diisi selain 0",
                text: "Data tidak akan Tersimpan!",
                type: "warning"
              });
            } else {
              $('#kdaks2').attr("disabled",true);
              $('#qtyaks2').prop("disabled",true);
              $('.btn-add2').hide();
              $('.btn-del2').hide();
              aksdisabled3();
              getKdAks3(kdaks1,kdaks2);
            }
          }
        });

        $('.btn-add3').click(function () {
          var kdaks1 = $('#kdaks1').val();
          var kdaks2 = $('#kdaks2').val();
          var kdaks3 = $('#kdaks3').val();
          var qty = $('#qtyaks3').val();
          if(kdaks3===''){
            swal({
              title: "Tolong di isi Aksesorisnya",
              text: "Data tidak akan Tersimpan!",
              type: "warning"
            });
          } else {
            if(qty==='0'){
              swal({
                title: "Mohon diisi selain 0",
                text: "Data tidak akan Tersimpan!",
                type: "warning"
              });
            } else {
              $('#kdaks3').attr("disabled",true);
              $('#qtyaks3').prop("disabled",true);
              $('.btn-add3').hide();
              $('.btn-del3').hide();
              aksdisabled4();
              getKdAks4(kdaks1,kdaks2,kdaks3);
            }
          }
        });

        $('.btn-del4').click(function () {
          $('#kdaks4').val('');
          $('#qtyaks4').val('0'); 
          $('#kdaks3').attr("disabled",false);
          $('#qtyaks3').prop("disabled",false); 
          $('.btn-add3').show();
          $('.btn-del3').show();
          aksdisabled3();
          // getKdAks3();  
        });

        $('.btn-del3').click(function () {
          $('#kdaks3').val('');
          $('#qtyaks3').val('0'); 
          $('#kdaks2').attr("disabled",false);
          $('#qtyaks2').prop("disabled",false); 
          $('.btn-add2').show();
          $('.btn-del2').show();
          aksdisabled2();
          // getKdAks2();  
        });

        $('.btn-del2').click(function () {
          $('#kdaks2').val('');
          $('#qtyaks2').val('0'); 
          $('#kdaks1').attr("disabled",false);
          $('#qtyaks1').prop("disabled",false); 
          $('.btn-add1').show();
          aksdisabled1();
          // getKdAks1();  
        });

        $('.btn-cancel').click(function () {
          swal({
              title: "Input Transaki SPK Inden akan dibatalkan!",
              text: "Data tidak akan Tersimpan!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#c9302c",
              confirmButtonText: "Ya, Lanjutkan!",
              cancelButtonText: "Batalkan!",
              closeOnConfirm: false
          }, function () {
            window.location.reload();
          });
        });

        $('.btn-copy').click(function () {
          $('#nama2').val($('#nama1').val());
          $('#alamat2').val($('#alamat1').val());
          $('#alamattt').val($('#alamat1').val());
          $('#kel2').val($('#kel1').val());
          $('#kec2').val($('#kec1').val());
          $('#nohp2').val($('#nohp1').val());
          $('#kota2').val($('#kota1').val());
          $("#kota2").trigger("chosen:updated");
          $('#kota2').select2({
              dropdownAutoWidth : true,
              width: '100%'
          });
        });

        $('.btn-imp').click(function () {
            var date7 = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            get_SPK(secret_key,api_key,req_time,rdate1,rdate7);
        });

    });
    // SPK
    function get_SPK(secret_key,api_key,req_time,rdate1,rdate7){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SPK'); ?>",
            data: {"secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7 },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) { 
                // console.log(resp);
                ins_SPK(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_SPK(resp){ 
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_SPK'); ?>",
            data: {"info" : resp}, 
            success: function (resp) {
                save_SPK();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_SPK(){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_SPK'); ?>",
            data: {}, 
            success: function (resp) {
                $("#load").fadeOut();
                console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

        // $('.btn-copy').click(function () {
        //   $('#nama2').val($('#nama1').val());
        //   $('#alamat2').val($('#alamat1').val());
        //   $('#alamat3').val($('#alamat1').val());
        //   $('#kel2').val($('#kel1').val());
        //   $('#kec2').val($('#kec1').val());
        //   $('#nohp2').val($('#nohp1').val());
        //   $('#kota2').val($('#kota1').val());
        //   $("#kota2").trigger("chosen:updated");
        //   $('#kota2').select2({
        //       dropdownAutoWidth : true,
        //       width: '100%'
        //   });
        // });

    function aksdisabled(){
      $('.aks1').show();
      $('#kdaks1').attr('disabled',true);
      $('#qtyaks1').attr('disabled',true);
      $('#qtyaks1').val('0');
      $('#qtyaks2').val('0');
      $('#qtyaks3').val('0');
      $('#qtyaks4').val('0');
      $('.btn-add1').hide();
      $('.aks2').hide();
      $('.aks3').hide();
      $('.aks4').hide(); 

    }

    function aksdisabled1(){
      $('.aks1').show();
      $('#kdaks1').attr('disabled',false);
      $('#qtyaks1').attr('disabled',false);
      $('.btn-add1').show();
      $('.aks2').hide();
      $('.aks3').hide();
      $('.aks4').hide();
      $('#qtyaks2').val('0');

    }

    function aksdisabled2(){
      $('.aks1').show();
      $('.aks2').show();
      $('.aks3').hide();
      $('.aks4').hide();
      $('#qtyaks3').val('0');
    }

    function aksdisabled3(){
      $('.aks1').show();
      $('.aks2').show();
      $('.aks3').show();
      $('.aks4').hide();
      $('#qtyaks4').val('0');
    }

    function aksdisabled4(){
      $('.aks1').show();
      $('.aks2').show();
      $('.aks3').show();
      $('.aks4').show();
      $('#qtyaks5').val('0');
    }

    function set_margin(){
      // var check = $('#ctk_stk_1').is(':checked');
      if($('#ctk_stk_1').is(':checked')){
        $('#margin_up').val('6');
      } else if($('#ctk_stk_2').is(':checked')){
        $('#margin_up').val('44');
      } else if($('#ctk_stk_3').is(':checked')){
        $('#margin_up').val('82');
      } else if($('#ctk_stk_4').is(':checked')){
        $('#margin_up').val('120');
      }
    }

    function set_margin2(){
      // var check = $('#ctk_stkkpb_1').is(':checked');
      if($('#ctk_stkkpb_1').is(':checked')){
        $('#margin_up2').val('6');
      } else if($('#ctk_stkkpb_2').is(':checked')){
        $('#margin_up2').val('44');
      } else if($('#ctk_stkkpb_3').is(':checked')){
        $('#margin_up2').val('82');
      } else if($('#ctk_stkkpb_4').is(':checked')){
        $('#margin_up2').val('120');
      }
    }



		// function set_size(){
    //     var check_1 = $('#ctk_stk_1').is(':checked');
    //     var check_2 = $('#ctk_stk_2').is(':checked');
    //     var check_3 = $('#ctk_stk_3').is(':checked');
		// 		var check_4 = $('#ctk_stk_4').is(':checked');
		// 		if(check_4){
		// 				$("#kddiv").prop("disabled", false);
		// 		}else if(check_3){
		// 				$("#kddiv").prop("disabled", true);
    //
		// 		}

    // ubah do
    function getNoDOHso(){
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getNoDOHso");?>",
            data: {},
            beforeSend: function() {
                $('#nospk').html("")
                            .append($('<option>', {})
                            .text('-- Pilih SPK dari HSO --'));
                $("#nospk").trigger("change.chosen");
                if ($('#nospk').hasClass("chosen-hidden-accessible")) {
                    $('#nospk').select2('destroy');
                    $("#nospk").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#nospk')
                      .append($('<option>', { value : value.idspk })
                      .html("<b style='font-size: 14px;'>" + value.idspk + " </b>"));
                });

                $('#nospk').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function init_assist(){
    	$("#nospk").select2({
    		ajax: {
    			url: "<?=site_url('donew/getNoAssist');?>",
    			type: 'post',
    			dataType: 'json',
    			delay: 250,
    			data: function (params) {
    				return {
    					q: params.term,
    					page: params.page
    				};
    			},
          processResults: function (data, params) {
    				params.page = params.page || 1;

    				return {
    					results: data.items,
    					pagination: {
    						more: (params.page * 30) < data.total_count
    					}
    				};
    			},
    			cache: true
    		},
    		placeholder: 'Masukkan No / Nama Assist ...',
    		dropdownAutoWidth : false,
    					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    					minimumInputLength: 3,
    					templateResult: format_nama, // omitted for brevity, see the source of this page
    					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
    				});
    }

    function init_spk(){
    	$("#nospk").select2({
    		ajax: {
    			url: "<?=site_url('donew/getSPKAssist');?>",
    			type: 'post',
    			dataType: 'json',
    			delay: 250,
    			data: function (params) {
    				return {
    					q: params.term,
    					page: params.page
    				};
    			},
          processResults: function (data, params) {
    				params.page = params.page || 1;

    				return {
    					results: data.items,
    					pagination: {
    						more: (params.page * 30) < data.total_count
    					}
    				};
    			},
    			cache: true
    		},
    		placeholder: 'Masukkan No / Nama Assist ...',
    		dropdownAutoWidth : false,
    					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    					minimumInputLength: 3,
    					templateResult: format_nama, // omitted for brevity, see the source of this page
    					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
    				});
    }

    function format_nama_terpilih (repo) {
    	return repo.full_name || repo.id;
    }

    function format_nama (repo) {
    	if (repo.loading) return "Mencari data ... ";


    	var markup = "<div class='select2-result-repository clearfix'>" +
    	"<div class='select2-result-repository__meta'>" +
    	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

    	markup += "<div class='select2-result-repository__statistics'>" +
    	"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.id +
    	"</div>" +
    	"</div>" +
    	"</div>";
    	return markup;
    }

    function set_nodo(ket){
      // alert(ket);
      var noso = $("#noso").val();
      var nospk = $("#nospk").val();
      var nodo = $("#nodo").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_nodo");?>",
            data: {"noso":noso,"nospk":nospk,"nodo":nodo,"ket":ket },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
                    $('.lr').hide();
                    $('.bbn').hide();
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                    // baris 1
                    if(ket==='nospk'){
                      $("#noso").val(data.noso);
                      $("#nodo").val(data.nodo);
                    } else if(ket==='nodo'){
                      $("#noso").val(data.noso);
                      $("#nospk").val(data.nosohso);
                      $("#nospk").trigger("chosen:updated");
                      $('#nospk').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                                 });
                    } else {
                      $("#nodo").val(data.nodo);
                      $("#nospk").val(data.nosohso);
                      $("#nospk").trigger("chosen:updated");
                      $('#nospk').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                                 });
                    }
                      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));

                      //baris2
                      $("#nama1").val(data.nama);
                      $("#nama2").val(data.nama_s);
                      $("#alamat1").val(data.alamat);
                      $("#alamat2").val(data.alamat_s);
                      $("#alamattt").val(data.alamat_s);
                      $("#kel1").val(data.kel);
                      $("#nohp1").val(data.nohp);
                      $("#kel2").val(data.kel_s);
                      $("#nohp2").val(data.nohp_s);
                      $("#kec1").val(data.kec);
                      $("#notelp").val(data.notelp);
                      $("#kec2").val(data.kec_s);
                      $("#kota1").val(data.kota);
                      $("#npwp").val(data.npwp);
                      $("#noktp").val(data.noktp);
                      $("#kota2").val(data.kota_s);

                      //baris3
                      $("#nosin").val(data.nosin);
                      $("#nosin").mask('***-***-***-***');
                      $("#norangka").val(data.nora);
                      $("#kdtipe").val(data.kdtipe);
                      $("#kdtipe").trigger("chosen:updated");
                      $('#kdtipe').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kdkerja").val(data.kdkerja);
                      $("#kdkerja").trigger("chosen:updated");
                      $('#kdkerja').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kota1").val(data.kota);
                      $("#kota1").trigger("chosen:updated");
                      $('#kota1').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kota2").val(data.kota_s);
                      $("#kota2").trigger("chosen:updated");
                      $('#kota2').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kdwarna").val(data.kdwarna);
                      $("#kdwarna").trigger("chosen:updated");
                      $('#kdwarna').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      }); 
                      $("#tahun").val(data.tahun);
                      if (data.status_otr==='ON THE ROAD'){
                        $("#stat_otr").val('N');
                      } else {
                        $("#stat_otr").val('F');
                      }
                      // alert(data.harga_otr);
                      $('#hrg_otr').autoNumeric('set',data.harga_otr);
                      $("#ket_astra").val(data.ket_astra);

                      //baris4
                      if(data.tk==='T'){
                        $("#kdleasing").val(data.tk);
                        $('#diskon').autoNumeric('set',data.disc);
                        $('#diskon').val(data.disc);
                          $(".khusus").hide();
                          $(".khusus2").hide();
                          $(".khusus3").hide();
                          $(".umum").show();
                          $('.btn-ctk-skkb').attr('disabled',true);
                      } else {
                        $("#kdleasing").val(data.kdleasing);
                        $("#tenor").val(data.tenor);
                        $('#angsuran').autoNumeric('set',data.angsuran);
                        $('#um').autoNumeric('set',data.um);
                        $('#diskon1').autoNumeric('set',data.disc);
                        $('#sub_leas').autoNumeric('set',data.disc_leasing);
                        $('#total').autoNumeric('set',data.um_riel);
                        $("#progleas").val(data.kdprogleas);
                        $("#progleas").trigger("chosen:updated");
                        $('#progleas').select2({
                                     dropdownAutoWidth : true,
                                     width: '100%'
                        });
                          $(".khusus").show();
                          $(".khusus2").show();
                          $(".khusus3").show();
                          $(".umum").hide();
                          $('.btn-ctk-skkb').attr('disabled',false);
                      } 
                      if (data.status_otr==='ON THE ROAD'){
                        $("#stat_otr").val('N');
                      } else {
                        $("#stat_otr").val('F');
                      }
                      if (data.promo==='Y'){ 
                        $("#promo").val('Y');
                      } else {
                        $("#promo").val('T');
                      }
                      $("#kdsales").val(data.kdsales_1);
                      // alert(data.kdsales_1);
                      $("#kdsales").trigger("chosen:updated");
                      $('#kdsales').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#insentif").val(data.kdsales_ins);
                      $("#keterangan").val(data.ket);
                      if (data.kirim_tagih === 't' ) {
                        $("#kirim_tagih").prop("checked",true);
                      } else {
                          $("#kirim_tagih").prop("checked",false);
                      }
                      $('#nilai_kt').autoNumeric('set',data.kt_nominal);
                      // $('#nilai_kt').val(data.kt_nominal);

                      $(".btn-add").attr('disabled',false);
                      $(".btn-batal").show();
                      set_akso(data.nodo);
                      getKdWarnaHeader();
                      getKodeTipeHeader();
                      getKodeSalesHeader();
                      setket();
                      init_assist();
                      ctk_enabled();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_akso(nodo){
      // alert(ket);  
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_akso");?>",
            data: {"nodo":nodo},
            success: function(resp){ 
              if(resp==='"empty"'){
                  $('.aks1').show();
                  $('#kdaks1').attr("disabled",true);
                  $('#qtyaks1').attr("disabled",true);
                  $('.aks2').hide();
                  $('.aks3').hide();
                  $('.aks4').hide();
                $('#qtyaks1').val('0');
                $('#kdaks1').val('');
                $("#kdaks1").trigger("chosen:updated");
                $('#kdaks1').select2({
                              dropdownAutoWidth : true,
                              width: '100%'
                            });
              } else {
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){ 
                    getAks1(data.kdaks1);  
                    getAks2(data.kdaks1,data.kdaks2);  
                    getAks3(data.kdaks1,data.kdaks2,data.kdaks3);  
                    getAks4(data.kdaks1,data.kdaks2,data.kdaks3,data.kdaks4);  
                    set_lakso(data.nodo);
                   
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_lakso(nodo){
      // alert(ket);  
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_akso");?>",
            data: {"nodo":nodo},
            success: function(resp){ 
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                // baris 1
                if(data.kdaks1>0){
                  if(data.kdaks2>0){
                    if(data.kdaks3>0){
                      if(data.kdaks4>0){
                        // if(data.kdaks5>0){
                        // }
                        aksdisabled4();  
              
                      } else {
                        aksdisabled3();  
                      }
                    } else { 
                        aksdisabled2(); 
                    }
                  } else {
                        aksdisabled1();   
                  }
                } else {
                  $('.aks1').hide();
                  $('.aks2').hide();
                  $('.aks3').hide();
                  $('.aks4').hide();
                } 
                $('#qtyaks1').val(data.qtyaks1);
                $('#kdaks1').val(data.kdaks1);
                $("#kdaks1").trigger("chosen:updated");
                $('#kdaks1').select2({
                              dropdownAutoWidth : true,
                              width: '100%'
                            });
                $('#qtyaks2').val(data.qtyaks2);
                $('#kdaks2').val(data.kdaks2);
                $("#kdaks2").trigger("chosen:updated");
                $('#kdaks2').select2({
                              dropdownAutoWidth : true,
                              width: '100%'
                           });
                $('#qtyaks3').val(data.qtyaks3);
                $('#kdaks3').val(data.kdaks3);
                $("#kdaks3").trigger("chosen:updated");
                $('#kdaks3').select2({
                             dropdownAutoWidth : true,
                              width: '100%'
                           });
                $('#qtyaks4').val(data.qtyaks4);
                $('#kdaks4').val(data.kdaks4);
                $("#kdaks4").trigger("chosen:updated");
                $('#kdaks4').select2({
                              dropdownAutoWidth : true,
                              width: '100%'
                           });
                $('#kdaks4').attr("disabled",true);
                $('#qtyaks4').attr("disabled",true);
                $('#kdaks3').attr("disabled",true);
                $('#qtyaks3').attr("disabled",true);
                $('#kdaks2').attr("disabled",true);
                $('#qtyaks2').attr("disabled",true);
                $('#kdaks1').attr("disabled",true);
                $('#qtyaks1').attr("disabled",true);
                $('.btn-add1').hide();$('.btn-add2').hide();$('.btn-add3').hide();
                $('.btn-del2').hide();$('.btn-del3').hide();$('.btn-del4').hide();
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_akso_enab(nodo){
      // alert(ket);  
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_akso");?>",
            data: {"nodo":nodo},
            success: function(resp){ 
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                // baris 1
                if(data.kdaks1>0){
                  if(data.kdaks2>0){
                    if(data.kdaks3>0){
                      if(data.kdaks4>0){
                        // if(data.kdaks5>0){
                        // }
                        aksdisabled4();  
              
                      } else {
                        aksdisabled3();  
                      }
                    } else { 
                        aksdisabled2(); 
                    }
                  } else {
                        aksdisabled1();   
                  }
                } else {
                  $('.aks1').hide();
                  $('.aks2').hide();
                  $('.aks3').hide();
                  $('.aks4').hide();
                } 
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function enabled(){
      $(".box-view").hide();
      $(".box-new").show();
      //baris 1
      $("#tglso").attr('disabled',false);
      $("#tgldo").attr('disabled',false);
      //baris 2
      $("#nama1").attr('disabled',false);
      $("#nama2").attr('disabled',false);
      $("#alamat1").attr('disabled',false);
      $("#alamat2").attr('disabled',false);
      $("#alamattt").attr('disabled',false);
      $("#kel1").attr('disabled',false);
      $("#kel2").attr('disabled',false);
      $("#nohp1").attr('disabled',false);
      $("#nohp2").attr('disabled',false);
      $("#kec1").attr('disabled',false);
      $("#kec2").attr('disabled',false);
      $("#kdkerja").attr('disabled',false);
      $("#kota1").attr('disabled',false);
      $("#kota2").attr('disabled',false);
      $("#notelp").attr('disabled',false);
      $("#noktp").attr('disabled',false);
      $("#npwp").attr('disabled',false);

      //baris 3
      $("#nosin").attr('disabled',false);
      $("#norangka").attr('disabled',true);
      $("#kdtipe").attr('disabled',true);
      $("#nmtipe").attr('disabled',true);
      $("#kdwarna").attr('disabled',true);
      $("#nmwarna").attr('disabled',true);
      $("#tahun").attr('disabled',true);
      $("#promo").attr('disabled',false);
      // $(".btn-batal").show();

    }
    function enabled1(){

        $("#stat_otr").attr('disabled',false);
        $("#hrg_otr").attr('disabled',false);
    }

    function enabled2(){
      //baris 4
      $("#ket_astra").attr('disabled',true);
      $("#kdleasing").attr('disabled',false);
      $("#progleas").attr('disabled',false);
      $("#um").attr('disabled',false);
      $("#angsuran").attr('disabled',false);
      $("#tenor").attr('disabled',false);
      $("#diskon").attr('disabled',false);
      $("#diskon1").attr('disabled',false);
      $("#sub_leas").attr('disabled',false);
      //baris 5
      $("#kdsales").attr('disabled',false);
      $("#nmsales").attr('disabled',true);
      $("#nmspv").attr('disabled',true);
      $("#nmpos").attr('disabled',true);
      $("#insentif").attr('disabled',false);
      $("#kirim_tagih").attr('disabled',false);
      $("#nilai_kt").attr('disabled',false);
      $("#keterangan").attr('disabled',false);
    }
    // tambah do
    function getNoSPK(){
        var so = $("#noso").val();
        var noso = so.toUpperCase();
        var nospk = $("#nospk").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getNoSPK");?>",
            data: {"nospk":nospk,"noso":noso},
            beforeSend: function() {
                $('#nospk').html("")
                            .append($('<option>', { value :  ''  })
                            .text('-- Pilih SPK dari HSO --'));
                $("#nospk").trigger("change.chosen");
                if ($('#nospk').hasClass("chosen-hidden-accessible")) {
                    $('#nospk').select2('destroy');
                    $("#nospk").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#nospk')
                      .append($('<option>', { value : value.idspk })
                      .html("<b style='font-size: 14px;'>" + value.idspk + " </b>"));
                });

                // $('#nospk').select2({
                //     dropdownAutoWidth : true,
                //     width: '100%'
                //   });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
    // tambah do
    function set_spk(){

      var so = $("#noso").val(); 
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_spk");?>",
            data: {"noso":noso },
            success: function(resp){
              if(resp==='"empty"'){ 
              }else{ 
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      if (data.nodo===null) {
                          $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                          $("#nama1").val(data.nama);
                          $("#nama2").val(data.nama_s);
                          $("#alamat1").val(data.alamat);
                          $("#alamat2").val(data.alamat_s);
                          $("#alamattt").val(data.alamat_s);
                          $("#kel1").val(data.kel);
                          $("#nohp1").val(data.nohp);
                          $("#kel2").val(data.kel_s);
                          $("#nohp2").val(data.nohp_s);
                          $("#kec1").val(data.kec);
                          $("#kdkerja").val(data.kdkerja);
                          $("#kdkerja").trigger("chosen:updated");
                          $('#kdkerja').select2({
                                       dropdownAutoWidth : true,
                                       width: '100%'
                          });
                          $("#kota1").val(data.kota);
                          $("#kota1").trigger("chosen:updated");
                          $('#kota1').select2({
                                       dropdownAutoWidth : true,
                                       width: '100%'
                          });
                          $("#kota2").val(data.kota_s);
                          $("#kota2").trigger("chosen:updated");
                          $('#kota2').select2({
                                       dropdownAutoWidth : true,
                                       width: '100%'
                          });
                          $("#notelp").val(data.notelp);
                          $("#kec2").val(data.kec_s); 
                          $("#npwp").val(data.npwp);
                          $("#noktp").val(data.noktp); 
                          $("#nospk").val(data.nosohso);
                          $("#kdsales").val(data.kdsales);
                          $("#kdsales").trigger("chosen:updated");
                          $('#kdsales').select2({
                                        dropdownAutoWidth : true,
                                        width: '100%'
                                      });
                           // alert(data.nosohso);
                          $("#nospk").trigger("chosen:updated");
                          $('#nospk').select2({
                                        dropdownAutoWidth : true,
                                        width: '100%'
                                      });
                          $(".btn-add").attr('disabled',false);
                          $(".btn-edit").attr('disabled',false);
                          $(".btn-del").attr('disabled',false); 
                          $(".btn-batal").show();
                          getKodeSalesHeader();
                          init_spk(); 
                      } else {
                        swal({
                            title: "Proses Gagal",
                            text: "No SPK Sudah Dipakai "+data.nodo,
                            type: "error"
                        }, function(){
                           $('#noso').val('');
                        });
                      }
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function carispk(){

      var so = $("#noso").val();
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/carispk");?>",
            data: {"noso":noso },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                    // if(data.noso=$('#noso').val()){
                        swal({
                            title: "Proses Gagal",
                            text: "No SPK Sudah Ada",
                            type: "error"
                        }, function(){
                            clear();
                            $('#noso').val('');
                        });
                    // }
                });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_apispk(){
      var spk = $("#nospk").val();
      var so = $("#noso").val();
      // var test = '"empty"';
      var nospk = spk.toUpperCase();
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_apispk");?>",
            data: {"nospk":nospk,"noso":noso },
            success: function(resp){
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      // $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpsn)));
                      $("#nama1").val(data.nmcustomer);
                      if(data.tppembayaran==='Cash'){
                        $("#ket_astra").val('T');
                      } else {
                        $("#ket_astra").val('K');
                      }
                      $("#nama2").val(data.nmbpkb);
                      $("#alamat1").val(data.alamatkk);
                      $("#alamattt").val(data.alamatkk);
                      $("#alamat2").val(data.alamatkk);
                      $("#kel1").val(data.nmlurah);
                      $("#nohp1").val(data.nokontak);
                      $("#kel2").val(data.nmlurahbpkb);
                      $("#nohp2").val(data.nokontak);
                      $("#kec1").val(data.nmkec);
                      $("#notelp").val(data.nokontak);
                      $("#kec2").val(data.nmkecbpkb); 
                      $("#kota1").val(data.nmkab);
                      $("#kota1").trigger("chosen:updated");
                      $('#kota1').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kota2").val(data.nmkotabpkb);
                      $("#kota2").trigger("chosen:updated");
                      $('#kota2').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#noktp").val(data.noktp);
                      if(data.npwp==='.'){
                        $("#npwp").val('00.000.000.0-000.000'); 
                      } else {
                        $("#npwp").val(data.npwp); 
                      }
                      
                      // getTipeUnit();
                      // getKdWarna();
                    });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_apispk_ubh(){
      var spk = $("#nospk").val();
      var so = $("#noso").val();
      // var test = '"empty"';
      var nospk = spk.toUpperCase();
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_apispk_ubh");?>",
            data: {"nospk":nospk,"noso":noso },
            success: function(resp){
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      // $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpsn)));

                      if($("#noso").val()===''){
                        $("#noso").prop("disabled",false);
                        // alert('2');
                      } else {
                        $("#noso").prop("disabled",true);
                        // alert('1');
                      }
                      $("#nama1").val(data.nmcustomer);
                      if(data.tppembayaran==='Cash'){
                        $("#ket_astra").val('T');
                      } else {
                        $("#ket_astra").val('K');
                      }
                      $("#nama2").val(data.nmbpkb);
                      $("#alamat1").val(data.alamatkk);
                      $("#alamattt").val(data.alamatkk);
                      $("#alamat2").val(data.alamatkk);
                      $("#kel1").val(data.nmlurah);
                      $("#nohp1").val(data.nokontak);
                      $("#kel2").val(data.nmlurahbpkb);
                      $("#nohp2").val(data.nokontak);
                      $("#kec1").val(data.nmkec);
                      $("#notelp").val(data.nokontak);
                      $("#kec2").val(data.nmkecbpkb);
                      $("#kota1").val(data.nmkab);
                      $("#kota1").trigger("chosen:updated");
                      $('#kota1').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kota2").val(data.nmkotabpkb);
                      $("#kota2").trigger("chosen:updated");
                      $('#kota2').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#noktp").val(data.noktp);
                      $("#npwp").val(data.npwp); 
                      // getTipeUnit();
                      // getKdWarna();
                    });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function GetNoID_NOSO(){
      var tg = $("#tglso").val();
      var tgl = tg.toString();
      var tanggal = tgl.substring(8,10);  
      var tg1 = tgl.substring(8,9);  
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){    
                  $.mask.definitions['9'] = '';
                  $.mask.definitions['d'] = '[0-9]';
                  $('#noso').mask(value.kode+tg1+"d-dddddd");
                  // $('#noso').mask(value.kode+tanggal+"-dddddd");
                  // $('#noso').mask(value.kode+"dd-dddddd");

                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_nosin(){
      var mesin = $("#nosin").val();
      // var test = '"empty"';
      var nosin = mesin.replace("-","");
      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");
      var nosin = nosin.toUpperCase();
      // alert(nosin);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_nosin");?>",
            data: {"nosin":nosin },
            success: function(resp){
              if(resp==='"empty"'){ 
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      // $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpsn)));
                      enabled1();
                      enabled2();
                      $("#norangka").val(data.nora2);
                      // alert(data.kdleasing);
                      // if(data.kdleasing==='TUNAI'){
                      //   $("#kdleasing").val('T');
                      //   $("#kdleasing").trigger("change");
                      // } else {
                      //   $("#kdleasing").val('K');
                      //   $("#kdleasing").trigger("change");
                      // }
                      $("#kdtipe").val(data.kdtipe);
                      $("#kdtipe").trigger("chosen:updated");
                      $('#kdtipe').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#kdwarna").val(data.kdwarna);
                      $("#kdwarna").trigger("chosen:updated");
                      $('#kdwarna').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#tahun").val(data.tahun);
                      // $("#nmspv").val(data.nmsales_header);
                      // alert(data.finden);
                      // if (data.finden === 't' ) {
                      //   $("#finden").prop("checked",true);
                      // } else {
                      //     $("#finden").prop("checked",false);
                      // }
                      $("#kdtipe").attr('disabled',true);
                      $("#kdwarna").attr('disabled',true);
                      // $("#kdsales").attr('disabled',true);
                      // $("#nmsales").attr('disabled',true);
                      // $("#nmspv").attr('disabled',true); 
                      $("#kdleasing").attr('disabled',false);
                      $("#nilai_kt").attr('disabled',true);
                      $(".btn-add").attr('disabled',true);
                      $(".btn-edit").attr('disabled',false);
                      $(".btn-del").attr('disabled',false);
                      $(".btn-batal").show();
                      // getTipeUnit();
                      // getKdWarna();
                      getKdWarnaHeader();
                      getKodeTipeHeader();
                      gethrg_otr();
                      aksdisabled1();
                      // gettotal();
                    });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function setket(){
      var nodo = $("#nodo").val();
      // var test = '"empty"';
      var nodo = nodo.toUpperCase();
      // alert(nosin);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/setket");?>",
            data: {"nodo":nodo },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                    // alert(data.progres);
                      if(data.progres<1){
                        if(data.tgl_aju_fa===null){
                        $('#ket_lr').val('BELUM L/R');
                        $('#ket_bbn').val('BELUM BBN');
                        // alert('1');
                        $('.lr').hide();
                        $('.bbn').hide();
                        $(".btn-edit").prop('disabled',false);
                        $(".btn-del").prop('disabled',false);
                      } else {
                        $('#ket_lr').val('BELUM L/R');
                        $('#ket_bbn').val('SUDAH PROSES BBN');
                        $('.lr').hide();
                        $('.bbn').show();
                        $(".btn-edit").prop('disabled',false);
                        $(".btn-del").prop('disabled',true);
                        }
                      } else{
                      if(data.tgl_aju_fa===null){
                        $('#ket_lr').val('SUDAH PROSES L/R');
                        $('#ket_bbn').val('BELUM BBN');
                        $('.lr').show();
                        $('.bbn').hide();
                        $(".btn-edit").prop('disabled',true);
                        $(".btn-del").prop('disabled',true);
                      } else {
                        $('#ket_lr').val('SUDAH PROSES L/R');
                        $('#ket_bbn').val('SUDAH PROSES BBN');
                        $('.lr').show();
                        $('.bbn').show();
                        $(".btn-edit").prop('disabled',true);
                        $(".btn-del").prop('disabled',true);
                      }
                      }
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_leas(){
        var kdleasing = $("#kdleasing").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/set_leas");?>",
            data: {"kdleasing":kdleasing},
            beforeSend: function() {
                $('#progleas').html("")
                            .append($('<option>', { value : '' }));
                $("#progleas").trigger("change.chosen");
                if ($('#progleas').hasClass("chosen-hidden-accessible")) {
                    $('#progleas').select2('destroy');
                    $("#progleas").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#progleas')
                      .append($('<option>', { value : value.kdprogleas })
                      .html("<b style='font-size: 14px;'>" + value.nmprogleas + " </b>"));
                });

                $('#progleas').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function full_leas(){
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/full_leas");?>",
            data: {},
            beforeSend: function() {
                $('#progleas').html("")
                            .append($('<option>', { value : '' }));
                $("#progleas").trigger("change.chosen");
                if ($('#progleas').hasClass("chosen-hidden-accessible")) {
                    $('#progleas').select2('destroy');
                    $("#progleas").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#progleas')
                      .append($('<option>', { value : value.kdprogleas })
                      .html("<b style='font-size: 14px;'>" + value.nmprogleas + " </b>"));
                });

                $('#progleas').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeSales(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKodeSales");?>",
            data: {},
            beforeSend: function() {
                $('#kdsales').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Salesman --'));
                $("#kdsales").trigger("change.chosen");
                if ($('#kdsales').hasClass("chosen-hidden-accessible")) {
                    $('#kdsales').select2('destroy');
                    $("#kdsales").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdsales')
                      .append($('<option>', { value : value.kdsales })
                      .html("<b style='font-size: 14px;'>" + value.nmsales + " </b>"));
                });

                $('#kdsales').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeSalesHeader(){
      var kdsales = $('#kdsales').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKodeSalesHeader");?>",
            data: {"kdsales":kdsales },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmspv").val(data.nmsales_header);
                $("#nmpos").val(data.nmdiv);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getTipeUnit(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getTipeUnit");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdtipe').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Tipe Unit --'));
                $("#kdtipe").trigger("change.chosen");
                if ($('#kdtipe').hasClass("chosen-hidden-accessible")) {
                    $('#kdtipe').select2('destroy');
                    $("#kdtipe").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdtipe')
                      .append($('<option>', { value : value.kdtipe })
                      .html("<b style='font-size: 14px;'>" + value.kdtipe + " </b>"));
                });

                $('#kdtipe').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeTipeHeader(){
      var kdtipe = $('#kdtipe').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKodeTipeHeader");?>",
            data: {"kdtipe":kdtipe },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmtipe").val(data.nmtipe);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdWarna(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdWarna");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdwarna').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kode Warna --'));
                $("#kdwarna").trigger("change.chosen");
                if ($('#kdwarna').hasClass("chosen-hidden-accessible")) {
                    $('#kdwarna').select2('destroy');
                    $("#kdwarna").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdwarna')
                      .append($('<option>', { value : value.kdwarna })
                      .html("<b style='font-size: 14px;'>" + value.kdwarna + " </b>"));
                });

                $('#kdwarna').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdKerja(){
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdKerja");?>",
            data: {},
            beforeSend: function() {
                $('#kdkerja').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Pekerjaan --'));
                $("#kdkerja").trigger("change.chosen");
                if ($('#kdkerja').hasClass("chosen-hidden-accessible")) {
                    $('#kdkerja').select2('destroy');
                    $("#kdkerja").chosen({ width: '100%' });
                } 
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdkerja')
                      .append($('<option>', { value : value.kdkerja })
                      .html("<b style='font-size: 12px;'>" + value.nmkerja + " </b>")); 
                });

                $('#kdkerja').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdKota(){
        var nospk = $("#nospk").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdKota");?>",
            data: {"nospk":nospk},
            beforeSend: function() {
                $('#kota1').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kota --'));
                $("#kota1").trigger("change.chosen");
                if ($('#kota1').hasClass("chosen-hidden-accessible")) {
                    $('#kota1').select2('destroy');
                    $("#kota1").chosen({ width: '100%' });
                }
                $('#kota2').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kota --'));
                $("#kota2").trigger("change.chosen");
                if ($('#kota2').hasClass("chosen-hidden-accessible")) {
                    $('#kota2').select2('destroy');
                    $("#kota2").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kota1')
                      .append($('<option>', { value : value.nmkab })
                      .html("<b style='font-size: 12px;'>" + value.nmkab + " </b>"));
                  
                  $('#kota2')
                      .append($('<option>', { value : value.nmkab })
                      .html("<b style='font-size: 12px;'>" + value.nmkab + " </b>"));
                });

                $('#kota1').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                });

                $('#kota2').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getAks1(data1){ 
        // alert(data1);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getAks1");?>",
            data: {"data1":data1},
            beforeSend: function() {
                $('#kdaks1').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks1").trigger("change.chosen");
                if ($('#kdaks1').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks1').select2('destroy');
                    $("#kdaks1").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks1')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks1').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getAks2(data1,data2){ 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getAks2");?>",
            data: {"data1":data1,"data2":data2},
            beforeSend: function() {
                $('#kdaks2').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks2").trigger("change.chosen");
                if ($('#kdaks2').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks2').select2('destroy');
                    $("#kdaks2").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks2')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks2').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getAks3(data1,data2,data3){ 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getAks3");?>",
            data: {"data1":data1,"data2":data2,"data3":data3},
            beforeSend: function() {
                $('#kdaks3').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks3").trigger("change.chosen");
                if ($('#kdaks3').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks3').select2('destroy');
                    $("#kdaks3").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks3')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks3').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getAks4(data1,data2,data3,data4){ 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getAks4");?>",
            data: {"data1":data1,"data2":data2,"data3":data3,"data4":data4},
            beforeSend: function() {
                $('#kdaks4').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks4").trigger("change.chosen");
                if ($('#kdaks4').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks4').select2('destroy');
                    $("#kdaks4").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks4')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks4').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdAks1(){ 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdAks1");?>",
            data: {},
            beforeSend: function() {
                $('#kdaks1').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks1").trigger("change.chosen");
                if ($('#kdaks1').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks1').select2('destroy');
                    $("#kdaks1").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks1')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks1').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdAks2(kdaks1){  
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdAks2");?>",
            data: {"kdaks1":kdaks1},
            beforeSend: function() {
                $('#kdaks2').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks2").trigger("change.chosen");
                if ($('#kdaks2').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks2').select2('destroy');
                    $("#kdaks2").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks2')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks2').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdAks3(kdaks1,kdaks2){  
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdAks3");?>",
            data: {"kdaks1":kdaks1,"kdaks2":kdaks2},
            beforeSend: function() {
                $('#kdaks3').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks3").trigger("change.chosen");
                if ($('#kdaks3').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks3').select2('destroy');
                    $("#kdaks3").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks3')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks3').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdAks4(kdaks1,kdaks2,kdaks3){  
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdAks4");?>",
            data: {"kdaks1":kdaks1,"kdaks2":kdaks2,"kdaks3":kdaks3},
            beforeSend: function() {
                $('#kdaks4').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks4").trigger("change.chosen");
                if ($('#kdaks4').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks4').select2('destroy');
                    $("#kdaks4").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks4')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks4').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdAks5(kdaks1,kdaks2,kdaks3,kdaks4){  
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdAks5");?>",
            data: {"kdaks1":kdaks1,"kdaks2":kdaks2,"kdaks3":kdaks3,"kdaks4":kdaks4},
            beforeSend: function() {
                $('#kdaks5').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Aksesoris --'));
                $("#kdaks5").trigger("change.chosen");
                if ($('#kdaks5').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks5').select2('destroy');
                    $("#kdaks5").chosen({ width: '100%' }); 
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdaks5')
                      .append($('<option>', { value : value.kdaks })
                      .html("<b style='font-size: 12px;'>" + value.nmaks + " </b>")); 
                });

                $('#kdaks5').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                }); 

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdWarnaHeader(){
      var kdwarna = $('#kdwarna').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getKdWarnaHeader");?>",
            data: {"kdwarna":kdwarna },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmwarna").val(data.nmwarna);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function gethrg_otr(){
      var nosin = $('#nosin').val();
      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");
      var nosin = nosin.toUpperCase();
      var stat_otr = $("#stat_otr").val();

        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/gethrg_otr");?>",
            data: {"nosin":nosin,"stat_otr":stat_otr },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                if(stat_otr === 'N'){
                    $("#hrg_otr").autoNumeric('set',data.harga_ontr);
                } else {
                    $("#hrg_otr").autoNumeric('set',data.harga_oftr);
                }

                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function gethrg_otr_edt(){
      var nosin = $('#nosin').val();
      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");
      var nosin = nosin.toUpperCase();
      var stat_otr = $("#stat_otr").val();

        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/gethrg_otr_edt");?>",
            data: {"nosin":nosin,"stat_otr":stat_otr },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                if(stat_otr === 'N'){
                    $("#hrg_otr").autoNumeric('set',data.harga_ontr);
                } else {
                    $("#hrg_otr").autoNumeric('set',data.harga_oftr);
                }

                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function adddo(){
      var tglso = $("#tglso").val();
      var so = $("#noso").val();                          var noso = so.toUpperCase();
      var nosohso = $("#nospk").val();
      if (nosohso===null){
        var nospk = '';
      }else{
        var nospk = nosohso.toUpperCase();
      }
      var mesin = $("#nosin").val();                      var nosin = mesin.replace("-","");                      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");                  var nosin = nosin.toUpperCase();
      var nodo = $("#nodo").val();                        var nodo = nodo.toUpperCase();

      var kdsales = $("#kdsales").val();
      var ket_astra = $("#ket_astra").val();
      var kdleasing = $("#kdleasing").val();              var kdleasing = kdleasing.toUpperCase();
      if(kdleasing==='T'){
        var jnsbayar = 'T';
        var kdprogleas = '0.00';
        var disc = $("#diskon").autoNumeric('get');
          if (disc>0){
            var disc = $("#diskon").autoNumeric('get');
          } else {
            var disc = '0';
          }
        var um = '0.00';

        var disc_leasing = '0.00';
        var angsuran = '0.00';   var tenor = '0.00';
      } else {
        var jnsbayar = 'K';
        var kdprogleas = $("#progleas").val();
        var disc = $("#diskon1").autoNumeric('get');    var um = $("#um").autoNumeric('get');

        var disc_leasing = $("#sub_leas").autoNumeric('get');
        var angsuran = $("#angsuran").autoNumeric('get');   var tenor = $("#tenor").val();
      }
      var stat_otr = $("#stat_otr").val();
      var promo = $("#promo").val();

      var hrg_otr = $("#hrg_otr").autoNumeric('get');

      var nama1 = $("#nama1").val();
      var nama2 = nama1.replace("'","''");          var nama = nama2.toUpperCase();       
      var nama_s1 = $("#nama2").val();
      var nama_s2 = nama_s1.replace("'","''");              var nama_s = nama_s2.toUpperCase();
      var alamat1 = $("#alamat1").val();      var alamat2 = alamat1.replace("'","''");
      var alamat = alamat2.toUpperCase();   
      var alamat_s1 = $("#alamat2").val();     var alamat_s2 = alamat_s1.replace("'","''");     
      var alamat_s = alamat_s2.toUpperCase();
      var alamatktp_s1 = $("#alamattt").val();  var alamatktp_s2 = alamatktp_s1.replace("'","''");    
      var alamatktp_s = alamatktp_s2.toUpperCase();
      var kel = $("#kel1").val();            var kel = kel.toUpperCase();         var kel_s = $("#kel2").val();                 var kel_s = kel_s.toUpperCase();
      var nohp = $("#nohp1").val();          var nohp = nohp.toUpperCase();       var nohp_s = $("#nohp2").val();               var nohp_s = nohp_s.toUpperCase();
      var kec = $("#kec1").val();            var kec = kec.toUpperCase();         var kec_s = $("#kec2").val();                 var kec_s = kec_s.toUpperCase();
      var notelp = $("#notelp").val();       var notelp = notelp.toUpperCase();   var notelp_s = $("#notelp").val();            var notelp_s = notelp_s.toUpperCase();
      var npwp = $("#npwp").val();           var npwp = npwp.toUpperCase();       var noktp = $("#noktp").val();                var noktp = noktp.toUpperCase();
      var kota1 = $("#kota1").val();          var kota2 = kota1.replace("'","''");    var kota = kota2.toUpperCase();

      var kdkerja = $("#kdkerja").val(); 

      var kota_s1 = $("#kota2").val();        var kota_s2 = kota_s1.replace("'","''");    var kota_s = kota_s2.toUpperCase();

      var ket = $("#keterangan").val();      var ket = ket.toUpperCase();

      var tgldo = $("#tgldo").val();

      if ($("#kirim_tagih").prop("checked")){
          var kirim_tagih = 'true';
          var nilai_kt = $("#nilai_kt").autoNumeric('get');
      } else {
        var kirim_tagih = 'false';
        var nilai_kt = '0';
      }

      var insentif = $("#insentif").val();
      var kdaks1 = $("#kdaks1").val();
      var qtyaks1 = $("#qtyaks1").val();
      var kdaks2 = $("#kdaks2").val();
      var qtyaks2 = $("#qtyaks2").val();
      var kdaks3 = $("#kdaks3").val();
      var qtyaks3 = $("#qtyaks3").val();
      var kdaks4 = $("#kdaks4").val();
      var qtyaks4 = $("#qtyaks4").val();

        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/adddo");?>",
            data: {"noso":noso
                    ,"tglso":tglso
                    ,"nosin":nosin
                    ,"nodo":nodo
                    ,"kdsales":kdsales
                    ,"jnsbayar":jnsbayar
                    ,"ket_astra":ket_astra
                    ,"kdleasing":kdleasing
                    ,"kdprogleas":kdprogleas
                    ,"stat_otr":stat_otr
                    ,"hrg_otr":hrg_otr
                    ,"um":um
                    ,"disc":disc
                    ,"disc_leasing":disc_leasing
                    ,"nama":nama
                    ,"nama_s":nama_s
                    ,"alamat":alamat
                    ,"alamat_s":alamat_s
                    ,"alamatktp_s":alamatktp_s
                    ,"kel":kel
                    ,"kel_s":kel_s
                    ,"nohp":nohp
                    ,"nohp_s":nohp_s
                    ,"kec":kec
                    ,"kec_s":kec_s
                    ,"notelp":notelp
                    ,"notelp_s":notelp_s
                    ,"npwp":npwp
                    ,"noktp":noktp
                    ,"kota":kota
                    ,"kota_s":kota_s
                    ,"angsuran":angsuran
                    ,"tenor":tenor
                    ,"ket":ket
                    ,"tgldo":tgldo
                    ,"kirim_tagih":kirim_tagih
                    ,"nilai_kt":nilai_kt
                    ,"insentif":insentif
                    ,"nospk":nospk
                    ,"kdkerja":kdkerja
                    ,"kdaks1":kdaks1
                    ,"kdaks2":kdaks2
                    ,"kdaks3":kdaks3
                    ,"kdaks4":kdaks4
                    ,"qtyaks1":qtyaks1
                    ,"qtyaks2":qtyaks2
                    ,"qtyaks3":qtyaks3
                    ,"qtyaks4":qtyaks4
                    ,"promo":promo},
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          if(data.tipe==='success'){
                              window.location.reload();

                              $('#nodo').val(nodo);
                              set_nodo('nodo');
                              var margin = 0;

                              window.open('donew/ctk_do/'+nodo, '_blank'); 
                              window.open('donew/ctk_stk/'+nodo+'/'+alamat+'/'+margin, '_blank'); 
                              // window.open('donew/ctk_stkkpb/'+nodo+'/'+margin, '_blank'); 
                              // window.open('donew/ctk_um/'+nodo, '_blank'); 
                              // window.open('donew/ctk_krd/'+nodo, '_blank');
                          }
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function updatedo(){
      var tglso = $("#tglso").val();
      var so = $("#noso").val();                          var noso = so.toUpperCase();
      var nosohso = $("#nospk").val();
      if (nosohso===null){
        var nospk = '';
      }else{
        var nospk = nosohso.toUpperCase();
      }
      var mesin = $("#nosin").val();                      var nosin = mesin.replace("-","");                      var nosin = nosin.replace("-","");
      var nosin = nosin.replace("-","");                  var nosin = nosin.toUpperCase();
      var nodo = $("#nodo").val();                        var nodo = nodo.toUpperCase();

      var kdsales = $("#kdsales").val();
      var ket_astra = $("#ket_astra").val();
      var kdleasing = $("#kdleasing").val();              var kdleasing = kdleasing.toUpperCase();
      if(kdleasing==='T'){
        var jnsbayar = 'T';
        var kdprogleas = '0.00';
        var disc = $("#diskon").autoNumeric('get');
          if (disc>0){
            var disc = $("#diskon").autoNumeric('get');
          } else {
            var disc = '0';
          }
        var um = '0.00';

        var disc_leasing = '0.00';
        var angsuran = '0.00';   var tenor = '0.00';
      } else {
        var jnsbayar = 'K';
        var kdprogleas = $("#progleas").val();
        var disc = $("#diskon1").autoNumeric('get');    var um = $("#um").autoNumeric('get');

        var disc_leasing = $("#sub_leas").autoNumeric('get');
        var angsuran = $("#angsuran").autoNumeric('get');   var tenor = $("#tenor").val();
      }
      var stat_otr = $("#stat_otr").val();
      var promo = $("#promo").val();

      var hrg_otr = $("#hrg_otr").autoNumeric('get');

      var nama = $("#nama1").val();          var nama = nama.toUpperCase();       var nama_s = $("#nama2").val();               var nama_s = nama_s.toUpperCase();
      var alamat = $("#alamat1").val();      var alamat = alamat.toUpperCase();   var alamat_s = $("#alamat2").val();           var alamat_s = alamat_s.toUpperCase();
                                                                                  var alamatktp_s = $("#alamattt").val();       var alamatktp_s = alamatktp_s.toUpperCase();
      var kel = $("#kel1").val();            var kel = kel.toUpperCase();         var kel_s = $("#kel2").val();                 var kel_s = kel_s.toUpperCase();
      var nohp = $("#nohp1").val();          var nohp = nohp.toUpperCase();       var nohp_s = $("#nohp2").val();               var nohp_s = nohp_s.toUpperCase();
      var kec = $("#kec1").val();            var kec = kec.toUpperCase();         var kec_s = $("#kec2").val();                 var kec_s = kec_s.toUpperCase();
      var notelp = $("#notelp").val();       var notelp = notelp.toUpperCase();   var notelp_s = $("#notelp").val();            var notelp_s = notelp_s.toUpperCase();
      var npwp = $("#npwp").val();           var npwp = npwp.toUpperCase();       var noktp = $("#noktp").val();                var noktp = noktp.toUpperCase();
      var kota = $("#kota1").val();          var kota = kota.toUpperCase();       var kota_s = $("#kota2").val();               var kota_s = kota_s.toUpperCase();
      var kdkerja = $("#kdkerja").val();

      var ket = $("#keterangan").val();      var ket = ket.toUpperCase();

      var tgldo = $("#tgldo").val();

      if ($("#kirim_tagih").prop("checked")){
          var kirim_tagih = 'true';
          var nilai_kt = $("#nilai_kt").autoNumeric('get');
      } else {
        var kirim_tagih = 'false';
        var nilai_kt = '0';
      }

      var insentif = $("#insentif").val();
      var kdaks1 = $("#kdaks1").val();
      var qtyaks1 = $("#qtyaks1").val();
      var kdaks2 = $("#kdaks2").val();
      var qtyaks2 = $("#qtyaks2").val();
      var kdaks3 = $("#kdaks3").val();
      var qtyaks3 = $("#qtyaks3").val();
      var kdaks4 = $("#kdaks4").val();
      var qtyaks4 = $("#qtyaks4").val();

        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/updatedo");?>",
            data: {"noso":noso
                    ,"tglso":tglso
                    ,"nosin":nosin
                    ,"nodo":nodo
                    ,"kdsales":kdsales
                    ,"jnsbayar":jnsbayar
                    ,"ket_astra":ket_astra
                    ,"kdleasing":kdleasing
                    ,"kdprogleas":kdprogleas
                    ,"stat_otr":stat_otr
                    ,"hrg_otr":hrg_otr
                    ,"um":um
                    ,"disc":disc
                    ,"disc_leasing":disc_leasing
                    ,"nama":nama
                    ,"nama_s":nama_s
                    ,"alamat":alamat
                    ,"alamat_s":alamat_s
                    ,"alamatktp_s":alamatktp_s
                    ,"kel":kel
                    ,"kel_s":kel_s
                    ,"nohp":nohp
                    ,"nohp_s":nohp_s
                    ,"kec":kec
                    ,"kec_s":kec_s
                    ,"notelp":notelp
                    ,"notelp_s":notelp_s
                    ,"npwp":npwp
                    ,"noktp":noktp
                    ,"kota":kota
                    ,"kota_s":kota_s
                    ,"angsuran":angsuran
                    ,"tenor":tenor
                    ,"ket":ket
                    ,"tgldo":tgldo
                    ,"kirim_tagih":kirim_tagih
                    ,"nilai_kt":nilai_kt
                    ,"insentif":insentif
                    ,"nospk":nospk
                    ,"kdkerja":kdkerja
                    ,"kdaks1":kdaks1
                    ,"kdaks2":kdaks2
                    ,"kdaks3":kdaks3
                    ,"kdaks4":kdaks4
                    ,"qtyaks1":qtyaks1
                    ,"qtyaks2":qtyaks2
                    ,"qtyaks3":qtyaks3
                    ,"qtyaks4":qtyaks4
                    ,"promo":promo},
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getsubleas(){
      var um = $("#um").autoNumeric('get');
      var sub_leas = $("#sub_leas").autoNumeric('get');

      var result = parseFloat(um) - parseFloat(sub_leas);
      if (!isNaN(result)){
        if(result<0){
          swal({
              title: "Data Error",
              text: "Beban Leasing Tidak Boleh Melebih Uang Muka",
              type: "warning"
          });

          $("#sub_leas").autoNumeric('set',0);
        }
      }
    }

    function gettotal(){
      var um = $("#um").autoNumeric('get');
      var diskon = $("#diskon1").autoNumeric('get');

      var result = parseFloat(um) - parseFloat(diskon);
      if (!isNaN(result)){
          $("#total").autoNumeric('set',result);
      }
    }
    // no id
    function GetNoID(){
      var tg = new Date();
      var tgl = tg.toString();
      var tanggal = tgl.substring(13,15);
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#noso').mask(value.kode+"99-999999");
                  $('#nodo').mask(value.kode+"99-999999");
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
    // no id
    function GetNoID_ADD(){
      var tg = new Date();
      var tgl = tg.toString();
      var tanggal = tgl.substring(11,15);
      var kode = tgl.substring(13,15);
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $.mask.definitions['9'] = '';
                  $.mask.definitions['d'] = '[0-9]';
                  // $('#noso').mask(value.kode+kode+"-dddddd");
                  if(value.jml > 99999){
                      $('#nodo').mask(value.kode+kode+"-dddddd");
                      $('#nodo').val(value.kode+kode+"-"+value.jml);
                  }else if(value.jml > 9999){
                      $('#nodo').mask(value.kode+kode+"-dddddd");
                      $('#nodo').val(value.kode+kode+"-0"+value.jml);
                  }else if(value.jml > 999){
                      $('#nodo').mask(value.kode+kode+"-dddddd");
                      $('#nodo').val(value.kode+kode+"-00"+value.jml);
                  }else if(value.jml > 99){
                      $('#nodo').mask(value.kode+kode+"-dddddd");
                      $('#nodo').val(value.kode+kode+"-000"+value.jml);
                  }else if(value.jml > 9){
                      $('#nodo').mask(value.kode+kode+"-dddddd");
                      $('#nodo').val(value.kode+kode+"-0000"+value.jml);
                  }else if(value.jml > 0){
                      $('#nodo').mask(value.kode+kode+"-dddddd");
                      $('#nodo').val(value.kode+kode+"-00000"+value.jml);
                  }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function deletedo(){
      var noso = $('#noso').val();
      var noso = noso.toUpperCase();
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("donew/deletedo");?>",
                    data: {"noso":noso },
                    success: function(resp){
                      var obj = JSON.parse(resp);
                      $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            window.location.reload();
                        });
                    });
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }

    function disabled(){

      $(".box-new").hide();                         $(".box-view").show();
      $("#noso").attr('disabled',false);            $("#nosopk").attr('disabled',false);          $("#nodo").attr('disabled',false);
      $("#tglso").attr('disabled',true);            $("#tgldo").attr('disabled',true);            $("#nama1").attr('disabled',true);
      $("#nama2").attr('disabled',true);            $("#alamat1").attr('disabled',true);          $("#alamat2").attr('disabled',true);
      $("#alamattt").attr('disabled',true);         $("#kel1").attr('disabled',true);             $("#kel2").attr('disabled',true);
      $("#nohp1").attr('disabled',true);            $("#nohp2").attr('disabled',true);            $("#kec1").attr('disabled',true);
      $("#kec2").attr('disabled',true);             $("#kota1").attr('disabled',true);            $("#kota2").attr('disabled',true);
      $("#kdkerja").attr('disabled',true);
      $("#notelp").attr('disabled',true);           $("#noktp").attr('disabled',true);            $("#npwp").attr('disabled',true);
      $("#nosin").attr('disabled',true);            $("#norangka").attr('disabled',true);         $("#stat_otr").attr('disabled',true);
      $("#hrg_otr").attr('disabled',true);          $("#kdtipe").attr('disabled',true);           $("#nmtipe").attr('disabled',true);
      $("#kdwarna").attr('disabled',true);          $("#nmwarna").attr('disabled',true);          $("#tahun").attr('disabled',true);
      $("#ket_astra").attr('disabled',true);

      $("#promo").attr('disabled',true);
      $("#kdleasing").attr('disabled',true);        $("#progleas").attr('disabled',true);         $("#um").attr('disabled',true);
      $("#sub_leas").attr('disabled',true);         $("#diskon").attr('disabled',true);           $("#diskon1").attr('disabled',true);
      $("#tenor").attr('disabled',true);            $("#angsuran").attr('disabled',true);
      $("#insentif").attr('disabled',true);         $("#kirim_tagih").attr('disabled',true);      $("#nilai_kt").attr('disabled',true);
      $("#kdsales").attr('disabled',true);          $("#nmsales").attr('disabled',true);          $("#nmspv").attr('disabled',true);
      $("#nmpos").attr('disabled',true);            $("#keterangan").attr('disabled',true);       $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").hide();
    }

    function clear(){
      $('#tglso').val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $('#tgldo').val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nospk").val('');                          $("#noso").val('');                           $("#nodo").val('');
      $("#kirim_tagih").prop("checked",false);      $("#nama1").val('');                          $("#nama2").val('');
      $("#alamat1").val('');                        $("#alamat2").val('');                        $("#alamattt").val('');
      $("#kel1").val('');                           $("#kel2").val('');                           $("#nohp1").val('');
      $("#nohp2").val('');                          $("#kec1").val('');                           $("#kec2").val('');
      $("#kota1").val('');                          $("#kota2").val('');                          $("#notelp").val('');
      $("#kdkerja").val('');
      $("#noktp").val('');                          $("#npwp").val('');                           $("#nosin").val('');
      $("#norangka").val('');                       $("#kdtipe").val('');                         $("#nmtipe").val('');
      $("#kdwarna").val('');                        $("#nmwarna").val('');                        $("#tahun").val('');
      $("#stat_otr").val('');                       $("#hrg_otr").val('');                        $("#kdleasing").val('T');     $("#promo").val('T');
      $("#ket_astra").val('T');
      $("#progleas").val('');                       $("#um").val('');                             $("#sub_leas").val('');
      $("#total").val('');                          $("#tenor").val('');                          $("#angsuran").val('');
      $("#diskon").val('');                         $("#diskon1").val('');                        $("#nilai_kt").val('');
      $("#insentif").val('0.00');                   $("#kdsales").val('');                        $("#nmsales").val('');
      $("#nmspv").val('');                          $("#nmpos").val('');                          $("#keterangan").val('');
      $(".btn-edit").prop('disabled',true);         $(".btn-del").prop('disabled',true);

      $("#kdsales").trigger("chosen:updated");
      $('#kdsales').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
      });

        $('.lr').hide();
        $('.bbn').hide();

    }

    function dis_nosin(){
      $('#norangka').val('');
      $('#kdtipe').val('');
      $("#kdtipe").trigger("chosen:updated");
      $('#kdtipe').select2({
          dropdownAutoWidth : true,
          width: '100%'
      });
      $('#nmtipe').val('');
      $('#kdwarna').val('');
      $("#kdwarna").trigger("chosen:updated");
      $('#kdwarna').select2({
          dropdownAutoWidth : true,
          width: '100%'
      });
      $("#nmwarna").val('');
      $("#tahun").val(''); 
      $("#stat_otr").val('');
      $("#stat_otr").attr('disabled',true);
      $("#hrg_otr").autoNumeric('set','0');
      $("#hrg_otr").attr('disabled',true); 
      $("#kdleasing").attr('disabled',true); 
      $("#progleas").attr('disabled',true);
      $("#um").attr('disabled',true);
      $("#sub_leas").attr('disabled',true);
      $("#angsuran").attr('disabled',true);
      $("#diskon").attr('disabled',true);
      $("#diskon1").attr('disabled',true);
      $("#tenor").attr('disabled',true);
      $("#kdsales").attr('disabled',true);
      $("#insentif").attr('disabled',true);
      aksdisabled();
      $("#kirim_tagih").attr('disabled',true);
      $("#nilai_kt").attr('disabled',true);
      $("#keterangan").attr('disabled',true); 

    }

    function set_stiker(alamat,nodo){
      $("#alamat_stnk").val(alamat);
      $("#alamat_ctk").val(alamat);
      $("#nodo_ctk").val(nodo);
    }

    function set_kpb(kdtipe,nodo){
      $("#kdtipe_ctk").val(kdtipe);
      $("#nodo_ctk2").val(nodo);
    }

    function clear_modal(){
      $("#margin_up").val('');
      $("#alamat_stnk").val('');
      $("#alamat_ctk").val('');
    }

    function clear_modal2(){
      $("#margin_up").val('');
      $("#kdtipe_ctk").val('');
    }

    //Cetak

    function ctk_disabled(){
      $(".btn-ctk-do").attr("disabled",true);
      $(".btn-ctk-um").attr("disabled",true);
      $(".btn-ctk-stkpb").attr("disabled",true);
      $(".btn-ctk-srv").attr("disabled",true);
    }

    function ctk_enabled(){
      $(".btn-ctk-do").attr("disabled",false);
      $(".btn-ctk-um").attr("disabled",false);
      $(".btn-ctk-stkpb").attr("disabled",false);
      $(".btn-ctk-srv").attr("disabled",false);
    }

    function ctk_do(){
      var nodo = $("#nodo").val();
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/ctk_do");?>",
            data: {"nodo":nodo }
        });
    } 

    function ctk_um(){
      var nodo = $("#nodo").val();
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/ctk_um");?>",
            data: {"nodo":nodo },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                });
            }
        });
    }

    function ctk_stiker(){
      var nodo_ctk = $("#nodo_ctk").val();
      var alamat_ctk = $("#alamat_ctk").val();
      var margin_up = $("#margin_up").val();
      var margin = margin_up - 6;
      var nodo = nodo_ctk.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/ctk_stiker");?>",
            data: {"nodo":nodo,"margin_up":margin,"alamat_ctk":alamat_ctk },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                });
            }
        });
    }

    function ctk_kredit(){
      var nodo = $("#nodo").val(); ;
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("donew/ctk_kredit");?>",
            data: {"nodo":nodo },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                });
            }
        });
    }


</script>

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Doinnew_qry
 *
 * @author adi
 */
class Doinnew_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    } 

    public function getNoID() { 
      $tgldoin  = $this->apps->dateConvert($this->input->post('tgldoin')); 
      $query = $this->db->query("select (select kddoi from mst.divisi where kddiv = '". $this->session->userdata('data')['kddiv'] ."') as kddoi, 
                                        (select COALESCE(max(SUBSTRING(nodoin,10,3))::int+1,1) as jml from pzu.t_do_intern where periode = date_to_periode('".$tgldoin."')), 
                                        SUBSTRING(date_to_periode('".$tgldoin."'),3,4) as prd");
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getNoID_idx() {  
      $query = $this->db->query("select (select kddoi from mst.divisi where kddiv = '". $this->session->userdata('data')['kddiv'] ."') as kddoi, 
                                        (select COALESCE(max(SUBSTRING(nodoin,10,3))::int+1,1) as jml from pzu.t_do_intern where periode = date_to_periode(now()::date)), 
                                        SUBSTRING(date_to_periode(now()::date),3,4) as prd");
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function setnoid() { 
      $query = $this->db->query("select coalesce(SUBSTRING(max(b.nodo),5,6)::int+1,1) as jml from pzu.t_do_intern where kddiv = '". $this->session->userdata('data')['kddiv'] ."'");
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function setnodoin() {
      $nodoin = $this->input->post('nodoin');
      $query = $this->db->query("select * from pzu.vm_do_intern where nodoin =  '".$nodoin."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "";
      }
      return json_encode($res);
    }

    public function setnosin() {
      $nosin = $this->input->post('nosin');
      $query = $this->db->query("select * from pzu.vl_stock where nosin2 =  '".$nosin."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "";
      }
      return json_encode($res);
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
       $this->db->where('status','I');
       $this->db->where('faktif',true);
        $q = $this->db->get("pzu.supplier");
        return $q->result_array();
    }

    public function add_doin() { 
        $tgldoin  = $this->apps->dateConvert($this->input->post('tgldoin')); 
        $kdsup    = $this->input->post('kdsup');
        $nosin    = $this->input->post('nosin');
        $nosin    = str_replace('-', '', $nosin);
        $nilai    = $this->input->post('nilai'); 
        $ket      = $this->input->post('ket');  

        $q = $this->db->query("select title,msg,tipe from pzu.dointern_ins_w('".$tgldoin."', 
                                                                            '".$this->session->userdata('data')['kddiv']."',
                                                                            ".$kdsup.",
                                                                            '".$nosin."',
                                                                            ".$nilai.",
                                                                            '".$ket."', 
                                                                            '".$this->session->userdata("username")."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

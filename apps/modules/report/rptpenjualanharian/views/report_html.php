<?php
    $periode_awal = $this->input->post("periode_awal");
?>
<div class="row" style="width: 500px;">
    <div class="col-lg-12 text-center" style="font-size: 18px;">
        <?=strtoupper($this->session->userdata('data')['cabang']);?>
    </div>
    <div class="col-lg-12  text-center" style="font-size: 18px;">
        LAPORAN PENJUALAN HARIAN
    </div>
    <div class="col-lg-12  text-center" style="font-size: 18px;">
        TANGGAL <?=$this->apps->dateConvert($periode_awal);?>
    </div>
    <div class="col-lg-12">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td style="width: 10px;">NO</td>
                    <td style="">NAMA SPV</td>
                    <td style="width: 150px;">TOTAL PENJUALAN</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $total = 0;
                foreach ($rpt as $value) {
                    echo '<tr>';
                    echo '<td class="text-center">'.$value['no'].'</td>';
                    echo '<td>'.$value['nmsales_header'].'</td>';
                    echo '<td class="text-right">'.$value['jml'].'</td>';
                    echo '</tr>';
                    $total+=$value['jml'];
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class="text-right" colspan="2">TOTAL</td>
                    <td class="text-right"><?=$total;?></td>
                </tr>                
            </tfoot>
        </table>
    </div>
</div>
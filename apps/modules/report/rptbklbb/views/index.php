<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>

			<div class="box-body">

				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<?=form_label($form['kddiv']['placeholder']);?>
							<div class="input-group">
								<?php
									echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
									echo form_error('kddiv', '<div class="note">', '</div>');
								?>
								<div class="input-group-btn">
									<button type="button" class="btn btn-info btn-set" data-dismiss="fileinput" id="btn-set">Set</button>
									<button type="button" class="btn btn-success btn-reset" data-dismiss="fileinput" id="btn-unset">Unset</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_input($form['kddiv2']);
								echo form_error('kddiv2', '<div class="note">', '</div>');
							?>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="form-group">
							<?php
								echo form_input($form['nmdiv']);
								echo form_error('nmdiv', '<div class="note">', '</div>');
							?>
						</div>
					</div>

				</div>




				<div class="row">
					<div class="col-xs-12">
						<div style="border-top: 1px solid #ddd; height: 10px;"></div>
					</div>
					<div class="col-xs-12">
						<div style="border-top: 0px solid #ddd; height: 10px;"></div>
					</div>
				</div>


			<div class="form-main main-form">

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Kode Akun <small>(bisa dipilih lebih dari satu akun)</small>');
								echo '</div>';
								echo form_dropdown($form['akun_multi']['name'],$form['akun_multi']['data'] ,$form['akun_multi']['value'] ,$form['akun_multi']['attr']);
								echo form_error('akun_multi','<div class="note">','</div>');
							?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Kode Akun Awal');
								echo '</div>';
								echo form_dropdown($form['akun_awal']['name'],$form['akun_awal']['data'] ,$form['akun_awal']['value'] ,$form['akun_awal']['attr']);
								echo form_error('akun_awal','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Kode Akun Akhir');
								echo form_dropdown($form['akun_akhir']['name'],$form['akun_akhir']['data'] ,$form['akun_akhir']['value'] ,$form['akun_akhir']['attr']);
								echo form_error('akun_akhir','<div class="note">','</div>');
							?>
						</div>

					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Periode Awal');
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			</div>

			<div class="box-footer">
				<button type="submit" class="btn btn-primary" name="submit" value="html">
					<i class="fa fa-print"></i> Cetak
				</button>
				<button type="submit" class="btn btn-success" name="submit" value="excel">
					<i class="fa fa-file-excel-o"></i> Export Ke Excel
				</button>
				<a href="<?php echo $reload;?>" class="btn btn-default">
					Batal
				</a>
			</div>
			<?php echo form_close(); ?>
		<!-- /.box -->
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function () {


		disabled();


		$('.btn-set').click(function(event){
			enabled();
			getAkun();

			//var nmdiv = $('option:selected', this).html();
			//$('#nmdiv').val(nmdiv);

			$('#kddiv2').val( $("#kddiv").val() );
		});

		$(".btn-reset").click(function(){
			window.location.reload();
		});



		$("#akun_multi").prop("disabled", true);
		$("#akun_awal").prop("disabled", true);
		$("#akun_akhir").prop("disabled", true);
		$("#rdakun_multi").click(function(){
			setAkun();
		});
		$("#rdakun_between").click(function(){
			setAkun();
		});
		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});
		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});



		
		var nmdiv = $('option:selected', this).html();
		$('#nmdiv').val(nmdiv);

		$("#kddiv").change(function(){
			nmdiv = $('option:selected', this).html();
			$('#nmdiv').val(nmdiv);
		});
		





		function disabled(){
			$("#kddiv").attr("disabled",false);
			$(".btn-set").show();
			$(".btn-reset").hide();
			$('.form-main').hide();
		}

		function enabled(){
			$("#kddiv").attr("disabled",true);
			$(".btn-set").hide();
			$(".btn-reset").show();
			$('.form-main').show();
		}


	});

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#akun_multi").prop("disabled", false);
			$("#akun_awal").prop("disabled", true);
			$("#akun_akhir").prop("disabled", true);
		}else{
			$("#akun_multi").prop("disabled", true);
			$("#akun_awal").prop("disabled", false);
			$("#akun_akhir").prop("disabled", false);

		}
	}


	function getAkun(){
		 var kddiv = $("#kddiv").val();
		 //alert(kddiv);
		  $.ajax({
			  type: "POST",
			  url: "<?=site_url("rptbklbb/getAkun");?>",
			  data: {"kddiv":kddiv},
			  beforeSend: function() {
				  $('#akun_multi').html("")
								.append($('<option>', { value : '' })
								.text('-- Pilih Kode Akun --'));
				  $('#akun_awal').html("")
								.append($('<option>', { value : '' })
								.text('-- Pilih Kode Akun --'));
				  $('#akun_akhir').html("")
								.append($('<option>', { value : '' })
								.text('-- Pilih Kode Akun --'));
				  $("#akun_multi").trigger("change.chosen");
				  $("#akun_awal").trigger("change.chosen");
				  $("#akun_akhir").trigger("change.chosen");
			  },
			  success: function(resp){
				  var obj = jQuery.parseJSON(resp);
				  $.each(obj, function(key, value){
					  $('#akun_multi')
						  .append($('<option>', { value : value.kdakun })
						  .html("<b style='font-size: 14px;'>" + value.kdakun + " - " + value.nmakun + " </b>"));
					  $('#akun_awal')
						  .append($('<option>', { value : value.kdakun })
						  .html("<b style='font-size: 14px;'>" + value.kdakun + " - " + value.nmakun + " </b>"));
					  $('#akun_akhir')
						  .append($('<option>', { value : value.kdakun })
						  .html("<b style='font-size: 14px;'>" + value.kdakun + " - " + value.nmakun + " </b>"));
				  });

				  $('#akun_multi').select2({
					  placeholder: '-- Pilih Akun --',
					  dropdownAutoWidth : true,
					  width: '100%',
					  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					//  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
					//  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
				  });

				  $('#akun_awal').select2({
					  placeholder: '-- Pilih Kode Akun Awal --',
					  dropdownAutoWidth : true,
					  width: '100%',
					  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					//  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
					//  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
				  });

				  $('#akun_akhir').select2({
					  placeholder: '-- Pilih Kode Akun Akhir --',
					  dropdownAutoWidth : true,
					  width: '100%',
					  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					//  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
					//  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
				  });

				  function formatSalesHeader (repo) {
					  if (repo.loading) return "Mencari data ... ";
					  var separatora = repo.text.indexOf("[");
					  var separatorb = repo.text.indexOf("]");
					  var text = repo.text.substring(0,separatora);
					  var status = repo.text.substring(separatora+1,separatorb);
					  var markup = "<b style='font-size: 14px;'>" + repo.kdakun + " - " + repo.nmakun + " </b>" ;
					  return markup;
				  }

				  function formatSalesHeaderSelection (repo) {
					  var separatora = repo.text.indexOf("[");
					  var text = repo.text.substring(0,separatora);
					  return text;
				  }
				  //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
			  },
			  error:function(event, textStatus, errorThrown) {
				  swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
			  }
		  });
	}

</script>

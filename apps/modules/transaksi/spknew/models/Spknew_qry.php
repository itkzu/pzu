<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Spknew_qry
 *
 * @author adi
 */
class Spknew_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function set_spk() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from pzu.vm_so a left join pzu.t_do b on a.noso = b.noso
                                                                      left join api.v_so c on a.nosohso = c.idspk where a.noso =  '".$noso."' and b.nodo is null");
      // echo $this->db->last_query();
      // $res = $q->result_array();
      // return json_encode($res);
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_spk2() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from pzu.vm_so a left join pzu.t_do b on a.noso = b.noso
                                                                      left join api.v_so c on a.nosohso = c.idspk where a.noso =  '".$noso."' and b.nodo is null");
      // echo $this->db->last_query();
      // $res = $q->result_array();
      // return json_encode($res);
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, coalesce(SUBSTRING(max(b.nodo),5,6)::int+1,1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function carispk() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select noso from pzu.t_so where noso = '" . $noso . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_apispk() {
      $nospk = $this->input->post('nospk');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from api.v_so where idspk =  '".$nospk."'  UNION select * from api.v_so where idspk =  '".$nospk."' AND noso = '" .$noso. "' ");//
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getKodeSales() {
      $noso = $this->input->post('noso');
      $this->db->where("faktif","true");
      $q = $this->db->get("pzu.v_sales");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getNama() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(idspk)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->or_like('LOWER(nmbpkb)', strtolower($q));
        $this->db->or_like('LOWER(noktp)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(idspk) like '".strtolower($q)."' then 1
                                when LOWER(nmbpkb) like '".strtolower($q)."%' then 2
                                when LOWER(noktp) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nmbpkb");
        $query = $this->db->get('api.v_so');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['idspk'],
                    'text' => $value['nmbpkb'],
                    'noktp' => $value['noktp'],
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => '',
                'noktp' => 'noktp',
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }
 
    public function getkdkota() {
      // $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.bbn_tarif");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res); 
    }

    public function getNoSPK() {
      $nospk = $this->input->post('nospk');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from api.v_so where idspk not in (select nosohso from pzu.t_so where nosohso != '' ) UNION select * from api.v_so where  noso = '" .$noso. "'");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getTipeUnit() {
      $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.v_tipe");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKdWarna() {
      $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.warna");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKodeSalesHeader() {
      $kdsales = $this->input->post('kdsales');
      $this->db->where("faktif","true");
      $this->db->where("kdsales",$kdsales);
      $q = $this->db->get("pzu.v_sales");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKodeTipeHeader() {
      $kdtipe = $this->input->post('kdtipe');
      // $this->db->where("faktif","true");
      $this->db->where("kode",$kdtipe);
      $q = $this->db->get("pzu.v_tipe");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKdWarnaHeader() {
      $kdwarna = $this->input->post('kdwarna');
      // $this->db->where("faktif","true");
      $this->db->where("kdwarna",$kdwarna);
      $q = $this->db->get("pzu.warna");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.get_divisi()");
        return $q->result_array();
    }

    public function getDetailPO() {
        $this->db->select("nopo, to_char(tglpo,'DD-MM-YYYY') AS tglpo, nap"
                . " , 0 as ndef, '' as nourut ");
        $nopo = $this->input->post('nopo');
        $kddiv = $this->input->post('kddiv');
        $this->db->where("kddiv",$kddiv);
        $this->db->where("nopo",$nopo);
        $this->db->where("nodf IS NULL");
        $q = $this->db->get("pzu.v_df_po");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function addspk() {
      $noso           = $this->input->post('noso');
      $tglso          = $this->apps->dateConvert($this->input->post('tglso'));
      $nospk          = $this->input->post('nospk');
      $finden         = $this->input->post('finden');
      $nama           = $this->input->post('nama');
      $nama_s         = $this->input->post('nama_s');
      $alamat         = $this->input->post('alamat');
      $alamat_s       = $this->input->post('alamat_s');
      $alamat_ktps    = $this->input->post('alamat_ktps');
      $kel            = $this->input->post('kel');
      $kel_s          = $this->input->post('kel_s');
      $nohp           = $this->input->post('nohp');
      $nohp_s         = $this->input->post('nohp_s');
      $kec            = $this->input->post('kec');
      $kec_s          = $this->input->post('kec_s');
      $notelp         = $this->input->post('notelp');
      $npwp           = $this->input->post('npwp');
      $noktp           = $this->input->post('noktp');
      $kota           = $this->input->post('kota');
      $kota_s         = $this->input->post('kota_s');
      $kdtipe         = $this->input->post('kdtipe');
      $nmtipe         = $this->input->post('nmtipe');
      $kdwarna        = $this->input->post('kdwarna');
      $nmwarna        = $this->input->post('nmwarna');
      $tpbayar        = $this->input->post('tpbayar');
      $kdsales        = $this->input->post('kdsales');
      $kdsales_header = $this->input->post('kdsales_header');
      $kdprogleas     = '0';
      $um     = '0';
      $disc   = '0';
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.so_ins('" . $noso . "',
                                                                      '" . $tglso . "',
                                                                      " . $kdsales . ",

                                                                      '" .  $kdtipe . "',
                                                                      '" . $nmtipe . "',
                                                                      '" . $kdwarna . "',

                                                                      '" . $tpbayar . "',
                                                                       '',
                                                                     " . $kdprogleas . ",

                                                                      " . $um . ",
                                                                      " . $disc . ",

                                                                      '" . $nama . "',
                                                                      '" . $alamat . "',
                                                                      '" . $kel . "',
                                                                      '" . $kec . "',
                                                                      '" . $kota . "',
                                                                      '" . $nohp . "',
                                                                      '" . $notelp . "',
                                                                      '" . $npwp . "',
                                                                      
                                                                      '" . $nama_s . "',
                                                                      '" . $alamat_s . "',
                                                                      '" . $alamat_ktps . "',
                                                                      '" . $kel_s . "',
                                                                      '" . $kec_s . "',
                                                                      '" . $kota_s . "',
                                                                      '" . $nohp_s . "',
                                                                      '',
                                                                      '".$finden."',
                                                                      '". $this->session->userdata('username') ."',
                                                                      '".$nospk."',
                                                                      '".$noktp."' )");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updatespk() {
      $noso           = $this->input->post('noso');
      $tglso          = $this->apps->dateConvert($this->input->post('tglso'));
      $nospk          = $this->input->post('nospk');
      $finden         = $this->input->post('finden');
      $nama           = $this->input->post('nama');
      $nama_s         = $this->input->post('nama_s');
      $alamat         = $this->input->post('alamat');
      $alamat_s       = $this->input->post('alamat_s');
      $kel            = $this->input->post('kel');
      $kel_s          = $this->input->post('kel_s');
      $nohp           = $this->input->post('nohp');
      $nohp_s         = $this->input->post('nohp_s');
      $kec            = $this->input->post('kec');
      $kec_s          = $this->input->post('kec_s');
      $notelp         = $this->input->post('notelp');
      $npwp           = $this->input->post('npwp');
      $noktp           = $this->input->post('noktp');
      $kota           = $this->input->post('kota');
      $kota_s         = $this->input->post('kota_s');
      $kdtipe         = $this->input->post('kdtipe');
      $nmtipe         = $this->input->post('nmtipe');
      $kdwarna        = $this->input->post('kdwarna');
      $nmwarna        = $this->input->post('nmwarna');
      $tpbayar        = $this->input->post('tpbayar');
      $kdsales        = $this->input->post('kdsales');
      $kdsales_header = $this->input->post('kdsales_header');
      $kdprogleas     = '0';
      $um     = '0';
      $disc   = '0';

        $q = $this->db->query("select title,msg,tipe from pzu.so_api_upd('" . $noso . "',
                                                                          '" . $tglso . "',
                                                                          " . $kdsales . ",
                                                                          '" .  $kdtipe . "',
                                                                          '" . $nmtipe . "',
                                                                          '" . $kdwarna . "',
                                                                          '" . $tpbayar . "',
                                                                          '',
                                                                          " . $kdprogleas . ",
                                                                          " . $um . ",
                                                                          " . $disc . ",
                                                                          '" . $nama . "',
                                                                          '" . $alamat . "',
                                                                          '" . $kel . "',
                                                                          '" . $kec . "',
                                                                          '" . $kota . "',
                                                                          '" . $nohp . "',
                                                                          '" . $notelp . "',
                                                                          '" . $npwp . "',
                                                                          '" . $nama_s . "',
                                                                          '" . $alamat_s . "',
                                                                          '" . $alamat_s . "',
                                                                          '" . $kel_s . "',
                                                                          '" . $kec_s . "',
                                                                          '" . $kota_s . "',
                                                                          '" . $nohp_s . "',
                                                                          '',
                                                                          '".$finden."',
                                                                          '". $this->session->userdata('username') ."',
                                                                          '".$nospk."',
                                                                          '".$noktp."' )");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function deletespk() {
        $noso = $this->input->post('noso');
        $q = $this->db->query("select title,msg,tipe from pzu.so_apidel('". $noso ."','". $this->session->userdata('username') ."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

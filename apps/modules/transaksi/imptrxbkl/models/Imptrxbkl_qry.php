<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Imptrxbkl_qry
 *
 * @author
 */

class Imptrxbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_div_unit_bkl()");
        return $q->result_array();
    }

    public function getkb(){
        $kddiv = $this->input->post('kddiv');

        $this->db->select("kdkb,nmkb");
        $this->db->where("kddiv",$kddiv);
        $this->db->where("faktif",true);
        $query = $this->db->get('bkl.kasbank');
        $this->db->order_by("jenis desc,nmkb");
      //  echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);

    }

    /*
    public function upload_file($filename){
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size']  = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }
    */

    public function insertBeli($data,$kddiv)
    { 
        if(count($data)>0){
            $this->db->empty_table('bkl.t_po_tmp');
            $this->db->insert_batch('bkl.t_po_tmp', $data);
        }
        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from bkl.po2_ins('".$kddiv."', '". $this->session->userdata('username') ."')");

            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
            return 'Error! '.$this->db->_error_message();
        }

        return json_encode($res);
    }

    public function insertServis($data,$kddiv)
    {
        if(count($data)>0){
            $this->db->empty_table('bkl.t_sv_tmp');
            $this->db->insert_batch('bkl.t_sv_tmp', $data); 
        }

        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from bkl.sv2_ins('".$kddiv."','". $this->session->userdata('username') ."')");
            // echo $this->db->last_query();
            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
            return 'Error! '.$this->db->_error_message();
        }
        return json_encode($res);
    }

    public function insertJual($data,$kddiv)
    {
        if(count($data)>0){
            $this->db->empty_table('bkl.t_so_tmp');
            $this->db->insert_batch('bkl.t_so_tmp', $data); 
        }
        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from bkl.so2_ins('".$kddiv."','". $this->session->userdata('username') ."')");
            // echo $this->db->last_query();
            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
            return 'Error! '.$this->db->_error_message();
        }
        return json_encode($res);
    }

    public function insertKB($data,$kddiv,$kdkb)
    {
        $this->db->empty_table('bkl.t_kb_tmp');
        $this->db->insert_batch('bkl.t_kb_tmp', $data); 
        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from bkl.kb_ins('".$kddiv."','".$kdkb."','". $this->session->userdata('username') ."')");
            //return 'Data Kas & Bank sukses di Import';
            //echo $this->db->last_query();
            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
            return 'Error! '.$this->db->_error_message();
        }
        return json_encode($res);
    }

}

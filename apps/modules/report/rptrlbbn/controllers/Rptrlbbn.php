<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptrlbbn
 *
 * @author adi
 */
class Rptrlbbn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptrlbbn/submit'),
            'add' => site_url('rptrlbbn/add'),
            'edit' => site_url('rptrlbbn/edit'),
            'reload' => site_url('rptrlbbn'),
        );
        $this->load->model('rptrlbbn_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $this->data['rpt'] = $this->rptrlbbn_qry->submit();
        if(empty($this->data['rpt'])){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('debug/err_000',$this->data);
        }else{
            $array = $this->input->post();
            if($array['submit']==="html" || $array['submit']==="excel"){
                $this->template
                    ->title($this->data['msg_main'],$this->apps->name)
                    ->set_layout('print-layout')
                    ->build('html',$this->data);            
            }else{
                redirect("rptrlbbn");            
            }    
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
           'tbj'=> array(
                    'placeholder' => 'No. TBJ',
                    'id'          => 'tbj',
                    'name'        => 'tbj',
                    'value'       => "",
                    'class'       => 'form-control',
            ),
           'do'=> array(
                    'placeholder' => 'No. DO',
                    'id'          => 'do',
                    'name'        => 'do',
                    'value'       => "",
                    'class'       => 'form-control',
            ),
        );
    }
}

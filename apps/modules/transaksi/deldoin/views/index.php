<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <?php
                                echo form_label($form['nodoin']['placeholder']);
                            ?>  

                            <div class="input-group">
                                <?php
                                    echo form_input($form['nodoin']);
                                    echo form_error('nodoin','<div class="note">','</div>');
                                ?>

                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-tampil" id="btn-tampil">Tampil</button>
                                    <button type="button" class="btn btn-success btn-reset">Reset</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- detail transaksi via ajax -->
				<div class="table-responsive"> 

					<!--<table style="width: 2500px;"  class="table table-bordered table-hover js-basic-example dataTable">-->
					<table class="table table-bordered table-hover js-basic-example dataTable">
						<thead style="background-color: #effffc ">
							<tr> 
                                <th style="width: 10px;text-align: center;">No.</th>
								<th style="text-align: center;">No. DO Intern</th> 
								<th style="text-align: center;">Tgl DO Intern</th>
								<th style="text-align: center;">Ke Cabang</th>
								<th style="text-align: center;">No Mesin</th>
								<th style="text-align: center;">Tipe</th>
                                <th style="text-align: center;">Warna</th>
                                <th style="width: 25px;text-align: center;">Tahun</th>
                                <th style="width: 50px;text-align: center;">Nilai</th>
								<th style="width: 10px;text-align: center;">
									<i class="fa fa-th-large"></i>
							</tr>
						</thead>
						<tbody id="tbodyid"></tbody>
                        <!--
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
                        -->
					</table>
				</div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#nodoin").mask("***-****-***");

        //init
        $(".btn-reset").hide();
        $("#nodoin").focus();

        $(".btn-tampil").click(function(){
            getData();
        });

        $(".btn-reset").click(function(){
            reset();
        });

        $(".btn-unposting").click(function(){
            unposting();
        });




        var input = document.getElementById("nodoin");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("btn-tampil").click();
            }
        });  
    });



    function buttonState(){
        $(".btn-tampil").toggle();
        $(".btn-reset").toggle();
    }

    function reset(){
        //$(".form-control").val("");
        buttonState();

        //$("#nodoin").val('');
        $("#nodoin").prop("readonly", false);
        $("#nodoin").focus(); $("#nodoin").select();

        //$('.dataTable').DataTable().fnClearTable();
        table.clear(); //table.draw();
        $("#tbodyid").empty();
    }

    function getData(){
        var nodoin = $("#nodoin").val();
        var nodoin = nodoin.toUpperCase();
        $("#nodoin").prop("readonly", true);
        buttonState();

        var column = [];

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        column.push({
            "aTargets": [ 0,1,2,3,4,5,6,7,8,9 ],
            "orderable": false
        });

        column.push({
            "aTargets": [ 8 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right",
            "orderable": false
        });

        // column.push({
        //         "aTargets": [ 0 ],
        //         "searchable": false,
        //         "orderable": false, 
        // });


        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            //"orderable": false,
            "order": [],
            "fixedColumns": {
                leftColumns: 2
            }, 


            "lengthMenu": [[-1], ["Semua Data"]],
            //"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],

            //"bLengthChange": false,
            //"bPaging": false,
            "bPaginate": false,
            //"bInfo": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": true,
            "sAjaxSource": "<?=site_url('deldoin/getData');?>",
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "nodoin", "value": nodoin } );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },

            'rowCallback': function(row, data, index){
                    //if(data[23]){
                            //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                    //}
            },

            "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } ); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 


    }

    function bataldoin(nodoin){ 
        // alert(nodoin);
        swal({
            title: "Konfirmasi",
            text: "Proses DO Intern akan dibatalkan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            },

            function () { 
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('deldoin/proses');?>",
                    data: {"nodoin":nodoin},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                $("#nodoin").val('');
                                reset();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    } 

</script>

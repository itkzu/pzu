<?php
/*
 * ***************************************************************
 * Script : adh.php
 * Version : 
 * Date : Nov 21, 2017 10:11:14 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style type="text/css">
    .dt-buttons{
        margin-bottom: 10px;
    }
</style>
<!-- Main row -->
<div class="row">
    <div class="col-lg-12">
        <div class="box box-danger">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i> Periode
                                </div>
                                <?= form_input($form['periode_awal']); ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-tampil">Tampil</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                            <?= form_error('periode_awal', '<div class="note">', '</div>'); ?>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Penjualan Prima Zirang Utama <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_penjualan" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;" rowspan="2">No.</th>
                                        <th  style="text-align: center;" rowspan="2">Nama Cabang</th>
                                        <th style="width: 270px;text-align: center;" colspan="3">
                                            Bulan
                                            <span id="nm_bulan"></span>
                                        </th>
                                        <th style="width: 270px;text-align: center;" colspan="3">
                                            Tahun
                                            <span id="nm_tahun"></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" class="sum">MTD</th>
                                        <th style="width: 90px;text-align: center;" class="sum">MTD-1</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Selisih</th>
                                        <th style="width: 90px;text-align: center;" class="sum">YTD</th>
                                        <th style="width: 90px;text-align: center;" class="sum">YTD-1</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Selisih</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <h4><i class="fa fa-arrow-circle-up"></i> Target Penjualan Prima Zirang Utama <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_target_penjualan" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;">No.</th>
                                        <th  style="text-align: center;">Nama Cabang</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Target</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Aktual</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Selisih</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 90px;text-align: center;" colspan="2">TOTAL</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>  
                    </div>

                    <div class="col-lg-6">
                        <h4><i class="fa fa-tags"></i> Rekap DO Belum Terima PO Leasing <small>Semua Cabang</small></h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered trn_blm_trm_po_leasing" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th  style="width:10px;text-align: center;">No.</th>
                                        <th  style="text-align: center;">Nama Cabang</th>
                                        <th style="width: 140px;text-align: center;">Leasing</th>
                                        <th style="width: 90px;text-align: center;" class="sum">Jumlah DO</th>
                                        <th style="width: 90px;text-align: center;">Detail</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th style="text-align: center;" colspan="3">TOTAL</th>
                                        <th style="width: 90px;text-align: right;">0</th>
                                        <th style="width: 90px;text-align: right;">&nbsp;</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>  
    </div> 
</div>


<!-- Modal Chart -->
<div id="modal-detail-po-leasing" class="modal fade" role="dialog" style="overflow-y: scroll;">
    <div class="modal-dialog" style="width: 99%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Daftar DO Belum Terima PO Leasing</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered trn_blm_trm_po_leasing_detail" style="margin-top: 0px !important;">
                        <thead>
                            <tr>
                                <th  style="width:10px;text-align: center;">No.</th>
                                <th style="width: 140px;text-align: center;">Leasing</th>
                                <th style="width: 140px;text-align: left;">Program</th>
                                <th style="width: 90px;text-align: right;" class="sum">Pelunasan</th>
                                <th style="width: 140px;text-align: left;">No. DO</th>
                                <th style="width: 140px;text-align: left;">Tgl DO</th>
                                <th style="width: 140px;text-align: left;">Nama STNK</th>
                                <th style="width: 140px;text-align: left;">Jenis Unit</th>
                                <th style="width: 140px;text-align: left;">Warna</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align: center;" colspan="3">TOTAL</th>
                                <th style="text-align: right;">0</th>
                                <th style="text-align: right;" colspan="5">&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>

    </div>
</div>

<!-- /.row (main row) -->
<script>
    $(function () {
        var periode = $("#periode_awal").val();
        var mm = periode.substring(0, 2);
        var yyyy = periode.substring(3, 7);
//        $("#nm_bulan").html(bulan[Number(mm)]);
//        $("#nm_tahun").html(yyyy);
        $(".btn-tampil").click(function () {
            var periode = $("#periode_awal").val();
            mm = periode.substring(0, 2);
            yyyy = periode.substring(3, 7);
            trn_penjualan.ajax.reload();
            trn_target_penjualan.ajax.reload();
        });

        trn_penjualan = $('.trn_penjualan').DataTable({
            "columnDefs": [
                {className: "text-center", "targets": [0]}
                , {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
                , {className: "text-right", "targets": [5]}
                , {className: "text-right", "targets": [6]}
                , {className: "text-right", "targets": [7]}
            ],
            "fixedColumns": {
                leftColumns: 2
            },
            "ordering": false,
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push(
                        {"name": "key", "value": "pzu"}
                , {"name": "periode_awal", "value": $("#periode_awal").val()}
                );
                $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                });
            },
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    // console.log(sum); //alert(sum);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "sAjaxSource": "<?= site_url('dashallcabang/get_penjualan'); ?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
        });

        $('.trn_penjualan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // Target penjualan 
        trn_target_penjualan = $('.trn_target_penjualan').DataTable({
            "columnDefs": [
                {className: "text-center", "targets": [0]}
                , {className: "text-right", "targets": [2]}
                , {className: "text-right", "targets": [3]}
                , {className: "text-right", "targets": [4]}
            ],
            "fixedColumns": {
                leftColumns: 2
            },
            "ordering": false,
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push(
                        {"name": "key", "value": "pzu"}
                , {"name": "periode_awal", "value": $("#periode_awal").val()}
                );
                $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                });
            },
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    // console.log(sum); //alert(sum);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "sAjaxSource": "<?= site_url('dashallcabang/get_target_penjualan'); ?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'>> "
        });

        $('.trn_target_penjualan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        trn_blm_trm_po_leasing = $('.trn_blm_trm_po_leasing').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "cabang"},
                {"data": "kdleasing"},
                {"data": "count"},
                {"data": null,
                    render: function (data, type, row) {
                        var btn = '<center>' +
                                '<a class="btn btn-xs btn-info"' +
                                ' onclick="detail_po(\'' + row.cabang + '\', \'' + row.kdleasing + '\')"' +
                                ' href="javascript:void(0);">' +
                                'Detail' +
                                '</a>' +
                                '</center>';
                        return btn;
                    }
                }
            ],
            "lengthMenu": [[5, 10, 15, 20, 25, 50, -1], [5, 10, 15, 20, 25, 50, "Semua Data"]],
            "bProcessing": false,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });
        trn_blm_trm_po_leasing.on('order.dt search.dt', function () {
            trn_blm_trm_po_leasing.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        trn_blm_trm_po_leasing_detail = $('.trn_blm_trm_po_leasing_detail').DataTable({
            "ordering": false,
            "columns": [
                {"data": null},
                {"data": "kdleasing"},
                {"data": "nmprogleas"},
                {"data": "pl",
                    render: function (data, type, row) {
                        return type === 'export' ? data : numeral(data).format('0,0');
                    }
                },
                {"data": "nodo"},
                {"data": "tgldo"},
                {"data": "nama_s"},
                {"data": "nmtipe"},
                {"data": "nmwarna"}
            ],
            "columnDefs": [
                {className: "text-right", "targets": [3]}
            ],
            "lengthMenu": [[5, 10, 15, 20, 25, 50, -1], [5, 10, 15, 20, 25, 50, "Semua Data"]],
            "bProcessing": false,
            "bServerSide": false,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            'rowCallback': function (row, data, index) {
                //if(data[23]){
                //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                api.columns('.sum', {
                    page: 'current'
                }).every(function () {
                    var sum = this
                            .data()
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                    $(this.footer()).html(numeral(sum).format('0,0'));
                });
            },
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6' l><'col-sm-6 text-right' f>B r> t <'row'<'col-sm-6'><'col-sm-6 text-right'p > > "
        });
        trn_blm_trm_po_leasing_detail.on('order.dt search.dt', function () {
            trn_blm_trm_po_leasing_detail.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        get_blm_trm_po_leasing();

        setInterval(function () {
            trn_penjualan.ajax.reload();
            trn_target_penjualan.ajax.reload();
            get_blm_trm_po_leasing();
        }, 300000);
    });

    function get_blm_trm_po_leasing() {
        var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_blm_trm_po_leasing'); ?>",
            data: {"periode": periode},
            beforeSend: function () {
                
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_blm_trm_po_leasing.clear().draw();
                $.each(obj, function (key, data) {
                    trn_blm_trm_po_leasing.rows.add(data).draw();
                    ;
                });
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function detail_po(cabang, kdleasing) {
        $.ajax({
            type: "POST",
            url: "<?= site_url('dashallcabang/get_blm_trm_po_leasing_detail'); ?>",
            data: {"cabang": cabang
                ,"kdleasing": kdleasing},
            beforeSend: function () {
                trn_blm_trm_po_leasing_detail.clear().draw();
                $("#modal-detail-po-leasing").modal("show");
            },
            success: function (resp) {
                var obj = jQuery.parseJSON(resp);
                trn_blm_trm_po_leasing_detail.rows.add(obj).draw();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
</script>
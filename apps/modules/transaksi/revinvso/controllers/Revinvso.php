<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Revinvso
 *
 * @author
 */

class Revinvso extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('revinvso/submit'),
            'add' => site_url('revinvso/add'),
            'edit' => site_url('revinvso/edit'),
            'reload' => site_url('revinvso'),
        );
        $this->load->model('revinvso_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
        }


    public function getData() {
        echo $this->revinvso_qry->getData();

    }

    public function proses(){
        echo $this->revinvso_qry->proses();

    }

    public function getid(){
        echo $this->revinvso_qry->getid();

    }

    public function submit() {
        $nodo = $this->input->post('nodo'); 

        if($this->validate() == TRUE){
            $res = $this->revinvso_qry->submit();
            if(empty($nourut)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($nokb)){
                        $this->_check_id();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_init_edit($nokb);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($nokb)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_check_id($nokb);
                $this->template->build('form', $this->data);
            }
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
           'nodo'=> array(
                    'placeholder' => 'Nomor DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    //'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),

        );
    }

    private function _init_edit($nodo = null){
        if(!$nodo){
            $nodo = $this->uri->segment(3); 
        }
        $this->_check_id($nodo);
        $this->data['form'] = array(
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => $this->val[0]['nodo'],
                    'class'       => 'form-control',
                    'readonly'    => ''
            ),
           'nosin'=> array(
                    'type'         => 'hidden',
                    'placeholder' => 'No. Mesin',
                    'id'           => 'nosin',
                    'value'       => $this->val[0]['nosin'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    'id'           => 'noso',
                    'value'       => $this->val[0]['noso'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ), 
            'tgldo'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => $this->val[0]['tgldo'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => ''
            ),
           'jnsbayar'=> array(
                    'placeholder' => 'Jenis Pembayaran',
                    'id'      => 'jnsbayar',
                    'name'        => 'jnsbayar',
                    'value'       => $this->val[0]['jnsbayar'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'nama'=> array(
                    'placeholder' => 'Nama',
                    'id'      => 'nama',
                    'name'        => 'nama',
                    'value'       => $this->val[0]['nama'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'alamat'=> array(
                    'placeholder' => 'Alamat',
                    'id'      => 'alamat',
                    'name'        => 'alamat',
                    'value'       => $this->val[0]['alamat'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'noktp'=> array(
                    'placeholder' => 'No. KTP',
                    'id'    => 'noktp',
                    'class' => 'form-control', 
                    'value'    => $this->val[0]['noktp'],
                    'name'     => 'ket',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'harga'=> array(
                    'placeholder' => 'Harga OFF',
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => $this->val[0]['harga'],
                    'name'     => 'harga',
                    // 'readonly'    => '',
                    'style'       => 'text-transform: uppercase;text-align: right',
            ),
            'tot_hrg'=> array(
                    'placeholder' => 'Total Harga',
                    'id'    => 'tot_hrg',
                    'class' => 'form-control',
                    'value'    => $this->val[0]['tot_hrg'],
                    'name'     => 'tot_hrg',
                    'readonly'    => '',
                    'style'       => 'text-transform: uppercase;text-align: right',
            ),
            'diskon'=> array(
                    'placeholder' => 'Discount',
                    'id'    => 'diskon',
                    'class' => 'form-control',
                    'value'    => $this->val[0]['tot_disc'],
                    'name'     => 'diskon',
                    // 'readonly'    => '',
                    'style'       => 'text-transform: uppercase;text-align: right',
            ),
            'dpp_unit'=> array(
                    'placeholder' => 'DPP',
                    'id'    => 'dpp_unit',
                    'class' => 'form-control',
                    'value'    => $this->val[0]['dpp_unit'],
                    'name'     => 'dpp_unit',
                    'readonly'    => '',
                    'style'       => 'text-transform: uppercase;text-align: right',
            ),
            'ppn_unit'=> array(
                    'placeholder' => 'PPN',
                    'id'    => 'ppn_unit',
                    'class' => 'form-control',
                    'value'    => $this->val[0]['ppn_unit'],
                    'name'     => 'ppn_unit',
                    'readonly'    => '',
                    'style'       => 'text-transform: uppercase;text-align: right',
            ),
            'bbn'=> array(
                    'placeholder' => 'Cad BBN',
                    // 'type'         => 'hidden',
                    'id'    => 'bbn',
                    'class' => 'form-control',
                    'value'    => $this->val[0]['bbn'],
                    'name'     => 'bbn',
                    // 'readonly'    => '',
                    'style'       => 'text-transform: uppercase;text-align: right',
            ),
        );
    }
    private function _check_id($nodo){
        if(empty($nodo)){
            redirect($this->data['edit']);
        }

        $this->val= $this->revinvso_qry->select_data($nodo);

        if(empty($this->val)){
            redirect($this->data['edit']);
        }
    }

    private function validate($nokb,$nourut) {
      if(!empty($nokb) && !empty($nourut)){
          return true;
      }
      $config = array(
            array(
                    'field' => 'nokb',
                    'label' => 'No. Kas Bank',
                    'rules' => 'required|max_length[10]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

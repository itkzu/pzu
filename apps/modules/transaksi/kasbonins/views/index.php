<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style>  
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-xs-12 box-header box-view">
              <div class="col-xs-3">
                <div class="row">
                    <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a> 
                </div>
              </div>
              <div class="col-xs-2">
                <div class="row"> 
                </div>
              </div>
              <!-- <div class="col-xs-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak Nota</button>
                </div>
              </div> -->
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['nokasbon']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['nokasbon']);
                                echo form_error('nokasbon','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['tglkasbon']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['tglkasbon']);
                                echo form_error('tglkasbon','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                      <div class="col-xs-2 col-md-2 tagbbn">
                        <div class="form-group">
                          <?php echo form_label("Tagihan BBN"); ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                           <?php echo form_label($form['refkb']['placeholder']); ?>
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php
                              echo form_dropdown($form['refkb']['name'], $form['refkb']['data'],
                                                $form['refkb']['value'], $form['refkb']['attr']);
                              echo form_error('refkb','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                      <div class="col-xs-2 col-md-2 tagbbn">
                        <div class="form-group">
                          <?php echo form_label($form['notagbbn']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-3 col-md-3 tagbbn">
                        <div class="form-group">
                          <?php echo form_input($form['notagbbn']);
                                echo form_error('notagbbn','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                           <?php echo form_label($form['kdjkasbon']['placeholder']); ?>
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php
                              echo form_dropdown($form['kdjkasbon']['name'], $form['kdjkasbon']['data'], $form['kdjkasbon']['value'], $form['kdjkasbon']['attr']);
                              echo form_error('kdjkasbon','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                      <div class="col-xs-2 col-md-2 tagbbn">
                        <div class="form-group">
                          <?php echo form_label($form['tgltagbbn']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-3 col-md-3 tagbbn">
                        <div class="form-group">
                          <?php echo form_input($form['tgltagbbn']);
                                echo form_error('tgltagbbn','<div class="note">','</div>');
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['penerima']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['penerima']);
                                echo form_error('penerima','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['ket']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['ket']);
                                echo form_error('ket','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['nilai']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['nilai']);
                                echo form_error('nilai','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-2 col-md-2 ">
                        <div class="form-group">
                          <?php echo form_label($form['tglpelaksanaan']['placeholder']); ?> 
                        </div>
                      </div>
                      <div class="col-xs-5 col-md-5 ">
                        <div class="form-group">
                          <?php echo form_input($form['tglpelaksanaan']);
                                echo form_error('tglpelaksanaan','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div> 
                    </div>
                  </div>

                  <!--KAS/BANK MASUK UMUM-->
                  <div class="kontribusi">
                    <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <div class="col-xs-12">
                            <label>Data Kontribusi</label>
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div>
                      </div>
                    </div>   

                    <div class="col-xs-12"> 
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered DataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">CL</th>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">Kontributor</th> 
                            <th style="text-align: center;">Catatan</th>  
                            <th style="text-align: center;">Nilai</th> 
                          </tr>
                        </thead> 
                        <tbody></tbody>
                      </table>
                    </div>
                    </div>     
                  </div>  

                </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        $('#nokasbon').focus();
        $('#nokasbon').mask('BON-****-****'); 
        $('#notagbbn').mask('TBJ-****-****'); 
        disabled(); 
        autoNum(); 
        $('.tagbbn').hide();
        $('.kontribusi').hide(); 

        $('.btn-ctk').click(function(){
            var notbj = $('#notbj').val();
                        // var lik = nokps.replace("/","+");
                        // var lik = lik.replace("/","+");
                        // var lik = lik.replace("/","+");
            window.open('kasbonins/ctk/'+notbj, '_blank');
            // window.location.href = 'lrnew/ctk_lr/'+nodo;

            // ctk_lr();
        }); 

        //nokb
          $('#nokasbon').keyup(function(){
            var jml = $('#nokasbon').val(); 
            var nokasbon = jml.replace("-","");
            var nokasbon = nokasbon.replace("-","");
            var nokasbon = nokasbon.replace("-","");
            var nokasbon = nokasbon.replace("_",""); 
            var n = nokasbon.length; 
            
            if(n!=11){
                // alert(n);  
              clear();  
                // day();
            } else { 
                set_kasbon();  
            }
          });
    });



    function set_kasbon(){

      var nokasbon = $("#nokasbon").val();
      var nokasbon = nokasbon.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/set_kasbon");?>",
            data: {"nokasbon":nokasbon },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  $('#nokasbon').val('');
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){  
                    $("#tglkasbon").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkasbon)));  
                    $("#refkb").val(data.kdkb);  
                    $("#refkb").trigger("chosen:updated");
                    $('#refkb').select2({
                        dropdownAutoWidth : true,
                        width: '100%'
                    });
                    $("#kdjkasbon").val(data.kdjkasbon);  
                    $("#kdjkasbon").trigger("chosen:updated");
                    $('#kdjkasbon').select2({
                        dropdownAutoWidth : true,
                        width: '100%'
                    });
                    $("#penerima").val(data.nama); 
                    $("#ket").val(data.ket); 
                    $("#nilai").autoNumeric('set',data.nilai);  
                    $("#tglpelaksanaan").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglplks)));  
                    // alert(data.kdjkasbon);
                    if(data.kdjkasbon!=2){
                      $('.tagbbn').hide();
                      $('.kontribusi').show();
                      if($('#kdjkasbon').val()!=''){
                        kontribusi();  
                      }
                    } else {
                      $('.tagbbn').show();
                      $('.kontribusi').hide();
                      $("#notagbbn").val(data.nobbn_trm);  
                      tagihanbbn(data.nobbn_trm);
                      // tagbbn();         
                    }

                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function tagihanbbn(nobbn_trm){    
        // alert(count);
        notbj = nobbn_trm;
        $.ajax({
            type: "POST",
            url: "<?=site_url('kasbonins/set_tglbbn');?>",
            data: { "notbj":notbj},
            success: function(resp){
              // $("#modal_sub").modal("hide");  
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                  // $('#ket').show();  
                      $("#tgltagbbn").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglbbn_trm)));  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    function kontribusi(){  
      var nokasbon = $('#nokasbon').val();
      var nokasbon = nokasbon.toUpperCase();
      table = $('.DataTable').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [ {
                    "aTargets": [ 4 ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"
                    
                }],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "cl"},
                { "data": "no"},
                { "data": "kdleasing" },
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nokasbon", "value": nokasbon  }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('kasbonins/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.DataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
            var PageInfo = $('.DataTable').DataTable().page.info();
            table.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }

    function disabled(){  
      // $("#refkb").prop('disabled',true); 
      // $("#tglkasbon").prop('disabled',true);  
      $("#refkb").prop('disabled',true);  
      $("#kdjkasbon").prop('disabled',true);  
      // $("#penerima").prop('disabled',true);  
      // $("#ket").prop('disabled',true);  
      // $("#nilai").prop('disabled',true);  
      // $("#tglpelaksanaan").prop('disabled',true);    

      // $("#notagbbn").prop('disabled',true);  
      // $("#tgltagbbn").attr('disabled',true);    
    }

    function autoNum(){   
      //detail pembayaran konsumen
          //nominal pembayaran konsumen
          $('#nilai').autoNumeric('init',{ currencySymbol : 'Rp. '});   
    } 

    function clear(){
      //detail realisasi kasbon 
      $('#tglkasbon').val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $('#refkb').val('');
      $('#kdjkasbon').val('');  
      $('#penerima').val('');    
      $('#ket').val('');    
      $('#nilai').val('');    
      $('#notagbbn').val('');    
      $('#tgltagbbn').val($.datepicker.formatDate('dd-mm-yy', new Date())); 
      $('.tagbbn').hide(); 
      $('.kontribusi').hide(); 
    }

    function reset(){   

      $('#nokasbon').val('');
      $('#tglkasbon').val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $('#refkb').val('');
      $('#kdjkasbon').val('');  
      $('#penerima').val('');    
      $('#ket').val('');    
      $('#nilai').val('');    
      $('#notagbbn').val('');    
      $('#tgltagbbn').val($.datepicker.formatDate('dd-mm-yy', new Date()));  
    } 

    function day(tgl){
      var n = tgl.getDay();
      // var n = new Date(tgl);
      // alert(n);
      if(n>=7){
        var nd = 'MINGGU';
      } else if(n>=6){
        var nd = 'SABTU';
      } else if(n>=5){
        var nd = 'JUMAT';
      } else if(n>=4){
        var nd = 'KAMIS';
      } else if(n>=3){
        var nd = 'RABU';
      } else if(n>=2){
        var nd = 'SELASA';
      } else if(n>=1){
        var nd = 'SENIN';
      }
      $('#day').val(nd);
    }

    function settrx(){
      // alert
      var nokb = $('#nokb').val();
      if ($('#jnstrans').val()==='PEMBAYARAN KONSUMEN (D)') {
        $('.pkons').show();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_pkons();
        }
      } else if ($('#jnstrans').val()==='PEMBAYARAN LEASING (D)') {
        $('.pkons').hide();
        $('.pleas').show();
        $('.rleas').hide();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_pleas();
        }
      } else if ($('#jnstrans').val()==='REFUND LEASING (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').show();
        $('.psub').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_rleas();
        }
      } else if ($('#jnstrans').val()==='PENGEMBALIAN KONSUMEN (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.balik_kons').show();
        $('.psub').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_blkkons();
        }
      } else if ($('#jnstrans').val()==='REALISASI KASBON (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.kbk_umum').hide();
        $('.psub').hide();
        $('.rkasbon').show();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_rkasbon();
        }
      } else if ($('#jnstrans').val()==='K/B KELUAR - UMUM (K)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.kbk_umum').show();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.psub').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_kaskeluar(nokb);
        }
      } else if ($('#jnstrans').val()==='K/B MASUK - UMUM (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.rleas').hide();
        $('.kbk_umum').hide();
        $('.balik_kons').hide();
        $('.psub').hide();
        $('.rkasbon').hide();
        $('.kbm_umum').show();
        if($('.kbm_umum').show()){
          // alert('1');
          set_kasmasuk(nokb);
        }
      } else if ($('#jnstrans').val()==='PENCAIRAN SUBSIDI (D)') {
        $('.pkons').hide();
        $('.pleas').hide();
        $('.kbk_umum').hide();
        $('.rkasbon').hide();
        $('.rleas').hide();
        $('.balik_kons').hide();
        $('.psub').show();
        $('.kbm_umum').hide();
        if(nokb!=''){
          set_psub();
        }
      } 
    }

    function pkons_nocetak(){
      var check = $('#pkons_lanjut').is(':checked');
      if(check){ 
        $("#byrkons_nocetak").prop("disabled", false);
        $("#byrkons_jmltrans").prop("disabled", false); 
      }else{ 
        $("#byrkons_nocetak").prop("disabled", true);
        $("#byrkons_jmltrans").prop("disabled", true); 

      }
    }

    function pkons_nocetak(){
      var check = $('#pleas_lanjut').is(':checked');
      if(check){ 
        $("#pleas_nocetak").prop("disabled", false);
        $("#pleas_jmltrans").prop("disabled", false); 
      }else{ 
        $("#pleas_nocetak").prop("disabled", true);
        $("#pleas_jmltrans").prop("disabled", true); 

      }
    }

    function enabled(){  
      $("#tgltbj").attr('disabled',true); 
      $("#disc").attr('disabled',true); 
      $("#pph21").attr('disabled',true); 
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',false);
      $(".btn-del").attr('disabled',false);
      $(".btn-ctk").attr('disabled',false); 
    }

    function c_pkons(){ 
      //detail pembayaran konsumen
          $('#byrkons_noso').val('');  
          $('#byrkons_nodo').val('');  
          $('#byrkons_tglso').val('');  
          $('#byrkons_tgldo').val('');  
          $('#byrkons_nmsales').val('');  
          $('#byrkons_nmspv').val('');  
          $('#byrkons_nama').val('');  
          $('#byrkons_alamat').val('');  
          $('#byrkons_kdleas').val('');  
          $('#byrkons_kdprogleas').val('');  
          $('#byrkons_nosin').val('');  
          $('#byrkons_nora').val('');  
          $('#byrkons_kdtipe').val('');  
          $('#byrkons_nmtipe').val('');  
          $('#byrkons_warna').val('');  
          $('#byrkons_tahun').val('');  
          $('#byrkons_status').val('');  

          //nominal pembayaran konsumen
          $('#byrkons_hjnet').autoNumeric('set',0);  
          $('#byrkons_um').autoNumeric('set',0);  
          $('#byrkons_pelunasan').autoNumeric('set',0);  
          $('#byrkons_byr1').autoNumeric('set',0);  
          $('#byrkons_byr2').autoNumeric('set',0);  
          $('#byrkons_nbyr').autoNumeric('set',0);  
          $('#byrkons_saldoum').autoNumeric('set',0);  
          $('#byrkons_saldo_pelunasan').autoNumeric('set',0);  
          $('#byrkons_nocetak').val('');  
          $('#byrkons_jmltrans').val('');  
    }

    function c_pleas(){ 
      //detail pembayaran leasing
          $('#pleas_noso').val('');   
          $('#pleas_nodo').val('');   
          $('#pleas_tglso').val('');   
          $('#pleas_tgldo').val('');   
          $('#pleas_nmsales').val('');   
          $('#pleas_nmspv').val('');   
          $('#pleas_nama').val('');   
          $('#pleas_alamat').val('');   
          $('#pleas_kdleas').val('');   
          $('#pleas_kdprogleas').val('');   
          $('#pleas_nosin').val('');   
          $('#pleas_nora').val('');   
          $('#pleas_kdtipe').val('');   
          $('#pleas_nmtipe').val('');   
          $('#pleas_warna').val('');   
          $('#pleas_tahun').val('');   
          $('#pleas_status').val('');    

          //nominal pembayaran leasing
          $('#pleas_pelunasan').autoNumeric('set',0);     
          $('#pleas_jp').autoNumeric('set',0);     
          $('#pleas_byr1').autoNumeric('set',0);  
          $('#pleas_byr2').autoNumeric('set',0);  
          $('#pleas_nbyr1').autoNumeric('set',0);  
          $('#pleas_nbyr2').autoNumeric('set',0);  
          $('#pleas_nocetak').val('');    
          $('#pleas_jmltrans').val('');  
    }

    function c_rleas(){ 
      //detail refund leasing
          $('#rleas_kdleas').val('');   
          $('#rleas_jenis').val('');   
          $('#rleas_nilai').val('');   
          $('#rleas_ket').val('');    
    }

    function c_psub(){
      //detail pencairan subsidi
          $('#psub_kdleas').val('');   
          $('#psub_selisih').autoNumeric('set',0);   
          $('#psub_ket').val('');   
          $('#psub_totsub').autoNumeric('set',0);   
          $('#psub_selsbg').val('');   
          
          //more detail pencairan subsidi
          $('#psub_nokasbon').val('');    
          $('#psub_tglkasbon').val('');    
          $('#psub_ketkasbon').val('');    
          $('#psub_catkasbon').val('');    
          $('#psub_nsubkasbon').autoNumeric('set',0);    
          $('#psub_ncairkasbon').autoNumeric('set',0);   
    }

    function c_blkkons(){
      //detail pengembalian konsumen
          $('#blkkons_noso').val('');     
          $('#blkkons_nodo').val('');     
          $('#blkkons_tglso').val('');     
          $('#blkkons_tgldo').val('');     
          $('#blkkons_nmsales').val('');     
          $('#blkkons_nmspv').val('');     
          $('#blkkons_nama').val('');     
          $('#blkkons_alamat').val('');     
          $('#blkkons_kdleas').val('');     
          $('#blkkons_kdprogleas').val('');     
          $('#blkkons_nosin').val('');     
          $('#blkkons_nora').val('');     
          $('#blkkons_kdtipe').val('');     
          $('#blkkons_nmtipe').val('');     
          $('#blkkons_warna').val('');     
          $('#blkkons_tahun').val('');     
          $('#blkkons_status').val('');    

          //nominal pembayaran konsumen
          $('#blkkons_hjnet').autoNumeric('set',0);
          $('#blkkons_pl').autoNumeric('set',0);
          $('#blkkons_byr1').autoNumeric('set',0);  
          $('#blkkons_pengembalian').autoNumeric('set',0);
          $('#blkkons_lbhbyr').autoNumeric('set',0);    
    }

    function set_pkons(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/set_pkons");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#byrkons_noso").val(data.noso); 
                    $("#byrkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                    $("#byrkons_nodo").val(data.nodo); 
                    $("#byrkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                    $("#byrkons_nmsales").val(data.nmsales); 
                    $("#byrkons_nmspv").val(data.nmsales_header); 
                    $("#byrkons_nama").val(data.nama); 
                    $("#byrkons_alamat").val(data.alamat); 
                    $("#byrkons_kdleas").val(data.jnsbayar); 
                    $("#byrkons_kdprogleas").val(data.nmprogleas); 
                    $("#byrkons_nosin").val(data.nosin); 
                    $("#byrkons_nora").val(data.nora); 
                    $("#byrkons_kdtipe").val(data.kdtipe); 
                    $("#byrkons_nmtipe").val(data.nmtipe); 
                    $("#byrkons_warna").val(data.warna); 
                    $("#byrkons_tahun").val(data.tahun); 
                    $("#byrkons_status").val(data.status_otr); 

                    $("#byrkons_hjnet").autoNumeric('set',data.harga_jual_net); 
                    $("#byrkons_um").autoNumeric('set',data.ar_um); 
                    $("#byrkons_pelunasan").autoNumeric('set',data.ar_pl); 
                    $("#byrkons_byr1").autoNumeric('set',data.byr_um); 
                    $("#byrkons_byr2").autoNumeric('set',data.byr_pl); 
                    $("#byrkons_nbyr").autoNumeric('set',data.nilai_byr); 
                    $("#byrkons_saldoum").autoNumeric('set',data.saldo_um); 
                    $("#byrkons_saldo_pelunasan").autoNumeric('set',data.saldo_pl); 

                    $('#pkons_lanjut').prop("checked",true);
                    $("#byrkons_nocetak").val(data.nocetak); 
                    $("#byrkons_jmltrans").val(data.jmltrans);  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_pleas(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/set_pleas");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#pleas_noso").val(data.noso); 
                    $("#pleas_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                    $("#pleas_nodo").val(data.nodo); 
                    $("#pleas_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                    $("#pleas_nmsales").val(data.nmsales);   
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_blkkons(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/set_blkkons");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#blkkons_noso").val(data.noso); 
                    $("#blkkons_tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));   
                    $("#blkkons_nodo").val(data.nodo); 
                    $("#blkkons_tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));   
                    $("#blkkons_nmsales").val(data.nmsales); 
                    $("#blkkons_nmspv").val(data.nmsales_header); 
                    $("#blkkons_nama").val(data.nama); 
                    $("#blkkons_alamat").val(data.alamat); 
                    $("#blkkons_kdleas").val(data.jnsbayar); 
                    $("#blkkons_kdprogleas").val(data.nmprogleas); 
                    $("#blkkons_nosin").val(data.nosin); 
                    $("#blkkons_nora").val(data.nora); 
                    $("#blkkons_kdtipe").val(data.kdtipe); 
                    $("#blkkons_nmtipe").val(data.nmtipe); 
                    $("#blkkons_warna").val(data.warna); 
                    $("#blkkons_tahun").val(data.tahun); 
                    $("#blkkons_status").val(data.status_otr); 

                    $("#blkkons_hjnet").autoNumeric('set',data.harga_jual_net);  
                    $("#blkkons_pl").autoNumeric('set',data.ar_pl); 
                    $("#blkkons_byr1").autoNumeric('set',data.byr_pl); 
                    $("#blkkons_pengembalian").autoNumeric('set',Math.abs(data.saldo_pl)); 
                    $("#blkkons_lbhbyr").autoNumeric('set',data.saldo_pl);   

                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_rleas(){

      var nokb = $("#nokb").val();
      var nokb = nokb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kasbonins/set_rleas");?>",
            data: {"nokb":nokb },
            success: function(resp){ 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){  
                    $("#rleas_kdleas").val(data.kdleasing); 
                    $("#rleas_jenis").val(data.jenis); 
                    $("#rleas_nilai").autoNumeric('set',data.nilai); 
                    $("#rleas_ket").val(data.ket);  

                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function set_kasmasuk(nokb){  
      var nokb = nokb.toUpperCase();
      tabel_kbm = $('.tbl_kbm').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [ {
                    "aTargets": [ 5 ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"
                    
                }],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nmrefkb" },
                { "data": "darike" },
                { "data": "nofaktur"},
                { "data": "ket" },
                { "data": "nilai"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nokb", "value": nokb  }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('kasbonins/set_kbm');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                // $('#total').autoNumeric('set',total); 
                // sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.tbl_kbm').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        tabel_kbm.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        tabel_kbm.on( 'draw.dt', function () {
            var PageInfo = $('.tbl_kbm').DataTable().page.info();
            tabel_kbm.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });   
    }

    function sum(){
        var total = $('#total').autoNumeric('get'); 
        var disc  = $('#disc').autoNumeric('get'); 
        var pph21 = $('#pph21').autoNumeric('get'); 

        var alltot = parseInt(total) - (parseInt(disc) + parseInt(pph21));
        if(!isNaN(alltot)){
            $('#alltot').autoNumeric('set',alltot); 
            var terbilang = penyebut(alltot);
            if(alltot==='0'){
              $('#banner').val(terbilang);  
            } else {
              $('#banner').val(terbilang+" RUPIAH");  
            }
            
        }
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 
</script>

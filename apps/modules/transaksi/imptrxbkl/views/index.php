<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
			<!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
			-->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?=form_label($form['kddiv']['placeholder']);?>
                                    <div class="input-group">
                                        <?php
                                            echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                            echo form_error('kddiv', '<div class="note">', '</div>');
                                        ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-set" data-dismiss="fileinput" id="btn-set">Set</button>
                                            <button type="button" class="btn btn-success btn-reset" data-dismiss="fileinput" id="btn-unset">Unset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    

                    <div class="col-md-12">
                        <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div>




                    <div class="form-import main-form">


                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active">
                                    <a href="#tab_1_1" class="tab_imp" data-toggle="tab"> Import Ahassystem </a>
                                </li> 
                                <li>
                                    <a href="#tab_1_3" class="tab_sv" data-toggle="tab"> STAR Service PKB </a>
                                </li>
                                <li>
                                    <a href="#tab_1_4" class="tab_po" data-toggle="tab"> STAR Purchace Order </a>
                                </li>
                                <li>
                                    <a href="#tab_1_5" class="tab_so" data-toggle="tab"> STAR Penjualan </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="row">
                                         <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Data Pembelian</label>

                                                <div class="fileinputBeli fileinput-new input-group" name="fileinputBeli" id="fileinputBeli" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>

                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Cari...</span>
                                                        <span class="fileinput-exists">Ubah</span>
                                                        <input type="file" name="fileBeli" id="fileBeli" data-original-value="" required accept=".xls, .xlsx">
                                                    </span>

                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    <span class="input-group-btn fileinput-exists">
                                                        <button class="btn btn-primary" name="importBeli" id="importBeli"><i class="fa fa-cloud-upload"></i> Import</button>
                                                    </span>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div><a href="<?=site_url('imptrxbkl/download_template_po');?>" name="d_po" id="d_po" target="_blank">
                                                <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                    Download Template
                                                </button></a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Data Service</label>

                                                <div class="fileinputServise fileinput-new input-group" name="fileinputServise" id="fileinputServise" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>

                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Cari...</span>
                                                        <span class="fileinput-exists">Ubah</span>
                                                        <input type="file" name="fileServis" id="fileServis" data-original-value="" required accept=".xls, .xlsx">
                                                    </span>

                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    <span class="input-group-btn fileinput-exists">
                                                        <button class="btn btn-primary" name="importServis" id="importServis"><i class="fa fa-cloud-upload"></i> Import</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div>
                                                <a href="<?=site_url('imptrxbkl/download_template_sv');?>" name="d_sv" id="d_sv" target="_blank">
                                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                        Download Template
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Data Penjualan</label>

                                                <div class="fileinputJual fileinput-new input-group" name="fileinputJual" id="fileinputJual" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>

                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Cari...</span>
                                                        <span class="fileinput-exists">Ubah</span>
                                                        <input type="file" name="fileJual" id="fileJual" data-original-value="" required accept=".xls, .xlsx">
                                                    </span>

                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    <span class="input-group-btn fileinput-exists">
                                                        <button class="btn btn-primary" name="importJual" id="importJual"><i class="fa fa-cloud-upload"></i> Import</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div>
                                                <a href="<?=site_url('imptrxbkl/download_template_so');?>" name="d_so" id="d_so" target="_blank">
                                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                        Download Template
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>

                                        <!-- <div class="col-md-8">
                                            <div class="form-group">
                                                <form id="uploadForm" enctype="multipart/form-data">
                                                    <label for="exampleInputFile">Data Kas & Bank</label>

                                                    <div class="fileinputKB fileinput-new input-group" name="fileinputKB" id="fileinputKB" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                            <span class="fileinput-filename"></span>
                                                        </div>

                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            <span class="fileinput-new">Cari...</span>
                                                            <span class="fileinput-exists">Ubah</span>
                                                            <input type="file" name="fileKB" id="fileKB" data-original-value="" required accept=".xls, .xlsx">
                                                        </span>

                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    </div>

                                                    <div class="upload_kb">
                                                        <?=form_label($form['kb']['placeholder']);?>
                                                        <div class="input-group">
                                                            <?php
                                                                echo form_dropdown($form['kb']['name'],$form['kb']['data'] ,$form['kb']['value'] ,$form['kb']['attr']);
                                                                echo form_error('kb', '<div class="note">', '</div>');
                                                            ?>
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-primary" name="importKB" id="importKB"><i class="fa fa-cloud-upload"></i> Import</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div>
                                                <a href="<?=site_url('imptrxbkl/download_template_kb');?>" name="d_kb" id="d_kb" target="_blank">
                                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                        Download Template
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> -->

                                    </div>
                                </div>

                                <!-- tab_1_2 -->
                                <!-- <div class="tab-pane active" id="tab_1_2">
                                    <div class="row">
                                         <div class="col-md-8">

                                            <div class="form-group">
                                                <label for="exampleInputFile">Data Pembelian</label>

                                                <div class="fileinputBeliStar fileinput-new input-group" name="fileinputBeliStar" id="fileinputBeliStar" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>

                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Cari...</span>
                                                        <span class="fileinput-exists">Ubah</span>
                                                        <input type="file" name="fileBeliStar" id="fileBeliStar" data-original-value="" required accept=".xls, .xlsx">
                                                    </span>

                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    <span class="input-group-btn fileinput-exists">
                                                        <button class="btn btn-primary" name="importBeli" id="importBeli"><i class="fa fa-cloud-upload"></i> Import</button>
                                                    </span>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div><a href="<?=site_url('imptrxbkl/download_template_po');?>" name="d_po_star" id="d_po_star" target="_blank">
                                                <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                    Download Template
                                                </button></a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>




                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Data Service</label>

                                                <div class="fileinputServiseStar fileinput-new input-group" name="fileinputServiseStar" id="fileinputServiseStar" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>

                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Cari...</span>
                                                        <span class="fileinput-exists">Ubah</span>
                                                        <input type="file" name="fileServisStar" id="fileServisStar" data-original-value="" required accept=".xls, .xlsx">
                                                    </span>

                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    <span class="input-group-btn fileinput-exists">
                                                        <button class="btn btn-primary" name="importServis" id="importServis"><i class="fa fa-cloud-upload"></i> Import</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div>
                                                <a href="<?=site_url('imptrxbkl/download_template_sv');?>" name="d_sv_star" id="d_sv_star" target="_blank">
                                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                        Download Template
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>





                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Data Penjualan</label>

                                                <div class="fileinputJualStar fileinput-new input-group" name="fileinputJualStar" id="fileinputJualStar" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>

                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Cari...</span>
                                                        <span class="fileinput-exists">Ubah</span>
                                                        <input type="file" name="fileJual" id="fileJual" data-original-value="" required accept=".xls, .xlsx">
                                                    </span>

                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    <span class="input-group-btn fileinput-exists">
                                                        <button class="btn btn-primary" name="importJualStar" id="importJualStar"><i class="fa fa-cloud-upload"></i> Import</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div>
                                                <a href="<?=site_url('imptrxbkl/download_template_so');?>" name="d_so_star" id="d_so_star" target="_blank">
                                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                        Download Template
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <form id="uploadForm" enctype="multipart/form-data">
                                                    <label for="exampleInputFile">Data Kas & Bank</label>

                                                    <div class="fileinputKB fileinput-new input-group" name="fileinputKB" id="fileinputKB" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                            <span class="fileinput-filename"></span>
                                                        </div>

                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            <span class="fileinput-new">Cari...</span>
                                                            <span class="fileinput-exists">Ubah</span>
                                                            <input type="file" name="fileKB" id="fileKB" data-original-value="" required accept=".xls, .xlsx">
                                                        </span>

                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>

                                                    </div>

                                                    <div class="upload_kb">
                                                        <?=form_label($form['kb']['placeholder']);?>
                                                        <div class="input-group">
                                                            <?php
                                                                echo form_dropdown($form['kb']['name'],$form['kb']['data'] ,$form['kb']['value'] ,$form['kb']['attr']);
                                                                echo form_error('kb', '<div class="note">', '</div>');
                                                            ?>
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-primary" name="importKB" id="importKB"><i class="fa fa-cloud-upload"></i> Import</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>&nbsp;</label>
                                            <div>
                                                <a href="<?=site_url('imptrxbkl/download_template_kb');?>" name="d_kb" id="d_kb" target="_blank">
                                                    <button type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                                                        Download Template
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div>

                                    </div>
                                </div> -->

                                <!--tab_1_3-->
                                <div class="tab-pane" id="tab_1_3">
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="row"> 
                                                <div class="col-md-2">
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tglsv1']['placeholder']);?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"> 
                                                        <?php  
                                                            echo form_input($form['tglsv1']);
                                                            echo form_error('tglsv1','<div class="note">','</div>');
                                                        ?>                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tglsv2']['placeholder']);?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"> 
                                                        <?php  
                                                            echo form_input($form['tglsv2']);
                                                            echo form_error('tglsv1','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="col-md-6"> 
                                            <span>
                                                <button type="button" class="btn btn-primary btn-add-sv"><i class="fa fa-plus"> Import Data</i></button>
                                            </span>
                                            <span>
                                                <button type="button" class="btn btn-success btn-upload-sv"><i class="fa fa-upload"> Upload Data</i></button>
                                            </span> 
                                        </div> 
                                        <div class="col-md-12"> 
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover js-basic-example tbl_sv" class="display select">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 1%;text-align: center;">Detail</th>
                                                            <th style="width: 20%;text-align: center;">No PKB</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Servis</th>
                                                            <th style="width: 10%;text-align: center;">No Polisi</th>
                                                            <th style="width: 10%;text-align: center;">No Mesin</th>
                                                            <th style="width: 10%;text-align: center;">No Rangka</th>
                                                            <th style="width: 20%;text-align: center;">Nama Pembawa</th>
                                                            <th style="width: 9%;text-align: center;">Dealer ID</th> 
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th style="width: 1%;text-align: center;">Detail</th>
                                                            <th style="width: 20%;text-align: center;">No PKB</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Servis</th>
                                                            <th style="width: 10%;text-align: center;">No Polisi</th>
                                                            <th style="width: 10%;text-align: center;">No Mesin</th>
                                                            <th style="width: 10%;text-align: center;">No Rangka</th>
                                                            <th style="width: 20%;text-align: center;">Nama Pembawa</th>
                                                            <th style="width: 9%;text-align: center;">Dealer ID</th> 
                                                        </tr>
                                                    </tfoot>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>
                                </div>

                                <!--tab_1_4-->
                                <div class="tab-pane" id="tab_1_4">
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="row"> 
                                                <div class="col-md-2">
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tglpo1']['placeholder']);?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"> 
                                                        <?php  
                                                            echo form_input($form['tglpo1']);
                                                            echo form_error('tglpo1','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tglpo2']['placeholder']);?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"> 
                                                        <?php  
                                                            echo form_input($form['tglpo2']);
                                                            echo form_error('tglpo2','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="col-md-6"> 
                                            <span>
                                                <button type="button" class="btn btn-primary btn-add-po"><i class="fa fa-plus"> Import Data</i></button>
                                            </span>
                                            <span>
                                                <button type="button" class="btn btn-success btn-upload-po"><i class="fa fa-upload"> Upload Data</i></button>
                                            </span> 
                                        </div> 
                                        <div class="col-md-12"> 
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover js-basic-example tbl_po" class="display select">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 1%;text-align: center;">Detail</th>
                                                            <th style="width: 20%;text-align: center;">No Penerimaan</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Penerimaan</th>
                                                            <th style="width: 10%;text-align: center;">No Shippinglist</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th style="width: 1%;text-align: center;">Detail</th>
                                                            <th style="width: 20%;text-align: center;">No Penerimaan</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Penerimaan</th>
                                                            <th style="width: 10%;text-align: center;">No Shippinglist</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>
                                </div>

                                <!--tab_1_5-->
                                <div class="tab-pane" id="tab_1_5">
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="row"> 
                                                <div class="col-md-2">
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tglso1']['placeholder']);?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"> 
                                                        <?php  
                                                            echo form_input($form['tglso1']);
                                                            echo form_error('tglso1','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tglso2']['placeholder']);?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"> 
                                                        <?php  
                                                            echo form_input($form['tglso2']);
                                                            echo form_error('tglso2','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="col-md-6"> 
                                            <span>
                                                <button type="button" class="btn btn-primary btn-add-so"><i class="fa fa-plus"> Import Data</i></button>
                                            </span>
                                            <span>
                                                <button type="button" class="btn btn-success btn-upload-so"><i class="fa fa-upload"> Upload Data</i></button>
                                            </span> 
                                        </div> 
                                        <div class="col-md-12"> 
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover js-basic-example tbl_so" class="display select">
                                                    <thead>
                                                        <tr> 
                                                            <th style="text-align: center;">No SO</th>
                                                            <th style="text-align: center;">Tgl SO</th>
                                                            <th style="text-align: center;">ID Cust</th>
                                                            <th style="text-align: center;">Nama Cust</th>
                                                            <th style="text-align: center;">Disc SO</th>
                                                            <th style="text-align: center;">Total SO</th>
                                                            <th style="text-align: center;">ID Dealer</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr> 
                                                            <th style="text-align: center;">No SO</th>
                                                            <th style="text-align: center;">Tgl SO</th>
                                                            <th style="text-align: center;">ID Cust</th>
                                                            <th style="text-align: center;">Nama Cust</th>
                                                            <th style="text-align: center;">Disc SO</th>
                                                            <th style="text-align: center;">Total SO</th>
                                                            <th style="text-align: center;">ID Dealer</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        	<?php echo form_close(); ?>
      	</div>
      	<!-- /.box -->
	</div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
        disabled();

        $('.btn-set').click(function(event){
            enabled();
        });

        $(".btn-reset").click(function(){
            //disabled();
            window.location.reload();
        });

        //import Ahasssystem
        //import data Pembelian
    	$('#importBeli').click(function(event){
            $("#load").show();
            event.preventDefault();
            var formData = new FormData();
    		formData.append('file', $('input[type=file]')[0].files[0]);
    		formData.append('kddiv',  $("#kddiv").val());

    		$.ajax({
    			url:"<?php echo base_url(); ?>imptrxbkl/importBeli",
    			method:"POST",
                data:formData,
                dataType:'json',
    			contentType:false,
    			cache:false,
    			processData:false, 
   				success:function(resp){

                    var obj = resp;
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputBeli').fileinput('clear');
                                    $('#importBeli').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputBeli').fileinput('clear');
                                    $('#importBeli').attr('disabled', false);
                                });

                            }
                        });
                }
            })
    	});

		//import data Servis
		$('#importServis').click(function(event){
            $("#load").show();
  			event.preventDefault();

			var formData = new FormData();
			formData.append('file', $('input[type=file]')[1].files[0]);
			formData.append('kddiv',  $("#kddiv").val());
			//formData.append('file', $('.fileinputON').files[0]);

  			$.ajax({
   				url:"<?php echo base_url(); ?>imptrxbkl/importServis",
   				method:"POST",
   				//data:new FormData(this),
				data:formData,
   				contentType:false,
   				cache:false,
   				processData:false,
   				success:function(resp){
                    // if($("#status").html('ok')){
                        var obj = JSON.parse(resp);
                        $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputServise').fileinput('clear');
                                    $('#importServis').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputServise').fileinput('clear');
                                    $('#importServis').attr('disabled', false);
                                });

                            }
                        });
                    // }
                }
  			})
 		});

		//import data Penjualan
		$('#importJual').click(function(e){
            $("#load").show();
  			e.preventDefault();

			var formData = new FormData();
            formData.append('file', $('input[type=file]')[2].files[0]);
			formData.append('kddiv',  $("#kddiv").val());
			//formData.append('file', $('#fileinputOFF').files[0]);
  			$.ajax({
   				url:"<?php echo base_url(); ?>imptrxbkl/importJual",
                method:"POST",
   				//data:new FormData(this),
				data:formData,
   				contentType:false,
   				cache:false,
   				processData:false, 
   				success:function(resp){

                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputJual').fileinput('clear');
                                    $('#importJual').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputJual').fileinput('clear');
                                    $('#importJual').attr('disabled', false);
                                });

                            }
                        });
   				}
  			})
 		});

        //Assist
        //servis 
        $('.btn-add-sv').click(function () {  
            var d1 = $('#tglsv1').val().substring(0,2);
            var m1 = $('#tglsv1').val().substring(3,5);
            var y1 = $('#tglsv1').val().substring(6,10);
            var d2 = $('#tglsv2').val().substring(0,2);
            var m2 = $('#tglsv2').val().substring(3,5);
            var y2 = $('#tglsv2').val().substring(6,10);
            var date7       = new Date(y1+"-"+m1+"-"+d1).toISOString().slice(0,10);
            var rdate7      = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1       = new Date(y2+"-"+m2+"-"+d2).toISOString().slice(0,10);
            var rdate1      = moment(date1).format('YYYY-MM-DD HH:mm:ss');  
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            var kddiv       = $('#kddiv').val();
            get_SV(secret_key,api_key,req_time,rdate1,rdate7,kddiv); 
            // alert(rdate7);
        })

            $('.btn-upload-sv').click(function () {   
            save_SV(); 
        })

        $('.tab_sv').click(function () {  
            getTable_sv();
        })
        //direct 
        $('.btn-add-so').click(function () {  
            var d1 = $('#tglso1').val().substring(0,2);
            var m1 = $('#tglso1').val().substring(3,5);
            var y1 = $('#tglso1').val().substring(6,10);
            var d2 = $('#tglso2').val().substring(0,2);
            var m2 = $('#tglso2').val().substring(3,5);
            var y2 = $('#tglso2').val().substring(6,10);
            var date7       = new Date(y1+"-"+m1+"-"+d1).toISOString().slice(0,10);
            var rdate7      = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1       = new Date(y2+"-"+m2+"-"+d2).toISOString().slice(0,10);
            var rdate1      = moment(date1).format('YYYY-MM-DD HH:mm:ss');  
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            var kddiv       = $('#kddiv').val();
            get_SO(secret_key,api_key,req_time,rdate1,rdate7,kddiv); 
            // alert(rdate7);
        })

        $('.btn-upload-so').click(function () {   
            save_SO(); 
        })

        $('.tab_so').click(function () {  
            getTable_SO();
        })
        //PO 
        $('.btn-add-po').click(function () {  
            var d1 = $('#tglpo1').val().substring(0,2);
            var m1 = $('#tglpo1').val().substring(3,5);
            var y1 = $('#tglpo1').val().substring(6,10);
            var d2 = $('#tglpo2').val().substring(0,2);
            var m2 = $('#tglpo2').val().substring(3,5);
            var y2 = $('#tglpo2').val().substring(6,10);
            var date7       = new Date(y1+"-"+m1+"-"+d1).toISOString().slice(0,10);
            var rdate7      = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1       = new Date(y2+"-"+m2+"-"+d2).toISOString().slice(0,10);
            var rdate1      = moment(date1).format('YYYY-MM-DD HH:mm:ss');  
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            var kddiv       = $('#kddiv').val();
            get_PO(secret_key,api_key,req_time,rdate1,rdate7,kddiv); 
            // alert(rdate7);
        })

        $('.btn-upload-po').click(function () {   
            save_SV(); 
        })

        $('.tab_po').click(function () {  
            getTable_PO();
        })

        function disabled(){
            $(".upload_kb").hide();
            $("#kddiv").attr("disabled",false);
            $(".btn-set").show();
            $(".btn-reset").hide();
            $('.form-import').hide();
        }

        function enabled(){
            $("#kddiv").attr("disabled",true);
            $(".btn-set").hide();
            $(".btn-reset").show();
            $('.form-import').show();
        }

    });

    //Servis
    function get_SV(secret_key,api_key,req_time,rdate1,rdate7,kddiv){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SV'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7  , "kddiv" : kddiv },
            success: function (resp) {
                $("#load").fadeOut();
                ins_SV(resp);
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_SV(resp){  
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_SV'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    },function () {
                        tbl_sv.ajax.reload();
                    });
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_SV(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_SV'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        tbl_sv.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function getTable_sv(){
        var column_sv = [];

        column_sv.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        }); 

        $('.tbl_sv_detail').DataTable({});
        tbl_sv = $('.tbl_sv').DataTable({
            "aoColumnDefs": column_sv,
            "order": [[ 1, "asc" ]],
            "columns": [
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "noworkorder" },
                { "data": "tanggalservis" },
                { "data": "nopolisi" },
                { "data": "nomesin" },
                { "data": "norangka" },
                { "data": "namapembawa" },
                { "data": "dealerid" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('msttrk_hso/sv_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                    },
                    exportOptions: {orthogonal: 'export'}
                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                    .addClass('compact')
                                   .css('font-size', 'inherit');
                    }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.tbl_sv').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        // Add event listener for opening and closing details
        $('.tbl_sv tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tbl_sv.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );

        $('.tbl_sv tfoot th').each( function () {
            var title = $('.tbl_sv thead th').eq( $(this).index() ).text();
            if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        tbl_sv.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


                download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover tbl_sv_detail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
                                                '<th style="text-align: center;">Jenis</th>'+
                                                '<th style="text-align: center;">Nama Pekerjaan</th>'+
                                                '<th style="text-align: center;">Jenis Pekerjaan</th>'+
                                                '<th style="text-align: center;">No. Parts</th>'+
                                                '<th style="text-align: center;">Kuantitas </th>'+
                                                '<th style="text-align: center;">Harga</th>'+
                                                '<th style="text-align: center;">Diskon</th>'+
                                                '<th style="text-align: center;">Persentase Diskon</th>'+
                                                '<th style="text-align: center;">Total Harga</th>'+ 
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
                                                '<td style="text-align: ">'+data.jenis+'</td>'+
                                                '<td style="text-align: ">'+data.namapekerjaan+'</td>'+
                                                '<td style="text-align: ">'+data.jenispekerjaan+'</td>'+
                                                '<td style="text-align: ">'+data.partsnumber+'</td>'+
                                                '<td style="text-align: ">'+data.kuantitas+'</td>'+
                                                '<td style="text-align: ">'+data.harga+'</td>'+
                                                '<td style="text-align: ">'+data.diskon+'</td>'+
                                                '<td style="text-align: ">'+data.dispersen+'</td>'+
                                                '<td style="text-align: ">'+data.totalharga+'</td>'+
                    '</tr>';
                    no++;
                });
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    }

    //PO
    function get_PO(secret_key,api_key,req_time,rdate1,rdate7,kddiv){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_PO'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7  , "kddiv" : kddiv },
            success: function (resp) {
                $("#load").fadeOut();
                ins_PO(resp);
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_PO(resp){  
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_PO'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    },function () {
                        tbl_po.ajax.reload();
                    });
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_PO(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_PO'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        tbl_po.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function getTable_PO(){
        var column_po = [];

        column_po.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        }); 

        $('.tbl_po_detail').DataTable({});
        tbl_po = $('.tbl_po').DataTable({
            "aoColumnDefs": column_po,
            "order": [[ 1, "asc" ]],
            "columns": [
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "nopenerimaan" },
                { "data": "tglpenerimaan" }, 
                { "data": "noshippinglist" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('msttrk_hso/po_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                    },
                    exportOptions: {orthogonal: 'export'}
                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                    .addClass('compact')
                                   .css('font-size', 'inherit');
                    }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.tbl_po').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        // Add event listener for opening and closing details
        $('.tbl_po tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tbl_po.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );

        $('.tbl_po tfoot th').each( function () {
            var title = $('.tbl_po thead th').eq( $(this).index() ).text();
            if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        tbl_po.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


                download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover tbl_po_detail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
                                                '<th style="text-align: center;">No Penerimaan</th>'+
                                                '<th style="text-align: center;">Nopo</th>'+
                                                '<th style="text-align: center;">Jenis Order</th>'+
                                                '<th style="text-align: center;">ID Warehouse</th>'+
                                                '<th style="text-align: center;">Kode Part</th>'+
                                                '<th style="text-align: center;">Qty</th>'+ 
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
                                                '<td style="text-align: ">'+data.nopenerimaan+'</td>'+
                                                '<td style="text-align: ">'+data.nopo+'</td>'+
                                                '<td style="text-align: ">'+data.jenisorder+'</td>'+
                                                '<td style="text-align: ">'+data.idwarehouse+'</td>'+
                                                '<td style="text-align: ">'+data.partsnumber+'</td>'+
                                                '<td style="text-align: ">'+data.kuantitas+'</td>'+ 
                    '</tr>';
                    no++;
                });
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    }

    //SO
    function get_SO(secret_key,api_key,req_time,rdate1,rdate7,kddiv){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SO'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7  , "kddiv" : kddiv },
            success: function (resp) {
                $("#load").fadeOut();
                ins_SO(resp);
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_SO(resp){  
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_SO'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){
                    swal({
                        title: "Data Berhasil Disimpan",
                        text: "Data Berhasil",
                        type: "success"
                    },function () {
                        tbl_so.ajax.reload();
                    });
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_SO(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_SO'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        tbl_so.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function getTable_SO(){
        var column_so = [];

        column_so.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        }); 

        $('.tbl_so_detail').DataTable({});
        tbl_so = $('.tbl_so').DataTable({
            "aoColumnDefs": column_so,
            "order": [[ 1, "asc" ]],
            "columns": [
                {
                    "className":      'details-control',
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "noso" },
                { "data": "tglso" }, 
                { "data": "idcustomer" }, 
                { "data": "namacustomer" }, 
                { "data": "discso" }, 
                { "data": "totalhargaso" }, 
                { "data": "dealerid" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('msttrk_hso/po_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'},
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                    },
                    exportOptions: {orthogonal: 'export'}
                },
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                    .addClass('compact')
                                   .css('font-size', 'inherit');
                    }
                }
                */
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.tbl_so').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

        // Add event listener for opening and closing details
        $('.tbl_so tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tbl_so.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child( format(row.data()) ).show();
                //format(row.data());
                tr.addClass('shown');
            }
        } );

        $('.tbl_so tfoot th').each( function () {
            var title = $('.tbl_so thead th').eq( $(this).index() ).text();
            if(title!=="DETAIL" && title!=="EDIT" && title!=="DELETE"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        tbl_so.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2= $Tabla_Nueva.filter(function() {
                 return (this.childElementCount != 1 );
            });

            var $rows = Tabla_Nueva2,
                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // Solo Dios Sabe por que puse esta linea
                colDelim = (filename.indexOf("xls") !=-1)? '"\t"': '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row);
                    var   $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                    return $cols.map(function (j, col) {
                        var $col = $(col);
                        var text = $col.text().replace(/\./g, '');
                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);
                    csv =csv +'"\r\n"' +'fin '+'"\r\n"';
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


                download_csv(csv, filename);
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format ( d ) {
            //console.log(d);
            var tdetail =  '<table class="table table-bordered table-hover tbl_so_detail">'+
                '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">'+
                        '<th style="text-align: center;width:16px;"></th>'+
                        '<th style="text-align: center;width:15px;">No.</th>'+
                                                '<th style="text-align: center;">No SO</th>'+
                                                '<th style="text-align: center;">Kode Part</th>'+
                                                '<th style="text-align: center;">Qty</th>'+
                                                '<th style="text-align: center;">Harga</th>'+
                                                '<th style="text-align: center;">Disc</th>'+
                                                '<th style="text-align: center;">Disc %</th>'+ 
                    '</tr>'+
                '</thead><tbody>';
            var no = 1;
            var total = 0;
            if(d.detail.length>0){
                $.each(d.detail, function(key, data){
                    tdetail+='<tr>'+
                        '<td style="text-align: center;"></td>'+
                        '<td style="text-align: center;">'+no+'</td>'+
                                                '<td style="text-align: ">'+data.noso+'</td>'+
                                                '<td style="text-align: ">'+data.partsnumber+'</td>'+
                                                '<td style="text-align: ">'+data.kuantitas+'</td>'+
                                                '<td style="text-align: ">'+data.hargaparts+'</td>'+
                                                '<td style="text-align: ">'+data.discamount+'</td>'+
                                                '<td style="text-align: ">'+data.discpercentage+'</td>'+ 
                    '</tr>';
                    no++;
                });
            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    }

</script>

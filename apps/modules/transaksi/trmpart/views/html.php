<style type="text/css">
  .{
    margin: -10em 0 0 -10em;
  }
  table th, table td {
    margin: 10000px 0 0 10000px;
/*    border:1px solid #000;  */
    padding:0;
    height: 12px;
    font-size: 12px;
    font-family: 'Nunito', sans-serif;
/*    font-weight: bold;*/
  }
</style>
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

      <?php

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      }

      function baris($jml) {
        $jml = abs($jml);
        $hasil = "";
        if($jml==0){
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==1) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==2) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==3) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } elseif ($jml==4) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==5) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==6) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==7) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==8) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr></tr>";
        }
        return $hasil;
      }

      ?>
          <table border="0" style="width:100%; height:400px; margin: 0 0 0 0;">
            <tbody>
              <!-- <tr>
                <td style="width:2%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:8%;text-align: center;"> - </td>
                <td style="width:2%;text-align: center;"> - </td>
              </tr> -->
              <tr style="height:24px">
                <td style="width:50%" colspan="7"></td>
                <td style="width:50%;text-align: right;font-size:24px;font-weight: bold;" colspan="7" rowspan="2"> FAKTUR PEMBELIAN </td>
              </tr> 
              <tr>
                <td style="width:50%;font-weight:bold;" colspan="7"> <?php echo $data[0]['companyname'];?> </td> 
              </tr> 
              <tr>
                <td style="width:50%" colspan="7"> <?php echo $data[0]['alcomp'];?> Instagram@<?php echo $data[0]['idinsta'];?> </td> 
                <td style="width:8%;text-align: center;"></td> 
                <td style="width:8%;text-align: left;font-size: 18px; font-weight: bold;" rowspan="2"> Nomor </td>
                <td style="width:36%;text-align: left;font-size: 18px; font-weight: bold;" colspan="5" rowspan="2"> : <?php echo $data[0]['nopo'];?></td>
              </tr>     
              <tr>
                <td style="width:50%" colspan="7"> <?php echo $data[0]['notelp2'];?> / <?php echo $data[0]['notelp'];?> </td> 
              </tr>
              <tr>
                <td style="width:50%" colspan="7"></td> 
                <td style="width:8%;text-align: center;"></td> 
                <td style="width:8%;text-align: left;font-size: 18px;" rowspan="2"> Tanggal </td>
                <td style="width:36%;text-align: left;font-size: 18px;" colspan="5" rowspan="2"> : <?php echo date_format(date_create($data[0]['tglpo']),"d-m-Y"); ?></td>
              </tr>     
              <tr>
                <td style="width:50%" colspan="7"></td> 
              </tr>
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr> 
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> Kepada </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['nmsup'];?></td> 
                <td style="width:8%"></td> 
                <td style="width:8%" colspan="1"> Tempo </td> 
                <td style="width:40%" colspan="5"> : <?php echo $data[0]['tmp_hr'];?> Hari</td> 
                <td style="width:2%"></td> 
              </tr>  
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> Alamat </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['alamat'];?></td> 
                <td style="width:8%"></td> 
                <td style="width:8%" colspan="1"> Jth. Tempo </td> 
                <td style="width:40%" colspan="5"> : <?php echo date_format(date_create($data[0]['tanggalpembayaran']),"d/m/Y"); ?> </td> 
                <td style="width:2%"></td> 
              </tr>  
              <tr>
                <td style="width:2%"></td> 
                <td style="width:16%" colspan="2"> Telepon </td> 
                <td style="width:32%" colspan="4"> : <?php echo $data[0]['nohp'];?></td> 
                <td style="width:50%" colspan="7"></td> 
              </tr>   
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr> 
              <tr>
                <td style="width:2%;text-align: center;"> No. </td>
                <td style="width:16%;text-align: center;" colspan="2"> No. Ref # </td>
                <td style="width:16%;text-align: center;" colspan="2"> Kode </td>
                <td style="width:24%;text-align: center;" colspan="3"> Nama </td>
                <td style="width:8%;text-align: right;" colspan="1"> Qty </td>
                <td style="width:8%;text-align: right;" colspan="1"> Satuan  </td>
                <td style="width:8%;text-align: right;" colspan="1"> Harga </td> 
                <td style="width:8%;text-align: right;" colspan="1"> Discount </td> 
                <td style="width:8%;text-align: right;" colspan="1"> Jumlah </td> 
                <td style="width:2%;text-align: center;"></td> 
              </tr> 
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr>   
              <?php
                $no = 1;  
                $sum = [];
                $disc = [];
                $qty = [];
                $ub = [];
                foreach ($data as $value) {

                  array_push($sum, $value['totalharga']); 
                  array_push($disc, $value['nilaidiscount']); 
                  array_push($qty, $value['qty']); 
                  array_push($ub, $value['uangbayar']); 
                  $hasil = '<tr>
                              <td style="width:2%;text-align: center;"> '.$value["no"].' </td>
                              <td style="width:16%;text-align: left;" colspan="2"> '.$value["nofaktur"].' </td>
                              <td style="width:16%;text-align: left;" colspan="2"> '.$value["kdpart"].' </td>
                              <td style="width:24%;text-align: left;" colspan="3"> '.$value["nmpart"].' </td>
                              <td style="width:8%;text-align: right;" colspan="1"> '.$value["qty"].' </td>
                              <td style="width:8%;text-align: center;" colspan="1"> PCS </td>
                              <td style="width:8%;text-align: right;" colspan="1"> '.number_format($value["hargabeli"],0,",",".").' </td>
                              <td style="width:8%;text-align: right;" colspan="1"> '.number_format($value["nilaidiscount"],0,",",".").' </td> 
                              <td style="width:8%;text-align: right;" colspan="1"> '.number_format($value["totalharga"],0,",",".").' </td> 
                              <td style="width:2%;text-align: center;"></td> 
                            </tr>'; 
                  echo $hasil;
                  $no++;  
                } 
                $total = array_sum($sum);
                $totdisc = array_sum($disc);
                $totqty = array_sum($qty);
                $totalub = array_sum($ub);
              ?>
              <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr>   
              <tr> 
                <td style="width:34%;text-align: center;" colspan="6"></td>
                <td style="width:16%;text-align: center;" colspan="2"> Total Qty :</td>
                <td style="width:8%;text-align: right;" > <?php echo $totqty ?>  </td>
                <td style="width:8%;text-align: center;"> </td>
                <td style="width:8%;text-align: center;"> SubTotal</td> 
                <td style="width:8%;text-align: left;">  : </td> 
                <td style="width:8%;text-align: right;"> <?php echo number_format($total,0,",",".") ?>  </td> 
                <td style="width:2%;text-align: center;"> </td> 
              </tr>
              <tr> 
                <td style="width:74%;text-align: center;" colspan="10"></td>
                <td style="width:8%;text-align: center;"> Disc Final</td> 
                <td style="width:8%;text-align: left;"> : 0%</td> 
                <td style="width:8%;text-align: right;"> 0  </td> 
                <td style="width:2%;text-align: center;"> </td> 
              </tr> 
              <!-- <tr>
                <td style="width:100%;text-align: center;" colspan="14"> <hr style="border:1.4px dashed black; padding: 0; margin: 0 0 0 0 ;"> </td>
              </tr>  -->
              <?php
                $jml = $total ;
                $dpp = $total / 1.11;
                $ppn = $total - $dpp;

                if($data[0]['ket']==='1'){
              ?>
                <tr>  
                  <td style="width:2%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"> Disiapkan Oleh,</td>
                  <td style="width:8%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"> Bagian Pembelian,</td>
                  <td style="width:24%;text-align: right;" colspan="3"> Total ( Termasuk PPN )</td> 
                  <td style="width:8%;text-align: left;"> : </td> 
                  <td style="width:8%;text-align: right;" > <?php echo number_format($jml,0,",",".") ?>  </td>  
                  <td style="width:2%;text-align: center;"></td> 
                </tr>
                <tr>  
                  <td style="width:2%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:8%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:24%;text-align: right;" colspan="3"> DPP </td> 
                  <td style="width:8%;text-align: left;"> : </td> 
                  <td style="width:8%;text-align: right;"> <?php echo number_format($dpp,0,",",".") ?>  </td>  
                  <td style="width:2%;text-align: center;"></td> 
                </tr>
                <tr>  
                  <td style="width:2%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:8%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:24%;text-align: right;" colspan="3"> PPN </td> 
                  <td style="width:8%;text-align: left;"> : </td> 
                  <td style="width:8%;text-align: right;" > <?php echo number_format($ppn,0,",",".") ?>  </td>  
                  <td style="width:2%;text-align: center;"></td> 
                </tr> 
              <?php } else { ?>
                <tr>  
                  <td style="width:2%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"> Disiapkan Oleh,</td>
                  <td style="width:8%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"> Bagian Pembelian,</td>
                  <td style="width:24%;text-align: right;" colspan="3">  </td> 
                  <td style="width:8%;text-align: left;"> </td> 
                  <td style="width:8%;text-align: right;" >  </td>  
                  <td style="width:2%;text-align: center;"></td> 
                </tr>
                <tr>  
                  <td style="width:2%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:8%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:24%;text-align: right;" colspan="3">  </td> 
                  <td style="width:8%;text-align: left;"> </td> 
                  <td style="width:8%;text-align: right;">  </td>  
                  <td style="width:2%;text-align: center;"></td> 
                </tr>
                <tr>  
                  <td style="width:2%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:8%;text-align: center;"> </td> 
                  <td style="width:24%;text-align: center;" colspan="3"></td>
                  <td style="width:24%;text-align: right;" colspan="3">  </td> 
                  <td style="width:8%;text-align: left;"> </td> 
                  <td style="width:8%;text-align: right;" >  </td>  
                  <td style="width:2%;text-align: center;"></td> 
                </tr> 
              <?php } ?>

              <tr>  
                <td style="width:2%;text-align: center;"> </td> 
                <td style="width:24%;text-align: center;" colspan="3"></td>
                <td style="width:8%;text-align: center;"> </td> 
                <td style="width:24%;text-align: center;" colspan="3"></td>
                <td style="width:24%;text-align: right;" colspan="3"> Total </td> 
                <td style="width:8%;text-align: left;"> : </td> 
                <td style="width:8%;text-align: right;"> <?php echo number_format($dpp + $ppn,0,",",".") ?>  </td>  
                <td style="width:2%;text-align: center;"></td> 
              </tr>  
              <tr>  
                <td style="width:2%;text-align: center;"> </td> 
                <td style="width:24%;text-align: center;" colspan="3"></td>
                <td style="width:8%;text-align: center;"> </td> 
                <td style="width:24%;text-align: center;" colspan="3"></td>
                <td style="width:24%;text-align: right;" colspan="3"> Bayar </td> 
                <td style="width:8%;text-align: left;"> : </td> 
                <td style="width:8%;text-align: right;"> <?php echo number_format($totalub,0,",",".") ?>  </td>  
                <td style="width:2%;text-align: center;"></td> 
              </tr> 
              <tr>
                <td style="width:2%;text-align: center;"> </td> 
                <td style="width:24%;text-align: center;" colspan="3"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
                <td style="width:8%;text-align: center;"> </td> 
                <td style="width:24%;text-align: center;" colspan="3"> <hr style="border:1.4px black solid; padding: 0; margin: 0 0 0 0 ;"> </td>
                <td style="width:42%;text-align: right;" colspan="6"> </td>  
              </tr>
              <tr>
                <td style="width:2%;text-align: center;"> </td> 
                <td style="width:24%;text-align: left; font-style: italic;" colspan="3"> Created By : <?php echo $data[0]['kabeng'];?> </td>
                <td style="width:74%;text-align: center;" colspan="10"> </td>  
              </tr> 
             </tbody> 
            <tfoot>
            </tfoot>
          </table>
      </div>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_edit'
                    , 'name' => 'form_edit'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="col-lg-4">
                    <div class="form-group">
                        <?php
                            echo form_label($form['nodo']['placeholder']);
                            echo form_input($form['nodo']);
                            echo form_error('nodo','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['noso']['placeholder']);
                            echo form_input($form['noso']);
                            echo form_error('noso','<div class="note">','</div>');
                        ?>
                    </div>

                    <div class="form-group">
                        <?php
                            echo form_input($form['nosin']);
                            echo form_error('nosin','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['tgldo']['placeholder']);
                            echo form_input($form['tgldo']);
                            echo form_error('tgldo','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['jnsbayar']['placeholder']);
                            echo form_input($form['jnsbayar']);
                            echo form_error('jnsbayar','<div class="note">','</div>');
                        ?>
                    </div> 
                </div>
                <div class="col-lg-4"> 
                    <div class="form-group">
                        <?php
                            echo form_label($form['nama']['placeholder']);
                            echo form_input($form['nama']);
                            echo form_error('nama','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['alamat']['placeholder']);
                            echo form_input($form['alamat']);
                            echo form_error('alamat','<div class="note">','</div>');
                        ?>
                    </div>

                    <div class="form-group">
                        <?php
                            echo form_label($form['noktp']['placeholder']);
                            echo form_input($form['noktp']);
                            echo form_error('noktp','<div class="note">','</div>');
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <?php
                            echo form_label($form['harga']['placeholder']);
                            echo form_input($form['harga']);
                            echo form_error('harga','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['diskon']['placeholder']);
                            echo form_input($form['diskon']);
                            echo form_error('diskon','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['dpp_unit']['placeholder']);
                            echo form_input($form['dpp_unit']);
                            echo form_error('dpp_unit','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['ppn_unit']['placeholder']);
                            echo form_input($form['ppn_unit']);
                            echo form_error('ppn_unit','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['bbn']['placeholder']);
                            echo form_input($form['bbn']);
                            echo form_error('bbn','<div class="note">','</div>');
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <?php
                            echo form_label($form['tot_hrg']['placeholder']);
                            echo form_input($form['tot_hrg']);
                            echo form_error('tot_hrg','<div class="note">','</div>');
                        ?>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <a href="<?php echo $reload;?>" class="btn btn-default">
                    Batal
                </a>
            </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        autoNum();
        /*
        $(".form-control").keyup(function(){
            var val = $(this).val();
            $(this).val(val.toUpperCase());
        });
        */
        $(".btn-submit").click(function(){
            updating();
        });

        $("#harga").keyup(function(){
            var harga = $('#harga').autoNumeric('get'); 
            var bbn = $('#bbn').autoNumeric('get');
            var diskon = $('#diskon').autoNumeric('get');
            var total = parseInt(harga) - parseInt(diskon) ; 
            var hrg_tot = parseInt(harga) + parseInt(bbn) - parseInt(diskon) ; 
            var dpp_unit = parseInt(total) / 1.11;
            var ppn_unit = parseInt(dpp_unit) * 0.11; 
            $('#dpp_unit').autoNumeric('set',Math.round(dpp_unit));
            $('#ppn_unit').autoNumeric('set',Math.round(ppn_unit));
            $('#tot_hrg').autoNumeric('set',hrg_tot);
        });

        $("#diskon").keyup(function(){
            var harga = $('#harga').autoNumeric('get'); 
            var bbn = $('#bbn').autoNumeric('get');
            var diskon = $('#diskon').autoNumeric('get');
            var total = parseInt(harga) - parseInt(diskon); 
            var tgldo = $('#tgldo').val();
            // alert(tgldo);
            // if(tgldo>'31-03-2022'){
                // alert('1');
                // var dpp_unit = parseInt(total) / 1.11;
                // var ppn_unit = parseInt(dpp_unit) * 0.11; 
            // } else {
            //     alert('2');
            //     var dpp_unit = parseInt(total) / 1.1;
            //     var ppn_unit = parseInt(dpp_unit) * 0.1; 
            // }
            var hrg_tot = parseInt(harga) + parseInt(bbn) - parseInt(diskon) ; 
                var dpp_unit = parseInt(total) / 1.11;
                var ppn_unit = parseInt(dpp_unit) * 0.11; 
            $('#dpp_unit').autoNumeric('set',Math.round(dpp_unit));
            $('#ppn_unit').autoNumeric('set',Math.round(ppn_unit));
            $('#tot_hrg').autoNumeric('set',hrg_tot); 
        }); 

        $("#bbn").keyup(function(){
            var harga = $('#harga').autoNumeric('get'); 
            var dpp_unit = $('#dpp_unit').autoNumeric('get');
            var ppn_unit = $('#ppn_unit').autoNumeric('get');
            var diskon = $('#diskon').autoNumeric('get');
            var bbn = $('#bbn').autoNumeric('get');
            var hrg_tot = parseInt(harga) + parseInt(bbn) - parseInt(diskon) ; 
            // $('#dpp_unit').autoNumeric('set',dpp_unit);
            // $('#ppn_unit').autoNumeric('set',ppn_unit);
            $('#tot_hrg').autoNumeric('set',hrg_tot); 
        }); 


    });

    function autoNum(){
        $('#harga').autoNumeric('init');
        $('#diskon').autoNumeric('init');
        $('#dpp_unit').autoNumeric('init');
        $('#ppn_unit').autoNumeric('init');
        $('#bbn').autoNumeric('init');
        $('#tot_hrg').autoNumeric('init');
    }



    function updating(){
        swal({
            title: "Konfirmasi",
            text: "Proses Update Diskon Invoice Penjualan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            },

            function () {
              var nodo = $("#nodo").val();
              var diskon = $("#diskon").autoNumeric('get');
              var dpp_unit = $("#dpp_unit").autoNumeric('get');
              var ppn_unit = $("#ppn_unit").autoNumeric('get');
              var bbn = $("#bbn").autoNumeric('get');
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('revinvso/proses');?>",
                    data: {"nodo":nodo
                          ,"diskon":diskon
                          ,"dpp_unit":dpp_unit
                          ,"ppn_unit":ppn_unit
                          ,"bbn":bbn
                  },
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                window.location.href = "<?=site_url('revinvso/');?>"
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }

</script>

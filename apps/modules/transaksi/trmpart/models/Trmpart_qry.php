<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Trmpart_qry
 *
 * @author adi
 */
class Trmpart_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    } 

    //cari no assist
    public function getNotrm() { 
        if ($this->session->userdata('data')['kddiv']==='ZPH01.01B'){
            $idtrm = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B'){
            $idtrm = '12603';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B'){
            $idtrm = '13435';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B'){
            $idtrm = '13528';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B'){
            $idtrm = '14031';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B'){
            $idtrm = '15544';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.01B.01'){
            $idtrm = '10091';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.01'){
            $idtrm = '12603';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH01.02B.02'){
            $idtrm = '12603';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.01B.01'){
            $idtrm = '13435';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH02.02B.01'){
            $idtrm = '13528';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH03.01B.01'){
            $idtrm = '14031';
        } else if ($this->session->userdata('data')['kddiv']==='ZPH04.01B.01'){
            $idtrm = '15544';
        } else {
            $idtrm = '';
        } 
        $q = $this->db->query("select nopenerimaan,tglpenerimaan,noshippinglist from api.bpo where nopenerimaan like '".$idtrm."%' AND nopenerimaan not in (select grno from bkl.tb_po group by grno) group by nopenerimaan order by nopenerimaan ASC");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return $res;
    }

    public function getDataSupplier() {
        $this->db->select("kdsupplier,nmsupplier");
        $this->db->where("faktif",true);
        if($this->session->userdata('data')['kddiv']!='ZPH01.02S'){
            // $idr = array()
            $this->db->where("kddiv",substr($this->session->userdata('data')['kddiv'],0,9));
            $this->db->or_where("kddiv",null);
        }
        $this->db->order_by("kdsupplier","ASC");
        $query = $this->db->get("bkl.supplier");
        // echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }

    public function getGrup() {
        $q = $this->db->query("select kdgrup from bkl.grup where fjasa = false group by kdgrup");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return $res;
    }
       // if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            // $q = $this->db->query("select kdpart,nmpart from bkl.part group by kdpart,nmpart");   
        // } else {
            // $q = $this->db->query("select kdpart,nmpart from bkl.part where kddiv = substring('".$this->session->userdata('data')['kddiv']."',0,10) group by kdpart,nmpart");
            
    public function getPart() {
        $q = $this->db->query("select a.kdpart,a.nmpart from bkl.part a join bkl.grup b on a.kdgrup = b.kdgrup where b.fjasa = false group by a.kdpart,a.nmpart order by a.kdpart,a.nmpart ");
        
       
                
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return $res;
    }

    //import
    public function get_PO() {  
        $secret_key  = $this->input->post('secret_key'); 
        $api_key    = $this->input->post('api_key'); 
        $req_time  = $this->input->post('req_time'); 
        $rdate1    = $this->input->post('rdate1'); 
        $rdate7    = $this->input->post('rdate7'); 

        $hash = hash('sha256', $api_key.$secret_key.$req_time);

        if ($this->input->post('kddiv')==='SHOWROOM'){
            if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
                $kddiv = 'ZPH01.01B';    
            } else {
                $kddiv = $this->session->userdata('data')['kddiv'];
            }  
        }

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://astraapigc.astra.co.id/dmsahassapi/dgi-api/v2/pinb/read',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "fromTime": "'.$rdate7.'",
                "toTime": "'.$rdate1.'",
                "dealerId":"'.$dealerId.'",
                "noPO" : ""
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-Request-Time: '.$req_time,
                'DGI-Api-Key: '.$api_key,
                'DGI-API-Token: '.$hash,
                'Cookie: __cf_bm=yibWElurH59qWQNssvTYCZzwSaxRMYs1kH_WT_v3QYY-1698196857-0-AU80JsC+DN7RnicjIxWqPepjs7Lkv1J9B8YzWY+FfzshqpoddUkEKQzhDQMaHaQlglJayvb8Z02btLqh1JKyNw8='
            ),
        ));   

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response; 
    } 

    public function ins_PO() {    
        $info   = $this->input->post('info'); 
        $info   = str_replace("'","''",$info);
        $q = $this->db->query("insert into api.bpo_tmp (info) values ('".$info."')");
        // $r = $this->db->query("update dbo.MS_KEND_DEALER set fresi = '".$lnotis."', nopolisi = '".$nopolisi."', img = '".$img."' where notrans = '".$notrans."'");
        // echo $this->db->last_query();   
        if($q>0){
            $res = "success";
        }else{
            $res = "";
        }
        // echo $q;
        // $res = "";

        return json_encode($res);
    }

    public function save_PO() { 
        $q = $this->db->query("select title,msg,tipe from api.in_bpo()");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    //table 
    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['notrm']) ){
            if($_GET['notrm']){
                $notrm = $_GET['notrm'];
            }else{
                $notrm = '';
            }
        }else{
            $notrm = '';
        } 

        $aColumns = array('nofaktur',
                          'materialno',
                          'qtypo',
                          'hargabeli',
                          'nilaidiscount',
                          'totalharga',
                          'uangbayar',
                          'kdgrup',
                          'grno',
                          'no',
                          'nourut');
        $sIndexColumn = "materialno";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( SELECT '' as no, a.*
                              FROM api.in_bpo('".$notrm."','".$this->session->userdata('data')['kddiv']."') a) AS a"; 

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "order by materialno";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
 
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }

            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nofaktur']."','".$aRow['materialno']."','".$aRow['qtypo']."',".$aRow['hargabeli'].",".$aRow['nilaidiscount'].",".$aRow['totalharga'].",".$aRow['uangbayar'].",'".$aRow['kdgrup']."','".$aRow['grno']."',".$aRow['nourut'].");\">Edit</button>"; 
            $aRow['hapus'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-warning btn-xs \" onclick=\"del('".$aRow['materialno']."');\">Delete</button>"; 

            $output['data'][] = $aRow;
        }
      echo  json_encode( $output );
    } 

    public function set_nmpart(){
        $kdpart = $this->input->post('materialno');

        $query = $this->db->query("select * from bkl.part where kdpart =  '".$kdpart."'");
        echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);

    }

    public function batal() {     
        $nobpo = $this->input->post('notrm');
        $q = $this->db->query("select * from api.btl_bpo('".$nobpo."','".$this->session->userdata('data')['kddiv']."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    } 

    public function update() {
      $notrm  = $this->input->post('notrm');
      $kdpart = $this->input->post('kdpart');
      $nofa = $this->input->post('nofa');
      $qty    = $this->input->post('qty');
      $harga  = $this->input->post('harga');
      $disc   = $this->input->post('disc');
      $total  = $this->input->post('total');
      $bayar  = $this->input->post('bayar');
      $grno  = $this->input->post('grno');
      $nourut  = $this->input->post('nourut');      

    //   $q  = $this->db->query("update api.t_bpo_tmp set nofaktur = '".$nofa."',qtypo = ".$qty.", hargabeli = ".$harga.", nilaidiscount = ".$disc." , totalharga = ".$total.", uangbayar = ".$bayar." where pono = '".$notrm."' and kddiv = '".$this->session->userdata('data')['kddiv']."' and materialno = '".$kdpart."' and qtypo = '".$qty."' and grno = '".$grno."'");
      $q  = $this->db->query("update api.t_bpo_tmp set nofaktur = '".$nofa."',qtypo = ".$qty.", hargabeli = ".$harga.", nilaidiscount = ".$disc." , totalharga = ".$total.", uangbayar = ".$bayar." where pono = '".$notrm."' and kddiv = '".$this->session->userdata('data')['kddiv']."' and materialno = '".$kdpart."' and nourut = '".$nourut."' and grno = '".$grno."'");      
      
      if(!$q) { 
          $this->state = "0";
      } else { 
          $this->state = "1";
      }   
      return json_encode($this->state);  
    }

    public function det_update() {  
      $kdpart   = $this->input->post('kdpart');
      $qty      = $this->input->post('qty');
      $kdgrup   = $this->input->post('kdgrup');
      $harga    = $this->input->post('harga');
      $disc     = $this->input->post('disc');
      $total    = $this->input->post('total');
      $nofa       = $this->input->post('nofa'); 
      $ub       = $this->input->post('ub'); 
      $query        = $this->db->query("select * from api.bpo_d_ins('".$kdpart."'
                                                                  ,".$qty."
                                                                  ,'".$kdgrup."'
                                                                  ,".$harga."
                                                                  ,".$disc."
                                                                  ,".$total."
                                                                  ,".$ub."
                                                                  ,'".$nofa."'
                                                                  ,'".$this->session->userdata('data')['kddiv']."')");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    } 

    public function del() {  
      $notrm        = $this->input->post('notrm');
      $materialno   = $this->input->post('materialno'); 
      $query        = $this->db->query("delete from api.t_bpo_tmp where pono = '".$notrm."' and materialno = '".$materialno."' and kddiv = '".$this->session->userdata('data')['kddiv']."'");
      // echo $this->db->last_query();
      if(!$query) { 
          $this->state = "0";
      } else { 
          $this->state = "1";
      }   
      return json_encode($this->state);  
    } 

    public function submit() { 
      $notrm        = $this->input->post('notrm');
      // $nofa         = $this->input->post('nofa');
      $kdsupplier   = $this->input->post('kdsupplier');
      $tipebayar    = $this->input->post('tipebayar');
      $tgltrm       = $this->apps->dateConvert($this->input->post('tgltrm'));
      $tglbyr       = $this->apps->dateConvert($this->input->post('tglbyr'));
      $query        = $this->db->query("select * from api.bpo_ins('".$notrm."'  
                                                                  ,".$kdsupplier."
                                                                  ,'".$tipebayar."'
                                                                  ,'".$tgltrm."'
                                                                  ,'".$tglbyr."'
                                                                  ,'".$this->session->userdata('data')['kddiv']."'
                                                                  ,'".$this->session->userdata('username')."')");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    } 

    private function getdetail_GR(){
        $output = array();
        $str = "SELECT a.materialno
                    , b.nmpart
                    , a.qtypo
                    , a.hargabeli
                    , a.nilaidiscount
                    , a.totalharga
                    , a.uangbayar 
                    , a.pono
                      FROM bkl.tb_po_d a
                      JOIN bkl.part b ON a.materialno = b.kdpart
                      WHERE a.pono not like '%BAGI%'
                    ORDER BY a.materialno";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
      }
        return $output;
    }

    private function getdetail_BAG(){
        $output = array();
        $str = "SELECT a.materialno
                    , b.nmpart
                    , a.qtypo
                    , a.hargabeli
                    , a.nilaidiscount
                    , a.totalharga
                    , a.uangbayar 
                    , a.pono
                      FROM bkl.tb_po_d a
                      JOIN bkl.part b ON a.materialno = b.kdpart
                      WHERE a.pono like '%BAGI%'
                    ORDER BY a.materialno";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
      }
        return $output;
    }

    private function getdetail_PRS(){
        $output = array();
        $str = "SELECT a.nopenerimaan
                    , a.nopo
                    , a.jenisorder
                    , a.idwarehouse
                    , a.partsnumber
                    , CASE WHEN b.nmpart is null THEN 'BELUM TERDAFTAR DATA MASTER' else b.nmpart END AS nmpart
                    , a.kuantitas
                    , a.uom
                      FROM api.bpo_d a
                      JOIN bkl.part b ON a.partsnumber = b.kdpart
                    GROUP BY a.nopenerimaan, a.nopo, a.jenisorder, a.idwarehouse, a.partsnumber, b.nmpart, a.kuantitas, a.uom
                    ORDER BY a.nopenerimaan";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
      }
        return $output;
    }

    public function json_dgview() {
        error_reporting(-1);
        
        if( isset($_GET['periode_gr']) ){
            if($_GET['periode_gr']){
                $tgl2 = explode('-', $_GET['periode_gr']);
                $periode_awal = $tgl2[1].$tgl2[0]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode_awal = '';
            } 
        }else{
            $periode_awal = '';
        } 

        $aColumns = array('pono', 'podate', 'vendorname', 'vendoraddress','tipepembayaran' );
        $sIndexColumn = "pono";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, pono, podate, vendorname, vendoraddress, tipepembayaran
                        FROM bkl.tb_po where date_to_periode(podate) = '".$periode_awal."' and pono not like '%BAGI%') AS a";
        } else {
            $sTable = " ( SELECT '' as no, pono, podate, vendorname, vendoraddress, tipepembayaran
                        FROM bkl.tb_po where date_to_periode(podate) = '".$periode_awal."' and pono not like '%BAGI%' and substring(kddiv,1,9) = substring('".$this->session->userdata('data')['kddiv']."',1,9)) AS a";
        }
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by pono desc";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_GR();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['pono']==$value['pono']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function json_dgview_bag() {
        error_reporting(-1);
        
        if( isset($_GET['periode_bag']) ){
            if($_GET['periode_bag']){
                $tgl2 = explode('-', $_GET['periode_bag']);
                $periode_awal = $tgl2[1].$tgl2[0]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode_awal = '';
            } 
        }else{
            $periode_awal = '';
        } 

        $aColumns = array('pono', 'podate', 'vendorname', 'vendoraddress','tipepembayaran' );
        $sIndexColumn = "pono";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, pono, podate, vendorname, vendoraddress, tipepembayaran
                        FROM bkl.tb_po where date_to_periode(podate) = '".$periode_awal."' and pono like '%BAGI%') AS a";
        } else {
            $sTable = " ( SELECT '' as no, pono, podate, vendorname, vendoraddress, tipepembayaran
                        FROM bkl.tb_po where date_to_periode(podate) = '".$periode_awal."' and pono like '%BAGI%' and substring(kddiv,1,9) = substring('".$this->session->userdata('data')['kddiv']."',1,9)) AS a";
        }
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by pono desc";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_BAG();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['pono']==$value['pono']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function json_dgview_prs() {
        error_reporting(-1);
        
        if( isset($_GET['periode_prs']) ){
            if($_GET['periode_prs']){
                $tgl2 = explode('-', $_GET['periode_prs']);
                $periode_awal = $tgl2[1].$tgl2[0]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode_awal = '';
            } 
        }else{
            $periode_awal = '';
        } 

        $aColumns = array('nopenerimaan', 'tglpenerimaan', 'noshippinglist');
        $sIndexColumn = "nopenerimaan";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }

        $kddiv = $this->session->userdata('data')['kddiv'];

        if($kddiv==='ZPH01.01B'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B'){
            $dealerId = '15544';        
        } else if ($kddiv==='ZPH01.01B.01'){
            $dealerId = '10091';
        } else if ($kddiv==='ZPH01.02B.01'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH01.02B.02'){
            $dealerId = '12603'; 
        } else if ($kddiv==='ZPH02.01B.01'){
            $dealerId = '13435'; 
        } else if ($kddiv==='ZPH02.02B.01'){
            $dealerId = '13528'; 
        } else if ($kddiv==='ZPH03.01B.01'){
            $dealerId = '14031'; 
        } else if ($kddiv==='ZPH04.01B.01'){
            $dealerId = '15544';        
        }

        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $sTable = " ( SELECT '' as no, nopenerimaan, tglpenerimaan, noshippinglist
                        FROM api.bpo where date_to_periode(tglpenerimaan::date) = '".$periode_awal."' and nopenerimaan not in (select grno from bkl.tb_po where grno is not null group by grno) GROUP BY nopenerimaan, tglpenerimaan, noshippinglist) AS a";
        } else {
            $sTable = " ( SELECT '' as no, nopenerimaan, tglpenerimaan, noshippinglist
                        FROM api.bpo where date_to_periode(tglpenerimaan::date) = '".$periode_awal."' and substring(nopenerimaan,0,6) = '".$dealerId."' and nopenerimaan not in (select grno from bkl.tb_po where grno is not null group by grno) GROUP BY nopenerimaan, tglpenerimaan, noshippinglist) AS a";
        }
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        if(empty($sOrder)){
            $sOrder = " order by nopenerimaan desc";
        }
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail_PRS();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nopenerimaan']==$value['nopenerimaan']){
                    $aRow['detail'][]= $value;
                }
            }

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }

    public function html($nopo) {
        $q = $this->db->query("SELECT *,ROW_NUMBER () OVER (ORDER BY nopo) as no FROM bkl.vb_po where nopo = upper('".$nopo."') ");
        // $this->db->select("to_char(now(),'HH:MI:SS') as wkt, *");
        // $this->db->where('nodo',upper('.$nodo.'));
        // $q = $this->db->get("pzu.vb_do");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

}

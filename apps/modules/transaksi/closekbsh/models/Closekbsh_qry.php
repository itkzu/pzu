<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Closekbsh_qry
 *
 * @author adi
 */
class Closekbsh_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function refKasBank() {
        if($this->perusahaan[0]['kddiv']===$this->session->userdata('data')['kddiv']){ 
            $this->db->where('faktif','true');
            $this->db->order_by('nmkb','asc');
        } else {
          $this->db->where('kddiv',$this->session->userdata('data')['kddiv']);
          $this->db->where('faktif','true');
          $this->db->order_by('nmkb','asc');
        }
        $query = $this->db->get('pzu.kasbank');
        return $query->result_array();
    }

    public function tglkb() {
        $kdkb = $this->input->post('kdkb');
        $q = $this->db->query("select kdkb, nmkb, tglkb from pzu.kasbank where kdkb = '".$kdkb."' AND kddiv = '". $this->session->userdata('data')['kddiv'] ."'");
        $res = $q->result_array();
        return json_encode($res);
    }

    public function setNilai() {
        $kdkb = $this->input->post('kdkb');
        $query = $this->db->query("select * from bkl.vx_kasbank where kdkb = '".$kdkb."'");
        $res = $query->result_array();
        return json_encode($res);
    }

    public function process() {
        $kdkb = $this->input->post('kdkb');
        $so_fisik = $this->input->post('so_fisik');
        $query = $this->db->query("select title,msg,tipe from bkl.closing_kb('".$kdkb."',".$so_fisik.")");
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

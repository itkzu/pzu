<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">

                    <div class="col-md-6 col-lg-6">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['nodf']['placeholder']);
                                        echo form_input($form['nodf']);
                                        echo form_error('nodf','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['tgldf']['placeholder']);
                                        echo form_input($form['tgldf']);
                                        echo form_error('tgldf','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="row">
                            <!--
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['ket']['placeholder']);
                                        echo form_input($form['ket']);
                                        echo form_error('ket','<div class="note">','</div>');
                                    ?>
                                 </div>
                            </div>-->
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?=form_label($form['kddiv']['placeholder']);?>
                                    <div class="input-group">
                                        <?php
                                            echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                            echo form_error('kddiv', '<div class="note">', '</div>');
                                        ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-set" id="btn-set">Set</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?=form_label($form['nopo']['placeholder']);?>
                                    <div class="input-group">
                                        <?php
                                            echo form_input($form['nopo']);
                                            echo form_error('nopo', '<div class="note">', '</div>');
                                        ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-cari" id="btn-cari">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['tglpo']['placeholder']);
                                        echo form_input($form['tglpo']);
                                        echo form_error('tglpo','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_input($form['nourut']);
                                        echo form_error('nourut','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['nap']['placeholder']);
                                        echo form_input($form['nap']);
                                        echo form_error('nap','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?php
                                        echo form_label($form['nint']['placeholder']);
                                        echo form_input($form['nint']);
                                        echo form_error('nint','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                  <?php
                                      echo form_label($form['total']['placeholder']);
                                      echo form_input($form['total']);
                                      echo form_error('total','<div class="note">','</div>');
                                  ?>

                                </div>
                            </div>


                            <!--
                            <div class="col-xs-6">
                                <div class="form-group">
                                  <?php
                                      //echo form_input($form['nsel']);
                                      //echo form_error('nsel','<div class="note">','</div>');
                                  ?>
                                </div>
                            </div>
                            -->

                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-add">
                                        Tambah
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;text-align: center;">No. Faktur</th>
                                        <th style="width: 120px;text-align: center;">Tgl Faktur</th>
                                        <th style="width: 120px;text-align: center;">Pokok Hutang</th>
                                        <th style="width: 200px;text-align: center;">Bunga</th>
                                        <th style="width: 200px;text-align: center;">Total</th>
                                        <th style="width: 10px;text-align: center;">Edit</th>
                                        <th style="width: 10px;text-align: center;">Hapus</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th style="text-align: right;"></th>
                                        <th style="text-align: right;"></th>
                                        <th style="text-align: right;"></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function () {

        /*
        $('#tglpo').datepicker({
            format: "dd-mm-yyyy"
        });
        */

        if(!$("#nodf").val()){
            $("#kddiv").select2({});
            $("#nopo").prop("readonly", true);
            $(".btn-cari").hide();
        } else {
            $("#kddiv").attr('disabled',true);
            $(".btn-set").hide();
            $("#nopo").prop("readonly", false);
            $(".btn-cari").show();
        };

        $('#nap').autoNumeric();
        $('#nint').autoNumeric();
        $('#nsel').autoNumeric();
        $('#total').autoNumeric();


        var column = [];

        column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });

        column.push({
            "aTargets": [1],
            "mRender": function (data, type, full) {
              return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        column.push({
            "aTargets": [ 2,3,4 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });


        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nopo"  },
                { "data": "tglpo" },
                { "data": "nap"   },
                { "data": "nint"  },
                { "data": "total" },
                { "data": "edit"  },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "nodf", "value": $("#nodf").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){

            },
            "sAjaxSource": "<?=site_url('dfnew/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;


                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var pokok = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var bunga = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 1 ).footer() ).html('Total');
                $( api.column( 2 ).footer() ).html(numeral(pokok).format('0,0.00'));
                $( api.column( 3 ).footer() ).html(numeral(bunga).format('0,0.00'));
                $( api.column( 4 ).footer() ).html(numeral(total).format('0,0.00'));
            },
            buttons: [
                {
                    extend:    'excelHtml5',
                    text:      'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": { filter: 'applied', order: 'current' },
                    "sFileName": "report.xls",
                    action : function( e, dt, button, config ) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                }
            ],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });




        $(".btn-set").click(function(){
            disabled();
        });

        $(".btn-cari").click(function(){
            getDetailPO();
        });

        $(".btn-add").click(function(){
            addDetail($("#nodf").val() , $("#nopo").val());
        });

        $(".btn-reset-detail").click(function(){
            resetDetail();
        });

        $(".btn-batal").click(function(){
            batal();
        });

        $(".btn-submit").click(function(){
            submit();
        });



        /*
        function disable() {
                  document.getElementById("kddiv").option["selected"].disabled=true;
        }
        */

        var nopo_input = document.getElementById("nopo");
        // Execute a function when the user releases a key on the keyboard
        nopo_input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("btn-cari").click();
            }
        });



    });

    /*
    function getDiv(){
        var kddiv = $("#kddiv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('dfnew/getDiv');?>",
            data: {"kddiv":kddiv},
            success: function(resp){

                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var tgl_trm_fa = "";
                    $.each(obj, function(key, data){
                        $("#kddiv").val(data.kddiv);
                        $("#nmdiv").val(data.nmdiv);
                    });
                    $("#nopo").prop("readonly", false);
                    $(".btn-cari").show();

                }else{
                    swal("Error", obj.msg, "error");
                }
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function disable(){
        document.getElementById("kddiv").disabled=true;
      }
    */

    function sum() {
        var nap = $("#nap").autoNumeric('get');
        var nint = $("#nint").autoNumeric('get');

        var result = parseFloat(nap) + parseFloat(nint);
        if (!isNaN(result)){
            $("#total").val(result);
        }
    }

    function disabled(){
        $("#kddiv").attr('disabled',true);
        $("#nopo").prop("readonly", false);
        $(".btn-set").hide();
        $(".btn-cari").show();
    }


    function resetDetail(){
        //$("#nourut").val('').trigger("chosen:updated");
        $("#nourut").val('');
        $("#total").val('');
        $("#nopo").val('');
        $("#tglpo").val('');
        $("#nap").val('');
        $("#nint").val('');
    }

    function edit(nodf,nourut,nopo,tglpo,nap,nint){
        $("#nodf").val(nodf.replace('/-/g','/'));
        $("#nourut").val(nourut);
        $("#nopo").val(nopo).prop("readonly", true);
        $("#tglpo").val(tglpo);

        $("#nap").val(nap);     $("#nap").autoNumeric('init');
        $("#nint").val(nint);   $("#nint").autoNumeric('init');

        $("#kddiv").attr('disabled',true);
        $(".btn-set").hide();
        $(".btn-cari").hide();
        sum();
    }

    function getDetailPO(){
        var kddiv = $("#kddiv").val();
        var nopo = $("#nopo").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('dfnew/getDetailPO');?>",
            data: {"nopo":nopo,"kddiv":kddiv},
            success: function(resp){

                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var tgl_trm_fa = "";
                    $.each(obj, function(key, data){
                        $("#nopo").val(data.nopo);
                        $("#nmdiv").val(data.nmdiv);
                        $("#tglpo").val(data.tglpo);
                        $("#nourut").val(data.nourut);
                        $("#nap").val(data.nap);
                        $("nint").val('0');
                        $("#total").val(data.nap);
                    });
                }else{
                    swal("Error", obj.msg, "error");
                }
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function addDetail(nodf,nopo,kddiv){

        var nodf = $("#nodf").val();

        if ($("#nourut").val() == ''){
            var nourut ='0';
        } else {
            var nourut = $("#nourut").val();
        }

        var tgldf = $("#tgldf").val();
        var ket = $("#ket").val();
        var kddiv = $("#kddiv").val();
        var nopo = $("#nopo").val();
        var nint = $("#nint").autoNumeric('get');
        //alert(nint);

        $.ajax({
            type: "POST",
            url: "<?=site_url("dfnew/addDetail");?>",
            data: {"nodf":nodf
                    ,"nourut":nourut
                    ,"tgldf":tgldf
                    ,"ket":ket
                    ,"kddiv":kddiv
                    ,"nopo":nopo
                    ,"nint":nint },
            success: function(resp){
                $(".btn-cari").show();
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    //$('#kddiv').val(data.kddiv).attr("selected", "selected");
                    $("#nodf").val(data.nodf);
                    resetDetail();
                    if (data.tipe==="error"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            //refresh();
                        });
                    }else{
                        //refresh();
                        $("#nopo").prop("readonly", false);
                    }
                    //alert(data.title);
                    /*localStorage.setItem("v_df_d", JSON.stringify({"nodf":obj.nodf
                                                       ,"nopo":nopo
                                                       ,"tglpo":tglpo
                                                       ,"nap":nap
                                                       ,"nit":nit
                                                       ,"nsel":nsel}));*/


                });
                refresh();
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                //refresh();
            }
        });
    }

    function deleted(nodf,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("dfnew/detaildeleted");?>",
                data: {"nodf":nodf
                        ,"nourut":nourut
                      },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    console.log(obj)
                    $.each(obj, function(key, data){

                        $("#nodf").val(data.rnodf);
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    /*
    function get_data_edit(){
                    var nodf = $("#nodf").val();
                    $.ajax({
                        url : "<?php echo site_url('dfnew/get_data_edit');?>",
                        method : "POST",
                        data :{'nodf' :nodf},
                        async : true,
                        dataType : 'json',
                        success : function(data){
                          $.each(obj, function(key, data){
                                $("#kddiv").val(data.kddiv).trigger('change');
                            });
                        }

                    });
                }
    */

    function batal(){
        var nodf = $("#nodf").val();
        if(nodf!==""){
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
                window.location.href = '<?=site_url('dfnew');?>';

            });

        }else{
            window.location.href = '<?=site_url('dfnew');?>';
        }
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nodf = $("#nodf").val();
            var tgldf = $("#tgldf").val();
            var ket = $("#ket").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("dfnew/submit");?>",
                data: {"nodf":nodf
                        ,"tgldf":tgldf
                        ,"ket":ket },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if (data.tipe==='success'){
                            window.location.href = '<?=site_url('dfnew');?>';
                        }
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    /*
    function getHeader(){
        if (localStorage.refkb_m){
            var obj = jQuery.parseJSON(localStorage.refkb_m);
            $("#nodf").val(obj.nodf);
            $("#nmrefkb").val(obj.nmrefkb);
            $("#dk").val(obj.dk);
            $("#faktif").val(obj.faktif);
            $("#ket").val(obj.ket);
        }
    }
    */

</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">
          <div class="box-header box-view">
              <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a> 
              <a class="btn btn-warning btn-ubah" disabled>Ubah</a> 
              <a class="btn btn-danger btn-hapus">Hapus</a> 
            <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
          </div> 

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['nompp']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nompp']);
                                    echo form_error('nompp','<div class="note">','</div>');
                                ?>
                            </div>
                        </div> 
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['tglmpp']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['tglmpp']);
                                    echo form_error('tglmpp','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>  
                      </div>
                  </div>

                  <div class="col-xs-12">
                      <br>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-2">
                            <label>
                              <?php echo form_label($form['nosin']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nosin']);
                                      echo form_error('nosin','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div> 

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nora']);
                                      echo form_error('nora','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>  
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['kdtipe']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['kdtipe']);
                                  echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nmtipe']);
                                    echo form_error('nmtipe','<div class="note">','</div>');
                                ?>
                            </div>
                        </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['warna']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['warna']);
                                  echo form_error('warna','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                        <div class="col-xs-1">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['tahun']);
                                  echo form_error('tahun','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                      </div>
                  </div> 

                  <div class="col-xs-12">
                      <br>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['kdlokasi1']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['kdlokasi1']['name'],$form['kdlokasi1']['data'] ,$form['kdlokasi1']['value'] ,$form['kdlokasi1']['attr']);
                                    echo form_error('kdlokasi1','<div class="note">','</div>');
                                  // echo form_label($form['kdtipe']['placeholder']);
                                  // echo form_input($form['kdtipe']);
                                  // echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>  

                        <div class="col-xs-3">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['nmlokasi1']);
                                  echo form_error('nmlokasi1','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['kdlokasi2']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['kdlokasi2']['name'],$form['kdlokasi2']['data'] ,$form['kdlokasi2']['value'] ,$form['kdlokasi2']['attr']);
                                    echo form_error('kdlokasi2','<div class="note">','</div>');
                                  // echo form_label($form['kdtipe']['placeholder']);
                                  // echo form_input($form['kdtipe']);
                                  // echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>  

                        <div class="col-xs-3">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['nmlokasi2']);
                                  echo form_error('nmlokasi2','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <br>
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['ket']['placeholder']); ?>
                          </label>
                        </div> 

                        <div class="col-xs-4">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['ket']);
                                  echo form_error('ket','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div> 


              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 
        clear();
        disabled();
        $('#nompp').mask('MPP/****/****');
        
        $('#nompp').change(function(){
            setnompp();
        });
        
        $('.btn-hapus').click(function(){
            hapus();
        });
    }); 

    // get no do
    function setnompp(){
        var nompp = $('#nompp').val(); 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("mtsunit/setnompp");?>",
            data: {"nompp":nompp},
            success: function(resp){
              if(resp==='"empty"'){ 
                  clear();  
              }else{
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#tglmpp').val($.datepicker.formatDate('dd-mm-yy', new Date(value.tglmpp)));
                    $('#nosin').val(value.nosin2);
                    $('#nora').val(value.nora2); 
                    $('#kdtipe').val(value.kdtipe);
                    $('#nmtipe').val(value.nmtipe); 
                    $('#warna').val(value.warna);
                    $('#tahun').val(value.tahun); 
                    $('#kdlokasi1').val(value.kdlok_awal);
                    $('#nmlokasi1').val(value.status_ket);
                    $('#kdlokasi2').val(value.kdlok_akhir);
                    $('#nmlokasi2').val(value.status_ket_akhir);
                    $('#ket').val(value.ket);
                });
              }
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    function hapus(){
        var nompp = $('#nompp').val(); 
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("mtsunit/hapus");?>",
                data: {"nompp":nompp},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            window.location.href = '<?=site_url('mtsunit');?>';
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    // disabled
    function disabled(){ 
        $('#tglmpp').attr('disabled',true);
        $('#kdlokasi2').prop('disabled',true);
        $('#kdlokasi1').prop('disabled',true);
        $('#nosin').prop('disabled',true);
        $('#ket').prop('disabled',true);
    }  

    // clear
    function clear(){  
        // var date = Date.now(); 
        $('#nompp').val('');
        $('#tglmpp').val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#kdlokasi1').val('');
        $('#kdlokasi2').val('');
        $('#nosin').val('');
        $('#nora').val('');
        $('#kdtipe').val('');
        $('#nmtipe').val('');
        $('#warna').val('');
        $('#tahun').val('');
        $('#ket').val('');
    }  
</script>

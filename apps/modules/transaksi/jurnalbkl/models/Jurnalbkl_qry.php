<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Jurnalbkl_qry
 *
 * @author adi
 */
class Jurnalbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataPos() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_div_unit_bkl()");
        return $q->result_array();
    }

    public function getDataCabang() {
        $this->db->select('kddiv as kdcab ,nmdiv as nmcab');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_div_unit_bkl()");
        return $q->result_array();
    }

    public function getKodeAkun() {
        $kddiv = $this->input->post('kddiv');
        $this->db->select("*");
        $this->db->where("kddiv",$kddiv);
        $this->db->order_by("kdakun","ASC");
        $query = $this->db->get('glr.v_akun_div');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function getKodeAkunEdit() {
        $kddiv = $this->input->post('kddiv');
        $this->db->select("*");
        $this->db->where("kddiv",$kddiv);
        $this->db->where("faktif",true);
        $query = $this->db->get('glr.v_akun_div');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function setnojurnal() {
        $nojurnal = $this->input->post('nojurnal');
        $kddiv = $this->input->post('kddiv');
        $q = $this->db->query("select * from glr.t_jurnal where nojurnal like '%".$nojurnal."%' and kddiv = '".$kddiv."' union select * from glr.t_jurnal_tmp where nomor like '%".$nojurnal."%' and kddiv = '".$kddiv."'");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3); 
        $this->db->select('*'); 
        $q = $this->db->get("glr.vjurnal_d_edit('".$no."')");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;


        // $nojurnal = $this->uri->segment(3);
        // $q = $this->db->query("select * from glr.vjurnal_d_edit('".$nojurnal."')");
        // //echo $this->db->last_query();
        //   foreach ($q->result_array() as $row){
        //       if($row['tipe']=='success'){
        //           $this->db->where("nojurnal",$nojurnal);
        //           $q2 = $this->db->get("glr.vm_jurnal");
        //           $res = $q2->result_array();
        //       }
        //   }
        // return $res;
    }
 

    public function json_dgview() {
        error_reporting(-1);

        if( isset($_GET['kddiv']) ){
            if($_GET['kddiv']){
                $kddiv = $_GET['kddiv'];
            }else{
                $kddiv = '';
            }
        }else{
            $kddiv = '';
        }

        if( isset($_GET['nojurnal']) ){
            if($_GET['nojurnal']){
                $nojurnal = $_GET['nojurnal'];
            }else{
                $nojurnal = '';
            }
        }else{
            $nojurnal = '';
        }
        $aColumns = array('kdakun', 'nmakun', 'debet', 'kredit', 'ket');
    	$sIndexColumn = "nourut";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT kdakun
                              , nmakun
                              , debet
                              , kredit
                              , ket
                              , nourut
                              FROM glr.v_jurnal_d where kddiv = '".$kddiv."' and nojurnal like '%".$nojurnal."%') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by nourut ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            } 

            // $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('jurnalbkl/edit/'.$aRow['nojurnal'])."\">Edit</a>";
            // $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['nojurnal']."');\">Hapus</button>";

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['nojurnal']) ){
            if($_GET['nojurnal']){
                $nojurnal = $_GET['nojurnal'];
            }else{
                $nojurnal = '';
            }
        }else{
            $nojurnal = '';
        }

        $aColumns = array('kdakun',
                          'nmakun',
                          'debet',
                          'kredit',
                          'nourut',
                          'kddiv',
                          'dk',
                          'ket',
                          'nominal');
	    $sIndexColumn = "kddiv";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //
        // if ($this->session->userdata("username")==='ADMINISTRATOR') {
            $sTable = " ( SELECT *  FROM glr.v_jurnal_d_tmp
                          WHERE nomor='".$nojurnal."') AS a";
        // } else {
        //     $sTable = " ( SELECT *  FROM glr.v_jurnal_d_tmp
        //                   WHERE userentry = '".$this->session->userdata("username")."' AND nomor='".$nojurnal."') AS a";
        // }

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            /*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['kddiv']==$value['kddiv']){
                    $aRow['detail'][]= $value;
                }
            }*/
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nourut']."','".$aRow['kddiv']."','".$aRow['kdakun']."'
            ,'".$aRow['nmakun']."','".$aRow['dk']."','".$aRow['nominal']."','".$aRow['ket']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }

    public function json_dgview_detail_edit() {
        error_reporting(-1);
        if( isset($_GET['nojurnal']) ){
            if($_GET['nojurnal']){
                $nojurnal = $_GET['nojurnal'];
            }else{
                $nojurnal = '';
            }
        }else{
            $nojurnal = '';
        }

        $aColumns = array('kdakun',
                          'kddiv',
                          'nmakun',
                          'debet',
                          'kredit',
                          'nourut',
                          'dk',
                          'nominal',
                          'ket',
                          'nomor');
      $sIndexColumn = "nomor";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //
        $sTable = " ( SELECT *  FROM glr.v_jurnal_d_tmp
                      WHERE nomor = '".$nojurnal."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
              $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        // $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }/*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['kddiv']==$value['kddiv']){
                    $aRow['detail'][]= $value;
                }
            }*/
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['nomor']."','".$aRow['nourut']."','".$aRow['kddiv']."',
            '".$aRow['kdakun']."','".$aRow['dk']."',".$aRow['nominal'].",'".$aRow['ket']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['nomor']."','".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function addDetail() {
        $nojurnal = $this->input->post('nojurnal');
        $kddiv = $this->input->post('kddiv');
        $kdakun = $this->input->post('kdakun');
        $tgljurnal = $this->apps->dateConvert($this->input->post('tgljurnal'));
        $dk = $this->input->post('dk');
        //$tglrf = $this->apps->dateConvert($this->input->post('tglrf'));
        $nominal = $this->input->post('nominal');
        $ket = $this->input->post('ket'); 
        //$nsel = $this->input->post('nsel');

        $q = $this->db->query("select nojurnal,title,msg,tipe from glr.jurnal_d_new_ins('" .$nojurnal."','" .$tgljurnal."' ,'" . $kddiv . "','" .$this->session->userdata("username")."' ,'" . $kdakun . "','" . $dk . "'," .  $nominal . ",'" . $ket . "')");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updateDetail() {
        $nojurnal = $this->input->post('nojurnal');
        $nourut = $this->input->post('nourut');
        $kddiv = $this->input->post('kddiv');
        $kdakun = $this->input->post('kdakun');
        $dk = $this->input->post('dk');
        $nominal = $this->input->post('nominal');
        $ket = $this->input->post('ket'); 

        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_new_upd('" . $nojurnal . "','" . $nourut . "' ,'" . $kddiv . "','" . $kdakun . "','" . $dk . "'," .  $nominal . ",'" . $ket . "')");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function addDetailEdit() {
        $nojurnal = $this->input->post('nojurnal');
        $kddiv = $this->input->post('kddiv');
        $kdakun = $this->input->post('kdakun');
        $dk = $this->input->post('dk');
        //$tglrf = $this->apps->dateConvert($this->input->post('tglrf'));
        $no = $this->input->post('nominal');
        $nominal = str_replace(',','',$no);
        $ket = $this->input->post('ket'); 
        //$nsel = $this->input->post('nsel');

        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_edit_ins('".$nojurnal."' ,'" . $kddiv . "','" . $kdakun . "','" . $dk . "'," .  $nominal . ",'" .  $ket . "')");

        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updateDetailEdit() {
        $nojurnal = $this->input->post('nojurnal');
        $nourut = $this->input->post('nourut');
        $kddiv = $this->input->post('kddiv');
        $kdakun = $this->input->post('kdakun');
        $dk = $this->input->post('dk');
        //$tglrf = $this->apps->dateConvert($this->input->post('tglrf'));
        $ket = $this->input->post('ket');
        $no = $this->input->post('nominal');
        $nominal = str_replace(',','',$no);
        //$nsel = $this->input->post('nsel');

        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_new_upd('".$nojurnal."' ," . $nourut . ",'" . $kddiv . "','" . $kdakun . "','" . $dk . "'," .  $nominal . ",'" .  $ket . "')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function detaildeleted() {
        $nojurnal = $this->input->post('nojurnal');
        $nourut = $this->input->post('nourut');
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_new_del('".$nojurnal."',". $nourut . ")");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function detaildeleted_edit() {
        $nojurnal = $this->input->post('nojurnal');
        $nourut = $this->input->post('nourut');
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_edit_del('". $nojurnal ."',".$nourut.")");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function delete() {
        $nojurnal = $this->input->post('nojurnal');
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_del('". $nojurnal ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function batal() {
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_new_batal('".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function batalEdit() {
        $nojurnal = $this->input->post('nojurnal');
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_d_new_batal('".$nojurnal."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function submit() {
        $nojurnal = $this->input->post('nojurnal');
        $tgljurnal = $this->apps->dateConvert($this->input->post('tgljurnal'));
        $noref = $this->input->post('noref');
        $trans = $this->input->post('trans');
        $kddiv = $this->input->post('kddiv');
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_ins_dash( '".$nojurnal."','". $tgljurnal ."','". $kddiv ."','". $noref ."','". $trans ."',false,'".$this->session->userdata("username")."' )");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function del() {
        $nojurnal = $this->input->post('nojurnal'); 
        $kddiv = $this->input->post('kddiv'); 
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_del_dash( '".$nojurnal."','".$this->session->userdata("username")."','".$kddiv."' )");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function submitedit() {
        $nojurnal = $this->input->post('nojurnal');
        $tgljurnal = $this->apps->dateConvert($this->input->post('tgljurnal'));
        $noref = $this->input->post('noref');
        $ket = $this->input->post('ket');
        $kddiv = $this->input->post('kddiv');
        $q = $this->db->query("select title,msg,tipe from glr.jurnal_upd_dash( '".$nojurnal."','". $tgljurnal ."','". $kddiv ."','". $noref ."','". $ket ."',false,'".$this->session->userdata("username")."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

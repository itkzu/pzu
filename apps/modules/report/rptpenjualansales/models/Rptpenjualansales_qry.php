<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptpenjualansales_qry
 *
 * @author adi
 */
class Rptpenjualansales_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }
    
    public function getPenjualanPerSales() {
        $periode_awal = $this->apps->dateConvert($this->input->post('periode_awal'));
        $periode_akhir = $this->apps->dateConvert($this->input->post('periode_akhir'));
        $header = $this->_header($periode_awal, $periode_akhir);
        $res = array(
            'header' => $header,
            'detail' => $this->_detail($periode_awal, $periode_akhir,$header),
            'footer' => $this->_footer($periode_awal, $periode_akhir,$header),
        );
        return json_encode($res);
    }
    
    
    
    public function getArrayPenjualanPerSales() {
        $periode_awal = $this->apps->dateConvert($this->input->post('periode_awal'));
        $periode_akhir = $this->apps->dateConvert($this->input->post('periode_akhir'));
        $header = $this->_header($periode_awal, $periode_akhir);
        $res = array(
            'header' => $header,
            'detail' => $this->_detail($periode_awal, $periode_akhir,$header),
            'footer' => $this->_footer($periode_awal, $periode_akhir,$header),
        );
        return $res;
    }
    
    private function _header($periode_awal, $periode_akhir){
        $str = "SELECT kode,kdtipe,nmtipe FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' )
                    GROUP BY kode,kdtipe,nmtipe
                    ORDER BY kode;";
        $qry = $this->db->query($str);
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }

    private function _detail($periode_awal, $periode_akhir, $header) {
        $column = "";
        foreach ($header as $value) {
            $column.=", SUM(CASE WHEN kode = '".$value['kode']."' AND tk = 'K' THEN jml ELSE 0 END) AS ".$value['kode']."_K";
            $column.=", SUM(CASE WHEN kode = '".$value['kode']."' AND tk = 'T' THEN jml ELSE 0 END) AS ".$value['kode']."_T";
        }
        
        $str = "SELECT nmsales ".$column." FROM 
                    (SELECT nmsales,kode,tk,count(nodo) AS jml
                        FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' )
                    GROUP BY nmsales,kode,tk) AS jual
                    WHERE (nmsales <> '') 
                    GROUP BY nmsales
                    ORDER BY nmsales;";
        $qry = $this->db->query($str);
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }
    
    private function _footer($periode_awal, $periode_akhir, $header) {
        $column = "";
        foreach ($header as $value) {
            $column.=", SUM(CASE WHEN kode = '".$value['kode']."' AND tk = 'K' THEN jml ELSE 0 END) AS ".$value['kode']."_K";
            $column.=", SUM(CASE WHEN kode = '".$value['kode']."' AND tk = 'T' THEN jml ELSE 0 END) AS ".$value['kode']."_T";
        }
        $resColumn = substr($column, 1,  strlen($column));
        $str = "SELECT ".$resColumn." FROM 
                    (SELECT nmsales,kode,tk,count(nodo) AS jml
                        FROM pzu.vl_jual
                    WHERE (to_char(tgldo,'YYYY-MM-DD') 
                        between '".$periode_awal."' 
                        AND '".$periode_akhir."' )
                    GROUP BY nmsales,kode,tk) AS jual
                    WHERE (nmsales <> '');";
        $qry = $this->db->query($str);
        //// echo $this->db->last_query();
        if($qry->num_rows()>0){
            return $qry->result_array();
        }else{
            return false;
        }
    }
}

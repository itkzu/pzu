<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashkacab_qry
 *
 * @author adi
 */
class Dashkacab_qry extends CI_Model {

	//put your code here
	protected $res = "";
	protected $delete = "";
	protected $state = "";

	public function __construct() {
		parent::__construct();
	}



	public function get_target_penjualan() {
		error_reporting(-1);

		if (isset($_GET['periode_awal'])) {
			if ($_GET['periode_awal']) {
                $dt_awal = explode("-", $_GET['periode_awal']);
                $periode_awal = $dt_awal[1].$dt_awal[0];

				//$periode_awal = $_GET['periode_awal'];
				//$periode_awal = $this->apps->dateConvert($_GET['periode_awal']);
			} else {
				$periode_awal = '';
			}
		} else {
			$periode_awal = '';
		}

		$aColumns = array(	'no',
							'nm',
							'montgt',
							'tot',
							'diff',
							'daycount',
							'daydiff',
							'avg',
						);
		$sIndexColumn = "nm";
		$sLimit = "";
		if (!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1') {
			$sLimit = " LIMIT " . $_GET['iDisplayLength'];
		}

		$sTable = " (
						SELECT row_number() OVER () AS no
							, nm
							, montgt
							, tot
							, diff
							, daycount
							, daytgt
							, daydiff
							, avg 
 						from pzu.pl_tgtvsact_bln('{$periode_awal}')

					) AS a";

/*
		$sTable = " (
						SELECT row_number() OVER () AS no
							, nm
							, daycount
							, daytgt
							, montgt
							, tot
							, diff
						FROM pzu.pl_tgtvsact('{$periode_awal}')
					) AS a";
*/

		if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
			if ($_GET['iDisplayStart'] > 0) {
				$sLimit = "LIMIT " . intval($_GET['iDisplayLength']) . " OFFSET " .
						intval($_GET['iDisplayStart']);
			}
		}

		$sOrder = "";
		if (isset($_GET['iSortCol_0'])) {
			$sOrder = " ORDER BY  ";
			for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
				if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
					$sOrder .= "" . $aColumns[intval($_GET['iSortCol_' . $i])] . " " .
							($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
				}
			}

			$sOrder = substr_replace($sOrder, "", -2);
			if ($sOrder == " ORDER BY") {
				$sOrder = "";
			}
		}
		$sWhere = "";

		if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
			$sWhere = " AND (";
			for ($i = 0; $i < count($aColumns); $i++) {
				$sWhere .= "lower(" . $aColumns[$i] . "::varchar) LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch'])) . "%' OR ";
			}
			$sWhere = substr_replace($sWhere, "", -3);
			$sWhere .= ')';
		}

		for ($i = 0; $i < count($aColumns); $i++) {

			if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
				if ($sWhere == "") {
					$sWhere = " WHERE ";
				} else {
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "lower(" . $aColumns[$i] . "::varchar)  LIKE '%" . strtolower($this->db->escape_str($_GET['sSearch_' . $i])) . "%' ";
			}
		}


		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";

//        echo $sQuery;

		$rResult = $this->db->query($sQuery);

		$sQuery = "
				SELECT COUNT(" . $sIndexColumn . ") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()

		$rResultFilterTotal = $this->db->query($sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(" . $sIndexColumn . ") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $this->db->query($sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$output = array(
			"sEcho" => intval($_GET['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$no = 1;
		foreach ($rResult->result_array() as $aRow) {
			$row = array();
			for ($i = 0; $i < count($aColumns); $i++) {
				if ($aRow[$aColumns[$i]] === null) {
					$aRow[$aColumns[$i]] = '-';
				}
				if (is_numeric($aRow[$aColumns[$i]])) {
					$row[] = (float) $aRow[$aColumns[$i]];
				} else {
					$row[] = $aRow[$aColumns[$i]];
				}
			}
			$output['aaData'][] = $row;
			$no++;
		}
		echo json_encode($output);
	}

	public function get_blm_trm_po_leasing() {
		try {
			$str = "select 	kdleasing
							, count(nodo) 
					from 	pzu.vm_trm_po_leasing 
					where 	tgltrmpo is null 
					group by kdleasing 
					order by kdleasing";
			$query = $this->db->query($str);
			if($query->num_rows()>0){
				$res = $query->result_array();
				return $res;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return 0;
		}
	}

	public function get_blm_trm_po_leasing_detail() {
		$kdleasing = $this->input->post('kdleasing');
		try {        
			$str = "select kdleasing
					, nmprogleas
					, pl
					, nodo
					, tgldo
					, nama_s
					, nmtipe
					, nmwarna 
					from pzu.vm_trm_po_leasing 
					where tgltrmpo is null 
						and kdleasing = '{$kdleasing}'
					order by nodo";
			$query = $this->db->query($str);
			if($query->num_rows()>0){
				$res = $query->result_array();
				return $res;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return 0;
		}
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Spknew
 *
 * @author adi
 */
class Spknew extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('spknew/submit'),
            'add' => site_url('spknew/add'),
            'edit' => site_url('spknew/edit'),
            'reload' => site_url('spknew'),
        );
        $this->load->model('spknew_qry');
        $cabang = $this->spknew_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }

        $this->data['tipebayar'] = array(
            "T" => "TUNAI",
            "K" => "KREDIT"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function set_spk() {
        echo $this->spknew_qry->set_spk();
    }

    public function set_spk2() {
        echo $this->spknew_qry->set_spk2();
    }

    public function carispk() {
        echo $this->spknew_qry->carispk();
    }

    public function addspk() {
        echo $this->spknew_qry->addspk();
    }

    public function getKdKota() {
        echo $this->spknew_qry->getkdkota();
    }

    public function updatespk() {
        echo $this->spknew_qry->updatespk();
    }

    public function getNoID() {
        echo $this->spknew_qry->getNoID();
    }

    public function set_apispk() {
        echo $this->spknew_qry->set_apispk();
    }

    public function getNama() {
        echo $this->spknew_qry->getNama();
    }

    public function deletespk() {
        echo $this->spknew_qry->deletespk();
    }

    public function getKodeSales() {
        echo $this->spknew_qry->getKodeSales();
    }

    public function getNoSPK() {
        echo $this->spknew_qry->getNoSPK();
    }

    public function getTipeUnit() {
        echo $this->spknew_qry->getTipeUnit();
    }

    public function getKodeSalesHeader() {
        echo $this->spknew_qry->getKodeSalesHeader();
    }

    public function getKdWarna() {
        echo $this->spknew_qry->getKdWarna();
    }

    public function getKdWarnaHeader() {
        echo $this->spknew_qry->getKdWarnaHeader();
    }

    public function getKodeTipeHeader() {
        echo $this->spknew_qry->getKodeTipeHeader();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglso'=> array(
                    'placeholder' => 'Tanggal SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
           'nospk'=> array(
                    'placeholder' => 'No. Assist',
                    'attr'        => array(
                       'id'    => 'nospk',
                       'class' => 'form-control',
                     ),
                     'data'     => '',
                     'name'        => 'nospk',
                     'value'       => set_value('nospk'),
            ),
      		   'finden'=> array(
                'placeholder' => 'Status Inden',
      					'id'          => 'finden',
      					// 'value'       => 't',
      					'checked'     => $faktif,
      					'class'       => 'custom-control-input',
      					'name'		  => 'finden',
      					'type'		  => 'checkbox',
      			),
            'nama1'=> array(
                     'placeholder' => 'Nama',
                     'id'          => 'nama1',
                     'name'        => 'nama1',
                     'value'       => set_value('nama1'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'alamat1'=> array(
                    'placeholder' => 'Alamat',
                    'value'       => set_value('alamat1'),
                    'id'          => 'alamat1',
                    'name'        => 'alamat1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kel1'=> array(
                    'placeholder' => 'Kel.',
                    'value'       => set_value('kel1'),
                    'id'          => 'kel1',
                    'name'        => 'kel1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kec1'=> array(
                    'placeholder' => 'Kec.',
                    'value'       => set_value('kec1'),
                    'id'          => 'kec1',
                    'name'        => 'kec1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'noktp'=> array(
                    'placeholder' => 'No KTP',
                    'value'       => set_value('noktp'),
                    'id'          => 'noktp',
                    'name'        => 'noktp',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
            ),
            'nohp1'=> array(
                    'placeholder' => 'No. HP',
                    'value'       => set_value('nohp1'),
                    'id'          => 'nohp1',
                    'name'        => 'nohp1',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'notelp'=> array(
                    'placeholder' => 'No. Telp',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('notelp'),
                    'id'          => 'notelp',
                    'name'        => 'notelp',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kota1'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kota1',
                        'class'       => 'form-control',
                    ), 
                    'data'     => '', 
                    'placeholder' => 'Kota',
                    'value'       => set_value('kota1'),
                    'name'        => 'kota1',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'npwp'=> array(
                    'placeholder' => 'NPWP',
                    'value'       => set_value('npwp'),
                    'id'          => 'npwp',
                    'name'        => 'npwp',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'type'        => 'hidden',
                    // 'readonly'    => '',
            ),
            'nama2'=> array(
                     'placeholder' => 'Nama',
                     'id'          => 'nama2',
                     'name'        => 'nama2',
                     'value'       => set_value('nama2'),
                     'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'alamat2'=> array(
                    'placeholder' => 'Alamat KTP/KIMS',
                    'value'       => set_value('alamat2'),
                    'id'          => 'alamat2',
                    'name'        => 'alamat2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'alamat3'=> array(
                    'placeholder' => 'Alamat T. Tinggal',
                    'value'       => set_value('alamat3'),
                    'id'          => 'alamat3',
                    'name'        => 'alamat3',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kel2'=> array(
                    'placeholder' => 'Kel.',
                    'value'       => set_value('kel2'),
                    'id'          => 'kel2',
                    'name'        => 'kel2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kec2'=> array(
                    'placeholder' => 'Kec.',
                    'value'       => set_value('kec2'),
                    'id'          => 'kec2',
                    'name'        => 'kec2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'nohp2'=> array(
                    'placeholder' => 'No. HP',
                    'value'       => set_value('nohp2'),
                    'id'          => 'nohp2',
                    'name'        => 'nohp2',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kota2'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kota2',
                        'class'       => 'form-control',
                    ), 
                    'data'     => '', 
                    'placeholder' => 'Kota',
                    'value'       => set_value('kota2'),
                    'name'        => 'kota2',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
            'kdtipe'=> array(
                    'attr'        => array(
                        'id'    => 'kdtipe',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'placeholder' => 'Tipe Unit',
                    'value'       => set_value('kdtipe'),
                    'name'        => 'kdtipe',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'type'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'kdwarna'=> array(
                    'attr'        => array(
                        'id'    => 'kdwarna',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'placeholder' => 'Kode Warna',
                    'value'       => set_value('kdwarna'),
                    'name'        => 'kdwarna',
                    'style'       => '',
            ),
            'nmwarna'=> array(
                    'placeholder' => 'Nama Warna',
                    // 'type'        => 'hidden',
                    'value'       => set_value('nmwarna'),
                    'id'          => 'nmwarna',
                    'name'        => 'nmwarna',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'tipebayar'=> array(
                    'placeholder' => 'Pembayaran',
                    'attr'        => array(
                        'id'    => 'tipebayar',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['tipebayar'],
                    'class' => 'form-control',
                    'value'    => set_value('tipebayar'),
                    'name'     => 'tipebayar',
                    // 'readonly' => '',
            ),
            'kdsales'=> array(
                    'attr'        => array(
                        'id'    => 'kdsales',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('kdsales'),
                    'name'     => 'kdsales',
                    'required'    => '',
            ),
            'nmsales'=> array(
                    'placeholder' => 'Salesman',
                    'type'        => 'hidden',
                    'value'       => set_value('nmsales'),
                    'id'          => 'nmsales',
                    'name'        => 'nmsales',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'nmspv'=> array(
                    'placeholder' => 'Superviser',
                    // 'type'        => 'hidden',
                    'value'       => set_value('nmspv'),
                    'id'          => 'nmspv',
                    'name'        => 'nmspv',
                    'class'       => 'form-control',
                    'style'       => '',
                    // 'readonly'    => '',
            ),
            'ket'=> array(
                    // 'placeholder' => 'Kec.',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control',
                    'style'       => '',
                    'type'        => 'hidden',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(
          // 'nodf'=> array(
          //          'placeholder' => 'No. DF',
          //          //'type'        => 'hidden',
          //          'id'          => 'nodf',
          //          'name'        => 'nodf',
          //          'value'       => $this->val[0]['nodf'],
          //          'class'       => 'form-control',
          //          'style'       => '',
          //          'readonly'    => '',
          //  ),

              'noso'=> array(
                       'placeholder' => 'No. SPK',
                       //'type'        => 'hidden',
                       'id'          => 'noso',
                       'name'        => 'noso',
                       'value'       => set_value('noso'),
                       'class'       => 'form-control',
                       'style'       => '',
               ),
              'tglso'=> array(
                       'placeholder' => 'Tanggal SPK',
                       'id'          => 'tglso',
                       'name'        => 'tglso',
                       'value'       => date('d-m-Y'),
                       'class'       => 'form-control calendar',
                       'style'       => '',
               ),
              'nospk'=> array(
                        'attr'        => array(
                            'id'    => 'nospk',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                       'placeholder' => 'No. SPK HSO',
                       'name'        => 'nospk',
                       'value'       => set_value('nospk'),
                       'style'       => '',
               ),
               'nama1'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama1',
                        'name'        => 'nama1',
                        'value'       => set_value('nama1'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat1'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat1'),
                       'id'          => 'alamat1',
                       'name'        => 'alamat1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel1'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel1'),
                       'id'          => 'kel1',
                       'name'        => 'kel1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'noktp'=> array(
                       'placeholder' => 'No KTP',
                       'value'       => set_value('noktp'),
                       'id'          => 'noktp',
                       'name'        => 'noktp',
                       'class'       => 'form-control',
                       // 'type'        => 'hidden',
                       'style'       => '',
               ),
               'kec1'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec1'),
                       'id'          => 'kec1',
                       'name'        => 'kec1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'hohp1'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp1'),
                       'id'          => 'hohp1',
                       'name'        => 'hohp1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'notelp'=> array(
                       'placeholder' => 'No. Telp',
                       'type'        =>  'hidden',
                       'value'       => set_value('nsel'),
                       'id'          => 'nsel',
                       'name'        => 'nsel',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota1'=> array(
                       'placeholder' => 'Kota',
                       'value'       => set_value('kota1'),
                       'id'          => 'kota1',
                       'name'        => 'kota1',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'npwp'=> array(
                       'placeholder' => 'Npwp',
                       'value'       => set_value('npwp'),
                       'id'          => 'npwp',
                       'name'        => 'npwp',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'nama2'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama2',
                        'name'        => 'nama2',
                        'value'       => set_value('nama2'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat2'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat2'),
                       'id'          => 'alamat2',
                       'name'        => 'alamat2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel2'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel2'),
                       'id'          => 'kel2',
                       'name'        => 'kel2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kec2'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec2'),
                       'id'          => 'kec2',
                       'name'        => 'kec2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'ket'=> array(
                       // 'placeholder' => 'Kec.',
                       'value'       => set_value('ket'),
                       'id'          => 'ket',
                       'name'        => 'ket',
                       'class'       => 'form-control',
                       'style'       => '',
                       'type'        => 'hidden',
               ),
               'hohp2'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp2'),
                       'id'          => 'hohp2',
                       'name'        => 'hohp2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota2'=> array(
                       'placeholder' => 'Kota',
                       'value'       => set_value('kota2'),
                       'id'          => 'kota2',
                       'name'        => 'kota2',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdtipe'=> array(
                       'attr'        => array(
                           'id'    => 'kdtipe',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Tipe Unit',
                       'value'       => set_value('kdtipe'),
                       'name'        => 'kdtipe',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'nmtipe'=> array(
                       'placeholder' => 'Nama Tipe',
                       'type'        => 'nmtipe',
                       'value'       => set_value('nmtipe'),
                       'id'          => 'nmtipe',
                       'name'        => 'nmtipe',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'kdwarna'=> array(
                       'attr'        => array(
                           'id'    => 'kdwarna',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Kode Warna',
                       'value'       => set_value('kdwarna'),
                       'name'        => 'kdwarna',
                       'style'       => '',
               ),
               'nmwarna'=> array(
                       'placeholder' => 'Nama Warna',
                       // 'type'        => 'hidden',
                       'value'       => set_value('nmwarna'),
                       'id'          => 'nmwarna',
                       'name'        => 'nmwarna',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'tipebayar'=> array(
                       'placeholder' => 'Pembayaran',
                       'attr'        => array(
                           'id'    => 'tipebayar',
                           'class' => 'form-control select',
                       ),
                       'data'     => $this->data['tipebayar'],
                       'value'    => set_value('tipebayar'),
                       'name'     => 'tipebayar',
                       'required'    => '',
               ),
               'kdsales'=> array(
                       'placeholder' => 'Salesman',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdsales'),
                       'id'          => 'kdsales',
                       'name'        => 'kdsales',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdspv'=> array(
                       'placeholder' => 'Superviser',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdspv'),
                       'id'          => 'kdspv',
                       'name'        => 'kdspv',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->spknew_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

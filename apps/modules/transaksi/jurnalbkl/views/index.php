<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>

            <div class="box-header">
                <a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
                <a class="btn btn-warning btn-ubah">Ubah</a> 
                <a class="btn btn-danger btn-del">Hapus</a> 
                 
                
                <div class="box-tools pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?php echo $add;?>" >Tambah Data Baru</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn-refersh">Refresh</a>
                            </li>
                        </ul>

                    </div> 
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    
                </div>
                

            </div>
            <div class="box-body">
                    <div class="row">


                        <div class="col-md-12 col-lg-12">
                            <div class="row"> 

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=form_label($form['kddiv']['placeholder']);?>
                                        <div class="input-group">
                                            <?php
                                                echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                                                echo form_error('kddiv', '<div class="note">', '</div>');
                                            ?> 
                                        </div>
                                    </div>
                                </div> 

                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12">
                            <div class="row"> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['nojurnal']['placeholder']);
                                            echo form_input($form['nojurnal']);
                                            echo form_error('nojurnal','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tgljurnal']['placeholder']);
                                            echo form_input($form['tgljurnal']);
                                            echo form_error('tgljurnal','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['noref']['placeholder']);
                                            echo form_input($form['noref']);
                                            echo form_error('noref','<div class="note">','</div>');
                                        ?>
                                     </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['trans']['placeholder']);
                                            echo form_input($form['trans']);
                                            echo form_error('trans','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div> 

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">Kode Akun</th>
                                            <th style="text-align: center;">Nama Akun</th>
                                            <th style="width: 150px;text-align: center;">Debet</th>
                                            <th style="width: 150px;text-align: center;">Kredit</th>
                                            <th style="width: 150px;text-align: center;">Keterangan</th> 
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th> 
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#nojurnal').val("");  
        $('#noref').val(""); 
        $('#trans').val(""); 
        ubh();
        $(".btn-del").click(function(){
            del();
        }); 
        $('#nojurnal').mask("9999-9999"); 
        $('#kddiv').change(function(){ 
            $('#nojurnal').val(""); 
            $('#nojurnal').mask("9999-9999"); 
        }); 
        // $('#nojurnal').change(function(){ 
        //     setnojurnal();
        // }); 
        $('#nojurnal').keyup(function(){ 
            // setnojurnal();
            var nojurnal = $('#nojurnal').val();  
            checkLength(nojurnal);
        }); 
        $('#tgljurnal').prop("disabled",true);
        $('#noref').prop("disabled",true);
        $('#trans').prop("disabled",true); 

    });

    function edit(){
      var nojurnal = $("#nojurnal").val();
      var kddiv = $("#kddiv").val();
      // var no = kddiv+nojurnal;
      // alert(no);
      window.location.href = 'jurnalbkl/edit/'+kddiv+'-'+nojurnal;
      // window.open('jurnalbkl/edit/'+kddiv+'-'+nojurnal); 
    } 
    
    function tbl(nojurnal){

        var column = [];
        var kddiv = $('#kddiv').val();

        /*
        for (i = 1 i <= 1; i++) {
            column.push({
                "aType":"numeric-comma",
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    // return formmatedvalue;
                    }
                });
        }
        */

        // column.push({ "aDataSort": [ 1,2 ], "aTargets": [ 1 ] });

        column.push({
            "aTargets": [ 2,3 ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    // return formmatedvalue;
            },
            "sClass": "center"
        }); 

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [ 
                { "data": "kdakun" },
                { "data": "nmakun" },
                { "data": "debet" },
                { "data": "kredit" },
                { "data": "ket" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "kddiv", "value": kddiv },
                                { "name": "nojurnal", "value": nojurnal }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('jurnalbkl/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{extend: 'copy'},
                //{extend: 'csv'},
                //{extend: 'excel'}, 
                /*
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
                */
            ],
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }); 

    }

    function ubh(){ 
        if( $('#nojurnal').val()===''){
            $(".btn-ubah").attr('disabled','disabled');
            $("#noref").val("");
            $("#trans").val("");
            var nojurnal = 'EMPTY';
            tbl(nojurnal);
        } else {
            $(".btn-ubah").attr('disabled', false);
            $(".btn-ubah").click(function(){
                edit();
            }); 
        }
    }

    function checkLength(nojurnal) {
        var jml = nojurnal.replaceAll("_",""); 
        if (jml.length === 9) {
            setnojurnal();
        } else {
            setclear();
        }
    }

    function setnojurnal(){
        var nojurnal = $('#nojurnal').val();
        var kddiv = $('#kddiv').val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("jurnalbkl/setnojurnal");?>",
                data: {"nojurnal":nojurnal,"kddiv":kddiv },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        $("#tgljurnal").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgljurnal))); 
                        $('#noref').val(data.noref);
                        $('#trans').val(data.ket);
                        var nojurnal = $('#nojurnal').val();
                        tbl(nojurnal);
                        ubh();
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
    }
    

    function setclear(){  
        $('#noref').val('');
        $('#trans').val('');
        var nojurnal = 'EMPTY';
        tbl(nojurnal);
    }

    function del(){
        swal({
            title: "Konfirmasi Hapus Transaksi!",
            text: "Data yang akan disimpan akan hilang !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nojurnal = $("#nojurnal").val(); 
            var kddiv = $("#kddiv").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("jurnalbkl/del");?>",
                data: {"nojurnal":nojurnal,
                        "kddiv":kddiv},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('jurnalbkl');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>
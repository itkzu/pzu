<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
</style>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->
                <div class="box-header">
                    <button type="button" class="btn btn-primary btn-submit">
                       Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                       Batal
                    </button>
                </div>
                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nosin']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <?php
                                                echo form_input($form['nosin']);
                                                echo form_error('nosin','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-2 ">
                                            <?php echo form_label($form['tgltrmfa']['placeholder']); ?>
                                        </div>

                                        <div class="col-md-3 ">
                                            <?php
                                                echo form_input($form['tgltrmfa']);
                                                echo form_error('tgltrmfa','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 


                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nama']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nama']);
                                                        echo form_error('nama','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nofaktur']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['nofaktur']);
                                                        echo form_error('nofaktur','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>      

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['alamat']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['alamat']);
                                                        echo form_error('alamat','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tglfaktur']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['tglfaktur']);
                                                        echo form_error('tglfaktur','<div class="note">','</div>');
                                                    ?>
                                                </div> 

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_label("<small><i>Tanggal Terbit Faktur</i></small>");
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>   

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-1">  
                                                </div>
                                                <div class="col-md-1"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kel']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_input($form['kel']);
                                                        echo form_error('kel','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                                <div class="col-md-1"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kec']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_input($form['kec']);
                                                        echo form_error('kec','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['harga']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['harga']);
                                                        echo form_error('harga','<div class="note">','</div>');
                                                    ?>
                                                </div> 

                                                <div class="col-md-4"> 
                                                    <?php
                                                        echo form_label("<small><i>Harga yg tertera di Faktur</i></small>");
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>  

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-1">  
                                                </div>
                                                <div class="col-md-1"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kota']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-4"> 
                                                    <?php 
                                                        echo form_dropdown($form['kota']['name'],
                                                                           $form['kota']['data'],
                                                                           $form['kota']['value'],
                                                                           $form['kota']['attr']);
                                                        echo form_error('kota', '<div class="note">', '</div>');  
                                                    ?>
                                                </div>  
                                            </div>
                                        </div>  
                                    </div>
                                </div>   

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['noktp']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['noktp']);
                                                        echo form_error('noktp','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nodo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['nodo']);
                                                        echo form_error('nodo','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 
                                        <br>
                                    </div>
                                </div>     

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['kdtipe']);
                                                        echo form_error('kdtipe','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tgldo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-5"> 
                                                    <?php
                                                        echo form_input($form['tgldo']);
                                                        echo form_error('tgldo','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['tahun']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-3"> 
                                                    <?php
                                                        echo form_input($form['tahun']);
                                                        echo form_error('tahun','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['cc']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-3"> 
                                                    <?php
                                                        echo form_input($form['cc']);
                                                        echo form_error('cc','<div class="note">','</div>');
                                                    ?>
                                                </div> 

                                                <div class="col-md-1"> 
                                                    <?php
                                                        echo form_label('CC');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>   
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['warna']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['warna']);
                                                        echo form_error('warna','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['notpt']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['notpt']);
                                                        echo form_error('notpt','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nora2']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nora2']);
                                                        echo form_error('nora2','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nosut']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nosut']);
                                                        echo form_error('nosut','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="row"> 

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nosin2']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-9"> 
                                                    <?php
                                                        echo form_input($form['nosin2']);
                                                        echo form_error('nosin2','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>  
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>  

<script type="text/javascript">
    $(document).ready(function () {  
        autoNum();
        $('#nofaktur').mask('FH/AC/999999/*')

        $('.btn-set').click(function(){
            $('.btn-set').hide();
            $('.btn-add').attr('disabled',false);
            $('#stathrg').attr('disabled',true);
            if ($("#stathrg").prop("checked")){
              $('#harga').prop('readonly',false);
            } else {
              $('#harga').prop('readonly',true);
            }

        });


        $('.btn-add').click(function(){

        	  getKodetrmfa();
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kdaks').select2({
                placeholder: '-- Pilih Barang --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            clear();
        });

        $('.btn-simpan').click(function(){
            addDetail(); 
        }); 

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){ 
              submit(); 
        });
    });

    function autoNum(){
        $('#harga').autoNumeric('init');
        $("#cc").autoNumeric('init');
    }

    function reset(){
        $('#sub_tot').val('');
        $('#potongan').val('');
        $('#disc_top').val('');
        $('#dpp').val('');
        $('#ppn').val('');
        $('#total').val('');
        $('#kode').val('');
        // $('#kdtipe').val('');
        $('#nmtipe').val(''); 
        $('#qty').val(''); 
        $('#harga').val(''); 
        $('#nourut').val(''); 
    }


    function dateback(tgl) {
        var less = $("#jth_tmp").val();
        var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
        var day = new_date.format('DD');
        var month = new_date.format('MM');
        var year = new_date.format('YYYY');
        var res = day + '-' + month + '-' + year;
        return res;
    }

    function sum(){
        var sub_tot = $('#sub_tot').autoNumeric('get');
        var potongan = $('#potongan').autoNumeric('get');
        var disc_top = $('#disc_top').autoNumeric('get');
        if(sub_tot===''){
            $('#sub_tot').autoNumeric('set',0);
            var sub_tot = 0;
        }
        if(potongan===''){
            $('#potongan').autoNumeric('set',0);
            var potongan = 0;
        }
        if(disc_top===''){
            $('#disc_top').autoNumeric('set',0);
            var disc_top = 0;
        }

        var dpp = parseFloat(sub_tot) - parseFloat(disc_top) - parseFloat(potongan);
        if(!isNaN(dpp)){
            $('#dpp').autoNumeric('set',dpp);
        } else {
            $('#dpp').autoNumeric('set',0);
            var dpp = 0;
        }

        var ppn = parseFloat(dpp) / 10;
        if(!isNaN(ppn)){
            $('#ppn').autoNumeric('set',ppn);
        } else {
            $('#ppn').autoNumeric('set',0);
            var ppn = 0;
        }

        var total = parseFloat(dpp) + parseFloat(ppn);
        if(!isNaN(total)){
            $('#total').autoNumeric('set',total);
        } else {
            $('#total').autoNumeric('set',0);
            var total = 0;
        }

        var terbilang = penyebut(total);
        $('#banner').val(terbilang);
    }

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 

    function clear(){
        $("#nourut").val('');
    } 

    function edit(nopo,kode,kdtipe,nmtipe,qty,harga,nourut){

        $('#modal_transaksi').modal('toggle');
        $("#nourut").val(nourut);

        $("#kode").val(kode);
        $("#kdtipe").val(kdtipe); 
        $("#nmtipe").val(nmtipe); 
        $("#qty").val(qty);  
        $("#harga").autoNumeric('set',harga);
//         $("#harga").autoNumeric('set',harga);

//         $('#modal_transaksi').modal('toggle');
    }


    function addDetail(){

        var po = $("#nopo").val();
        var nopo = po.replace("-","");
        var nopo = nopo.replace("-","");
        var nopo = nopo.toUpperCase();  

        var nourut = $("#nourut").val();
        var harga = $("#harga").autoNumeric('get'); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmfa/addDetail");?>",
            data: {"nopo":nopo
                    ,"nourut":nourut 
                    ,"harga":harga },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                var obj = jQuery.parseJSON(resp);
                if(obj.state==='1'){
                    swal({
                        title: 'Data Berhasil Disimpan',
                        text: obj.msg,
                        type: 'success'
                    }, function(){
                        refresh();
                    }); 
                }else{ 
                    swal({
                        title: 'Data Gagal Disimpan',
                        text: obj.msg,
                        type: 'error'
                    }); 
                } 
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(notrmfa,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("trmfa/detaildeleted");?>",
                data: {"notrmfa":notrmfa ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){    
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            window.location.href = '<?=site_url('trmfa');?>';
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () { 
            var no = $("#nosin").val();
            var nosin = no.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.toUpperCase();  

            var nodo = $("#nodo").val(); 
            var nodo = nodo.toUpperCase(); 

            var tgltfa = $("#tgltrmfa").val();   
            var nama = $("#nama").val();   
            var alamat = $("#alamat").val();   
            var kel = $("#kel").val();   
            var kec = $("#kec").val();   
            var kota = $("#kota").val();   
            var noktp = $("#noktp").val();   
            var kdtipe = $("#kdtipe").val();   
            var cc = $("#cc").autoNumeric('get');      
            var nofaktur = $("#nofaktur").val();   
            var tglfaktur = $("#tglfaktur").val();   
            var harga = $("#harga").autoNumeric('get'); 
            var notpt = $("#notpt").val();   
            var nosut = $("#nosut").val();   
            $.ajax({
                type: "POST",
                url: "<?=site_url("trmfa/submit");?>",
                data: {"nodo":nodo
                    ,"tgltfa":tgltfa
                    ,"nama":nama
                    ,"alamat":alamat
                    ,"kel":kel
                    ,"kec":kec
                    ,"kota":kota
                    ,"noktp":noktp
                    ,"kdtipe":kdtipe
                    ,"cc":cc
                    ,"nofaktur":nofaktur
                    ,"tglfaktur":tglfaktur
                    ,"harga":harga
                    ,"notpt":notpt
                    ,"nosut":nosut },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        if(data.tfa_upd=1){
                            swal({
                                title: 'Proses Berhasil',
                                text: '',
                                type: 'success'
                            }, function() {
                                window.location.href = '<?=site_url('trmfa');?>';
                            }); 
                        } else {
                            swal({
                                title: 'Proses Gagal',
                                text: '',
                                type: 'error'
                            });
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

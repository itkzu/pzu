<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
    		outline: none;
    		border: none;
    }

    .radio {
    		margin-top: 0px;
    		margin-bottom: 0px;
    }

    .checkbox label, .radio label {
    		min-height: 20px;
    		padding-left: 20px;
    		margin-bottom: 5px;
    		font-weight: bold;
    		cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <!-- form start -->
        <!-- <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes);
        ?> -->
        <div class="box box-danger">

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-1">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()" > Ubah</button> 
                                    <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="box-header box-view">
                                    <a class="btn btn-info btn-um" target="_blank">
                                        <i class="fa fa-print"></i> Kw. UM
                                    </a>
                                    <a class="btn btn-info btn-pl" target="_blank">
                                        <i class="fa fa-print"></i> Kw. Pelunasan
                                    </a>
                                    <a class="btn btn-info btn-jp" target="_blank">
                                        <i class="fa fa-print"></i> Kw. JP
                                    </a>
                                    <a class="btn btn-info btn-sp" target="_blank">
                                        <i class="fa fa-print"></i> Surat Pernyataan
                                    </a>
                                </div>
                            </div>  
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

            		<div class="col-xs-12">
            			<ul class="nav nav-tabs" id="myTab" role="tablist">
            				<li class="nav-item active" role="presentation">
            					<a class="nav-link " id="list-do" name="list-do" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Daftar DO yang Belum terima PO Leasing</a>
            				</li>
            				<li class="nav-item" role="presentation">
            					<a class="nav-link" id="input-do" name="input-do" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Input Terima PO Leasing</a>
            				</li>
            			</ul>
            			<div class="tab-content" id="myTabContent">
            				<div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-do">

            					<div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable display nowrap" style="width:3500px">
                                            <thead>
                                                <tr>
                                                    <th style="width: 1%;text-align: center;">No.</th>
                                                    <th style="width: 3%;text-align: center;">No. DO</th>
                                                    <th style="width: 3%;text-align: center;">Tgl. DO</th>
                                                    <th style="width: 3%;text-align: center;">Nama Pemesan</th>
                                                    <th style="width: 3%;text-align: center;">Nama STNK</th>
                                                    <th style="width: 8%;text-align: center;">Alamat STNK</th>
                                                    <th style="width: 2%;text-align: center;">Leasing</th>
                                                    <th style="width: 3%;text-align: center;">Program</th>
                                                    <th style="width: 2%;text-align: center;">Nilai Pelunasan</th>
                                                    <th style="width: 8%;text-align: center;">Nilai JP</th>
                                                    <th style="width: 8%;text-align: center;">Matriks</th>
                                                    <th style="width: 2%;text-align: center;">Kode</th>
                                                    <th style="width: 5%;text-align: center;">Tipe</th>
                                                    <th style="width: 5%;text-align: center;">Jenis</th>
                                                    <th style="width: 8%;text-align: center;">Warna</th>
                                                    <th style="width: 8%;text-align: center;">No. Mesin</th>
                                                    <th style="width: 8%;text-align: center;">No. Rangka</th>
                                                    <th style="width: 8%;text-align: center;">Sales</th>
                                                    <th style="width: 1%;text-align: center;">Edit</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 2%;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
            					</div>
            				</div>

            				<div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-do">

            					<div class="col-xs-12">
            						<hr>
            					</div>

            					<div class="col-xs-12">
            						<div class="row">

            							<div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nodo']['placeholder']); ?>
                                            </div>
            							</div>

            							<div class="col-xs-2">
            								<?php
            									echo form_input($form['nodo']);
            									echo form_error('nodo','<div class="note">','</div>');
            								?>
            							</div>

                                        <div class="col-xs-1"> 
                                        </div>

                                        <div class="col-xs-2 ">
                                            <?php echo form_label($form['noso']['placeholder']); ?>
                                        </div>

            							<div class="col-xs-2 ">
            								<?php
            									echo form_input($form['noso']);
            									echo form_error('noso','<div class="note">','</div>');
            								?>
            							</div> 

            						</div>
        						</div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tgldo']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['tgldo']);
                                                echo form_error('tgldo','<div class="note">','</div>');
                                            ?>
                                        </div>  

                                        <div class="col-xs-1"> 
                                        </div>


                                        <div class="col-xs-2">
                                            <?php echo form_label($form['tglso']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['tglso']);
                                                echo form_error('tglso','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">
                                        <br>
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nopoleasing']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['nopoleasing']);
                                                echo form_error('nopoleasing','<div class="note">','</div>');
                                            ?>
                                        </div>   


                                        <div class="col-xs-2">
                                            <?php echo form_label($form['nama_p']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['nama_p']);
                                                echo form_error('nama_p','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tglterbitpo']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['tglterbitpo']);
                                                echo form_error('tglterbitpo','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-1"> 
                                        </div>   


                                        <div class="col-xs-2">
                                            <?php echo form_label($form['ket_ar_leas']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['ket_ar_leas']);
                                                echo form_error('ket_ar_leas','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tgltrmpo']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['tgltrmpo']);
                                                echo form_error('tgltrmpo','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['tgltagihleas']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['tgltagihleas']);
                                                echo form_error('tgltagihleas','<div class="note">','</div>');
                                            ?>
                                        </div>

                                        <div class="col-xs-1"> 
                                        </div>   


                                        <div class="col-xs-2">
                                            <?php echo form_label($form['nama']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['nama']);
                                                echo form_error('nama','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5"> 
                                        </div>   


                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nama_s']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['nama_s']);
                                                echo form_error('nama_s','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nmleasing']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['nmleasing']);
                                                echo form_error('nmleasing','<div class="note">','</div>');
                                            ?>
                                        </div>  

                                        <div class="col-xs-2">
                                            <?php echo form_label($form['alamat']['placeholder']); ?>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['alamat']);
                                                echo form_error('alamat','<div class="note">','</div>');
                                            ?>
                                        </div>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['nmprogleas']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                           <?php
                                                echo form_input($form['nmprogleas']);
                                                echo form_error('nmprogleas','<div class="note">','</div>');
                                            ?>
                                        </div>   
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row"> 
                                        <br>  
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['pl']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['pl']);
                                                echo form_error('pl','<div class="note">','</div>');
                                            ?>
                                        </div>   
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['jp']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['jp']);
                                                echo form_error('jp','<div class="note">','</div>');
                                            ?>
                                        </div>   
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['matriks']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['matriks']);
                                                echo form_error('matriks','<div class="note">','</div>');
                                            ?>
                                        </div>   
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <?php echo form_label($form['total']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                           <?php
                                                echo form_input($form['total']);
                                                echo form_error('total','<div class="note">','</div>');
                                            ?>
                                        </div>   
                                    </div>
                                </div> 
            				</div>
            			</div>
            		</div>
                </div>
            </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //input_lr

        $('.btn-ubah').hide();  
        $('#btn-refresh').show();
        $('.btn-um').hide();
        $('.btn-pl').hide();
        $('.btn-jp').hide();
        $('.btn-sp').hide();
        clear(); 
        autonum(); 

        //manggil kode do
        GetNoID();

        //daftar do yang blm input
        var column = [];

        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "orderCellsTop": true,
            "fixedHeader": true,
            // "scrollY": 500,
            // "scrollX": true,
            // "aoColumnDefs": column,
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mData" : "nodo",
                    "mRender": function (data, type, row) {
                    var btn = '<a href="trmpoleas/edit/' + data +'">' + data + ' </a>';
                    return btn;
                }
            }],
            "columns": [ 
                { "data": "no"},
                { "data": "nodo"},
                { "data": "tgldo" }, 
                { "data": "nama" },
                { "data": "nama_s" },
                { "data": "alamat" },
                { "data": "kdleasing" },
                { "data": "nmprogleas" },
                { "data": "pl" },
                { "data": "jp" },
                { "data": "matriks" },
                { "data": "kode" },
                { "data": "kdtipe" },
                { "data": "nmtipe" },
                { "data": "nmwarna" },
                { "data": "nosin" },
                { "data": "nora" },
                { "data": "nmsales" },
                { "data": "edit"},
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('trmpoleas/json_dgview');?>",
      			"oLanguage": {
      				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
      			},
      			//dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                    .search( this.value )
                    .draw();
                //}
            });
        });

      	//row number
      	table.on( 'draw.dt', function () {
      	var PageInfo = $('.dataTable').DataTable().page.info();
      			table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
      					cell.innerHTML = i + 1 + PageInfo.start;
      			});
      	});

        //fungsi tombol
        $('.btn-refresh').click(function(){
            table.ajax.reload();
        });

        $('.btn-cetak').click(function(){ 
            var nodo = $('#nodo').val();
            window.open('trmpoleas/ctk_lr/'+nodo, '_blank'); 
        });

        $('#input-do').click(function(){
            clear();
            $('.btn-ubah').show();  
            $('.btn-um').hide();
            $('.btn-pl').hide();
            $('.btn-jp').hide();
            $('.btn-sp').hide();
            $('.btn-refresh').hide();
        });

        $('#list-do').click(function(){
            $('.btn-ubah').hide(); 
            $('.btn-um').hide();
            $('.btn-pl').hide();
            $('.btn-jp').hide();
            $('.btn-sp').hide(); 
            $('.btn-refresh').show();
        });

        $('.btn-um').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            // window.location.href = 'trmpoleas/ctk_um/'+nodo;
            window.open('trmpoleas/ctk_um/'+nodo, '_blank');
            // ctk_um();
        }); 

        $('.btn-pl').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            // window.location.href = 'trmpoleas/ctk_pl/'+nodo;
            window.open('trmpoleas/ctk_pl/'+nodo, '_blank');
            // ctk_um();
        }); 

        $('.btn-jp').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            // window.location.href = 'trmpoleas/ctk_jp/'+nodo;
            window.open('trmpoleas/ctk_jp/'+nodo, '_blank');
            // ctk_um();
        }); 

        $('.btn-sp').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            // window.location.href = 'trmpoleas/ctk_sp/'+nodo;
            window.open('trmpoleas/ctk_sp/'+nodo, '_blank');
            // ctk_um();
        }); 

        //fungsi input
        $('#nodo').change(function(){
            set_nodo();
        });
      });

      //fungsi sistem

      // no id
    function GetNoID(){
        var tg = new Date();
        var tgl = tg.toString();
        var tanggal = tgl.substring(13,15);
          //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmpoleas/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#nodo').mask(value.kode+"99-999999");
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function ctk_lr(){
        var nodo = $("#nodo").val();
        var nodo = nodo.toUpperCase();
          $.ajax({
              type: "POST",
              url: "<?=site_url("trmpoleas/ctk_labarugi");?>",
              data: {"nodo":nodo },
              success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                  });
              }
          });
    }


    function ubah(){
        var nodo = $('#nodo').val();
        window.location.href = 'trmpoleas/edit/'+nodo;
    }

     //memanggil data do

    function set_nodo(){
        var nodo = $("#nodo").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmpoleas/set_nodo");?>",
            data: {"nodo":nodo},
            success: function(resp){
                if(resp==='"empty"'){
                    swal({
                        title: "Data Tidak Ada",
                        text: "Data DO Tidak Ditemukan",
                        type: "warning"
                    })
                    clear();  
                }else{
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        // baris 1 
                        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                        $("#noso").val(data.noso);
                        $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                        $("#nopoleasing").val(data.nopoleasing);
                        $("#nama_p").val(data.nama_p);
                        $("#ket_ar_leas").val(data.ket_ar_leas);
                        $("#nama").val(data.nama);
                        $("#nama_s").val(data.nama_s);
                        $("#nmleasing").val(data.nmleasing);
                        $("#alamat").val(data.alamat);
                        $("#nmleasing").val(data.nmleasing);
                        $("#nmprogleas").val(data.nmprogleas);

                        $("#pl").autoNumeric('set',data.pl);
                        $("#jp").autoNumeric('set',data.jp);   
                        $("#matriks").autoNumeric('set',data.matriks);  
                        sum(); 
                        $('.btn-ubah').show();  
                        $('.btn-ubah').attr("disabled",false); 
                        if(data.tgltrmpo===null){
                            $("#tgltrmpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
                            $("#tgltagihleas").val($.datepicker.formatDate('dd-mm-yy', new Date()));
                            $("#tglterbitpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
                            $('.btn-um').hide();
                            $('.btn-pl').hide();
                            $('.btn-jp').hide();
                            $('.btn-sp').hide(); 
                        } else {
                            $("#tgltrmpo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgltrmpo)));
                            $("#tgltagihleas").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgltagihleas)));
                            $("#tglterbitpo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglterbitpo)));
                            $('.btn-um').show();
                            $('.btn-pl').show();
                            $('.btn-jp').show();
                            $('.btn-sp').show(); 
                        }
                    });
                } 
             },
             error:function(event, textStatus, errorThrown) {
               swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
             }
         });

     }

    //fungsi tampilan
    function clear(){
      //baris 1
        $('#ket').val('');
        $('#nodo').val('');         
        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#noso').val('');     
        $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#nopoleasing').val('');     
        $('#nama_p').val('');      
        $("#tglterbitpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tgltrmpo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $("#tgltagihleas").val($.datepicker.formatDate('dd-mm-yy', new Date()));   
        $('#ket_ar_leas').val(''); 
        $('#nama').val('');     
        $('#nama_s').val('');     
        $('#alamat').val('');    
        $('#nmleasing').val('');    
        $('#nmprogleas').val('');      
        $('#pl').val('');      
        $('#jp').val('');      
        $('#matriks').val('');      
        $('#total').val('');      
    }

    function autonum(){

      //baris 2
        $('#pl').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#jp').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#matriks').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'});
        $('#total').autoNumeric('init',{ currencySymbol : 'Rp.' , mDec: '0'}); 
    } 

    function sum(){

      //baris 2
        var pl      = $('#pl').autoNumeric('get');
        var jp      = $('#jp').autoNumeric('get');
        var matriks = $('#matriks').autoNumeric('get');

        var total = parseInt(pl) + parseInt(jp) + parseInt(matriks);
        if(!isNaN(total)){
            $('#total').autoNumeric('set', total);
        } 
    } 
</script>

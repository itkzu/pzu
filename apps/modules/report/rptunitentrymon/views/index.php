<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
                        , 'class' => "form-inline");
					echo form_open($submit,$attributes);
				?>
				<div class="form-group">
					<?php 
						echo form_label($form['tahun']['placeholder']);
                        echo form_dropdown($form['tahun']['name'],
                        					$form['tahun']['data'],
                        					$form['tahun']['value'],
                        					$form['tahun']['attr']);
						echo form_error('tahun','<div class="note">','</div>');
					?>
				</div>

				<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				
				<?php echo form_close(); ?>
				<hr>
				<div class="table-responsive">
					<table class="table table-bordered table-hover js-basic-example dataTable">

						<thead>
							<tr>
								<th rowspan="2" style="text-align: center;" id="periode">Periode</th>
								<th rowspan="2" style="text-align: center;">Jumlah Hari</th>
								<th colspan="5" style="text-align: center;">TOTAL</th>
								<th colspan="5" style="text-align: center;">REGULER</th>
								<th colspan="5" style="text-align: center;">KPB</th>
							</tr>

							<tr>
								<th style="width: 100px;text-align: center;">Qty</th>
								<th style="width: 100px;text-align: center;">Total</th>
								<th style="width: 100px;text-align: center;">Jasa</th>
								<th style="width: 100px;text-align: center;">Oli</th>
								<th style="width: 100px;text-align: center;">Part</th>
								<th style="width: 100px;text-align: center;">Qty</th>
								<th style="width: 100px;text-align: center;">Total</th>
								<th style="width: 100px;text-align: center;">Jasa</th>
								<th style="width: 100px;text-align: center;">Oli</th>
								<th style="width: 100px;text-align: center;">Part</th>
								<th style="width: 100px;text-align: center;">Qty</th>
								<th style="width: 100px;text-align: center;">Total</th>
								<th style="width: 100px;text-align: center;">Jasa</th>
								<th style="width: 100px;text-align: center;">Oli</th>
								<th style="width: 100px;text-align: center;">Part</th>
							</tr>
						</thead>

						<tbody></tbody>

						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
								<th style="text-align: right;"></th>
							</tr>
						</tfoot>

					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		$(".btn-tampil").click(function(){
			//table.ajax.clear();
			table.ajax.reload();
		});

		var column = [];

		column.push({ 
				"aTargets": [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ],
				"mRender": function (data, type, full) {
						return type === 'export' ? data : numeral(data).format('0,0');
				},
				"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"bPaginate": false,
			"bInfo": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "tahun", "value": $("#tahun").val() }
							);
				$.ajax( {
					"dataType": 'json', 
					"type": "GET", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				$('#periode').text('Periode '+$("#tahun").val());
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},

			'footerCallback': function ( row, data, start, end, display ) {

	            var api = this.api(), data;
	 
	            // converting to interger to find total
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };

	            function setTotal(col) {
	            	return api
	                .column( col )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	            }

				var tot_1 = numeral(setTotal(1)).format('0,0');
				var tot_2 = numeral(setTotal(2)).format('0,0');
				var tot_3 = numeral(setTotal(3)).format('0,0');
				var tot_4 = numeral(setTotal(4)).format('0,0');
				var tot_5 = numeral(setTotal(5)).format('0,0');
				var tot_6 = numeral(setTotal(6)).format('0,0');
				var tot_7 = numeral(setTotal(7)).format('0,0');
				var tot_8 = numeral(setTotal(8)).format('0,0');
				var tot_9 = numeral(setTotal(9)).format('0,0');
				var tot_10 = numeral(setTotal(10)).format('0,0');
				var tot_11 = numeral(setTotal(11)).format('0,0');
				var tot_12 = numeral(setTotal(12)).format('0,0');
				var tot_13 = numeral(setTotal(13)).format('0,0');
				var tot_14 = numeral(setTotal(14)).format('0,0');
				var tot_15 = numeral(setTotal(15)).format('0,0');
				var tot_16 = numeral(setTotal(16)).format('0,0');
	 

	            // computing column Total of the complete result 
	            /*
	            var tot_1 = api
	                .column( 1 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
				*/
					

				
				//alert(monTotal);
	            // Update footer by showing the total with the reference of the column index 

	            
				$( api.column( 0 ).footer() ).html('Total');
				$( api.column( 1 ).footer() ).html(tot_1);
				$( api.column( 2 ).footer() ).html(tot_2);
				$( api.column( 3 ).footer() ).html(tot_3);
				$( api.column( 4 ).footer() ).html(tot_4);
				$( api.column( 5 ).footer() ).html(tot_5);
				$( api.column( 6 ).footer() ).html(tot_6);
				$( api.column( 7 ).footer() ).html(tot_7);
				$( api.column( 8 ).footer() ).html(tot_8);
				$( api.column( 9 ).footer() ).html(tot_9);
				$( api.column( 10 ).footer() ).html(tot_10);
				$( api.column( 11 ).footer() ).html(tot_11);
				$( api.column( 12 ).footer() ).html(tot_12);
				$( api.column( 13 ).footer() ).html(tot_13);
				$( api.column( 14 ).footer() ).html(tot_14);
				$( api.column( 15 ).footer() ).html(tot_15);
				$( api.column( 16 ).footer() ).html(tot_16);

			},

			"sAjaxSource": "<?=site_url('rptunitentrymon/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//"dom": 'Bfrtip',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}, footer: true},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}, footer: true},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3', 
					footer: true
				},
				{extend: 'print', footer: true,
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					}
				}
			],
			//"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});
		


		
		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		/*
		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" ){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
		*/
	});
</script>
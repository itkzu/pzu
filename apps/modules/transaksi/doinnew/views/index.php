<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">
          <div class="box-header box-view">
              <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a> 
            <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
          </div> 

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['nodoin']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nodoin']);
                                    echo form_error('nodoin','<div class="note">','</div>');
                                ?>
                            </div>
                        </div> 
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['tgldoin']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['tgldoin']);
                                    echo form_error('tgldoin','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>  
                      </div>
                  </div>

                  <div class="col-xs-12">
                      <br>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['nmcabang']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['nmcabang']['name'],$form['nmcabang']['data'] ,$form['nmcabang']['value'] ,$form['nmcabang']['attr']);
                                    echo form_error('nmcabang','<div class="note">','</div>');
                                  // echo form_label($form['kdtipe']['placeholder']);
                                  // echo form_input($form['kdtipe']);
                                  // echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-2">
                            <label>
                              <?php echo form_label($form['nosin']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nosin']);
                                      echo form_error('nosin','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div> 

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nora']);
                                      echo form_error('nora','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['nopo']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nopo']);
                                      echo form_error('nopo','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['kdtipe']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['kdtipe']);
                                  echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['nmtipe']);
                                    echo form_error('nmtipe','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['tglpo']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['tglpo']);
                                  echo form_error('tglpo','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['warna']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['warna']);
                                  echo form_error('warna','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                        <div class="col-xs-1">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['tahun']);
                                  echo form_error('tahun','<div class="note">','</div>');
                              ?>
                            </div>
                        </div> 

                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['hrgjual']['placeholder']); ?>
                          </label>
                        </div> 

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['hrgjual']);
                                  echo form_error('hrgjual','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row"> 
                        <br>
                      </div>
                  </div> 

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['ket']['placeholder']); ?>
                          </label>
                        </div> 

                        <div class="col-xs-4">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['ket']);
                                  echo form_error('ket','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div> 


              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#hrgjual').val('');
        $('#hrgjual').autoNumeric('init');
        clear();
        GetNoID_idx(); 
        disabled();
        
        $('#nodoin').change(function(){
            setnodoin();
        });
    });

    // no id
    function GetNoID_idx(){ 
        //alert(kddiv); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("doinnew/getNoID_idx");?>",
            data: {},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#nodoin').mask(value.kddoi+"-9999-999"); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    // get no do
    function setnodoin(){
        var nodoin = $('#nodoin').val(); 
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("doinnew/setnodoin");?>",
            data: {"nodoin":nodoin},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#tgldoin').val($.datepicker.formatDate('dd-mm-yy', new Date(value.tgldoin)));
                    $('#nosin').val(value.nosin);
                    $('#nora').val(value.nora);
                    $('#nmcabang').val(value.kdsup);
                    $('#nopo').val(value.nopo);
                    $('#kdtipe').val(value.kdtipe);
                    $('#nmtipe').val(value.nmtipe);
                    $('#tglpo').val($.datepicker.formatDate('dd-mm-yy', new Date(value.tglpo)));
                    $('#warna').val(value.warna);
                    $('#tahun').val(value.tahun);
                    $('#hrgjual').autoNumeric('set',value.nilai);
                    $('#ket').val(value.ket);
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    // disabled
    function disabled(){ 
        $('#tgldoin').attr('disabled',true);
        $('#tglpo').attr('disabled',true);
        $('#nmcabang').prop('disabled',true);
        $('#nosin').prop('disabled',true);
        $('#ket').prop('disabled',true);
    }  

    // clear
    function clear(){  
        // var date = Date.now(); 
        $('#nodoin').val('');
        $('#tgldoin').val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#tglpo').val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#nmcabang').val('');
        $('#nosin').val('');
        $('#nora').val('');
        $('#nopo').val('');
        $('#kdtipe').val('');
        $('#nmtipe').val('');
        $('#warna').val('');
        $('#tahun').val('');
        $('#hrgjual').autoNumeric('set',0);
        $('#ket').val('');
    }  
</script>

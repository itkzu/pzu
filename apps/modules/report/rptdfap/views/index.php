<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes);
                ?>
                <div class="form-group">
                    <?php
                        echo form_label($form['periode_akhir']['placeholder']);
                        echo form_input($form['periode_akhir']);
                        echo form_error('periode_akhir','<div class="note">','</div>');
                    ?>
                </div>
                <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                <table style=""  class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;width: 80px;">No. Faktur</th>
                            <th style="text-align: center;width: 80px;">Tgl Faktur</th>
                            <th style="text-align: center;">Cabang</th>
                            <th style="text-align: center;width: 100px;">Saldo Awal</th>
                            <th style="text-align: center;width: 100px;">Pokok Hutang</th>
                            <th style="text-align: center;width: 100px;">Pembayaran</th>
                            <th style="text-align: center;width: 100px;">Saldo Akhir</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: right;"></th>
                            <th style="text-align: right;"></th>
                            <th style="text-align: right;"></th>
                        </tr>
                    </tfoot>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#periode_akhir').datepicker({
                    startView: "year",
                    minViewMode: "months",
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: false,
                    autoclose: true,
                    format: "yyyy-mm"
                });

        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });

        if ( $.fn.DataTable.isDataTable('.data-table') ) {
          $('.data-table').DataTable().destroy();
          $(".body-data").html('');
        }


        var column = [];

        column.push({
            "aTargets": [ 3,4,5,6 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
                // return formmatedvalue;
              },
              "sClass": "right"
        });

        column.push({
            "aTargets": [ 1 ],
            "mRender": function (data, type, full) {
              return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu":  [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                            { "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(0)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptdfap/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                  // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var so_awal = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var nap = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var nbyr = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                      }, 0 );

          	     var so_akhir = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
          	    $( api.column( 2 ).footer() ).html('Total');
                $( api.column( 3 ).footer() ).html(numeral(so_awal).format('0,0.00'));
                $( api.column( 4 ).footer() ).html(numeral(nap).format('0,0.00'));
                $( api.column( 5 ).footer() ).html(numeral(nbyr).format('0,0.00'));
                $( api.column( 6 ).footer() ).html(numeral(so_akhir).format('0,0.00'));
            },

            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy', footer: true,
                    exportOptions: {orthogonal: 'export' }},
                {extend: 'csv', footer: true,
                    exportOptions: {orthogonal: 'export' }},
                {extend: 'excel', footer: true,
                    exportOptions: {orthogonal: 'export' }},
                {extend: 'pdf', footer: true,
                    orientation: 'landscape',
                    pageSize: 'A0'
                },
                {extend: 'print', footer: true,
                    customize: function (win){
                       $(win.document.body).addClass('white-bg');
                       $(win.document.body).css('font-size', '10px');
                       $(win.document.body).find('table')
                           .addClass('compact')
                           .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {

        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptkbbkl
 *
 * @author adi
 */
class Rptkbbkl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptkbbkl/submit'),
            'add' => site_url('rptkbbkl/add'),
            'edit' => site_url('rptkbbkl/edit'),
            'reload' => site_url('rptkbbkl'),
        );
        $this->load->model('rptkbbkl_qry');

        $kasbank = $this->rptkbbkl_qry->refKasBank();
        foreach ($kasbank as $value) {
            $this->data['kasbank'][$value['kdkb']] = $value['nmkb'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->rptkbbkl_qry->json_dgview();
    }

    public function tglkb() {
        echo $this->rptkbbkl_qry->tglkb();
    }

    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptkbbkl_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kdkb'=> array(
                    'attr'        => array(
                        'id'    => 'kdkb',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kasbank'],
                    'value'    => '',
                    'name'     => 'kdkb',
                    'autofocus'   => ''
            ),
           'tglkb'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => set_value('tglkb'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'tglkb'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => set_value('tglkb'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
                    'readonly'    => '',
            ),
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }

    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_awal',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'periode_akhir',
                    'label' => 'Periode Akhir',
                    'rules' => 'required|max_length[20]',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashadh
 *
 * @author adi
 */ 
class Dashadh extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('dashadh/submit'),
            'add' => site_url('dashadh/add'),
            'edit' => site_url('dashadh/edit'),
            'reload' => site_url('dashadh'),
        );
        $this->load->model('dashadh_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }

    public function getTrnHarian() {
        echo $this->dashadh_qry->adh_json_trn_harian();
    }

    public function getTrnKasBank() {
        echo $this->dashadh_qry->adh_json_trn_kas_bank();
    }

    public function getTrnProgBBN() {
        echo $this->dashadh_qry->adh_json_trn_prog_bbn();
    }

    public function detail_prog_bbn() {
        echo $this->dashadh_qry->detail_prog_bbn();
    }

    public function getProgBBNDetail() {
        echo $this->dashadh_qry->getProgBBNDetail();
    }

    public function getProgBBNDetail_leas() {
        echo $this->dashadh_qry->getProgBBNDetail_leas();
    }


    public function get_blm_trm_po_leasing() {
        $res = $this->dashadh_qry->get_blm_trm_po_leasing();
        echo json_encode($res);
    }

    public function get_blm_trm_po_leasing_detail() {
        $data = $this->dashadh_qry->get_blm_trm_po_leasing_detail();
        $res = array();
        $no = 0;
        foreach ($data as $dt) {
            foreach ($dt as $k => $val) {
                if(is_numeric($val)){
                    $res[$no][$k] = (float) $val;
                }else{
                    $res[$no][$k] = $val;
                }
            }
            $no++;
        }
        echo json_encode($res);
    }
}


      <?php

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      }

      function baris($jml) {
        $jml = abs($jml);
        $hasil = "";
        if($jml==0){
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==1) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==2) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==3) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } elseif ($jml==4) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==5) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==6) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==7) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr>
                    <tr><td style='height:23px;' colspan='9'>&nbsp;<br></td></tr>";
        } else if ($jml==8) {
          $hasil = "<tr><td style='height:23px;' colspan='9'>&nbsp;</td></tr></tr>";
        }
        return $hasil;
      }

      ?>
          <table border="0" style="width:690px">
            <thead>
              <tr>
                <td style="width:80%" colspan="5"></td>
                <td style="width:5%"></td>
                <td style="width:5%"></td>
                <td style="width:5%;text-align:left;"><?php echo $data[0]['nocetak'];?></td>
                  <td style="width:5%"></td>
              </tr><tr>
                <td style="width:80%" colspan="5"></td>
                <td style="width:5%"></td>
                <td style="width:5%"></td>
                <td style="width:15%;text-align:left;" colspan="2"><?php echo $data[0]['tglkb'];?></td>
              </tr><tr>
                <td style="width:80%" colspan="8"></td>
                <td style="width:20%"><br><br><br><br><br></td>
              </tr><tr>
                <td style="width:80%" colspan="9"></td>
              </tr>
              <?php
              $no = 1;

              foreach ($data as $value) {

                  $kode = $data[0]['kode'];
                  $hasil = "";
                  if($kode=='KM-BCA'){
                    $hasil = '<tr>
                                <td style="text-align: left; width: 1%; height:23px;">'.$no.'</td>
                                <td style="text-align: left; width: 26%; height:23px;">'.$value["darike"].'</td>
                                <td style="text-align: left; width: 26%; height:23px;"></td>
                                <td style="text-align: left; width: 26%; height:23px;" colspan="2">'.$value["ket_det"].'</td>
                                <td style="text-align: right; width: 15%; height:23px;" colspan="3">'.number_format($value["nilai_det"],2,",",".").'</td>
                                <td style="text-align: left; width: 5%; height:23px;"></td>
                              </tr>';
                  } else if($kode=='KM-PMT') {
                    $hasil = '<tr>
                    <td style="text-align: left; width: 1%; height:23px;">'.$no.'</td>
                    <td style="text-align: left; width: 26%; height:23px;">'.$value["darike"].'</td>
                                <td style="text-align: left; width: 26%; height:23px;"></td>
                                <td style="text-align: left; width: 26%; height:23px;" colspan="2">'.$value["ket_det"].'</td>
                                <td style="text-align: right; width: 15%; height:23px;" colspan="3">'.number_format($value["nilai_det"],2,",",".").'</td>
                                <td style="text-align: left; width: 5%; height:23px;"></td>
                              </tr>';
                  } else if($kode=='KK-BCA') {
                    $hasil = '<tr>
                    <td style="text-align: left; width: 1%; height:23px;">'.$no.'</td>
                    <td style="text-align: left; width: 26%; height:23px;">'.$value["darike"].'</td>
                                <td style="text-align: left; width: 28%; height:23px;"></td>
                                <td style="text-align: left; width: 24%; height:23px;" colspan="2">'.$value["ket_det"].'</td>
                                <td style="text-align: right; width: 15%; height:23px;" colspan="3">'.number_format($value["nilai_det"],2,",",".").'</td>
                                <td style="text-align: left; width: 5%; height:23px;"></td>
                              </tr>';
                  } else if($kode=='KK-PMT') {
                    $hasil = '<tr>
                    <td style="text-align: left; width: 1%; height:23px;">'.$no.'</td>
                    <td style="text-align: left; width: 26%; height:23px;">'.$value["darike"].'</td>
                                <td style="text-align: left; width: 28%; height:23px;"></td>
                                <td style="text-align: left; width: 24%; height:23px;" colspan="2">'.$value["ket_det"].'</td>
                                <td style="text-align: right; width: 15%; height:23px;" colspan="3">'.number_format($value["nilai_det"],2,",",".").'</td>
                                <td style="text-align: left; width: 5%; height:23px;"></td>
                              </tr>';
                  } else {
                    $hasil = '<tr>
                    <td style="text-align: left; height:23px;">'.$no.'</td>
                    <td style="text-align: left; width: 40%; height:23px;">'.$value["darike"].'</td>
                                <td style="text-align: left; width: 40%; height:23px;" colspan="3">'.$value["ket_det"].'</td>
                                <td style="text-align: right; width: 15%; height:23px;" colspan="3">'.number_format($value["nilai_det"],2,",",".").'</td>
                                <td style="text-align: left; width: 5%; height:23px;"></td>
                              </tr>';
                  }
                echo $hasil;
                $no++;
                $data2 = $value['nourut'];
                } ?>
            </thead>
            <?php echo baris($data2); ?> 

            <tbody>
              <tr>
                  <td colspan="2"></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <th style="text-align: right; width: 30%;" colspan="3"><?php echo number_format($data[0]['nilai'],2,",","."); ?></th>
                  <td></td>
              </tr>
                <tr>
                    <td colspan="9"></td>
                </tr>
              <tr>
                  <th colspan="9" style="text-align: center;"><?php echo penyebut($data[0]['nilai']); ?> Rupiah</th>
              </tr>
          </tbody>
            <tfoot>
            </tfoot>
          </table>
      </div>

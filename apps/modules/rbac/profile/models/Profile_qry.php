<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Profile_qry
 *
 * @author adi
 */
class Profile_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }

    public function select_data() { 
        $id = $this->session->userdata('username');
        $this->db->where('lower("user".kduser)', strtolower($id));
        $this->db->join('mst."divisi"','user.kddiv = divisi.kddiv');
        $query = $this->db->get('pzu."user"');
        return $query->result_array(); 
    }

    public function submit() {
        try {
            $array = $this->input->post();
            $data = array(
                    'nmuser'=> $array['nmuser'],
                    'kduser'=> $array['kduser'],
                );
            if(isset($array['password2'])&& !empty($array['password2'])){
                $this->db->set('pwd',$array['password2']);
            }
            $this->db->where('kduser', $array['id']);
            $resl = $this->db->update('pzu."user"', $data);
            $newdata = array(
                'username'  => strtoupper($array['kduser']),
                'nmuser'  => ucwords(strtolower($array['nmuser'])),
            );
            $this->session->set_userdata($newdata);
            if( ! $resl){
                $err = $this->db->error();
                $this->res = " Error : ". $this->apps->err_code($err['message']);
                $this->state = "0";
            }else{
                $this->res = "Data Terupdate";
                $this->state = "1";
            }
            
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        
        return $arr;
    }
}
<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Impumsl_qry
 *
 * @author adi
 */
class Impumsl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }
    
    public function submit() {
        try {
            $this->load->library('CSVReader');
            if( isset( $_FILES['csvfile'] ) == TRUE && !empty($_FILES['csvfile']['name'][0])){
                //$files = array_filter($_FILES['upload']['name']); something like that to be used before processing files.
                // Count # of uploaded files in array
                $total = count($_FILES['csvfile']['name']);

                // Loop through each file
                $rows = array();
                $sess_umsl = array();
                $sess_umsl_detail = array();
                $res = false;
                $this->db->trans_begin();
                for($i=0; $i<$total; $i++) {
                  //Get the temp file path
                  $tmpFilePath = $_FILES['csvfile']['tmp_name'][$i];
                  //Make sure we have a filepath
                  if ($tmpFilePath != ""){
                    //Setup our new file path
                    $newFilePath = './files/hso/' . $_FILES['csvfile']['name'][$i];
                    //Upload the file into the temp dir
                    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                        //Handle other code here
                        $filename = $_FILES['csvfile']['name'][$i];
                        $csvData = $this->csvreader->parse_file($newFilePath,false); //path to csv file
                        $path_parts = pathinfo($newFilePath);
                        if(strpos($filename, "(")===false && $path_parts['extension']==="UMSL"){
                            // Jika dalam file ada tanda kurung maka dianggap file ganda dan tidak akan di proses
                            if($csvData){
                                // Pharse untuk table header
                                $rows = $this->pharseHeader($filename, $csvData);

                                // Pharse untuk table detail
                                $rows_detail = $this->pharseDetail($csvData);
                                if($rows){
                                    // Mengecek apakah nodo sudah ada di table t_umdo
                                    $query = $this->db->get_where('pzu.t_umdo', array('noumdo' => $rows['noumdo']));
                                    if($query->num_rows()==0){
                                        // Jika tidak ada data di table pzu.t_umdo
                                        $res = false;
                                        $sess_umsl = null;
                                        $sess_umsl_detail = null;
                                        $this->db->trans_rollback();
                                        unlink($newFilePath);
                                        throw new Exception(" Error : ". "Data Nomor DO [" . $rows['noumdo']."] tidak dapat ditemukan di database."
                                                . " <br>Kemungkinan anda belum mengimport UMDO pada tanggal " . $rows['tglumsl'] . " atau sebelum tanggal " .$rows['tglumsl']
                                                . " , transaksi dibatalkan."
                                                . " <br>Reff File : " . $filename);
                                    }                             

                                    // Jika ada data insert table header
                                    $res = $this->db->insert('pzu.t_umsl',$rows);
                                    if($res==FALSE){
                                        // Jika gagal transaksi di roolback
                                        $this->db->trans_rollback();
                                        $err = $this->db->error();
                                        unlink($newFilePath);
                                        throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                                    }  

                                    if($rows_detail['data']){
                                        // Jika detail ada data dan tidak ada masalah di master pzu.unit
                                        foreach ($rows_detail['data'] as $value) {
                                            // Insert ke tabel detail
                                            $res_d = $this->db->insert('pzu.t_umsl_d',$value);
                                            if($res_d==FALSE){
                                                // Jika gagal transaksi di roolback
                                                $sess_umsl_detail = null;
                                                $this->db->trans_rollback();
                                                $err = $this->db->error();
                                                unlink($newFilePath);
                                                throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                                            }  
                                            // Mengisi data yg berhasil di insert kedalam array untuk ditampilkan
                                            $sess_umsl_detail[] = $value;
                                        }    
                                    }else{
                                        // Jika detail tidak ada data di master pzu.unit
                                        $res = false;
                                        $sess_umsl = null;
                                        $sess_umsl_detail = null;
                                        $this->db->trans_rollback();
                                        unlink($newFilePath);
                                        throw new Exception(" Error : ". "Kode unit [".$rows_detail['kode']."] tidak dapat ditemukan di database."
                                                . " <br>Kemungkinan kode tersebut adalah kode baru."
                                                . " , transaksi dibatalkan."
                                                . " <br>Reff File : " . $filename);
                                    }
                                    $sess_umsl[] = $rows;
                                }
                                else{
                                    // Jika data sudah pernah di update kedalam database
                                    $res = false;
                                    $sess_umsl = null;
                                    $sess_umsl_detail = null;
                                    $this->db->trans_rollback();
                                    unlink($newFilePath);
                                    throw new Exception(" Error : ". "Data file CSV Sudah ada didatabase"
                                            . " , transaksi dibatalkan."
                                            . " <br>Reff File : " . $filename);
                                }
                            }
                            else{
                                unlink($newFilePath);
                                throw new Exception(" Error : Isi File CSV Kosong, Silahkan Cek Kembali");
                            }    
                        }
                        else{
                            unlink($newFilePath);
                            throw new Exception("Error : Tidak ada data yang di simpan, karena file salah atau gagal di simpan ke database"
                                    . " <br>Reff File : Wrong File!");
                        }
                        unlink($newFilePath);
                    }
                  }
                }
                if($res!==FALSE){
                    $this->db->trans_commit();
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                    $this->session->set_userdata('tbumsl', $sess_umsl);
                    $this->session->set_userdata('tbumsl_d', $sess_umsl_detail);
                }
            }else{
                $this->res = " Error : ". "Data file CSV masih kosong"
                                . " , transaksi dibatalkan."
                                . " <br>Reff File : Empty File!";
                $this->state = "0";
            }
        }catch (Exception $e) {            
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_userdata('statsubmit', json_encode($arr));
        return $arr;
    }
    
    private function pharseHeader($filename, $array) {
        $data = array();
        foreach ($array as $value) {
            $data['noumsl'] = $value[0];
            $data['tglumsl'] = $this->getDate($value[1]);
            $data['noumdo'] = $value[13];
            $data['nmfile'] = $filename;
        }
        $query = $this->db->get_where('t_umsl', array('noumsl' => $data['noumsl']));
        if($query->num_rows()>0){
            return false;
        }else{
            return $data;   
        }         
    }
    
    private function pharseDetail($array) {
        $data = array();
        $nourut = 1;
        $kode = "";
        foreach ($array as $value) {
            $kode = $value[4];
            $query = $this->db->get_where('pzu.tipe', array('kode' => $value[4]));
            if($query->num_rows()>0){
                $data[] = array(
                    'noumsl' => $value[0], 
                    'nourut' => $nourut, 
                    'nosin' => $value[7],  
                    'kode' => $value[4], 
                    'nora' => "MH1".$value[6],  // Ditambahi kode rangka MH1
                    'kdwarna' => $value[5], 
                    'tahun' => $value[8], 
                );
                $nourut++;    
            }else{
                $res = array(
                    'kode' => $value[4],
                    'data' => "",
                );   
                return $res;
            }
        }
        $res = array(
            'kode' => "",
            'data' => $data,
        );        
        return $res;
    }
    
    private function getDate($ddmmyyyy = null){
        if(!$ddmmyyyy){
            $ddmmyyyy = date("dmY");
        }
        $dd = substr($ddmmyyyy,0,2);
        $mm = substr($ddmmyyyy,2,2);
        $yyyy = substr($ddmmyyyy,4,4);
        $date = $yyyy."-".$mm."-".$dd;
        return $date;
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Verbbn
 *
 * @author adi
 */
class Verbbn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('verbbn/submit'),
            'add' => site_url('verbbn/add'),
            'edit' => site_url('verbbn/edit'),
            'ctk'      => site_url('verbbn/ctk'),
            'reload' => site_url('verbbn'),
        );
        $this->load->model('verbbn_qry');
        $kota = $this->verbbn_qry->getkota();
        foreach ($kota as $value) {
            $this->data['kota'][$value['kota']] = $value['kota'];
        } 
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk() {
        $nokps = $this->uri->segment(3);   
        $this->data['data'] = $this->verbbn_qry->ctk($nokps);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function getNoID() {
        echo $this->verbbn_qry->getNoID();
    }

    public function set_notbj() {
        echo $this->verbbn_qry->set_notbj();
    }

    public function set_nosin() {
        echo $this->verbbn_qry->set_nosin();
    }

    public function submit() {
        echo $this->verbbn_qry->submit();
    }

    public function update() {
        echo $this->verbbn_qry->update();
    }

    public function batal() {
        echo $this->verbbn_qry->batal();
    }

    public function delDetail() {
        echo $this->verbbn_qry->delDetail();
    }

    public function json_dgview() {
        echo $this->verbbn_qry->json_dgview();
    }

    public function json_dgview_det() {
        echo $this->verbbn_qry->json_dgview_det();
    }

    private function _init_add(){ 
        $this->data['form'] = array(
           'notbj'=> array(
                    'placeholder' => 'No. Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'notbj',
                    'name'        => 'notbj',
                    'value'       => set_value('notbj'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgltbj'=> array(
                    'placeholder' => 'Tgl Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'tgltbj',
                    'name'        => 'tgltbj',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'nodo'=> array(
                    'placeholder' => 'No. DO',
                    //'type'        => 'hidden',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'tgldo'=> array(
                    'placeholder' => 'Tgl DO',
                    //'type'        => 'hidden',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
           'nama'=> array(
                    'placeholder' => 'Nama ',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    //'type'        => 'hidden',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'kota'=> array(
                    'placeholder' => 'Alamat Faktur',
                    //'type'        => 'hidden',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'pajak_bbn'=> array(
                    'placeholder' => 'Tarif Pajak',
                    //'type'        => 'hidden',
                    'id'          => 'pajak_bbn',
                    'name'        => 'pajak_bbn',
                    'value'       => set_value('pajak_bbn'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
                    'readonly'    => '',
            ),
           'tarif_bbn'=> array(
                    'placeholder' => 'By. Proses',
                    //'type'        => 'hidden',
                    'id'          => 'tarif_bbn',
                    'name'        => 'tarif_bbn',
                    'value'       => set_value('tarif_bbn'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
            ),
           'jasa'=> array(
                    'placeholder' => 'By. Jasa',
                    //'type'        => 'hidden',
                    'id'          => 'jasa',
                    'name'        => 'jasa',
                    'value'       => set_value('jasa'),
                    'class'       => 'form-control',
                    'style'       => 'text-align: right;',
            ),
        );
    }
     private function _init_edit($no = null){
        if(!$no){
            $nokb = $this->uri->segment(3);
        }
        $this->_check_id($nokb);
        $this->data['form'] = array(
           'notbj'=> array(
                    'placeholder' => 'No. Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'notbj',
                    'name'        => 'notbj',
                    'value'       => $this->val[0]['notbj'], 
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgltbj'=> array(
                    'placeholder' => 'Tgl Terima Tagihan',
                    //'type'        => 'hidden',
                    'id'          => 'tgltbj',
                    'name'        => 'tgltbj',
                    'value'       => $this->val[0]['tgltbj'],
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'target' => '_blank'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_akhir']['placeholder']);
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                    <button type="submit" class="btn btn-default" name="submit" >
                        <i class="fa fa-print"></i> Cetak
                    </button>
                    <button type="submit" class="btn btn-success" name="submit" value="excel">
                        <i class="fa fa-file-excel-o"></i> Excel
                    </button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                    <table style="width:2000px" class="table table-bordered table-hover data-table">
                        <thead>
                            <tr>
                                <th style="text-align: center;width: 50px;">No</th>
                                <th style="text-align: center;width: 100px;">Leasing</th>
                                <th style="text-align: center;">Program</th>
                                <th style="text-align: center;">Nama PO</th>
                                <th style="text-align: center;">Alamat STNK</th>
                                <th style="text-align: center;width: 100px;">Saldo AR</th>
                                <th style="text-align: center;width: 100px;">No. DO</th>
                                <th style="text-align: center;width: 100px;">Tgl DO</th>
                                <th style="text-align: center;">Tipe</th>
                                <th style="text-align: center;">Salesman</th>
                                <th style="text-align: center;">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody class="body-data"></tbody>
                    </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-tampil").click(function(){
            get_data();
        });
    });

    function get_data(){
        if ( $.fn.DataTable.isDataTable('.data-table') ) {
          $('.data-table').DataTable().destroy();
          $(".body-data").html('');
        }
        var periode_akhir = $("#periode_akhir").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('rptadharjp/get_data');?>",
            data: {"periode_akhir":periode_akhir},
            beforeSend: function(){
                $(".body-data").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var grand_total = 0;
                    $.each(obj.jp, function(key, leas){
                        var sub_total = 0;
                        $.each(obj.data, function(key, data){
                            if(leas===data.jnsbayar){
                                $(".body-data").append('<tr>' +
                                            '<td style="text-align: center; width: 20px;">'+data.no+'</td>' +
                                            '<td style="text-align: center;	width: 50px;">'+data.jnsbayar+'</td>' +
                                            '<td style="text-align: left;	width: 150px;">'+data.nmprogleas+'</td>' +
                                            '<td style="text-align: left;	width: 150px;">'+data.nama_s+'</td>' +
                                            '<td style="text-align: left;	width: 300px;">'+data.alamat+'</td>' +
                                            '<td style="text-align: right;	width: 80px;">'+numeral(Number(data.jp_saldo)).format('0,0')+'</td>' +
                                            '<td style="text-align: center;	width: 70px;">'+data.nodo+'</td>' +
                                            '<td style="text-align: center;	width: 70px;">'+moment(data.tgldo).format('L')+'</td>' +
                                            '<td style="text-align: left;	width: 200px;">'+data.kdtipe+'</td>' +
                                            '<td style="text-align: left;	width: 150px;">'+data.nmsales+'</td>' +
                                            '<td style="text-align: left;">'+data.ket_ar+'</td>' +
                                        '</tr>');
                                sub_total = Number(sub_total) + Number(data.jp_saldo);
                            }
                        }); 

                    $(".body-data").append('<tr style="background-color: #dadada;">' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;font-weight: bold;">Total '+leas+'</td>' +
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_total)).format('0,0')+'</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                            '</tr>');
                        grand_total = Number(grand_total) + Number(sub_total);
                    });


                    $(".body-data").append('<tr style="background-color: #dadada;">' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;font-weight: bold;">GRAND TOTAL</td>' +
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_total)).format('0,0')+'</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                                '<td style="text-align: left;">&nbsp</td>' +
                            '</tr>');
                }
              $(".data-table").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    },
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false,
                    //"scrollY": 500,
                    "scrollX": true
                });
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",false);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptstatbbn
 *
 * @author adi
 */
class Rptstatbbn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptstatbbn/submit'),
            'add' => site_url('rptstatbbn/add'),
            'edit' => site_url('rptstatbbn/edit'),
            'reload' => site_url('rptstatbbn'),
        );
        $this->load->model('rptstatbbn_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function getNama() {
        echo $this->rptstatbbn_qry->getNama();
    }
    
    public function getData() {
        echo $this->rptstatbbn_qry->getData();
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            'nama'=> array(
                    'placeholder' => 'Masukkan Kata Kunci <small>(Nama Pemilik / No. Polisi / No. DO / No. BPKB)</small>',
                    'attr'        => array(
                        'id'    => 'nama',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('nama'),
                    'name'     => 'nama',
            ),
            'pemilik'=> array(
                    'placeholder' => 'Nama Pemilik',
                    'id'          => 'pemilik',
                    'name'        => 'pemilik',
                    'value'       => set_value('pemilik'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nohp'=> array(
                    'placeholder' => 'No. HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nopol'=> array(
                    'placeholder' => 'No. Polisi',
                    'id'          => 'nopol',
                    'name'        => 'nopol',
                    'value'       => set_value('nopol'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nobpkb'=> array(
                    'placeholder' => 'No. BPKB',
                    'id'          => 'nobpkb',
                    'name'        => 'nobpkb',
                    'value'       => set_value('nobpkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'alamat'=> array(
                    'placeholder' => 'Alamat',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'resize: vertical;height: 50px; min-height: 50px;background-color: #fff;',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'jnsbayarx'=> array(
                    'placeholder' => 'Pembayaran',
                    'id'          => 'jnsbayarx',
                    'name'        => 'jnsbayarx',
                    'value'       => set_value('jnsbayarx'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'status_otrx'=> array(
                    'placeholder' => 'Status OTR',
                    'id'          => 'status_otrx',
                    'name'        => 'status_otrx',
                    'value'       => set_value('status_otrx'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nmsales'=> array(
                    'placeholder' => 'Nama Sales',
                    'id'          => 'nmsales',
                    'name'        => 'nmsales',
                    'value'       => set_value('nmsales'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nodo'=> array(
                    'placeholder' => 'No. DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => set_value('tgldo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_notis'=> array(
                    'placeholder' => 'Tgl Terima Notice',
                    'id'          => 'tgl_trm_notis',
                    'name'        => 'tgl_trm_notis',
                    'value'       => set_value('tgl_trm_notis'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_notis'=> array(
                    'placeholder' => 'Tgl Serah Notice',
                    'id'          => 'tgl_srh_notis',
                    'name'        => 'tgl_srh_notis',
                    'value'       => set_value('tgl_srh_notis'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_stnk'=> array(
                    'placeholder' => 'Tgl Terima STNK',
                    'id'          => 'tgl_trm_stnk',
                    'name'        => 'tgl_trm_stnk',
                    'value'       => set_value('tgl_trm_stnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_stnk'=> array(
                    'placeholder' => 'Tgl Serah STNK',
                    'id'          => 'tgl_srh_stnk',
                    'name'        => 'tgl_srh_stnk',
                    'value'       => set_value('tgl_srh_stnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_tnkb'=> array(
                    'placeholder' => 'Tgl Terima TNKB',
                    'id'          => 'tgl_trm_tnkb',
                    'name'        => 'tgl_trm_tnkb',
                    'value'       => set_value('tgl_trm_tnkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_tnkb'=> array(
                    'placeholder' => 'Tgl Serah TNKB',
                    'id'          => 'tgl_srh_tnkb',
                    'name'        => 'tgl_srh_tnkb',
                    'value'       => set_value('tgl_srh_tnkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_bpkb'=> array(
                    'placeholder' => 'Tgl Terima BPKB',
                    'id'          => 'tgl_trm_bpkb',
                    'name'        => 'tgl_trm_bpkb',
                    'value'       => set_value('tgl_trm_bpkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_bpkb'=> array(
                    'placeholder' => 'Tgl Serah BPKB',
                    'id'          => 'tgl_srh_bpkb',
                    'name'        => 'tgl_srh_bpkb',
                    'value'       => set_value('tgl_srh_bpkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
        );
    }
}

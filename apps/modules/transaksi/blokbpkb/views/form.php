<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>
            <div class="box-header box-view">
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-1">
                            <div class="form-group">
                            </div>
                        </div>

                        <div class="col-md-6"> 
                            <button type="button" class="btn btn-primary btn-save">Simpan</button>
                            <button type="button" class="btn btn-danger btn-cancel">Batal</button> 
                        </div>
                    </div>
                </div> 
            </div>
            
            <div class="col-xs-12">
                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
            </div>

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-1"> 
                                <label>
                                    <?php echo form_label($form['nodo']['placeholder']); ?>
                                </label> 
                            </div>

                            <div class="col-xs-2"> 
                                <?php
                                    echo form_input($form['nodo']);
                                    echo form_error('nodo','<div class="note">','</div>');
                                ?> 
                            </div>

                            <div class="col-xs-2">
                                <label>
                                    <?php echo form_label($form['noso']['placeholder']); ?>
                                </label>
                            </div>

                            <div class="col-xs-2">
                                <?php
                                    echo form_input($form['noso']);
                                    echo form_error('noso','<div class="note">','</div>');
                                ?> 
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-1"> 
                                <label>
                                    <?php echo form_label($form['tgldo']['placeholder']); ?>
                                </label> 
                            </div>

                            <div class="col-xs-2  "> 
                                <?php
                                    echo form_input($form['tgldo']);
                                    echo form_error('tgldo','<div class="note">','</div>');
                                ?> 
                            </div>

                            <div class="col-xs-2">
                                <label>
                                    <?php echo form_label($form['tglso']['placeholder']); ?>
                                </label>
                            </div>

                            <div class="col-xs-2"> 
                                <?php
                                    echo form_input($form['tglso']);
                                    echo form_error('tglso','<div class="note">','</div>');
                                ?> 
                            </div> 

                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>  </label>
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                            <div class="col-xs-6">
                                <label> Identitas Konsumen </label>
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3"> 
                                        <label>
                                            <?php echo form_label($form['nosin']['placeholder']); ?>
                                        </label> 
                                    </div>

                                    <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['nosin']);
                                            echo form_error('nosin','<div class="note">','</div>');
                                        ?> 
                                    </div>

                                    <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['nora']);
                                            echo form_error('nora','<div class="note">','</div>');
                                        ?> 
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>
                                            <?php echo form_label($form['nama']['placeholder']); ?>
                                        </label>
                                    </div>

                                    <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['nama']);
                                            echo form_error('nama','<div class="note">','</div>');
                                        ?> 
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3"> 
                                        <label>
                                            <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                        </label> 
                                    </div>

                                    <div class="col-xs-4"> 
                                        <?php
                                            echo form_input($form['kdtipe']);
                                            echo form_error('kdtipe','<div class="note">','</div>');
                                        ?> 
                                    </div>

                                    <div class="col-xs-5"> 
                                        <?php
                                            echo form_input($form['nmtipe']);
                                            echo form_error('nmtipe','<div class="note">','</div>');
                                        ?> 
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>
                                            <?php echo form_label($form['nama_s']['placeholder']); ?>
                                        </label>
                                    </div>

                                    <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['nama_s']);
                                            echo form_error('nama_s','<div class="note">','</div>');
                                        ?> 
                                    </div>
                                </div>
                            </div>
   
                        </div>
                    </div>  

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>
                                                <?php echo form_label($form['warna']['placeholder']); ?>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-xs-6"> 
                                        <?php
                                            echo form_input($form['warna']);
                                            echo form_error('warna','<div class="note">','</div>');
                                        ?> 
                                    </div>

                                    <div class="col-xs-3"> 
                                        <?php
                                            echo form_input($form['tahun']);
                                            echo form_error('tahun','<div class="note">','</div>');
                                        ?> 
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>
                                            <?php echo form_label($form['alamatktp_s']['placeholder']); ?>
                                        </label>
                                    </div>

                                    <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['alamatktp_s']);
                                            echo form_error('alamatktp_s','<div class="note">','</div>');
                                        ?> 
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>    

                          <div class="col-md-12 col-lg-12">
                              <div class="row">

                                 <div class="col-xs-6">
                                    <div class="row">
                                      <div class="col-xs-3"> 
                                          <label>
                                            <?php echo form_label($form['jnsbayar']['placeholder']); ?>
                                          </label> 
                                      </div>

                                      <div class="col-xs-6"> 
                                              <?php
                                                  echo form_input($form['jnsbayar']);
                                                  echo form_error('jnsbayar','<div class="note">','</div>');
                                              ?> 
                                      </div> 
                                    </div>
                                  </div>  
                              </div>
                          </div>   

                          <div class="col-md-12 col-lg-12">
                              <div class="row">

                                 <div class="col-xs-6">
                                    <div class="row">
                                      <div class="col-xs-3"> 
                                          <label>
                                            <?php echo form_label($form['harga_otr']['placeholder']); ?>
                                          </label> 
                                      </div>

                                      <div class="col-xs-4"> 
                                              <?php
                                                  echo form_input($form['harga_otr']);
                                                  echo form_error('harga_otr','<div class="note">','</div>');
                                              ?> 
                                      </div> 
                                    </div>
                                  </div> 
                              </div>
                          </div>   

                          <div class="col-md-12 col-lg-12">
                              <div class="row">

                                 <div class="col-xs-6">
                                    <div class="row">
                                      <div class="col-xs-3">
                                        <div class="form-group">
                                          <label>
                                            <?php echo form_label($form['um']['placeholder']); ?>
                                          </label>
                                        </div>
                                      </div>

                                      <div class="col-xs-4"> 
                                              <?php
                                                  echo form_input($form['um']);
                                                  echo form_error('um','<div class="note">','</div>');
                                              ?> 
                                      </div> 
                                    </div>
                                  </div> 
                              </div>
                          </div>   
                          
                          <div class="col-xs-12">
                              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                          </div>  

                          <div class="col-md-12 col-lg-12">
                              <div class="row">

                                 <div class="col-xs-6">
                                    <div class="row">
                                      <div class="col-xs-3">
                                          <label>
                                            <?php echo form_label($form['blokbpkb_nama']['placeholder']); ?>
                                          </label>
                                      </div>

                                      <div class="col-xs-9"> 
                                              <?php
                                                  echo form_input($form['blokbpkb_nama']);
                                                  echo form_error('blokbpkb_nama','<div class="note">','</div>');
                                              ?>  
                                      </div> 
                                    </div>
                                  </div> 

                                 <div class="col-xs-6">
                                    <div class="row">
                                      <div class="col-xs-6"> 
                                        <div class="checkbox">
                                          <label>
                                            <?php
                                                echo form_checkbox($form['blok']);
                                                echo "<b> Blok BPKB </b>";
                                            ?>
                                          </label>
                                        </div> 
                                    </div>
                                </div>
                            </div> 

                        </div>
                    </div>    

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3"> 
                                        <label>
                                            <?php echo form_label($form['blokbpkb_alamat']['placeholder']); ?>
                                        </label> 
                                    </div>

                                    <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blokbpkb_alamat']);
                                            echo form_error('blokbpkb_alamat','<div class="note">','</div>');
                                        ?> 
                                    </div> 
                                </div>
                            </div> 
                        </div>
                    </div>    

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-3"> 
                                    <label>
                                        <?php echo form_label($form['blokbpkb_noktp']['placeholder']); ?>
                                    </label> 
                                </div>

                                <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blokbpkb_noktp']);
                                            echo form_error('blokbpkb_noktp','<div class="note">','</div>');
                                        ?> 
                                    </div> 
                                </div>
                            </div> 
                        </div>
                    </div>    

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3"> 
                                        <label>
                                            <?php echo form_label($form['blokbpkb_nohp']['placeholder']); ?>
                                        </label> 
                                    </div>

                                    <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blokbpkb_nohp']);
                                            echo form_error('blokbpkb_nohp','<div class="note">','</div>');
                                        ?> 
                                    </div> 
                                </div>
                            </div> 
                        </div>
                    </div>    

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-3"> 
                                        <label>
                                            <?php echo form_label($form['blokbpkb_ket']['placeholder']); ?>
                                        </label> 
                                    </div>

                                    <div class="col-xs-9"> 
                                        <?php
                                            echo form_input($form['blokbpkb_ket']);
                                            echo form_error('blokbpkb_ket','<div class="note">','</div>');
                                        ?> 
                                    </div> 
                                </div>
                            </div> 
                        </div>
                    </div>    

                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function () {
        faktifblok(); 
        $("#harga_otr").autoNumeric('init');
        $("#um").autoNumeric('init');      

        $("#blok").click(function(){
            faktifblok();
        }); 

        $(".btn-save").click(function(){
            submit();
        });  

        $(".btn-cancel").click(function(){
            batal();
        });  
    });

    function faktifblok(){
        if($("#blok").prop("checked")){
            $('#blokbpkb_nama').prop('disabled',false);
            $('#blokbpkb_alamat').prop('disabled',false);
            $('#blokbpkb_noktp').prop('disabled',false);
            $('#blokbpkb_nohp').prop('disabled',false);
            $('#blokbpkb_ket').prop('disabled',false);
        } else {
            $('#blokbpkb_nama').val('');
            $('#blokbpkb_alamat').val('');
            $('#blokbpkb_noktp').val('');
            $('#blokbpkb_nohp').val('');
            $('#blokbpkb_ket').val('');
            $('#blokbpkb_nama').prop('disabled',true);
            $('#blokbpkb_alamat').prop('disabled',true);
            $('#blokbpkb_noktp').prop('disabled',true);
            $('#blokbpkb_nohp').prop('disabled',true);
            $('#blokbpkb_ket').prop('disabled',true);
        }
    }       

    function refresh(){
        table.ajax.reload();
    } 

    function batal(){  
        swal({
            title: "Konfirmasi Batal Blok BPKB!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            window.location.href = '<?=site_url('blokbpkb');?>';
        }); 
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nodo = $("#nodo").val();
            if($("#blok").prop("checked")){ 
                var fblokbpkb = 't';
            } else {
                var fblokbpkb = 'f';
            }
            var blokbpkb_nama = $("#blokbpkb_nama").val();
            var blokbpkb_alamat = $("#blokbpkb_alamat").val();
            var blokbpkb_noktp = $("#blokbpkb_noktp").val();
            var blokbpkb_nohp = $("#blokbpkb_nohp").val();
            var blokbpkb_ket = $("#blokbpkb_ket").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("blokbpkb/submit");?>",
                data: {"nodo":nodo
                        ,"fblokbpkb":fblokbpkb
                        ,"blokbpkb_nama":blokbpkb_nama
                        ,"blokbpkb_alamat":blokbpkb_alamat
                        ,"blokbpkb_noktp":blokbpkb_noktp
                        ,"blokbpkb_nohp":blokbpkb_nohp
                        ,"blokbpkb_ket":blokbpkb_ket },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if (data.tipe==='success'){
                            window.location.href = '<?=site_url('blokbpkb');?>';
                        }
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

</script>

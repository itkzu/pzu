<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-md-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-md-12 box-header box-view">
              <div class="col-md-3">
                <div class="row">
                    <a href="<?php echo $add;?>" class="btn btn-primary btn-add">Tambah</a>
                    <a class="btn btn-primary btn-edit">Ubah</a>
                    <a class="btn btn-danger btn-del">Hapus</a> 
                </div>
              </div>
              <div class="col-md-2">
                <div class="row"> 
                </div>
              </div>
              <div class="col-md-7">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-ctk"><i class="fa fa-print" aria-hidden="true"></i> Cetak</button>
                </div>
              </div>
          </div>  

          <div class="col-md-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['noajustnk']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-9">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['noajustnk']);
                                          echo form_error('noajustnk','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div>

                        <div class="col-md-3 col-md-3">
                          <div class="row">

                              <div class="col-md-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['noso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['noso']);
                                        echo form_error('noso','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>
                          </div>
                        </div>

                        <div class="col-md-3 col-md-3">
                          <div class="row">

                              <div class="col-md-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['tglso']);
                                        echo form_error('tglso','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-5 col-md-5 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglajustnk']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tglajustnk']);
                                          echo form_error('tglajustnk','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['ket']);
                                          echo form_error('ket','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div>

                        <div class="col-md-3 col-md-3">
                          <div class="row">

                              <div class="col-md-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['nodo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['nodo']);
                                        echo form_error('nodo','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>
                          </div>
                        </div>

                        <div class="col-md-3 col-md-3">
                          <div class="row">

                              <div class="col-md-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgldo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-7">
                                  <div class="form-group">
                                    <?php
                                        echo form_input($form['tgldo']);
                                        echo form_error('tgldo','<div class="note">','</div>');
                                    ?>
                                  </div>
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-md-6">
                          <label> Unit </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                      <div class="col-md-6">
                          <label> Identitas STNK </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div> 

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-6 col-md-6 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['nosin']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                      <?php
                                          echo form_input($form['nosin']);
                                          echo form_error('nosin','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-md-5"> 
                                      <?php
                                          echo form_input($form['nora']);
                                          echo form_error('nora','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-md-6 col-md-6">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['nama']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-9"> 
                                    <?php
                                        echo form_input($form['nama']);
                                        echo form_error('nama','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-6 col-md-6 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                      <?php
                                          echo form_input($form['kdtipe']);
                                          echo form_error('kdtipe','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-md-5"> 
                                      <?php
                                          echo form_input($form['nmtipe']);
                                          echo form_error('nmtipe','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-md-6 col-md-6">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['alamat']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-9"> 
                                    <?php
                                        echo form_input($form['alamat']);
                                        echo form_error('alamat','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-6 col-md-6 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['warna']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                      <?php
                                          echo form_input($form['warna']);
                                          echo form_error('warna','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-md-5"> 
                                      <?php
                                          echo form_input($form['tahun']);
                                          echo form_error('tahun','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-6 col-md-6 ">
                          <div class="row"> 

                          </div>
                        </div>

                        <div class="col-md-6 col-md-6">
                          <div class="row">

                              <div class="col-md-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['kel']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                    <?php
                                        echo form_input($form['kel']);
                                        echo form_error('kel','<div class="note">','</div>');
                                    ?> 
                              </div>

                              <div class="col-md-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['kec']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                    <?php
                                        echo form_input($form['kec']);
                                        echo form_error('kec','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-6 col-md-6 ">
                          <div class="row">

                              <div class="col-md-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['harga_beli']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                      <?php
                                          echo form_input($form['harga_beli']);
                                          echo form_error('harga_beli','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-md-5"> 
                                      <?php
                                          echo form_label("<small><i>Harga Beli yang tertera pada Faktur Astra</i></small>")
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-md-6 col-md-6">
                          <div class="row">

                              <div class="col-md-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['kota']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                    <?php
                                        echo form_dropdown($form['kota']['name'],
                                                                           $form['kota']['data'],
                                                                           $form['kota']['value'],
                                                                           $form['kota']['attr']);
                                        echo form_error('kota', '<div class="note">', '</div>');  
                                    ?>
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-6 col-md-6 ">
                          <div class="row"> 

                          </div>
                        </div>

                        <div class="col-md-6 col-md-6">
                          <div class="row">

                              <div class="col-md-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['nohp']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                    <?php
                                        echo form_input($form['nohp']);
                                        echo form_error('nohp','<div class="note">','</div>');
                                    ?> 
                              </div>

                              <div class="col-md-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['notelp']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-md-4"> 
                                    <?php
                                        echo form_input($form['notelp']);
                                        echo form_error('notelp','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        reset();
        // $("#noajustnk").mask("9999/PZUS-KPS/99/99");
        autonum();
        disabled();
        set_kode();

        $('#noajustnk').keyup(function () {
            set_noaju();
        }); 

        $('.btn-ctk').click(function(){
            var nokps = $('#noajustnk').val();
                        var lik = nokps.replace("/","+");
                        var lik = lik.replace("/","+");
                        var lik = lik.replace("/","+");
            window.open('kwajustnk/ctk/'+lik, '_blank');
            // window.location.href = 'lrnew/ctk_lr/'+nodo;

            // ctk_lr();
        });
    });

    function disabled(){  
      $("#tglajustnk").attr('disabled',true);
      $("#nosin").attr('disabled',true);
      $("#harga_beli").attr('disabled',true);
      $("#nama").attr('disabled',true);
      $("#alamat").attr('disabled',true);
      $("#kel").attr('disabled',true);
      $("#kec").attr('disabled',true);
      $("#kota").attr('disabled',true);
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',true);
      $(".btn-del").attr('disabled',true);
      $(".btn-ctk").attr('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){  
      $(".btn-add").attr('disabled',false);
      $(".btn-edit").attr('disabled',false);
      $(".btn-del").attr('disabled',false);
      $(".btn-ctk").attr('disabled',false); 
    }

    function clear(){ 
      $("#tglajustnk").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#noso").val('');
      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nosin").val('');
      $("#nora").val('');
      $("#kdtipe").val('');
      $("#nmtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#harga_beli").val('');
      $("#nama").val('');
      $("#alamat").val('');
      $("#kel").val('');
      $("#kec").val('');
      $("#nohp").val('');
      $("#notelp").val(''); 
    }

    function reset(){
      $("#noajustnk").val('');
      $("#tglajustnk").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#noso").val('');
      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nosin").val('');
      $("#nora").val('');
      $("#kdtipe").val('');
      $("#nmtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#harga_beli").val('');
      $("#nama").val('');
      $("#alamat").val('');
      $("#kel").val('');
      $("#kec").val('');
      $("#nohp").val('');
      $("#notelp").val('');
    }

    function autonum(){
      $('#harga_beli').autoNumeric('init',{ currencySymbol : 'Rp.'});
    }

    function set_kode(){
        var tg = new Date();
        var tgl = tg.toString();
        var tanggal = tgl.substring(13,15);
        $.ajax({
            type: "POST",
            url: "<?=site_url("kwajustnk/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#nodo").mask(data.kode+"99-999999");
                      $("#noso").mask(data.kode+"99-999999");
                      $("#noajustnk").mask("9999/PZU"+data.kode+"-KPS/99/99");
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_noaju(){

      var nokps = $("#noajustnk").val();
      var nokps = nokps.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("kwajustnk/set_noaju");?>",
            data: {"nokps":nokps },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  clear();
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){ 

                  $("#tglajustnk").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkps)));
                  $("#noso").val(data.noso);
                  $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                  $("#nodo").val(data.nodo);
                  $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                  $("#nosin").val(data.nosin);
                  $("#nora").val(data.nora);
                  $("#kdtipe").val(data.kdtipe);
                  $("#nmtipe").val(data.nmtipe);
                  $("#warna").val(data.warna);
                  $("#tahun").val(data.warna);
                  $("#harga_beli").autoNumeric('set',data.harga);
                  $("#nama").val(data.nama_s);
                  $("#alamat").val(data.alamat_s);
                  $("#kel").val(data.kel_s);
                  $("#kec").val(data.kec_s);
                  $("#kota").val(data.kota_s);
                  $("#nohp").val(data.nohp_s);
                  $("#notelp").val(data.notelp_s); 
                  $(".btn-ctk").attr('disabled',false);
                  $(".btn-edit").attr('disabled',false);
                  $(".btn-del").attr('disabled',false);
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstdatacust
 *
 * @author adi
 */
class Mstdatacust extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('mstdatacust/submit'),
            'add' => site_url('mstdatacust/add'),
            'edit' => site_url('mstdatacust/edit'),
            'reload' => site_url('mstdatacust'),
        );
        $this->load->model('mstdatacust_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function updateData() {
        echo $this->mstdatacust_qry->updateData();
    }
    
    public function getNama() {
        echo $this->mstdatacust_qry->getNama();
    }
    
    public function getData() {
        echo $this->mstdatacust_qry->getData();
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            'nama'=> array(
                    'attr'        => array(
                        'id'    => 'nama',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('nama'),
                    'name'     => 'nama',
            ),
            'tgldo'=> array(
                    'placeholder' => 'Tanggal DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => set_value('tgldo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'pemilik'=> array(
                    'placeholder' => 'Nama Pemilik',
                    'id'          => 'pemilik',
                    'name'        => 'pemilik',
                    'value'       => set_value('pemilik'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'alamat'=> array(
                    'placeholder' => 'Alamat',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'resize: vertical;height: 100px; min-height: 100px;background-color: #fff;',
            ),
            
            'nohp'=> array(
                    'placeholder' => 'No. HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            
            'jnsbayarx'=> array(
                    'placeholder' => 'Pembayaran',
                    'id'          => 'jnsbayarx',
                    'name'        => 'jnsbayarx',
                    'value'       => set_value('jnsbayarx'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
        );
    }
}

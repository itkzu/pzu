<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dfnew
 *
 * @author adi
 */
class Dfnew extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('dfnew/submit'),
            'add' => site_url('dfnew/add'),
            'edit' => site_url('dfnew/edit'),
            'reload' => site_url('dfnew'),
        );
        $this->load->model('dfnew_qry');
        $cabang = $this->dfnew_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function getDetailPO() {
        echo $this->dfnew_qry->getDetailPO();
    }


    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->dfnew_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->dfnew_qry->json_dgview_detail();
    }

    public function printAll() {
        $this->data['data'] = $this->dfnew_qry->printAll();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function addDetail() {
            echo $this->dfnew_qry->addDetail();
    }

    public function detaildeleted() {
            echo $this->dfnew_qry->detaildeleted();
    }

    public function delete() {
            echo $this->dfnew_qry->delete();
    }

    public function submit() {
            echo $this->dfnew_qry->submit();
    }

    private function _init_add(){
        $this->data['form'] = array(
           'nodf'=> array(
                    'placeholder' => 'No. DF',
                    //'type'        => 'hidden',
                    'id'          => 'nodf',
                    'name'        => 'nodf',
                    'value'       => set_value('nodf'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tgldf'=> array(
                    'placeholder' => 'Tanggal Rencana Bayar',
                    'id'          => 'tgldf',
                    'name'        => 'tgldf',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
           'ket'=> array(
                    'placeholder' => 'Catatan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '',
                    'placeholder' => 'Cabang',
            ),
            'nopo'=> array(
                    'placeholder' => 'No. Faktur',
                    'value'       => set_value('nopo'),
                    'id'          => 'nopo',
                    'name'        => 'nopo',
                    'class'       => 'form-control',
                    'style'       => '',
            ),
            'nap'=> array(
                    'placeholder' => 'Pokok Hutang',
                    'value'       => set_value('nap'),
                    'id'          => 'nap',
                    'name'        => 'nap',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
                    'onkeyup'     => 'sum();'
            ),
            'tglpo'=> array(
                    'placeholder' => 'Tanggal Faktur',
                    'value'       => set_value('tglpo'),
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'nint'=> array(
                    'placeholder' => 'Bunga',
                    'value'       => set_value('nint'),
                    'id'          => 'nint',
                    'name'        => 'nint',
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'sum();'
            ),
            'nsel'=> array(
                    'placeholder' => 'Selisih',
                    'type'        =>  'hidden',
                    'value'       => set_value('nsel'),
                    'id'          => 'nsel',
                    'name'        => 'nsel',
                    'class'       => 'form-control',
                    'style'       => '',
                    'onkeyup'     => 'sum();'
            ),
            'total'=> array(
                    'placeholder' => 'Total',
                    'value'       => set_value('total'),
                    'id'          => 'total',
                    'name'        => 'total',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'nourut'=> array(
                    'placeholder' => 'No urut',
                    'type'        => 'hidden',
                    'value'       => set_value('nourut'),
                    'id'          => 'nourut',
                    'name'        => 'nourut',
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(
          'nodf'=> array(
                   'placeholder' => 'No. DF',
                   //'type'        => 'hidden',
                   'id'          => 'nodf',
                   'name'        => 'nodf',
                   'value'       => $this->val[0]['nodf'],
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
          'tgldf'=> array(
                   'placeholder' => 'Tanggal Rencana Bayar',
                   'id'          => 'tgldf',
                   'name'        => 'tgldf',
                   'value'       => $this->apps->dateConvert($this->val[0]['tgldf']),
                   'class'       => 'form-control calendar',
                   'style'       => '',
           ),
          'ket'=> array(
                   'placeholder' => 'Catatan',
                   'id'          => 'ket',
                   'name'        => 'ket',
                   'value'       => $this->val[0]['ket'],
                   'class'       => 'form-control',
                   'style'       => '',
           ),
           'kddiv'=> array(
                   'attr'        => array(
                       'id'    => 'kddiv',
                       'class' => 'form-control  select',
                   ),
                   //'data'     =>  $this->data['kddiv'][$this->val[0]['nmdiv']]=$this->val[0]['nmdiv'],
                   'data'     =>  $this->data['kddiv'],
                   'value'    =>  $this->val[0]['kddiv'],
                   'name'     => 'kddiv',
                   'required'    => '',
                   'placeholder' => 'Cabang',
           ),
           'nopo'=> array(
                   'placeholder' => 'No. Faktur',
                   'value'       => set_value('nopo'),
                   'id'          => 'nopo',
                   'name'        => 'nopo',
                   'class'       => 'form-control',
                   'style'       => '',
           ),
           'nap'=> array(
                   'placeholder' => 'Pokok Hutang',
                   'value'       => set_value('nap'),
                   'id'          => 'nap',
                   'name'        => 'nap',
                   'class'       => 'form-control',
                   'style'       => 'text-transform: uppercase;',
                   'readonly'    => '',
                   'onkeyup'     => 'sum();'
           ),
           'tglpo'=> array(
                   'placeholder' => 'Tanggal Faktur',
                   'value'       => set_value('tglpo'),
                   'id'          => 'tglpo',
                   'name'        => 'tglpo',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nint'=> array(
                   'placeholder' => 'Bunga',
                   'value'       => set_value('nint'),
                   'id'          => 'nint',
                   'name'        => 'nint',
                   'class'       => 'form-control',
                   'style'       => 'text-transform: uppercase;',
                   'onkeyup'     => 'sum();'
           ),
           'nsel'=> array(
                   'placeholder' => 'Selisih',
                   'type'        =>  'hidden',
                   'value'       => set_value('nsel'),
                   'id'          => 'nsel',
                   'name'        => 'nsel',
                   'class'       => 'form-control',
                   'style'       => '',
                   'onkeyup'     => 'sum();'
           ),
           'total'=> array(
                   'placeholder' => 'Total',
                   'value'       => set_value('total'),
                   'id'          => 'total',
                   'name'        => 'total',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nourut'=> array(
                   'placeholder' => 'No urut',
                   'type'        => 'hidden',
                   'value'       => set_value('nourut'),
                   'id'          => 'nourut',
                   'name'        => 'nourut',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->dfnew_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nodf,$nopo) {
        if(!empty($nodf) && !empty($nopo)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nopo',
                    'label' => 'No. Faktur',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

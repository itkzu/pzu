<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>
			<div class="box-body">
				<div class="form-group">
                    <?php 
                        echo form_label($form['kddiv']['placeholder']);
                        echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                		echo form_error('kddiv','<div class="note">','</div>');  
                    ?>
                </div>
				<div class="form-group">
					<?php
						echo '<div class="radio">';
						echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Periode Bulan <small>per bulan</small>');
						echo '</div>';
						echo form_input($form['periode']);
						echo form_error('akun_multi','<div class="note">','</div>');
					?>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Periode Awal');
								echo '</div>';
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Pilih Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							 ?>
						</div>

					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				</div>

				<div class="table-responsive">
					<table style="width: 4000px;" class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Nomor Nota Servis</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Tgl Nota Servis</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">No Mesin</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Nama Motor</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Nama Pemilik</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Jenis</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Kode Jasa/Part</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Grup Jasa/Part</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Nama Jasa/Part</th>
								<th rowspan="2" style="width: 10px; text-align: center; vertical-align: middle;">Jumlah</th>
								<th rowspan="2" style="width: 10px; text-align: center; vertical-align: middle;">Total</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Jenis Bayar</th>
								<th rowspan="2" style="width: 20px; text-align: center; vertical-align: middle;">Tanggal Bayar</th>
								<th colspan="14" style="width: 20px; text-align: center;">Oli Tunai</th>
								<th rowspan="2" style="width: 10px; text-align: center; vertical-align: middle;">Jasa Tunai</th>
								<th rowspan="2" style="width: 10px; text-align: center; vertical-align: middle;">Sparepart</th>
								<th rowspan="2" style="width: 10px; text-align: center; vertical-align: middle;">Total Tunai</th>
								<th colspan="12" style="width: 20px; text-align: center;">KPB</th>
								
							</tr>
							<tr>
								<th style="width: 10px; text-align: center;">Scooter Gear</th>
								<th style="width: 10px; text-align: center;">MPX1 0,8L</th>
								<th style="width: 10px; text-align: center;">MPX1 1L</th>
								<th style="width: 10px; text-align: center;">MPX1 1,2L</th>
								<th style="width: 10px; text-align: center;">MPX2 0,65L</th>
								<th style="width: 10px; text-align: center;">MPX2 0,8L</th>
								<th style="width: 10px; text-align: center;">MPX3 0,8L</th>
								<th style="width: 10px; text-align: center;">SPX1 0,8L</th>
								<th style="width: 10px; text-align: center;">SPX1 1L</th>
								<th style="width: 10px; text-align: center;">SPX1 1,2L</th>
								<th style="width: 10px; text-align: center;">SPX2 0,65L</th>
								<th style="width: 10px; text-align: center;">SPX2 0,8L</th>
								<th style="width: 10px; text-align: center;">TUNAI OTH</th>
								<th style="width: 10px; text-align: center;">Total Oli</th>
								<th style="width: 10px; text-align: center;">Jasa KPB</th>
								<th style="width: 10px; text-align: center;">MPX1 0,8L</th>
								<th style="width: 10px; text-align: center;">MPX1 1L</th>
								<th style="width: 10px; text-align: center;">MPX1 1,2L</th>
								<th style="width: 10px; text-align: center;">MPX2 0,65L</th>
								<th style="width: 10px; text-align: center;">MPX2 0,8L</th>
								<th style="width: 10px; text-align: center;">MPX3 0,8L</th>
								<th style="width: 10px; text-align: center;">SPX1 0,8L</th>
								<th style="width: 10px; text-align: center;">SPX1 1L</th>
								<th style="width: 10px; text-align: center;">SPX1 1,2L</th>
								<th style="width: 10px; text-align: center;">SPX2 0,65L</th>
								<th style="width: 10px; text-align: center;">SPX2 0,8L</th>
								<th style="width: 10px; text-align: center;">KPB OTHER</th>
								<th style="width: 10px; text-align: center;">Total KPB</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		disable();
		$("#rdakun_multi").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});
		$("#rdakun_between").click(function(){
			$(".btn-tampil").prop("disabled", false);
			setAkun();
		});

		$('#periode').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});
		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});
		$(".btn-tampil").click(function(){
			$(".dataTable").show();
			tampilData();
		});
	});

	function tampilData(){

		var kddiv = $("#kddiv").val();
		var periode = $("#periode").val();
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val();
		var periode_d = $('#rdakun_multi').is(':checked');
		if(periode_d){
			var ket = "periode_multi";
		} else {
			var ket = "periode_between";
		}

		var column = [];

        column.push({
            "aTargets": [ 1,11 ],
            "mRender": function (data, type, full) {
              return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

		column.push({
			"aTargets": [ 9 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
			},
			"sClass": "right"
		});

        for (i = 12; i <= 42; i++) { 
            column.push({ 
                "aTargets": [ i ],
                "mRender": function (data, type, full) {
                    return type === 'export' ? data : numeral(data).format('0,0.00');
                    } ,
                "sClass": "right"
                });
        }


		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"columns": [
				{ "data": "nomor_nota_servis" },
				{ "data": "tgl_nota_servis" }, 
				{ "data": "nomor_mesin" }, 
				{ "data": "nama_motor" }, 
				{ "data": "nama_pemilik" }, 
				{ "data": "jenis" },
				{ "data": "kode_jasa_part" },
				{ "data": "grup_jasa_part" },
				{ "data": "nama_jasa_part" },
				{ "data": "jumlah" },
				{ "data": "total" },
				{ "data": "jenis_bayar" },
				{ "data": "tglbayar" },
				{ "data": "scooter_gear" },		// 11
				{ "data": "tunai_mpx1_08l" },
				{ "data": "tunai_mpx1_1l" },
				{ "data": "tunai_mpx1_12l" },
				{ "data": "tunai_mpx2_065l" },
				{ "data": "tunai_mpx2_08l" },
				{ "data": "tunai_mpx3_08l" },
				{ "data": "tunai_spx1_08l" },
				{ "data": "tunai_spx1_1l" },
				{ "data": "tunai_spx1_12l" },
				{ "data": "tunai_spx2_065l" },
				{ "data": "tunai_spx2_08l" },
				{ "data": "tunai_oth_oli" },
				{ "data": "total_oli" },
				{ "data": "total_jasa" },
				{ "data": "total_sparepart" },
				{ "data": "total_tunai" },
				{ "data": "kpb_jasa" },
				{ "data": "kpb_mpx1_08l" },
				{ "data": "kpb_mpx1_1l" },
				{ "data": "kpb_mpx1_12l" },
				{ "data": "kpb_mpx2_065l" },
				{ "data": "kpb_mpx2_08l" },
				{ "data": "kpb_mpx3_08l" },
				{ "data": "kpb_spx1_08l" },
				{ "data": "kpb_spx1_1l" },
				{ "data": "kpb_spx1_12l" },
				{ "data": "kpb_spx2_065l" },
				{ "data": "kpb_spx2_08l" },
				{ "data": "kpb_oth_oli" },
				{ "data": "total_kpb" }
			],
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push(
							{ "name": "kddiv", "value": kddiv },
							{ "name": "ket", "value": ket },
							{ "name": "periode", "value": periode },
							{ "name": "periode_awal", "value": periode_awal },
							{ "name": "periode_akhir", "value": periode_akhir }
							);
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
        		$('td', row).eq(13).css('background-color', '#DAEEF3');
        		$('td', row).eq(14).css('background-color', '#DAEEF3');
        		$('td', row).eq(15).css('background-color', '#DAEEF3');
        		$('td', row).eq(16).css('background-color', '#DAEEF3');
        		$('td', row).eq(17).css('background-color', '#DAEEF3');
        		$('td', row).eq(18).css('background-color', '#DAEEF3');
        		$('td', row).eq(19).css('background-color', '#DAEEF3');
        		$('td', row).eq(20).css('background-color', '#DAEEF3');
        		$('td', row).eq(21).css('background-color', '#DAEEF3');
        		$('td', row).eq(22).css('background-color', '#DAEEF3');
        		$('td', row).eq(23).css('background-color', '#DAEEF3');
        		$('td', row).eq(24).css('background-color', '#DAEEF3');
        		$('td', row).eq(25).css('background-color', '#DAEEF3');
        		$('td', row).eq(26).css('background-color', '#DAEEF3');
        		$('td', row).eq(27).css('background-color', '#DAEEF3');
        		$('td', row).eq(28).css('background-color', '#DAEEF3');
        		$('td', row).eq(29).css('background-color', '#DAEEF3');
        		$('td', row).eq(30).css('background-color', '#F2DCDB');
        		$('td', row).eq(31).css('background-color', '#F2DCDB');
        		$('td', row).eq(32).css('background-color', '#F2DCDB');
        		$('td', row).eq(33).css('background-color', '#F2DCDB');
        		$('td', row).eq(34).css('background-color', '#F2DCDB');
        		$('td', row).eq(35).css('background-color', '#F2DCDB');
        		$('td', row).eq(36).css('background-color', '#F2DCDB');
        		$('td', row).eq(37).css('background-color', '#F2DCDB');
        		$('td', row).eq(38).css('background-color', '#F2DCDB');
        		$('td', row).eq(39).css('background-color', '#F2DCDB');
        		$('td', row).eq(40).css('background-color', '#F2DCDB');
        		$('td', row).eq(41).css('background-color', '#F2DCDB');
        		$('td', row).eq(42).css('background-color', '#F2DCDB');
        		$('td', row).eq(43).css('background-color', '#F2DCDB');
				// if(data[23]){
				// 	$(row).find('td:eq(23)').css('background-color', '#ff9933');
				// }
			},
			"sAjaxSource": "<?=site_url('rptsodfpbkl/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
          	    $( api.column( 10 ).footer() ).html('Total');
                for(var i=13; i<=43; i++) {
		            // let sum = api.column(i).data().sum();
		            var sum = api
		            	.column(i)
		            	.data()
		            	.reduce( function (a, b) {
		            		return intVal(a) + intVal(b);
		            	}, 0);
		            $(api.column(i).footer()).html(numeral(sum).format('0,0.00'));
		        }
                // computing column Total of the complete result
                // var scooter_gear = api
                //     .column( 5 )
                //     .data()
                //     .reduce( function (a, b) {
                //         return intVal(a) + intVal(b);
                //     }, 0 );
              },

			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'csv', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'excel', footer: true,
					exportOptions: {orthogonal: 'export' }},
				{extend: 'pdf', footer: true,
					orientation: 'landscape',
					pageSize: 'A0'
				},
				{extend: 'print', footer: true,
					customize: function (win){
					   $(win.document.body).addClass('white-bg');
					   $(win.document.body).css('font-size', '10px');
					   $(win.document.body).find('table')
							.addClass('compact')
							.css('font-size', 'inherit');
				   }
				}
			],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
            // var title = $('.dataTable thead th').eq( $(this).index() ).text();
            // if(title!=="Edit" && title!=="Delete" ){
            //     $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Cari" />' );
            // }else{
            //     $(this).html( '' );
            // }

		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			} );
		});
	}

	function disable(){
		$(".dataTable").hide();
		$("#periode").prop("disabled", true);
		$(".btn-tampil").prop("disabled", true);
		$("#periode_awal").prop("disabled", true);
		$("#periode_akhir").prop("disabled", true);
	}

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#periode").prop("disabled", false);
			$("#periode_awal").prop("disabled", true);
			$("#periode_akhir").prop("disabled", true);
		}else{
			$("#periode").prop("disabled", true);
			$("#periode_awal").prop("disabled", false);
			$("#periode_akhir").prop("disabled", false);

		}
	}
</script>

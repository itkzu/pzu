<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">
          <div class="box-header box-view">
            <div class="col-md-12 col-lg-12">
              <div class="row">
                <div class="col-xs-1">
                  <div class="form-group">
                  </div>
                </div>

                <div class="col-md-6"> 
                  <button type="button" class="btn btn-warning btn-edit" onclick="ubah()">Ubah</button>   
                </div>
              </div>
            </div> 
          </div>
          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1"> 
                              <label>
                                <?php echo form_label($form['nodo']['placeholder']); ?>
                              </label> 
                        </div>

                        <div class="col-xs-2"> 
                          <?php
                            echo form_input($form['nodo']);
                            echo form_error('nodo','<div class="note">','</div>');
                          ?> 
                        </div>

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['noso']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                          <?php
                            echo form_input($form['noso']);
                            echo form_error('noso','<div class="note">','</div>');
                          ?> 
                        </div> 
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1"> 
                              <label>
                                <?php echo form_label($form['tgldo']['placeholder']); ?>
                              </label> 
                        </div>

                        <div class="col-xs-2  "> 
                            <?php
                                echo form_input($form['tgldo']);
                                echo form_error('tgldo','<div class="note">','</div>');
                            ?> 
                        </div>

                        <div class="col-xs-2">
                          <label>
                            <?php echo form_label($form['tglso']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2"> 
                                <?php
                                    echo form_input($form['tglso']);
                                    echo form_error('tglso','<div class="note">','</div>');
                                ?> 
                        </div> 

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-6">
                          <label>  </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                      <div class="col-xs-6">
                          <label> Identitas Konsumen </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['nosin']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['nosin']);
                                          echo form_error('nosin','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-5"> 
                                      <?php
                                          echo form_input($form['nora']);
                                          echo form_error('nora','<div class="note">','</div>');
                                      ?> 
                              </div>
                            </div>
                          </div>

                          <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3">
                                <label>
                                  <?php echo form_label($form['nama']['placeholder']); ?>
                                </label>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['nama']);
                                          echo form_error('nama','<div class="note">','</div>');
                                      ?> 
                              </div>
                          </div>
                        </div>

                           
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['kdtipe']);
                                          echo form_error('kdtipe','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-5"> 
                                      <?php
                                          echo form_input($form['nmtipe']);
                                          echo form_error('nmtipe','<div class="note">','</div>');
                                      ?> 
                              </div>
                            </div>
                          </div>

                          <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3">
                                <label>
                                  <?php echo form_label($form['nama_s']['placeholder']); ?>
                                </label>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['nama_s']);
                                          echo form_error('nama_s','<div class="note">','</div>');
                                      ?> 
                              </div>
                          </div>
                        </div>

                           
                      </div>
                  </div>  

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3">
                                <div class="form-group">
                                  <label>
                                    <?php echo form_label($form['warna']['placeholder']); ?>
                                  </label>
                                </div>
                              </div>

                              <div class="col-xs-6"> 
                                      <?php
                                          echo form_input($form['warna']);
                                          echo form_error('warna','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-3"> 
                                      <?php
                                          echo form_input($form['tahun']);
                                          echo form_error('tahun','<div class="note">','</div>');
                                      ?> 
                              </div>
                            </div>
                          </div>

                          <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3">
                                <label>
                                  <?php echo form_label($form['alamatktp_s']['placeholder']); ?>
                                </label>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['alamatktp_s']);
                                          echo form_error('alamatktp_s','<div class="note">','</div>');
                                      ?> 
                              </div>
                          </div>
                        </div>

                           
                      </div>
                  </div>    

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['jnsbayar']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-6"> 
                                      <?php
                                          echo form_input($form['jnsbayar']);
                                          echo form_error('jnsbayar','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div>  
                      </div>
                  </div>   

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['harga_otr']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['harga_otr']);
                                          echo form_error('harga_otr','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div> 
                      </div>
                  </div>   

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3">
                                <div class="form-group">
                                  <label>
                                    <?php echo form_label($form['um']['placeholder']); ?>
                                  </label>
                                </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['um']);
                                          echo form_error('um','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div> 
                      </div>
                  </div>   
                  
                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                  </div>  

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3">
                                  <label>
                                    <?php echo form_label($form['blokbpkb_nama']['placeholder']); ?>
                                  </label>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['blokbpkb_nama']);
                                          echo form_error('blokbpkb_nama','<div class="note">','</div>');
                                      ?>  
                              </div> 
                            </div>
                          </div> 

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-6"> 
                                <div class="checkbox">
                                  <label>
                                    <?php
                                        echo form_checkbox($form['blok']);
                                        echo "<b> Blok BPKB </b>";
                                    ?>
                                  </label>
                                </div> 
                            </div>
                            </div>
                          </div> 

                        
                      </div>
                  </div>    

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['blokbpkb_alamat']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['blokbpkb_alamat']);
                                          echo form_error('blokbpkb_alamat','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div> 
                      </div>
                  </div>    

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['blokbpkb_noktp']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['blokbpkb_noktp']);
                                          echo form_error('blokbpkb_noktp','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div> 
                      </div>
                  </div>    

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['blokbpkb_nohp']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['blokbpkb_nohp']);
                                          echo form_error('blokbpkb_nohp','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div> 
                      </div>
                  </div>    

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                         <div class="col-xs-6">
                            <div class="row">
                              <div class="col-xs-3"> 
                                  <label>
                                    <?php echo form_label($form['blokbpkb_ket']['placeholder']); ?>
                                  </label> 
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['blokbpkb_ket']);
                                          echo form_error('blokbpkb_ket','<div class="note">','</div>');
                                      ?> 
                              </div> 
                            </div>
                          </div> 
                      </div>
                  </div>    

              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 
        clear();
        GetNoID();
        $(".btn-edit").prop('disabled',true); 
        $("#blok").prop("checked",false);
        $("#blok").prop('disabled',true);     
        $("#harga_otr").autoNumeric('init');
        $("#um").autoNumeric('init');

        $("#nodo").change(function (){
            set_do();
        });
    });

    // no id
    function GetNoID(){
      var tg = new Date();
      var tgl = tg.toString();
      var tanggal = tgl.substring(13,15);
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("blokbpkb/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#nodo').mask(value.kode+"99-999999"); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }  

    function ubah(){
        var nodo = $('#nodo').val();
        window.location.href = 'blokbpkb/edit/'+nodo;
    }

    function clear(){
      $("#nodo").val('');
      $("#noso").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nosin").val('');
      $("#nora").val('');
      $("#nama").val('');
      $("#nama_s").val('');
      $("#alamatktp_s").val('');
      $("#kdtipe").val('');
      $("#nmtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#jnsbayar").val('');
      $("#harga_otr").val('');
      $("#um").val('');
      $("#blokbpkb_nama").val('');
      $("#blokbpkb_alamat").val('');
      $("#blokbpkb_noktp").val('');
      $("#blokbpkb_nohp").val('');
      $("#blokbpkb_ket").val(''); 

      $(".btn-edit").prop('disabled',true); 
    }

    function set_do(){

      var nodo = $("#nodo").val(); 
      var nodo = nodo.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("blokbpkb/set_do");?>",
            data: {"nodo":nodo },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan atau bukan DO Tunai",
                      type: "warning"
                  })
                  clear(); 
                  $(".btn-edit").attr('disabled',true); 
                  $("#blok").prop("checked",false);
                  $("#blok").prop('disabled',true);     
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#noso").val(data.noso);
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#nosin").val(data.nosin2);
                      $("#kdtipe").val(data.kdtipex);
                      $("#nmtipe").val(data.nmtipe);
                      $("#nora").val(data.nora2);
                      $("#nama").val(data.nama);
                      $("#nama_s").val(data.nama_s);
                      $("#alamatktp_s").val(data.alamatktp_s2);
                      $("#warna").val(data.warna);
                      $("#tahun").val(data.tahun);
                      $("#jnsbayar").val(data.jnsbayar);
                      $("#harga_otr").autoNumeric('set',data.harga_otr);
                      $("#um").autoNumeric('set',data.um);
                      if(data.fblokbpkb==='t'){
                          $("#blok").prop("checked",true);
                      } else {
                          $("#blok").prop("checked",false);
                      }
                      $("#blokbpkb_nama").val(data.blokbpkb_nama);
                      $("#blokbpkb_alamat").val(data.blokbpkb_alamat);
                      $("#blokbpkb_noktp").val(data.blokbpkb_noktp);
                      $("#blokbpkb_nohp").val(data.blokbpkb_nohp);
                      $("#blokbpkb_ket").val(data.blokbpkb_ket); 

                      $(".btn-edit").attr('disabled',false);
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    } 
</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kddiv']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-6">
                                  <div class="form-group">
                                      <?php
                                          echo form_dropdown($form['kddiv']['name'],
                                                              $form['kddiv']['data'],
                                                              $form['kddiv']['value'],
                                                              $form['kddiv']['attr']);
                                          echo form_error('kddiv','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                                </div>
                            </div> 

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="table-responsive">
                            <table class="dataTable table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 100px;text-align: center;">No. Transaki</th>
                                        <th style="width: 20px;text-align: center;">No. Cetak</th>
                                        <th style="text-align: center;">Tgl Transaki</th>
                                        <th style="width: 20px;text-align: center;">No. Faktur</th>
                                        <th style="text-align: center;">Dari / Kepada</th>
                                        <th style="text-align: center;">Keterangan</th>
                                        <th style="text-align: center;">Jenis Transaki</th>
                                        <th style="text-align: center;">Nominal</th>
                                        <th style="width: 50px;text-align: center;"><i class="fa fa-th-large"></i></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <!--
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                -->
                            </table>
                        </div>
                    </div>
                  </div> 
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

      $('.btn-tampil').click(function() {
        tbl_json();
      });

      var kddiv = 'ZPH';
      var column = [];
      for (i = 7; i <= 7; i++) {
          if(i===7){
              column.push({
                  "bSortable": false,
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }else{
              column.push({
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }
      }
        column.push({
            "aTargets": [ 8 ],
            "bSortable": false,
        });

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
          "aoColumnDefs": column,
              "bProcessing": true,
              "bServerSide": true,
              "bDestroy": true,
              "bAutoWidth": false,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('postkbbkl/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            // "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "kddiv", "value": kddiv }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
        });


      // $('#kddiv').change(function(){
      //     table.ajax.reload();
      // });

    });

    function tbl_json(){ 
      var kddiv = $('#kddiv').val();
      var column = [];
      for (i = 7; i <= 7; i++) {
          if(i===7){
              column.push({
                  "bSortable": false,
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }else{
              column.push({
                  "aTargets": [ i ],
                  "mRender": function (data, type, full) {
                      return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return type === 'export' ? data : numeral(data).format('0,0.00');
                      // return formmatedvalue;
                      },
                  "sClass": "right"
                  });
          }
      }
        column.push({
            "aTargets": [ 8 ],
            "bSortable": false,
        });

        column.push({
            "aTargets": [ 2 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
          "aoColumnDefs": column,
              "bProcessing": true,
              "bServerSide": true,
              "bDestroy": true,
              "bAutoWidth": false,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('postkbbkl/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            // "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "kddiv", "value": kddiv }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
        }); 
    }

    function posting(nokb,nourut,kdrefkb){
      var kddiv = $('#kddiv').val();
        swal({
            title: "Konfirmasi",
            text: "Posting Kas/Bank "+nokb+" akan diproses.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            },

            function () {
                $.ajax({
                    type: "POST",
                      url: "<?=site_url('postkbbkl/proses');?>",
                      data: {"nokb":nokb,
                            "nourut":nourut,
                            "kdrefkb":kdrefkb,
                            "kddiv":kddiv},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                table.ajax.reload();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }

</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data' );
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">
            <form id="modalform" name="modalform" method="post">

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['kdleasing']['placeholder']);?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_dropdown( $form['kdleasing']['name'],
                                                  $form['kdleasing']['data'] ,
                                                  $form['kdleasing']['value'] ,
                                                  $form['kdleasing']['attr']);
                              echo form_error('kdleasing','<div class="note">','</div>');
                          ?> 
                      </div> 

                      <div class="col-md-4">
                        <div class="form-group"> 
                          <?php
                              echo form_input($form['kdprogleas']);
                              echo form_error('kdprogleas','<div class="note">','</div>');
                          ?> 
                        </div>
                      </div>

                      <div class="col-md-1 faktif">
                        <div class="form-group">
                          <?php echo "<u>Status</u>"; ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <div class="checkbox">
                              <?php
                                  echo form_checkbox($form['faktif']);
                              ?>
                          </div>
                      </div> 

                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['nmprogleas']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-3"> 
                          <?php
                              echo form_input($form['nmprogleas']);
                              echo form_error('nmprogleas','<div class="note">','</div>');
                          ?> 
                      </div> 

                      <div class="col-md-1">
                        <div class="form-group">
                          <?php echo "<b><u>TUNAI ?</u></b>"; ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <div class="checkbox">
                              <?php
                                  echo form_checkbox($form['tunai_leas']);
                              ?>
                          </div>
                      </div>  
                  </div>
              </div>  

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['ket']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                          <?php
                              echo form_input($form['ket']);
                              echo form_error('ket','<div class="note">','</div>');
                          ?> 
                      </div>  
                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <hr>
              </div>   

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['jp']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['jp']);
                              echo form_error('jp','<div class="note">','</div>');
                          ?> 
                      </div>  

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['status_ppn']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_dropdown( $form['status_ppn']['name'],
                                                  $form['status_ppn']['data'] ,
                                                  $form['status_ppn']['value'] ,
                                                  $form['status_ppn']['attr']);
                              echo form_error('status_ppn','<div class="note">','</div>');
                          ?> 
                      </div>  
                  </div>
              </div>   

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['matriks']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['matriks']);
                              echo form_error('matriks','<div class="note">','</div>');
                          ?> 
                      </div>  

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['dpp']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['dpp']);
                              echo form_error('dpp','<div class="note">','</div>');
                          ?> 
                      </div>  
                  </div>
              </div> 

              <div class="col-md-12 col-lg-12">
                  <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['tot_refund']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['tot_refund']);
                              echo form_error('tot_refund','<div class="note">','</div>');
                          ?> 
                      </div>  

                      <div class="col-md-2">
                        <div class="form-group">
                          <?php echo form_label($form['ppn']['placeholder']); ?>
                        </div>
                      </div>

                      <div class="col-md-2"> 
                          <?php
                              echo form_input($form['ppn']);
                              echo form_error('ppn','<div class="note">','</div>');
                          ?> 
                      </div>  
                  </div>
              </div> 
            </form>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="button" class="btn btn-primary btn-submit">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>
<script type="text/javascript">
    $(document).ready(function () {
        autonuminit();
        math();
        $('#tunai_leas').bootstrapToggle({
            on: 'AKTIF',
            off: 'TIDAK AKTIF',
            onstyle: 'success',
            offstyle: 'danger'
        }); 

        if(!$('#kdprogleas').val()){
            $('.faktif').hide();
            $('#faktif').hide(); 
            $('.btn-submit').click(function(){ 
              submit();  
            });
        } else if($('#kdprogleas').val()){
            $('.faktif').show();
            $('#faktif').show(); 
            $('#faktif').bootstrapToggle({
                on: 'AKTIF',
                off: 'TIDAK AKTIF',
                onstyle: 'success',
                offstyle: 'danger'
            }); 
            $('.btn-submit').click(function(){ 
              update();  
            });
        }  

        $('#status_ppn').change(function(){
            math();
        });
    });

    function autonuminit(){
        $('#jp').autoNumeric('init');
        $('#matriks').autoNumeric('init');
        $('#tot_refund').autoNumeric('init');
        $('#dpp').autoNumeric('init');
        $('#ppn').autoNumeric('init');
    } 

    function math(){
        var jp      = $('#jp').autoNumeric('get'); 
        var matriks = $('#matriks').autoNumeric('get');  
        var status_ppn = $('#status_ppn').val();
        if(jp>0 & matriks>0){
          var total = parseFloat(jp) + parseFloat(matriks);
          if(!isNaN(total)){
            $('#tot_refund').autoNumeric('set',total); 
            if(status_ppn==='I'){
              var bagi = parseFloat(total) / 1.1;
              var dpp = Math.round(bagi);
              if(!isNaN(dpp)){
                $('#dpp').autoNumeric('set',dpp);
                var ppn = parseFloat(total) - parseFloat(dpp);
                if(!isNaN(ppn)){
                  $('#ppn').autoNumeric('set',ppn);
                }
              }
            } else {
              $('#dpp').autoNumeric('set',0);
              $('#ppn').autoNumeric('set',0);
            }
          }
        } else if(jp>0 & matriks===''){
          var matriks = 0;
          var total = parseFloat(jp) + parseFloat(matriks);
          if(!isNaN(total)){
            $('#tot_refund').autoNumeric('set',total); 
            if(status_ppn==='I'){
              var bagi = parseFloat(total) / 1.1;
              var dpp = Math.round(bagi);
              if(!isNaN(dpp)){
                $('#dpp').autoNumeric('set',dpp);
                var ppn = parseFloat(total) - parseFloat(dpp);
                if(!isNaN(ppn)){
                  $('#ppn').autoNumeric('set',ppn);
                }
              }
            } else {
              $('#dpp').autoNumeric('set',0);
              $('#ppn').autoNumeric('set',0);
            }
          }
        } else if(jp==='' & matriks>0){
          var jp = 0;
          var total = parseFloat(jp) + parseFloat(matriks);
          if(!isNaN(total)){
            $('#tot_refund').autoNumeric('set',total); 
            if(status_ppn==='I'){
              var bagi = parseFloat(total) / 1.1;
              var dpp = Math.round(bagi);
              if(!isNaN(dpp)){
                $('#dpp').autoNumeric('set',dpp);
                var ppn = parseFloat(total) - parseFloat(dpp);
                if(!isNaN(ppn)){
                  $('#ppn').autoNumeric('set',ppn);
                }
              }
            } else {
              $('#dpp').autoNumeric('set',0);
              $('#ppn').autoNumeric('set',0);
            }
          }
        } else {
          $('#tot_refund').val(''); 
        } 
    }  

    function submit(){
        var kdleasing   = $("#kdleasing").val();
        var nmprogleas  = $("#nmprogleas").val().toUpperCase();
        var ket         = $("#ket").val().toUpperCase();
        var jp          = $("#jp").autoNumeric('get');
        var matriks     = $("#matriks").autoNumeric('get');
        var status_ppn  = $("#status_ppn").val();
        if ($("#tunai_leas").prop("checked")){
          var tunai_leas = 'true';
        } else {
          var tunai_leas = 'false';
        } 
        //tunai_leas(nmaks);
      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstaddprogleas/submit");?>",
      		data: {"kdleasing":kdleasing,"nmprogleas":nmprogleas,"ket":ket,"jp":jp,"matriks":matriks,"status_ppn":status_ppn,"tunai_leas":tunai_leas},
      		success: function(resp){
            var obj = jQuery.parseJSON(resp);
            if(obj.state==='1'){
              swal({
                title: 'Data Berhasil Disimpan',
                text: obj.msg,
                type: 'success'
              }, function(){
                window.location.href = '<?=site_url('mstaddprogleas');?>';
              }); 
            }else{ 
              swal({
                title: 'Data Gagal Disimpan',
                text: obj.msg,
                type: 'error'
              }); 
            }
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function update(){
        var kdprogleas  = $("#kdprogleas").val();
        var kdleasing   = $("#kdleasing").val();
        var nmprogleas  = $("#nmprogleas").val().toUpperCase();

        var jp          = $("#jp").autoNumeric('get');
        var matriks     = $("#matriks").autoNumeric('get');
        var status_ppn  = $("#status_ppn").val(); 
        var ket         = $("#ket").val().toUpperCase();

        if ($("#tunai_leas").prop("checked")){
          var tunai_leas = 'true';
        } else {
          var tunai_leas = 'false';
        } 
        if ($("#faktif").prop("checked")){
          var faktif = 't';
        } else {
          var faktif = 'f';
        }

      	$.ajax({
      		type: "POST",
      		url: "<?=site_url("mstaddprogleas/update");?>",
      		data: {"kdprogleas":kdprogleas,
                  "kdleasing":kdleasing,
                  "nmprogleas":nmprogleas,
                  "ket":ket,
                  "jp":jp,
                  "matriks":matriks,
                  "status_ppn":status_ppn,
                  "tunai_leas":tunai_leas,
                  "faktif":faktif}, 
      		success: function(resp){
      			var obj = jQuery.parseJSON(resp);
      			$.each(obj, function(key, data){
                  swal({
                      title: data.title,
                      text: data.msg,
                      type: data.tipe
                  }, function(){
                    if (data.tipe==="success"){
                      window.location.href = '<?=site_url('mstaddprogleas');?>';
                    }
                  });
      			});
          },
          error:function(event, textStatus, errorThrown) {
          	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
        });
    }

    function refresh(){
      window.location.reload();
    }
</script>

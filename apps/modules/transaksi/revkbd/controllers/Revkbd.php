<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Revkbd
 *
 * @author
 */

class Revkbd extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('revkbd/submit'),
            'add' => site_url('revkbd/add'),
            'edit' => site_url('revkbd/edit'),
            'reload' => site_url('revkbd'),
        );

        $this->load->model('revkbd_qry');
       
        $jnstrx = $this->revkbd_qry->getjnstrans();
        foreach ($jnstrx as $value) {
            $this->data['jnstrx'][$value['jnstrx']] = $value['jnstrx'];
        }

        $this->load->model('revkbd_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
        }

    public function getKdRef() {
        echo $this->revkbd_qry->getKdRef();
    }

    public function proses_dk() {
        echo $this->revkbd_qry->proses_dk();
    }

    public function getjnskredit(){
        echo $this->revkbd_qry->getjnskredit();
    }

    public function getDataTrx(){
        echo $this->revkbd_qry->getDataTrx();
    }

    public function set_jenis() {
        echo $this->revkbd_qry->set_jenis();

    }

    public function getData() {
        echo $this->revkbd_qry->getData();

    }

    public function proses(){
        echo $this->revkbd_qry->proses();

    }

    public function submit() {
        $nokb = $this->input->post('nokb');
        $nourut = $this->input->post('nourut');

        if($this->validate() == TRUE){
            $res = $this->revkbd_qry->submit();
            if(empty($nourut)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($nokb)){
                        $this->_check_id();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_init_edit($nokb);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($nokb)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_check_id($nokb);
                $this->template->build('form', $this->data);
            }
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
           'nokb'=> array(
                    'placeholder' => 'Nomor Transaksi',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    //'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),

        );
    }

    private function _init_edit($nokb = null){
        if(!$nokb){
            $nokb = $this->uri->segment(3);
            $nourut = $this->uri->segment(4);
        }
        $this->_check_id($nokb,$nourut);
        $this->data['form'] = array(
           'nokb'=> array(
                    'placeholder' => 'No. Kas',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => $this->val[0]['nokb'],
                    'class'       => 'form-control',
                    'readonly'    => ''
            ),
           'nourut'=> array(
                    'type'         => 'hidden',
                    'placeholder' => 'No. Urut',
                    'id'           => 'nourut',
                    'value'       => $this->val[0]['nourut'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nocetak'=> array(
                    'placeholder' => 'No. Cetak',
                    'id'           => 'nocetak',
                    'value'       => $this->val[0]['nocetak'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),

            'tglkb'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglkb',
                    'name'        => 'tglkb',
                    'value'       => $this->val[0]['tglkb'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => ''
            ),
           'jenis'=> array(
                    'placeholder' => 'Jenis Transaksi',
                    'attr'        => array(
                        'id'      =>'jenis',
                        'class'   =>'form-control select2',
                    ),
                    'name'        => 'jenis',
                    'data'        => '',
                    'value'       => set_value('jenis'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),
           'nofaktur'=> array(
                    'placeholder' => 'No. Faktur',
                    'id'      => 'nofaktur',
                    'name'        => 'nofaktur',
                    'value'       => $this->val[0]['nofaktur'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'drkpd'=> array(
                    'placeholder' => 'Dari/Kepada',
                    'id'      => 'drkpd',
                    'name'        => 'drkpd',
                    'value'       => $this->val[0]['drkpd'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'    => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['ket'],
                    'name'     => 'ket',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'debet'=> array(
                    'placeholder' => 'Debet',
                    'id'    => 'debet',
                    'class' => 'form-control',
                    'value'    => number_format($this->val[0]['debet'],2,",","."),
                    'name'     => 'debet',
                    // 'readonly'    => '',
            ),
            'kredit'=> array(
                    'placeholder' => 'Kredit',
                    'id'    => 'kredit',
                    'class' => 'form-control',
                    'value'    => number_format($this->val[0]['kredit'],2,",","."),
                    'name'     => 'kredit',
                    'readonly'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
        );
    }
    private function _check_id($nokb,$nourut){
        if(empty($nokb)){
            redirect($this->data['edit']);
        }

        $this->val= $this->revkbd_qry->select_data($nokb,$nourut);

        if(empty($this->val)){
            redirect($this->data['edit']);
        }
    }

    private function validate($nokb,$nourut) {
      if(!empty($nokb) && !empty($nourut)){
          return true;
      }
      $config = array(
            array(
                    'field' => 'nokb',
                    'label' => 'No. Kas Bank',
                    'rules' => 'required|max_length[10]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <?php 
                            echo form_label($form['periode_awal']['placeholder']);
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>
                <div class="table-responsive">
                <table style="width: 1800px;"  class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="width: 10px;text-align: center;">No. Mesin</th>
                            <th style="width: 10px;text-align: center;">No. Rangka</th>
                            <th style="width: 10px;text-align: center;">Kode</th>
                            <th style="text-align: center;">Tipe</th>
                            <th style="text-align: center;">Jenis</th>
                            <th style="width: 10px;text-align: center;">Kode Warna</th>
                            <th style="text-align: center;">Warna</th>
                            <th style="width: 10px;text-align: center;">No. Faktur</th>
                            <th style="width: 10px;text-align: center;">Tgl Faktur</th>
                            <th style="width: 10px;text-align: center;">Tgl Terima</th>
                            <th style="width: 10px;text-align: center;">Umur (Hari)</th>
                            <th style="width: 10px;text-align: center;">Nilai (DPP)</th>
                            <!--
                            <th style="width: 10px;text-align: center;">PPN</th>
                            <th style="width: 10px;text-align: center;">TOTAL</th>
                            -->
                            <th style="text-align: center;">Lokasi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">NO. MESIN</th>
                            <th style="text-align: center;">NO. RANGKA</th>
                            <th style="text-align: center;">KODE</th>
                            <th style="text-align: center;">TIPE</th>
                            <th style="text-align: center;">JENIS</th>
                            <th style="text-align: center;">KODE WARNA</th>
                            <th style="text-align: center;">WARNA</th>
                            <th style="text-align: center;">NO. FAKTUR</th>
                            <th style="text-align: center;">TGL FAKTUR</th>
                            <th style="text-align: center;">TGL TERIMA</th>
                            <th style="text-align: center;">UMUR/HARI</th>
                            <th style="text-align: center;">HARGA</th>
                            <!--
                            <th style="text-align: center;">PPN</th>
                            <th style="text-align: center;">TOTAL</th>
                            -->
                            <th style="text-align: center;">LOKASI</th>
                        </tr>                      
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //event.preventDefault();

        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        var column = [];

        column.push({ 
            "aTargets": [ 10,11 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
                // return formmatedvalue;
                },
            "sClass": "right"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "order": [[ 8 , "asc" ]],
            "bPaginate": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( 
                             { "name": "periode_awal", "value": $("#periode_awal").val() }
                            ,{ "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptstokunit/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    });
</script>
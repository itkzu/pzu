<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style type="text/css"> 
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    } 
</style>
<div id="load">Loading...</div> 
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <div class="col-md-2">
                    <a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
                    <a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a> 
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-1">
                    <label for="exampleInputFile">Import NIK </label>
                </div>
                <div class="col-md-6">
                    <div class="fileinputnik fileinput-new input-group" name="fileinputnik" id="fileinputnik" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Cari...</span>
                            <span class="fileinput-exists">Ubah</span>
                            <input type="file" name="filenik" id="filenik" data-original-value="" required accept=".xls, .xlsx">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
                        <span class="input-group-btn fileinput-exists">
                            <button class="btn btn-primary" name="importnik" id="importnik"><i class="fa fa-cloud-upload"></i> Import</button>
                        </span>
                    </div>
                </div> 
            </div> 
            <div class="box-tools pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-wrench"></i></button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="<?php echo $add;?>" >Tambah Data Baru</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="btn-refersh">Refresh</a>
                        </li> 
                    </ul>
                
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
                <div class="table-responsive">
                <table class="dataTable table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th style="width: 50px;text-align: center;">NIK</th>
                            <th style="width: 100px;text-align: center;">Nama Lengkap</th>
                            <th style="text-align: center;">Nama Cetak</th>
                            <th style="text-align: center;">Jabatan</th>
                            <th style="text-align: center;">Atasan</th>
                            <th style="text-align: center;">Pos/Cabang</th>
                            <th style="text-align: center;">Tgl Masuk</th>
                            <th style="text-align: center;">Tgl Keluar</th>
                            <th style="text-align: center;">Status Aktif</th>
                            <th style="width: 10px;text-align: center;">Edit</th>
                            <th style="width: 10px;text-align: center;">Hapus</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 50px;text-align: center;">NIK</th>
                            <th style="width: 100px;text-align: center;">Nama Lengkap</th>
                            <th style="text-align: center;">Nama Cetak</th>
                            <th style="text-align: center;">Jabatan</th>
                            <th style="text-align: center;">Atasan</th>
                            <th style="text-align: center;">Pos/Cabang</th>
                            <th style="text-align: center;">Tgl Masuk</th>
                            <th style="text-align: center;">Tgl Keluar</th>
                            <th style="text-align: center;">Status Aktif</th>
                            <th style="width: 10px;text-align: center;">Edit</th>
                            <th style="width: 10px;text-align: center;">Hapus</th>
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
        $(".btn-refersh").click(function(){
            table.ajax.reload();
        });
        
        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],
            "columnDefs": [
                { "orderable": false, "targets": 9 },
                { "orderable": false, "targets": 10 }
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('transalesin/json_dgview');?>",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            }
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="Set Privilege"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //import data Pembelian
        $('#importnik').click(function(event){
            $("#load").show();
            event.preventDefault();
            var formData = new FormData();
            formData.append('file', $('input[type=file]')[0].files[0]);
            formData.append('kddiv',  $("#kddiv").val());

            $.ajax({
                url:"<?php echo base_url(); ?>transalesin/importnik",
                method:"POST",
                data:formData,
                dataType:'json',
                contentType:false,
                cache:false,
                processData:false, 
                success:function(resp){

                    var obj = resp;
                    $.each(obj, function(key, data){
                            if (data.tipe==="success"){
                                $("#load").fadeOut();
                                swal({
                                    title: data.title,
                                    text: data.msg,
                                    type: data.tipe
                                }, function(){
                                    $('.fileinputnik').fileinput('clear');
                                    $('#importnik').attr('disabled', false);
                                });
                            } else {
                                $("#load").fadeOut();
                                swal({
                                    title: "Data Kosong",
                                    text: "Empty",
                                    type: "error"
                                }, function(){
                                    $('.fileinputnik').fileinput('clear');
                                    $('#importnik').attr('disabled', false);
                                });

                            }
                        });
                }
            })
        });
    });
    
    function refresh(){
        table.ajax.reload();
    }
    
    function deleted(kdsales){
        swal({
            title: "Konfirmasi Hapus !",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var submit = "<?php echo $submit;?>"; 
                $.ajax({
                    type: "POST",
                    url: submit,
                    data: {"kdsales":kdsales,"stat":"delete"},
                    success: function(resp){   
                        var obj = jQuery.parseJSON(resp);
                        if(obj.state==="1"){
                            table.ajax.reload();
                            swal({
                                title: "Terhapus",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                location.reload();
                            });
                        }else{
                            swal("Terhapus", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
</script>
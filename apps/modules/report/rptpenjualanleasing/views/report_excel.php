<?php

/* 
 * ***************************************************************
 * Script : pungutan_bank_excel
 * Version : 
 * Date : May 5, 2017 | 9:23:07 PM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

require_once APPPATH.'libraries/PHPExcel.php';
$objPHPExcel = new PHPExcel(); 
$idxsheet = 0;   
define('FORMAT_INDONESIA_CURRENCY', '#,##0.00');
define('FORMAT_INDONESIA_NUMBER', '#,##');
define('FORMAT_INDONESIA_DATE','dd-mmmm-yyyy');

$array = $this->input->post();

$namafile = "REKAP PENJUALAN PER LEASING ".  strtoupper(month(date('m'))) ." ".date('Y');
$objWorkSheet = $objPHPExcel->createSheet($idxsheet);
$objWorkSheet->setTitle(strtoupper(month(date('m')))." ".date('Y'));      
if(count($rpt['header'])>0){  
    // Kolom Header Start 
    $objWorkSheet->setCellValueByColumnAndRow(0, 1, strtoupper($this->apps->title) . $this->session->userdata('data')['cabang'])
            ->mergeCellsByColumnAndRow(0, 1, 8,1);
    $objWorkSheet->getStyleByColumnAndRow(0, 1)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
    $objWorkSheet->getRowDimension(1)->setRowHeight(18);
    
    $objWorkSheet->setCellValueByColumnAndRow(0, 2, 'REKAPITULASI PENJUALAN PERIODE' 
                                                    .' ' . $this->apps->dateConvert($array['periode_awal'])
                                                    .' s/d ' . $this->apps->dateConvert($array['periode_akhir'])
                                             )
                ->mergeCellsByColumnAndRow(0, 2, 8,2);
    $objWorkSheet->getStyleByColumnAndRow(0, 2)
                        ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
    $objWorkSheet->getRowDimension(2)->setRowHeight(18);
    
//    $objWorkSheet->setCellValueByColumnAndRow(0, 3, PHPExcel_Shared_Date::FormattedPHPToExcel(date('Y'),date('m'),date('d')))
//            ->mergeCellsByColumnAndRow(0, 3, 8,3);
//    $objWorkSheet->getStyleByColumnAndRow(0, 3)
//            ->getNumberFormat()
//            ->setFormatCode(FORMAT_INDONESIA_DATE); 

    // Generate Header Laporan Start
    $colHeader = 0;            
    $namaheader = array(
        'NO',
        'DESKRIPSI',
        'TYPE',
        'KODE',
        'T',
    );

    $rowheader = 4;
    foreach($rpt['header'] as $key => $val){
        array_push($namaheader, $val['nmleasing']);
    }
    array_push($namaheader, "TOTAL");
    foreach ($namaheader as $val) {
        $objWorkSheet->getRowDimension($rowheader)->setRowHeight(18);
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
        $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader)
                        ->getFont()
                            ->setBold(true);
        $objWorkSheet->setCellValueByColumnAndRow($colHeader, $rowheader, $val);  
        if($val==="NO" || $val==="DESKRIPSI" || $val==="TYPE" || $val==="KODE" || $val==="T" || $val==="TOTAL"){
            $objWorkSheet->mergeCellsByColumnAndRow($colHeader, $rowheader, $colHeader,$rowheader+1);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader, $colHeader,$rowheader+1)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setAutoSize(true); 
        }else{          
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader, $colHeader + 3,$rowheader)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            $objWorkSheet->mergeCellsByColumnAndRow($colHeader, $rowheader, $colHeader + 3,$rowheader);
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader+1)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader+1)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
            $objWorkSheet->getStyleByColumnAndRow($colHeader, $rowheader+1)
                            ->getFont()
                                ->setBold(true);
            $objWorkSheet->setCellValueByColumnAndRow($colHeader, $rowheader+1, '1 TH');  
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(5); 
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+1, $rowheader+1)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+1, $rowheader+1)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
            $objWorkSheet->getStyleByColumnAndRow($colHeader+1, $rowheader+1)
                            ->getFont()
                                ->setBold(true);
            $objWorkSheet->setCellValueByColumnAndRow($colHeader+1, $rowheader+1, '2 TH'); 
            $objWorkSheet->getColumnDimensionByColumn($colHeader+1)->setWidth(5); 
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+2, $rowheader+1)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+2, $rowheader+1)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
            $objWorkSheet->getStyleByColumnAndRow($colHeader+2, $rowheader+1)
                            ->getFont()
                                ->setBold(true);
            $objWorkSheet->setCellValueByColumnAndRow($colHeader+2, $rowheader+1, '3 TH'); 
            $objWorkSheet->getColumnDimensionByColumn($colHeader+2)->setWidth(5); 
            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+3, $rowheader+1)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);            
            $objWorkSheet->getStyleByColumnAndRow($colHeader+3, $rowheader+1)
                            ->getBorders()
                                ->getAllBorders()
                                    ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);             
            $objWorkSheet->getStyleByColumnAndRow($colHeader+3, $rowheader+1)
                            ->getFont()
                                ->setBold(true);
            $objWorkSheet->setCellValueByColumnAndRow($colHeader+3, $rowheader+1, '4 TH'); 
            $objWorkSheet->getColumnDimensionByColumn($colHeader+3)->setWidth(5); 
            
            $colHeader = $colHeader + 3;
        }
        
        if($colHeader===0){
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(5);    
        }elseif($colHeader===1){
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(60);    
        }else{
            $objWorkSheet->getColumnDimensionByColumn($colHeader)->setWidth(7);    
        }
        $colHeader++;
    }
    $rowDetail = $rowheader + 2;
    $nomor = 1;
    
    foreach($rpt['footer_leasing'] as $val){
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail,3,$rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);          
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail,3,$rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowDetail, 'TOTAL PER LEASING')
                    ->mergeCellsByColumnAndRow(0, $rowDetail,3,$rowDetail); 
        
        $subtotal = 0;
        $colDetail = 4;
        foreach ($val as $key => $v_kolom) {
            if($colDetail>4){
                $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                                    ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);  
                $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $v_kolom)
                                ->mergeCellsByColumnAndRow($colDetail, $rowDetail, $colDetail+3,$rowDetail);         
                $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail, $colDetail+3,$rowDetail)
                                ->getBorders()
                                    ->getAllBorders()
                                        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                $colDetail = $colDetail+4;
            }else{
                
                $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                                ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);   
                $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $v_kolom)
                                ->mergeCellsByColumnAndRow($colDetail, $rowDetail, $colDetail,$rowDetail);        
                $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                                ->getBorders()
                                    ->getAllBorders()
                                        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
                
                $colDetail++;
            }
            $subtotal = $subtotal + $v_kolom;
        }
        
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $subtotal); 
        
        $rowDetail++;
    }
    
    foreach($rpt['footer'] as $val){
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail,3,$rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);          
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail,3,$rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowDetail, 'TOTAL')
                    ->mergeCellsByColumnAndRow(0, $rowDetail,3,$rowDetail); 
        
        $subtotal = 0;
        $colDetail = 4;
        foreach ($val as $key => $v_kolom) {
            $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                                ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
            $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $v_kolom);          
            $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            if($colDetail>1){
                $subtotal = $subtotal + $v_kolom;
            }
            $colDetail++;
        }
        
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $subtotal); 
        
        $rowDetail++;
    }
    
    foreach($rpt['detail'] as $val){
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow(0, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow(0, $rowDetail, $nomor); 
        
        $subtotal = 0;
        $colDetail = 1;
        foreach ($val as $key => $v_kolom) {
            $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                                ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
            $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $v_kolom);          
            $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                        ->getBorders()
                            ->getAllBorders()
                                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            if($colDetail>1){
                $subtotal = $subtotal + $v_kolom;
            }
            $colDetail++;
        }
        
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                            ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);          
        $objWorkSheet->getStyleByColumnAndRow($colDetail, $rowDetail)
                    ->getBorders()
                        ->getAllBorders()
                            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
        $objWorkSheet->setCellValueByColumnAndRow($colDetail, $rowDetail, $subtotal); 
        
        $rowDetail++;
        $nomor++;
    }
    
    $row  = $rowDetail + 1;
    
    // Kepala Administrasi Start
    $objWorkSheet->setCellValueByColumnAndRow(0, $row, "Diperiksa Oleh :"); 
    $objWorkSheet->mergeCellsByColumnAndRow(0, $row, 1, $row);  

    $objWorkSheet->setCellValueByColumnAndRow(0, $row + 1, "Kepala Administrasi"); 
    $objWorkSheet->mergeCellsByColumnAndRow(0, $row + 1, 1, $row + 1);   
    

    $objWorkSheet->setCellValueByColumnAndRow(0, $row + 9, $this->apps->kepala_administrasi); 
    $objWorkSheet->getStyleByColumnAndRow(0, $row + 9)
                    ->getFont()
                        ->setBold(true)
                        ->setUnderline(true);    
    $objWorkSheet->mergeCellsByColumnAndRow(0, $row + 9, 1, $row + 9); 
    // Kepala Administrasi End
    
    
    
    // Kepala Administrasi Start
    $objWorkSheet->setCellValueByColumnAndRow(4, $row, "Mengetahui,"); 
    $objWorkSheet->mergeCellsByColumnAndRow(4, $row, 11, $row);  

    $objWorkSheet->setCellValueByColumnAndRow(4, $row + 1, "Kepala Cabang"); 
    $objWorkSheet->mergeCellsByColumnAndRow(4, $row + 1, 11, $row + 1);   
    

    $objWorkSheet->setCellValueByColumnAndRow(4, $row + 9, $this->apps->kepala_cabang); 
    $objWorkSheet->getStyleByColumnAndRow(4, $row + 9)
                    ->getFont()
                        ->setBold(true)
                        ->setUnderline(true);    
    $objWorkSheet->mergeCellsByColumnAndRow(4, $row + 9, 11, $row + 9); 
    // Kepala Administrasi End
    
    // Admin Penjualan Start
    $objWorkSheet->setCellValueByColumnAndRow(19, $row, "Semarang, ".date('d')." ".month(date('m')) ." ".date('Y')); 
    $objWorkSheet->mergeCellsByColumnAndRow(19, $row, 24, $row);  

    $objWorkSheet->setCellValueByColumnAndRow(19, $row + 1, "Admin Penjualan"); 
    $objWorkSheet->mergeCellsByColumnAndRow(19, $row + 1, 24, $row + 1);   
    

    $objWorkSheet->setCellValueByColumnAndRow(19, $row + 9, $this->apps->admin_penjualan); 
    $objWorkSheet->getStyleByColumnAndRow(19, $row + 9)
                    ->getFont()
                        ->setBold(true)
                        ->setUnderline(true);    
    $objWorkSheet->mergeCellsByColumnAndRow(19, $row + 9, 24, $row + 9); 
    // Admin Penjualan End
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$namafile.'.xls"'); 
        header('Cache-Control: max-age=0'); 
$objWriter->save('php://output'); 
exit();   
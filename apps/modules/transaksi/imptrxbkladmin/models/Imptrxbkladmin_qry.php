<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Imptrxbkladmin_qry
 *
 * @author
 */

class Imptrxbkladmin_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_divisi_bkl()");
        return $q->result_array();
    }
    public function insertBeli($data)
    { 
        if(count($data)>0){
            $this->db->empty_table('bkl.t_po_tmp');
            $this->db->insert_batch('bkl.t_po_tmp', $data);
        }

        // echo $this->db->last_query();
        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from bkl.po2_ins('".$this->session->userdata('data')['kddiv']."', '".$this->session->userdata('username')."')");
                // echo $this->db->last_query();
            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
                return 'Error! '.$this->db->_error_message();
        }

        return json_encode($res);
    }

    public function insertServis($data)
    {
        if(count($data)>0){
            $this->db->empty_table('bkl.t_sv_tmp');
            $this->db->insert_batch('bkl.t_sv_tmp', $data);
        }

        // echo json_encode($data);
        if ($this->db->affected_rows() > 0){
                  $q = $this->db->query("select title,msg,tipe from bkl.sv2_ins('".$this->session->userdata('data')['kddiv']."','". $this->session->userdata('username') ."')");
                // echo $this->db->last_query();
                  if($q->num_rows()>0){
                      $res = $q->result_array();
                  }else{
                      $res = "";
                  }
        } else{
            return 'Error! '.$this->db->_error_message();
        }
        return json_encode($res);
    }

    public function insertJual($data)
    {
        if(count($data)>0){
            $this->db->empty_table('bkl.t_so_tmp');
            $this->db->insert_batch('bkl.t_so_tmp', $data);
        }
//        echo $this->db->last_query();
//        echo json_encode($data);
        if ($this->db->affected_rows() > 0){
            $q = $this->db->query("select title,msg,tipe from bkl.so2_ins('".$this->session->userdata('data')['kddiv']."','". $this->session->userdata('username') ."')");
           // echo $this->db->last_query();
            if($q->num_rows()>0){
                $res = $q->result_array();
            }else{
                $res = "";
            }
        } else{
            return 'Error! '.$this->db->_error_message();
        }
        return json_encode($res);
    }
}

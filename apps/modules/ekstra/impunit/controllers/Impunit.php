<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Impunit
 *
 * @author
 */

class Impunit extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('impunit/submit'),
            'add' => site_url('impunit/add'),
            'edit' => site_url('impunit/edit'),
            'reload' => site_url('impunit'),
        );
        $this->load->model('impunit_qry');
        require_once APPPATH.'libraries/PHPExcel.php';
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        //$this->impunit_qry->submit();
        //redirect("impumsl");
    }

    public function importxls() {
        //if(isset($_FILES["file"]["name"])){
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
 
                for($row=2; $row<=$highestRow; $row++){
                    $desk = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $kode = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $tipe = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $harga = $worksheet->getCellByColumnAndRow(4, $row)->getValue();

                    if(!empty($kode)){  //jika kode tidak null -> simpan

                        //jika deskripsi, tipe, harga = null
                        if(empty($desk)){$desk = '';}
                        if(empty($tipe)){$tipe = '';}
                        if(empty($harga)){$harga = '';}

                        $data[] = array(
                            'desk'  => $desk,
                            'kode'  => $kode,
                            'tipe'  => $tipe,
                            'harga' => $harga
                        );
                    }
                }
            }
            return $data;           
        //} 
    }

    public function importBeli() {
        if(isset($_FILES["file"]["name"])){
            /*
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
 
                for($row=2; $row<=$highestRow; $row++){
                    $desk = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $kode = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $tipe = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $harga = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $data[] = array(
                        'desk'  => $desk,
                        'kode'   => $kode,
                        'tipe'    => $tipe,
                        'harga'  => $harga
                    );
                }

            }
            */
            $data = $this->importxls();
            $res = $this->impunit_qry->insertBeli($data);
            //echo 'Data Harga Beli Unit sukses di Import';
            echo $res;
        } 
    }
    
    public function importONTR() {
        if(isset($_FILES["file"]["name"])){

            $data = $this->importxls();
            $res = $this->impunit_qry->insertONTR($data);
            //echo 'Data Harga ON-The Road Unit sukses di Import';
            echo $res;
        } 
    }
    
    public function importOFFTR() {
        if(isset($_FILES["file"]["name"])){

            $data = $this->importxls();
            $res = $this->impunit_qry->insertOFFTR($data);
            //echo json_encode($data);
            echo $res;
        } 
    }
}
<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">

                <div class="table-responsive">
                    <table class="dataTable table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th style="width: 100px;text-align: center;">No. DO</th>
                                <th style="text-align: center;">Tgl DO</th>
                                <th style="text-align: center;">No. SPK</th>
                                <th style="text-align: center;">Nama Pemesan</th>
                                <th style="text-align: center;">Nama STNK</th>
                                <th style="text-align: center;">Leasing</th>
                                <th style="text-align: center;">Program</th>
                                <th style="width: 50px;text-align: center;"><i class="fa fa-th-large"></i></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <!--
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        -->
                    </table>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            //"lengthMenu": [[25,50,100,-1], ["25","50","100","Semua Data"]],

            "lengthMenu": [[-1], ["Semua Data"]],
            "columnDefs": [
                { "orderable": false, "targets": 7 }
            ],
            "order": [[ 1, "asc" ] , [ 0 , "asc" ]],

            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('postdo/json_dgview');?>",
            //"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6 text-left'i>r> t <'row'<'col-sm-6'i>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ...",
                "sInfo": "<b>Total Data  =  _TOTAL_ </b>"
            }
        });
        
    });

    function posting(nodo){
        swal({
            title: "Konfirmasi",
            text: "Posting Penjualan "+nodo+" akan diproses.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            }, 
            
            function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('postdo/proses');?>",
                    data: {"nodo":nodo},
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                table.ajax.reload();
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }

</script>
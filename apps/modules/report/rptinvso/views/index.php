<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<div class="row">

					<div class="table-responsive">
						<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#example2-tab1" aria-controls="example2-tab1" role="tab" data-toggle="tab">Per Penjualan</a></li>
								<li role="presentation"><a href="#example2-tab2" aria-controls="example2-tab2" role="tab" data-toggle="tab">Multi Penjualan</a></li>
	 					</ul>

						<!-- Tab panes -->
						<div class="tab-content">
					 			<div role="tabpanel" class="tab-pane fade in active" id="example2-tab1">
										
									<div class="col-xs-12">
										<div class="form-group has-error">
											<div class="col-xs-6">
												<div class="form-group has-error">
													<?php
													echo form_label('Masukkan Kata Kunci <small><i>( No. DO / No. SPK / Nama Customer )</i></small>');
													echo form_dropdown($form['nama']['name']
														,$form['nama']['data']
														,$form['nama']['value']
														,$form['nama']['attr']);
													echo form_error('nama','<div class="note">','</div>');
													?>
												</div>
											</div>
											<div class="col-xs-2"> 
												<div class="form-group has-error">  
														<br> 
						                            <a class="btn btn-success down-pdf" target="_blank">
						                              <i class="fa fa-save"></i> Download PDV
						                            </a>
						                        </div>
											</div>
											<div class="col-xs-2"> 
												<div class="form-group has-error">  
														<br> 
						                            <a class="btn btn-primary ctk-inv" target="_blank" >
						                              <i class="fa fa-print"></i> Cetak Invoice
						                            </a>
						                        </div>
											</div> 
										</div>
									</div>

									<div class="col-xs-12">
										<div class="row">

											<div class="col-xs-2 col-md-2 ">
												<div class="row">

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['nodo']['placeholder']);
															echo form_input($form['nodo']);
															echo form_error('nodo','<div class="note">','</div>');
															?>
														</div>
													</div>

													<div class="col-xs-12 ">
														<div class="form-group">
															<?php
															echo form_label($form['noso']['placeholder']);
															echo form_input($form['noso']);
															echo form_error('noso','<div class="note">','</div>');
															?>
														</div>
													</div>   

												</div>
											</div> 

											<div class="col-xs-2 col-md-2 ">
												<div class="row">

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['tgldo']['placeholder']);
															echo form_input($form['tgldo']);
															echo form_error('tgldo','<div class="note">','</div>');
															?>
														</div>
													</div>

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['tglso']['placeholder']);
															echo form_input($form['tglso']);
															echo form_error('tglso','<div class="note">','</div>');
															?>
														</div>
													</div>  

												</div>
											</div>



											<div class="col-xs-4 col-md-4 ">
												<div class="row">

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['kdtipe']['placeholder']);
															echo form_input($form['kdtipe']);
															echo form_error('kdtipe','<div class="note">','</div>');
															?>
														</div>
													</div>

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['nosin']['placeholder']);
															echo form_input($form['nosin']);
															echo form_error('nosin','<div class="note">','</div>');
															?>
														</div>
													</div>  

												</div>
											</div>



											<div class="col-xs-4 col-md-4 ">
												<div class="row">

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['nmtipe']['placeholder']);
															echo form_input($form['nmtipe']);
															echo form_error('nmtipe','<div class="note">','</div>');
															?>
														</div>
													</div>

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['nora']['placeholder']);
															echo form_input($form['nora']);
															echo form_error('nora','<div class="note">','</div>');
															?>
														</div>
													</div>  

												</div>
											</div>
										</div>
									</div> 

									<div class="col-xs-12">
										<div class="row">

											<div class="col-xs-2 col-md-2 ">
												<div class="row"> 

													<div class="col-xs-12 ">
														<div class="form-group">
															<?php
															echo form_label($form['status']['placeholder']);
															echo form_input($form['status']);
															echo form_error('status','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>



											<div class="col-xs-2 col-md-2 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['hrg']['placeholder']);
															echo form_input($form['hrg']);
															echo form_error('hrg','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>



											<div class="col-xs-2 col-md-2 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['disc']['placeholder']);
															echo form_input($form['disc']);
															echo form_error('disc','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>



											<div class="col-xs-3 col-md-3 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['dpp']['placeholder']);
															echo form_input($form['dpp']);
															echo form_error('dpp','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>



											<div class="col-xs-3 col-md-3 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['ppn']['placeholder']);
															echo form_input($form['ppn']);
															echo form_error('ppn','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>
										</div>
									</div> 

									<div class="col-xs-12">
										<div class="row"> 

											<div class="col-xs-2">
												<div class="form-group">
													<?php
														echo form_label($form['nmcust']['placeholder']); 
													?>
												</div>
											</div> 

											<div class="col-xs-10">
												<div class="form-group">
													<?php 
														echo form_input($form['nmcust']);
														echo form_error('nmcust','<div class="note">','</div>');
													?>
												</div>
											</div> 

										</div> 
									</div>

									<div class="col-xs-12">
										<div class="row"> 

											<div class="col-xs-2">
												<div class="form-group">
													<?php
														echo form_label($form['alamat']['placeholder']);
													?>
												</div>
											</div> 

											<div class="col-xs-10">
												<div class="form-group">
													<?php 
														echo form_textarea($form['alamat']);
														echo form_error('alamat','<div class="note">','</div>');
													?>
												</div>
											</div> 

										</div>  
									</div>

									<div class="col-xs-12">
										<div class="row"> 
											<div class="col-xs-2 col-md-2 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['noktp']['placeholder']);
															?>
														</div>
													</div> 

												</div>
											</div>
											<div class="col-xs-3 col-md-3 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php 
															echo form_input($form['noktp']);
															echo form_error('noktp','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>
											<div class="col-xs-2 col-md-2 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_label($form['npwp']['placeholder']);
															?>
														</div>
													</div> 

												</div>
											</div>



											<div class="col-xs-3 col-md-3 ">
												<div class="row"> 

													<div class="col-xs-12">
														<div class="form-group">
															<?php
															echo form_input($form['npwp']);
															echo form_error('npwp','<div class="note">','</div>');
															?>
														</div>
													</div> 

												</div>
											</div>
										</div>
									</div>
								</div>

				        <div role="tabpanel" class="tab-pane fade" id="example2-tab2">
				        	<div class="col-xs-12">
								<div class="form-group">
									<div class="col-xs-3">
										<div class="form-group">
											<?php
												echo form_label($form['nodo_awal']['placeholder']);
												echo form_input($form['nodo_awal']);
												echo form_error('nodo_awal','<div class="note">','</div>');
											?>
										</div>
									</div>
									<div class="col-xs-3"> 
										<div class="form-group">   
											<?php
												echo form_label($form['nodo_akhir']['placeholder']);
												echo form_input($form['nodo_akhir']);
												echo form_error('nodo_akhir','<div class="note">','</div>');
											?>
						                </div>
									</div>
									<div class="col-xs-1"> 
										<div class="form-group">  
											<br> 
						                        <a class="btn btn-primary btn-tampil">
						                            <i class="fa fa-list"></i> Tampil
						                        </a>
						                    </div>
										</div> 
											<div class="col-xs-1"> 
												<div class="form-group has-error">  
														<br> 
						                            <a class="btn btn-primary mlt-ctk-inv" target="_blank" >
						                              <i class="fa fa-print"></i> Cetak
						                            </a>
						                        </div>
											</div> 
											<div class="col-xs-1"> 
												<div class="form-group has-error">  
														<br> 
						                            <a class="btn btn-success mlt-down-pdf" target="_blank">
						                              <i class="fa fa-save"></i> Save PDF
						                            </a>
						                        </div>
											</div>
									</div>
								</div>
				        		<div class="col-xs-12">
									<table id="myTable2" class="dataTable table table-striped table-bordered table-condensed " width="100%" cellspacing="0">
		                    			<thead>
											<tr>
												<th style="width: 100px;text-align: center;">NO. DO</th>
												<th style="text-align: center;">TGL. DO</th>
												<th style="text-align: center;">NO. SPK</th> 
												<th style="text-align: center;">TIPE</th> 
												<th style="text-align: center;">NAMA </th>
												<th style="text-align: center;">ALAMAT</th> 
												<th style="text-align: center;">STATUS</th>
												<th style="text-align: center;">HARGA</th>
												<th style="text-align: center;">DISKON</th>
												<th style="text-align: center;">DPP</th>
												<th style="text-align: center;">PPN</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th style="width: 100px;text-align: center;">NO. DO</th>
												<th style="text-align: center;">TGL. DO</th>
												<th style="text-align: center;">NO. SPK</th> 
												<th style="text-align: center;">TIPE</th>
												<th style="text-align: center;">NAMA </th>
												<th style="text-align: center;">ALAMAT</th> 
												<th style="text-align: center;">STATUS</th>
												<th style="text-align: center;">HARGA</th>
												<th style="text-align: center;">DISKON</th>
												<th style="text-align: center;">DPP</th>
												<th style="text-align: center;">PPN</th> 
											</tr>
										</tfoot>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
 
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">
$(document).ready(function () {
	clear();
	getid();

	   $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
	      $($.fn.dataTable.tables(true)).DataTable()
	         .columns.adjust()
	         .responsive.recalc();
	   });
	init_nama();
	// clear();
	autoNum();
	$("#nama").change(function(){
		getData();
	});

	$(".btn-tampil").click(function(){
		table.ajax.reload();
	});

	$('.ctk-inv').click(function () {
            // clear();
            var nodo = $('#nodo').val();
            window.open('rptinvso/ctk_inv/'+nodo, '_blank');
            // window.location.href = 'donew/ctk_um/'+nodo;
            // ctk_um();
    });

	$('.mlt-ctk-inv').click(function () {
            // clear();
            var nodo_awal = $('#nodo_awal').val();
            var nodo_akhir = $('#nodo_akhir').val();
            window.open('rptinvso/mlt_ctk_inv/'+nodo_awal+'/'+nodo_akhir, '_blank');
            // window.location.href = 'donew/ctk_um/'+nodo;
            // ctk_um();
    });

	$('.down-pdf').click(function () {
            // clear();
            down_pdf();
            // window.location.href = 'donew/ctk_um/'+nodo;
            // ctk_um();
    });

	$('.mlt-down-pdf').click(function () {
            // clear();
            mlt_down_pdf();
            // window.location.href = 'donew/ctk_um/'+nodo;
            // ctk_um();
    });

    var column = [];
        for (i =7; i <= 10; i++) { 
            if(i===8){
                column.push({ 
                    "bSortable": false,
                    "aTargets": [ i ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return formmatedvalue;
                        },
                    "sClass": "right"
                    });
            }else{
                column.push({ 
                    "aTargets": [ i ],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return type === 'export' ? data : numeral(data).format('0,0.00');
                        // return formmatedvalue;
                        },
                    "sClass": "right"
                    });
            }
        }

        column.push({ 
                "aTargets": [ 1 ],
                "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                },
                "sClass": "center"
        });



        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,

            "fixedColumns": {
                leftColumns: 1
            },



            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "aaSorting": [],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "nodo_awal", "value": $("#nodo_awal").val() }
                            ,{ "name": "nodo_akhir", "value": $("#nodo_akhir").val() });
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptinvso/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete"  && title!=="SALDO"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
});

function getid(){ 
        $.ajax({
            type: "POST",
            url: "<?=site_url("rptinvso/getid");?>",
            data: {},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#nodo_awal').mask(value.kode+"99-999999");
                  $('#nodo_akhir').mask(value.kode+"99-999999");
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
}

    function down_pdf(){
      var nodo = $("#nodo").val(); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("rptinvso/down_pdf");?>",
            data: {"nodo":nodo },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                });
            }
        });
    }

    function mlt_down_pdf(){
      var nodo_awal = $("#nodo_awal").val(); 
      var nodo_akhir = $("#nodo_akhir").val(); 
        $.ajax({
            type: "POST",
            url: "<?=site_url("rptinvso/mlt_down_pdf");?>",
            data: {"nodo_awal":nodo_awal,"nodo_akhir":nodo_akhir },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                });
            }
        });
    }




function autoNum(){
	$('#hrg').autoNumeric('init',{aSign: 'Rp. '});
	$('#disc').autoNumeric('init',{aSign: 'Rp. '});
	$('#dpp').autoNumeric('init',{aSign: 'Rp. '});
	$('#ppn').autoNumeric('init',{aSign: 'Rp. '});
}

function clear(){
	$("#nodo").val('');
	$("#tgldo").val('');
	$("#noso").val('');				
	$("#tglso").val('');
	$("#kdtipe").val('');
	$("#nmtipe").val('');
	$("#nosin").val('');
	$("#nora").val('');
	$("#status").val(''); 
	$("#hrg").val(''); 
	$("#disc").val(''); 
	$("#dpp").val(''); 
	$("#ppn").val(''); 
	$("#nmcust").val('');
	$("#alamat").val(''); 
	$("#noktp").val(''); 
	$("#npwp").val(''); 
	$("#nodo_awal").val(''); 
	$("#nodo_akhir").val(''); 

}
function getData(){
	var nama = $("#nama").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("rptinvso/getData");?>",
		data: {"nama":nama},
		beforeSend: function() {

		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				$("#nodo").val(value.nodo);
				$("#tgldo").val(moment(value.tgldo).format('LL'));
				$("#noso").val(value.noso);
				$("#tglso").val(moment(value.tglso).format('LL'));
				$("#kdtipe").val(value.kdtipex);
				$("#nmtipe").val(value.nmtipe);
				$("#nosin").val(value.nosin);
				$("#nora").val(value.nora);
				$("#status").val(value.jnsbayar);
				var harga = parseInt(value.dpp_unit) + parseInt(value.tot_disc); 
				console.log(harga);
				$("#hrg").autoNumeric('set',harga); 
				$("#disc").autoNumeric('set',value.tot_disc); 
				$("#dpp").autoNumeric('set',value.dpp_unit); 
				$("#ppn").autoNumeric('set',value.ppn_unit); 
				$("#nmcust").val(value.nama);
				$("#alamat").val(value.alamat+" Kel."+value.kel+" Kec."+value.kec+" "+value.kota); 
				$("#noktp").val(value.noktp);
				$("#npwp").val(value.npwp);

										// $("#tgl_trm_tnkb").val(moment(value.tgl_trm_tnkb).format('LL'));
										// setBgColour(value.tgl_trm_tnkb,"tgl_trm_tnkb");
		});
},
error:function(event, textStatus, errorThrown) {
	swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
}
});
}

function setBgColour(val,object){
	if(val){
		$("#"+object).css("background-color", "#fff");
	}else{
		$("#"+object).css("background-color", "#eee");
	}
}

function init_nama(){
	$("#nama").select2({
		ajax: {
			url: "<?=site_url('rptinvso/getNama');?>",
			type: 'post',
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term,
					page: params.page
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total_count
					}
				};
			},
			cache: true
		},
		placeholder: 'Masukkan Nama Customer ...',
		dropdownAutoWidth : false,
					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					minimumInputLength: 3,
					templateResult: format_nama, // omitted for brevity, see the source of this page
					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
				});
}

function format_nama_terpilih (repo) {
	return repo.full_name || repo.text;
}

function format_nama (repo) {
	if (repo.loading) return "Mencari data ... ";


	var markup = "<div class='select2-result-repository clearfix'>" +
	"<div class='select2-result-repository__meta'>" +
	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

	if (repo.alamat) {
		markup += "<div class='select2-result-repository__description'> <i class=\"fa fa-map-marker\"></i> " + repo.alamat + " || " + repo.kel + "</div>";
	}

	markup += "<div class='select2-result-repository__statistics'>" +
	"<div class='select2-result-repository__forks'><i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " </div>" +
	"<div class='select2-result-repository__stargazers'> <i class=\"fa fa-file-o\"></i> " + repo.nodo + " | <i class=\"fa fa-calendar\"></i> " + repo.tgldo + "</div>" +
	"</div>" +
	"</div>" +
	"</div>";
	return markup;
}
</script>

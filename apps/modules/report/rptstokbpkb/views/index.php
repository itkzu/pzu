<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-body">

                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                <div class="form-group">
                    <?php 
                        echo form_label($form['periode']['placeholder']);
                        echo form_input($form['periode']);
                        echo form_error('periode','<div class="note">','</div>'); 
                    ?>
                </div>
                <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <?php echo form_close(); ?>
                <hr>


                <div class="table-responsive">
                    <table style="width: 2600px;" class="table table-bordered table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th style="text-align: center;width: 70px;">TERIMA BPKB</th>
                                <th style="text-align: center;width: 70px;">NO. BPKB</th>
                                <th style="text-align: center;width: 70px;">NO. POLISI</th>
                                <th style="text-align: center;width: 170px;">TIPE UNIT</th>
                                <th style="text-align: center;width: 70px;">LEASING</th>
                                <th style="text-align: center;width: 70px;">NO. DO</th>
                                <th style="text-align: center;width: 70px;">TANGGAL DO</th>
                                <th style="text-align: center;width: 200px;">NAMA</th>
                                <th style="text-align: center;">ALAMAT</th>
                                <th style="text-align: center;width: 150px;">KELURAHAN</th>
                                <th style="text-align: center;width: 150px;">KECAMATAN</th>
                                <th style="text-align: center;width: 150px;">KOTA</th>
                                <th style="text-align: center;width: 100px;">NO. HP</th>
                                <th style="text-align: center;width: 100px;">NO. MESIN</th>
                                <th style="text-align: center;width: 100px;">NO. RANGKA</th>
                                <th style="text-align: center;width: 170px;">SALES</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody></tbody>
                    </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        event.preventDefault();

        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        
        var column = [];

        column.push({ 
            "aTargets": [ 0,6 ],
            "mRender": function (data, type, full) {
                //data : moment(data).format('L');
                return type === 'export' ? data : moment(data).format('L');
                //return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                }//,
            //"sClass": "right"
        });

    
        table = $('.dataTable').DataTable({     
            "aoColumnDefs": column,
            "fixedColumns": {
                leftColumns: 2
            },
            "lengthMenu": [[-1], ["Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "order": [[ 0 , "asc" ]],
            "bPaginate": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( 
                            { "name": "periode", "value": $("#periode").val() }
                            //,{ "name": "periode_akhir", "value": $("#periode_akhir").val() }
                            );
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(0)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptstokbpkb/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf', 
                    orientation: 'landscape',
                    pageSize: 'A3'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
        
        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );
        
        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
    
        
    });
</script>
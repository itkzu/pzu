<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptaksksn
 *
 * @author
 */

class Rptaksksn extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptaksksn/submit'),
            'add' => site_url('rptaksksn/add'),
            'edit' => site_url('rptaksksn/edit'),
            'reload' => site_url('rptaksksn'),
        );
        $this->load->model('rptaksksn_qry');
        $aks = $this->rptaksksn_qry->getAks();
        foreach ($aks as $value) {
            $this->data['kdaks'][$value['kdaks']] = $value['nmaks'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->rptaksksn_qry->json_dgview();
    }

    public function getNama() {
        echo $this->rptaksksn_qry->getNama();
    }

    public function getData() {
        echo $this->rptaksksn_qry->getData();
    }

    private function _init_add(){
        $this->data['form'] = array(
            'kdaks'=> array(
                  'attr'        => array(
                      'id'    => 'kdaks',
                      'class' => 'form-control',
                  ),
                  'data'     => '',
                  //$this->data['kdaks']
                  'value'    => set_value('kdaks'),
                  'name'     => 'kdaks',
                  'placeholder'=> "Nama Aksesoris"
            ),
            'periode'=> array(
                     'placeholder' => 'Periode',
                     'id'          => 'periode',
                     'name'        => 'periode',
                     'value'       => date('Y-m'),
                     'class'       => 'form-control',
                     'style'       => 'margin-left: 5px;',
                     'required'    => '',
            ),
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode Akhir',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
            ),
        );
    }
}

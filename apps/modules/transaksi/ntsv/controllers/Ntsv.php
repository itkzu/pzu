<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Ntsv
 *
 * @author
 */

class Ntsv extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main'      => $this->msg_main,
            'msg_detail'    => $this->msg_detail,

            'ctk_nt' => site_url('ntsv/ctk_nt'),
            'submit' => site_url('ntsv/submit'),
            // 'add' => site_url('ntsv/add'),
            'edit' => site_url('ntsv/edit'),
            'reload' => site_url('ntsv'),
        );
        $this->load->model('ntsv_qry');
        $this->load->helper(array('url','download'));
        require_once APPPATH.'libraries/PHPExcel.php';
        $cabang = $this->ntsv_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }/*
        $kb = $this->ntsv_qry->getkb();
        $this->data['kb'] = array(
            "" => "-- Pilih Posisi(D/K) --",
        );*/
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    // public function add(){
    //     $this->_init_add();
    //     $this->template
    //         ->title($this->data['msg_main'],$this->apps->name)
    //         ->set_layout('main')
    //         ->build('form',$this->data);
    // }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function ctk_nt() {  
        $nopkb = $this->uri->segment(3); 
        $this->data['data'] = $this->ntsv_qry->ctk_nt($nopkb);  
        $this->data['data2'] = $this->ntsv_qry->ctk_nt2($nopkb);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_nt',$this->data);  
    } 

    //import
    public function get_SV() {
        echo $this->ntsv_qry->get_SV();
    }

    public function ins_SV() {
        echo $this->ntsv_qry->ins_SV();
    }

    public function save_SV() {
        echo $this->ntsv_qry->save_SV();
    }

    public function json_dgview() {
        echo $this->ntsv_qry->json_dgview();
    }  

    public function json_dgview_detail() {
        echo $this->ntsv_qry->json_dgview_detail();
    }  

    public function json_dgview_history() {
        echo $this->ntsv_qry->json_dgview_history();
    }  

    public function cekdata() {
        echo $this->ntsv_qry->cekdata();
    }  

    public function set_nmpart() {
        echo $this->ntsv_qry->set_nmpart();
    }  

    public function set_SV() {
        echo $this->ntsv_qry->set_SV();
    }

    public function set_NT() {
        echo $this->ntsv_qry->set_NT();
    }

    public function batal() {
        echo $this->ntsv_qry->batal();
    }

    public function update() {
        echo $this->ntsv_qry->update();
    }

    public function submit() {
        echo $this->ntsv_qry->submit();
    }

    public function getJenispekerjaan() {
        echo $this->ntsv_qry->getJenispekerjaan();
    }

    private function _init_add(){
        $this->data['form'] = array(
            'nopkb'     => array(
                    'placeholder' => 'No PKB',
                    //'type'        => 'hidden',
                    'id'          => 'nopkb',
                    'name'        => 'nopkb',
                    'value'       => set_value('nopkb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly' => '',
            ),
            'tglnota'     => array(
                    'placeholder' => 'Tanggal Nota',
                    //'type'        => 'hidden',
                    'id'          => 'tglnota',
                    'name'        => 'tglnota',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly' => '',
            ),
            //pkb
            'tglpkb1'     => array(
                    'placeholder' => 'Tgl Buat',
                    //'type'        => 'hidden',
                    'id'          => 'tglpkb1',
                    'name'        => 'tglpkb1',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly' => '',
            ),
            'tglpkb2'     => array(
                    'placeholder' => 'Tgl Akhir',
                    //'type'        => 'hidden',
                    'id'          => 'tglpkb2',
                    'name'        => 'tglpkb2',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly' => '',
            ),
            'tglsv'     => array(
                    'placeholder' => 'Tgl Service',
                    //'type'        => 'hidden',
                    'id'          => 'tglsv',
                    'name'        => 'tglsv',
                    'value'       => set_value('tglsv'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nopol'     => array(
                    'placeholder' => 'No Polisi',
                    //'type'        => 'hidden',
                    'id'          => 'nopol',
                    'name'        => 'nopol',
                    'value'       => set_value('nopol'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nosin'     => array(
                    'placeholder' => 'No Mesin/No Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nora'     => array(
                    'placeholder' => 'No Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nama'     => array(
                    'placeholder' => 'Nama Pembawa',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nama_s'     => array(
                    'placeholder' => 'Nama Pemilik',
                    //'type'        => 'hidden',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => set_value('nama_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'alamat_s'     => array(
                    'placeholder' => 'Alamat Pemilik',
                    //'type'        => 'hidden',
                    'id'          => 'alamat_s',
                    'name'        => 'alamat_s',
                    'value'       => set_value('alamat_s'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'ketsv'     => array(
                    'placeholder' => 'Keluhan',
                    //'type'        => 'hidden',
                    'id'          => 'ketsv',
                    'name'        => 'ketsv',
                    'value'       => set_value('ketsv'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'solvesv'     => array(
                    'placeholder' => 'Saran Mekanik',
                    //'type'        => 'hidden',
                    'id'          => 'solvesv',
                    'name'        => 'solvesv',
                    'value'       => set_value('solvesv'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ), 
        );
    } 

    private function _init_edit($no = null){
        if(!$no){
            $nodf = $this->uri->segment(3); 
        }
        $this->_check_id($nodf); 

          $this->data['form'] = array(
            'nopkb'     => array(
                    'placeholder' => 'No PKB',
                    //'type'        => 'hidden',
                    'id'          => 'nopkb',
                    'name'        => 'nopkb',
                    'value'       => $this->val[0]['noworkorder'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ), 
            'tglnota'     => array(
                    'placeholder' => 'Tanggal Nota',
                    //'type'        => 'hidden',
                    'id'          => 'tglnota',
                    'name'        => 'tglnota',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;', 
                    // 'readonly' => '',
            ),
            'tglsv'     => array(
                    'placeholder' => 'Tgl Service',
                    //'type'        => 'hidden',
                    'id'          => 'tglsv',
                    'name'        => 'tglsv',
                    'value'       => $this->val[0]['tanggalservis'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nopol'     => array(
                    'placeholder' => 'No Polisi',
                    //'type'        => 'hidden',
                    'id'          => 'nopol',
                    'name'        => 'nopol',
                    'value'       => $this->val[0]['nopolisi'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nosin'     => array(
                    'placeholder' => 'No Mesin/No Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => $this->val[0]['nomesin'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nora'     => array(
                    'placeholder' => 'No Rangka',
                    //'type'        => 'hidden',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => $this->val[0]['norangka'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nama'     => array(
                    'placeholder' => 'Nama Pembawa',
                    //'type'        => 'hidden',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => $this->val[0]['namapembawa'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'nama_s'     => array(
                    'placeholder' => 'Nama Pemilik',
                    //'type'        => 'hidden',
                    'id'          => 'nama_s',
                    'name'        => 'nama_s',
                    'value'       => $this->val[0]['namapemilik'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'alamat_s'     => array(
                    'placeholder' => 'Alamat Pemilik',
                    //'type'        => 'hidden',
                    'id'          => 'alamat_s',
                    'name'        => 'alamat_s',
                    'value'       => $this->val[0]['alamat_s'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'ketsv'     => array(
                    'placeholder' => 'Keluhan',
                    //'type'        => 'hidden',
                    'id'          => 'ketsv',
                    'name'        => 'ketsv',
                    'value'       => $this->val[0]['keluhankonsumen'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'solvesv'     => array(
                    'placeholder' => 'Saran Mekanik',
                    //'type'        => 'hidden',
                    'id'          => 'solvesv',
                    'name'        => 'solvesv',
                    'value'       => $this->val[0]['saranmekanik'],
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
 
            // edit 
            'e_nopkb'     => array(
                    'placeholder' => 'No PKB',
                    'type'        => 'hidden',
                    'id'          => 'e_nopkb',
                    'name'        => 'e_nopkb',
                    'value'       => set_value('e_nopkb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'e_jenis'     => array(
                    'placeholder' => 'Grup',
                    //'type'        => 'hidden',
                    'id'          => 'e_jenis',
                    'name'        => 'e_jenis',
                    'value'       => set_value('e_jenis'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
           // 'e_jenispekerjaan'=> array(
           //         'attr'        => array(
           //             'id'    => 'e_jenispekerjaan',
           //             'class' => 'form-control select2',
           //         ),
           //         'data'     =>  '',
           //         'value'    => set_value('e_jenispekerjaan'),
           //         'name'     => 'e_jenispekerjaan',
           //         'required'    => '',
           //         'placeholder' => 'Jenis',
           //  ),
           'e_jenispekerjaan'=> array(
                    'placeholder' => 'Jenis',
                    'id'          => 'e_jenispekerjaan',
                    'name'        => 'e_jenispekerjaan',
                    'value'       => set_value('e_jenispekerjaan'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ),
            'e_idjob'     => array(
                    'placeholder' => 'ID Job',
                    'type'        => 'hidden',
                    'id'          => 'e_idjob',
                    'name'        => 'e_idjob',
                    'value'       => set_value('e_idjob'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ), 
            'e_namapekerjaan'     => array(
                    'placeholder' => 'Nama',
                    //'type'        => 'hidden',
                    'id'          => 'e_namapekerjaan',
                    'name'        => 'e_namapekerjaan',
                    'value'       => set_value('e_namapekerjaan'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly' => '',
            ), 
            'e_um'     => array(
                    'placeholder' => 'Uang Muka',
                    //'type'        => 'hidden',
                    'id'          => 'e_um',
                    'name'        => 'e_um',
                    'value'       => set_value('e_um'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;text-align: right', 
                    'readonly' => '',
            ), 
            'e_diskper'     => array(
                    'placeholder' => 'Discount Percentage',
                    //'type'        => 'hidden',
                    'id'          => 'e_diskper',
                    'name'        => 'e_diskper',
                    'value'       => set_value('e_diskper'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;text-align: right', 
                    // 'readonly' => '',
            ), 
            'e_disk'     => array(
                    'placeholder' => 'Discount',
                    //'type'        => 'hidden',
                    'id'          => 'e_disk',
                    'name'        => 'e_disk',
                    'value'       => set_value('e_disk'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;text-align: right', 
                    // 'readonly' => '',
            ), 
            'e_harga'     => array(
                    'placeholder' => 'Harga',
                    //'type'        => 'hidden',
                    'id'          => 'e_harga',
                    'name'        => 'e_harga',
                    'value'       => set_value('e_harga'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;text-align: right', 
                    // 'readonly' => '',
            ), 
            'e_totharga'     => array(
                    'placeholder' => 'Total Harga',
                    //'type'        => 'hidden',
                    'id'          => 'e_totharga',
                    'name'        => 'e_totharga',
                    'value'       => set_value('e_totharga'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;text-align: right', 
                    'readonly' => '',
            ), 
            'e_qty'     => array(
                    'placeholder' => 'Quantity',
                    //'type'        => 'hidden',
                    'id'          => 'e_qty',
                    'name'        => 'e_qty',
                    'value'       => set_value('e_qty'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;text-align: right', 
                    'readonly' => '',
            ), 
        ); 
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->ntsv_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }


}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbkltb
 *
 * @author adi
 */
class Rptbkltb extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptbkltb/submit'),
            'add' => site_url('rptbkltb/add'),
            'edit' => site_url('rptbkltb/edit'),
            'reload' => site_url('rptbkltb'),
        );
        $this->load->model('rptbkltb_qry');
        $cabang = $this->rptbkltb_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }
        for ($x = 1; $x <= 13; $x++) {
          $this->data['bulan'][$x] = strmonth($x).". ".month($x);
        }

        for ($x = date('Y'); $x >= 2015; $x--) {
          $this->data['tahun'][$x] = $x;
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function json_dgview() {
        echo $this->rptbkltb_qry->json_dgview();
    }

    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->rptbkltb_qry->submit();
            var_dump($res);
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }

    private function _init_add(){
        $this->data['form'] = array(
            'periode_akhir'=> array(
                   'placeholder' => 'Periode',
                   'id'          => 'periode_akhir',
                   'name'        => 'periode_akhir',
                   'value'       => date('Y-m'),
                   'class'       => 'form-control',
                   'style'       => 'margin-left: 5px;',
                   'required'    => '',
           ),
            'kddiv'=> array(
                   'attr'        => array(
                       'id'    => 'kddiv',
                       'class' => 'form-control  select',
                   ),
                   'data'     =>  $this->data['kddiv'],
                   'value'    => set_value('kddiv'),
                   'name'     => 'kddiv',
                   'required'    => '',
                   'placeholder' => 'Cabang',
            ),
            'bulan_awal'=> array(
                'attr'        => array(
                  'id'    => 'bulan_awal',
                  'class' => 'form-control',
                ),
                'data'        => $this->data['bulan'],
                'value'       => $this->mm,
                'name'        => 'bulan_awal',
            ),
            'tahun'=> array(
                'attr'        => array(
                  'id'    => 'tahun',
                  'class' => 'form-control',
                ),
                'data'        => $this->data['tahun'],
                'value'       => '',
                'name'        => 'tahun',
            ),
        );
    }

    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_akhir',
                    'label' => 'Periode Akhir',
                    'rules' => 'required|max_length[20]',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

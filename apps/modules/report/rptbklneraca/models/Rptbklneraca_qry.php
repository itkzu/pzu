<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbklneraca_qry
 *
 * @author adi
 */
class Rptbklneraca_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	protected $kd_cabang = "";
	public function __construct() {
		parent::__construct();
		$this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
	}

	public function getDataCabang() {
		$this->db->select('*');
	//      $kddiv = $this->input->post('kddiv');
	//      $this->db->where('kddiv',$kddiv);
		$q = $this->db->get("bkl.get_div_unit_bkl()");
		return $q->result_array();
	}

	public function submit() {

		$array = $this->input->post();
		if($array['bulan_awal']>9){
			$array['periode_awal'] = $array['tahun']."-".$array['bulan_awal'];
		} else {
			$array['periode_awal'] = $array['tahun']."-0".$array['bulan_awal'];
		} 
		$array['periode_sebelumnya'] = month_before($array['periode_awal']);

		// echo "<script> console.log('PHP: ". json_encode($array) ."');</script>";
		//echo "<script> console.log('PHP: ". json_encode($this->session->userdata('data')['kddiv']) ."');</script>";

		$res = array(
			'mtd' => $this->mtd($array),
			'myd' => $this->myd($array),
		);
		return $res;
	}

	private function mtd($array) {
		//var_dump($array);
		//$str_period = "select glr.get_periode_gl('". $this->session->userdata('kddiv') ."') as period";
		$str_period = "select glr.get_periode_gl('". $array['kddiv'] ."') as period";
		$r_period = $this->db->query($str_period);

		//echo $this->db->last_query();

		if($r_period->num_rows()>0){
			$rw_period = $r_period->result_array();
			$period = $rw_period[0]['period'];
		}
		/*
		if($period>str_replace("-", "", $array['periode_awal'])){
			$array['periode_awal'] = $period;
		} */
		//echo $period;
		//echo str_replace("-", "", $array['periode_awal']);
		if($period==str_replace("-", "", $array['periode_awal'])){
			// Jika periode aktif sama dengan periode yg dipilih user maka ambil dari scheme yg berjalan (aktif)
			//$str_calc = "SELECT glr.neraca_calc ('". $this->session->userdata('kddiv') ."') as neraca_calc";
			$str_calc = "SELECT glr.neraca_calc ('". $array['kddiv'] ."') as neraca_calc";
			$this->db->query($str_calc);

			//echo $this->db->last_query();

			//$this->db->where("kddiv",$this->session->userdata('kddiv'));
			$this->db->where("kddiv", $array['kddiv'] );
			$q = $this->db->get('glr.vl_neraca');
		}else{
			// Jika periode aktif tidak sama dengan periode yg dipilih user maka ambil dari scheme history
			//$this->db->where("kddiv",$this->session->userdata('kddiv'));
			$this->db->where("kddiv",$array['kddiv']);
			$this->db->where("periode",  str_replace("-", "", $array['periode_awal']));
			$q = $this->db->get('glr_h.vl_neraca');
		}
		// echo $this->db->last_query();
		if($q->num_rows() > 0){
			return $q->result_array();
		}else{
			return false;
		}
	}

	private function myd($array) {
		//$this->db->where("kddiv",$this->session->userdata('kddiv'));
		$this->db->where("kddiv", $array['kddiv'] );
		$this->db->where("periode",  str_replace("-", "", $array['periode_sebelumnya']));
		$q = $this->db->get('glr_h.vl_neraca');
		if($q->num_rows() > 0){
			return $q->result_array();
		}else{
			return false;
		}
	}
}

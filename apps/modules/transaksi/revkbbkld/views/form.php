<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_edit'
                    , 'name' => 'form_edit'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="col-lg-6">
                    <div class="form-group">
                        <?php
                            echo form_label($form['nocetak']['placeholder']);
                            echo form_input($form['nocetak']);
                            echo form_error('nocetak','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['nokb']['placeholder']);
                            echo form_input($form['nokb']);
                            echo form_error('nokb','<div class="note">','</div>');
                        ?>
                    </div>

                    <div class="form-group">
                        <?php
                            echo form_input($form['nourut']);
                            echo form_error('nourut','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['tglkb']['placeholder']);
                            echo form_input($form['tglkb']);
                            echo form_error('tglkb','<div class="note">','</div>');
                        ?>
                    </div> 
                    <div class="form-group">
                        <?php
                            echo form_label('Jenis Transaksi');
                            echo form_dropdown($form['jenis']['name'],$form['jenis']['data'] ,$form['jenis']['value'] ,$form['jenis']['attr']);
                            echo form_error('jenis','<div class="note">','</div>');
                        ?> 
                    </div>
<!--                     <div class="form-group">
                        <?php
                            echo form_label($form['jenis']['placeholder']);
                            echo form_input($form['jenis']);
                            echo form_error('jenis','<div class="note">','</div>');
                        ?>
                    </div> -->
                    <div class="form-group">
                        <?php
                            echo form_label($form['kddiv']['placeholder']);
                            echo form_input($form['kddiv']);
                            echo form_error('kddiv','<div class="note">','</div>');
                        ?>
                    </div> 
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <?php
                            echo form_label($form['nofaktur']['placeholder']);
                            echo form_input($form['nofaktur']);
                            echo form_error('nofaktur','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['drkpd']['placeholder']);
                            echo form_input($form['drkpd']);
                            echo form_error('drkpd','<div class="note">','</div>');
                        ?>
                    </div>

                    <div class="form-group">
                        <?php
                            echo form_label($form['ket']['placeholder']);
                            echo form_input($form['ket']);
                            echo form_error('ket','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['debet']['placeholder']);
                            echo form_input($form['debet']);
                            echo form_error('debet','<div class="note">','</div>');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo form_label($form['kredit']['placeholder']);
                            echo form_input($form['kredit']);
                            echo form_error('kredit','<div class="note">','</div>');
                        ?>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <a href="<?php echo $reload;?>" class="btn btn-default">
                    Batal
                </a>
            </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 

        $("#jenis").select2({ width: '100%' }); 
        getKdRef();
        set_jenis();

        $(".btn-submit").click(function(){
            updating();
        });
    });

    function set_jenis(){
        var nokb = $("#nokb").val();
        var nourut = $("#nourut").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("revkbbkld/set_jenis");?>",
            data: {"nokb":nokb,"nourut":nourut },
            success: function(resp){
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    
                    $("#jenis").val(data.kdrefkb);
                    $("#jenis").trigger("change");
                    $('#jenis').select2({
                                  dropdownAutoWidth : true,
                                  width: '100%'
                                });
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdRef(){
        // alert($('#kredit').val());
        if($('#kredit').val()>'0') {
            var jenis = 'K';
        } else {
            var jenis = 'D';
        }
        var kddiv = $('#kddiv').val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("revkbbkld/getKdRef");?>",
            data: {"jenis":jenis,"kddiv":kddiv},
            beforeSend: function() {
                $('#jenis').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Jenis Transaksi --'));
                $("#jenis").trigger("change.chosen");
                if ($('#jenis').hasClass("chosen-hidden-accessible")) {
                    $('#jenis').select2('destroy');
                    $("#jenis").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#jenis')
                        .append($('<option>', { value : value.kdrefkb })
                        .html("<b style='font-size: 14px;'>" + value.nmrefkb + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmrefkb + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function updating(nokb,nourut){
        swal({
            title: "Konfirmasi",
            text: "Proses Update Transaksi Kas & Bank akan dilakukan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true
            },

            function () {
              var nokb = $("#nokb").val();
              var nourut = $("#nourut").val();
              var nofaktur = $("#nofaktur").val();
              var darike = $("#drkpd").val();
              var jenis = $("#jenis").val();
              var ket = $("#ket").val();
              var debet = $("#debet").val();
              var kredit= $("#kredit").val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('revkbbkld/proses');?>",
                    data: {"nokb":nokb
                          ,"nourut":nourut
                          ,"nofaktur":nofaktur
                          ,"darike":darike
                          ,"ket":ket
                          ,"jenis":jenis
                          ,"debet":debet
                          ,"kredit":kredit
                  },
                    success: function(resp){
                        var obj = JSON.parse(resp);
                        //alert(JSON.stringify(resp));
                        $.each(obj, function(key, data){
                            swal({
                                title: data.title,
                                text: data.msg,
                                type: data.tipe
                            }, function(){
                                window.location.href = "<?=site_url('revkbbkld/');?>"
                            });
                        });
                    },
                    error: function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
            }
        );
    }

</script>

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style>
	.select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
		outline: none;
		border: none;
	}
	.radio {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.checkbox label, .radio label {
		min-height: 20px;
		padding-left: 20px;
		margin-bottom: 5px;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
			<!--
			<div class="box-header with-border">
				<h3 class="box-title">{msg_main}</h3>
			</div>
			-->
			<!-- /.box-header -->
			<!-- form start -->
			<?php
				$attributes = array(
					'role=' => 'form'
					, 'id' => 'form_add'
					, 'name' => 'form_add'
					, 'enctype' => 'multipart/form-data'
					, 'target' => '_blank'
					, 'data-validate' => 'parsley');
				echo form_open($submit,$attributes);
			?>
			<div class="box-body">
                <div class="form-group">
                    <?php  
                        echo form_input($form['ket']);
                        echo form_error('ket','<div class="note">','</div>');  
                    ?>
                </div>
                <div class="form-group">
                    <?php 
                        echo form_label($form['kddiv']['placeholder']);
                        echo form_dropdown($form['kddiv']['name'],$form['kddiv']['data'] ,$form['kddiv']['value'] ,$form['kddiv']['attr']);
                        echo form_error('kddiv','<div class="note">','</div>');  
                    ?>
                </div>
				<div class="form-group">
					<?php
						echo '<div class="radio">';
						echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Periode Bulan <small>per bulan</small>');
						echo '</div>';
						echo form_input($form['periode']);
						echo form_error('akun_multi','<div class="note">','</div>');
					?>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo '<div class="radio">';
								echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Periode Awal');
								echo '</div>';
								echo form_input($form['periode_awal']);
								echo form_error('periode_awal','<div class="note">','</div>');
							?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php
								echo form_label('Pilih Periode Akhir');
								echo form_input($form['periode_akhir']);
								echo form_error('periode_akhir','<div class="note">','</div>');
							 ?>
						</div>

					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="button" class="btn btn-primary btn-tampil">Tampil</button>
				</div>
                                        
                <div class="form-group">
                	<button type="submit" class="btn btn-default html" name="submit"  ><i class="fa fa-print"></i> Cetak</button>
                	<button type="submit" class="btn btn-success html" name="submit" value="excel"><i class="fa fa-file-excel-o"></i> Excel</button>
                </div>

				<div class="table-responsive">
					<table class="dataTable table table-bordered table-striped table-hover dataTable">
						<thead>
							<tr>
								<th style="width: 20%; text-align: center;" rowspan="2">Kode </th>
								<th style="width: 20%; text-align: center;" rowspan="2">Nama </th>
								<th style="width: 14%; text-align: center;" rowspan="2">Kategori</th>
								<th style="width: 13%; text-align: center;" rowspan="2">Harga</th>
								<th style="width: 10%; text-align: center;" colspan="2">Saldo Awal</th>
								<th style="width: 10%; text-align: center;" colspan="2">Masuk</th>
								<th style="width: 10%; text-align: center;" colspan="2">BAG Masuk</th>
								<th style="width: 10%; text-align: center;" colspan="2">Keluar</th>
								<th style="width: 10%; text-align: center;" colspan="2">BAG Keluar</th>
								<!-- <th style="width: 10%; text-align: center;" colspan="2">Adjustment</th> -->
								<th style="width: 10%; text-align: center;" colspan="2">Saldo Akhir</th>
							</tr>
							<tr>
								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>

								<td style="text-align: center;">Qty</td>
								<td style="text-align: center;">Total</td>
							</tr>
						</thead> 
                        <tbody class="tunai_bd"></tbody>
						<!-- <tfoot>
							<tr>
								<td style="text-align: center;" colspan="4"></td>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
								<th style="text-align: center;"></th>
							</tr>
						</tfoot> -->
					</table>
				</div>
				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		disable();
		$("#rdakun_multi").click(function(){
			$(".btn-tampil").prop("disabled", false);
			$("#ket").val('periode_multi');
			setAkun();
		});
		$("#rdakun_between").click(function(){
			$(".btn-tampil").prop("disabled", false);
			$("#ket").val('periode_between');
			setAkun();
		});

		$('#periode').datepicker({
			startView: "year",
			minViewMode: "months",
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "yyyy-mm"
		});

		$('#periode_awal').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$('#periode_akhir').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "dd-mm-yyyy"
		});

		$(".btn-tampil").click(function(){
			$(".dataTable").show(); 
			tampiltable();
		});
	}); 
    
    function tampiltable(){
        if ( $.fn.DataTable.isDataTable('.dataTable') ) {
          $('.dataTable').DataTable().destroy();
          $(".tunai_bd").html('');
        }

		var kddiv = $("#kddiv").val();
		var periode = $("#periode").val();
		var periode_awal = $("#periode_awal").val();
		var periode_akhir = $("#periode_akhir").val();
		var periode_d = $('#rdakun_multi').is(':checked');
		if(periode_d){
			var ket = "periode_multi";
		} else {
			var ket = "periode_between";
		}
        $.ajax({
            type: "POST",
            url: "<?=site_url('rptbkluntnsn/getdata_table');?>",
            data: {"ket":ket, "periode":periode, "periode_awal":periode_awal, "periode_akhir":periode_akhir, "kddiv":kddiv}, 
            beforeSend: function(){
                $(".tunai_bd").html('');
                $(".btn").attr("disabled",true);
            },
            success: function(resp){  
                $(".btn").attr("disabled",false);
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var grand_saw = 0;
                    var grand_qtyin = 0;
                    var grand_qtybagin = 0;
                    var grand_qtyout = 0;
                    var grand_qtybagout = 0;
                    var grand_sak = 0;
                    $.each(obj.leasing, function(key, leas){
	                    var sub_saw = 0;
	                    var sub_qtyin = 0;
	                    var sub_qtybagin = 0;
	                    var sub_qtyout = 0;
	                    var sub_qtybagout = 0;
	                    var sub_sak = 0;
                        $.each(obj.data, function(key, data){ 
                            if(leas===data.kategori){
                                $(".tunai_bd").append('<tr>' + 
                                            // '<td style="text-align: center;">'+data.no+'</td>' + 
                                            '<td style="text-align: left;">'+data.kdpart+'</td>' +  
                                            '<td style="text-align: left;">'+data.nmpart+'</td>' + 
                                            '<td style="text-align: left;">'+data.kdgrup+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.harga)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.saw_qty)).format('0,0')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.saw_total)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.in_qty)).format('0,0')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.in_total)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.bag_in_qty)).format('0,0')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.bag_in_total)).format('0,0.00')+'</td>' + 
                                            '<td style="text-align: right;">'+numeral(Number(data.out_qty)).format('0,0')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.out_total)).format('0,0.00')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.bag_out_qty)).format('0,0')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.bag_out_total)).format('0,0.00')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.sak_qty)).format('0,0')+'</td>' +  
                                            '<td style="text-align: right;">'+numeral(Number(data.sak_total)).format('0,0.00')+'</td>' +  
                                        '</tr>');
                                sub_saw 		= Number(sub_saw) + Number(data.saw_total);
                                sub_qtyin  		= Number(sub_qtyin) + Number(data.in_total);
                                sub_qtybagin  	= Number(sub_qtybagin) + Number(data.bag_in_total);
                                sub_qtyout   	= Number(sub_qtyout) + Number(data.out_total);
                                sub_qtybagout	= Number(sub_qtybagout) + Number(data.bag_out_total);
                                sub_sak   		= Number(sub_sak) + Number(data.sak_total);
                            }
                        }); 
                        
                    $(".tunai_bd").append('<tr style="background-color: #dadada;">' +  
                                '<td style="text-align: left;font-weight: bold;">TOTAL '+leas+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_saw)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_qtyin)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_qtybagin)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_qtyout)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_qtybagout)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(sub_sak)).format('0,0.00')+'</td>' + 
                            '</tr>');
                        grand_saw 		= Number(grand_saw) + Number(sub_saw);
                        grand_qtyin 	= Number(grand_qtyin) + Number(sub_qtyin);
                        grand_qtybagin 	= Number(grand_qtybagin) + Number(sub_qtybagin);
                        grand_qtyout 	= Number(grand_qtyout) + Number(sub_qtyout);
                        grand_qtybagout = Number(grand_qtybagout) + Number(sub_qtybagout);
                        grand_sak 		= Number(grand_sak) + Number(sub_sak);
                    });

                    $(".tunai_bd").append('<tr style="background-color: #dadada;">' +  
                                '<td style="text-align: left;font-weight: bold;">GRAND TOTAL</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_saw)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_qtyin)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_qtybagin)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_qtyout)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_qtybagout)).format('0,0.00')+'</td>' + 
                                '<td style="text-align: left;">&nbsp</td>' +  
                                '<td style="text-align: right;font-weight: bold;">'+numeral(Number(grand_sak)).format('0,0.00')+'</td>' + 
                            '</tr>');
                }    
            	
            	$(".dataTable").DataTable({
                    "lengthMenu": [[-1], ["Semua Data"]],
                    "oLanguage": {
                        "url": "<?=base_url('assets/plugins/dtables/dataTables.indonesian.lang');?>"
                    }, 
                    "bProcessing": false,
                    "bServerSide": false,
                    "bDestroy": true,
                    "bAutoWidth": false,
                    "ordering": false
                });                
            },
            error:function(event, textStatus, errorThrown) {
                $(".btn").attr("disabled",true);
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
      });
    }

	function disable(){
		$(".dataTable").hide();
		$("#periode").prop("disabled", true);
		$(".btn-tampil").prop("disabled", true);
		$("#periode_awal").prop("disabled", true);
		$("#periode_akhir").prop("disabled", true);
	}

	function setAkun(){
		var check = $('#rdakun_multi').is(':checked');
		if(check){
			$("#periode").prop("disabled", false);
			$("#periode_awal").prop("disabled", true);
			$("#periode_akhir").prop("disabled", true);
		}else{
			$("#periode").prop("disabled", true);
			$("#periode_awal").prop("disabled", false);
			$("#periode_akhir").prop("disabled", false);

		}
	}
</script>

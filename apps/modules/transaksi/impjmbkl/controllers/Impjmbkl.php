<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Impjmbkl
 *
 * @author
 */

class Impjmbkl extends MY_Controller {
    public $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('impjmbkl/submit'),
            'add' => site_url('impjmbkl/add'),
            'edit' => site_url('impjmbkl/edit'),
            'reload' => site_url('impjmbkl'),
        );
        $this->load->model('impjmbkl_qry');
        $this->load->helper(array('url','download'));
        $this->load->library('PHPExcel');
        // require_once APPPATH.'libraries/PHPExcel.php';
        $pos = $this->impjmbkl_qry->getDataPos();
        foreach ($pos as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    // public function submit() {
    //         $res = $this->impjmbkl_qry->getAllSo();
    //         //$this->load->view('export_html', $this->data);
    //         $this->template
    //             ->title($this->data['msg_main'],$this->apps->name)
    //             ->set_layout('print-layout')
    //             ->build('export_excel',$this->data);
    // }


    public function int($string) {
        return (int) $string;
    }

    public function download_template_jm(){
        force_download('assets/template/Template JM.xlsx',NULL);
    } 

    public function proces_servis() {
        $this->impjmbkl_qry->proces_servis();
        //redirect("impumsl");
    }

    public function json_dgview_detail() {
        echo $this->impjmbkl_qry->json_dgview_detail();  
    }

    public function tambah() {
        echo $this->impjmbkl_qry->tambah();  
    }

    public function batal() {
        echo $this->impjmbkl_qry->batal();  
    }

    public function fromExcelToLinux($excel_time){
      return ($excel_time-25569)*86400;
    }

    private function _init_add(){
        $this->data['form'] = array( 
           'nojurnal'=> array(
                    'placeholder' => 'No. Jurnal',
                    //'type'        => 'hidden',
                    'id'          => 'nojurnal',
                    'name'        => 'nojurnal',
                    'value'       => set_value('nojurnal'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tgljurnal'=> array(
                    'placeholder' => 'Tanggal Jurnal',
                    'id'          => 'tgljurnal',
                    'name'        => 'tgljurnal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kddiv'],
                    'value'    => set_value('kddiv'),
                    'name'     => 'kddiv',
                    'required'    => '',
                    'placeholder' => 'Cabang',
            ), 
        );
    }

    public function jmbkl() {
        //if(isset($_FILES["file"]["name"])){
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $value = array();

                for($row=2; $row<=$highestRow; $row++){
                    $no                 = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $noref              = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $ket                = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $akun               = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $debet              = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $kredit             = $worksheet->getCellByColumnAndRow(5, $row)->getValue(); 


                    

                    if(empty($no)){  //jika kode tidak null -> simpan

                        $value[] = array('empty');
                    } else {
                        //jika deskripsi, tipe, harga = null
                        if(empty($no)){$no = '';}
                        if(empty($noref)){$noref = '';}
                        if(empty($ket)){$ket = '';}
                        if(empty($akun)){$akun = '';}
                        if(empty($debet)){$debet = 0;}  
                        if(empty($kredit)){$kredit = 0;}  

                        $value[] = array(
                            'no'                => $no,
                            'noref'             => $noref,
                            'ket'               => $ket,
                            'akun'              => $akun,//toString($akun),
                            'debet'             => $debet,
                            'kredit'            => $kredit
                        );
                        // $res = $this->impjmbkl_qry->insertBeli($data);
                        // echo $debet;
                    }
                }
                // echo $value;
                // if($values[1] = 'empty'){
                //     alert("Gagal");
                // } else { 
                    // $res = $value->result_array(); 
                    return json_encode($value);
                    // return $value;
                // }
                
            //}
            }
            // return $data;
    } 

    public function importJm() {
        if(isset($_FILES["file"]["name"])){
            $data = $this->jmbkl();

            echo $this->impjmbkl_qry->insertBeli(json_decode($data));
        }
    } 
}

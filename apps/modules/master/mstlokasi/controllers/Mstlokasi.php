<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstlokasi
 *
 * @author adi
 */
class Mstlokasi extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstlokasi/submit'),
            'add' => site_url('mstlokasi/add'),
            'edit' => site_url('mstlokasi/edit'),
            'reload' => site_url('mstlokasi'),
        );
        $this->load->model('mstlokasi_qry'); 
        $this->data['jenis'] = array(
            "" => "-- Pilih Jenis --",
            "PAMERAN" => "PAMERAN",
            "GUDANG" => "GUDANG",
            "POS" => "POS",
          );

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstlokasi_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->mstlokasi_qry->getKategori();
    }*/
    
    public function submit() {  
        echo $this->mstlokasi_qry->submit(); 
    }

    // public function update() {
    //     echo $this->mstlokasi_qry->update();
    // }

    public function delete() {
        echo $this->mstlokasi_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdlokasi'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Lokasi',
                    'id'          => 'kdlokasi',
                    'name'        => 'kdlokasi',
                    'value'       => set_value('kdlokasi'),
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmlokasi'=> array(
                    'placeholder' => 'Nama Lokasi',
                    'id'      => 'nmlokasi',
                    'name'        => 'nmlokasi',
                    'value'       => set_value('nmlokasi'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           'status'=> array(
                    'placeholder' => 'Status',
                    'type'        => 'hidden',
                    'id'          => 'status',
                    'name'        => 'status',
                    'value'       => set_value('status'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'jenis'=> array(
                    'attr'        => array(
                        'id'    => 'jenis',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['jenis'],
                    'value'    => set_value('jenis'),
                    'name'     => 'jenis',
                    'placeholder' => 'Jenis',
                    'required' => ''
            ),
      		'faktif'=> array(
                    'placeholder' => '',
      				'id'          => 'faktif',
      				'value'       => set_value('faktif'),
      				'checked'     => $faktif,
      				'class'       => 'custom-control-input',
      				'name'		  => 'faktif',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdlokasi'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Lokasi',
                    'id'          => 'kdlokasi',
                    'name'        => 'kdlokasi',
                    'value'       => $this->val[0]['kdlokasi'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmlokasi'=> array(
                    'placeholder' => 'Nama Lokasi',
                    'id'          => 'nmlokasi',
                    'name'        => 'nmlokasi',
                    'value'       => $this->val[0]['nmlokasi'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
           'status'=> array(
                    'placeholder' => 'Status',
                    'type'        => 'hidden',
                    'id'          => 'status',
                    'name'        => 'status',
                    'value'       => $this->val[0]['status'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'jenis'=> array(
                    'attr'        => array(
                        'id'    => 'jenis',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['jenis'],
                    'value'       => $this->val[0]['jenis'],
                    'name'     => 'jenis',
                    'placeholder' => 'Jenis',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => 'Status Lokasi',
      					'id'          => 'faktif',
                        'value'       => $this->val[0]['faktif'],
      					'checked'     => $faktifx,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstlokasi_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdlokasi) {
        if(!empty($kdaks)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdlokasi',
                    'label' => 'Kode Lokasi',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmlokasi',
                    'label' => 'Nama Lokasi',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Slnew_qry
 *
 * @author adi
 */
class Slnew_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function set_faktur() {
      $nopo = $this->input->post('nopo');
      $query = $this->db->query("select a.*,case when a.jnsbayar = 'TUNAI' then 'T' else 'F' end as tpbyr, case when a.progres = 1 then 'SUDAH PROSES FAKTUR' else 'BELUM FAKTUR' end as ket from pzu.vr_po a where a.nopo =  '".$nopo."'");
      // echo $this->db->last_query();
      // $res = $q->result_array();
      // return json_encode($res);
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function caripo() {
      $nopo = $this->input->post('nopo');
      $query = $this->db->query("select nopo from pzu.t_po where nopo = '" . $nopo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_apispk() {
      $nospk = $this->input->post('nospk');
      $noso = $this->input->post('noso');
      $query = $this->db->query("select * from api.v_so where idspk =  '".$nospk."' AND noso = '' UNION select * from api.v_so where idspk =  '".$nospk."' AND noso = '" .$noso. "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function getKdSup() {
      // $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.v_supplier");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getSLHSO() {
      $noslhso = $this->input->post('noslhso');
      $nopo = $this->input->post('nopo');
      $query = $this->db->query("select * from api.v_po where nopo is null or nopo = '".$nopo."'");
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getSLHSO2() { 
      $noslhso = $this->input->post('noslhso');
      $nopo = $this->input->post('nopo');
      $query = $this->db->query("select * from api.v_po where nopo is null and noshippinglist != '".$noslhso."'");
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getSLHSO3() {
      $nosl = $this->input->post('nosl');
      $noslhso = $this->input->post('noslhso');
      $nopo = $this->input->post('nopo');
      $query = $this->db->query("select * from api.v_po where nopo is null and noshippinglist != '".$noslhso."' and noshippinglist != '".$nosl."'"  );
      // echo $this->db->last_query();  
      $res = $query->result_array();
      return json_encode($res);
    }

    public function getTipeUnit() {
      $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.v_tipe");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKdWarna() {
      $noso = $this->input->post('noso');
      // $this->db->where("faktif","true");
      $q = $this->db->get("pzu.warna");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function kdlokasi() {
      $this->db->where("faktif","true");
      $this->db->where("kduser",$this->session->userdata('username'));
      $q = $this->db->get("pzu.user");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKodeTipeHeader() {
      $kdtipe = $this->input->post('kdtipe');
      // $this->db->where("faktif","true");
      $this->db->where("kode",$kdtipe);
      $q = $this->db->get("pzu.v_tipe");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKdWarnaHeader() {
      $kdwarna = $this->input->post('kdwarna');
      // $this->db->where("faktif","true");
      $this->db->where("kdwarna",$kdwarna);
      $q = $this->db->get("pzu.warna");
      // echo $this->db->last_query();
      $res = $q->result_array();
      return json_encode($res);
    }

    public function getKodeSpkHso() {
      $noso = $this->input->post('noso');
      $query = $this->db->query("select a.* from api.so a left join pzu.t_so b on a.noso = b.noso where and a.idspk = '".$noso."' b.noso is null");
      echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.get_divisi()");
        return $q->result_array();
    }

    public function deleted() {
        $nosin = $this->input->post('nosin');
        $q = $this->db->query("select title,msg,tipe from pzu.po_d_del('". $nosin ."','". $this->session->userdata('username') ."' )");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updatepo() {
      $nopo           = $this->input->post('nopo');
      $tglpo          = $this->apps->dateConvert($this->input->post('tglpo'));
      $tgltrm          = $this->apps->dateConvert($this->input->post('tgltrm'));
      $kdsup          = $this->input->post('kdsup');
      $noslhso         = $this->input->post('noslhso');
      $tpbayar           = $this->input->post('tpbayar');
      $jth_tmp         = $this->input->post('jth_tmp');
      $kdlokasi         = $this->input->post('kdlokasi');
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_upd('" . $nopo . "',
                                                                      '" . $tglpo . "',
                                                                      '" . $tgltrm . "',
                                                                      '" . $this->session->userdata('data')['kddiv'] ."',
                                                                      " . $kdsup . ",
                                                                      '" . $noslhso . "',
                                                                      '" . $tpbayar . "',
                                                                      " . $jth_tmp . ",
                                                                      " . $kdlokasi.",
                                                                      '" . $this->session->userdata('username') ."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updatepo2() {
      $nopo           = $this->input->post('nopo');
      $tglpo          = $this->apps->dateConvert($this->input->post('tglpo'));
      $tgltrm          = $this->apps->dateConvert($this->input->post('tgltrm'));
      $kdsup          = $this->input->post('kdsup');
      $noslhso         = $this->input->post('noslhso');
      $noslhso2         = $this->input->post('noslhso2');
      $tpbayar           = $this->input->post('tpbayar');
      $jth_tmp         = $this->input->post('jth_tmp');
      $kdlokasi         = $this->input->post('kdlokasi');
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_upd('" . $nopo . "',
                                                                      '" . $tglpo . "',
                                                                      '" . $tgltrm . "',
                                                                      '" . $this->session->userdata('data')['kddiv'] ."',
                                                                      " . $kdsup . ",
                                                                      '" . $noslhso . "',
                                                                      '" . $noslhso2 . "',
                                                                      '" . $tpbayar . "',
                                                                      " . $jth_tmp . ",
                                                                      " . $kdlokasi.",
                                                                      '" . $this->session->userdata('username') ."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function updatepo3() {
      $nopo           = $this->input->post('nopo');
      $tglpo          = $this->apps->dateConvert($this->input->post('tglpo'));
      $tgltrm          = $this->apps->dateConvert($this->input->post('tgltrm'));
      $kdsup          = $this->input->post('kdsup');
      $noslhso         = $this->input->post('noslhso');
      $noslhso2         = $this->input->post('noslhso2');
      $noslhso3         = $this->input->post('noslhso3');
      $tpbayar           = $this->input->post('tpbayar');
      $jth_tmp         = $this->input->post('jth_tmp');
      $kdlokasi         = $this->input->post('kdlokasi');
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_upd('" . $nopo . "',
                                                                      '" . $tglpo . "',
                                                                      '" . $tgltrm . "',
                                                                      '" . $this->session->userdata('data')['kddiv'] ."',
                                                                      " . $kdsup . ",
                                                                      '" . $noslhso . "',
                                                                      '" . $noslhso2 . "',
                                                                      '" . $noslhso3 . "',
                                                                      '" . $tpbayar . "',
                                                                      " . $jth_tmp . ",
                                                                      " . $kdlokasi.",
                                                                      '" . $this->session->userdata('username') ."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function addpo() {
      $nopo           = $this->input->post('nopo');
      $tglpo          = $this->apps->dateConvert($this->input->post('tglpo'));
      $tgltrm          = $this->apps->dateConvert($this->input->post('tgltrm'));
      $kdsup          = $this->input->post('kdsup');
      $noslhso         = $this->input->post('noslhso');
      $tpbayar           = $this->input->post('tpbayar');
      $jth_tmp         = $this->input->post('jth_tmp');
      $kdlokasi1         = $this->input->post('kdlokasi');
      if($kdlokasi1===''){
        $kdlokasi = 1 ;
      } else {
        $kdlokasi = $kdlokasi1;
      }
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_ins('" . $nopo . "',
                                                                      '" . $tglpo . "',
                                                                      '" . $tgltrm . "',
                                                                      '" . $this->session->userdata('data')['kddiv'] ."',
                                                                      " . $kdsup . ",
                                                                      '" . $noslhso . "',
                                                                      '" . $tpbayar . "',
                                                                      " . $jth_tmp . ",
                                                                      " . $kdlokasi.",
                                                                      '" . $this->session->userdata('username') ."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function addpo2() {
      $nopo           = $this->input->post('nopo');
      $tglpo          = $this->apps->dateConvert($this->input->post('tglpo'));
      $tgltrm          = $this->apps->dateConvert($this->input->post('tgltrm'));
      $kdsup          = $this->input->post('kdsup');
      $noslhso         = $this->input->post('noslhso');
      $noslhso2         = $this->input->post('noslhso2');
      $tpbayar           = $this->input->post('tpbayar');
      $jth_tmp         = $this->input->post('jth_tmp');
      $kdlokasi1         = $this->input->post('kdlokasi');
      if($kdlokasi1===''){
        $kdlokasi = 1 ;
      } else {
        $kdlokasi = $kdlokasi1;
      }
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_ins('" . $nopo . "',
                                                                      '" . $tglpo . "',
                                                                      '" . $tgltrm . "',
                                                                      '" . $this->session->userdata('data')['kddiv'] ."',
                                                                      " . $kdsup . ",
                                                                      '" . $noslhso . "',
                                                                      '" . $noslhso2 . "',
                                                                      '" . $tpbayar . "',
                                                                      " . $jth_tmp . ",
                                                                      " . $kdlokasi.",
                                                                      '" . $this->session->userdata('username') ."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function addpo3() {
      $nopo           = $this->input->post('nopo');
      $tglpo          = $this->apps->dateConvert($this->input->post('tglpo'));
      $tgltrm          = $this->apps->dateConvert($this->input->post('tgltrm'));
      $kdsup          = $this->input->post('kdsup');
      $noslhso         = $this->input->post('noslhso');
      $noslhso2         = $this->input->post('noslhso2');
      $noslhso3         = $this->input->post('noslhso3');
      $tpbayar           = $this->input->post('tpbayar');
      $jth_tmp         = $this->input->post('jth_tmp');
      $kdlokasi1         = $this->input->post('kdlokasi');
      if($kdlokasi1===''){
        $kdlokasi = 1 ;
      } else {
        $kdlokasi = $kdlokasi1;
      }
      // $npwp   = '';

        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_ins('" . $nopo . "',
                                                                      '" . $tglpo . "',
                                                                      '" . $tgltrm . "',
                                                                      '" . $this->session->userdata('data')['kddiv'] ."',
                                                                      " . $kdsup . ",
                                                                      '" . $noslhso . "',
                                                                      '" . $noslhso2 . "',
                                                                      '" . $noslhso3 . "',
                                                                      '" . $tpbayar . "',
                                                                      " . $jth_tmp . ",
                                                                      " . $kdlokasi.",
                                                                      '" . $this->session->userdata('username') ."')");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function deletepo() {
        $nopo = $this->input->post('nopo');
        $q = $this->db->query("select title,msg,tipe from pzu.po_hso_del('". $nopo ."','". $this->session->userdata('username') ."' )");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function getdetHSO() {
        error_reporting(-1);
        if( isset($_GET['noslhso']) ){
            if($_GET['noslhso']){
                $noslhso = $_GET['noslhso'];
            }else{
                $noslhso = '';
            }
        }else{
            $noslhso = '';
        }

        $aColumns = array('nomesin',
                          'kodetipeunit',
                          'kdtipe',
                          'nmtipe',
                          'nmwarna',
                          'nosin',
                          'nora',
                          'tahun',
                          'statdo');
      $sIndexColumn = "nomesin";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select noshippinglist, nomesin, kodetipeunit, kdtipe, nmtipe, kodewarna, nmwarna, nosin, nora, tahun, statdo, nogoodsreceipt from api.v_po_d where noshippinglist = '".$noslhso."' order by tahun ) AS a";


        //$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
        $sWhere = "";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
          if($_GET['iDisplayStart']>0){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                intval( $_GET['iDisplayStart'] );
          }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
          $sOrder = " ORDER BY  ";
          for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
          {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
          }

          $sOrder = substr_replace( $sOrder, "", -2 );
          if ( $sOrder == " ORDER BY" )
          {
              $sOrder = "";
          }
        }

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = "";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
            if ( $sWhere == "" )
            {
              $sWhere = " WHERE ";
            }
            else
            {
              $sWhere .= " AND ";
            }
            //echo $sWhere."<br>";
            $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
        }


        /*
         * SQL queries
         */
        $sQuery = "
            SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";

            //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere"; //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo json_encode( $output );
    }

    public function getdetHSO2() {
        error_reporting(-1);
        if( isset($_GET['noslhso']) ){
            if($_GET['noslhso']){
                $noslhso = $_GET['noslhso'];
            }else{
                $noslhso = '';
            }
        }else{
            $noslhso = '';
        }
        if( isset($_GET['noslhso2']) ){
            if($_GET['noslhso2']){
                $noslhso2 = $_GET['noslhso2'];
            }else{
                $noslhso2 = '';
            }
        }else{
            $noslhso2 = '';
        }

        $aColumns = array('nomesin',
                          'kodetipeunit',
                          'kdtipe',
                          'nmtipe',
                          'nmwarna',
                          'nosin',
                          'nora',
                          'tahun',
                          'statdo');
      $sIndexColumn = "nomesin";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select noshippinglist, nomesin, kodetipeunit, kdtipe, nmtipe, kodewarna, nmwarna, nosin, nora, tahun, statdo, nogoodsreceipt from api.v_po_d where noshippinglist = '".$noslhso."' union select noshippinglist, nomesin, kodetipeunit, kdtipe, nmtipe, kodewarna, nmwarna, nosin, nora, tahun, statdo, nogoodsreceipt from api.v_po_d where noshippinglist = '".$noslhso2."' order by tahun ) AS a";


        //$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
        $sWhere = "";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
          if($_GET['iDisplayStart']>0){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                intval( $_GET['iDisplayStart'] );
          }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
          $sOrder = " ORDER BY  ";
          for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
          {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
          }

          $sOrder = substr_replace( $sOrder, "", -2 );
          if ( $sOrder == " ORDER BY" )
          {
              $sOrder = "";
          }
        }

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = "";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
            if ( $sWhere == "" )
            {
              $sWhere = " WHERE ";
            }
            else
            {
              $sWhere .= " AND ";
            }
            //echo $sWhere."<br>";
            $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
        }


        /*
         * SQL queries
         */
        $sQuery = "
            SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";

            //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere"; //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo json_encode( $output );
    }

    public function getdetHSO3() {
        error_reporting(-1);
        if( isset($_GET['noslhso']) ){
            if($_GET['noslhso']){
                $noslhso = $_GET['noslhso'];
            }else{
                $noslhso = '';
            }
        }else{
            $noslhso = '';
        }
        if( isset($_GET['noslhso2']) ){
            if($_GET['noslhso2']){
                $noslhso2 = $_GET['noslhso2'];
            }else{
                $noslhso2 = '';
            }
        }else{
            $noslhso2 = '';
        }
        if( isset($_GET['noslhso3']) ){
            if($_GET['noslhso3']){
                $noslhso3 = $_GET['noslhso3'];
            }else{
                $noslhso3 = '';
            }
        }else{
            $noslhso3 = '';
        }

        $aColumns = array('nomesin',
                          'kodetipeunit',
                          'kdtipe',
                          'nmtipe',
                          'nmwarna',
                          'nosin',
                          'nora',
                          'tahun',
                          'statdo');
      $sIndexColumn = "nomesin";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( select noshippinglist, nomesin, kodetipeunit, kdtipe, nmtipe, kodewarna, nmwarna, nosin, nora, tahun, statdo, nogoodsreceipt from api.v_po_d where noshippinglist = '".$noslhso."' union select noshippinglist, nomesin, kodetipeunit, kdtipe, nmtipe, kodewarna, nmwarna, nosin, nora, tahun, statdo, nogoodsreceipt from api.v_po_d where noshippinglist = '".$noslhso2."' union select noshippinglist, nomesin, kodetipeunit, kdtipe, nmtipe, kodewarna, nmwarna, nosin, nora, tahun, statdo, nogoodsreceipt from api.v_po_d where noshippinglist = '".$noslhso3."' order by tahun ) AS a";


        //$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
        $sWhere = "";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
          if($_GET['iDisplayStart']>0){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                intval( $_GET['iDisplayStart'] );
          }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
          $sOrder = " ORDER BY  ";
          for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
          {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
          }

          $sOrder = substr_replace( $sOrder, "", -2 );
          if ( $sOrder == " ORDER BY" )
          {
              $sOrder = "";
          }
        }

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = "";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
            if ( $sWhere == "" )
            {
              $sWhere = " WHERE ";
            }
            else
            {
              $sWhere .= " AND ";
            }
            //echo $sWhere."<br>";
            $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
        }


        /*
         * SQL queries
         */
        $sQuery = "
            SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";

            //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere"; //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo json_encode( $output );
    }

    public function getdetPO() {
        error_reporting(-1);
        if( isset($_GET['nopo']) ){
            if($_GET['nopo']){
                $nopo = $_GET['nopo'];
            }else{
                $nopo = '';
            }
        }else{
            $nopo = '';
        }

        $aColumns = array('nosin',
                          'kode',
                          'kdtipe',
                          'nmtipe',
                          'warna',
                          'nosin',
                          'nora',
                          'tahun',
                          'status_do');
      $sIndexColumn = "nosin";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select nopo, kode, kdtipe, nmtipe, warna, nosin, nora, tahun, status_do from pzu.vr_po_d where nopo = '".$nopo."' order by tahun ) AS a";


        //$sWhere = "WHERE (to_char(tglkasbon,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";
        $sWhere = "";

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
          if($_GET['iDisplayStart']>0){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                intval( $_GET['iDisplayStart'] );
          }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
          $sOrder = " ORDER BY  ";
          for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
          {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
          }

          $sOrder = substr_replace( $sOrder, "", -2 );
          if ( $sOrder == " ORDER BY" )
          {
              $sOrder = "";
          }
        }

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
        $sWhere = " AND ";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
            if ( $sWhere == "" )
            {
              $sWhere = " WHERE ";
            }
            else
            {
              $sWhere .= " AND ";
            }
            //echo $sWhere."<br>";
            $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
        }


        /*
         * SQL queries
         */
        $sQuery = "
            SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";

            //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";	//SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") AS jml
            FROM $sTable
            $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['aaData'][] = $aRow;
        }
        echo json_encode( $output );
    }

}

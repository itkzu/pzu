<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger"> 
          <div class="col-xs-12 box-header box-view">
              <div class="col-xs-3">
                <div class="row">
                    <button class="btn btn-primary btn-rev">Revisi</button> 
                </div>
              </div> 
          </div>  
          <div class="col-xs-12 box-header box-simpan">
              <div class="col-xs-3">
                <div class="row">
                    <button class="btn btn-primary btn-save">Simpan</button>
                    <button class="btn btn-primary btn-btl">Batal</button> 
                </div>
              </div> 
          </div>  

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['nobpkb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                        echo form_dropdown($form['nobpkb']['name'],
                                                                           $form['nobpkb']['data'],
                                                                           $form['nobpkb']['value'],
                                                                           $form['nobpkb']['attr']);
                                        echo form_error('nobpkb', '<div class="note">', '</div>');  
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgltrm']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['tgltrm']);
                                        echo form_error('tgltrm','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['nama']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                        echo form_input($form['nama']);
                                        echo form_error('nama','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['noso']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['noso']);
                                        echo form_error('noso','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div>

                        <div class="col-xs-2 col-md-2">
                          <div class="row"> 

                              <div class="col-xs-12"> 
                                    <?php
                                        echo form_input($form['tglso']);
                                        echo form_error('tglso','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div>

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['alamat']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['alamat']);
                                          echo form_error('alamat','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-5">
                                  <div class="form-group">
                                      <?php echo form_label($form['nodo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-7"> 
                                    <?php
                                        echo form_input($form['nodo']);
                                        echo form_error('nodo','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div>

                        <div class="col-xs-2 col-md-2">
                          <div class="row">

                              <div class="col-xs-12"> 
                                    <?php
                                        echo form_input($form['tgldo']);
                                        echo form_error('tgldo','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div>

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-1 col-md-1 ">
                          <div class="row"> 
                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kel']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['kel']);
                                          echo form_error('kel','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kec']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['kec']);
                                        echo form_error('kec','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-1 col-md-1 ">
                          <div class="row"> 
                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kota']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                      <?php
                                          echo form_input($form['kota']);
                                          echo form_error('kota','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-3 col-md-3">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['nohp']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['nohp']);
                                        echo form_error('nohp','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row">
                      <br>
                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row"> 

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['nosin']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['nosin']);
                                          echo form_error('nosin','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-5"> 
                                      <?php
                                          echo form_input($form['nora']);
                                          echo form_error('nora','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-5 col-md-5">
                          <div class="row">

                              <div class="col-xs-6">
                                  <div class="form-group">
                                      <?php echo form_label($form['byk_revbpkb']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-2"> 
                                    <?php
                                        echo form_input($form['byk_revbpkb']);
                                        echo form_error('byk_revbpkb','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row"> 

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['kdtipe']);
                                          echo form_error('kdtipe','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-5"> 
                                      <?php
                                          echo form_input($form['nmtipe']);
                                          echo form_error('nmtipe','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div>

                        <div class="col-xs-5 col-md-5">
                          <div class="row">

                              <div class="col-xs-6">
                                  <div class="form-group">
                                      <?php echo form_label($form['tglrevakhir']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                    <?php
                                        echo form_input($form['tglrevakhir']);
                                        echo form_error('tglrevakhir','<div class="note">','</div>');
                                    ?> 
                              </div>
                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row"> 

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['warna']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-6"> 
                                      <?php
                                          echo form_input($form['warna']);
                                          echo form_error('warna','<div class="note">','</div>');
                                      ?> 
                              </div>

                              <div class="col-xs-3"> 
                                      <?php
                                          echo form_input($form['tahun']);
                                          echo form_error('tahun','<div class="note">','</div>');
                                      ?> 
                              </div>

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">
                      <br>
                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row"> 

                        <div class="col-xs-7 col-md-7">
                          <div class="row">

                              <div class="col-xs-3">
                                  <div class="form-group">
                                      <?php echo form_label($form['ket_rev']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-9"> 
                                    <?php
                                        echo form_input($form['ket_rev']);
                                        echo form_error('ket_rev','<div class="note">','</div>');
                                    ?> 
                              </div>  
                          </div>
                        </div> 

                    </div>
                  </div> 

              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        reset();
        // $("#noajustnk").mask("9999/PZUS-KPS/99/99"); 
        disabled();  

        init_search();
        $(".btn-rev").prop('disabled',true); 

        $('#nobpkb').change(function(){
          setbpkb();
        });
        $('.btn-rev').click(function(){
          enabled();
        });
        $('.btn-btl').click(function(){
          window.location.href = '<?=site_url('revbpkb');?>';
        });
        $('.btn-save').click(function(){
          if($('#ket_rev').val()===''){
            swal({
              title: "Data tidak boleh kosong",
              text: "",
              type: "error"
            }, function(){
              $('#nobpkb').focus();
              $('#nobpkb').css("border", "2px solid red");
            }); 
          } else {
              $('#nostnk').blur();
              $('#nostnk').css("border", "1px solid gainsboro");
              submit(); 
          } 
        });
    });

    function disabled(){  
      $("#ket_rev").attr('disabled',true); 
      $(".box-view").show();
      $(".box-simpan").hide(); 
    }

    function enabled(){  
      $("#ket_rev").attr('disabled',false); 
      $(".box-view").hide();
      $(".box-simpan").show(); 
    }

    function reset(){
      $("#nobpkb").val('');
      $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#noso").val('');
      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));

      $("#nama").val('');
      $("#alamat").val('');
      $("#kel").val('');
      $("#kec").val('');
      $("#kota").val('');
      $("#nohp").val('');

      $("#nosin").val('');
      $("#nora").val('');
      $("#kdtipe").val('');
      $("#nmtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#notelp").val('');
 
      $("#byk_revbpkb").val('0 x');
      $("#tglrevakhir").val($.datepicker.formatDate('dd-mm-yy', new Date()));
      $("#ket_rev").val('');
    }

    function init_search(){
        $("#nobpkb").select2({
            ajax: {
                url: "<?=site_url('revbpkb/getDtbpkb');?>",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
          processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan No BPKB / No DO / No Mesin / Nama Konsumen / ...',
            dropdownAutoWidth : true, 
            width: '100%',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 3,
                        templateResult: format_nama, // omitted for brevity, see the source of this page
                        templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
                    });
    }

    function format_nama_terpilih (repo) {
        return repo.full_name || repo.id;
    }

    function format_nama (repo) {
        if (repo.loading) return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.nama + "</b></div>";

        markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.id +
        "</div>" +
        "<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.text +
        "</div>" +
        "<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nosin +
        "</div>" +
        "</div>" +
        "</div>";
        return markup;
    }

    function setbpkb(){

      var nobpkb = $("#nobpkb").val();
      var nobpkb = nobpkb.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("revbpkb/setbpkb");?>",
            data: {"nobpkb":nobpkb },
            success: function(resp){  
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){ 

                    $("#tgltrm").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgl_trm_bpkb)));
                    
                    $("#nama").val(data.nama_s);
                    $("#noso").val(data.noso);
                    $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                    $("#alamat").val(data.alamatktp_s);
                    $("#nodo").val(data.nodo);
                    $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                    $("#kel").val(data.kel_s);
                    $("#kec").val(data.kec_s);
                    $("#kota").val(data.kota_s);
                    $("#nohp").val(data.nohp_s);

                    $("#nosin").val(data.nosin);
                    $("#nora").val(data.nora);
                    $("#kdtipe").val(data.kdtipe);
                    $("#nmtipe").val(data.nmtipe);
                    $("#warna").val(data.warna);
                    $("#tahun").val(data.warna);

                    $("#byk_revbpkb").val(data.revke_bpkb+" x");
                    $("#tglrevakhir").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglrev_bpkb)));
                    $("#ket_rev").val(data.ketrev_bpkb); 
                    $(".btn-rev").prop('disabled',false);  
                  }); 
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () { 
            var nodo    = $("#nodo").val();
            var nodo    = nodo.toUpperCase();   
            var ket    = $("#ket_rev").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("revbpkb/submit");?>",
                data: {"nodo":nodo 
                      ,"ket":ket},
                success: function(resp){
                    var obj = JSON.parse(resp);
                        if(obj.skbbn_kons_ins=1){
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                                window.location.href = '<?=site_url('revbpkb');?>';
                            }); 
                        }else{ 
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        } 
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }
</script>

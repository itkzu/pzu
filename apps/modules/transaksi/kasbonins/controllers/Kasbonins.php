<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Kasbonins
 *
 * @author adi
 */
class Kasbonins extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('kasbonins/submit'),
            'add' => site_url('kasbonins/add'),
            'edit' => site_url('kasbonins/edit'),
            'ctk'      => site_url('kasbonins/ctk'),
            'reload' => site_url('kasbonins'),
        );
        $this->load->model('kasbonins_qry');
        $kdkb = $this->kasbonins_qry->getKB();
        foreach ($kdkb as $value) {
            $this->data['kdkb'][$value['kdkb']] = $value['nmkb'];
        } 
        $jnstrx = $this->kasbonins_qry->getjnstrans();
        foreach ($jnstrx as $value) {
            $this->data['kdjkasbon'][$value['kdjkasbon']] = $value['nmjkasbon'];
        }  
        // $kdleas = $this->kasbonins_qry->getleas();
        // foreach ($kdleas as $value) {
        //     $this->data['kdleasing'][$value['kdleasing']] = $value['kdleasing'];
        // }     

        //manual
        $this->data['jnsrleas'] = array(
            "MX" => "REFUND BELAKANG (MX)",
            "JP" => "REFUND DEPAN (JP)"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk() {
        $nokps = $this->uri->segment(3);   
        $this->data['data'] = $this->kasbonins_qry->ctk($nokps);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function tambah() {  
        echo $this->kasbonins_qry->tambah(); 
    }

    public function nokasbon() {
        echo $this->kasbonins_qry->nokasbon();
    }

    public function set_kasbon() {
        echo $this->kasbonins_qry->set_kasbon();
    }

    public function json_dgview() {
        echo $this->kasbonins_qry->json_dgview();
    }

    public function det_json_dgview() {
        echo $this->kasbonins_qry->det_json_dgview();
    }

    //yang lalu

    public function getNoID() {
        echo $this->kasbonins_qry->getNoID();
    }

    public function getKdjenis() {
        echo $this->kasbonins_qry->getKdjenis();
    }

    public function getKdleas() {
        echo $this->kasbonins_qry->getKdleas();
    }

    public function set_tbj() {
        echo $this->kasbonins_qry->set_tbj();
    }

    public function set_tglbbn() {
        echo $this->kasbonins_qry->set_tglbbn();
    }

    public function set_pleas() {
        echo $this->kasbonins_qry->set_pleas();
    }

    public function set_rleas() {
        echo $this->kasbonins_qry->set_rleas();
    }

    public function set_kbm() {
        echo $this->kasbonins_qry->set_kbm();
    }

    public function set_blkkons() {
        echo $this->kasbonins_qry->set_blkkons();
    }

    public function set_blkkonsnoso() {
        echo $this->kasbonins_qry->set_blkkonsnoso();
    }

    public function set_blkkonsnodo() {
        echo $this->kasbonins_qry->set_blkkonsnodo();
    }

    public function refkb() {
        echo $this->kasbonins_qry->refkb();
    }

    public function set_pkonsnoso() {
        echo $this->kasbonins_qry->set_pkonsnoso();
    }

    public function set_pkonsnodo() {
        echo $this->kasbonins_qry->set_pkonsnodo();
    }

    public function set_pleasnoso() {
        echo $this->kasbonins_qry->set_pleasnoso();
    }

    public function set_pleasnodo() {
        echo $this->kasbonins_qry->set_pleasnodo();
    }

    public function set_nocetak() {
        echo $this->kasbonins_qry->set_nocetak();
    }

    public function set_jmlctk() {
        echo $this->kasbonins_qry->set_jmlctk();
    }

    public function submit() {
        echo $this->kasbonins_qry->submit();
    }

    public function pleas_submit() {
        echo $this->kasbonins_qry->pleas_submit();
    }

    public function rleas_submit() {
        echo $this->kasbonins_qry->rleas_submit();
    }

    public function jenis() {
        echo $this->kasbonins_qry->jenis();
    } 

    public function batal() {
        echo $this->kasbonins_qry->batal();
    }

    public function delDetail() {
        echo $this->kasbonins_qry->delDetail();
    }  

    public function get_pkons() {
        echo $this->kasbonins_qry->get_pkons();
    }

    public function get_pleas() {
        echo $this->kasbonins_qry->get_pleas();
    }

    public function get_blkkons() {
        echo $this->kasbonins_qry->get_blkkons();
    }

    public function kbm_submit() {
        echo $this->kasbonins_qry->kbm_submit();
    }

    public function kbm_del() {
        echo $this->kasbonins_qry->kbm_del();
    }

    public function kbsubmit() {
        echo $this->kasbonins_qry->kbsubmit();
    }

    public function proses_dk() {
        echo $this->kasbonins_qry->proses_dk();
    } 

    private function _init_add(){ 
        $this->data['form'] = array(
            //header
           'nokasbon'=> array(
                    'placeholder' => 'No. Kasbon',
                    //'type'        => 'hidden',
                    'id'          => 'nokasbon',
                    'name'        => 'nokasbon',
                    'value'       => set_value('nokasbon'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'tglkasbon'=> array(
                    'placeholder' => 'Tanggal Kasbon',
                    //'type'        => 'hidden',
                    'id'          => 'tglkasbon',
                    'name'        => 'tglkasbon',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly'    => '',
            ), 
            'refkb'=> array(
                    'placeholder' => 'Kas/Bank',
                    'attr'        => array(
                        'id'    => 'refkb',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdkb'],
                    'value'    => set_value('refkb'),
                    'name'     => 'refkb',
                    'readonly'    => '',
            ),
            'kdjkasbon'=> array(
                    'placeholder' => 'Jenis Kasbon',
                    'attr'        => array(
                        'id'    => 'kdjkasbon',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdjkasbon'], //$this->data['kdsales_ins'],
                    'value'    => set_value('kdjkasbon'),
                    'name'     => 'kdjkasbon',
                    'readonly'    => '',
            ),
            'penerima'=> array(
                    'placeholder' => 'Penerima',
                    //'type'        => 'hidden',
                    'id'          => 'penerima',
                    'name'        => 'penerima',
                    'value'       => set_value('penerima'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'ket'=> array(
                    'placeholder' => 'Keterangan', 
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'nilai'=> array(
                    'placeholder' => 'Nilai Kasbon', 
                    'id'          => 'nilai',
                    'name'        => 'nilai',
                    'value'       => set_value('nilai'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    'readonly'    => '',
            ), 
            'tglpelaksanaan'=> array(
                    'placeholder' => 'Tgl. Pelaksanaan',
                    //'type'        => 'hidden',
                    'id'          => 'tglpelaksanaan',
                    'name'        => 'tglpelaksanaan', 
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),  

            //detail Tagihan BBN
                'notagbbn'=> array(
                        'placeholder' => 'No. Tagihan BBN',
                        //'type'        => 'hidden',
                        'id'          => 'notagbbn',
                        'name'        => 'notagbbn',
                        'value'       => set_value('notagbbn'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'tgltagbbn'=> array(
                        'placeholder' => 'Tgl. Tagihan BBN',
                        //'type'        => 'hidden',
                        'id'          => 'tgltagbbn',
                        'name'        => 'tgltagbbn', 
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control ',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),   

        );
    }
    
    private function _init_edit(){
        // if(!$no){
        //     $nokb = $this->uri->segment(3);
        // }
        // $this->_check_id($nokb); 
        $this->data['form'] = array(
            //header
           'nokasbon'=> array(
                    'placeholder' => 'No. Kasbon',
                    //'type'        => 'hidden',
                    'id'          => 'nokasbon',
                    'name'        => 'nokasbon',
                    'value'       => set_value('nokasbon'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'tglkasbon'=> array(
                    'placeholder' => 'Tanggal Kasbon',
                    //'type'        => 'hidden',
                    'id'          => 'tglkasbon',
                    'name'        => 'tglkasbon',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control ',
                    'style'       => 'text-transform: uppercase;', 
                    'readonly'    => '',
            ), 
            'refkb'=> array(
                    'placeholder' => 'Kas/Bank',
                    'attr'        => array(
                        'id'    => 'refkb',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdkb'],
                    'value'    => set_value('refkb'),
                    'name'     => 'refkb',
                    // 'readonly'    => '',
            ),
            'kdjkasbon'=> array(
                    'placeholder' => 'Jenis Kasbon',
                    'attr'        => array(
                        'id'    => 'kdjkasbon',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdjkasbon'], //$this->data['kdsales_ins'],
                    'value'    => set_value('kdjkasbon'),
                    'name'     => 'kdjkasbon',
                    // 'readonly'    => '',
            ),
            'penerima'=> array(
                    'placeholder' => 'Penerima',
                    //'type'        => 'hidden',
                    'id'          => 'penerima',
                    'name'        => 'penerima',
                    'value'       => set_value('penerima'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'ket'=> array(
                    'placeholder' => 'Keterangan', 
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ), 
            'nilai'=> array(
                    'placeholder' => 'Nilai Kasbon', 
                    'id'          => 'nilai',
                    'name'        => 'nilai',
                    'value'       => set_value('nilai'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase; text-align: right;',
                    // 'readonly'    => '',
            ), 
            'tglpelaksanaan'=> array(
                    'placeholder' => 'Tgl. Pelaksanaan',
                    //'type'        => 'hidden',
                    'id'          => 'tglpelaksanaan',
                    'name'        => 'tglpelaksanaan', 
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'text-transform: uppercase;',
                    // 'readonly'    => '',
            ),  

            //detail Tagihan BBN
                'notagbbn'=> array(
                        'placeholder' => 'No. Tagihan BBN',
                        //'type'        => 'hidden',
                        'id'          => 'notagbbn',
                        'name'        => 'notagbbn',
                        'value'       => set_value('notagbbn'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ),
                'tgltagbbn'=> array(
                        'placeholder' => 'Tgl. Tagihan BBN',
                        //'type'        => 'hidden',
                        'id'          => 'tgltagbbn',
                        'name'        => 'tgltagbbn', 
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control calendar',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ),  

            //detail kontribusi
                'kdleasing'=> array(
                        'placeholder' => 'Kontributor',
                        'attr'        => array(
                            'id'    => 'kdleasing',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                        'value'    => set_value('kdleasing'),
                        'name'     => 'kdleasing', 
                        //'type'        => 'hidden',  
                        // 'readonly'    => '',
                ),
                'cat'=> array(
                        'placeholder' => 'Catatan',
                        //'type'        => 'hidden',
                        'id'          => 'cat',
                        'name'        => 'cat', 
                        'value'       => set_value('cat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '', 
                ),
                'det_nilai'=> array(
                        'placeholder' => 'Nilai',
                        //'type'        => 'hidden',
                        'id'          => 'det_nilai',
                        'name'        => 'det_nilai', 
                        'value'       => set_value('det_nilai'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase; text-align: right;',
                        // 'readonly'    => '',
                ),    
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

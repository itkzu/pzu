<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-6">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-tags"></i> Kode Mesin Tidak Lengkap
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table id="dt1" class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="width: 100px;text-align: center;">No. SRV</th>
								<th style="width: 100px;text-align: center;">Tanggal</th>
								<th style="width: 100px;text-align: center;">Nama</th>
								<th style="width: 120px;text-align: center;">KPB ke-</th>
								<th style="width: 120px;text-align: center;">Kode Mesin</th>
							</tr>
						</thead>

						<tbody></tbody>

						<!--
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						-->

					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>


	<div class="col-xs-6">
		<div class="box box-danger">
			<div class="box-header" style="text-align: center;font-size: 22px;padding-bottom: 0px;">
				<i class="fa fa-tags"></i> Data KPB Tidak Lengkap
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table id="dt2" class="table table-bordered table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th style="width: 100px;text-align: center;">No. SRV</th>
								<th style="width: 100px;text-align: center;">Tanggal</th>
								<th style="width: 100px;text-align: center;">Nama</th>
								<th style="width: 120px;text-align: center;">Kode Mesin</th>
							</tr>
						</thead>

						<tbody></tbody>

						<!--
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						-->

					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>	
</div>


<script type="text/javascript">

	$(document).ready(function () {

		var column = [];

/*
		column.push({ 
				"aTargets": [ 0 ],
				"sClass": "center"
		});

		column.push({ 
				"aTargets": [ 1,2,3,4,5,6,7 ],
				"mRender": function (data, type, full) {
						return type === 'export' ? data : numeral(data).format('0,0');
				},
				"sClass": "right"
		});
*/

		table = $('#dt1').DataTable({
			"aoColumnDefs": column,
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"bPaginate": false,
			//"bInfo": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
							);
				$.ajax( {
					"dataType": 'json', 
					"type": "GET", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},

			"sAjaxSource": "<?=site_url('rptcekdatakpb/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//"dom": 'Bfrtip',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					 }
				}
			],
			//"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});
			

		table = $('#dt2').DataTable({
			"aoColumnDefs": column,
			"fixedColumns": {
				leftColumns: 2
			},
			"lengthMenu": [[ -1], [ "Semua Data"]],
			//"lengthMenu": [[10,25,50, 100,500,1000], [10,25,50, 100,500,1000]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"ordering": false,
			"bPaginate": false,
			//"bInfo": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
							);
				$.ajax( {
					"dataType": 'json', 
					"type": "GET", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},

			"sAjaxSource": "<?=site_url('rptcekdatakpb/json_dgview2');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//"dom": 'Bfrtip',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');
						$(win.document.body).find('table')
											.addClass('compact')
											.css('font-size', 'inherit');
					 }
				}
			],
			//"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});



		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		/*
		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" ){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
		*/
	});
</script>
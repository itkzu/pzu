<?php
/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?= base_url('assets/plugins/dtables/resource/details_open.png'); ?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?= base_url('assets/plugins/dtables/resource/details_close.png'); ?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="<?php echo $add; ?>" class="btn btn-primary">Tambah</a>
                <a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>
                <div class="box-tools pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?php echo $add; ?>" >Tambah Data Baru</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn-refersh">Refresh</a>
                            </li>
                        </ul>
                    </div>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="dataTable table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th style="width: 10px;text-align: center;">Detail</th>
                                <th style="width: 100px;text-align: center;">No. Transaksi</th>
                                <th style="width: 150px;text-align: center;">Tanggal Transaksi</th>
                                <th style="text-align: center;">Keterangan</th>
                                <th style="width: 10px;text-align: center;">Edit</th>
                                <th style="width: 10px;text-align: center;">Delete</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 10px;text-align: center;">Detail</th>
                                <th style="width: 100px;text-align: center;">No. Transaksi</th>
                                <th style="width: 150px;text-align: center;">Tanggal Transaksi</th>
                                <th style="text-align: center;">Keterangan</th>
                                <th style="width: 10px;text-align: center;">Edit</th>
                                <th style="width: 10px;text-align: center;">Delete</th>
                            </tr>
                        </tfoot>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-refersh").click(function () {
            table.ajax.reload();
        });

        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "noashp"},
                {"data": "tglashp"},
                {"data": "ket"},
                {
                    "data": "edit",
                    "orderable": false,
                },
                {
                    "data": "delete",
                    "orderable": false,
                },
            ],    
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                });
            },
            'rowCallback': function (row, data, index) {
                
            },
            "sAjaxSource": "<?= site_url('mstasethapus/json_dgview'); ?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    titleAttr: 'Excel',
                    "oSelectorOpts": {filter: 'applied', order: 'current'},
                    "sFileName": "report.xls",
                    action: function (e, dt, button, config) {
                        exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);

                    },
                    exportOptions: {orthogonal: 'export'}

                },
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // Add event listener for opening and closing details
        $('.dataTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                //format(row.data());
                tr.addClass('shown');
            }
        });

        $('.dataTable tfoot th').each(function () {
            var title = $('.dataTable thead th').eq($(this).index()).text();
            if (title !== "Edit" && title !== "Delete" && title !== "Detail") {
                $(this).html('<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />');
            } else {
                $(this).html('');
            }
        });

        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                        .search(this.value)
                        .draw();
                //}
            });
        });



        function exportTableToCSV($table, filename) {

            //rescato los títulos y las filas
            var $Tabla_Nueva = $table.find('tr:has(td,th)');
            // elimino la tabla interior.
            var Tabla_Nueva2 = $Tabla_Nueva.filter(function () {
                return (this.childElementCount != 1);
            });

            var $rows = Tabla_Nueva2,
                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    tmpColDelim = String.fromCharCode(11), // vertical tab character
                    tmpRowDelim = String.fromCharCode(0), // null character

                    // Solo Dios Sabe por que puse esta linea
                    colDelim = (filename.indexOf("xls") != -1) ? '"\t"' : '","',
                    rowDelim = '"\r\n"',
                    // Grab text from table into CSV formatted string
                    csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row);
                        var $cols = $row.find('td:not(.hidden),th:not(.hidden)');

                        return $cols.map(function (j, col) {
                            var $col = $(col);
                            var text = $col.text().replace(/\./g, '');
                            return text.replace('"', '""'); // escape double quotes

                        }).get().join(tmpColDelim);
                        csv = csv + '"\r\n"' + 'fin ' + '"\r\n"';
                    }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';


            download_csv(csv, filename);


        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            // Lanzamos
            downloadLink.click();
        }

        function format(d) {
            //console.log(d);
            var tdetail = '<table class="table table-bordered table-hover dataTableDetail">' +
                    '<thead>' +
                    '<tr style="background-color: #d73925;color: #fff;">' +
                    '<th style="text-align: center;width:16px;"></th>' +
                    '<th style="text-align: center;width:10px;">NO.</th>' +
                    '<th style="text-align: center;">KODE ASET</th>' +
                    '<th style="text-align: center;">NAMA ASET</th>' +
                    '<th style="text-align: center;">KET ASET</th>' +
                    '<th style="text-align: center;">TGL. TERIMA</th>' +
                    '<th style="text-align: center;">ALASAN PENGHAPUSAN</th>' +
                    '</tr>' +
                    '</thead><tbody>';
            var no = 1;
            var total = 0;
            if (d.detail.length > 0) {
                $.each(d.detail, function (key, data) {
                    //console.log(data);
                    tdetail += '<tr>' +
                            '<td style="text-align: center;"></td>' +
                            '<td style="text-align: center;">' + no + '</td>' +
                            '<td style="text-align: center;">' + data.kdaset + '</td>' +
                            '<td style="text-align: left;">' + data.nmaset + '</td>' +
                            '<td style="text-align: left;">' + data.ket_aset + '</td>' +
                            '<td style="text-align: center;">' + data.tglbeli + '</td>' +
                            '<td>' + data.ket_hapus + '</td>' +
                            '</tr>';
                    no++;
                });

            }
            tdetail += '</tbody></table>';
            return tdetail;
        }
    });

    function refresh() {
        table.ajax.reload();
    }

    function deleted(kode) {
        swal({
            title: "Konfirmasi Hapus !",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?= site_url('mstasethapus/proses'); ?>",
                data: {"kode": kode, "stat": "delete"},
                success: function (resp) {
                    var obj = jQuery.parseJSON(resp);
                    if (obj.state === "1") {
                        swal({
                            title: "Terhapus",
                            text: obj.msg,
                            type: "success"
                        }, function () {
                            table.ajax.reload();
                        });
                    } else {
                        swal("Terhapus", obj.msg, "error");
                    }
                },
                error: function (event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }
</script>
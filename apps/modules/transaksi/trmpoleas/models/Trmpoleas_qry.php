<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Trmpoleas_qry
 *
 * @author adi
 */
class Trmpoleas_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function set_nodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vm_trm_po_leasing where nodo =  '".$nodo."'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    } 

    public function select_data() {
        $nodo = $this->uri->segment(3);
        // $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vm_trm_po_leasing");
        $res = $q->result_array();
        return $res;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.get_divisi()");
        return $q->result_array();
    }  

    //cetak
    public function ctk_um($nodo) {
        $this->db->select("*");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vb_kwitansi_um");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    //cetak
    public function ctk_pl($nodo) {
        $this->db->select("*");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vb_kwitansi_fincoy");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    //cetak
    public function ctk_jp($nodo) {
        $this->db->select("*");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vb_kwitansi_jp");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    //cetak
    public function ctk_sp($nodo) {
        $this->db->select("*");
        $this->db->where('nodo',$nodo);
        $q = $this->db->get("pzu.vb_kwitansi_fincoy");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

  public function json_dgview() {
      error_reporting(-1);
      if( isset($_GET['nodo']) ){
          $nodo = $_GET['nodo'];
      }else{
          $nodo = '';
      }

      $aColumns = array('no',
                              'nodo'      ,
                              'tgldo'     , 
                              'nama'      ,
                              'nama_s'    ,
                              'alamat'  ,
                              'kdleasing',
                              'nmprogleas',
                              'pl'      ,
                              'jp'      ,
                              'matriks'      ,
                              'kode'      ,
                              'kdtipe'    ,
                              'nmtipe'    ,
                              'nmwarna'   ,
                              'nosin'    ,
                              'nora'     ,
                              'nmsales'   );
$sIndexColumn = "nodo";

      $sLimit = "";
      if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
          $sLimit = " LIMIT " . $_GET['iDisplayLength'];
      }
      $sTable = " ( SELECT ''::varchar as no, nodo,	tgldo,	nama,	nama_s,	alamat,	kdleasing,	nmprogleas,	pl,	jp,	matriks,	kode, kdtipe,	nmtipe,	nmwarna,	nosin,	nora, nmsales
         					    FROM pzu.vm_trm_po_leasing WHERE tgltrmpo is null ORDER BY tgldo ) AS a";
      if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
      {
          if($_GET['iDisplayStart']>0){
              $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                      intval( $_GET['iDisplayStart'] );
          }
      }

      $sOrder = "";
      if ( isset( $_GET['iSortCol_0'] ) )
      {
              $sOrder = " ORDER BY  ";
              for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
              {
                      if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                      {
                              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                      ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                      }
              }

              $sOrder = substr_replace( $sOrder, "", -2 );
              if ( $sOrder == " ORDER BY" )
              {
                      $sOrder = "";
              }
      }
      $sWhere = "";

      if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
      {
  $sWhere = " Where (";
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  }
  $sWhere = substr_replace( $sWhere, "", -3 );
  $sWhere .= ')';
      }

      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
              if ( $sWhere == "" )
              {
                  $sWhere = " WHERE ";
              }
              else
              {
                  $sWhere .= " AND ";
              }
              //echo $sWhere."<br>";
              $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
      }


      /*
       * SQL queries
       * QUERY YANG AKAN DITAMPILKAN
       */
      $sQuery = "
              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
              FROM   $sTable
              $sWhere
              $sOrder
              $sLimit
              ";

      //echo $sQuery;

      $rResult = $this->db->query( $sQuery);

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";    //SELECT FOUND_ROWS()

      $rResultFilterTotal = $this->db->query( $sQuery);
      $aResultFilterTotal = $rResultFilterTotal->result_array();
      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";
      $rResultTotal = $this->db->query( $sQuery);
      $aResultTotal = $rResultTotal->result_array();
      $iTotal = $aResultTotal[0]['jml'];

      $output = array(
              "sEcho" => intval($_GET['sEcho']),
              "iTotalRecords" => $iTotal,
              "iTotalDisplayRecords" => $iFilteredTotal,
              "aaData" => array()
      );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                }


                $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('trmpoleas/edit/'.$aRow['nodo'])."\">Edit</a>";

                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    }

    public function save() {
        $noso         = $this->input->post('noso');
        $nopoleasing  = $this->input->post('nopoleasing');
        $tglterbitpo  = $this->apps->dateConvert($this->input->post('tglterbitpo'));
        $tgltrmpo     = $this->apps->dateConvert($this->input->post('tgltrmpo'));
        $tgltagihleas = $this->apps->dateConvert($this->input->post('tgltagihleas'));
        $nama_p       = $this->input->post('nama_p');
        $ket_ar       = $this->input->post('ket_ar_leas'); 
        $q = $this->db->query("select title,msg,tipe from pzu.trm_po_leas_ins( '". $noso ."',
                                                                                '". $nopoleasing ."',
                                                                                '". $tglterbitpo ."',
                                                                                '". $tgltrmpo ."',
                                                                                '". $tgltagihleas ."',
                                                                                '". $nama_p ."',
                                                                                '". $ket_ar ."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

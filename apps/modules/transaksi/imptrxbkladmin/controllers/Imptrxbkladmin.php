<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Imptrxbkladmin
 *
 * @author
 */

class Imptrxbkladmin extends MY_Controller {
    public $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('imptrxbkladmin/submit'),
            'add' => site_url('imptrxbkladmin/add'),
            'edit' => site_url('imptrxbkladmin/edit'),
            'reload' => site_url('imptrxbkladmin'),
        );
        $this->load->model('imptrxbkladmin_qry');
        $this->load->helper(array('url','download'));
        require_once APPPATH.'libraries/PHPExcel.php';
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    // public function submit() {
    //         $res = $this->imptrxbkladmin_qry->getAllSo();
    //         //$this->load->view('export_html', $this->data);
    //         $this->template
    //             ->title($this->data['msg_main'],$this->apps->name)
    //             ->set_layout('print-layout')
    //             ->build('export_excel',$this->data);
    // }

    public function download_template_po(){
        force_download('assets/template/Template PO.xls',NULL);
    }

    public function download_template_sv(){
        force_download('assets/template/Template Nota Service.xls',NULL);
    }

    public function download_template_so(){
        force_download('assets/template/Template Penjualan Sparepart.xls',NULL);
    }

    public function download_template_kb(){
        force_download('assets/template/Template Kas Bank Bengkel.xlsx',NULL);
    }

    public function proces_servis() {
        $this->imptrxbkladmin_qry->proces_servis();
        //redirect("impumsl");
    }

    public function fromExcelToLinux($excel_time){
      return ($excel_time-25569)*86400;
    }

    private function _init_add(){
        $this->data['form'] = array(
        );
    }

    public function Beli() {
        //if(isset($_FILES["file"]["name"])){
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();

                for($row=2; $row<=$highestRow; $row++){
                    $pono               = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $podate             = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $vendorname         = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $vendoraddress      = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $description        = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $materialno         = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $qtypo              = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $hargabeli          = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $nilaidiscount      = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $totalharga         = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $grno               = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $tipepembayaran     = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $uangbayar          = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $qtybayar           = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $tanggalpembayaran  = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $bkuno              = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $nofaktur           = $worksheet->getCellByColumnAndRow(16, $row)->getValue();


                    

                    if(empty($pono)){  //jika kode tidak null -> simpan

                        $data = 'empty';
                    } else {
                        //jika deskripsi, tipe, harga = null
                        if(empty($pono)){$pono = '';}
                        if(empty($podate)){$podate = null;}
                        if(empty($vendorname)){$vendorname = '';}
                        if(empty($vendoraddress)){$vendoraddress = '';}
                        if(empty($description)){$description = '';}
                        if(empty($materialno)){$materialno = '';}
                        if(empty($qtypo)){$qtypo = 0;}
                       if(empty($hargabeli)){$hargabeli = 0;}
                       if(empty($nilaidiscount)){$nilaidiscount = 0;}
                       if(empty($totalharga)){$totalharga = 0;}
                        if(empty($grno)){$grno = '';}
                        if(empty($tipepembayaran)){$tipepembayaran = '';}
                       if(empty($uangbayar)){$uangbayar = 0;}
                       if(empty($qtybayar)){$qtybayar = 0;}
                        if(empty($tanggalpembayaran)){$tanggalpembayaran = null;}
                        if(empty($bkuno)){$bkuno = '';}
                        if(empty($nofaktur)){$nofaktur = '';}

                        $xpodate = $this->fromExcelToLinux($podate);
                        $tglbyr = $this->fromExcelToLinux($tanggalpembayaran);

                        $data[] = array(
                            'pono'               => $pono,
                            'podate'             => gmdate("Y-m-d", $xpodate),
                            'vendorname'         => $vendorname,
                            'vendoraddress'      => $vendoraddress,
                            'description'        => $description,
                            'materialno'         => $materialno,
                            'qtypo'              => $qtypo,
                            'hargabeli'          => $hargabeli,
                            'nilaidiscount'      => $nilaidiscount,
                            'totalharga'         => $totalharga,
                            'grno'               => $grno,
                            'tipepembayaran'     => $tipepembayaran,
                            'uangbayar'          => $uangbayar,
                            'qtybayar'           => $qtybayar,
                            'tanggalpembayaran'  => gmdate("Y-m-d", $tglbyr),
                            'bkuno'              => $bkuno,
                            'nofaktur'           => $nofaktur
                        );
                        // $res = $this->imptrxbkladmin_qry->insertBeli($data);
                        // echo $res
                        // echo $path                        
                    }
                }
                return json_encode($data);
            //}
            }
            // return $data;
    }

    public function Jual() {
         
        //if(isset($_FILES["file"]["name"])){
             $error = '';
             $total_line = '';
             $allowed_extension = array('csv','xls');
            $path = $_FILES["file"]["tmp_name"];
            // print $path;
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                
                $highestColumn = $worksheet->getHighestColumn();
                
                
                for($row=2; $row<=$highestRow; $row++){
                  
                    $datefrom           = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $dateto             = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $companyname        = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $branchcode         = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $address            = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $telp               = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $tanggal            = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $nopol              = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $nama_pelanggan     = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $nik_pelanggan      = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $alamat_pelanggan   = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $kodepart           = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $namapart           = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $satuan             = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $qty                = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $harga              = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $discount           = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                    $total              = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $grup               = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                    $penjualan          = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $noref              = $worksheet->getCellByColumnAndRow(20, $row)->getValue();

                    $xdatefrom = $this->fromExcelToLinux($datefrom);
                    $xdateto = $this->fromExcelToLinux($dateto);
                    $xtanggal = $this->fromExcelToLinux($tanggal);

                    if(!empty($kodepart)){  //jika kode tidak null -> simpan

                        //jika deskripsi, tipe, harga = null
                        if(empty($datefrom)){$datefrom = '';}
                        if(empty($dateto)){$dateto = '';}
                        if(empty($companyname)){$companyname = '';}
                        if(empty($branchcode)){$branchcode = '';}
                        if(empty($address)){$address = '';}
                        if(empty($telp)){$telp = '';}
                        if(empty($tanggal)){$tanggal = '';}
                        if(empty($nopol)){$nopol = '';}
                        if(empty($nama_pelanggan)){$nama_pelanggan = '';}
                        if(empty($nik_pelanggan)){$nik_pelanggan = '';}
                        if(empty($alamat_pelanggan)){$alamat_pelanggan = '';}
                        if(empty($kodepart)){$kodepart = '';}
                        if(empty($namapart)){$namapart = '';}
                        if(empty($satuan)){$satuan = '';}
//                        if(empty($qty)){$qty = '';}
//                        if(empty($harga)){$harga = 0;}
                        if(empty($grup)){$grup = '';}
                        if(empty($penjualan)){$penjualan = '';}
                        if(empty($noref)){$noref = '';}

                        $data[] = array(
                            'datefrom'        => gmdate("Y-m-d", $xdatefrom),
                            'dateto'          => gmdate("Y-m-d", $xdateto),
                            'companyname'     => $companyname,
                            'branchcode'      => $branchcode,
                            'address'         => $address,
                            'telp'            => $telp,
                            'tanggal'         => gmdate("Y-m-d", $xtanggal),
                            'nopol'           => $nopol,
                            'nama_pelanggan'  => $nama_pelanggan,
                            'nik_pelanggan'   => $nik_pelanggan,
                            'alamat_pelanggan'=> $alamat_pelanggan,
                            'kodepart'        => $kodepart,
                            'namapart'        => $namapart,
                            'satuan'          => $satuan,
                            'qty'             => $qty,
                            'harga'           => $harga,
                            'discount'           => $discount,
                            'total'           => $total,
                            'grup'            => $grup,
                            'penjualan'       => $penjualan,
                            'noref'           => $noref
                        );
                    }

                }
                return json_encode($data);
            }
        //}
    }

    public function Servis() {
        //if(isset($_FILES["file"]["name"])){
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet){
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();

                for($row=2; $row<=$highestRow; $row++){
                    $nomor                          = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $nama_ahass                     = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $no_ahass                       = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $nomor_nota_servis              = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $tgl_nota_servis                = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $no_pkb                         = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $no_antrian                     = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $nama_tipe_kedatangan           = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $alasan_ingat_service           = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $dealer_sendiri                 = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $no_polisi                      = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $nomor_mesin                    = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $nomor_rangka                   = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $nama_motor                     = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $warna                          = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $tahun_rakit                    = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $nama_pemilik                   = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                    $alamat                         = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $kelurahan                      = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                    $kecamatan                      = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $kabupaten                      = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                    $no_telp                        = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                    $no_hp                          = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                    $km_sekarang                    = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                    $km_berikut                     = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                    $jenis                          = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                    $kode_jasa_part                 = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                    $grup_jasa_part                 = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                    $nama_jasa_part                 = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                    $satuan                         = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
                    $jumlah                         = $worksheet->getCellByColumnAndRow(30, $row)->getValue();
                    $harga                          = $worksheet->getCellByColumnAndRow(31, $row)->getValue();
                    $total                          = $worksheet->getCellByColumnAndRow(32, $row)->getValue();
                    $tipe_pembayaran                = $worksheet->getCellByColumnAndRow(33, $row)->getValue();
                    $nama_mekanik                   = $worksheet->getCellByColumnAndRow(34, $row)->getValue();
                    $user_login                     = $worksheet->getCellByColumnAndRow(35, $row)->getValue();
                    $uang_bayar                     = $worksheet->getCellByColumnAndRow(36, $row)->getValue();
                    $eta_service                    = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
                    $actual_service                 = $worksheet->getCellByColumnAndRow(38, $row)->getValue();
                    $nik_pemilik                    = $worksheet->getCellByColumnAndRow(39, $row)->getValue();
                    $nama_pembawa                   = $worksheet->getCellByColumnAndRow(40, $row)->getValue();
                    $nik_pembawa                    = $worksheet->getCellByColumnAndRow(41, $row)->getValue();
                    $alamat_pembawa                 = $worksheet->getCellByColumnAndRow(42, $row)->getValue();
                    $discount                       = $worksheet->getCellByColumnAndRow(43, $row)->getValue();
                    $qrcode                         = $worksheet->getCellByColumnAndRow(44, $row)->getValue();
                    $activity_capacity              = $worksheet->getCellByColumnAndRow(45, $row)->getValue();
                    $proses_servis_pud              = $worksheet->getCellByColumnAndRow(46, $row)->getValue();
                    $status_servis_pud              = $worksheet->getCellByColumnAndRow(47, $row)->getValue();
                    $transaksiid                    = $worksheet->getCellByColumnAndRow(48, $row)->getValue();
                    $nama_service_adv               = $worksheet->getCellByColumnAndRow(49, $row)->getValue();
                    
                    
                    

                    $xtgl = $this->fromExcelToLinux($tgl_nota_servis);

                    if(!empty($nomor)){  //jika kode tidak null -> simpan

                       //jika deskripsi, tipe, harga = null
                        if(empty($nomor)){$nomor = '';}
                        if(empty($nama_ahass)){$nama_ahass = '';}
                        if(empty($no_ahass)){$no_ahass = '';}
                        if(empty($eta_service)){$eta_service = '';}
                        if(empty($actual_service)){$actual_service = '';}
                        if(empty($nomor_nota_servis)){$nomor_nota_servis = '';}
                        if(empty($tgl_nota_servis)){$tgl_nota_servis = '';}
                        if(empty($no_pkb)){$no_pkb = '';}
                        if(empty($no_antrian)){$no_antrian = '';}
                        if(empty($nama_tipe_kedatangan)){$nama_tipe_kedatangan = '';}
                        if(empty($alasan_ingat_service)){$alasan_ingat_service = '';}
                        if(empty($dealer_sendiri)){$dealer_sendiri = '';}
                        if(empty($no_polisi)){$no_polisi = '';}
                        if(empty($nomor_mesin)){$nomor_mesin = '';}
                        if(empty($nomor_rangka)){$nomor_rangka = '';}
                        if(empty($nama_motor)){$nama_motor = '';}
                        if(empty($warna)){$warna = '';}
                        // if(empty($tahun_rakit)){$tahun_rakit = '';}
                        if(empty($nama_pemilik)){$nama_pemilik = '';}
                        if(empty($nik_pemilik)){$nik_pemilik = '';}
                        if(empty($alamat)){$alamat = '';}
                        if(empty($kelurahan)){$kelurahan = '';}
                        if(empty($kecamatan)){$kecamatan = '';}
                        if(empty($kabupaten)){$kabupaten = '';}
                        if(empty($no_telp)){$no_telp = '';}
                        if(empty($no_hp)){$no_hp = '';}
                        if(empty($nama_pembawa)){$nama_pembawa = '';}
                        if(empty($nik_pembawa)){$nik_pembawa = '';}
                        if(empty($alamat_pembawa)){$alamat_pembawa = '';}
                        if(empty($km_sekarang)){$km_sekarang = '';}
                        if(empty($km_berikut)){$km_berikut = '';}
                        if(empty($jenis)){$jenis = '';}
                        if(empty($kode_jasa_part)){$kode_jasa_part = '';}
                        if(empty($grup_jasa_part)){$grup_jasa_part = '';}
                        if(empty($nama_jasa_part)){$nama_jasa_part = '';}
                        // if(empty($satuan)){$satuan = '';}
                        // if(empty($jumlah)){$jumlah = '';}
//                        if(empty($harga)){$harga = '';}
//                        if(empty($total)){$total = '';}
                        if(empty($tipe_pembayaran)){$tipe_pembayaran = '';}
                        if(empty($nama_mekanik)){$nama_mekanik = '';}
                        if(empty($user_login)){$user_login = '';}
//                        if(empty($uang_bayar)){$uang_bayar = '';}
                       if(empty($activity_capacity)){$activity_capacity = '';}
                       if(empty($proses_servis_pud)){$proses_servis_pud = '';}
                       if(empty($status_servis_pud)){$status_servis_pud = '';}
                       if(empty($transaksiid)){$transaksiid = '';}
                       if(empty($nama_service_adv)){$nama_service_adv = '';} 
                       if(empty($qrcode)){$qrcode = '';} 
                     

                        $data[] = array(
                          'nomor'                 =>$nomor ,
                          'nama_ahass'            =>$nama_ahass ,
                          'no_ahass'              =>$no_ahass ,
                          'nomor_nota_servis'     =>$nomor_nota_servis ,
                          'tgl_nota_servis'       =>gmdate("Y-m-d", $xtgl),
                          'no_pkb'                =>$no_pkb ,
                          'no_antrian'            =>$no_antrian ,
                          'nama_tipe_kedatangan'  =>$nama_tipe_kedatangan ,
                          'alasan_ingat_service'  =>$alasan_ingat_service ,
                          'dealer_sendiri'        =>$dealer_sendiri ,
                          'no_polisi'             =>$no_polisi ,
                          'nomor_mesin'           =>$nomor_mesin ,
                          'nomor_rangka'          =>$nomor_rangka ,
                          'nama_motor'            =>$nama_motor ,
                          'warna'                 =>$warna ,
                          'tahun_rakit'           =>"$tahun_rakit",
                          'nama_pemilik'          =>$nama_pemilik ,
                          'alamat'                =>$alamat ,
                          'kelurahan'             =>$kelurahan ,
                          'kecamatan'             =>$kecamatan ,
                          'kabupaten'             =>$kabupaten ,
                          'no_telp'               =>$no_telp ,
                          'no_hp'                 =>$no_hp ,
                          'nama_pembawa'          =>$nama_pembawa ,
                          'nik_pembawa'           =>$nik_pembawa ,
                          'alamat_pembawa'        =>$alamat_pembawa ,
                          'km_sekarang'           =>"$km_sekarang" ,
                          'km_berikut'            =>"$km_berikut" ,
                          'jenis'                 =>$jenis ,
                          'kode_jasa_part'        =>$kode_jasa_part ,
                          'grup_jasa_part'        =>$grup_jasa_part ,
                          'nama_jasa_part'        =>$nama_jasa_part ,
                          'satuan'                =>$satuan ,
                          'jumlah'                =>$jumlah ,
                          'harga'                 =>$harga ,
                          'total'                 =>$total ,
                          'tipe_pembayaran'       =>$tipe_pembayaran ,
                          'nama_mekanik'          =>$nama_mekanik ,
                          'user_login'            =>$user_login ,
                          'uang_bayar'            =>$uang_bayar,
                          'nik_pemilik'           =>$nik_pemilik,
                          'discount'              =>$discount,
                          'eta_service'           =>$eta_service,
                          'actual_service'        =>$actual_service,
                          'activity_capacity'     =>$activity_capacity,
                          'proses_servis_pud'     =>$proses_servis_pud,
                          'status_servis_pud'     =>$status_servis_pud,
                          'transaksiid'           =>$transaksiid,
                          'nama_service_adv'      =>$nama_service_adv,
                          'qrcode'                =>$qrcode

                        );
                    }
                }
                return json_encode($data);
            }
        //}
    }

    public function importBeli() {
        if(isset($_FILES["file"]["name"])){
            $data = $this->Beli();

            echo $this->imptrxbkladmin_qry->insertBeli(json_decode($data));
        }
    }

    public function importServis() {
        if(isset($_FILES["file"]["name"])){
            $data = $this->Servis();
            echo $this->imptrxbkladmin_qry->insertServis(json_decode($data));
        }
    }

    public function importJual() {
        if(isset($_FILES["file"]["name"])){
            $data = $this->Jual();
            echo $this->imptrxbkladmin_qry->insertJual(json_decode($data));
        }
    }
}

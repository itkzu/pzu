<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-body">
                    <div class="form-group">
                        <?php 
                            echo form_label($form['nodo']['placeholder']);
                            echo form_input($form['nodo']);
                            echo form_error('nodo','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                <hr>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php 
                                echo form_input($form['iddo']);
                                echo form_label($form['nama']['placeholder']);
                                echo form_input($form['nama']);
                                echo form_error('nama','<div class="note">','</div>'); 
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo form_label($form['alamat']['placeholder']);
                                echo form_textarea($form['alamat']);
                                echo form_error('alamat','<div class="note">','</div>'); 
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php 
                                        echo form_label($form['kel']['placeholder']);
                                        echo form_input($form['kel']);
                                        echo form_error('kel','<div class="note">','</div>'); 
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php 
                                        echo form_label($form['kec']['placeholder']);
                                        echo form_input($form['kec']);
                                        echo form_error('kec','<div class="note">','</div>'); 
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php 
                                        echo form_label($form['kota']['placeholder']);
                                        echo form_input($form['kota']);
                                        echo form_error('kota','<div class="note">','</div>'); 
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php 
                                        echo form_label($form['nohp']['placeholder']);
                                        echo form_input($form['nohp']);
                                        echo form_error('nohp','<div class="note">','</div>'); 
                                    ?>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php 
                                echo form_label($form['tgldo']['placeholder']);
                                echo form_input($form['tgldo']);
                                echo form_error('tgldo','<div class="note">','</div>'); 
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo form_label($form['kdtipe']['placeholder']);
                                echo form_input($form['kdtipe']);
                                echo form_error('kdtipe','<div class="note">','</div>'); 
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo form_label($form['nmtipe']['placeholder']);
                                echo form_input($form['nmtipe']);
                                echo form_error('nmtipe','<div class="note">','</div>'); 
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo form_label($form['nosin']['placeholder']);
                                echo form_input($form['nosin']);
                                echo form_error('nosin','<div class="note">','</div>'); 
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo form_label($form['nora']['placeholder']);
                                echo form_input($form['nora']);
                                echo form_error('nora','<div class="note">','</div>'); 
                            ?>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary btn-validasi" disabled="">Validasi</button>
                <button type="button" class="btn btn-default btn-batal">Batal</button>             
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#nodo").mask("a99-999999");
        $(".btn-batal").click(function(){
            reset();
        });
        $(".btn-tampil").click(function(){
            getData();
            $(".btn-validasi").attr("disabled",true);
        });
        $(".btn-validasi").click(function(){
            batalFaktur();
        });
    });
    
    function reset(){
        $(".form-control").val("");
    }
    
    function getData(){
        var nodo = $("#nodo").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('tranbfakastra/getData');?>",
            data: {"nodo":nodo},
            success: function(resp){  
                if(resp){
                    var obj = jQuery.parseJSON(resp);
                    var tgl_trm_fa = "";
                    $.each(obj, function(key, data){
                        $("#iddo").val(data.nodo);
                        $("#nama").val(data.nama);
                        $("#tgldo").val(data.tgldo);
                        $("#alamat").val(data.alamat);
                        $("#kel").val(data.kel);
                        $("#kec").val(data.kec);
                        $("#kota").val(data.kota);
                        $("#nohp").val(data.nohp);
                        $("#kdtipe").val(data.kdtipe);
                        $("#nmtipe").val(data.nmtipe);
                        $("#nosin").val(data.nosin);
                        $("#nora").val(data.nora);
                        tgl_trm_fa = data.tgl_trm_fa;
                        //nodo, tgldo, nama, alamat, kel, kec, kota, nohp, kdtipe, nmtipe, nosin, nora
                    });
                    if(tgl_trm_fa===null){
                        $(".btn-validasi").attr("disabled",false);
                    }else if(tgl_trm_fa==="" && obj===""){
                        swal({
                            title: "Informasi",
                            text: "Data No. DO : " + nodo + " Tidak Ditemukan, Mungkin Nomor Pengajuan Faktur Astra Belum Di Input",
                            type: "info"
                        }, function(){
                            
                        });
                    }else if(tgl_trm_fa!==null && tgl_trm_fa!=="" ){
                        swal({
                            title: "Informasi",
                            text: "Data No. DO : " + nodo + " Tidak Bisa Batal Faktur, Karena Sudah Pernah Terima Faktur Tanggal " + tgl_trm_fa,
                            type: "info"
                        }, function(){
                            
                        });
                    }else{
                        swal({
                            title: "Informasi",
                            text: "Data No. DO : " + nodo + " Tidak Ditemukan, Alasan Tidak Diketahui",
                            type: "info"
                        }, function(){
                            
                        });
                    }
                }else{
                    swal("Error", obj.msg, "error");
                }
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });        
    }
    
    function batalFaktur(){
        swal({
            title: "Konfirmasi Pembatalan Faktur !",
            text: "Faktur Astra akan dibatalkan, data tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                var submit = "<?php echo $submit;?>"; 
                var nodo = $("#iddo").val();
                $.ajax({
                    type: "POST",
                    url: submit,
                    data: {"nodo":nodo,"stat":"cancel"},
                    success: function(resp){   
                        var obj = jQuery.parseJSON(resp);
                        if(obj.state==="1"){
                            swal({
                                title: "Pembatalan Berhasil",
                                text: obj.msg,
                                type: "success"
                            }, function(){
                                reset();
                            });
                        }else{
                            swal("Pembatalan Gagal", obj.msg, "error");
                        }
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
</script>
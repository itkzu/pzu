<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Lrnew
 *
 * @author adi
 */
class Lrnew extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main'    => $this->msg_main,
            'msg_detail'  => $this->msg_detail,

            'submit'      => site_url('lrnew/submit'),
            'ctk_lr'      => site_url('lrnew/ctk_lr'),
            'add'         => site_url('lrnew/add'),
            'edit'        => site_url('lrnew/edit'),
            'reload'      => site_url('lrnew'),
        );
        $this->load->model('lrnew_qry');
        $cabang = $this->lrnew_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }

        $salesins = $this->lrnew_qry->getkdsalesins();
        foreach ($salesins as $value) {
            $this->data['kdsales_ins'][$value['kdsales_ins']] = $value['nmsales_ins'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function ctk_lr() {
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->lrnew_qry->ctk_labarugi($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_lr',$this->data);
    }

    // public function ctk_labarugi() {
    //     $nodo = $this->input->post('nodo');

    //     $this->load->library('dompdf_lib');

    //     $this->dompdf_lib->filename = "Cetak Laba/Rugi '".$nodo."'.pdf";
    //     $customPaper = array(0,0,360,360);

    //     $this->data['data'] = $this->lrnew_qry->ctk_labarugi($nodo);
    //     // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
    //     //
    //     // $this->dompdf_lib->load_view('sample_view', $this->sum );
    //     $this->dompdf_lib->load_view('ctk_lr', $this->data );
    // }
    // tampil data belum do
    public function json_dgview() {
        echo $this->lrnew_qry->json_dgview();
    }
    // kode no do
    public function getNoID() {
        echo $this->lrnew_qry->getNoID();
    }
    // kode no do
    public function refund_dpp() {
        echo $this->lrnew_qry->refund_dpp();
    }
    public function set_nodo() {
        echo $this->lrnew_qry->set_nodo();
    }
    public function insentif() {
        echo $this->lrnew_qry->insentif();
    }
    public function sales_ins() {
        echo $this->lrnew_qry->sales_ins();
    }
    public function save() {
        echo $this->lrnew_qry->save();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
          //baris 1
          'nodo'=> array(
                   'placeholder' => 'No. DO',
                   //'type'        => 'hidden',
                   'id'          => 'nodo',
                   'name'        => 'nodo',
                   'value'       => set_value('nodo'),
                   'class'       => 'form-control',
                   'style'       => 'text-transform: uppercase;',
           ),
           'noso'=> array(
                    'placeholder' => 'No. SPK',
                    //'type'        => 'hidden',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
           ),
           'nosin'=> array(
                   'placeholder' => 'No Mesin',
                   'value'       => set_value('nosin'),
                   'id'          => 'nosin',
                   'name'        => 'nosin',
                   'class'       => 'form-control',
                   'style'       => 'text-transform: uppercase;',
                   'readonly'    => '',
           ),
           'norangka'=> array(
                   'placeholder' => 'No Rangka',
                   'value'       => set_value('norangka'),
                   'id'          => 'norangka',
                   'name'        => 'norangka',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => ''
           ),
           'tgldo'=> array(
                  'placeholder' => 'Tanggal DO',
                  'id'          => 'tgldo',
                  'name'        => 'tgldo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
           'nama'=> array(
                    'placeholder' => 'Customer',
                    'id'          => 'nama',
                    'name'        => 'nama',
                    'value'       => set_value('nama'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
           ),
           'kdtipe'=> array(
                   'placeholder' => 'Tipe Unit',
                   'id'          => 'kdtipe',
                   'class'       => 'form-control',
                   'value'       => set_value('kdtipe'),
                   'name'        => 'kdtipe',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nmtipe'=> array(
                   'placeholder' => 'Nama Tipe',
                   'value'       => set_value('nmtipe'),
                   'id'          => 'nmtipe',
                   'name'        => 'nmtipe',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nmsales'=> array(
                   'placeholder' => 'Salesman',
                   // 'type'        => 'hidden',
                   'value'       => set_value('nmsales'),
                   'id'          => 'nmsales',
                   'name'        => 'nmsales',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nmleasing'=> array(
                   'placeholder' => 'Leasing',
                   // 'type'        => 'hidden',
                   'value'       => set_value('nmleasing'),
                   'id'          => 'nmleasing',
                   'name'        => 'nmleasing',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nmwarna'=> array(
                   'placeholder' => 'Nama Warna',
                   // 'type'        => 'hidden',
                   'value'       => set_value('nmwarna'),
                   'id'          => 'nmwarna',
                   'name'        => 'nmwarna',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'tahun'=> array(
                   'placeholder' => 'Tahun',
                   // 'type'        => 'hidden',
                   'value'       => set_value('tahun'),
                   'id'          => 'tahun',
                   'name'        => 'tahun',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nopo'=> array(
                   'placeholder' => 'Faktur',
                   // 'type'        => 'hidden',
                   'value'       => set_value('nopo'),
                   'id'          => 'nopo',
                   'name'        => 'nopo',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'nmspv'=> array(
                   'placeholder' => 'Superviser',
                   // 'type'        => 'hidden',
                   'value'       => set_value('nmspv'),
                   'id'          => 'nmspv',
                   'name'        => 'nmspv',
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),
           'progleas'=> array(
                   'placeholder' => 'Program',
                   'id'          => 'progleas',
                   'class'       => 'form-control',
                   'value'    => set_value('progleas'),
                   'name'     => 'progleas',
                   'readonly'    => '',
           ),
           'stat_otr'=> array(
                   'placeholder' => 'Status OTR',
                   'id'          => 'stat_otr',
                   'class'       => 'form-control',
                   'value'       => set_value('stat_otr'),
                   'name'        => 'stat_otr',
                   'style'       => '',
                   'readonly'    => '',
           ),
         'tglpo'=> array(
                  'placeholder' => 'Tanggal',
                  'id'          => 'tglpo',
                  'name'        => 'tglpo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => '',
          ),
        'ket'=> array(
                 'placeholder' => 'Keterangan',
                 'id'          => 'ket',
                 'name'        => 'ket',
                 'value'       => set_value('ket'),
                 'class'       => 'form-control',
                 'style'       => '',
                 'readonly'    => '',
         ),

          //baris 2
          'hrg_otr'=> array(
                   'placeholder' => 'Harga OTR',
                   'id'          => 'hrg_otr',
                   'name'        => 'hrg_otr',
                   'value'       => set_value('hrg_otr'),
                   'class'       => 'form-control',
                   'style'       => 'text-align:right;',
                   'readonly'    => '',
          ),
          'disc_unit1'=> array(
                    'placeholder' => 'Discount Unit',
                    'id'          => 'disc_unit1',
                    'name'        => 'disc_unit1',
                    'value'       => set_value('disc_unit1'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'disc_unit2'=> array(
                    'placeholder' => 'Discount Unit',
                    'id'          => 'disc_unit2',
                    'name'        => 'disc_unit2',
                    'value'       => set_value('disc_unit2'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'disc_leasing'=> array(
                    'placeholder' => 'Beban Leasing',
                    'id'          => 'disc_leasing',
                    'name'        => 'disc_leasing',
                    'value'       => set_value('disc_leasing'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_dealer1'=> array(
                    'placeholder' => 'Subsidi Dealer',
                    'id'          => 'sub_dealer1',
                    'name'        => 'sub_dealer1',
                    'value'       => set_value('sub_dealer1'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_dealer2'=> array(
                    'placeholder' => 'Subsidi Dealer',
                    'id'          => 'sub_dealer2',
                    'name'        => 'sub_dealer2',
                    'value'       => set_value('sub_dealer2'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_md1'=> array(
                    'placeholder' => 'Subsidi MD/HSO',
                    'id'          => 'sub_md1',
                    'name'        => 'sub_md1',
                    'value'       => set_value('sub_md1'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_md2'=> array(
                    'placeholder' => 'Subsidi MD/HSO',
                    'id'          => 'sub_md2',
                    'name'        => 'sub_md2',
                    'value'       => set_value('sub_md2'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_ahm1'=> array(
                    'placeholder' => 'Subsidi AHM',
                    'id'          => 'sub_ahm1',
                    'name'        => 'sub_ahm1',
                    'value'       => set_value('sub_ahm1'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_ahm2'=> array(
                    'placeholder' => 'Subsidi AHM',
                    'id'          => 'sub_ahm2',
                    'name'        => 'sub_ahm2',
                    'value'       => set_value('sub_ahm2'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_leas1'=> array(
                    'placeholder' => 'Subsidi Leasing',
                    'id'          => 'sub_leas1',
                    'name'        => 'sub_leas1',
                    'value'       => set_value('sub_leas1'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_leas2'=> array(
                    'placeholder' => 'Subsidi Leasing',
                    'id'          => 'sub_leas2',
                    'name'        => 'sub_leas2',
                    'value'       => set_value('sub_leas2'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'hdd1'=> array(
                    'placeholder' => 'HARGA DITERIMA DEALER',
                    'id'          => 'hdd1',
                    'name'        => 'hdd1',
                    'value'       => set_value('hdd1'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'hdd2'=> array(
                    'placeholder' => 'Harga Diterima Dealer',
                    'id'          => 'hdd2',
                    'name'        => 'hdd2',
                    'value'       => set_value('hdd2'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'um_cust'=> array(
                    'placeholder' => 'Uang Muka Customer',
                    'id'          => 'um_cust',
                    'name'        => 'um_cust',
                    'value'       => set_value('um_cust'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'um'=> array(
                    'placeholder' => 'UANG MUKA',
                    'id'          => 'um',
                    'name'        => 'um',
                    'value'       => set_value('um'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'lunas'=> array(
                    'placeholder' => 'PELUNASAN',
                    'id'          => 'lunas',
                    'name'        => 'lunas',
                    'value'       => set_value('lunas'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'cad_bbn'=> array(
                    'placeholder' => 'Cadangan BBN',
                    'id'          => 'cad_bbn',
                    'name'        => 'cad_bbn',
                    'value'       => set_value('cad_bbn'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'hd_dpp'=> array(
                    'placeholder' => 'Harga Diterima (DPP)',
                    'id'          => 'hd_dpp',
                    'name'        => 'hd_dpp',
                    'value'       => set_value('hd_dpp'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'sub_hso'=> array(
                    'placeholder' => 'Subsidi HSO & AHM',
                    'id'          => 'sub_hso',
                    'name'        => 'sub_hso',
                    'value'       => set_value('sub_hso'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'ins_leas_dpp'=> array(
                    'placeholder' => 'Insentif Leasing (DPP)',
                    'id'          => 'ins_leas_dpp',
                    'name'        => 'ins_leas_dpp',
                    'value'       => set_value('ins_leas_dpp'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'ins_ai'=> array(
                    'placeholder' => 'Insentif AI',
                    'id'          => 'ins_ai',
                    'name'        => 'ins_ai',
                    'value'       => set_value('ins_ai'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'hbu_dpp'=> array(
                    'placeholder' => 'Harga Beli Unit (DPP)',
                    'id'          => 'hbu_dpp',
                    'name'        => 'hbu_dpp',
                    'value'       => set_value('hbu_dpp'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'jaket'=> array(
                    'placeholder' => 'Jaket',
                    'id'          => 'jaket',
                    'name'        => 'jaket',
                    'value'       => set_value('jaket'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'insentif'=> array(
                  'placeholder' => 'Program',
                  'attr'        => array(
                      'id'    => 'insentif',
                      'class' => 'form-control',
                  ),
                  'data'     => $this->data['kdsales_ins'],
                  'value'    => set_value('insentif'),
                  'name'     => 'insentif',
                  'readonly'    => '',
          ),
          'ins_sales'=> array(
                    'placeholder' => 'Insentif Sales',
                    'id'          => 'ins_sales',
                    'name'        => 'ins_sales',
                    'value'       => set_value('ins_sales'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'biops'=> array(
                    'placeholder' => 'Biops',
                    'id'          => 'biops',
                    'name'        => 'biops',
                    'value'       => set_value('biops'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'komisi'=> array(
                    'placeholder' => 'Komisi',
                    'id'          => 'komisi',
                    'name'        => 'komisi',
                    'value'       => set_value('komisi'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'oth'=> array(
                    'placeholder' => 'Lain-lain',
                    'id'          => 'oth',
                    'name'        => 'oth',
                    'value'       => set_value('oth'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'hpp'=> array(
                    'placeholder' => 'HPP',
                    'id'          => 'hpp',
                    'name'        => 'hpp',
                    'value'       => set_value('hpp'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'iris'=> array(
                    'placeholder' => 'Iris',
                    'id'          => 'iris',
                    'name'        => 'iris',
                    'value'       => set_value('iris'),
                    'class'       => 'form-control',
                    'type'        => 'hidden',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'lr_unit'=> array(
                    'placeholder' => 'LABA(RUGI) UNIT',
                    'id'          => 'lr_unit',
                    'name'        => 'lr_unit',
                    'value'       => set_value('lr_unit'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'jp'=> array(
                    'placeholder' => 'JP',
                    'id'          => 'jp',
                    'name'        => 'jp',
                    'value'       => set_value('jp'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'matriks'=> array(
                    'placeholder' => 'Matriks',
                    'id'          => 'matriks',
                    'name'        => 'matriks',
                    'value'       => set_value('matriks'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'total'=> array(
                    'placeholder' => 'Total',
                    'id'          => 'total',
                    'name'        => 'total',
                    'value'       => set_value('total'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
          'hj_net'=> array(
                    'placeholder' => 'HARGA JUAL NET',
                    'id'          => 'hj_net',
                    'name'        => 'hj_net',
                    'value'       => set_value('hj_net'),
                    'class'       => 'form-control',
                    'style'       => 'text-align:right;',
                    'readonly'    => '',
          ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);

        if(($this->val[0]['nmleasing'])==='TUNAI'){

          $this->data['form'] = array(
            //baris 1
            'nodo'=> array(
                     'placeholder'  => 'No. DO',
                     // 'type'         => 'hidden',
                     'id'           => 'nodo',
                     'name'         => 'nodo',
                     'value'        => $this->val[0]['nodo'],
                     'class'        => 'form-control',
                     'style'        => 'text-transform: uppercase;',
             ),
             'noso'=> array(
                      'placeholder' => 'No. SPK',
                      // 'type'        => 'hidden',
                      'id'          => 'noso',
                      'name'        => 'noso',
                      'value'       => $this->val[0]['noso'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
             ),
             'nosin'=> array(
                     'placeholder'  => 'No Mesin',
                     'value'        => $this->val[0]['nosin'],
                     'id'           => 'nosin',
                     'name'         => 'nosin',
                     'class'        => 'form-control',
                     'style'        => 'text-transform: uppercase;',
                     'readonly'     => '',
             ),
             'norangka'=> array(
                     'placeholder'  => 'No Rangka',
                     'value'        => $this->val[0]['nora'],
                     'id'           => 'norangka',
                     'name'         => 'norangka',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => ''
             ),
             'tgldo'=> array(
                    'placeholder'   => 'Tanggal DO',
                    'id'            => 'tgldo',
                    'name'          => 'tgldo',
                    'value'         => $this->apps->dateConvert($this->val[0]['tgldo']),
                    'class'         => 'form-control',
                    'style'         => '',
                    'readonly'      => '',
            ),
             'nama'=> array(
                      'placeholder' => 'Customer',
                      'id'          => 'nama',
                      'name'        => 'nama',
                      'value'       => $this->val[0]['nama'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
             ),
             'kdtipe'=> array(
                     'placeholder'  => 'Tipe Unit',
                     'id'           => 'kdtipe',
                     'class'        => 'form-control',
                     'value'        => $this->val[0]['kdtipe'],
                     'name'         => 'kdtipe',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmtipe'=> array(
                     'placeholder'  => 'Nama Tipe',
                     'value'        => $this->val[0]['nmtipe'],
                     'id'           => 'nmtipe',
                     'name'         => 'nmtipe',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmsales'=> array(
                     'placeholder'  => 'Salesman',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['nmsales'],
                     'id'           => 'nmsales',
                     'name'         => 'nmsales',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmleasing'=> array(
                     'placeholder'  => 'Leasing',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['nmleasing'],
                     'id'           => 'nmleasing',
                     'name'         => 'nmleasing',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmwarna'=> array(
                     'placeholder'  => 'Nama Warna',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['warna'],
                     'id'           => 'nmwarna',
                     'name'         => 'nmwarna',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'tahun'=> array(
                     'placeholder'  => 'Tahun',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['tahun'],
                     'id'           => 'tahun',
                     'name'         => 'tahun',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nopo'=> array(
                     'placeholder'  => 'Faktur',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['nopox'],
                     'id'           => 'nopo',
                     'name'         => 'nopo',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmspv'=> array(
                     'placeholder'  => 'Superviser',
                     // 'type'          => 'hidden',
                     'value'        => $this->val[0]['nmsales_header'],
                     'id'           => 'nmspv',
                     'name'         => 'nmspv',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'progleas'=> array(
                     'placeholder' => 'Program',
                     'id'          => 'progleas',
                     'class'       => 'form-control',
                     'value'       => $this->val[0]['nmprogleas'],
                     'name'        => 'progleas',
                     'readonly'    => '',
             ),
             'stat_otr'=> array(
                     'placeholder' => 'Status OTR',
                     'id'          => 'stat_otr',
                     'class'       => 'form-control',
                     'value'       => $this->val[0]['status_otr'],
                     'name'        => 'stat_otr',
                     'style'       => '',
                     'readonly'    => '',
             ),
           'tglpo'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => $this->apps->dateConvert($this->val[0]['tglpo']),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
          'ket'=> array(
                   'placeholder' => 'Keterangan',
                   'id'          => 'ket',
                   'name'        => 'ket',
                   'value'       => set_value('ket'),
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),

            //baris 2
            'hrg_otr'=> array(
                     'placeholder' => 'Harga OTR',
                     'id'          => 'hrg_otr',
                     'name'        => 'hrg_otr',
                     'value'       => $this->val[0]['harga_otr'],
                     'class'       => 'form-control',
                     'style'       => 'text-align:right;',
                     'readonly'    => '',
                     'required'    => '',
            ),
            'disc_unit1'=> array(
                      'placeholder' => 'Discount Unit',
                      'id'          => 'disc_unit1',
                      'name'        => 'disc_unit1',
                      'value'       => $this->val[0]['disc'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      // 'type'        => 'hidden'
                      'readonly'    => '',
                      'required'    => '',
            ),
            'sub_disc1'=> array(
                      'id'          => 'sub_disc1',
                      'name'        => 'sub_disc1',
                      'value'       => $this->val[0]['disc'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
                      'type'        => 'hidden'
            ),
            'disc_leasing'=> array(
                      'placeholder' => 'Beban Leasing',
                      'id'          => 'disc_leasing',
                      'name'        => 'disc_leasing',
                      'value'       => $this->val[0]['disc_leasing'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'sub_dealer1'=> array(
                      'placeholder' => 'Subsidi Dealer',
                      'id'          => 'sub_dealer1',
                      'name'        => 'sub_dealer1',
                      'value'       => $this->val[0]['sub_dealer'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'sub1();'
                      // 'readonly'    => '',
            ),
            'sub_md1'=> array(
                      'placeholder' => 'Subsidi MD/HSO',
                      'id'          => 'sub_md1',
                      'name'        => 'sub_md1',
                      'value'       => $this->val[0]['sub_md'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub1();'
                      // 'readonly'    => '',
            ),
            'sub_ahm1'=> array(
                      'placeholder' => 'Subsidi AHM',
                      'id'          => 'sub_ahm1',
                      'name'        => 'sub_ahm1',
                      'value'       => $this->val[0]['sub_ahm'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub1();'
                      // 'readonly'    => '',
            ),
            'sub_leas1'=> array(
                      'placeholder' => 'Subsidi Leasing',
                      'id'          => 'sub_leas1',
                      'name'        => 'sub_leas1',
                      'value'       => $this->val[0]['sub_fincoy'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub1();'
                      // 'readonly'    => '',
            ),
                'disc_unit2'=> array(
                          'placeholder' => 'Discount Unit',
                          'id'          => 'disc_unit2',
                          'name'        => 'disc_unit2',
                          'value'       => set_value('disc_unit2'),
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_dealer2'=> array(
                          'placeholder' => 'Subsidi Dealer',
                          'id'          => 'sub_dealer2',
                          'name'        => 'sub_dealer2',
                          'value'       => set_value('sub_dealer2'),
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_md2'=> array(
                          'placeholder' => 'Subsidi MD/HSO',
                          'id'          => 'sub_md2',
                          'name'        => 'sub_md2',
                          'value'       => set_value('sub_md2'),
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_ahm2'=> array(
                          'placeholder' => 'Subsidi AHM',
                          'id'          => 'sub_ahm2',
                          'name'        => 'sub_ahm2',
                          'value'       => set_value('sub_ahm2'),
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_leas2'=> array(
                          'placeholder' => 'Subsidi Leasing',
                          'id'          => 'sub_leas2',
                          'name'        => 'sub_leas2',
                          'value'       => set_value('sub_leas2'),
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
            'hdd1'=> array(
                      'placeholder' => 'HARGA DITERIMA DEALER',
                      'id'          => 'hdd1',
                      'name'        => 'hdd1',
                      'value'       => set_value('hdd1'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'hdd2'=> array(
                      'placeholder' => 'Harga Diterima Dealer',
                      'id'          => 'hdd2',
                      'name'        => 'hdd2',
                      'value'       => set_value('hdd2'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'um_cust'=> array(
                      'placeholder' => 'Uang Muka Customer',
                      'id'          => 'um_cust',
                      'name'        => 'um_cust',
                      'value'       => $this->val[0]['um'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'um'=> array(
                      'placeholder' => 'UANG MUKA',
                      'id'          => 'um',
                      'name'        => 'um',
                      'value'       => set_value('um'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'lunas'=> array(
                      'placeholder' => 'PELUNASAN',
                      'id'          => 'lunas',
                      'name'        => 'lunas',
                      'value'       => set_value('lunas'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'cad_bbn'=> array(
                      'placeholder' => 'Cadangan BBN',
                      'id'          => 'cad_bbn',
                      'name'        => 'cad_bbn',
                      'value'       => $this->val[0]['bbn'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_bbn();'
                      // 'readonly'    => '',
            ),
            'hd_dpp'=> array(
                      'placeholder' => 'Harga Diterima (DPP)',
                      'id'          => 'hd_dpp',
                      'name'        => 'hd_dpp',
                      'value'       => set_value('hd_dpp'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'sub_hso'=> array(
                      'placeholder' => 'Subsidi HSO & AHM',
                      'id'          => 'sub_hso',
                      'name'        => 'sub_hso',
                      'value'       => $this->val[0]['sub_hso'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_bbn();'
                      // 'readonly'    => '',
            ),
            'ins_leas_dpp'=> array(
                      'placeholder' => 'Insentif Leasing (DPP)',
                      'id'          => 'ins_leas_dpp',
                      'name'        => 'ins_leas_dpp',
                      'value'       => $this->val[0]['ins_leasing'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'ins_ai'=> array(
                      'placeholder' => 'Insentif AI',
                      'id'          => 'ins_ai',
                      'name'        => 'ins_ai',
                      'value'       => $this->val[0]['ins_ai'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_bbn();'
                      // 'readonly'    => '',
            ),
            'hbu_dpp'=> array(
                      'placeholder' => 'Harga Beli Unit (DPP)',
                      'id'          => 'hbu_dpp',
                      'name'        => 'hbu_dpp',
                      'value'       => $this->val[0]['harga_beli'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
                      'onkeyup'     => 'sub_hb();'
            ),
            'jaket'=> array(
                      'placeholder' => 'Jaket',
                      'id'          => 'jaket',
                      'name'        => 'jaket',
                      'value'       => $this->val[0]['jaket'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      // 'readonly'    => '',
                      'onkeyup'     => 'sub_hb();'
            ),
            'insentif'=> array(
                    'placeholder' => 'Program',
                    'attr'        => array(
                        'id'    => 'insentif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdsales_ins'],
                    'value'       => $this->val[0]['kdsales_ins'],
                    'name'     => 'insentif',
                    // 'readonly'    => '',
            ),
            'ins_sales'=> array(
                      'placeholder' => 'Insentif Sales',
                      'id'          => 'ins_sales',
                      'name'        => 'ins_sales',
                      'value'       => $this->val[0]['ins_sales'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      // 'readonly'    => '',
                      'onkeyup'     => 'sub_hb();'
            ),
            'sales_ins'=> array(
                      'id'          => 'sales_ins',
                      'name'        => 'sales_ins',
                      'value'       => set_value('sales_ins'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
                      'type'        => 'hidden',
            ),
            'biops'=> array(
                      'placeholder' => 'Biops',
                      'id'          => 'biops',
                      'name'        => 'biops',
                      'value'       => $this->val[0]['biops'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      // 'readonly'    => '',
                      'onkeyup'     => 'sub_hb();'
            ),
            'komisi'=> array(
                      'placeholder' => 'Komisi',
                      'id'          => 'komisi',
                      'name'        => 'komisi',
                      'value'       => $this->val[0]['komisi'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      // 'readonly'    => '',
                      'onkeyup'     => 'sub_hb();'
            ),
            'oth'=> array(
                      'placeholder' => 'Lain-lain',
                      'id'          => 'oth',
                      'name'        => 'oth',
                      'value'       => $this->val[0]['topcover'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      // 'readonly'    => '',
                      'onkeyup'     => 'sub_hb();'
            ),
            'hpp'=> array(
                      'placeholder' => 'HPP',
                      'id'          => 'hpp',
                      'name'        => 'hpp',
                      'value'       => set_value('hpp'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'iris'=> array(
                      'placeholder' => 'Iris',
                      'id'          => 'iris',
                      'name'        => 'iris',
                      'value'       => $this->val[0]['iris'],
                      'class'       => 'form-control',
                      'type'        => 'hidden',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'lr_unit'=> array(
                      'placeholder' => 'LABA(RUGI) UNIT',
                      'id'          => 'lr_unit',
                      'name'        => 'lr_unit',
                      'value'       => set_value('lr_unit'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'jp'=> array(
                      'placeholder' => 'JP',
                      'id'          => 'jp',
                      'name'        => 'jp',
                      'value'       => $this->val[0]['jp'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'matriks'=> array(
                      'placeholder' => 'Matriks',
                      'id'          => 'matriks',
                      'name'        => 'matriks',
                      'value'       => $this->val[0]['matriks'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'total'=> array(
                      'placeholder' => 'Total',
                      'id'          => 'total',
                      'name'        => 'total',
                      'value'       => set_value('total'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'hj_net'=> array(
                      'placeholder' => 'HARGA JUAL NET',
                      'id'          => 'hj_net',
                      'name'        => 'hj_net',
                      'value'       => set_value('hj_net'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'progres'=> array(
                      'placeholder' => '',
                      'id'          => 'progres',
                      'name'        => 'progres',
                      'value'       => $this->val[0]['progres_po'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'type'        => 'hidden',
            ));
        } else {

          $this->data['form'] = array(
            //baris 1
            'nodo'=> array(
                     'placeholder'  => 'No. DO',
                     // 'type'         => 'hidden',
                     'id'           => 'nodo',
                     'name'         => 'nodo',
                     'value'        => $this->val[0]['nodo'],
                     'class'        => 'form-control',
                     'style'        => 'text-transform: uppercase;',
             ),
             'noso'=> array(
                      'placeholder' => 'No. SPK',
                      // 'type'        => 'hidden',
                      'id'          => 'noso',
                      'name'        => 'noso',
                      'value'       => $this->val[0]['noso'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
             ),
             'nosin'=> array(
                     'placeholder'  => 'No Mesin',
                     'value'        => $this->val[0]['nosin'],
                     'id'           => 'nosin',
                     'name'         => 'nosin',
                     'class'        => 'form-control',
                     'style'        => 'text-transform: uppercase;',
                     'readonly'     => '',
             ),
             'norangka'=> array(
                     'placeholder'  => 'No Rangka',
                     'value'        => $this->val[0]['nora'],
                     'id'           => 'norangka',
                     'name'         => 'norangka',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => ''
             ),
             'tgldo'=> array(
                    'placeholder'   => 'Tanggal DO',
                    'id'            => 'tgldo',
                    'name'          => 'tgldo',
                    'value'         => $this->apps->dateConvert($this->val[0]['tgldo']),
                    'class'         => 'form-control',
                    'style'         => '',
                    'readonly'      => '',
            ),
             'nama'=> array(
                      'placeholder' => 'Customer',
                      'id'          => 'nama',
                      'name'        => 'nama',
                      'value'       => $this->val[0]['nama'],
                      'class'       => 'form-control',
                      'style'       => 'text-transform: uppercase;',
                      'readonly'    => '',
             ),
             'kdtipe'=> array(
                     'placeholder'  => 'Tipe Unit',
                     'id'           => 'kdtipe',
                     'class'        => 'form-control',
                     'value'        => $this->val[0]['kdtipe'],
                     'name'         => 'kdtipe',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmtipe'=> array(
                     'placeholder'  => 'Nama Tipe',
                     'value'        => $this->val[0]['nmtipe'],
                     'id'           => 'nmtipe',
                     'name'         => 'nmtipe',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmsales'=> array(
                     'placeholder'  => 'Salesman',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['nmsales'],
                     'id'           => 'nmsales',
                     'name'         => 'nmsales',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmleasing'=> array(
                     'placeholder'  => 'Leasing',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['nmleasing'],
                     'id'           => 'nmleasing',
                     'name'         => 'nmleasing',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmwarna'=> array(
                     'placeholder'  => 'Nama Warna',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['warna'],
                     'id'           => 'nmwarna',
                     'name'         => 'nmwarna',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'tahun'=> array(
                     'placeholder'  => 'Tahun',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['tahun'],
                     'id'           => 'tahun',
                     'name'         => 'tahun',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nopo'=> array(
                     'placeholder'  => 'Faktur',
                     // 'type'         => 'hidden',
                     'value'        => $this->val[0]['nopox'],
                     'id'           => 'nopo',
                     'name'         => 'nopo',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'nmspv'=> array(
                     'placeholder'  => 'Superviser',
                     // 'type'          => 'hidden',
                     'value'        => $this->val[0]['nmsales_header'],
                     'id'           => 'nmspv',
                     'name'         => 'nmspv',
                     'class'        => 'form-control',
                     'style'        => '',
                     'readonly'     => '',
             ),
             'progleas'=> array(
                     'placeholder' => 'Program',
                     'id'          => 'progleas',
                     'class'       => 'form-control',
                     'value'       => $this->val[0]['nmprogleas'],
                     'name'        => 'progleas',
                     'readonly'    => '',
             ),
             'stat_otr'=> array(
                     'placeholder' => 'Status OTR',
                     'id'          => 'stat_otr',
                     'class'       => 'form-control',
                     'value'       => $this->val[0]['status_otr'],
                     'name'        => 'stat_otr',
                     'style'       => '',
                     'readonly'    => '',
             ),
           'tglpo'=> array(
                    'placeholder' => 'Tanggal',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => $this->apps->dateConvert($this->val[0]['tglpo']),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
          'ket'=> array(
                   'placeholder' => 'Keterangan',
                   'id'          => 'ket',
                   'name'        => 'ket',
                   'value'       => set_value('ket'),
                   'class'       => 'form-control',
                   'style'       => '',
                   'readonly'    => '',
           ),

            //baris 2
            'hrg_otr'=> array(
                     'placeholder' => 'Harga OTR',
                     'id'          => 'hrg_otr',
                     'name'        => 'hrg_otr',
                     'value'       => $this->val[0]['harga_otr'],
                     'class'       => 'form-control',
                     'style'       => 'text-align:right;',
                     'readonly'    => '',
                     'required'    => '',
            ),
            'disc_unit1'=> array(
                      'placeholder' => 'Discount Unit',
                      'id'          => 'disc_unit1',
                      'name'        => 'disc_unit1',
                      'value'       => $this->val[0]['disc'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'sub_disc1'=> array(
                      'id'          => 'sub_disc1',
                      'name'        => 'sub_disc1',
                      'value'       => $this->val[0]['disc'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'type'        => 'hidden'
            ),
            'disc_leasing'=> array(
                      'placeholder' => 'Beban Leasing',
                      'id'          => 'disc_leasing',
                      'name'        => 'disc_leasing',
                      'value'       => $this->val[0]['disc_leasing'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'sub_dealer1'=> array(
                      'placeholder' => 'Subsidi Dealer',
                      'id'          => 'sub_dealer1',
                      'name'        => 'sub_dealer1',
                      'value'       => $this->val[0]['sub_dealer'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'sub2();',
                      // 'readonly'    => '',
            ),
            'sub_md1'=> array(
                      'placeholder' => 'Subsidi MD/HSO',
                      'id'          => 'sub_md1',
                      'name'        => 'sub_md1',
                      'value'       => $this->val[0]['sub_md'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub2();',
                      // 'readonly'    => '',
            ),
            'sub_ahm1'=> array(
                      'placeholder' => 'Subsidi AHM',
                      'id'          => 'sub_ahm1',
                      'name'        => 'sub_ahm1',
                      'value'       => $this->val[0]['sub_ahm'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub2();',
                      // 'readonly'    => '',
            ),
            'sub_leas1'=> array(
                      'placeholder' => 'Subsidi Leasing',
                      'id'          => 'sub_leas1',
                      'name'        => 'sub_leas1',
                      'value'       => $this->val[0]['sub_fincoy'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub2();',
                      // 'readonly'    => '',
            ),
                'disc_unit2'=> array(
                          'placeholder' => 'Discount Unit',
                          'id'          => 'disc_unit2',
                          'name'        => 'disc_unit2',
                          'value'       => $this->val[0]['disc2'],
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_dealer2'=> array(
                          'placeholder' => 'Subsidi Dealer',
                          'id'          => 'sub_dealer2',
                          'name'        => 'sub_dealer2',
                          'value'       => $this->val[0]['sub_dealer'],
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_md2'=> array(
                          'placeholder' => 'Subsidi MD/HSO',
                          'id'          => 'sub_md2',
                          'name'        => 'sub_md2',
                          'value'       => $this->val[0]['sub_md'],
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'required'    => '',
                          'readonly'    => '',
                ),
                'sub_ahm2'=> array(
                          'placeholder' => 'Subsidi AHM',
                          'id'          => 'sub_ahm2',
                          'name'        => 'sub_ahm2',
                          'value'       => $this->val[0]['sub_ahm'],
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'readonly'    => '',
                          'required'    => '',
                ),
                'sub_leas2'=> array(
                          'placeholder' => 'Subsidi Leasing',
                          'id'          => 'sub_leas2',
                          'name'        => 'sub_leas2',
                          'value'       => $this->val[0]['sub_fincoy'],
                          'class'       => 'form-control',
                          'style'       => 'text-align:right;',
                          'readonly'    => '',
                          'required'    => '',
                ),
            'hdd1'=> array(
                      'placeholder' => 'HARGA DITERIMA DEALER',
                      'id'          => 'hdd1',
                      'name'        => 'hdd1',
                      'value'       => set_value('hdd1'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'hdd2'=> array(
                      'placeholder' => 'Harga Diterima Dealer',
                      'id'          => 'hdd2',
                      'name'        => 'hdd2',
                      'value'       => set_value('hdd2'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'um_cust'=> array(
                      'placeholder' => 'Uang Muka Customer',
                      'id'          => 'um_cust',
                      'name'        => 'um_cust',
                      'value'       => $this->val[0]['um'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'um'=> array(
                      'placeholder' => 'UANG MUKA',
                      'id'          => 'um',
                      'name'        => 'um',
                      'value'       => set_value('um'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'lunas'=> array(
                      'placeholder' => 'PELUNASAN',
                      'id'          => 'lunas',
                      'name'        => 'lunas',
                      'value'       => set_value('lunas'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'cad_bbn'=> array(
                      'placeholder' => 'Cadangan BBN',
                      'id'          => 'cad_bbn',
                      'name'        => 'cad_bbn',
                      'value'       => $this->val[0]['bbn'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_bbn();'
                      // 'readonly'    => '',
            ),
            'hd_dpp'=> array(
                      'placeholder' => 'Harga Diterima (DPP)',
                      'id'          => 'hd_dpp',
                      'name'        => 'hd_dpp',
                      'value'       => set_value('hd_dpp'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
            ),
            'sub_hso'=> array(
                      'placeholder' => 'Subsidi HSO & AHM',
                      'id'          => 'sub_hso',
                      'name'        => 'sub_hso',
                      'value'       => $this->val[0]['sub_hso'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_bbn();'
                      // 'readonly'    => '',
            ),
            'ins_leas_dpp'=> array(
                      'placeholder' => 'Insentif Leasing (DPP)',
                      'id'          => 'ins_leas_dpp',
                      'name'        => 'ins_leas_dpp',
                      'value'       => $this->val[0]['ins_leasing'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'ins_ai'=> array(
                      'placeholder' => 'Insentif AI',
                      'id'          => 'ins_ai',
                      'name'        => 'ins_ai',
                      'value'       => $this->val[0]['ins_ai'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_bbn();'
                      // 'readonly'    => '',
            ),
            'hbu_dpp'=> array(
                      'placeholder' => 'Harga Beli Unit (DPP)',
                      'id'          => 'hbu_dpp',
                      'name'        => 'hbu_dpp',
                      'value'       => $this->val[0]['harga_beli'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'jaket'=> array(
                      'placeholder' => 'Jaket',
                      'id'          => 'jaket',
                      'name'        => 'jaket',
                      'value'       => $this->val[0]['jaket'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'onkeyup'     => 'sub_hb();'
                      // 'readonly'    => '',
            ),
            'insentif'=> array(
                    'placeholder' => 'Program',
                    'attr'        => array(
                        'id'    => 'insentif',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdsales_ins'],
                    'value'       => $this->val[0]['kdsales_ins'],
                    'name'     => 'insentif',
                    'required'    => '',
                    'readonly'    => '',
            ),
            'ins_sales'=> array(
                      'placeholder' => 'Insentif Sales',
                      'id'          => 'ins_sales',
                      'name'        => 'ins_sales',
                      'value'       => $this->val[0]['ins_sales'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'sub_hb();',
                      'required'    => '',
                      // 'readonly'    => '',
            ),
            'sales_ins'=> array(
                      'id'          => 'sales_ins',
                      'name'        => 'sales_ins',
                      'value'       => set_value('sales_ins'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'required'    => '',
                      'type'        => 'hidden',
            ),
            'biops'=> array(
                      'placeholder' => 'Biops',
                      'id'          => 'biops',
                      'name'        => 'biops',
                      'value'       => $this->val[0]['biops'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'sub_hb();',
                      'required'    => '',
                      // 'readonly'    => '',
            ),
            'komisi'=> array(
                      'placeholder' => 'Komisi',
                      'id'          => 'komisi',
                      'name'        => 'komisi',
                      'value'       => $this->val[0]['komisi'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'sub_hb();',
                      'required'    => '',
                      // 'readonly'    => '',
            ),
            'oth'=> array(
                      'placeholder' => 'Lain-lain',
                      'id'          => 'oth',
                      'name'        => 'oth',
                      'value'       => $this->val[0]['topcover'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'sub_hb();',
                      'required'    => '',
                      // 'readonly'    => '',
            ),
            'hpp'=> array(
                      'placeholder' => 'HPP',
                      'id'          => 'hpp',
                      'name'        => 'hpp',
                      'value'       => set_value('hpp'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'iris'=> array(
                      'placeholder' => 'Iris',
                      'id'          => 'iris',
                      'name'        => 'iris',
                      'value'       => $this->val[0]['iris'],
                      'class'       => 'form-control',
                      'type'        => 'hidden',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'lr_unit'=> array(
                      'placeholder' => 'LABA(RUGI) UNIT',
                      'id'          => 'lr_unit',
                      'name'        => 'lr_unit',
                      'value'       => set_value('lr_unit'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'required'    => '',
                      'readonly'    => '',
            ),
            'jp'=> array(
                      'placeholder' => 'JP',
                      'id'          => 'jp',
                      'name'        => 'jp',
                      'value'       => $this->val[0]['jp'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'jpmatriks();',
                      // 'readonly'    => '',
            ),
            'matriks'=> array(
                      'placeholder' => 'Matriks',
                      'id'          => 'matriks',
                      'name'        => 'matriks',
                      'value'       => $this->val[0]['matriks'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'onkeyup'     => 'jpmatriks();',
                      // 'readonly'    => '',
            ),
            'total'=> array(
                      'placeholder' => 'Total',
                      'id'          => 'total',
                      'name'        => 'total',
                      'value'       => set_value('total'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'hj_net'=> array(
                      'placeholder' => 'HARGA JUAL NET',
                      'id'          => 'hj_net',
                      'name'        => 'hj_net',
                      'value'       => set_value('hj_net'),
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
            ),
            'progres'=> array(
                      'placeholder' => '',
                      'id'          => 'progres',
                      'name'        => 'progres',
                      'value'       => $this->val[0]['progres_po'],
                      'class'       => 'form-control',
                      'style'       => 'text-align:right;',
                      'readonly'    => '',
                      'type'        => 'hidden',
            ));
        }
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->lrnew_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

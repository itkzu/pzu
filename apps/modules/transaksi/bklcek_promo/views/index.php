
<?php
/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div id="load">Loading...</div> 
<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-body">
				<div class="row">

					<div class="col-md-12">
						<div class="form-group has-error">
							<?php
									echo form_label('Masukkan Kata Kunci <small><i>( No. Mesin / No. Rangka / No. DO / Nama Pemilik /  )</i></small>');
									echo form_dropdown($form['nama']['name']
																		,$form['nama']['data']
																		,$form['nama']['value']
																		,$form['nama']['attr']);
									echo form_error('nama','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="row">
							<div class="col-md-4 col-md-4 ">
								<div class="row">

									<div class="col-md-12">
										<div class="form-group">
											<?php
													echo form_label($form['nosin']['placeholder']);
													echo form_input($form['nosin']);
													echo form_error('nosin','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-md-12 ">
										<div class="form-group">
											<?php
													echo form_label($form['nora']['placeholder']);
													echo form_input($form['nora']);
													echo form_error('nora','<div class="note">','</div>');
											?>
										</div>
									</div>

								</div>
							</div>

							<div class="col-md-4 col-md-4 ">
								<div class="row">

									<div class="col-md-12">
										<div class="form-group">
											<?php
													echo form_label($form['nodo']['placeholder']);
													echo form_input($form['nodo']);
													echo form_error('nodo','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<?php
													echo form_label($form['tgldo']['placeholder']);
													echo form_input($form['tgldo']);
													echo form_error('tgldo','<div class="note">','</div>');
											?>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-4 col-md-4 ">
								<div class="row">

									<div class="col-md-12">
										<div class="form-group">
											<?php
													echo form_label($form['nmtipe']['placeholder']);
													echo form_input($form['nmtipe']);
													echo form_error('nmtipe','<div class="note">','</div>');
											?>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<?php
													echo form_label($form['nama_s']['placeholder']);
													echo form_input($form['nama_s']);
													echo form_error('nama_s','<div class="note">','</div>');
											?>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 col-md-12 ">
								<div class="row">

									<div class="col-md-2">
										<div class="form-group">
											<?php
													echo form_label($form['jenis']['placeholder']);
													echo form_input($form['jenis']);
													echo form_error('jenis','<div class="note">','</div>');
											?>
										</div>
									</div> 

									<div class="col-md-10">
										<div class="form-group">
											<?php
													echo form_label($form['promo']['placeholder']);
													echo form_input($form['promo']);
													echo form_error('promo','<div class="note">','</div>');
											?>
										</div>
									</div> 
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item active" role="presentation">
										<a class="nav-link" id="unit-tab" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Data KPB</a>
								</li> 
							</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane active" id="unit" role="tabpanel" aria-labelledby="unit-tab"> 

								<div class="col-md-12">
									<div class="row">
										<div class="col-md-10">
											<div class="form-group"></div>	
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<a class="btn btn-group btn-primary btn-submit"><i class="fa fa-upload"></i> Update</a>		
											</div>	
										</div>
									</div>
								</div> 

								<div class="col-md-12">
									<div class="row">
										<div class="col-md-1 kpb1">
											<div class="form-group">
						                        <?=form_label($form['faktif_kpb1']['placeholder']);?>
						                        <div class="checkbox">
						            				<?php
						                                echo form_checkbox($form['faktif_kpb1']);
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-2 kpb1">
											<div class="form-group">
						                        <?=form_label('&nbsp;');?>
						                        <div class="checkbox">
						            				<?php
														echo form_input($form['c1_kpb1']);
														echo form_error('c1_kpb1','<div class="note">','</div>');
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-1 kpb2">
											<div class="form-group">
						                        <?=form_label($form['faktif_kpb2']['placeholder']);?>
						                        <div class="checkbox">
						            				<?php
						                                echo form_checkbox($form['faktif_kpb2']);
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-2 kpb2">
											<div class="form-group">
						                        <?=form_label('&nbsp;');?>
						                        <div class="checkbox">
						            				<?php
														echo form_input($form['c1_kpb2']);
														echo form_error('c1_kpb2','<div class="note">','</div>');
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-1 kpb3">
											<div class="form-group">
						                        <?=form_label($form['faktif_kpb3']['placeholder']);?>
						                        <div class="checkbox">
						            				<?php
						                                echo form_checkbox($form['faktif_kpb3']);
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-2 kpb3">
											<div class="form-group">
						                        <?=form_label('&nbsp;');?>
						                        <div class="checkbox">
						            				<?php
														echo form_input($form['c1_kpb3']);
														echo form_error('c1_kpb3','<div class="note">','</div>');
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-1 kpb4">
											<div class="form-group">
						                        <?=form_label($form['faktif_kpb4']['placeholder']);?>
						                        <div class="checkbox">
						            				<?php
						                                echo form_checkbox($form['faktif_kpb4']);
						            				?>
						            			</div>
											</div>	
										</div>
										<div class="col-md-2 kpb4">
											<div class="form-group">
						                        <?=form_label('&nbsp;');?>
						                        <div class="checkbox">
						            				<?php
														echo form_input($form['c1_kpb4']);
														echo form_error('c1_kpb4','<div class="note">','</div>');
						            				?>
						            			</div>
											</div>	
										</div>
									</div>
								</div> 

								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group"></div>	
										</div> 
									</div>
								</div> 
								
<!-- 								<div class="col-md-12">
									<div class="row">

										<div class="table-responsive">
											<table class="table table-bordered dataTable">
											<thead>
												<tr class="head-data">  
												</tr>
											</thead> 
											<tbody></tbody>
										</table>
										</div>
									</div>
								</div>  -->
							</div> 
						</div>
					</div>

				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
        $("#load").hide();
		init_nama(); 
		clear();
		$('.kpb1').hide();
		$('.kpb2').hide();
		$('.kpb3').hide();
		$('.kpb4').hide();
		$('#faktif_kpb1').bootstrapToggle({
		    on: 'SUDAH',
		    off: 'BELUM',
		    onstyle: 'success',
		    offstyle: 'danger'
		}); 
		$('#faktif_kpb2').bootstrapToggle({
		    on: 'SUDAH',
		    off: 'BELUM',
		    onstyle: 'success',
		    offstyle: 'danger'
		}); 
		$('#faktif_kpb3').bootstrapToggle({
		    on: 'SUDAH',
		    off: 'BELUM',
		    onstyle: 'success',
		    offstyle: 'danger'
		});
		$('#faktif_kpb4').bootstrapToggle({
		    on: 'SUDAH',
		    off: 'BELUM',
		    onstyle: 'success',
		    offstyle: 'danger'
		}); 

		$("#nama").change(function(){ 
			$("#load").show();
			getDataUnit(); 
		}); 

		$('.btn-submit').click(function(){
			update();
		});
	}); 

function getDataUnit(){
	var nama = $("#nama").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("bklcek_promo/getDataUnit");?>",
		data: {"nama":nama},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama
				$("#nosin").val(value.nosin); 
				$("#nora").val(value.nora);
				$("#nodo").val(value.nodo);
				$("#tgldo").val(moment(value.tgldo).format('LL'));
				$("#nmtipe").val(value.nmtipe);
				$("#nama_s").val(value.nama_s);
				$("#jenis").val(value.jenis);
				$("#promo").val(value.promo);
				getDataKPB();


				if(value.jenis==='KPB 3'){
					$('.kpb1').show(); 
					$('.kpb2').show(); 
					$('.kpb3').show();  
					$('.kpb4').hide();
				} else {
					$('.kpb1').show(); 
					$('.kpb2').show(); 
					$('.kpb3').show(); 
					$('.kpb4').show(); 
				} 
			});
		},
		error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}

function getDataKPB(){
	var nosin = $("#nosin").val();
	$.ajax({
		type: "POST",
		url: "<?=site_url("bklcek_promo/getDataKPB");?>",
		data: {"nosin":nosin},
		beforeSend: function() {
		},
		success: function(resp){
			var obj = jQuery.parseJSON(resp);
			$.each(obj, function(key, value){
				//menu utama 
				$('#c1_kpb1').val(value.kpb1);
				$('#c1_kpb2').val(value.kpb2);
				$('#c1_kpb3').val(value.kpb3);
				$('#c1_kpb4').val(value.kpb4);
				if(value.cx_kpb1==='CHECKED'){
					$('#faktif_kpb1').is(':checked');
				} 
				if(value.cx_kpb2==='CHECKED'){
					$('#faktif_kpb2').is(':checked');
				} 
				if(value.cx_kpb3==='CHECKED'){
					$('#faktif_kpb3').is(':checked');
				} 
				   if($("#jenis").val() === 'KPB 3'){
				   		$('#faktif_kpb4').prop('checked', false);
				   }
				   else {
						if(value.cx_kpb4==='CHECKED'){
							$('#faktif_kpb4').is(':checked');
						} 
				   }
				$('#faktif_kpb1').val(value.cx_kpb1);
				$('#faktif_kpb3').val(value.cx_kpb2);
				$('#faktif_kpb3').val(value.cx_kpb3);
				$('#faktif_kpb4').val(value.cx_kpb4);
				$("#load").fadeOut();
			});
		},
		error:function(event, textStatus, errorThrown) {
				swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
		}
	});
}
 
function update(){
	var nosin = $('#nosin').val();
	if($('#faktif_kpb1').is(':checked')){
		var fkpb1 = 'TRUE';
	} else {
		var fkpb1 = 'FALSE';
	}
	if($('#faktif_kpb2').is(':checked')){
		var fkpb2 = 'TRUE';
	} else {
		var fkpb2 = 'FALSE';
	}
	if($('#faktif_kpb3').is(':checked')){
		var fkpb3 = 'TRUE';
	} else {
		var fkpb3 = 'FALSE';
	}
	if($('#faktif_kpb4').is(':checked')){
		var fkpb4 = 'TRUE';
	} else {
		var fkpb4 = 'FALSE';
	}
	var tglkpb1 = $('#faktif_kpb1').val();
	var tglkpb2 = $('#faktif_kpb2').val();
	var tglkpb3 = $('#faktif_kpb3').val();
	var tglkpb4 = $('#faktif_kpb4').val();
 
	// alert($('#faktif_kpb1').is(':checked'));
    swal({
        title: "Konfirmasi",
        text: "Proses Update KPB, data tidak dapat dikembalikan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#c9302c",
        confirmButtonText: "Ya, Lanjutkan!",
        cancelButtonText: "Batalkan!",
        closeOnConfirm: true
        }, 
            
        function () { 
            $.ajax({
                type: "POST",
                url: "<?=site_url('bklcek_promo/proses');?>",
                data: {"nosin":nosin,"fkpb1":fkpb1,"tglkpb1":tglkpb1,"fkpb2":fkpb2,"tglkpb2":tglkpb2,"fkpb3":fkpb3,"tglkpb3":tglkpb3,"fkpb4":fkpb4,"tglkpb4":tglkpb4},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    //alert(JSON.stringify(resp));
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
          					window.location.href = '<?=site_url('bklcek_promo');?>';
                    	});
                	});
            	},
            	error: function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }
    );
}

function formatDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

function setBgColour(val,object){
	if(val){
		$("#"+object).css("background-color", "#fff");
	}else{
		$("#"+object).css("background-color", "#eee");
	}
}

function init_nama(){
	$("#nama").select2({
		ajax: {
			url: "<?=site_url('bklcek_promo/getNama');?>",
			type: 'post',
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term,
					page: params.page
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total_count
					}
				};
			},
			cache: true
		},
		placeholder: 'Masukkan Nama Pemilik ...',
		dropdownAutoWidth : false,
					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
					minimumInputLength: 3,
					templateResult: format_nama, // omitted for brevity, see the source of this page
					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
				});
}

function format_nama_terpilih (repo) {
	return repo.full_name || repo.text;
}

function format_nama (repo) {
	if (repo.loading) return "Mencari data ... ";


	var markup = "<div class='select2-result-repository clearfix'>" +
	"<div class='select2-result-repository__meta'>" +
	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>"; 

	markup += "<div class='select2-result-repository__statistics'>" +
	"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.nosin + " | <i class=\"fa fa-tag\"></i> " + repo.id + " | <i class=\"fa fa-motorcycle\"></i> " + repo.nmtipe + " </div>" +
	"<div class='select2-result-repository__stargazers'> <i class=\"fa fa-file-o\"></i> " + repo.nodo + " | <i class=\"fa fa-calendar\"></i> " + repo.tgldo + "</div>" +
	"</div>" +
	"</div>" +
	"</div>";
	return markup;
}
function clear(){
	$("#nosin").val(''); 
	$("#nora").val('');
	$("#nodo").val('');
	$("#tgldo").val('');
	$("#nmtipe").val('');
	$("#nama_s").val('');
	$("#jenis").val('');
	$("#promo").val('');
}
</script>

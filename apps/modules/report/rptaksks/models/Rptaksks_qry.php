<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptaksks_qry
 *
 * @author adi
 */
class Rptaksks_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getAks() {
        $this->db->select("kdaks,nmaks");
        $this->db->order_by("nmaks","ASC");
        $query = $this->db->get("pzu.v_aks");
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }

    public function getData() {
        $kdaks = $this->input->post('kdaks');
        $this->db->where('LOWER(nmaks)', strtolower($kdaks));
        $this->db->order_by("nmaks","ASC");
        $this->db->select("kdaks,nmaks");
        $query = $this->db->get('pzu.v_aks');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = "";
        }
        return json_encode($res);
    }

    public function getNama() {
        $q = $this->input->post('q');
        $this->db->like('LOWER(nmaks)', strtolower($q));
        //$this->db->or_like('LOWER(alamat)', strtolower($q));
        $this->db->order_by("case
                                when LOWER(nmaks) like '".strtolower($q)."' then 1
                                when LOWER(nmaks) like '".strtolower($q)."%' then 2
                                when LOWER(nmaks) like '%".strtolower($q)."' then 3
                                else 4 end");
        $this->db->order_by("nmaks");
        $query = $this->db->get('pzu.v_aks');
        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->result_array();
            foreach ($res as $value) {
                $d_arr[] = array(
                    'id' => $value['kdaks'],
                    'text' => $value['nmaks']
                );

            }
            $data = array(
                'total_count' => $query->num_rows(),
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }else{
            $d_arr[] = array(
                'id' => '',
                'text' => ''
            );

            $data = array(
                'total_count' => 0,
                'incomplete_results' => true,
                'items' => $d_arr
            );
            return json_encode($data);
        }
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdaks']) ){
            $kdaks = $_GET['kdaks'];
        }else{
            $kdaks = '';
        }
        if( isset($_GET['periode_awal']) ){
            if($_GET['periode_awal']){
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
            }else{
                $periode_awal = '';
            }
        }else{
            $periode_awal = '';
        }

        if( isset($_GET['periode_akhir']) ){
            if($_GET['periode_akhir']){
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl1[1].$tgl1[0];
            }else{
                $periode_akhir = '';
            }
        }else{
            $periode_akhir = '';
        }

        if( isset($_GET['periode']) ){
            if($_GET['periode']){
                $tgl2 = explode('-',$_GET['periode']);
                $periode = $tgl2[0].$tgl2[1]; //$this->apps->dateConvert($_GET['periode_akhir']);//
            }else{
                $periode = '';
            }
        }else{
            $periode = '';
        }

        if( isset($_GET['ket']) ){
            if($_GET['ket']){
                $ket = $_GET['ket'];//$tgl1[1].$tgl1[0];
            }else{
                $ket = '';
            }
        }else{
            $ket = '';
        }

        if($ket=='periode_between'){
          $where1=" ( SELECT ket, noref, tgl, saw, qtyin, qtyout, qtyadj, sak
                                FROM pzu.sl_aks_ks('".$this->session->userdata('data')['kddiv']."',".$kdaks.",'".$periode_awal."','".$periode_akhir."')) AS a";
        } else {
          $where1=" ( SELECT ket, noref, tgl, saw, qtyin, qtyout, qtyadj, sak
                                FROM pzu.sl_aks_ks('".$this->session->userdata('data')['kddiv']."',".$kdaks.",'".$periode."')) AS a";
        }

        $aColumns = array('ket', 'noref', 'tgl', 'saw', 'qtyin', 'qtyout', 'qtyadj', 'sak');
    	$sIndexColumn = "noref";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = $where1;
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = "";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

                //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }
}

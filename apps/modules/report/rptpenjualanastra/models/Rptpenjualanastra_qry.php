<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptpenjualanastra_qry
 *
 * @author adi
 */
class Rptpenjualanastra_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['periode_awal']) ){
            if($_GET['periode_awal']){
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode_awal = $this->apps->dateConvert($_GET['periode_awal']);//$tgl1[1].$tgl1[0];
            }else{
                $periode_awal = '';
            }
        }else{
            $periode_awal = '';
        }

        if( isset($_GET['periode_akhir']) ){
            if($_GET['periode_akhir']){
                //$tgl2 = explode('-', $_GET['periode_akhir']);
                $periode_akhir = $this->apps->dateConvert($_GET['periode_akhir']);//$tgl2[1].$tgl2[0];
            }else{
                $periode_akhir = '';
            }
        }else{
            $periode_akhir = '';
        }
        $aColumns = array('nodo'
                            ,'tgldo'
                            ,'noso'
                            ,'nama'
                            ,'tk'
                            ,'nmleasingx'
                            ,'nmprogleas'
                            ,'tenor'
                            ,'angsuran'
                            ,'status_otr'
                            ,'nama_s'
                            ,'alamatktp_s'
                            ,'kel_s'
                            ,'kec_s'
                            ,'kota_s'
                            ,'nohp_s'
                            ,'kode'
                            ,'kdtipe'
                            ,'nmtipe'
                            ,'nmwarna'
                            ,'nosin'
                            ,'nora'
                            ,'harga_otr'
                            ,'disc'
                            ,'sub_dealer'
                            ,'sub_md'
                            ,'sub_ahm'
                            ,'disc_leasing'
                            ,'tot_disc'
                            ,'disc_dealer'
                            ,'harga_net'
                            ,'bbn'
                            ,'harga_offtr'
                            ,'dpp'
                            ,'ppn'
                            ,'um'
                            ,'um_riel'
                            ,'ar_konsumen'
                            ,'ar_fincoy'
                            ,'sub_hso'
                            ,'ins_leasing'
                            ,'harga_beli'
                            ,'komisi'
                            ,'jaket'
                            ,'topcover'
                            ,'ins_sales'
                            ,'by_lgsg'
                            ,'jp_dpp'
                            ,'gross_profit'
                            ,'biops'
                            ,'matriks_dpp'
                            ,'labarugi'
                            ,'nmsales'
                            ,'nmsales_2'
                            ,'nmdiv'
                            ,'ket_do'
                            ,'ket'
                            ,'dpp_total'
                            ,'ppn_total'
                            ,'pph_total'
                            ,'ar_total'
                            ,'periode'
                            ,'tgl_do');
	$sIndexColumn = "nodo";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT nodo
                ,to_char(tgldo,'DD-MM-YYYY')  AS tgldo
                ,noso
                ,nama
                ,tk
                ,nmleasingx
                ,nmprogleas
                ,tenor
                ,angsuran
                ,status_otr
                ,nama_s
                ,alamatktp_s
                ,kel_s
                ,kec_s
                ,kota_s
                ,nohp_s
                ,kode
                ,kdtipe
                ,nmtipe
                ,nmwarna
                ,nosin
                ,nora
                ,harga_otr
                ,disc
                ,sub_dealer
                ,sub_md
                ,sub_ahm
                ,disc_leasing
                ,tot_disc
                ,disc_dealer
                ,harga_net
                ,bbn
                ,harga_net-bbn AS harga_offtr
                ,dpp_unit AS dpp
                ,ppn_unit as ppn
                ,um
                ,um_riel
                ,ar_konsumen
                ,ar_fincoy
                ,sub_hso
                ,ins_leasing
                ,harga_beli
                ,komisi
                ,jaket
                ,topcover
                ,ins_sales
                ,by_lgsg
                ,jp_dpp
                ,gross_profit
                ,biops
                ,matriks_dpp
                ,labarugi
                ,nmsales
                ,nmsales_2
                ,nmdiv
                ,ket_do
                ,ket
                ,dpp_total
                ,ppn_total
                ,pph_total
                ,ar_total
                ,periode
                ,tgldo AS tgl_do
                from pzu.vl_do_astra ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "WHERE (to_char(tgl_do,'YYYY-MM-DD') between '".$periode_awal."' AND '".$periode_akhir."')";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '';
                    }
                    if(($i>21 && $i<=51) || ($i>56 && $i<=60) || $i==8){
                        $row[] = (float) $aRow[ $aColumns[$i] ];

                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                //23 - 28
                //$row[8] = (float) $aRow['angsuran'];
               $output['aaData'][] = $row;
	}
	echo  json_encode( $output );
    }

    public function submit() {
        try {
            $array = $this->input->post();
            if(empty($array['id'])){
                unset($array['id']);
                $resl = $this->db->insert('menus',$array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && empty($array['stat'])){
                if($array['mainmenuid']=="" || empty($array['mainmenuid'])){
                    unset($array['mainmenuid']);
                    $this->db->set('mainmenuid', 'NULL', FALSE);
                }
                $this->db->where('id', $array['id']);
                $resl = $this->db->update('menus', $array);
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && !empty($array['stat'])){
                $this->db->where('id', $array['id']);
                $resl = $this->db->delete('menus');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }
            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes); 
        ?> 
          <div class="box-body">
            <div class="form-group">
                <?php
                    echo form_input($form['id']);
                
                    echo form_label($form['periode']['placeholder']);
                    echo form_input($form['periode']);
                    echo form_error('periode','<div class="note">','</div>'); 
                ?>
            </div>
            <div class="form-group">
                <?php
                    echo form_label('Pilih Tipe Series');
                    echo form_dropdown($form['kdtipesr']['name'],$form['kdtipesr']['data'] ,$form['kdtipesr']['value'] ,$form['kdtipesr']['attr']);
                    echo form_error('kdtipesr','<div class="note">','</div>'); 
                ?>
            </div>     
            <div class="form-group">
                <?php 
                    echo form_label($form['cp_target_total']['placeholder']);
                    echo form_input($form['cp_target_total']);
                    echo form_error('cp_target_total','<div class="note">','</div>'); 
                ?>
            </div>        
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">
                Simpan
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>    
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#jml_hr_kerja").keyup(function(){
            getCtargetHari();
        });
        $("#cp_target_total").keyup(function(){
            getCtargetHari();
        });
            $('#periode').on("changeDate", function(e) {
            console.log(e);
            getDataCapaian();
        });
        $('#kdtipesr').change(function(){
            getDataCapaian();
        });
        function getCtargetHari(){
            var jml_hr_kerja = $("#jml_hr_kerja").val();
            var cp_target_total = $("#cp_target_total").val();
            var cp_target_harian = 0;
            cp_target_harian = Number(cp_target_total) / Number(jml_hr_kerja);
            $("#cp_target_harian").val(cp_target_harian.toFixed(0));
        }
        
        function getDataCapaian(){
            var periode = $('#periode').val();
            var kdtipesr = $('#kdtipesr').val();
            $.ajax({
                type: "POST",
                url: "<?=site_url('mstcptargettipe/select_data');?>",
                data: {"periode":periode,"kdtipesr":kdtipesr},
                beforeSend: function(){
                    $("#id").val('');
                    $("#jml_hr_kerja").val('');
                    $("#cp_target_total").val('');
                    $("#cp_target_harian").val('');
                },
                success: function(resp){   
                    var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){
                        $("#id").val(data.id);
                        $("#jml_hr_kerja").val(data.jml_hr_kerja);
                        $("#cp_target_total").val(data.cp_target_total);
                        $("#cp_target_harian").val(data.cp_target_harian);
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }
    });
</script>
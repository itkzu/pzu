	<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-header">
				<!--<a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
				<a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>-->
				<!--
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-wrench"></i></button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo $add;?>" >Tambah Data Baru</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="btn-refersh">Refresh</a>
							</li>
						</ul>
					</div>
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
				-->

			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<p><a id="aktif" class="btn btn-primary">Aktifkan</a>
				<a id="nonaktif" class="btn btn-danger">Nonaktifkan</a></p>
				<p id="alldata" class="kata"></p>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover js-basic-example dataTable" class="display select">
							<thead>
								<tr>
									<th style="width: 10px;text-align: center;"></th>
									<th style="width: 10px;text-align: center;">Kode</th>
									<th style="text-align: center;">Tipe</th>
									<th style="text-align: center;">Jenis</th>
									<th style="width: 60px;text-align: center;">Grup</th>
									<th style="width: 10px;text-align: center;">Status</th>

								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>No.</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>

								</tr>
							</tfoot>
							<tbody></tbody>
						</table>
					</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {

		/*$(".btn-refersh").click(function(){
			table.ajax.reload();
		});*/


		$("#nonaktif").click(function(){
			set_aktif(false);
		});


		$("#aktif").click(function(){
			set_aktif(true);
		});






		//var rows_selected = [];
		var column = [];

		column.push({
			"aTargets": [ 1,2,3,4,5 ],
			"orderable": true,
			"searchable": true,
		});

		column.push({
			"aTargets":  0 ,
			"searchable": false,
			"orderable": false,
			"checkboxes": {
				'selectRow': true
			}

		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"bProcessing": true,
			"select":{
				style: 'multi',
			},
			"fixedColumns": {
				leftColumns: 2
			},
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Semua Data"]],
			"bPaginate": true,
			"bDestroy": true,
			"bServerSide": true,
			"order": [[ 1, 'asc' ]],
			"rowCallback": function(row, data, dataIndex){
			/*
		       	// Get row ID
		        var rowId = data[0];

		        // If row ID is in the list of selected row IDs
		        if($.inArray(rowId, rows_selected) !== -1){
					$(row).find('input[type="checkbox"]').prop('checked', true);
					$(row).addClass('selected');
				}
			*/
			},

			// "pagingType": "simple",
			"sAjaxSource": "<?=site_url('mstunit/json_dgview');?>",
			"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
			}
		});

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Hapus" && title!=="No."){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		} );

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
	});



	function set_aktif(faktif){
		if (faktif){
			var msg = 'Aktif';
		} else{
			var msg = 'Non-aktif';
		}

		swal({
			title: 'Konfirmasi untuk '+msg+'kan Data',
			text: "Apakah Anda Yakin?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, '+msg+'kan!',
			cancelButtonText: "Batalkan!",
			closeOnConfirm: true
		}, function (){
			var rows_selected = table.column(0).checkboxes.selected();
			$('#alldata').val(rows_selected.join(","));
			var id_data = $("#alldata").val();
			// console.log(id_data);
			$.ajax({
				type: "POST",
				url: "<?=site_url('mstunit/proses_setaktif');?>",
				data: {"iddata":id_data
					  ,"faktif":faktif
				},
				success: function(resp){
					var obj = JSON.parse(resp);
					$.each(obj, function(key, data){
						swal({
							title: data.title,
							text: data.msg,
							type: data.tipe
						}, function(){
							table.ajax.reload();
						});
					});
				},
				error: function(event, textStatus, errorThrown) {
					swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				}
			});
		})
	}


/*
	function deleted(id){
		swal({
			title: "Konfirmasi Hapus !",
			text: "Data yang dihapus tidak dapat dikembalikan!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#c9302c",
			confirmButtonText: "Ya, Lanjutkan!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false
		}, function () {
			var submit = "<?php echo $submit;?>";
				$.ajax({
					type: "POST",
					url: submit,
					data: {"id":id,"stat":"delete"},
					success: function(resp){
						var obj = jQuery.parseJSON(resp);
						if(obj.state==="1"){
							swal({
								title: "Terhapus",
								text: obj.msg,
								type: "success"
							}, function(){
								table.ajax.reload();
							});
						}else{
							swal("Gagal!", obj.msg, "error");
						}
					},
					error:function(event, textStatus, errorThrown) {
						swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
					}
				});
		});
	}*/
</script>

<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto; 
    }
</style>
<?php

/* 
 * ***************************************************************
 * Script : html.php
 * Version : 
 * Date : Oct 31, 2017 11:46:13 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);
$array = $this->input->post();
$decode = json_decode($decode); 
// echo "<script>console.log('" . $decode->cabang . "');</script>"; 
// echo $decode->data->nmcabang;
if($array['submit']==="tunai_excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAPORAN PEMBELIAN SPAREPART TUNAI DARI " .$array['periode_awal'] ." SAMPAI " .$array['periode_akhir'] ." ".date('Y-m-d H:i:s').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');
//$caption = "<b>DAFTAR PIUTANG PELUNASAN LEASING </b>"
//        . "<br>"
//        . "<b>". strtoupper($this->apps->title)." - ". strtoupper($this->apps->logintag)."</b>"
//        . "<br>"
//        . "<b> PER TANGGAL " . $array['periode_akhir'] ."</b>"
//        . "<br><br>";

$this->table->add_row(array(
    array('data' => '<b>DAFTAR PEMBELIAN SPAREPART TUNAI</b>'
                        , 'colspan' => '11'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$this->table->add_row(array(
    array('data' => "<b>". strtoupper($this->apps->title)." - ". strtoupper($decode->cabang)."</b>"
                        , 'colspan' => '11'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);   
$this->table->add_row(array(
    array('data' => "<b> DARI  " . $array['periode_awal'] ." TANGGAL " . $array['periode_akhir'] ."</b>"
                        , 'colspan' => '11'
                        , 'align' => 'center'
                        , 'style' => ' font-size: 12px;'),
        )
);  
$namaheader = array(
    array('data' => 'NO'
                        , 'align' => 'center'
                        , 'width' => '10px'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'TANGGAL'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'NO. FAKTUR'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'NAMA SUPPLIER'
                        , 'align' => 'center'
                        , 'style' => ' width: 100px; font-size: 12px;'),
    array('data' => 'KODE SPAREPART'
                        , 'align' => 'center'
                        , 'style' => ' width: 100px; font-size: 12px;'),
    array('data' => 'NAMA SPAREPART'
                        , 'align' => 'center'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'QUANTITY'
                        , 'align' => 'center'
                        , 'style' => ' width: 10px; font-size: 12px;'),
    array('data' => 'HARGA /PCS'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'DISCOUNT'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'HARGA NETO /PCS'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
    array('data' => 'JUMLAH'
                        , 'align' => 'center'
                        , 'style' => ' width: 50px; font-size: 12px;'),
);
$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);
// Caption text
//$this->table->set_caption($caption);
$this->table->add_row($namaheader);

$leas = array();
foreach ($decode->leasing as $leasing) {
    $leas[$leasing] = $leasing;
}
$res = array();
$g_hrg = 0;
$g_dsc = 0;
$g_nto = 0;
$g_tot = 0;
foreach ($leas as $v) {
    $no = 1;
    $s_hrg = 0;
    $s_dsc = 0;
    $s_nto = 0;
    $s_tot = 0;
    foreach ($decode->data as $data) {
        if($v==$data->kdgrup){          
            $akun_d = array(
                array('data' => $data->no
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->podate
                                    , 'style' => 'text-align: center; font-size: 12px;'),
                array('data' => $data->nofaktur
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $data->nmsup
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $data->materialno
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $data->nmpart
                                    , 'style' => 'text-align: left; font-size: 12px;'),
                array('data' => $data->qty
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->hargabeli, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->nilaidiscount, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->hrgneto, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
                array('data' => number_format($data->tot, 0,',','.')
                                    , 'style' => 'text-align: right; font-size: 12px;'),
            );                     
            $this->table->add_row($akun_d);    
            $s_hrg+=$data->hargabeli;
            $s_dsc+=$data->nilaidiscount;
            $s_nto+=$data->hrgneto;
            $s_tot+=$data->tot;
        }
    }
    $separator = array(
        array('data' => '<b>TOTAL ' . $v.'</b>'
                            , 'colspan' => '7'
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => '<b>'.number_format($s_hrg, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_dsc, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_nto, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
        array('data' => '<b>'.number_format($s_tot, 0,',','.').'</b>'
                            , 'style' => 'text-align: right; font-size: 12px;'), 
    );
    $this->table->add_row($separator);    
    $g_hrg = $g_hrg+$s_hrg;
    $g_dsc = $g_dsc+$s_dsc;
    $g_nto = $g_nto+$s_nto;
    $g_tot = $g_tot+$s_tot;
}

$g_total = array(
    array('data' => '<b>GRAND TOTAL</b>'
                        , 'colspan' => '7'
                        , 'style' => 'text-align: left; font-size: 12px;'),
    array('data' => '<b>'.number_format($g_hrg, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_dsc, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_nto, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
    array('data' => '<b>'.number_format($g_tot, 0,',','.').'</b>'
                        , 'style' => 'text-align: right; font-size: 12px;'), 
);
$this->table->add_row($g_total); 


// $desc_1 = array(
//     array('data' => '&nbsp;'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => 'DIBUAT OLEH'
//                         , 'colspan' => '2'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => '&nbsp;'
//                         , 'colspan' => '3'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => 'MENGETAHUI'
//                         , 'colspan' => '5'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
// );
// $this->table->add_row($desc_1);   
// for($i=1;$i<=5;$i++){
//   $this->table->add_row(array(
//         array('data' => '&nbsp;'
//                             , 'colspan' => '11'
//                             , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     ));   
// }
// $desc_2 = array(
//     array('data' => '&nbsp;'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => $this->perusahaan[0]['nmadh']
//                         , 'colspan' => '2'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => '&nbsp;'
//                         , 'colspan' => '3'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
//     array('data' => $this->perusahaan[0]['nmkacab']
//                         , 'colspan' => '5'
//                         , 'style' => 'text-align: left; font-size: 12px;border: none;'),
// );
// $this->table->add_row($desc_2); 

$this->table->set_template($template);
echo $this->table->generate();      
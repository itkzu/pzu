<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Slnew
 *
 * @author adi
 */
class Slnew extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('slnew/submit'),
            'add' => site_url('slnew/add'),
            'edit' => site_url('slnew/edit'),
            'reload' => site_url('slnew'),
        );
        $this->load->model('slnew_qry');
        $cabang = $this->slnew_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }

        $this->data['tipebayar'] = array(
            "T" => "TUNAI",
            "K" => "KREDIT"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function set_faktur() {
        echo $this->slnew_qry->set_faktur();
    }

    public function getdetPO() {
        echo $this->slnew_qry->getdetPO();
    }

    public function getdetHSO() {
        echo $this->slnew_qry->getdetHSO();
    }

    public function getdetHSO2() {
        echo $this->slnew_qry->getdetHSO2();
    }

    public function getdetHSO3() {
        echo $this->slnew_qry->getdetHSO3();
    }

    public function caripo() {
        echo $this->slnew_qry->caripo();
    }

    public function addpo() {
        echo $this->slnew_qry->addpo();
    }

    public function addpo2() {
        echo $this->slnew_qry->addpo2();
    }

    public function addpo3() {
        echo $this->slnew_qry->addpo3();
    }

    public function updatepo() {
        echo $this->slnew_qry->updatepo();
    }

    public function updatepo2() {
        echo $this->slnew_qry->updatepo2();
    }

    public function updatepo3() {
        echo $this->slnew_qry->updatepo3();
    }

    public function kdlokasi() {
        echo $this->slnew_qry->kdlokasi();
    }

    public function deleted() {
        echo $this->slnew_qry->deleted();
    }

    public function deletepo() {
        echo $this->slnew_qry->deletepo();
    }

    public function getKdSup() {
        echo $this->slnew_qry->getKdSup();
    }

    public function getSLHSO() {
        echo $this->slnew_qry->getSLHSO();
    }

    public function getSLHSO2() {
        echo $this->slnew_qry->getSLHSO2();
    }

    public function getSLHSO3() {
        echo $this->slnew_qry->getSLHSO3();
    }

    public function getTipeUnit() {
        echo $this->slnew_qry->getTipeUnit();
    }

    public function getKodeSpkHso() {
        echo $this->slnew_qry->getKodeSpkHso();
    }

    public function getKodeSalesHeader() {
        echo $this->slnew_qry->getKodeSalesHeader();
    }

    public function getKdWarna() {
        echo $this->slnew_qry->getKdWarna();
    }

    public function getKdWarnaHeader() {
        echo $this->slnew_qry->getKdWarnaHeader();
    }

    public function getKodeTipeHeader() {
        echo $this->slnew_qry->getKodeTipeHeader();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
          'kdsup'     => array(
                  'placeholder' => 'Supplier',
                  'attr'        => array(
                      'id'      => 'kdsup',
                      'class'   => 'form-control',
                  ),
                  'data'        => '',
                  'class'       => 'form-control',
                  'value'       => set_value('kdsup'),
                  'name'        => 'kdsup',
                  // 'readonly' => '',
          ),
          'nopo'      => array(
                  'placeholder' => 'No. Faktur',
                  //'type'        => 'hidden',
                  'id'          => 'nopo',
                  'name'        => 'nopo',
                  'value'       => set_value('nopo'),
                  'class'       => 'form-control',
                  'style'       => 'text-transform: uppercase;',
            ),
          'tipebayar' => array(
                  'placeholder' => 'Pembayaran',
                  'attr'        => array(
                      'id'      => 'tipebayar',
                      'class'   => 'form-control',
                  ),
                  'data'        => $this->data['tipebayar'],
                  'class'       => 'form-control',
                  'value'       => set_value('tipebayar'),
                  'name'        => 'tipebayar',
                    // 'readonly' => '',
            ),
          'jth_tmp'   => array(
                  'placeholder' => 'Jatuh Tempo',
                  'id'          => 'jth_tmp',
                  'class'       => 'form-control',
                  'class'       => 'form-control',
                  'value'       => set_value('jth_tmp'),
                  'name'        => 'jth_tmp',
                    // 'readonly' => '',
            ),
          'tgltmp'    => array(
                  'id'          => 'tgltmp',
                  'name'        => 'tgltmp',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'readonly'    => ''
            ),
          'tglpo'     => array(
                  'placeholder' => 'Tanggal Faktur/Nota',
                  'id'          => 'tglpo',
                  'name'        => 'tglpo',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control calendar',
                  'style'       => '',
            ),
          'tgltrm'    => array(
                  'placeholder' => 'Tanggal Terima',
                  'id'          => 'tgltrm',
                  'name'        => 'tgltrm',
                  'value'       => date('d-m-Y'),
                  'class'       => 'form-control calendar',
                  'style'       => '',
            ),
          'ket'    => array(
                  // 'placeholder' => 'ket',
                  'id'          => 'ket',
                  'name'        => 'ket',
                  'value'       => set_value('ket'),
                  'class'       => 'form-control',
                  'style'       => '',
                  'type'       => 'hidden',
            ),

          //detail
          'noslhso'     => array(
                  'attr'        => array(
                       'id'     => 'noslhso',
                       'class'  => 'form-control',
                   ),
                  'data'        => '',
                  'placeholder' => 'No. Shipping List HSO',
                  'name'        => 'noslhso',
                  'value'       => set_value('nospk'),
                  'style'       => '',
            ),
          'noslhso2'     => array(
                  'attr'        => array(
                       'id'     => 'noslhso2',
                       'class'  => 'form-control',
                   ),
                  'data'        => '',
                  'placeholder' => 'No. Shipping List HSO',
                  'name'        => 'noslhso2',
                  'value'       => set_value('nospk2'),
                  'style'       => '',
            ),
          'noslhso3'     => array(
                  'attr'        => array(
                       'id'     => 'noslhso3',
                       'class'  => 'form-control',
                   ),
                  'data'        => '',
                  'placeholder' => 'No. Shipping List HSO',
                  'name'        => 'noslhso3',
                  'value'       => set_value('nospk3'),
                  'style'       => '',
            ),
            'statpo'     => array(
                    'id'          => 'statpo',
                    'class'       => 'form-control',
                    'data'        => '',
                    'name'        => 'statpo',
                    'value'       => set_value('statpo'),
                    'style'       => '',
                    'readonly'    => ''
                    // 'type'       => 'hidden',
              ),
              'kdlokasi'     => array(
                      'id'          => 'kdlokasi',
                      'class'       => 'form-control',
                      'data'        => '',
                      'name'        => 'kdlokasi',
                      'value'       => set_value('kdlokasi'),
                      'style'       => '',
                      'readonly'    => '',
                      'type'       => 'hidden',
                ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(
          // 'nodf'=> array(
          //          'placeholder' => 'No. DF',
          //          //'type'        => 'hidden',
          //          'id'          => 'nodf',
          //          'name'        => 'nodf',
          //          'value'       => $this->val[0]['nodf'],
          //          'class'       => 'form-control',
          //          'style'       => '',
          //          'readonly'    => '',
          //  ),

              'noso'=> array(
                       'placeholder' => 'No. SPK',
                       //'type'        => 'hidden',
                       'id'          => 'noso',
                       'name'        => 'noso',
                       'value'       => set_value('noso'),
                       'class'       => 'form-control',
                       'style'       => '',
               ),
              'tglso'=> array(
                       'placeholder' => 'Tanggal SPK',
                       'id'          => 'tglso',
                       'name'        => 'tglso',
                       'value'       => date('d-m-Y'),
                       'class'       => 'form-control calendar',
                       'style'       => '',
               ),
              'nospk'=> array(
                        'attr'        => array(
                            'id'    => 'nospk',
                            'class' => 'form-control',
                        ),
                        'data'     => '',
                       'placeholder' => 'No. SPK HSO',
                       'name'        => 'nospk',
                       'value'       => set_value('nospk'),
                       'style'       => '',
               ),
               'nama1'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama1',
                        'name'        => 'nama1',
                        'value'       => set_value('nama1'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat1'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat1'),
                       'id'          => 'alamat1',
                       'name'        => 'alamat1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel1'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel1'),
                       'id'          => 'kel1',
                       'name'        => 'kel1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'noktp'=> array(
                       'placeholder' => 'No KTP',
                       'value'       => set_value('noktp'),
                       'id'          => 'noktp',
                       'name'        => 'noktp',
                       'class'       => 'form-control',
                       // 'type'        => 'hidden',
                       'style'       => '',
               ),
               'kec1'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec1'),
                       'id'          => 'kec1',
                       'name'        => 'kec1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'hohp1'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp1'),
                       'id'          => 'hohp1',
                       'name'        => 'hohp1',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'notelp'=> array(
                       'placeholder' => 'No. Telp',
                       // 'type'        =>  'hidden',
                       'value'       => set_value('nsel'),
                       'id'          => 'nsel',
                       'name'        => 'nsel',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota1'=> array(
                       'placeholder' => 'Kota',
                       'value'       => set_value('kota1'),
                       'id'          => 'kota1',
                       'name'        => 'kota1',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'npwp'=> array(
                       'placeholder' => 'Npwp',
                       'value'       => set_value('npwp'),
                       'id'          => 'npwp',
                       'name'        => 'npwp',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'nama2'=> array(
                        'placeholder' => 'Nama',
                        'id'          => 'nama2',
                        'name'        => 'nama2',
                        'value'       => set_value('nama2'),
                        'class'       => 'form-control',
                        'style'       => '',
               ),
               'alamat2'=> array(
                       'placeholder' => 'Alamat',
                       'value'       => set_value('alamat2'),
                       'id'          => 'alamat2',
                       'name'        => 'alamat2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kel2'=> array(
                       'placeholder' => 'Kel.',
                       'value'       => set_value('kel2'),
                       'id'          => 'kel2',
                       'name'        => 'kel2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kec2'=> array(
                       'placeholder' => 'Kec.',
                       'value'       => set_value('kec2'),
                       'id'          => 'kec2',
                       'name'        => 'kec2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'ket'=> array(
                       // 'placeholder' => 'Kec.',
                       'value'       => set_value('ket'),
                       'id'          => 'ket',
                       'name'        => 'ket',
                       'class'       => 'form-control',
                       'style'       => '',
                       'type'        => 'hidden',
               ),
               'hohp2'=> array(
                       'placeholder' => 'No. HP',
                       'value'       => set_value('hohp2'),
                       'id'          => 'hohp2',
                       'name'        => 'hohp2',
                       'class'       => 'form-control',
                       'style'       => '',
               ),
               'kota2'=> array(
                       'placeholder' => 'Kota',
                       'value'       => set_value('kota2'),
                       'id'          => 'kota2',
                       'name'        => 'kota2',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdtipe'=> array(
                       'attr'        => array(
                           'id'    => 'kdtipe',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Tipe Unit',
                       'value'       => set_value('kdtipe'),
                       'name'        => 'kdtipe',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'nmtipe'=> array(
                       'placeholder' => 'Nama Tipe',
                       'type'        => 'nmtipe',
                       'value'       => set_value('nmtipe'),
                       'id'          => 'nmtipe',
                       'name'        => 'nmtipe',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'kdwarna'=> array(
                       'attr'        => array(
                           'id'    => 'kdwarna',
                           'class' => 'form-control',
                       ),
                       'data'     => '',
                       'placeholder' => 'Kode Warna',
                       'value'       => set_value('kdwarna'),
                       'name'        => 'kdwarna',
                       'style'       => '',
               ),
               'nmwarna'=> array(
                       'placeholder' => 'Nama Warna',
                       // 'type'        => 'hidden',
                       'value'       => set_value('nmwarna'),
                       'id'          => 'nmwarna',
                       'name'        => 'nmwarna',
                       'class'       => 'form-control',
                       'style'       => '',
                       'readonly'    => '',
               ),
               'tipebayar'=> array(
                       'placeholder' => 'Pembayaran',
                       'attr'        => array(
                           'id'    => 'tipebayar',
                           'class' => 'form-control select',
                       ),
                       'data'     => $this->data['tipebayar'],
                       'value'    => set_value('tipebayar'),
                       'name'     => 'tipebayar',
                       'required'    => '',
               ),
               'kdsales'=> array(
                       'placeholder' => 'Salesman',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdsales'),
                       'id'          => 'kdsales',
                       'name'        => 'kdsales',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
               'kdspv'=> array(
                       'placeholder' => 'Superviser',
                       // 'type'        => 'hidden',
                       'value'       => set_value('kdspv'),
                       'id'          => 'kdspv',
                       'name'        => 'kdspv',
                       'class'       => 'form-control',
                       'style'       => '',
                       // 'readonly'    => '',
               ),
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->slnew_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Set_keterangando_qry
 *
 * @author adi
 */
class Set_keterangando_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function set_nodo() {
      $nodo = $this->input->post('nodo');
      $query = $this->db->query("select * from pzu.vl_do where nodo = '" . $nodo . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    }

    public function ubah_ketdo() {
      $nodo            = $this->input->post('nodo');
      $ket_do          = $this->input->post('ket_do');

        $q = $this->db->query("update pzu.t_do set ket_do = '".$ket_do."' where nodo = '".$nodo."'");

        // echo $this->db->last_query();\
        $res = 'success';

        return json_encode($res);
    }
}

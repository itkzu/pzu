<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptpenjualanharian
 *
 * @author adi
 */
class Rptpenjualanharian extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptpenjualanharian/submit'),
            'add' => site_url('rptpenjualanharian/add'),
            'edit' => site_url('rptpenjualanharian/edit'),
            'reload' => site_url('rptpenjualanharian'),
        );
        $this->load->model('rptpenjualanharian_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function json_dgview() {
        echo $this->rptpenjualanharian_qry->json_dgview();
    }
    
    public function submit() {
        if($this->validate() == TRUE){
            $this->data['rpt'] = $this->rptpenjualanharian_qry->submit();
            $btn = $this->input->post('submit');
            if($btn==="print"){
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('report_html',$this->data);
                //$this->load->view('report_html', $this->data);
            }
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Tanggal Periode',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }
    
    private function validate() {
        $config = array(
            array(
                    'field' => 'periode_awal',
                    'label' => 'Periode Awal',
                    'rules' => 'required|max_length[20]',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

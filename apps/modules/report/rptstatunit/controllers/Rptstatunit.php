<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptstatunit
 *
 * @author adi
 */
class Rptstatunit extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptstatunit/submit'),
            'add' => site_url('rptstatunit/add'),
            'edit' => site_url('rptstatunit/edit'),
            'reload' => site_url('rptstatunit'),
        );
        $this->load->model('rptstatunit_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function getNama() {
        echo $this->rptstatunit_qry->getNama();
    }

    public function getDataBayar() {
        echo $this->rptstatunit_qry->getDataBayar();
    }

    public function getDataBBN() {
        echo $this->rptstatunit_qry->getDataBBN();
    }
    public function getDataUnit() {
        echo $this->rptstatunit_qry->getDataUnit();
    }

    public function json_dgview() {
        echo $this->rptstatunit_qry->json_dgview();
    }

    //data
    public function getDataTFA() {
        echo $this->rptstatunit_qry->getDataTFA();
    }

    public function getDataKPS() {
        echo $this->rptstatunit_qry->getDataKPS();
    }

    public function getDataBBN_aju() {
        echo $this->rptstatunit_qry->getDataBBN_aju();
    }

    public function getDataBBN_trm() {
        echo $this->rptstatunit_qry->getDataBBN_trm();
    }

    public function getDataKBBN() {
        echo $this->rptstatunit_qry->getDataKBBN();
    }

    private function _init_add(){
        if(isset($_POST['kitag']) && strtoupper($_POST['kitag']) == 't'){
            $faktif = TRUE;
        } else{
            $faktif = FALSE;
        }
        $this->data['form'] = array(
            'nama'=> array(
                    'placeholder' => 'Masukkan Kata Kunci <small>(Nama Pemilik / No. Polisi / No. DO / No. BPKB)</small>',
                    'attr'        => array(
                        'id'    => 'nama',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('nama'),
                    'name'     => 'nama',
            ),
            //data utama
            'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'noso'=> array(
                    'placeholder' => 'No. SPK',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nodo'=> array(
                    'placeholder' => 'No. DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tglso'=> array(
                    'placeholder' => 'Tgl SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => set_value('tglso'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgldo'=> array(
                    'placeholder' => 'Tgl DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => set_value('tgldo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //data unit sec 1
            'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => set_value('warna'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tahun'=> array(
                    'placeholder' => 'Tahun',
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'value'       => set_value('tahun'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nofaktur'=> array(
                    'placeholder' => 'No. Faktur',
                    'id'          => 'nofaktur',
                    'name'        => 'nofaktur',
                    'value'       => set_value('nofaktur'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tglfaktur'=> array(
                    'placeholder' => 'Tgl Faktur',
                    'id'          => 'tglfaktur',
                    'name'        => 'tglfaktur',
                    'value'       => set_value('tglfaktur'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nmsup'=> array(
                    'placeholder' => 'Supplier',
                    'id'          => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => set_value('nmsup'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            //Data unit sec 2
            'nmstnk'=> array(
                    'placeholder' => 'Nama STNK',
                    'id'          => 'nmstnk',
                    'name'        => 'nmstnk',
                    'value'       => set_value('nmstnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'alamat_ktp'=> array(
                    'placeholder' => 'Alamat KTP',
                    'id'          => 'alamat_ktp',
                    'name'        => 'alamat_ktp',
                    'value'       => set_value('alamat_ktp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'kel'=> array(
                    'placeholder' => 'Kel.',
                    'id'          => 'kel',
                    'name'        => 'kel',
                    'value'       => set_value('kel'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'kec'=> array(
                    'placeholder' => 'Kec.',
                    'id'          => 'kec',
                    'name'        => 'kec',
                    'value'       => set_value('kec'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'kota'=> array(
                    'placeholder' => 'Kota',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nohp'=> array(
                    'placeholder' => 'No. HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'sls'=> array(
                    'placeholder' => 'Salesman',
                    'id'          => 'sls',
                    'name'        => 'sls',
                    'value'       => set_value('sls'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'spv'=> array(
                    'placeholder' => 'Supervisor',
                    'id'          => 'spv',
                    'name'        => 'spv',
                    'value'       => set_value('spv'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //data unit sec 2 side
            'sotr'=> array(
                    'placeholder' => 'Status OTR',
                    'id'          => 'sotr',
                    'name'        => 'sotr',
                    'value'       => set_value('sotr'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'hotr'=> array(
                    'placeholder' => 'Harga OTR',
                    'id'          => 'hotr',
                    'name'        => 'hotr',
                    'value'       => set_value('hotr'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'um'=> array(
                    'placeholder' => 'Uang Muka',
                    'id'          => 'um',
                    'name'        => 'um',
                    'value'       => set_value('um'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'disc'=> array(
                    'placeholder' => 'Diskon ( - )',
                    'id'          => 'disc',
                    'name'        => 'disc',
                    'value'       => set_value('disc'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'leas'=> array(
                    'placeholder' => 'Subsidi Leasing',
                    'id'          => 'leas',
                    'name'        => 'leas',
                    'value'       => set_value('leas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'umnet'=> array(
                    'placeholder' => 'Uang Muka Net',
                    'id'          => 'umnet',
                    'name'        => 'umnet',
                    'value'       => set_value('umnet'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
      		  'kitag'=> array(
          					'id'          => 'kitag',
                    'value'       => set_value('kitag'),
          					'checked'     => $faktif,
          					'class'       => 'filled-in',
          					'name'		    => 'kitag',
      			),
            'bayar'=> array(
                    'placeholder' => 'Pembayaran',
                    'id'          => 'bayar',
                    'name'        => 'bayar',
                    'value'       => set_value('bayar'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'prog'=> array(
                    'placeholder' => 'Program',
                    'id'          => 'prog',
                    'name'        => 'prog',
                    'value'       => set_value('prog'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'angsur'=> array(
                    'placeholder' => 'Angsuran',
                    'id'          => 'angsur',
                    'name'        => 'angsur',
                    'value'       => set_value('angsur'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tenor'=> array(
                    'placeholder' => 'Tenor',
                    'id'          => 'tenor',
                    'name'        => 'tenor',
                    'value'       => set_value('tenor'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //Data Piutang sec 1
            'viabayar'=> array(
                    'placeholder' => 'Pembayaran Via',
                    'id'          => 'viabayar',
                    'name'        => 'viabayar',
                    'value'       => set_value('viabayar'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'progleas'=> array(
                    'placeholder' => 'Program Leasing',
                    'id'          => 'progleas',
                    'name'        => 'progleas',
                    'value'       => set_value('progleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //Data Piutang sec 2
            'npk'=> array(
                    'placeholder' => 'Nilai Piutang Konsumen',
                    'id'          => 'npk',
                    'name'        => 'npk',
                    'value'       => set_value('npk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nppk'=> array(
                    'placeholder' => 'Nilai Piutang Pokok Leasing',
                    'id'          => 'nppk',
                    'name'        => 'nppk',
                    'value'       => set_value('nppk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'npjpleas'=> array(
                    'placeholder' => 'Nilai Piutang JP Leasing',
                    'id'          => 'npjpleas',
                    'name'        => 'npjpleas',
                    'value'       => set_value('npjpleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'npmleas'=> array(
                    'placeholder' => 'Nilai Piutang Matriks Leasing',
                    'id'          => 'npmleas',
                    'name'        => 'npmleas',
                    'value'       => set_value('npmleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'ppk'=> array(
                    'placeholder' => 'Pembayaran Konsumen',
                    'id'          => 'ppk',
                    'name'        => 'ppk',
                    'value'       => set_value('ppk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'pppk'=> array(
                    'placeholder' => 'Pembayaran Pokok Leasing',
                    'id'          => 'pppk',
                    'name'        => 'pppk',
                    'value'       => set_value('pppk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'ppjpleas'=> array(
                    'placeholder' => 'Pembayaran JP Leasing',
                    'id'          => 'ppjpleas',
                    'name'        => 'ppjpleas',
                    'value'       => set_value('ppjpleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'ppmleas'=> array(
                    'placeholder' => 'Pembayaran Matriks Leasing',
                    'id'          => 'ppmleas',
                    'name'        => 'ppmleas',
                    'value'       => set_value('ppmleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'spk'=> array(
                    'placeholder' => ' Saldo Konsumen',
                    'id'          => 'spk',
                    'name'        => 'spk',
                    'value'       => set_value('spk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'sppk'=> array(
                    'placeholder' => ' Saldo Pokok Leasing',
                    'id'          => 'sppk',
                    'name'        => 'sppk',
                    'value'       => set_value('sppk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'spjpleas'=> array(
                    'placeholder' => ' Saldo JP Leasing',
                    'id'          => 'spjpleas',
                    'name'        => 'spjpleas',
                    'value'       => set_value('spjpleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'spmleas'=> array(
                    'placeholder' => ' Saldo Matriks Leasing',
                    'id'          => 'spmleas',
                    'name'        => 'spmleas',
                    'value'       => set_value('spmleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //data bbn sec 1
            'nopol'=> array(
                    'placeholder' => 'No. Polisi',
                    'id'          => 'nopol',
                    'name'        => 'nopol',
                    'value'       => set_value('nopol'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nobpkb'=> array(
                    'placeholder' => 'No. BPKB',
                    'id'          => 'nobpkb',
                    'name'        => 'nobpkb',
                    'value'       => set_value('nobpkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //data bbn sec 1
            'tgl_aju_fa'=> array(
                    'placeholder' => 'Tgl Pengajuan Faktur Astra',
                    'id'          => 'tgl_aju_fa',
                    'name'        => 'tgl_aju_fa',
                    'value'       => set_value('tgl_aju_fa'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trb_fa'=> array(
                    'placeholder' => 'Tgl Terbit Faktur Astra',
                    'id'          => 'tgl_trb_fa',
                    'name'        => 'tgl_trb_fa',
                    'value'       => set_value('tgl_trb_fa'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_fa'=> array(
                    'placeholder' => 'Tgl Terima Faktur Astra',
                    'id'          => 'tgl_trm_fa',
                    'name'        => 'tgl_trm_fa',
                    'value'       => set_value('tgl_trm_fa'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_kps'=> array(
                    'placeholder' => 'Tgl Kwitansi Pengajuan STNK',
                    'id'          => 'tgl_kps',
                    'name'        => 'tgl_kps',
                    'value'       => set_value('tgl_kps'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_aju_bbn'=> array(
                    'placeholder' => 'Tgl Pengajuan BBN',
                    'id'          => 'tgl_aju_bbn',
                    'name'        => 'tgl_aju_bbn',
                    'value'       => set_value('tgl_aju_bbn'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_bbn'=> array(
                    'placeholder' => 'Tgl Terima Tagihan BBN',
                    'id'          => 'tgl_trm_bbn',
                    'name'        => 'tgl_trm_bbn',
                    'value'       => set_value('tgl_trm_bbn'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'notrans_trm_fa'=> array(
                    'placeholder' => 'No. Transaksi Terima Faktur Astra',
                    'id'          => 'notrans_trm_fa',
                    'name'        => 'notrans_trm_fa',
                    'value'       => set_value('notrans_trm_fa'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'notrans_aju_stnk'=> array(
                    'placeholder' => 'No. Transaksi Pengajuan STNK',
                    'id'          => 'notrans_aju_stnk',
                    'name'        => 'notrans_aju_stnk',
                    'value'       => set_value('notrans_aju_stnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'notrans_aju_bbn'=> array(
                    'placeholder' => 'No. Transaksi Pengajuan BBN',
                    'id'          => 'notrans_aju_bbn',
                    'name'        => 'notrans_aju_bbn',
                    'value'       => set_value('notrans_aju_bbn'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'notrans_trm_stnk'=> array(
                    'placeholder' => 'No. Transaksi Terima Tagihan STNK',
                    'id'          => 'notrans_trm_stnk',
                    'name'        => 'notrans_trm_stnk',
                    'value'       => set_value('notrans_trm_stnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //Data BBN sec 2 side
            'tgl_trm_notis'=> array(
                    'placeholder' => 'Tgl Terima Notis',
                    'id'          => 'tgl_trm_notis',
                    'name'        => 'tgl_trm_notis',
                    'value'       => set_value('tgl_trm_notis'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_notis'=> array(
                    'placeholder' => 'Tgl Serah Notis',
                    'id'          => 'tgl_srh_notis',
                    'name'        => 'tgl_srh_notis',
                    'value'       => set_value('tgl_srh_notis'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_stnk'=> array(
                    'placeholder' => 'Tgl Terima STNK',
                    'id'          => 'tgl_trm_stnk',
                    'name'        => 'tgl_trm_stnk',
                    'value'       => set_value('tgl_trm_stnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_stnk'=> array(
                    'placeholder' => 'Tgl Serah STNK',
                    'id'          => 'tgl_srh_stnk',
                    'name'        => 'tgl_srh_stnk',
                    'value'       => set_value('tgl_srh_stnk'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_tnkb'=> array(
                    'placeholder' => 'Tgl Terima TNKB',
                    'id'          => 'tgl_trm_tnkb',
                    'name'        => 'tgl_trm_tnkb',
                    'value'       => set_value('tgl_trm_tnkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_tnkb'=> array(
                    'placeholder' => 'Tgl Serah TNKB',
                    'id'          => 'tgl_srh_tnkb',
                    'name'        => 'tgl_srh_tnkb',
                    'value'       => set_value('tgl_srh_tnkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_trm_bpkb'=> array(
                    'placeholder' => 'Tgl Terima BPKB',
                    'id'          => 'tgl_trm_bpkb',
                    'name'        => 'tgl_trm_bpkb',
                    'value'       => set_value('tgl_trm_bpkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgl_srh_bpkb'=> array(
                    'placeholder' => 'Tgl Serah BPKB',
                    'id'          => 'tgl_srh_bpkb',
                    'name'        => 'tgl_srh_bpkb',
                    'value'       => set_value('tgl_srh_bpkb'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),

            //last Data BBN
            'no_srh_bpkb_leas'=> array(
                    'placeholder' => 'No. Penyerahan BPKB ke Leasing',
                    'id'          => 'no_srh_bpkb_leas',
                    'name'        => 'no_srh_bpkb_leas',
                    'value'       => set_value('no_srh_bpkb_leas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
        );
    }
}

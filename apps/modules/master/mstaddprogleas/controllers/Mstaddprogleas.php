<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstaddprogleas
 *
 * @author adi
 */
class Mstaddprogleas extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstaddprogleas/submit'),
            'add' => site_url('mstaddprogleas/add'),
            'edit' => site_url('mstaddprogleas/edit'),
            'reload' => site_url('mstaddprogleas'),
        );
        $this->load->model('mstaddprogleas_qry');
        $kategori = $this->mstaddprogleas_qry->getLeasing();
        foreach ($kategori as $value) {
            $this->data['kdleasing'][$value['kdleasing']] = $value['kdleasing'];
        }
        $this->data['status_ppn'] = array( 
            "I" => "INCLUDE",
            "N" => "NON-PPN"
          );

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstaddprogleas_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->mstaddprogleas_qry->getKategori();
    }*/

    public function submit() {
        echo $this->mstaddprogleas_qry->submit();
    }

    public function update() {
        echo $this->mstaddprogleas_qry->update();
    }

    public function delete() {
        echo $this->mstaddprogleas_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        if(isset($_POST['tunai_leas']) && strtoupper($_POST['tunai_leas']) == 't'){
          $tunai_leas = TRUE;
        } else{
          $tunai_leas = FALSE;
        }

        $this->data['form'] = array(
           'kdprogleas'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Program Leasing',
                    'id'          => 'kdprogleas',
                    'name'        => 'kdprogleas',
                    'value'       => set_value('kdprogleas'),
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'kdleasing'=> array( 
                    'attr'        => array(
                        'id'    => 'kdleasing',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdleasing'],
                    'value'    => set_value('kdleasing'),
                    'name'     => 'kdleasing',
                    'placeholder' => 'Leasing',
                    'required' => ''
            ),
           'nmprogleas'=> array(
                    'placeholder' => 'Nama Program',
                    'id'      => 'nmprogleas',
                    'name'        => 'nmprogleas',
                    'value'       => set_value('nmprogleas'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'required' => ''
            ),
            'tunai_leas'=> array(
                    'placeholder' => '',
                    'id'          => 'tunai_leas',
                    'value'       => 't',
                    'checked'     => $tunai_leas,
                    'class'       => 'custom-control-input',
                    'name'        => 'tunai_leas',
                    'type'        => 'checkbox',
            ),
           'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           // nominal
           'jp'=> array(
                    'placeholder' => 'Refund di Depan (JP)',
                    'id'          => 'jp',
                    'name'        => 'jp',
                    'value'       => set_value('jp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'math();'
            ),
           'matriks'=> array(
                    'placeholder' => 'Refund di Belakang (Matriks)',
                    'id'          => 'matriks',
                    'name'        => 'matriks',
                    'value'       => set_value('matriks'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'math();'
            ),
           'tot_refund'=> array(
                    'placeholder' => 'Total Refund',
                    'id'          => 'tot_refund',
                    'name'        => 'tot_refund',
                    'value'       => set_value('tot_refund'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
                    'required' => ''
            ),
            'status_ppn'=> array(
                    'attr'        => array(
                        'id'    => 'status_ppn',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['status_ppn'],
                    'value'    => set_value('status_ppn'),
                    'name'     => 'status_ppn',
                    'placeholder' => 'Status PPN',
                    'required' => ''
            ),
           'dpp'=> array(
                    'placeholder' => 'DPP Refund',
                    'id'      => 'dpp',
                    'name'        => 'dpp',
                    'value'       => set_value('dpp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'ppn'=> array(
                    'placeholder' => 'PPN Refund',
                    'id'      => 'ppn',
                    'name'        => 'ppn',
                    'value'       => set_value('ppn'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
      		'faktif'=> array(
                    'placeholder' => '',
      				'id'          => 'faktif',
      				'value'       => 't',
      				'checked'     => $faktif,
      				'class'       => 'custom-control-input',
      				'name'		  => 'faktif',
      				'type'		  => 'checkbox',
      		),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks);

        if($this->val[0]['faktif'] == 't'){
                    $faktifx = true;
                } else {
                    $faktifx = false;
        }

        if($this->val[0]['tunai_leas'] == 't'){
                    $tunai_leas = true;
                } else {
                    $tunai_leas = false;
        }

        $this->data['form'] = array(
           'kdprogleas'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Program Leasing',
                    'id'          => 'kdprogleas',
                    'name'        => 'kdprogleas',
                    'value'       => $this->val[0]['kdprogleas'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'kdleasing'=> array( 
                    'attr'        => array(
                        'id'    => 'kdleasing',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdleasing'],
                    'value'       => $this->val[0]['kdleasing'], 
                    'name'     => 'kdleasing',
                    'placeholder' => 'Leasing',
                    'required' => ''
            ),
           'nmprogleas'=> array(
                    'placeholder' => 'Nama Program',
                    'id'      => 'nmprogleas',
                    'name'        => 'nmprogleas',
                    'value'       => $this->val[0]['nmprogleas'],  
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'required' => ''
            ),
            'tunai_leas'=> array(
                    'placeholder' => '',
                    'id'          => 'tunai_leas',
                    'value'       => 't',
                    'checked'     => $tunai_leas,
                    'class'       => 'custom-control-input',
                    'name'        => 'tunai_leas',
                    'type'        => 'checkbox',
            ),
           'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => $this->val[0]['ket'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
           // nominal
           'jp'=> array(
                    'placeholder' => 'Refund di Depan (JP)',
                    'id'          => 'jp',
                    'name'        => 'jp',
                    'value'       => $this->val[0]['jp'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'math();'
            ),
           'matriks'=> array(
                    'placeholder' => 'Refund di Belakang (Matriks)',
                    'id'          => 'matriks',
                    'name'        => 'matriks',
                    'value'       => $this->val[0]['matriks'], 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'onkeyup'     => 'math();'
            ),
           'tot_refund'=> array(
                    'placeholder' => 'Total Refund',
                    'id'          => 'tot_refund',
                    'name'        => 'tot_refund',
                    'value'       => set_value('tot_refund'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
                    'required' => ''
            ),
            'status_ppn'=> array(
                    'attr'        => array(
                        'id'    => 'status_ppn',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['status_ppn'],
                    'value'       => $this->val[0]['status_ppn'], 
                    'name'     => 'status_ppn',
                    'placeholder' => 'Status PPN',
                    'required' => ''
            ),
           'dpp'=> array(
                    'placeholder' => 'DPP Refund',
                    'id'      => 'dpp',
                    'name'        => 'dpp',
                    'value'       => set_value('dpp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
           'ppn'=> array(
                    'placeholder' => 'PPN Refund',
                    'id'      => 'ppn',
                    'name'        => 'ppn',
                    'value'       => set_value('ppn'), 
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'faktif'=> array(
                    'placeholder' => '',
                    'id'          => 'faktif',
                    'value'       => 't',
                    'checked'     => $faktifx,
                    'class'       => 'custom-control-input',
                    'name'        => 'faktif',
                    'type'        => 'checkbox',
            ), 
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstaddprogleas_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'Kode Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmaks',
                    'label' => 'Nama Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?> -->
        <div class="box box-danger">
          <div class="box-header box-view">
            <button type="button" class="btn btn-primary btn-add">Tambah</button>
            <button type="button" class="btn btn-warning btn-edit">Ubah</button>
            <button type="button" class="btn btn-danger btn-del">Hapus</button>
            <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
          </div>
          <div class="box-header box-new">
            <button type="button" class="btn btn-primary btn-save">Simpan</button>
            <button type="button" class="btn btn-danger btn-cancel">Batal</button>
          </div>

          <div class="col-xs-12">
              <div style="border-top: 1px solid #ddd; height: 10px;"></div>
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['tglso']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['tglso']);
                                    echo form_error('tglso','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['noso']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['noso']);
                                    echo form_error('noso','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['nospk']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                      echo form_dropdown($form['nospk']['name'],$form['nospk']['data'] ,$form['nospk']['value'] ,$form['nospk']['attr']);
                                      echo form_error('nospk','<div class="note">','</div>');
                                    // echo form_label($form['nospk']['placeholder']);
                                    // echo form_input($form['nospk']);
                                    // echo form_error('nospk','<div class="note">','</div>');
                                ?>
                             </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <div class="checkbox">
                                  <label>
                                    <?php
                                        echo form_checkbox($form['finden']);
                        								echo "<b> Status Inden </b>";
                                    ?>
                                  </label>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row">
                      <div class="col-xs-6">
                          <label> Identitas Pemesan </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                      <div class="col-xs-6">
                          <label> Identitas STNK / BPKB </label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['nama1']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-5">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nama1']);
                                      echo form_error('nama1','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['nama2']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-5">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nama2']);
                                      echo form_error('nama2','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['alamat1']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-5">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['alamat1']);
                                      echo form_error('alamat1','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['alamat2']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-5">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['alamat2']);
                                      echo form_error('alamat2','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">
                          
                          <div class="col-xs-4"> 
                          </div>
                          
                          <div class="col-xs-2"> 
                            <button type="button" style="text-align: right" class="btn btn-secondary btn-copy">Copy Data =></button>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['alamat3']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-5">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['alamat3']);
                                      echo form_error('alamat3','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['kel1']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['kel1']);
                                      echo form_error('kel1','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['nohp1']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nohp1']);
                                      echo form_error('nohp1','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['kel2']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['kel2']);
                                      echo form_error('kel2','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>

                          <div class="col-xs-1">
                            <label>
                              <?php echo form_label($form['nohp2']['placeholder']); ?>
                            </label>
                          </div>

                          <div class="col-xs-2">
                              <div class="form-group">
                                  <?php
                                      echo form_input($form['nohp2']);
                                      echo form_error('nohp2','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['kec1']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['kec1']);
                                  echo form_error('kec1','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['notelp']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                                <?php
                                    echo form_input($form['notelp']);
                                    echo form_error('notelp','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['kec2']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['kec2']);
                                  echo form_error('kec2','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['noktp']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  // echo form_label($form['ket']['placeholder']);
                                  echo form_input($form['noktp']);
                                  echo form_error('noktp','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['kota1']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['kota1']['name'],$form['kota1']['data'] ,$form['kota1']['value'] ,$form['kota1']['attr']);
                                    echo form_error('kota1','<div class="note">','</div>');
                                    // echo form_input($form['kota1']);
                                    // echo form_error('kota1','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>  

                        <div class="col-xs-3">
                            <div class="form-group">
                              <?php
                                  // echo form_label($form['ket']['placeholder']);
                                  echo form_input($form['ket']);
                                  echo form_error('ket','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['kota2']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['kota2']['name'],$form['kota2']['data'] ,$form['kota2']['value'] ,$form['kota2']['attr']);
                                    echo form_error('kota2','<div class="note">','</div>');
                                    // echo form_input($form['kota2']);
                                    // echo form_error('kota2','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['npwp']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['npwp']);
                                  echo form_error('npwp','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-xs-12">
                      <div style="border-top: 1px solid #ddd; height: 5px;"></div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['kdtipe']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['kdtipe']['name'],$form['kdtipe']['data'] ,$form['kdtipe']['value'] ,$form['kdtipe']['attr']);
                                    echo form_error('kdtipe','<div class="note">','</div>');
                                  // echo form_label($form['kdtipe']['placeholder']);
                                  // echo form_input($form['kdtipe']);
                                  // echo form_error('kdtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['nmtipe']);
                                  echo form_error('nmtipe','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['kdwarna']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-2">
                            <div class="form-group">
                              <?php
                                    echo form_dropdown($form['kdwarna']['name'],$form['kdwarna']['data'] ,$form['kdwarna']['value'] ,$form['kdwarna']['attr']);
                                    echo form_error('kdwarna','<div class="note">','</div>');
                                  // echo form_label($form['kdwarna']['placeholder']);
                                  // echo form_input($form['kdwarna']);
                                  // echo form_error('kdwarna','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['nmwarna']);
                                  echo form_error('nmwarna','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['tipebayar']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-5">
                            <div class="form-group">
                              <?php
                                  echo form_dropdown( $form['tipebayar']['name'],
                                                      $form['tipebayar']['data'] ,
                                                      $form['tipebayar']['value'] ,
                                                      $form['tipebayar']['attr']);
                                  echo form_error('tipebayar','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label('Salesman'); ?>
                          </label>
                        </div>

                        <div class="col-xs-5">
                            <div class="form-group">
                              <?php
                                  echo form_dropdown($form['kdsales']['name'],$form['kdsales']['data'] ,$form['kdsales']['value'] ,$form['kdsales']['attr']);
                                  echo form_error('kdsales','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>
                        <!--
                        <div class="col-xs-3">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kdsales']['placeholder']);
                                    echo form_input($form['kdsales']);
                                    echo form_error('kdsales','<div class="note">','</div>');
                                ?>
                             </div>
                        </div>-->

                      </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                      <div class="row">

                        <div class="col-xs-1">
                          <label>
                            <?php echo form_label($form['nmspv']['placeholder']); ?>
                          </label>
                        </div>

                        <div class="col-xs-5">
                            <div class="form-group">
                              <?php
                                  echo form_input($form['nmspv']);
                                  echo form_error('nmspv','<div class="note">','</div>');
                              ?>
                            </div>
                        </div>

                      </div>
                  </div>


              </div>
          </div>
          <!-- /.box-body -->
          <!-- <?php echo form_close(); ?> -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 
        $("#noktp").mask("99.99.99.999999.9999",{"placeholder":"00.00.00.000000.0000"});
        $("#ket").val('0');
        $("#noso").val('');
        $("#finden").attr('disabled',true);
        $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        // $("#npwp").mask("s99-999999");
        clear();
        disabled();
        getKodeSales();
        getTipeUnit();
        getKdWarna();
        getKdKota();
        GetNoID();
        // getNoSPK();
        init_nama();

        $('#nospk').change(function () {
            set_apispk();
        });

        $('#kdsales').change(function () {
            getKodeSalesHeader();
        });

        $('#kdtipe').change(function () {
            getKodeTipeHeader();
        });

        $('#kdwarna').change(function () {
            getKdWarnaHeader();
        });

        $('.btn-batal').click(function () {
            clear();
            $("#finden").attr('disabled',true);
            disabled();
        });

        $("#tglso").change(function (){
          GetNoID_ADD();
        });

        $('.btn-copy').click(function () {
          $('#nama2').val($('#nama1').val());
          $('#alamat2').val($('#alamat1').val());
          $('#alamat3').val($('#alamat1').val());
          $('#kel2').val($('#kel1').val());
          $('#kec2').val($('#kec1').val());
          $('#nohp2').val($('#nohp1').val());
          $('#kota2').val($('#kota1').val());
          $("#kota2").trigger("chosen:updated");
          $('#kota2').select2({
              dropdownAutoWidth : true,
              width: '100%'
          });
        });

        $('.btn-add').click(function () {
            $("#noso").val('');
            $("#ket").val('1');
            GetNoID_ADD();
            getKodeSales();
            getTipeUnit();
            getKdWarna();
            enabled();
            clear();
            // getNoSPK();
            init_nama();
            var tg = new Date();
            var tgl = tg.toString();
            var tanggal = tgl.substring(13,15);
            $("#noso").mask("s"+tanggal+"-999999");
            $("#npwp").mask('99.999.999.9-999.999',{"placeholder" : '00.000.000.0-000.000'}); 
            $("#npwp").val('00.000.000.0-000.000'); 
        });

        $('.btn-edit').click(function () {
          $("#ket").val('2');
            enabled();
            $("#tglso").attr('disabled',true);
            $("#noso").attr('disabled',true);
            $("#kdtipe").attr('disabled',false);
            $("#kdwarna").attr('disabled',false);
        });

        $('#noso').change(function () {
          if($('#ket').val()==='0'){
            set_spk();
            getNoSPK();
          } else {
          carispk();
          }
            // set_spk();
        });

        $('.btn-save').click(function () {
          if($('#ket').val()==='1'){
            var noso = $('#noso').val();
            var kota1 = $('#kota1').val();
            var kota2 = $('#kota2').val();
            var noktp = $('#noktp').val();
            var noktp = noktp.replace(".","");
            var noktp = noktp.replace(".","");
            var noktp = noktp.replace(".","");
            var noktp = noktp.replace(".","");
            var n = noktp.length;
            
            if(n===16){
                $('#noktp').blur();
                $('#noktp').css("border", "1px solid gainsboro");
                if(noso===''){
                    swal({
                        title: "Data tidak boleh kosong",
                        text: "",
                        type: "error"
                    }, function(){
                        $('#noso').focus();
                        $('#noso').css("border", "2px solid red");
                    }); 
                } else {
                    $('#noso').blur();
                    $('#noso').css("border", "1px solid gainsboro");
                    if(kota1===''){
                        swal({
                            title: "Data Kota Tidak Boleh kosong",
                            text: "",
                            type: "error"
                        }, function(){
                            $('#kota1').focus();
                            $('#kota1').css("border", "2px solid red");
                        }); 
                    } else {
                        $('#kota1').blur();
                        $('#kota1').css("border", "1px solid gainsboro");
                        if(kota2===''){
                          swal({
                              title: "Data Kota Tidak Boleh kosong",
                              text: "",
                              type: "error"
                          }, function(){
                              $('#kota2').focus();
                              $('#kota2').css("border", "2px solid red");
                          }); 
                      } else {
                          $('#kota2').blur();
                          $('#kota2').css("border", "1px solid gainsboro");
                          addspk(); 
                      } 
                    } 
                }  
            } else {
                swal({
                    title: "Data tidak boleh kosong",
                    text: "",
                    type: "error"
                }, function(){
                    $('#noktp').focus();
                    $('#noktp').css("border", "2px solid red");
                }); 
            }  
          } else {
            updatespk();
          };
        });

        $('.btn-del').click(function () {
          deletespk();
        });

        $('.btn-cancel').click(function () {
          swal({
              title: "Input Transaki SPK Inden akan dibatalkan!",
              text: "Data tidak akan Tersimpan!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#c9302c",
              confirmButtonText: "Ya, Lanjutkan!",
              cancelButtonText: "Batalkan!",
              closeOnConfirm: false
          }, function () {
            window.location.reload();
          });
        });
    });

    // no id
    function GetNoID(){
      var tg = new Date();
      var tgl = tg.toString();
      var tanggal = tgl.substring(13,15);
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#noso').mask(value.kode+"99-999999"); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }
    // no id
    function GetNoID_ADD(){
      var tg = $("#tglso").val();
      var tgl = tg.toString();
      var tanggal = tgl.substring(8,10);  
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getNoID");?>",
            data: {"tanggal":tanggal},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){ 
                  $('#noso').mask(value.kode+tanggal+"-999999"); 
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeSales(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getKodeSales");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdsales').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Salesman --'));
                $("#kdsales").trigger("change.chosen");
                if ($('#kdsales').hasClass("chosen-hidden-accessible")) {
                    $('#kdsales').select2('destroy');
                    $("#kdsales").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdsales')
                      .append($('<option>', { value : value.kdsales })
                      .html("<b style='font-size: 14px;'>" + value.nmsales + " </b>"));
                });

                $('#kdsales').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getTipeUnit(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getTipeUnit");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdtipe').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Tipe Unit --'));
                $("#kdtipe").trigger("change.chosen");
                if ($('#kdtipe').hasClass("chosen-hidden-accessible")) {
                    $('#kdtipe').select2('destroy');
                    $("#kdtipe").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdtipe')
                      .append($('<option>', { value : value.kode })
                      .html("<b style='font-size: 14px;'>" + value.kode + " - " + value.nmtipe + " </b>"));
                });

                $('#kdtipe').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdWarna(){
        var noso = $("#noso").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getKdWarna");?>",
            data: {"noso":noso},
            beforeSend: function() {
                $('#kdwarna').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kode Warna --'));
                $("#kdwarna").trigger("change.chosen");
                if ($('#kdwarna').hasClass("chosen-hidden-accessible")) {
                    $('#kdwarna').select2('destroy');
                    $("#kdwarna").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kdwarna')
                      .append($('<option>', { value : value.kdwarna })
                      .html("<b style='font-size: 14px;'>" + value.kdwarna + " ( " + value.nmwarna + " ) </b>"));
                });

                $('#kdwarna').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getNoSPK(){
        var so = $("#noso").val();
        var noso = so.toUpperCase();
        var nospk = $("#nospk").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getNoSPK");?>",
            data: {"nospk":nospk,"noso":noso},
            beforeSend: function() {
                $('#nospk').html("")
                            .append($('<option>', { value :  ''  }));
                            // .text('-- Pilih SPK --'));
                $("#nospk").trigger("change.chosen");
                if ($('#nospk').hasClass("chosen-hidden-accessible")) {
                    $('#nospk').select2('destroy');
                    $("#nospk").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#nospk')
                      .append($('<option>', { value : value.idspk })
                      .html("<b style='font-size: 14px;'>" + value.idspk + " </b>"));
                });

                $('#nospk').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function setBgColour(val,object){
    	if(val){
    		$("#"+object).css("background-color", "#fff");
    	}else{
    		$("#"+object).css("background-color", "#eee");
    	}
    }

    function init_nama(){
    	$("#nospk").select2({
    		ajax: {
    			url: "<?=site_url('spknew/getNama');?>",
    			type: 'post',
    			dataType: 'json',
    			delay: 250,
    			data: function (params) {
    				return {
    					q: params.term,
    					page: params.page
    				};
    			},
          processResults: function (data, params) {
    				params.page = params.page || 1;

    				return {
    					results: data.items,
    					pagination: {
    						more: (params.page * 30) < data.total_count
    					}
    				};
    			},
    			cache: true
    		},
    		placeholder: 'Masukkan No SPK / Nama Customer ...',
    		dropdownAutoWidth : false,
    					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    					minimumInputLength: 3,
    					templateResult: format_nama, // omitted for brevity, see the source of this page
    					templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
    				});
    }

    function format_nama_terpilih (repo) {
    	return repo.full_name || repo.id;
    }

    function format_nama (repo) {
    	if (repo.loading) return "Mencari data ... ";


    	var markup = "<div class='select2-result-repository clearfix'>" +
    	"<div class='select2-result-repository__meta'>" +
    	"<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

    	markup += "<div class='select2-result-repository__statistics'>" +
    	"<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.id +
    	"</div>" +
    	"</div>" +
    	"</div>";
    	return markup;
    }


    function getKodeSalesHeader(){
      var kdsales = $('#kdsales').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getKodeSalesHeader");?>",
            data: {"kdsales":kdsales },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmspv").val(data.nmsales_header);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKodeTipeHeader(){
      var kdtipe = $('#kdtipe').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getKodeTipeHeader");?>",
            data: {"kdtipe":kdtipe },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmtipe").val(data.nmtipe);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function getKdWarnaHeader(){
      var kdwarna = $('#kdwarna').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getKdWarnaHeader");?>",
            data: {"kdwarna":kdwarna },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                $("#nmwarna").val(data.nmwarna);
                // alert(data.finden);
              });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function disabled(){
      $(".box-new").hide();
      $(".box-view").show();
      $("#noso").attr('disabled',false);
      $("#tglso").attr('disabled',true);
      $("#nospk").attr('disabled',true);
      $("#finden").attr('disabled',true);
      $("#nama1").attr('disabled',true);
      $("#nama2").attr('disabled',true);
      $("#alamat1").attr('disabled',true);
      $("#alamat2").attr('disabled',true);
      $("#alamat3").attr('disabled',true);
      $("#kel1").attr('disabled',true);
      $("#nohp1").attr('disabled',true);
      $("#kel2").attr('disabled',true);
      $("#nohp2").attr('disabled',true);
      $("#kec1").attr('disabled',true);
      $("#kota1").attr('disabled',true);
      $("#kota2").attr('disabled',true);
      $("#npwp").attr('disabled',false);
      $("#notelp").attr('disabled',true);
      $("#kec2").attr('disabled',true);
      $("#noktp").attr('disabled',true);
      $("#npwp").attr('disabled',true);
      $("#kdtipe").attr('disabled',true);
      $("#nmtipe").attr('disabled',true);
      $("#kdwarna").attr('disabled',true);
      $("#nmwarna").attr('disabled',true);
      $("#kdsales").attr('disabled',true);
      $("#nmsales").attr('disabled',true);
      $("#nmspv").attr('disabled',true);
      $("#tipebayar").attr('disabled',true);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").hide();
    }

    function enabled(){
      $(".box-view").hide();
      $(".box-new").show();
      $("#tglso").attr('disabled',false);
      $("#noso").attr('disabled',false);
      $("#nospk").attr('disabled',false);
      $("#finden").attr('disabled',false);
      $("#nama1").attr('disabled',false);
      $("#nama2").attr('disabled',false);
      $("#alamat1").attr('disabled',false);
      $("#alamat2").attr('disabled',false);
      $("#alamat3").attr('disabled',false);
      $("#kel1").attr('disabled',false);
      $("#nohp1").attr('disabled',false);
      $("#kel2").attr('disabled',false);
      $("#nohp2").attr('disabled',false);
      $("#kec1").attr('disabled',false);
      $("#kota1").attr('disabled',false);
      $("#kota2").attr('disabled',false);
      $("#notelp").attr('disabled',false);
      $("#noktp").attr('disabled',false);
      $("#npwp").attr('disabled',false);
      $("#kec2").attr('disabled',false);
      $("#kdtipe").attr('disabled',false);
      $("#nmtipe").attr('disabled',false);
      $("#kdwarna").attr('disabled',false);
      $("#nmwarna").attr('disabled',false);
      $("#kdsales").attr('disabled',false);
      $("#nmsales").attr('disabled',false);
      $("#nmspv").attr('disabled',true);
      $("#tipebayar").attr('disabled',false);
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
      // $(".btn-batal").show();
    }

    function getKdKota(){
        var nospk = $("#nospk").val();
        //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/getKdKota");?>",
            data: {"nospk":nospk},
            beforeSend: function() {
                $('#kota1').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kota --'));
                $("#kota1").trigger("change.chosen");
                if ($('#kota1').hasClass("chosen-hidden-accessible")) {
                    $('#kota1').select2('destroy');
                    $("#kota1").chosen({ width: '100%' });
                }
                $('#kota2').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Kota --'));
                $("#kota2").trigger("change.chosen");
                if ($('#kota2').hasClass("chosen-hidden-accessible")) {
                    $('#kota2').select2('destroy');
                    $("#kota2").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                  $('#kota1')
                      .append($('<option>', { value : value.kota })
                      .html("<b style='font-size: 12px;'>" + value.kota + " </b>"));
                  
                  $('#kota2')
                      .append($('<option>', { value : value.kota })
                      .html("<b style='font-size: 12px;'>" + value.kota + " </b>"));
                });

                $('#kota1').select2({
                    dropdownAutoWidth : true,
                    width: '100%' 
                });

                $('#kota2').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                  });

                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function clear(){
      $("#nospk").val('');
      $("#nama1").val('');
      $("#nama2").val('');
      $("#alamat1").val('');
      $("#alamat2").val('');
      $("#alamat3").val('');
      $("#kel1").val('');
      $("#nohp1").val('');
      $("#kel2").val('');
      $("#nohp2").val('');
      $("#kec1").val('');
      $("#kota1").val('');
      $("#kota2").val('');
      $("#notelp").val('');
      $("#kec2").val('');
      $("#kdtipe").val('');
      $("#npwp").val('');
      $("#noktp").val('');
      $("#nmtipe").val('');
      $("#kdwarna").val('');
      $("#nmwarna").val('');
      $("#kdsales").val('');
      $("#nmsales").val('');
      $("#nmspv").val('');
      $("#tipebayar").val('');
      $(".btn-edit").prop('disabled',true);
      $(".btn-del").prop('disabled',true);
    }

    function set_spk(){

      var so = $("#noso").val();
      var test = '"empty"';
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/set_spk");?>",
            data: {"noso":noso },
            success: function(resp){

              // alert(resp);
              // alert(test);
              if(resp==='"empty"'){ 
                  clear();
                  $("#finden").attr('disabled',true);
                  // swal({
                  //     title: "Proses Gagal",
                  //     text: "No SPK Tidak ditemukan",
                  //     type: "error"
                  // }, function(){
                  //     clear();
                  //     $('#noso').val('');
                  // });
                  $(".btn-add").attr('disabled',false);
                  $(".btn-edit").attr('disabled',true);
                  $(".btn-del").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglso)));
                      $("#nama1").val(data.nama);
                      $("#nama2").val(data.nama_s);
                      $("#alamat1").val(data.alamat);
                      $("#alamat2").val(data.alamat_s);
                      $("#alamat3").val(data.alamat_s);
                      $("#kel1").val(data.kel);
                      $("#nohp1").val(data.nohp);
                      $("#kel2").val(data.kel_s);
                      $("#kel2").val(data.kel_s);
                      $("#nohp2").val(data.nohp_s);
                      $("#kec1").val(data.kec);
                      $("#notelp").val(data.notelp);
                      $("#kec2").val(data.kec_s);
                      $("#kota1").val(data.kota);
                      $("#kota1").trigger("chosen:updated");
                      $('#kota1').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#kota2").val(data.kota_s);
                      $("#kota2").trigger("chosen:updated");
                      $('#kota2').select2({
                                   dropdownAutoWidth : true,
                                   width: '100%'
                      });
                      $("#npwp").val(data.npwp);
                      $("#noktp").val(data.noktp); 
                      $("#kdtipe").val(data.kdtipe);
                      $("#kdtipe").trigger("chosen:updated");
                      $('#kdtipe').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#nmtipe").val(data.nmtipe);
                      $("#kdwarna").val(data.kdwarna);
                      $("#kdwarna").trigger("chosen:updated");
                      $('#kdwarna').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#nmwarna").val(data.nmwarna);
                      // alert(data.kdleasing);
                      if(data.kdleasing==='TUNAI'){
                        $("#tipebayar").val('T');
                        $("#tipebayar").trigger("change");
                      } else {
                        $("#tipebayar").val('K');
                        $("#tipebayar").trigger("change");
                      }
                      $("#kdsales").val(data.kdsales);
                      $("#kdsales").trigger("chosen:updated");
                      $('#kdsales').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#nmspv").val(data.nmsales_header);
                      // alert(data.finden);
                      if (data.finden === 't' ) {
                        $("#finden").prop("checked",true);
                      } else {
                          $("#finden").prop("checked",false);
                      }
                      $("#nospk").val(data.nosohso);
                       // alert(data.nosohso);
                      $("#nospk").trigger("chosen:updated");
                      $('#nospk').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $(".btn-add").attr('disabled',false);
                      $(".btn-edit").attr('disabled',false);
                      $(".btn-del").attr('disabled',false);
                      $(".btn-batal").show();
                  });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function carispk(){

      var so = $("#noso").val();
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/carispk");?>",
            data: {"noso":noso },
            success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){
                    // if(data.noso=$('#noso').val()){
                        swal({
                            title: "Proses Gagal",
                            text: "No SPK Sudah Ada",
                            type: "error"
                        }, function(){  
                            $('#noso').val('');
                        });
                    // }
                });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function set_apispk(){
      var spk = $("#nospk").val();
      var so = $("#noso").val();
      // var test = '"empty"';
      var nospk = spk.toUpperCase();
      var noso = so.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/set_apispk");?>",
            data: {"nospk":nospk,"noso":noso },
            success: function(resp){
              if(resp==='"empty"'){
                  swal({
                      title: "Data Tidak Ada",
                      text: "Data Tidak Ditemukan",
                      type: "warning"
                  })
                  clear();
                 $("#finden").attr('disabled',true);
              }else{
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      // $("#tglso").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglpsn)));
                      $("#nama1").val(data.nmcustomer);
                      $("#nama2").val(data.nmbpkb);
                      $("#alamat1").val(data.alamat);
                      $("#alamat2").val(data.alamatbpkb);
                      $("#alamat3").val(data.alamatbpkb);
                      $("#kel1").val(data.nmlurah);
                      $("#nohp1").val(data.nokontak);
                      $("#kel2").val(data.nmlurahbpkb);
                      $("#nohp2").val(data.nokontak);
                      $("#kec1").val(data.nmkec);
                      $("#notelp").val(data.nokontak);
                      $("#kec2").val(data.nmkecbpkb);
                      $("#kota1").val(data.nmkab);
                      $("#kota1").trigger("chosen:updated");
                      $('#kota1').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#kota2").val(data.nmkab);
                      $("#kota2").trigger("chosen:updated");
                      $('#kota2').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#noktp").val(data.noktp);
                      $("#noktp").mask("99.99.99.999999.9999",{"placeholder" : data.noktp});
                      // .val(data.noktp).trigger("updated");
                      $("#noktp").val(data.noktp);
                      if (data.npwp==='.') { 
                        $("#npwp").mask('99.999.999.9-999.999',{"placeholder" : '00.000.000.0-000.000'}); 
                        $("#npwp").val('00.000.000.0-000.000'); 
                      } else {
                        $("#npwp").mask('99.999.999.9-999.999',{"placeholder" : data.npwp}); 
                        $("#npwp").val(data.npwp);
                      }
                      $("#kota2").val(data.nmkotabpkb);
                      $("#kdtipe").val(data.kdtipeunit);
                      $("#kdtipe").trigger("chosen:updated");
                      $('#kdtipe').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#nmtipe").val(data.nmtipe);
                      $("#kdwarna").val(data.kodewarna);
                      $("#kdwarna").trigger("chosen:updated");
                      $('#kdwarna').select2({
                                    dropdownAutoWidth : true,
                                    width: '100%'
                                  });
                      $("#nmwarna").val(data.nmwarna);
                      // alert(data.kdleasing);
                      // if(data.kdleasing==='TUNAI'){
                      //   $("#tipebayar").val('T');
                      //   $("#tipebayar").trigger("change");
                      // } else {
                      //   $("#tipebayar").val('K');
                      //   $("#tipebayar").trigger("change");
                      // }
                      // $("#kdsales").val(data.kdsales);
                      // $("#kdsales").trigger("chosen:updated");
                      // $('#kdsales').select2({
                      //               dropdownAutoWidth : true,
                      //               width: '100%'
                      //             });
                      // $("#nmspv").val(data.nmsales_header);
                      // alert(data.finden);
                      // if (data.finden === 't' ) {
                      //   $("#finden").prop("checked",true);
                      // } else {
                      //     $("#finden").prop("checked",false);
                      // }
                      // $("#kdtipe").attr('disabled',true);
                      // $("#kdwarna").attr('disabled',true);
                      // $("#kdsales").attr('disabled',true);
                      // $("#nmsales").attr('disabled',true);
                      // $("#nmspv").attr('disabled',true);
                      $("#tipebayar").attr('disabled',false);
                      $(".btn-add").attr('disabled',true);
                      $(".btn-edit").attr('disabled',false);
                      $(".btn-del").attr('disabled',false);
                      $(".btn-batal").show();
                      // getTipeUnit();
                      // getKdWarna();
                      getKdWarnaHeader();
                      getKodeTipeHeader();
                    });
              }
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });

    }

    function addspk(){
      var so = $("#noso").val();
      var noso = so.toUpperCase();
      var tglso = $("#tglso").val();
      var nospk = $("#nospk").val();
      if ($("#finden").prop("checked")){
          var finden = 'true';
      } else {
        var finden = 'false';
      }
      // alert(finden);
      var nama = $("#nama1").val();
      var nama_s = $("#nama2").val();
      var alamat = $("#alamat1").val();
      var alamat_s = $("#alamat2").val();
      var alamat_ktps = $("#alamat3").val();
      var kel = $("#kel1").val();
      var kel_s = $("#kel2").val();
      var nohp = $("#nohp1").val();
      var nohp_s = $("#nohp2").val();
      var kec = $("#kec1").val();
      var kec_s = $("#kec2").val();
      var notelp = $("#notelp").val();
      var npwp = $("#npwp").val();
      var noktp = $("#noktp").val();
      var kota = $("#kota1").val();
      var kota_s = $("#kota2").val();
      var kdtipe = $("#kdtipe").val();
      var nmtipe = $("#nmtipe").val();
      var kdwarna = $("#kdwarna").val();
      var nmwarna = $("#nmwarna").val();
      var tpbayar = $("#tipebayar").val();
      var kdsales = $("#kdsales").val();
      var kdsales_header = $("#nmspv").val();

      var nama = nama.toUpperCase();
      var nama_s = nama_s.toUpperCase();
      var alamat = alamat.toUpperCase();
      var alamat_s = alamat_s.toUpperCase();
      var alamat_ktps = alamat_ktps.toUpperCase();
      var nohp = nohp.toUpperCase();
      var nohp_s = nohp_s.toUpperCase();
      var kel = kel.toUpperCase();
      var kel_s = kel_s.toUpperCase();
      var kec = kec.toUpperCase();
      var kec_s = kec_s.toUpperCase();
      var nmtipe = nmtipe.toUpperCase();
      var nmwarna = nmwarna.toUpperCase();
      var kdsales_header = kdsales_header.toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/addspk");?>",
            data: {"noso":noso
                    ,"nospk":nospk
                    ,"tglso":tglso
                    ,"finden":finden
                    ,"nama":nama
                    ,"nama_s":nama_s
                    ,"alamat":alamat
                    ,"alamat_s":alamat_s
                    ,"alamat_ktps":alamat_ktps
                    ,"kel":kel
                    ,"kel_s":kel_s
                    ,"nohp":nohp
                    ,"nohp_s":nohp_s
                    ,"kec":kec
                    ,"kec_s":kec_s
                    ,"notelp":notelp
                    ,"npwp":npwp
                    ,"noktp":noktp
                    ,"kota":kota
                    ,"kota_s":kota_s
                    ,"kdtipe":kdtipe
                    ,"nmtipe":nmtipe
                    ,"kdwarna":kdwarna
                    ,"nmwarna":nmwarna
                    ,"tpbayar":tpbayar
                    ,"kdsales":kdsales
                    ,"kdsales_header":kdsales_header },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function updatespk(){
      var so = $("#noso").val();
      var noso = so.toUpperCase();
      var tglso = $("#tglso").val();
      var nospk = $("#nospk").val();
      if ($("#finden").prop("checked")){
          var finden = 'true';
      } else {
        var finden = 'false';
      }
      // alert(finden);
      var nama = $("#nama1").val();
      var nama_s = $("#nama2").val();
      var alamat = $("#alamat1").val();
      var alamat_s = $("#alamat2").val();
      var alamat_ktps = $("#alamat3").val();
      var kel = $("#kel1").val();
      var kel_s = $("#kel2").val();
      var nohp = $("#nohp1").val();
      var nohp_s = $("#nohp2").val();
      var kec = $("#kec1").val();
      var kec_s = $("#kec2").val();
      var notelp = $("#notelp").val();
      var npwp = $("#npwp").val();
      var noktp = $("#noktp").val();
      var kota = $("#kota1").val();
      var kota_s = $("#kota2").val();
      var kdtipe = $("#kdtipe").val();
      var nmtipe = $("#nmtipe").val();
      var kdwarna = $("#kdwarna").val();
      var nmwarna = $("#nmwarna").val();
      var tpbayar = $("#tipebayar").val();
      var kdsales = $("#kdsales").val();
      var kdsales_header = $("#nmspv").val();
        $.ajax({
            type: "POST",
            url: "<?=site_url("spknew/updatespk");?>",
            data: {"noso":noso
                    ,"nospk":nospk
                    ,"tglso":tglso
                    ,"finden":finden
                    ,"nama":nama
                    ,"nama_s":nama_s
                    ,"alamat":alamat
                    ,"alamat_s":alamat_s
                    ,"alamat_ktps":alamat_ktps
                    ,"kel":kel
                    ,"kel_s":kel_s
                    ,"nohp":nohp
                    ,"nohp_s":nohp_s
                    ,"kec":kec
                    ,"kec_s":kec_s
                    ,"notelp":notelp
                    ,"npwp":npwp
                    ,"noktp":noktp
                    ,"kota":kota
                    ,"kota_s":kota_s
                    ,"kdtipe":kdtipe
                    ,"nmtipe":nmtipe
                    ,"kdwarna":kdwarna
                    ,"nmwarna":nmwarna
                    ,"tpbayar":tpbayar
                    ,"kdsales":kdsales
                    ,"kdsales_header":kdsales_header },
            success: function(resp){
                  var obj = JSON.parse(resp);
                  $.each(obj, function(key, data){
                      swal({
                          title: data.title,
                          text: data.msg,
                          type: data.tipe
                      }, function(){
                          window.location.reload();
                      });
                  });
            },
            error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function deletespk(){
      var so = $('#noso').val();
      var noso = so.toUpperCase();
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("spknew/deletespk");?>",
                    data: {"noso":noso },
                    success: function(resp){
                      var obj = JSON.parse(resp);
                      $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            window.location.reload();
                        });
                    });
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
</script>

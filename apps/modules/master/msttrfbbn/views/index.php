<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-header">
            <a href="<?php echo $add;?>" class="btn btn-primary">Tambah</a>
            <a href="javascript:void(0);" class="btn btn-default btn-refersh">Refresh</a>
            <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="<?php echo $add;?>" >Tambah Data Baru</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="btn-refersh">Refresh</a>
                    </li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
                <div class="table-responsive">
                <table class="dataTable table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th style="width: 100px;text-align: center;">No.</th>
                            <th style="text-align: center;">Nama Kota</th>
                            <th style="text-align: center;">Biaya Proses</th>
                            <th style="text-align: center;">Biaya Jasa</th>
                            <th style="width: 10px;text-align: center;">Edit</th> 
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="width: 100px;text-align: center;">No.</th>
                            <th style="text-align: center;">Nama Kota</th>
                            <th style="text-align: center;">Biaya Proses</th>
                            <th style="text-align: center;">Biaya Jasa</th>
                            <th style="width: 10px;text-align: center;">Edit</th> 
                        </tr>
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-refersh").click(function(){
            table.ajax.reload();
        });

        //var rows_selected = [];
        var column = [];

        column.push({
            "aTargets": [ 2,3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });  

        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "orderCellsTop": true,
            "fixedHeader": true,
            // "scrollY": 500,
            // "scrollX": true,
            // "aoColumnDefs": column,
            "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "kota"},
                { "data": "byproses" },
                { "data": "byjasa"}, 
                { "data": "edit" }  
            ], 
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('msttrfbbn/json_dgview');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus" && title!=="No."){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
    });

    function refresh(){
        table.ajax.reload();
    }

    function deleted(kdaks){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("msttrfbbn/delete");?>",
                data: {"kdaks":kdaks },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            table.ajax.reload();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }
</script>

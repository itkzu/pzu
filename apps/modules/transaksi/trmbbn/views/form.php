<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    #modalform .errors {
        color: red;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }  
</style> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                  , 'id' => 'form_add'
                  , 'name' => 'form_add'
                  , 'enctype' => 'multipart/form-data'
                  , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-header">
                <button type="button" class="btn btn-primary btn-submit">
                    Simpan
                </button>
                <button type="button" class="btn btn-default btn-batal">
                    Batal
                </button>
            </div> 

          <div class="col-xs-12"> 
                <div style="border-top: 1px solid #ddd; height: 10px;"></div> 
          </div>

          <div class="box-body">
              <div class="row">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['notbj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['notbj']);
                                          echo form_error('notbj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div>

                          </div>
                        </div> 
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row">

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgltbj']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php
                                          echo form_input($form['tgltbj']);
                                          echo form_error('tgltbj','<div class="note">','</div>');
                                      ?>
                                  </div>
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-md-12 col-lg-12">
                    <div class="row"> 
                      <div class="col-xs-12">
                          <label></label>
                          <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                      </div>
                    </div>
                  </div>  

                  <div class="col-xs-12">
                    <p><a id="add" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                    <a id="del" class="btn btn-danger btn-del"><i class="fa fa-minus"></i> Hapus</a></p>
                    <p id="alldata" class="kata"></p>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered dataTable display nowrap">
                        <thead>
                          <tr>
                            <th style="width: 10px;text-align: center;">No.</th>
                            <th style="text-align: center;">No. Mesin</th> 
                            <th style="text-align: center;">Tipe Unit</th> 
                            <th style="text-align: center;">Nama Konsumen</th> 
                            <th style="text-align: center;">Alamat Konsumen</th> 
                            <th style="text-align: center;">Biaya BBN</th> 
                            <th style="text-align: center;">Jasa</th> 
                            <th style="text-align: center;">Total</th> 
                            <th style="text-align: center;">No. DO</th> 
                            <th style="text-align: center;">Tgl. DO</th>    
                          </tr>
                        </thead>
                        <tfoot></tfoot>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row"> 

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['total']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['total']);
                                          echo form_error('total','<div class="note">','</div>');
                                      ?> 
                              </div> 

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['disc']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['disc']);
                                          echo form_error('disc','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5 col-md-5 ">
                          <div class="row"> 

                          </div>
                        </div>

                        <div class="col-xs-7 col-md-7 ">
                          <div class="row">

                              <div class="col-xs-6"> 
                              </div> 

                              <div class="col-xs-2">
                                  <div class="form-group">
                                      <?php echo form_label($form['pph21']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['pph21']);
                                          echo form_error('pph21','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-8 col-md-8 ">
                          <div class="row"> 

                              <div class="col-xs-12"> 
                                      <?php
                                          echo form_input($form['banner']);
                                          echo form_error('banner','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4 ">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['alltot']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['alltot']);
                                          echo form_error('alltot','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>  

                    </div>
                  </div>
                </div>

              </div>
          </div>

        </form>
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- modal dialog -->
<div id="modal_trans" class="modal fade">
    <div class="modal-dialog"  style="width: 75%;">
        <div class="modal-content">
            <form id="modalf" name="modalf" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Detail Tagihan BBN</h4>
                </div>
                <!-- /.modal-header -->

                <!-- .modal-body -->
                <div class="modal-body">

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nosin']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['nosin']);
                                          echo form_error('nosin','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nodo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['nodo']);
                                          echo form_error('nodo','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nora']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['nora']);
                                          echo form_error('nora','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tgldo']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-4"> 
                                      <?php
                                          echo form_input($form['tgldo']);
                                          echo form_error('tgldo','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div> 

                  <div class="col-xs-12">
                    <div class="row"> 
                        <br>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['nama']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['nama']);
                                          echo form_error('nama','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['alamat']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['alamat']);
                                          echo form_error('alamat','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['kota']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['kota']);
                                          echo form_error('kota','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['bbn']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['bbn']);
                                          echo form_error('bbn','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['kdtipe']);
                                          echo form_error('kdtipe','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['jasa']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['jasa']);
                                          echo form_error('jasa','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-6 col-md-6">
                          <div class="row"> 

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['warna']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-5"> 
                                      <?php
                                          echo form_input($form['warna']);
                                          echo form_error('warna','<div class="note">','</div>');
                                      ?> 
                              </div> 

                              <div class="col-xs-3"> 
                                      <?php
                                          echo form_input($form['tahun']);
                                          echo form_error('tahun','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div>

                        <div class="col-xs-6 col-md-6">
                          <div class="row">  

                              <div class="col-xs-4">
                                  <div class="form-group">
                                      <?php echo form_label($form['tot_by']['placeholder']); ?>
                                  </div>
                              </div>

                              <div class="col-xs-8"> 
                                      <?php
                                          echo form_input($form['tot_by']);
                                          echo form_error('tot_by','<div class="note">','</div>');
                                      ?> 
                              </div> 

                          </div>
                        </div> 

                    </div>
                  </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
    </div> 
            <!-- /.box-body --> 
  </div>
            <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        reset(); 
        autoNum();
        // $("#notbj").val('ABJ-____-____');  
        $("#notbj").attr('disabled',true);  

        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                jnstrx : "required"
            },
            messages : {
                jnstrx : ""
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalf').validate({
            errorClass: 'errors',
            rules : {
                nosin  : "required",
                bbn  : "required",
                jasa  : "required"
            },
            messages : {
                nosin  : "Masukkan Nomer Mesin",
                bbn  : "Masukkan Nilai BBN",
                jasa  : "Masukkan Nilai Jasa"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        $('.btn-batal').click(function () { 
            batal();
        });   

        $('.btn-submit').click(function () { 
            submit();
        });  

        $('.btn-del').click(function () { 
            var nodo = table.row('.selected').data()['nodo']; 
            delDetail(nodo);
        });  

        var column = []; 

        column.push({
            "aTargets": [ 5,6,7 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        }); 

        column.push({
            "aTargets": [ 9 ],
            "mRender": function (data, type, full) {
                return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
            },
            "sClass": "center"
        });  

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "no"},
                { "data": "nosin" },
                { "data": "kdtipe" },
                { "data": "nama"},
                { "data": "alamat" },
                { "data": "bbn"},
                { "data": "jasa"},
                { "data": "total"},
                { "data": "nodo"}, 
                { "data": "tgldo"} 
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "select":{
                style: 'single',
            },
            "fixedColumns": {
                leftColumns: 2
            }, 
            "checkboxes": {
                'selectRow': true
            },
            "bPaginate": true, 
            "bSort": false,
            "bAutoWidth": false,
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,    
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "notbj", "value": $('#notbj').val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('trmbbn/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            }, 
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index 
                $('#total').autoNumeric('set',total); 
                sum(); 
            },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ", 
        });   

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });  

        $('.dataTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
 
        });



        $('.btn-add').click(function(){ 
            // getKodepiutangmx();
            // validator.resetForm();
            $('#modal_trans').modal('toggle');
            r_modal();
            autoNum_modal();
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            // $("#nourut").val('');
            r_modal();
        });

        $('.btn-simpan').click(function () { 
            // console.log($('#modelf').valid());
            if ($("#modalf").valid()) { 
                tambah();
                // alert('1');
            }
        });

        $('#nosin').keyup(function () {
            var jml = $('#nosin').val();
            var nosin = jml.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("_","");
            var n = nosin.length;
            
            if(n===12){
                // console.log(n);    
                set_nosin();   
            }
            // set_nosin();
        }); 
    });

    function reset(){
      // $("#notbj").val('ABJ-____-____');
      $("#total").val('');
      $("#disc").val('');
      $("#pph21").val('');
      $("#alltot").val('');
      $("#banner").val('');
      $("#tgltbj").val($.datepicker.formatDate('dd-mm-yy', new Date()));  
    }

    function r_modal(){
      $("#nosin").val('');
      $("#nosin").mask('***-***-***-***');
      $("#nora").val('');
      $("#nama").val('');
      $("#alamat").val('');
      $("#kdtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#kota").val('');
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));  
      $("#bbn").val('');
      $("#jasa").val('');
      $("#tot_by").val('');
    }

    function c_modal(){ 
      $("#nora").val('');
      $("#nama").val('');
      $("#alamat").val('');
      $("#kdtipe").val('');
      $("#warna").val('');
      $("#tahun").val('');
      $("#kota").val('');
      $("#nodo").val('');
      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));  
      $("#bbn").val('');
      $("#jasa").val('');
      $("#tot_by").val('');
    }

    function autoNum(){ 
      $("#total").autoNumeric('init',{ currencySymbol : 'Rp.'});
      $("#disc").autoNumeric('init',{ currencySymbol : 'Rp.'});
      $("#pph21").autoNumeric('init',{ currencySymbol : 'Rp.'});
      $("#alltot").autoNumeric('init',{ currencySymbol : 'Rp.'}); 
    }

    function autoNum_modal(){ 
      $("#bbn").autoNumeric('init',{ currencySymbol : 'Rp.'});
      $("#jasa").autoNumeric('init',{ currencySymbol : 'Rp.'});
      $("#tot_by").autoNumeric('init',{ currencySymbol : 'Rp.'});
    } 

  function set_nosin(){   
      var nosin = $("#nosin").val().toUpperCase();  
      // alert(count);
      $.ajax({
          type: "POST",
          url: "<?=site_url('trmbbn/set_nosin');?>",
          data: { "nosin":nosin},
          success: function(resp){
            // $("#modal_trans").modal("hide"); 
            if(resp==='"empty"'){
                swal({
                  title: 'No. Mesin tidak ditemukan atau sudah masuk Detail Tagihan BBN',
                  text: '',
                  type: 'error'
                },function (){
                  c_modal();
                });
            }else{
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                // $('#ket').show();  
                    // alert(data.nobbn_trm);
                    if(data.nobbn_trm===null){
                      $('#nora').val(data.nora);
                      $('#nodo').val(data.nodo);
                      $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo))); 
                      $('#nama').val(data.nama);
                      $('#alamat').val(data.alamat);
                      $('#kota').val(data.kota);
                      $('#kdtipe').val(data.kdtipe);
                      $('#warna').val(data.nmwarna);
                      $('#tahun').val(data.tahun);
                    } else {
                      swal({
                        title: 'No. Mesin sudah pernah input Tagihan BBN',
                        text: '',
                        type: 'error'
                      },function (){
                        c_modal();
                      });
                    }
                });
            } 
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  } 

    function sum(){
        var total = $('#total').autoNumeric('get'); 
        var disc  = $('#disc').autoNumeric('get'); 
        if(disc===''){
          var disc = 0;
        }
        var pph21 = $('#pph21').autoNumeric('get'); 
        if(pph21===''){
          var pph21 = 0;
        }

        var alltot = parseInt(total) - (parseInt(disc) + parseInt(pph21));
        if(!isNaN(alltot)){
            $('#alltot').autoNumeric('set',alltot); 
            var terbilang = penyebut(alltot);
            if(alltot==='0'){
              $('#banner').val(terbilang);  
            } else {
              $('#banner').val(terbilang+" RUPIAH");  
            }
            
        }
    }

  function sum1(){
      var bbn = $('#bbn').autoNumeric('get');
      if(bbn===''){
        var bbn = 0;
      }
      var jasa = $('#jasa').autoNumeric('get');
      if(jasa===''){
        var jasa = 0;
      }
      var tot_by = parseInt(bbn) + parseInt(jasa);
      if(!isNaN(tot_by)){
        $('#tot_by').autoNumeric('set',tot_by);
      }
  }

  function tambah(){   ; 
      var nobbn_trm = $("#notbj").val(); 
      var tglbbn_trm = $("#tgltbj").val();  
      var nodo = $("#nodo").val();  
      var nilai = $("#bbn").autoNumeric('get');  
      var jasa = $("#jasa").autoNumeric('get');  
      // alert(count);

      $.ajax({
          type: "POST",
          url: "<?=site_url('trmbbn/tambah');?>",
          data: { "nobbn_trm":nobbn_trm, "tglbbn_trm":tglbbn_trm, "nodo":nodo, "nilai":nilai, "jasa":jasa },
          success: function(resp){
            $("#modal_trans").modal("hide");
            var obj = jQuery.parseJSON(resp);
            $.each(obj, function(key, data){
              $('#notbj').val(data.nobbn_trm);
                swal({
                  title: data.title,
                  text: data.msg,
                  type: data.tipe
                }, function(){
                  table.ajax.reload();
                });
              });
          },
          error:function(event, textStatus, errorThrown) {
            swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  } 

  function delDetail(nodo){     
    
      var nobbn_trm = $('#notbj').val();
      $.ajax({
          type: "POST",
          url: "<?=site_url('trmbbn/delDetail');?>",
          data: {"nobbn_trm":nobbn_trm
                  ,"nodo":nodo 
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              $.each(obj, function(key, data){ 
                  table.ajax.reload(); 
              });
          },
          error: function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      }); 
    }

    function batal(){ 
            swal({
                title: "Konfirmasi Batal Transaksi!",
                text: "Data yang dibatalkan tidak disimpan !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c9302c",
                confirmButtonText: "Ya, Lanjutkan!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false
            }, function () {
            var nobbn_trm = $("#notbj").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("trmbbn/batal");?>",
                data: {"nobbn_trm":nobbn_trm },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){ 
                      swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                      }, function() {
                        window.location.href = '<?=site_url('trmbbn');?>';  
                      }); 
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
            }); 
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nobbn_trm = $("#notbj").val();
            var tglbbn_trm = $("#tgltbj").val(); 
            $.ajax({
                type: "POST",
                url: "<?=site_url("trmbbn/submit");?>",
                data: {"nobbn_trm":nobbn_trm
                        ,"tglbbn_trm":tglbbn_trm},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){  
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function() {
                                // window.location.href = '<?=site_url('trmbbn');?>'; 
                                // window.location.href = '../ctk_lr/'+nodo; 

                            window.open('ctk/'+nobbn_trm, '_blank');
                            window.location.href = '<?=site_url('trmbbn');?>';  
                        });  
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    } 

    function penyebut(nilai) { 
        var nilai = Math.floor(Math.abs(nilai));
        var huruf = ["", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS"];
        var temp = "";
        if (nilai < 12) {
          var temp = " "+huruf[nilai];
        } else if (nilai <20) {
          var temp = penyebut(parseFloat(nilai) - 10)+" BELAS";
        } else if (nilai < 100) {
          var temp = penyebut(parseFloat(nilai)/10)+" PULUH"+penyebut(parseFloat(nilai) % 10);
        } else if (nilai < 200) {
          var temp = " SERATUS"+penyebut(parseFloat(nilai) - 100);
        } else if (nilai < 1000) {
          var temp = penyebut(parseFloat(nilai)/100)+" RATUS"+penyebut(parseFloat(nilai) % 100);
        } else if (nilai < 2000) {
          var temp = " SERIBU"+penyebut(parseFloat(nilai) - 1000);
        } else if (nilai < 1000000) {
          var temp = penyebut(parseFloat(nilai)/1000)+" RIBU"+penyebut(parseFloat(nilai) % 1000);
        } else if (nilai < 1000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000)+" JUTA"+penyebut(parseFloat(nilai) % 1000000);
        } else if (nilai < 1000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000)+" MILIAR"+penyebut(fmod(parseFloat(nilai),1000000000));
        } else if (nilai < 1000000000000000) {
          var temp = penyebut(parseFloat(nilai)/1000000000000)+" TRILIUN"+penyebut(fmod(parseFloat(nilai),1000000000000));
        }
        return temp;
    } 
</script>

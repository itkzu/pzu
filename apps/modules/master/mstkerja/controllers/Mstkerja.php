<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstkerja
 *
 * @author adi
 */
class Mstkerja extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstkerja/submit'),
            'add' => site_url('mstkerja/add'),
            'edit' => site_url('mstkerja/edit'),
            'reload' => site_url('mstkerja'),
        );
        $this->load->model('mstkerja_qry'); 

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstkerja_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->mstkerja_qry->getKategori();
    }*/

    public function submit() {
        echo $this->mstkerja_qry->submit();
    }

    public function update() {
        echo $this->mstkerja_qry->update();
    }

    public function delete() {
        echo $this->mstkerja_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdkerja'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Kerja',
                    'id'          => 'kdkerja',
                    'name'        => 'kdkerja',
                    'value'       => set_value('kdkerja'),
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmkerja'=> array(
                    'placeholder' => 'Nama Pekerjaan',
                    'id'      => 'nmkerja',
                    'name'        => 'nmkerja',
                    'value'       => set_value('nmkerja'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		   'faktif'=> array(
                'placeholder' => '',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktif,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdkerja = $this->uri->segment(3);
        }
        $this->_check_id($kdkerja);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdkerja'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Kerja',
                    'id'          => 'kdkerja',
                    'name'        => 'kdkerja',
                    'value'       => $this->val[0]['kdkerja'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmkerja'=> array(
                    'placeholder' => 'Nama Pekerjaan',
                    'id'          => 'nmkerja',
                    'name'        => 'nmkerja',
                    'value'       => $this->val[0]['nmkerja'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ), 
      		   'faktif'=> array(
                'placeholder' => 'Status Pekerjaan',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktifx,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdkerja){
        if(empty($kdkerja)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstkerja_qry->select_data($kdkerja);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdkerja,$stat,$nmkerja,$faktif) {
        if(!empty($kdkerja) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdkerja',
                    'label' => 'Kode Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmkerja',
                    'label' => 'Nama Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

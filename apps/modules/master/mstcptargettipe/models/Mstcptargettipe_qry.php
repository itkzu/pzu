<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstcptargettipe_qry
 *
 * @author adi
 */
class Mstcptargettipe_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function getDataSeries() {
        $this->db->where('faktif',"TRUE");
        $q = $this->db->get("tipe_series");
        return $q->result_array();
    }

    public function select_data($param = null) {
        $periode = $this->input->post('periode');
        $kdtipesr = $this->input->post('kdtipesr');
        $this->db->select("id,kdtipesr, kd_cabang, to_char(periode,'MM-YYYY') AS periode, jml_hr_kerja, cp_target_total, cp_target_harian");
        if(!empty($periode) && !empty($kdtipesr)){
            $this->db->where("to_char(periode,'MM-YYYY')",$periode);
            $this->db->where('kdtipesr',$kdtipesr);
        }else{
            $this->db->where('id',$param);
        }
        $query = $this->db->get('mstcptargettipe');
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return "";
        }
    }

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['periode']) ){
            $periode = $_GET['periode'];
        }else{
            $periode = '';
        }

        $aColumns = array('periode',
                            'nmtipesr',
                            'cp_target_total',
                            'id',);
	$sIndexColumn = "id";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT to_char(periode,'YYYY-MM') AS periode, nmtipesr, jml_hr_kerja, cp_target_total, cp_target_harian, id
                        FROM
                        mstcptargettipe
                        JOIN pzu.tipe_series
                            ON mstcptargettipe.kdtipesr = tipe_series.kdtipesr
                        WHERE kd_cabang = '".$this->kd_cabang."'
                            AND to_char(periode,'MM-YYYY') = '".$periode."'
                    ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " Where (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         * QUERY YANG AKAN DITAMPILKAN
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );


        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
                $row[3] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('mstcptargettipe/edit/'.$aRow['id'])."\">Edit</a>";
                $row[4] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['id']."');\">Hapus</button>";
		$output['aaData'][] = $row;
	}
	echo  json_encode( $output );
    }

    public function submit() {
        try {
            $array = $this->input->post();
            $array['kd_cabang'] = $this->kd_cabang;
            if(empty($array['id'])){
                unset($array['id']);
                $array['periode'] = $this->apps->monthConvert($array['periode']);
                $resl = $this->db->insert('mstcptargettipe',$array);
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Tersimpan";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && empty($array['stat'])){
                $array['periode'] = $this->apps->monthConvert($array['periode']);
                $this->db->where('id', $array['id']);
                $resl = $this->db->update('mstcptargettipe', $array);
                // echo $this->db->last_query();
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terupdate";
                    $this->state = "1";
                }

            }elseif(!empty($array['id']) && !empty($array['stat'])){
                $this->db->where('id', $array['id']);
                $resl = $this->db->delete('mstcptargettipe');
                if( ! $resl){
                    $err = $this->db->error();
                    $this->res = " Error : ". $this->apps->err_code($err['message']);
                    $this->state = "0";
                }else{
                    $this->res = "Data Terhapus";
                    $this->state = "1";
                }
            }else{
                $this->res = "Variabel tidak sesuai";
                $this->state = "0";
            }

        }catch (Exception $e) {
            $this->res = $e->getMessage();
            $this->state = "0";
        }

        $arr = array(
            'state' => $this->state,
            'msg' => $this->res,
            );
        $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr);
    }

}

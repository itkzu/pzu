<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php
					$attributes = array(
						'role=' => 'form'
						, 'id' => 'form_add'
						, 'name' => 'form_add'
						, 'class' => "form-inline");
					echo form_open($submit,$attributes); 
				?> 
				<?php echo form_close(); ?>
				<!-- <hr> -->
				<div class="table-responsive">
					<table class="table table-bordered table-hover dataTable" style="width: 1600px;">
						<thead>
							<tr>
								<th style="width: 5px; text-align: left;">No.</th>
								<th style="width: 5px; text-align: left;">Kode Unit</th>
								<th style="width: 150px; text-align: left;">Tipe Unit</th>
								<th style="text-align: left;">Nama Tipe</th>

								<th style="width: 5px; text-align: left;">Tahun</th>

								<th style="width: 5px; text-align: left;">Kode</th>
								<th style="width: 100px; text-align: left;">Warna</th>

								<th style="width: 10px; text-align: right;">Harga ONTR</th>
								<th style="width: 10px; text-align: right;">Harga OFFTR</th>
								<th style="width: 10px; text-align: right;">Cad. BBN</th>

								<th style="width: 10px; text-align: left;">Tgl Faktur</th>
								<th style="width: 10px; text-align: right;">Umur (hari)</th>

								<th style="width: 100px; text-align: left;">Lokasi</th>

								<th style="width: 10px; text-align: left;">No. Mesin</th>
								<th style="width: 10px; text-align: left;">No. Rangka</th>

							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>No.</th>
								<th>Kode Unit</th>
								<th>Tipe Unit</th>
								<th>Nama Tipe</th>
								<th>No. Mesin</th>
								<th>No. Rangka</th>
								<th>Tahun</th>
								<th>Kode</th>
								<th>Warna</th>
								<th>Lokasi</th>
								<th>Tgl Faktur</th>
								<th>Umur (hari)</th>
								<th>Harga ON TR</th>
								<th>Harga OFF TR</th>
								<th>Cad. BBN</th>
							</tr>
						</tfoot>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		var column = [];
		
		column.push({ 
			"aTargets": [ 0 ],
			"searchable": false,
			"orderable": false  
		});

		column.push({ 
			"aTargets": [ 10 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : moment(data).format('L');
				// return formmatedvalue;
			}
		});

		column.push({ 
			"aTargets": [ 7,8,9,11 ],
			"mRender": function (data, type, full) {
				return type === 'export' ? data : numeral(data).format('0,0');
				// return formmatedvalue;
			},
			"sClass": "right"
		});

		table = $('.dataTable').DataTable({
			"aoColumnDefs": column,
			"order": [[ 1, 'asc' ]],
			"lengthMenu": [[-1], ["Semua Data"]],
			"bProcessing": true,
			"bServerSide": true,
			"bDestroy": true,
			"bAutoWidth": false,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'json',
					"type": "GET", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			},
			'rowCallback': function(row, data, index){
				//if(data[23]){
					//$(row).find('td:eq(23)').css('background-color', '#ff9933');
				//}
			},
			"sAjaxSource": "<?=site_url('rptstoksales/json_dgview');?>",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
			},
			//dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{extend: 'copy',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'csv',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'excel',
					exportOptions: {orthogonal: 'export'}},
				{extend: 'pdf', 
					orientation: 'landscape',
					pageSize: 'A3'
				},
				{extend: 'print',
					customize: function (win){
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');
							$(win.document.body).find('table')
																	.addClass('compact')
																	.css('font-size', 'inherit');
					}
				}
			],
			"sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
		});
		
		//row number
		table.on( 'order.dt search.dt', function () {
			table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				cell.innerHTML = i+1;
			});
		}).draw();

		$('.dataTable').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});

		$('.dataTable tfoot th').each( function () {
			var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
			if(title!=="Edit" && title!=="Delete" && title!=="No."){
				$(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
			}else{
				$(this).html( '' );
			}
		});

		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function (ev) {
				//if (ev.keyCode == 13) { //only on enter keypress (code 13)
					that
						.search( this.value )
						.draw();
				//}
			});
		});
	});
</script>
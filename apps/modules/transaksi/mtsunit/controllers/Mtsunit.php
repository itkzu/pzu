<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mtsunit
 *
 * @author adi
 */
class Mtsunit extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mtsunit/submit'),
            'add' => site_url('mtsunit/add'),
            'edit' => site_url('mtsunit/edit'),
            'reload' => site_url('mtsunit'),
        );
        $this->load->model('mtsunit_qry'); 
        $cabang = $this->mtsunit_qry->getDataLokasi();
        foreach ($cabang as $value) {
            $this->data['kdlokasi'][$value['kdlokasi']] = $value['nmlokasi'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    } 

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function setnompp() {
        echo $this->mtsunit_qry->setnompp();
    }

    public function hapus() {
        echo $this->mtsunit_qry->hapus();
    }

    public function setnosin() {
        echo $this->mtsunit_qry->setnosin();
    }

    public function setnoid() {
        echo $this->mtsunit_qry->setnoid();
    }

    public function add_doin() {
        echo $this->mtsunit_qry->add_doin();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'nompp'=> array(
                    'placeholder' => 'No. Mutasi',
                    //'type'        => 'hidden',
                    'id'          => 'nompp',
                    'name'        => 'nompp',
                    'value'       => set_value('nompp'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tglmpp'=> array(
                    'placeholder' => 'Tanggal Mutasi',
                    'id'          => 'tglmpp',
                    'name'        => 'tglmpp',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin / Rk', 
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => ''
            ), 
           'nora'=> array(
                    'placeholder' => '', 
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ), 
            'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'value'       => set_value('kdtipe'),
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => '',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ), 
            'warna'=> array(
                    'placeholder' => 'Warna / Tahun',
                    'value'       => set_value('warna'),
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => '',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'kdlokasi1'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdlokasi1',
                        'class'       => 'form-control',
                    ), 
                    'data'     => $this->data['kdlokasi'], 
                    'placeholder' => 'Lokasi Saat Ini',
                    'value'       => set_value('kdlokasi1'),
                    'name'        => 'kdlokasi1',
                    'style'       => 'text-transform: uppercase;',  
            ),
            'nmlokasi1'=> array(
                    'placeholder' => '',
                    'value'       => set_value('nmlokasi1'),
                    'id'          => 'nmlokasi1',
                    'name'        => 'nmlokasi1',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
           'kdlokasi2'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'kdlokasi2',
                        'class'       => 'form-control select2',
                    ), 
                    'data'     => $this->data['kdlokasi'], 
                    'placeholder' => 'Lokasi Mutasi',
                    'value'       => set_value('kdlokasi2'),
                    'name'        => 'kdlokasi2',
                    'style'       => 'text-transform: uppercase;',
            ), 
            'nmlokasi2'=> array(
                    'placeholder' => '',
                    'value'       => set_value('nmlokasi2'),
                    'id'          => 'nmlokasi2',
                    'name'        => 'nmlokasi2',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control', 
                    // 'readonly'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => ''
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(   
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->mtsunit_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptrekapmekanik_qry
 *
 * @author adi
 */
class Rptrekapmekanik_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();        
    }  
       
    public function get_data() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $kddiv = $this->input->post('kddiv');
        if($this->session->userdata('data')['kddiv']==='ZPH01.02S'){
            $str = "select *, row_number() over (order by nmmekanik,tglsv) as no from bkl.vl_ue_mk where tglsv between '".$this->apps->dateConvert($periode_awal)."' and '".$this->apps->dateConvert($periode_akhir)."' order by nmmekanik, tglsv"; 
        } else { 
            $str = "select *, row_number() over (order by nmmekanik,tglsv) as no from bkl.vl_ue_mk where kddiv = substring('".$this->session->userdata('data')['kddiv']."',0,10) and tglsv between '".$this->apps->dateConvert($periode_awal)."' and '".$this->apps->dateConvert($periode_akhir)."' order by nmmekanik, tglsv";
        }
        
        $q = $this->db->query($str);
                // echo $this->db->last_query();
        if($q->num_rows()>0){
            $arr = $q->result_array();
            $leas = array();
            $cbg = $arr[0]['nmcabang'];
            foreach ($arr as $leasing) {
                $leas[$leasing['nmmekanik']] = $leasing['nmmekanik'];
            }
            $dt_arr = array();
            foreach ($leas as $v) {
                $no = 1;
                foreach ($arr as $data) {
                    if($v==$data['nmmekanik']){
                        $dt = $data + array('no'=>$no);
                        $dt_arr[] = $dt;
                        $no++;
                    }
                }
            }
            $res = array(
                'leasing' => $leas,
                'data' => $dt_arr,
                'cabang' => $cbg,
            );
        }else{
            $res = null;
        }
        return json_encode($res);
    }

}

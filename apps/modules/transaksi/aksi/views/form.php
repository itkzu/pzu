<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['noaksi']['placeholder']);
                                            echo form_input($form['noaksi']);
                                            echo form_error('noaksi','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglaksi']['placeholder']);
                                            echo form_input($form['tglaksi']);
                                            echo form_error('tglaksi','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=form_label($form['kdsup']['placeholder']);?>
                                        <div class="input-group">
                                            <?php
                                                echo form_dropdown($form['kdsup']['name'],$form['kdsup']['data'] ,$form['kdsup']['value'] ,$form['kdsup']['attr']);
                                                echo form_error('kdsup', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['ket']['placeholder']);
                                            echo form_input($form['ket']);
                                            echo form_error('ket','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php
                                                echo form_input($form['nourut']);
                                                echo form_error('nourut', '<div class="note">', '</div>');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-2">
                                    <div class="checkbox">
                            						<label>
                              							<?php
                                                echo form_checkbox($form['stathrg']);
                                                echo '<b>Harga sudah diketahui</b>';
                              							?>
                            						</label>
                          					</div>
                                </div>

                                    <div class="col-md-2">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-set" id="btn-set">Set</button>
                                        </div>
                                    </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div style="border-top: 0px solid #ddd; height: 10px;"></div>
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-add">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">Nama Barang</th>
                                            <th style="width: 100px;text-align: Right;">Quantity</th>
                                            <th style="width: 150px;text-align: Right;">Harga</th>
                                            <th style="width: 150px;text-align: Right;">Sub Total</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                            <th style="width: 10px;text-align: center;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th style="text-align: right;"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['total']['placeholder']);
                                            echo form_input($form['total']);
                                            echo form_error('total','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['statppn']['placeholder']);
                                            echo form_dropdown($form['statppn']['name'],$form['statppn']['data'] ,$form['statppn']['value'] ,$form['statppn']['attr']);
                                            echo form_error('statppn','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo '<b>Diskon <small>dalam rupiah </small>(-)</b>';
                                            echo form_input($form['diskon']);
                                            echo form_error('diskon','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['dpp']['placeholder']);
                                            echo form_input($form['dpp']);
                                            echo form_error('dpp','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['biaya_lain']['placeholder']);
                                            echo form_input($form['biaya_lain']);
                                            echo form_error('biaya_lain','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['ppn']['placeholder']);
                                            echo form_input($form['ppn']);
                                            echo form_error('ppn','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['total_net']['placeholder']);
                                            echo form_input($form['total_net']);
                                            echo form_error('total_net','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['totppn']['placeholder']);
                                            echo form_input($form['totppn']);
                                            echo form_error('totppn','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-submit">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Transaksi</h4>
                </div>

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['kdaks']['placeholder']);
                                    echo form_dropdown( $form['kdaks']['name'],
                                                        $form['kdaks']['data'] ,
                                                        $form['kdaks']['value'] ,
                                                        $form['kdaks']['attr']);
                                    echo form_error('kdaks','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['qty']['placeholder']);
                                    echo form_input($form['qty']);
                                    echo form_error('qty','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo "<b>Harga @</b>";
                                    echo form_input($form['harga']);
                                    echo form_error('harga','<div class="note">','</div>');
                                  ?>
                              </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {

        empty();
        autoNum();
        $('.btn-add').attr('disabled',true);

        if($('#stathrg').val() == 't'){
          $('#stathrg').prop("checked", true);
        } else {
          $('#stathrg').prop("checked", false);
        }

        status();
      //	getKodeAksi();
        $('#myform').validate({
            errorClass: 'errors',
            rules : {
                kdaks : "required"
            },
            messages : {
                kdaks : "Masukkan Barang Aksesoris"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdakun  : "required",
                kdaks : "required"
            },
            messages : {
                kdakun  : "Masukkan Kode Akun",
                kdaks : "Masukkan Nama Barang"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 2,3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 4,5 ],
            "sClass": "center"
        });

        column.push({
            "aTargets": [ 1 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
            },
            "sClass": "right"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nmaks"},
                { "data": "qty" },
                { "data": "harga" },
                { "data": "total"},
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noaksi", "value": $("#noaksi").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('aksi/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 2 ).footer() ).html('Total');
                $( api.column( 3 ).footer() ).html(numeral(total).format('0,0.00'));
                $('#total').autoNumeric('set',total);
                status();
                getKodeAksi();
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        $('.btn-set').click(function(){
            $('.btn-set').hide();
            $('.btn-add').attr('disabled',false);
            $('#stathrg').attr('disabled',true);
            if ($("#stathrg").prop("checked")){
              $('#harga').prop('readonly',false);
            } else {
              $('#harga').prop('readonly',true);
            }

        });


        $('.btn-add').click(function(){

        	  getKodeAksi();
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kdaks').select2({
                placeholder: '-- Pilih Barang --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            clear();
        });
/*
        $('#statppn').click(function(){
          status();
        });
*/
        $('#statppn').change(function(){
          status();
        });

        $('#diskon').keyup(function(){
           status();
        });

        $('#biaya_lain').keyup(function(){
           status();
        });

        $('.btn-simpan').click(function(){
          //alert($('#kdaks').val());
            if ($("#modalform").valid()) {
                addDetail();
            }
        });

        $('.btn-update').click(function(){
            if ($("#modalform").valid()) {
                UpdateDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){
            if ($("#myform").valid()) {
              submit();
            }
        });
    });

    function autoNum(){
        $('#total').autoNumeric('init',{ 	aSep: '.' , aDec: ',' });
        $("#harga").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#qty").autoNumeric('init',{ 	aSep: '.' , aDec: ','  , mDec:'0'});
        $("#diskon").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#biaya_lain").autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $("#total_net").autoNumeric('init',{ 	aSep: '.' , aDec: ',' });
        $('#dpp').autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $('#ppn').autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
        $('#totppn').autoNumeric('init',{ 	aSep: '.' , aDec: ','  });
    }

    function empty(){
      $('#total').val('');
      $('#total_net').val('');
      $('#dpp').val('');
      $('#ppn').val('');
      $('#totppn').val('');
    }

    function status(){
        tot = $("#total").autoNumeric('get');
      //  tot = $("#total").val('');
        disc = $('#diskon').autoNumeric('get');
        bylain = $('#biaya_lain').autoNumeric('get');
          var tglaksi = $('#tglaksi').val();
          var tglaksi = tglaksi.substring(6,10)+tglaksi.substring(3,5)+tglaksi.substring(0,2); 
        total =   parseFloat(tot) - parseFloat(disc) + parseFloat(bylain);
        if (!isNaN(total)){
            $('#total_net').autoNumeric('set',total);
        }

        statppn = $("#statppn").val();
        if (statppn=='I'){
            if(tglaksi>'20220331'){
                dpp = parseFloat(total/1.11);
                ppn = parseFloat(dpp*11/100);
            } else {
                dpp = parseFloat(total/1.1);
                ppn = parseFloat(dpp*10/100);
            }
            total = parseFloat(dpp) + parseFloat(ppn);
        }else if (statppn=='E'){
            dpp = parseFloat(total);
            if(tglaksi>'20220331'){ 
                ppn = parseFloat(dpp*11/100);
            } else {
                ppn = parseFloat(dpp*10/100);
            }
            total = parseFloat(dpp) + parseFloat(ppn);
        } else {
            dpp = parseFloat(total);
            ppn = 0;
            total = parseFloat(dpp) - parseFloat(ppn);
        }

        $('#dpp').autoNumeric('set',dpp);
        $('#ppn').autoNumeric('set',ppn);
        $('#totppn').autoNumeric('set',total);
    }

    function getKodeAksi(){
       //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("aksi/getKodeAksi");?>",
            data: {},
            beforeSend: function() {
                $('#kdaks').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Barang --'));
                $("#kdaks").trigger("change.chosen");
                if ($('#kdaks').hasClass("chosen-hidden-accessible")) {
                    $('#kdaks').select2('destroy');
                    $("#kdaks").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdaks')
                        .append($('<option>', { value : value.kdaks })
                        .html("<b style='font-size: 14px;'>" + value.nmaks + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmaks + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function clear(){
        $("#kdaks").val('');
        $("#harga").val('');
        $("#qty").val('');
    }

    function edit(kdaks,nmaks,qty,harga,nourut){
        $("#nourut").val(nourut);

        $("#kdaks").val(kdaks);
        //$("#kdakun").val(nmakun);
        $("#kdaks").trigger("change");
        $('#kdaks').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });
        //$("#kdaks").val(kdaks).html("<b>"+nmaks+"</b>");
        //$("#kdaks").val(kdaks);
        /*$("#kdaks").select2({
            data: {
              id : kdaks,
                text : "nmaks"
            }
          });
*/
        $("#qty").autoNumeric('set',qty);
        $("#harga").autoNumeric('set',harga);

        $('#modal_transaksi').modal('toggle');
    }


    function addDetail(){

        var noaksi = $("#noaksi").val();
        var tglaksi = $("#tglaksi").val();
        var kdsup = $("#kdsup").val();
        var ket = $("#ket").val();
        var nourut = $("#nourut").val();
        var kdaks = $("#kdaks").val();
        var qty = $("#qty").autoNumeric('get');
        var statppn = $("#statppn").val();
        if ($("#harga").autoNumeric('get')==""){
          var harga = '0';
        } else {
          var harga = $("#harga").autoNumeric('get');
        }
        if ($("#diskon").autoNumeric('get')==""){
          var diskon = '0';
        } else {
          var diskon = $("#diskon").autoNumeric('get');
        }
        if ($("#biaya_lain").autoNumeric('get')==""){
          var biaya_lain = '0';
        } else {
          var biaya_lain = $("#biaya_lain").autoNumeric('get');
        }
        if ($("#stathrg").prop("checked")){
          var ffinal = true;
        } else {
          var ffinal = false;
        }
        $.ajax({
            type: "POST",
            url: "<?=site_url("aksi/addDetail");?>",
            data: {"noaksi":noaksi
                    ,"tglaksi":tglaksi
                    ,"kdsup":kdsup
                    ,"ffinal":ffinal
                    ,"disc":diskon
                    ,"bylain":biaya_lain
                    ,"sppn":statppn
                    ,"ket":ket
                    ,"nourut":nourut
                    ,"kdaks":kdaks
                    ,"qty":qty
                    ,"harga":harga },
            success: function(resp){
                $("#modal_transaksi").modal("hide");
                refresh();
                clear();

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $("#noaksi").val(data.noaksi);
                    if (data.tipe==="success"){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    }else{
                        //refresh();
                    }
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(noaksi,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("aksi/detaildeleted");?>",
                data: {"noaksi":noaksi ,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('aksi');?>';
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var noaksi = $("#noaksi").val();
            var tglaksi = $("#tglaksi").val();
            var kdsup = $("#kdsup").val();
            if ($("#stathrg").prop("checked")){
              var ffinal = true;
            } else {
              var ffinal = false;
            }
            var disc = $("#diskon").autoNumeric('get');
            var bylain = $("#biaya_lain").autoNumeric('get');
            var sppn = $("#statppn").val();
            var ket = $("#ket").val();
            $.ajax({
                type: "POST",
                url: "<?=site_url("aksi/submit");?>",
                data: {"noaksi":noaksi
                    ,"tglaksi":tglaksi
                    ,"kdsup":kdsup
                    ,"ffinal":ffinal
                    ,"disc":disc
                    ,"bylain":bylain
                    ,"sppn":sppn
                    ,"ket":ket  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('aksi');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>

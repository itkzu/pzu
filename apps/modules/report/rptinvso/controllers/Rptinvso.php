<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptinvso
 *
 * @author adi
 */
class Rptinvso extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptinvso/submit'),
            'add' => site_url('rptinvso/add'),
            'edit' => site_url('rptinvso/edit'),
            'reload' => site_url('rptinvso'),
            'ctk_inv'      => site_url('rptinvso/ctk_inv'),
            // 'down_pdf'      => site_url('rptinvso/down_pdf'),
        );
        $this->load->model('rptinvso_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function submit() {
        $this->data['rptakun'] = $this->rptinvso_qry->submit();

        if(empty($this->data['rptakun'])){
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('debug/err_000',$this->data);
        }else{
            $array = $this->input->post();
            if($array['submit']){
                $this->template
                    ->title($this->data['msg_main'],$this->apps->name)
                    ->set_layout('print-layout')
                    ->build('html',$this->data);
            }else{
                redirect("rptbklrl");
            }
        }
    }
    
    public function ctk_inv() {  
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->rptinvso_qry->ctk_inv($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);  
    }  
    
    public function mlt_ctk_inv() {  
        $nodo_awal = $this->uri->segment(3); 
        $nodo_akhir = $this->uri->segment(4); 

        $this->data['data']  = $this->rptinvso_qry->mlt_ctk_inv($nodo_awal,$nodo_akhir); 

        // $jml = $this->rptinvso_qry->get_jml($nodo_awal,$nodo_akhir);
        // $this->data2['jml'] = $jml; 

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html2',$this->data);  

        // $this->template
        //     ->title($this->data['msg_main'],$this->apps->name)
        //     ->set_layout('print-layout')
        //     ->build('html2',$this->data2);
    }  

    public function down_pdf() { 
        $nodo = $this->input->post('nodo'); 

        $this->load->library('dompdf_lib');

        $this->dompdf_lib->filename = "Invoice_Penjualan.pdf";
        $customPaper = array(0,0,360,360);

        $this->data['data'] = $this->rptinvso_qry->ctk_inv($nodo);
        // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
        //
        $this->dompdf_lib->load_view('pdf1', $this->data );
        // $this->dompdf_lib->load_view('ctk_stk', $this->data );
    }

    public function mlt_down_pdf() { 
        $nodo_awal = $this->input->post('nodo_awal'); 
        $nodo_akhir = $this->input->post('nodo_akhir'); 

        $this->load->library('dompdf_lib');

        $this->dompdf_lib->filename = "MLT_Invoice_Penjualan.pdf";
        $customPaper = array(0,0,360,360);

        $this->data['data'] = $this->rptinvso_qry->mlt_ctk_inv($nodo_awal,$nodo_akhir);
        // $this->sum['sum'] = $this->rptinvso_qry->get_jml($nodo_awal,$nodo_akhir);
        //
        $this->dompdf_lib->load_view('pdf2', $this->data );
        // $this->dompdf_lib->load_view('pdf2', $this->sum );
    }
    
    public function getNama() {
        echo $this->rptinvso_qry->getNama();
    }
    
    public function getData() {
        echo $this->rptinvso_qry->getData();
    }

    public function getid() {
        echo $this->rptinvso_qry->getid();
    }
    
    public function json_dgview() {
        echo $this->rptinvso_qry->json_dgview();
    }
    
    private function _init_add(){
        $this->data['form'] = array(
            //baris 1
            'nama'=> array(
                    'placeholder' => 'Masukkan Kata Kunci <small>(No. DO / No. SPK / Nama Customer)</small>',
                    'attr'        => array(
                        'id'    => 'nama',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'value'    => set_value('nama'),
                    'name'     => 'nama',
            ),
            'nodo'=> array(
                    'placeholder' => 'No. DO',
                    'id'          => 'nodo',
                    'name'        => 'nodo',
                    'value'       => set_value('nodo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tgldo'=> array(
                    'placeholder' => 'Tgl DO',
                    'id'          => 'tgldo',
                    'name'        => 'tgldo',
                    'value'       => set_value('tgldo'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'noso'=> array(
                    'placeholder' => 'No. SPK',
                    'id'          => 'noso',
                    'name'        => 'noso',
                    'value'       => set_value('noso'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'tglso'=> array(
                    'placeholder' => 'Tgl SPK',
                    'id'          => 'tglso',
                    'name'        => 'tglso',
                    'value'       => set_value('tglso'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nmcust'=> array(
                    'placeholder' => 'Nama Customer',
                    'id'          => 'nmcust',
                    'name'        => 'nmcust',
                    'value'       => set_value('nmcust'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ), 
            'alamat'=> array(
                    'placeholder' => 'Alamat',
                    'id'          => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'resize: vertical;height: 50px; min-height: 50px;background-color: #fff;',
            ),
            'kota'=> array(
                    'placeholder' => 'Kota',
                    'id'          => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ), 
            'nohp'=> array(
                    'placeholder' => 'No. HP',
                    'id'          => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ), 
            'noktp'=> array(
                    'placeholder' => 'No KTP',
                    'id'          => 'noktp',
                    'name'        => 'noktp',
                    'value'       => set_value('noktp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'npwp'=> array(
                    'placeholder' => 'NPWP',
                    'id'          => 'npwp',
                    'name'        => 'npwp',
                    'value'       => set_value('npwp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'value'       => set_value('ket'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nosin'=> array(
                    'placeholder' => 'No. Mesin',
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nora'=> array(
                    'placeholder' => 'No. Rangka',
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna',
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'value'       => set_value('warna'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'thn'=> array(
                    'placeholder' => 'Tahun',
                    'id'          => 'thn',
                    'name'        => 'thn',
                    'value'       => set_value('thn'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'kdtipe'=> array(
                    'placeholder' => 'Kode Tipe',
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'value'       => set_value('kdtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ),
            'nmtipe'=> array(
                    'placeholder' => 'Nama Tipe',
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'value'       => set_value('nmtipe'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ), 
            'status'=> array(
                    'placeholder' => 'Status Pembelian',
                    'id'          => 'status',
                    'name'        => 'status',
                    'value'       => set_value('status'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;',
            ), 
            'hrg'=> array(
                    'placeholder' => 'Harga Net',
                    'id'          => 'hrg',
                    'name'        => 'hrg',
                    'value'       => set_value('hrg'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;text-align: right',
            ), 
            'disc'=> array(
                    'placeholder' => 'Discount',
                    'id'          => 'disc',
                    'name'        => 'disc',
                    'value'       => set_value('disc'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;text-align: right',
            ), 
            'dpp'=> array(
                    'placeholder' => 'DPP',
                    'id'          => 'dpp',
                    'name'        => 'dpp',
                    'value'       => set_value('dpp'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;text-align: right',
            ), 
            'ppn'=> array(
                    'placeholder' => 'PPN',
                    'id'          => 'ppn',
                    'name'        => 'ppn',
                    'value'       => set_value('ppn'),
                    'class'       => 'form-control',
                    'readonly'    => '',
                    'style'       => 'background-color: #fff;text-align: right',
            ), 

            // baris 2

            'nodo_awal'=> array(
                    'placeholder' => 'Pilih No. DO awal',
                    'id'          => 'nodo_awal',
                    'name'        => 'nodo_awal',
                    'value'       => set_value('nodo_awal'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    // 'style'       => 'background-color: #fff;',
            ),
            'nodo_akhir'=> array(
                    'placeholder' => 'Pilih No. DO Akhir',
                    'id'          => 'nodo_akhir',
                    'name'        => 'nodo_akhir',
                    'value'       => set_value('nodo_akhir'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    // 'style'       => 'background-color: #fff;',
            ),
        );
    }
}

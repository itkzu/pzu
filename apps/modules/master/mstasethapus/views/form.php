<?php
/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>   
<style type="text/css">
    .form-control{
        text-transform: uppercase;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'data-validate' => 'parsley');
            echo form_open($submit, $attributes);
            ?> 
            <div class="box-body">
                <div class="form-group">
                    <?php
                    echo form_label($form['kode']['placeholder']);
                    echo form_input($form['kode']);
                    echo form_error('kode', '<div class="note">', '</div>');
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label($form['tglhapus']['placeholder']);
                    echo form_input($form['tglhapus']);
                    echo form_error('tglhapus', '<div class="note">', '</div>');
                    ?>
                </div> 
                <div class="form-group">
                    <?php
                    echo form_label($form['catatan']['placeholder']);
                    echo form_textarea($form['catatan']);
                    echo form_error('catatan', '<div class="note">', '</div>');
                    ?>
                </div>                    

                <button type="button" class="btn btn-success btn-sm btn-tambah-aset">
                    <i class="fa fa-plus-square"></i>
                    Tambah Aset
                </button>  

                <div style="padding-top: 15px;">
                    <div class="table-responsive">
                        <table class="table table-hover dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 10px;text-align: center;">
                                        No.
                                    </th>
                                    <th style="width: 120px;text-align: center;">
                                        Kode Aset
                                    </th>
                                    <th style="text-align: center;">
                                        Nama Aset
                                    </th>
                                    <th style="text-align: center;">
                                        Keterangan Aset
                                    </th>
                                    <th style="width: 140px;text-align: center;">
                                        Tanggal Terima
                                    </th>
                                    <th style="width: 260px;text-align: center;">
                                        Alasan Hapus
                                    </th>
                                    <th style="width: 10px;text-align: center;">
                                        Hapus
                                    </th>
                                    <th style="width: 120px;text-align: center;">
                                        ID Aset
                                    </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="button" class="btn btn-primary btn-proses">
                    Proses
                </button>
                <a href="<?php echo $reload; ?>" class="btn btn-default">
                    Batal
                </a>    
            </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-form" role="dialog" aria-labelledby="modalform">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pilih Aset</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <strong>Tips</strong>
                            <p>
                                Pencarian aset bisa dilakukan dengan menggunakan kata kunci :
                                <br>
                                <b>Kode Aset, Nama Aset, Tgl Beli (dd mm yyyy), dan Keterangan Aset </b>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <?php
                            echo form_label('Pilih Aset');
                            echo form_dropdown($form['kdaset']['name']
                                    , $form['kdaset']['data']
                                    , $form['kdaset']['value']
                                    , $form['kdaset']['attr']);
                            echo form_error('kdaset', '<div class="note">', '</div>');
                            ?>
                        </div> 
                    </div>
                    <div class="col-lg-12">

                        <div class="form-group">
                            <?php
                            echo form_label($form['alasan']['placeholder']);
                            echo form_textarea($form['alasan']);
                            echo form_error('alasan', '<div class="note">', '</div>');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary btn-simpan-aset">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var table = $('.dataTable').DataTable({
            "columns": [
                {"data": null},
                {"data": "kdaset"},
                {"data": "nmaset"},
                {"data": "ket_aset"},
                {"data": "tglbeli"},
                {"data": "alasan"},
                {"data": "hapus"},
                {"data": "kdasetx"}
            ],
            "paging": false,
            "bProcessing": false,
            "bServerSide": false,
            "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "targets": [7],
                    "visible": false
                }
            ],
            "order": [[1, 'asc']],
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
        $('.dataTable tbody').on('click', 'button.btn-delete', function () {
            table
                    .row($(this).parents('tr'))
                    .remove()
                    .draw();
        });

        $('#modal-form').on('shown.bs.modal', function (e) {
            var tbl_dt = table.rows().data();
            var arr_aset = [];
            $.each(tbl_dt, function (key, data) {
                arr_aset.push(data.kdasetx);
            });
            $('#kdaset').select2({
                placeholder: 'PENCARIAN ASET ...',
                dropdownAutoWidth: false,
                ajax: {
                    url: "<?= site_url('mstasethapus/get_aset'); ?>",
                    type: 'post',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            q: params.term,
                            arr_aset: arr_aset,
                            page: params.page
                        };
                        return query;
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 0,
                templateResult: formatMenus, // omitted for brevity, see the source of this page
                templateSelection: formatMenusSelection // omitted for brevity, see the source of this page
            });
        });

        $(".btn-proses").click(function () {
            proses();
        });

        $(".btn-tambah-aset").click(function () {
            $("#modal-form").modal({
                "show": true,
                "backdrop": "static"
            });
        });

        $(".btn-simpan-aset").click(function () {
            var tglhapus = $("#tglhapus").val();
            var kdaset = $("#kdaset").val();
            var alasan = $("#alasan").val();
            var url = $("#form_add").attr("action");
            $.ajax({
                type: "POST",
                url: url,
                data: {"tglhapus": tglhapus
                    , "kdaset": kdaset
                    , "alasan": alasan
                },
                success: function (resp) {
                    var obj = jQuery.parseJSON(resp);
                    if (obj.state === "1") {
                        //$("#tglhapus").val('').datepicker("update");
                        $("#kdaset").val(null).trigger('change');
                        $("#alasan").val('');
                        table.rows.add(obj.data).draw();
                        $('#modal-form').modal('hide');
                    } else {
                        swal({
                            title: "Kesalahan !",
                            text: obj.msg,
                            type: "error"
                        }, function () {

                        });
                    }
                },
                error: function (event, textStatus, errorThrown) {
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });

        var kode = $("#kode").val();
        if (kode) {
            get_detail_ahp(kode);
        }
        function proses() {
            var data = table.rows().data();
            if (data.length <= 0) {
                swal({
                    title: "Peringatan!",
                    text: "Data Aset Tidak Boleh Kosong",
                    type: "error"
                }, function () {

                });
                return false;
            } else {
                swal({
                    title: "Konfirmasi Proses Hapus Aset !",
                    text: "Aset yang dihapus tidak dapat dikembalikan!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#c9302c",
                    confirmButtonText: "Ya, Lanjutkan!",
                    cancelButtonText: "Batalkan!",
                    closeOnConfirm: false
                }, function () {

                    var p_kdasetx = [];
                    var p_alasan = [];
                    $.each(data, function (key, data) {
                        p_kdasetx.push(data.kdasetx);
                        p_alasan.push(data.alasan);
                    });
                    var tglhapus = $("#tglhapus").val();
                    var catatan = $("#catatan").val();
                    var kode = $("#kode").val();
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('mstasethapus/proses'); ?>",
                        data: {"kode": kode
                            , "tglhapus": tglhapus
                            , "catatan": catatan
                            , "kdaset": p_kdasetx
                            , "alasan": p_alasan
                        },
                        success: function (resp) {
                            var obj = jQuery.parseJSON(resp);
                            if (obj.state === "1") {
                                var kode = "";
                                $.each(obj.data, function (key, data) {
                                    kode = data.aset_hapus_ins;
                                });
                                $("#kode").val(kode);

                                $("#tglhapus").val('').datepicker("update");
                                $("#kdaset").val(null).trigger('change');
                                $("#alasan").val('');
                                $("#catatan").val('');
                                $("#kode").val('');
                                table.clear().draw();
                                if(kode){
                                    swal({
                                        title: "Berhasil!",
                                        text: obj.msg + " | Kode : " + kode,
                                        type: "success"
                                    }, function () {
                                        window.location.replace('<?= site_url('mstasethapus'); ?>');
                                    });
                                }else{
                                    swal({
                                        title: "Berhasil!",
                                        text: obj.msg + " | Kode : " + kode,
                                        type: "success",
                                        showCancelButton: true,
                                        confirmButtonColor: "#c9302c",
                                        confirmButtonText: "Kembali Ke Daftar",
                                        cancelButtonText: "Hapus Aset Lagi!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.replace('<?= site_url('mstasethapus'); ?>');
                                    });
                                }
                            } else {
                                swal({
                                    title: "Peringatan!",
                                    text: obj.msg,
                                    type: "error"
                                }, function () {

                                });
                            }
                        },
                        error: function (event, textStatus, errorThrown) {
                            console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                        }
                    });
                });
            }
        }

        function get_detail_ahp(kode) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('mstasethapus/get_detail_ahp'); ?>",
                data: {"noashp": kode
                },
                beforeSend: function (resp){
                    table.clear().draw();
                },
                success: function (resp) {
                    var obj = jQuery.parseJSON(resp);
                    console.log(obj);
                    if (obj.state === "1") {
                        table.rows.add(obj.data).draw();
                    } else {
                        swal({
                            title: "Peringatan!",
                            text: obj.msg,
                            type: "error"
                        }, function () {

                        });
                    }
                },
                error: function (event, textStatus, errorThrown) {
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }

        function formatMenus(repo) {
            if (repo.loading)
                return "MENCARI DATA  ... ";


            var markup = "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.text + "</b></div>";

            markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-repository__stargazers' style='font-size: 12px;'>" +
                    "<i class='fa fa-barcode'></i> Kd. Aset : " + repo.kdaset + " | <i class='fa fa-calendar'></i> Tgl Terima : " + repo.tglbeli + " </div>" +
                    "<div class='select2-result-repository__stargazers' style='font-size: 12px;'>" +
                    "<i class='fa fa-comment'></i> Ket : " + repo.ket_aset + " </div>" +
                    "</div>" +
                    "</div></div>";
            return markup;
        }

        function formatMenusSelection(repo) {
            return repo.full_name || repo.text;
        }
    });
</script>
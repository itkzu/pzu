<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptbklsaleday_qry
 *
 * @author adi
 */
class Rptbklsaleday_qry extends CI_Model{
	//put your code here
	protected $res="";
	protected $delete="";
	protected $state="";
	public function __construct() {
		parent::__construct();        
	}
	
	public function json_dgview() {
		error_reporting(-1);
		$dbbengkel = $this->load->database('bengkel', TRUE);

//$conn = new COM("ADODB.Connection") or die("Cannot start ADO"); 
//$connString= "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=e:\\dbmas.mdb";
//$dbbengkel = $conn->Open($connString);

		if( isset($_GET['periode_awal']) ){
			if($_GET['periode_awal']){
				//$tgl1 = explode('-', $_GET['periode_awal']);
				$periode = $_GET['periode_awal'];//$tgl1[1].$tgl1[0];
			}else{
				$periode = '';
			} 
		}else{
			$periode = '';
		}        
		

//echo "<script> console.log('PHP: ". $periode_awal ."');</script>";
//echo "<script> console.log('PHP: qry ". $periode_akhir ."');</script>";

//		$aColumns = array('dt','pk','no_ktp','nm','adr','subtotsrv','subtotitem','subtot','distrs','tottrs');

							//, 'nmtipe'
		$aColumns = array('dt'

							, 'tot_qty'
							, 'tot_dpp_tot'
							, 'tot_dpp_jasa'
							, 'tot_dpp_oli'
							, 'tot_dpp_part'

							, 'srv_qty'
							, 'srv_dpp_tot'
							, 'srv_dpp_jasa'
							, 'srv_dpp_oli'
							, 'srv_dpp_part'

							, 'pnj_qty'
							, 'pnj_dpp_tot'
							, 'pnj_dpp_oli'
							, 'pnj_dpp_part'
		);

		$sIndexColumn = "dt";
		$sLimit = "";
		
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
			$sLimit = " TOP " . $_GET['iDisplayLength'];
		}




		$sTable = "(


SELECT aa.dt

		, sum(aa.qty_srv + aa.qty_pnj) AS tot_qty
		, sum(aa.srv_jasa + aa.srv_oli + aa.srv_part + aa.pnj_oli + aa.pnj_part) AS tot_dpp_tot
		, sum(aa.srv_jasa) AS tot_dpp_jasa
		, sum(aa.srv_oli + aa.pnj_oli) AS tot_dpp_oli
		, sum(aa.srv_part + aa.pnj_part) AS tot_dpp_part

		
		, sum(aa.qty_srv) AS srv_qty
		, sum(aa.srv_jasa + aa.srv_oli + aa.srv_part) AS srv_dpp_tot
		, sum(aa.srv_jasa) AS srv_dpp_jasa
		, sum(aa.srv_oli) AS srv_dpp_oli
		, sum(aa.srv_part) AS srv_dpp_part

		, sum(aa.qty_pnj) AS pnj_qty
		, sum(aa.pnj_oli + aa.pnj_part) AS pnj_dpp_tot
		, sum(aa.pnj_oli) AS pnj_dpp_oli
		, sum(aa.pnj_part) AS pnj_dpp_part

FROM
(

SELECT zz.dt
		
		, IIF(zz.tp='SRV',1,0) AS qty_srv
		, IIF(zz.tp='PNJ',1,0) AS qty_pnj
		
		, sum(zz.srv_jasa) AS srv_jasa
		, sum(zz.srv_oli) AS srv_oli
		, sum(zz.srv_part) AS srv_part

		, sum(zz.pnj_oli) AS pnj_oli
		, sum(zz.pnj_part) AS pnj_part
FROM
(
SELECT	yy.pk
		, yy.dt
		, yy.tp

		, IIF(yy.tp='SRV' AND yy.grp='JASA',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS srv_jasa
		, IIF(yy.tp='SRV' AND yy.grp='OLI',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS srv_oli
		, IIF(yy.tp='SRV' AND yy.grp='PART',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS srv_part
		
		, IIF(yy.tp='PNJ' AND yy.grp='OLI',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS pnj_oli
		, IIF(yy.tp='PNJ' AND yy.grp='PART',IIF(yy.dpp IS NULL,0,yy.dpp),0) AS pnj_part
FROM
(
						SELECT a.pk
							, format(a.dt,'dd/mm/yyyy') AS dt 
							, 'SRV' AS tp
							, 'JASA' as grp

							, IIF(xd.nt = '1' AND val(a.kpbto) = 1, x.qty * c.kpb1 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
								IIF(xd.nt = '1' AND val(a.kpbto) = 2, x.qty * c.kpb2 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
									IIF(xd.nt = '1' AND val(a.kpbto) = 3, x.qty * c.kpb3 * (100 - x.dis) / 100 * (100 - a.distrs) / 100,
										IIF(xd.nt = '1' AND val(a.kpbto) = 4, x.qty * c.kpb4 * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
											round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0)
										)
									)
								)
							) AS dpp
							
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_srv x ON a.pk = x.fk)
							INNER JOIN m_srv xd ON x.srvfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON trim(b.cmn1) = trim(c.kdsin)
						WHERE format(a.dt,'mm-yyyy')='{$periode}'

						UNION ALL

						SELECT a.pk
							, format(a.dt,'dd/mm/yyyy') AS dt 
							, 'SRV' AS tp

							, IIF(xd.grpfk=2, 'OLI', 'PART') AS grp
								
							, IIF(val(a.kpbto) = 1 AND xd.grpfk = 2, 
								x.qty * c.oli * (100 - x.dis) / 100 * (100 - a.distrs) / 100, 
								round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0)
								) AS dpp
								
						FROM 	
							(((t_wi_hd a INNER JOIN m_vc b ON a.vcfk = b.pk)
							INNER JOIN t_wi_dt_item x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk)
							LEFT OUTER JOIN m_kpb c ON b.cmn1 = c.kdsin
						WHERE format(a.dt,'mm-yyyy')='{$periode}'
							
						UNION ALL

						SELECT a.pk
							, format(a.dt,'dd/mm/yyyy') AS dt 
							, 'PNJ' AS tp

							, IIF(xd.grpfk=2, 'OLI', 'PART') AS grp
								
							, round((x.qty * x.sprice * (100 - x.dis) / 100 * (100 - a.distrs) / 100) / 1.1 , 0) AS dpp
							
						FROM 	
							((t_si_hd a INNER JOIN m_cus b ON a.cusfk = b.pk)
							INNER JOIN t_si_dt x ON a.pk = x.fk)
							INNER JOIN m_item xd ON x.itemfk = xd.pk							
						WHERE format(a.dt,'mm-yyyy')='{$periode}'
						
) AS yy

) AS zz
GROUP BY zz.dt, zz.tp, zz.pk

) AS aa
GROUP BY aa.dt
ORDER BY aa.dt

					) AS A ";


		//echo "<script> console.log('PHP: qry ". $sTable ."');</script>";


		/*
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{   
			if($_GET['iDisplayStart']>0){
				$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
						intval( $_GET['iDisplayStart'] );
			}
		}
		*/

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
				$sOrder = " ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
						if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
						{
								$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
										($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
						}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == " ORDER BY" )
				{
						$sOrder = "";
				}
		}
		$sWhere = "";
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "".$aColumns[$i]." LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		}
		
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{   
				if ( $sWhere == "" )
				{
					$sWhere = " WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//echo $sWhere."<br>";
				$sWhere .= "".$aColumns[$i]."  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";    
			}
		}
		

		/*
		 * SQL queries
		 */
		$sQuery = "
				SELECT $sLimit ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM   $sTable
				$sWhere
				";
				
/*                 $sOrder
				$sLimit */
		
//        echo $sQuery;
		
		$rResult = $dbbengkel->query( $sQuery );

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";    //SELECT FOUND_ROWS()
		
		$rResultFilterTotal = $dbbengkel->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

		$sQuery = "
				SELECT COUNT(".$sIndexColumn.") AS jml
				FROM $sTable
				$sWhere";
		$rResultTotal = $dbbengkel->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0]['jml'];

		$this->load->database('bengkel', FALSE);
		
		$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		
		foreach ( $rResult->result_array() as $aRow )
		{
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					//if($aRow[ $aColumns[$i] ]===null){
					//	$aRow[ $aColumns[$i] ] = '';
					//}
					if(is_numeric($aRow[ $aColumns[$i] ])){
						$row[] = round((float) $aRow[ $aColumns[$i] ]);
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
				//23 - 28
				//$row[23] = "<label>".$aRow['disc']."</label>";
			   $output['aaData'][] = $row;
		}
		echo json_encode( $output );  
	}
	
	public function submit(){

	}
}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptbklrl_qry
 *
 * @author adi
 */
class Rptbklrl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function getDataCabang() {
        $this->db->select('*');
    //      $kddiv = $this->input->post('kddiv');
    //      $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_divisi_bkl()");
        return $q->result_array();
      }

    public function getDataUnitCabang() {
        $this->db->select('*');
    //      $kddiv = $this->input->post('kddiv');
    //      $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_div_unit_bkl()");
        return $q->result_array();
      }

    public function getDivisiAll($kddiv = null) {
        if(!$kddiv){
            $kddiv = $this->session->userdata('kddiv');
        }
        $this->db->select("nmdiv,status,kddiv");
        $this->db->where("kddiv LIKE '".$kddiv."%'");
        $this->db->where("nmdiv LIKE '%%'");
        $this->db->where("faktif","true");
        $this->db->order_by("kddiv","ASC");
        $query = $this->db->get("mst.divisi");
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }

    public function getDivisiPos($kddiv = null) {
        if(!$kddiv){
            $kddiv = $this->session->userdata('kddiv');
        }
        $this->db->select("nmdiv,status,kddiv");
        $this->db->where("kddiv LIKE '".$kddiv."%'");
        $this->db->where("nmdiv LIKE '%%'");
        $this->db->where("status","POS");
        $this->db->where("faktif","true");
        $this->db->order_by("kddiv","ASC");
        $query = $this->db->get("mst.divisi");
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return $res;
    }

    function strbulan($m){
        if($m<10){
            $m = "0".$m;
        }
        return $m;
    }


    public function submit() {
        $array = $this->input->post();

        //via menu RL
        if($array){
            $array['periode_akhir'] = $array['tahun']."-".strmonth($array['bulan_akhir']);
            $array['periode_awal'] = $array['tahun']."-".strmonth($array['bulan_awal']);
            if($array['rdjenis']==="unit"){
                $res = $this->rpt_unit($array);
            }elseif($array['rdjenis']==="pos"){
                $res = $this->rpt_pos($array);
            }elseif($array['rdjenis']==="konsol"){
                $res = $this->rpt_konsol($array);
            }else{
                $res = "";
            }
            return $res;

        }

        //via cURL
        else{

            $dt = array();
            foreach (getallheaders() as $name => $value) {
                $dt[$name] = $value;
                //echo "$name: $value\n";
            }

            //$array['rdjenis'] = $dt['key'];
            $array['kddiv2'] = $dt['kddiv2'];
            $array['kddiv'] = $dt['kddiv'];
            $array['periode_akhir'] = $dt['tahun']."-".strmonth($dt['bulan_akhir']);
            $array['periode_awal'] = $dt['tahun']."-".strmonth($dt['bulan_awal']);

            if ($dt['key'] == 'pzu'){
                $res = $this->rpt_konsol($array);
            } else{
                $res = "";
            }
            return json_encode($res);
        }
    }

    private function rpt_unit($array) {
        $str_period = "select glr.get_periode_gl('".$array['kddiv2']."') as period";
        $r_period = $this->db->query($str_period);
        if($r_period->num_rows()>0){
            $rw_period = $r_period->result_array();
            $period = $rw_period[0]['period'];
        }
        if($period<str_replace("-", "", $array['periode_akhir'])){
            $array['periode_akhir'] = $period;
        }
        if($period===str_replace("-", "", $array['periode_akhir'])){
            // Jika periode aktif sama dengan periode yg dipilih user maka ambil dari scheme yg berjalan (aktif)
            $str_calc = "SELECT glr.labarugi_calc ('".$array['kddiv2']."') as rl_calc";
            $this->db->query($str_calc);

            $str = "SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                           kdakun, nmakun, dk, nominal_akun, periode
					FROM
                           ( SELECT * FROM 
                                (SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                                    kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi
                                    WHERE kddiv ='".$array['kddiv2']."'
                                     AND periode >= '".str_replace("-", "", $array['periode_awal'])."'
                                     and nominal <> 0
                                     and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0))
                                     and level between 1 and 2
                                     UNION ALL
                                 SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                                    kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi
                                    WHERE kddiv ='".$array['kddiv2']."'
                                     AND periode >= '".str_replace("-", "", $array['periode_awal'])."'
                                     and nominal <> 0
                                     and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0))
                                    and level = 3 and kdakun is not null ) AS x
                               GROUP BY x.kddiv,x. kdheader, x.nmheader, x.kategori, x.jenis, x.level, x.nominal, x.tipeheader, x.kdakun, x.nmakun, x.dk, x.nominal_akun, x.periode 
                        union
                     SELECT * FROM 
                        (SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, '".str_replace("-", "", $array['periode_akhir'])."' AS periode
                           FROM glr.vl_labarugi_bkl WHERE kddiv ='".$array['kddiv2']."'
                             and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
                             or (nominal = 0 and nominal_akun <> 0))
                             and level between 1 and 2 
                             UNION ALL
                        SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, '".str_replace("-", "", $array['periode_akhir'])."' AS periode
                           FROM glr.vl_labarugi_bkl WHERE kddiv ='".$array['kddiv2']."'
                             and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
                             or (nominal = 0 and nominal_akun <> 0))
                             and level = 3 and kdakun is not null ) AS y 
                     GROUP BY y.kddiv, y.kdheader, y.nmheader, y.kategori, y.jenis, y.level, y.nominal, y.tipeheader,
                            y.kdakun, y.nmakun, y.dk, y.nominal_akun, y.periode) AS RL";

            $q_periode = $this->db->query("SELECT periode FROM (".$str.") x
                                GROUP BY periode
                                ORDER BY periode ASC");
            $data_periode = $q_periode->result_array();
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", SUM(CASE WHEN periode = '".$value['periode']."' AND nominal_akun IS NOT NULL THEN nominal_akun "
                        . " WHEN periode = '".$value['periode']."' AND nominal_akun IS NULL THEN nominal "
                        . " ELSE 0 END) AS \"" . $value['periode']."\"";
            }
            $q = $this->db->query("SELECT kdakun,level,CASE WHEN kdakun IS NULL THEN nmheader ELSE nmakun END AS nmakun "
                                    .$str_periode_nominal." FROM (".$str.") AS akn"
                                    . " GROUP BY kddiv, kdheader, nmheader, kategori, jenis, LEVEL, tipeheader, kdakun, nmakun, dk"
                                    . " ORDER BY kdheader, CASE WHEN kdakun IS NULL THEN '1' ELSE kdakun END");
        }else{
            // Jika periode aktif tidak sama dengan periode yg dipilih user maka ambil dari scheme history
            $str = "SELECT * FROM 
                         ( SELECT * FROM 
                                (SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                                    kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi
                                    WHERE kddiv ='".$array['kddiv2']."'
                                     AND ( periode BETWEEN '".str_replace("-", "", $array['periode_awal'])."' AND '".str_replace("-", "", $array['periode_akhir'])."' )
                                     and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
                                     or (nominal = 0 and nominal_akun <> 0))
                                     and level between 1 and 2
                                     UNION ALL
                                 SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                                    kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi
                                    WHERE kddiv ='".$array['kddiv2']."'
                                     AND ( periode BETWEEN '".str_replace("-", "", $array['periode_awal'])."' AND '".str_replace("-", "", $array['periode_akhir'])."' )
                                     and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
                                     or (nominal = 0 and nominal_akun <> 0))
                                    and level = 3 and kdakun is not null ) AS x
                               GROUP BY x.kddiv,x. kdheader, x.nmheader, x.kategori, x.jenis, x.level, x.nominal, x.tipeheader, x.kdakun, x.nmakun, x.dk, x.nominal_akun, x.periode) as y
                               ORDER BY y.periode DESC, CASE WHEN y.kdakun IS NULL THEN '1' ELSE y.kdakun END";

            // SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
            //                 kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi  WHERE kddiv ='".$array['kddiv2']."'
            //                  AND ( periode BETWEEN '".str_replace("-", "", $array['periode_awal'])."' AND '".str_replace("-", "", $array['periode_akhir'])."' )
            //                  and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
            //                  or (nominal = 0 and nominal_akun <> 0))
            //          GROUP BY kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
            //                 kdakun, nmakun, dk, nominal_akun, periode
            //          ORDER BY periode DESC, CASE WHEN kdakun IS NULL THEN '1' ELSE kdakun END";

            $q_periode = $this->db->query("SELECT periode FROM (".$str.") x
                                GROUP BY periode
                                ORDER BY periode ASC");
            $data_periode = $q_periode->result_array();
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", SUM(CASE WHEN periode = '".$value['periode']."' AND nominal_akun IS NOT NULL THEN nominal_akun "
                        . " WHEN periode = '".$value['periode']."' AND nominal_akun IS NULL THEN nominal "
                        . " ELSE 0 END) AS \"" . $value['periode']."\"";
            }

            $q = $this->db->query("SELECT kdakun,level,CASE WHEN kdakun IS NULL THEN nmheader ELSE nmakun END AS nmakun "
                                    .$str_periode_nominal." FROM (".$str.") AS akn"
                                    . " GROUP BY kddiv, kdheader, nmheader, kategori, jenis, LEVEL, tipeheader, kdakun, nmakun, dk"
                                    . " ORDER BY kdheader, CASE WHEN kdakun IS NULL THEN '1' ELSE kdakun END");
        }
        // echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->result_array();
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => $this->getDivisiAll($array['kddiv2']),
              'periode_aktif' => $period,
            );
            return $res;
        }else{
            return false;
        }
    }

    private function rpt_pos($array) {
        $str_period = "select glr.get_periode_gl('".$array['kddiv']."') as period";
        $r_period = $this->db->query($str_period);
        if($r_period->num_rows()>0){
            $rw_period = $r_period->result_array();
            $period = $rw_period[0]['period'];
        }
        if($period<str_replace("-", "", $array['periode_akhir'])){
            $array['periode_akhir'] = $period;
        }
        if($period===str_replace("-", "", $array['periode_akhir'])){
            // Jika periode aktif sama dengan periode yg dipilih user maka ambil dari scheme yg berjalan (aktif)
            $str_calc = "SELECT glr.labarugi_calc ('".$array['kddiv']."') as rl_calc";
            $this->db->query($str_calc);

            $str = "SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                           kdakun, nmakun, dk, nominal_akun, periode
					FROM
                           (
                        SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi
                            WHERE kddiv='".$array['kddiv']."'
                             AND periode >= '".str_replace("-", "", $array['periode_awal'])."'
                             and nominal <> 0
                             and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0))
                     GROUP BY kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, periode
                     union
                     SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, '".str_replace("-", "", $array['periode_akhir'])."' AS periode
                           FROM glr.vl_labarugi_bkl WHERE kddiv='".$array['kddiv']."'
                             and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
                             or (nominal = 0 and nominal_akun <> 0))
                     GROUP BY kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun) AS RL";

            $q_periode = $this->db->query("SELECT periode FROM (".$str.") x
                                GROUP BY periode
                                ORDER BY periode ASC");
            $data_periode = $q_periode->result_array();
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", SUM(CASE WHEN periode = '".$value['periode']."' AND nominal_akun IS NOT NULL THEN nominal_akun "
                        . " WHEN periode = '".$value['periode']."' AND nominal_akun IS NULL THEN nominal "
                        . " ELSE 0 END) AS \"" . $value['periode']."\"";
            }
            $q = $this->db->query("SELECT kdakun,level,CASE WHEN kdakun IS NULL THEN nmheader ELSE nmakun END AS nmakun "
                                    .$str_periode_nominal." FROM (".$str.") AS akn"
                                    . " GROUP BY kddiv, kdheader, nmheader, kategori, jenis, LEVEL, tipeheader, kdakun, nmakun, dk"
                                    . " ORDER BY kdheader, CASE WHEN kdakun IS NULL THEN '1' ELSE kdakun END");
        }else{
            // Jika periode aktif tidak sama dengan periode yg dipilih user maka ambil dari scheme history
            $str = "SELECT kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, periode FROM glr_h.vl_labarugi  WHERE kddiv='".$array['kddiv']."'
                             AND ( periode BETWEEN '".str_replace("-", "", $array['periode_awal'])."' AND '".str_replace("-", "", $array['periode_akhir'])."' )
                             and ((nominal <> 0 and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0)))
                             or (nominal = 0 and nominal_akun <> 0))
                     GROUP BY kddiv, kdheader, nmheader, kategori, jenis, level, nominal, tipeheader,
                            kdakun, nmakun, dk, nominal_akun, periode
                     ORDER BY periode DESC, CASE WHEN kdakun IS NULL THEN '1' ELSE kdakun END";

            $q_periode = $this->db->query("SELECT periode FROM (".$str.") x
                                GROUP BY periode
                                ORDER BY periode ASC");
            $data_periode = $q_periode->result_array();
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", SUM(CASE WHEN periode = '".$value['periode']."' AND nominal_akun IS NOT NULL THEN nominal_akun "
                        . " WHEN periode = '".$value['periode']."' AND nominal_akun IS NULL THEN nominal "
                        . " ELSE 0 END) AS \"" . $value['periode']."\"";
            }

            $q = $this->db->query("SELECT kdakun,level,CASE WHEN kdakun IS NULL THEN nmheader ELSE nmakun END AS nmakun "
                                    .$str_periode_nominal." FROM (".$str.") AS akn"
                                    . " GROUP BY kddiv, kdheader, nmheader, kategori, jenis, LEVEL, tipeheader, kdakun, nmakun, dk"
                                    . " ORDER BY kdheader, CASE WHEN kdakun IS NULL THEN '1' ELSE kdakun END");
        }
        // echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->result_array();
            $data_periode = $q_periode->result_array();
            $res = array(
              'data' => $data,
              'div' => $this->getDivisiAll($array['kddiv']),
              'periode_aktif' => $period,
            );
            return $res;
        }else{
            return false;
        }
    }

    private function rpt_konsol($array) {

        if (!isset($array['kddiv'])){
            $array['kddiv'] = $this->session->userdata('kddiv');
        }

        //echo "<script> console.log('PHP: qry". json_encode($array) ."');</script>";


        $str_period = "select glr.get_periode_gl('".$array['kddiv']."') as period";
        $r_period = $this->db->query($str_period);
        if($r_period->num_rows()>0){
            $rw_period = $r_period->result_array();
            $period = $rw_period[0]['period'];
        }
        if($period<str_replace("-", "", $array['periode_akhir'])){
            $array['periode_akhir'] = $period;
        }
        if($period===str_replace("-", "", $array['periode_akhir'])){
            // Jika periode aktif sama dengan periode yg dipilih user maka ambil dari scheme yg berjalan (aktif)
            $str_calc = "SELECT glr.labarugi_calc ('".$array['kddiv']."') as rl_calc";
            $this->db->query($str_calc);

            $str = "SELECT kdheader,nmheader,level,nominal_akun,kdakun,periode FROM (
                    SELECT kdheader,nmheader,level,nominal as nominal_akun,kdakun, periode
                        FROM glr_h.vl_labarugi
                            WHERE kddiv like '%B%'
                             AND periode >= '".str_replace("-", "", $array['periode_awal'])."'
                             and nominal <> 0
                                and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0))
                                and kdakun is null
                     GROUP BY kdheader,nmheader,level,nominal,kdakun, periode
                     union
                     SELECT kdheader,nmheader,level,nominal as nominal_akun,kdakun
                            , '".str_replace("-", "", $array['periode_akhir'])."' AS periode
                        FROM glr.vl_labarugi_bkl WHERE kddiv like '%B%'
                             and nominal <> 0
                                and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0))
                                and kdakun is null
                     GROUP BY kdheader,nmheader,level,nominal,kdakun, periode) AS RL";

            $q_periode = $this->db->query("SELECT periode FROM (".$str.") x
                                GROUP BY periode
                                ORDER BY periode ASC");
            $data_periode = $q_periode->result_array();
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", SUM(CASE WHEN periode = '".$value['periode']."' THEN nominal_akun "
                        . " ELSE 0 END) AS \"" . $value['periode']."\"";
            }
            $q = $this->db->query("SELECT kdheader as kdakun,level,nmheader AS nmakun "
                                    .$str_periode_nominal." FROM (".$str.") AS akn"
                                    . " GROUP BY kdheader, nmheader, LEVEL"
                                    . " ORDER BY kdheader");
        }else{
            // Jika periode aktif tidak sama dengan periode yg dipilih user maka ambil dari scheme history
            $str = "SELECT kdheader,nmheader,level,nominal as nominal_akun,kdakun, periode
                        FROM glr_h.vl_labarugi
                            WHERE kddiv like '%B'
                             AND ( periode BETWEEN '".str_replace("-", "", $array['periode_awal'])."' AND '".str_replace("-", "", $array['periode_akhir'])."' )
                             and nominal <> 0
                                and (nominal_akun is null or (nominal_akun is not null and nominal_akun <> 0))
                                and kdakun is null
                     GROUP BY kdheader,nmheader,level,nominal,kdakun, periode";

            $q_periode = $this->db->query("SELECT periode FROM (".$str.") x
                                GROUP BY periode
                                ORDER BY periode ASC");
            $data_periode = $q_periode->result_array();
            $str_periode_nominal = "";
            foreach ($data_periode as $value) {
                $str_periode_nominal.=", SUM(CASE WHEN periode = '".$value['periode']."' THEN nominal_akun "
                        . " ELSE 0 END) AS \"" . $value['periode']."\"";
            }

            $q = $this->db->query("SELECT kdheader as kdakun,level,nmheader AS nmakun "
                                    .$str_periode_nominal." FROM (".$str.") AS akn"
                                    . " GROUP BY kdheader, nmheader, LEVEL"
                                    . " ORDER BY kdheader");
        }
        // echo  $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->result_array();
            $data_periode = $q_periode->result_array();
            $div = "KONSOL BENGKEL ALL CABANG";
            $res = array(
              'data' => $data,
              'div' => $div,//$this->getDivisiAll($array['kddiv']),
              'periode_aktif' => $period,
            );
            return $res;
        }else{
            return false;
        }
    }
}

<style type="text/css">
    .nopadding {
        padding: 0 !important;
        margin: 0 !important;
    }
    .box{
        border-radius: 0px;
    }
</style>
<!-- Main row -->
<div class="row">
    <div class="col-lg-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12 animated bounce">
                        <label class="label label-primary">Klik Pada Grafik Untuk Melihat Detail</label>
                    </div>
                    <div class="col-lg-12">
                        <canvas id="pieBarJualPertahun" style="height:200px;"></canvas>
                    </div>                    
                    <div class="col-lg-12 chartBarJualPerbulan">
                        <canvas id="BarJualPerbulan" style="height: 300px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4">
        <div class="form-group">
            <?php 
                echo form_label($form['periode_awal']['placeholder']);
                echo form_input($form['periode_awal']);
                echo form_error('periode_awal','<div class="note">','</div>'); 
            ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?php 
                echo form_label($form['periode_akhir']['placeholder']);
                echo form_input($form['periode_akhir']);
                echo form_error('periode_akhir','<div class="note">','</div>'); 
            ?>
        </div>
    </div>
    <div class="col-lg-4" style="margin-bottom: 10px;">
        <button type="button" class="btn btn-info btn-preview" style="margin-top: 25px;" >Tampilkan</button>
    </div>
    
    <div class="col-lg-6 nopadding">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-motorcycle"></i> Penjualan Top 10 Unit Per Periode</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                    <div class="col-lg-12 animated bounce">
                        <label class="label label-primary">Klik Pada Grafik Untuk Melihat Detail</label>
                    </div>
                    <div class="col-lg-12">
                        <canvas id="pieChart" style="height:100px;"></canvas>
                    </div>
                    <div class="col-lg-12 text-center">
                        <h4><label class="label label-danger total-header-unit">Total Unit : </label></h4>
                    </div>
                    <div class="col-lg-12 detail-unit">
                        <hr>
                        <canvas id="barChartDetailUnit" style="height:100px;"></canvas>
                    </div>
              </div>
            </div>
            <div class="overlay">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
    <div class="col-lg-6 nopadding">
        <div class="box box-danger box-solid">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-users"></i> Penjualan Top 10 Sales Per Periode</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
                <div class="col-lg-12 animated bounce">
                    <label class="label label-primary">Klik Pada Grafik Untuk Melihat Detail</label>
                </div>
                <div class="col-lg-12 header-sales">
                    <canvas id="pieChartHeaderSales" style="height:100px;"></canvas>
                </div>
                <div class="col-lg-12 text-center">
                    <h4><label class="label label-danger total-header-sales">Total Unit : </label></h4>
                </div>
                <div class="col-lg-12 sales">
                    <hr>
                    <canvas id="pieChartSales" style="height:100px;"></canvas>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h4><label class="label label-danger total-sales">Total Unit : </label></h4>
                        </div>    
                    </div>
                </div>
                <div class="col-lg-12 detail-sales">
                    <hr>
                    <canvas id="barChartDetailSales" style="height:100px;"></canvas>
                </div>
            </div>
          </div>
          <div class="overlay">
            <i class="fa fa-refresh fa-spin"></i>
          </div>
          <!-- /.box-body -->
          </div>
    </div>
</div>
<!-- /.row (main row) -->

<!-- ChartJS -->
<script src="<?=base_url('assets/plugins/chartjs/Chart.bundle.js');?>"></script>
<script src="<?=base_url('assets/plugins/chartjs/utils.js');?>"></script>
<!-- FastClick -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<script>
$(function () {
    $('.overlay').hide();
    getJualUnitTahun();
    getJualHeaderSalesTahun();
    getJualPerTahun();
    $(".btn-preview").click(function(){
        getJualUnitTahun();
        getJualHeaderSalesTahun();
    });

//    // Define a plugin to provide data labels
    Chart.plugins.register({
        beforeDraw: function (chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                    var helpers = Chart.helpers;
                    var ctx = chart.chart.ctx;
                    var chartArea = chart.chartArea;

                    ctx.save();
                    ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                    ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                    ctx.restore();
            }
        },
//        afterDatasetsDraw: function(chart, easing) {
//            // To only draw at the end of animation, check for easing === 1
//            var ctx = chart.ctx;
//
//            chart.data.datasets.forEach(function (dataset, i) {
//                var meta = chart.getDatasetMeta(i);
//                if (!meta.hidden) {
//                    meta.data.forEach(function(element, index) {
//                        // Draw the text in black, with the specified font
//                        ctx.fillStyle = 'rgb(50, 50, 50)';
//
//                        var fontSize = 10;
//                        var fontStyle = 'normal';
//                        var fontFamily = 'Source Sans Pro';
//                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
//
//                        // Just naively convert to string for now
//                        var dataString = dataset.data[index].toString();
//
//                        // Make sure alignment settings are correct
//                        ctx.textAlign = 'center';
//                        ctx.textBaseline = 'middle';
//
//                        var padding = 5;
//                        var position = element.tooltipPosition();
//                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
//                    });
//                }
//            });
//        }
    });
});
    
  // Grafik untuk penjualan per tahun start
  function getJualPerTahun(){
      $(".chartBarJualPerbulan").hide();
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualPerTahun');?>",
          data: {"periode_awal":""
                  ,"periode_akhir":""},
          success: function(resp){   
                var obj = jQuery.parseJSON(resp);
                var PDataDetailUnit = [];
                var PLabelDetailUnit = [];
                $.each(obj, function(key, data){
                    PDataDetailUnit.push(data.jml);
                    PLabelDetailUnit.push(data.tahun);
                });
                var pieOptions     = {
                    onClick : function(e,i){
                        //console.log(e);
                        e = i[0];
                        //console.log(this.data);
                        //console.log(e._index) // Index
                        //console.log(this.data.datasets[0].backgroundColor[e._index]);
                        var x_value = this.data.labels[e._index];
                        var bg_color = this.data.datasets[0].backgroundColor[e._index];
                        var y_value = this.data.datasets[0].data[e._index];
                        //console.log(x_value); // Label
                        //console.log(y_value); // Value
                        getJualPerBulan(x_value,bg_color);
                    },
                    //Boolean - Whether we should show a stroke on each segment
                    segmentShowStroke    : true,
                    //String - The colour of each segment stroke
                    segmentStrokeColor   : '#fff',
                    //Number - The width of each segment stroke
                    segmentStrokeWidth   : 1,
                    //Number - The percentage of the chart that we cut out of the middle
                    percentageInnerCutout: 0, // This is 0 for Pie charts
                    //Number - Amount of animation steps
                    animationSteps       : 100,
                    //String - Animation easing effect
                    animationEasing      : 'easeOutBounce',
                    //Boolean - Whether we animate the rotation of the Doughnut
                    animateRotate        : true,
                    //Boolean - Whether we animate scaling the Doughnut from the centre
                    animateScale         : false,
                    //Boolean - whether to make the chart responsive to window resizing
                    responsive           : true,
                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                    maintainAspectRatio  : false,
                    //String - A legend template
                    legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                    legend: {
                        display: false,
                        position: 'bottom',
                        fontSize: 9,
                        boxWidth: 20
                    },
                    title: {
                        display: true,
                        text: 'Grafik Penjualan Semua Unit Per Tahun'
                    },
                    chartArea: {
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    }
                };

                var config = {
                    type: 'bar',
                    data: {
                            datasets: [{
                                    data: PDataDetailUnit,
                                    backgroundColor: [
                                        window.chartColors.a,
                                        window.chartColors.b,
                                        window.chartColors.c,
                                        window.chartColors.d,
                                        window.chartColors.e,
                                        window.chartColors.f,
                                        window.chartColors.g,
                                        window.chartColors.h,
                                        window.chartColors.i,
                                        window.chartColors.j,
                                        window.chartColors.k,
                                        window.chartColors.l
                                    ],
                                    borderColor: window.chartColors.a,
                                    label: 'Penjualan',
                                    fill: false,
                                    lineTension:0
                            }],
                            labels:PLabelDetailUnit
                        },
                    options: pieOptions
                };
                var Sales = $('#pieBarJualPertahun').get(0).getContext('2d');
                if(typeof myBarJualPertahun != 'undefined' ){
                    myBarJualPertahun.destroy();
                }
                myBarJualPertahun = new Chart(Sales, config);

          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
    });
  }
  
  function getJualPerBulan(tahun,bg_color){
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualPerBulan');?>",
          data: {"tahun":tahun},
          success: function(resp){   
              $(".chartBarJualPerbulan").show();
              var obj = jQuery.parseJSON(resp);
              var PDataDetailUnit = [];
              var PLabelDetailUnit = [];
              $.each(obj, function(key, data){
                  PDataDetailUnit.push(data.jml);
                  PLabelDetailUnit.push(bulan_short[data.bulan]);
              });
              var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: false,
                    position: 'bottom',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Grafik Penjualan Semua Unit Pada Tahun ' + tahun
                },
                chartArea: {
                    backgroundColor: 'rgba(255, 255, 255, 1)'
                }
              };

              var config = {
                  type: 'line',
                  data: {
                          datasets: [{
                                data: PDataDetailUnit,
                                backgroundColor: bg_color,
                                borderColor: bg_color,
                                fill: false,
                                lineTension:0.5,
                                label: 'Penjualan Tahun ' + tahun
                          }],
                          labels:PLabelDetailUnit
                      },
                  options: pieOptions
              };
              var Sales = $('#BarJualPerbulan').get(0).getContext('2d');
              if(typeof myBarJualPerbulan != 'undefined' ){
                  myBarJualPerbulan.destroy();
              }
              myBarJualPerbulan = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
    });
  }
  // Grafik untuk penjualan per tahun end
  
  // Grafik untuk box area unit start
  function getJualUnitTahun(){
      var periode_awal = $("#periode_awal").val();
      var periode_akhir = $("#periode_akhir").val();
      $('.detail-unit').hide();
      $('.overlay').show();
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualUnitTahun');?>",
          data: {"periode_awal":periode_awal
                  ,"periode_akhir":periode_akhir},
          success: function(resp){   
              $('.overlay').hide();
              var obj = jQuery.parseJSON(resp);
              var PieData = [];
              var PData = [];
              var PLabel = [];
              var totalunit = 0;
              $.each(obj, function(key, data){
                  PieData.push(
                      {value: data.jml, 
                       label : data.nmtipe
                      }
                  );
                  PData.push(data.jml);
                  PLabel.push(data.nmtipe + ' [' + data.jml + ']');
                  totalunit+=Number(data.jml);
              });
              $(".total-header-unit").html("Total Penjualan Per Unit " + numeral(totalunit).format('0,0'));
              var pieOptions     = {
                onClick : function(e,i){
                    //console.log(e);
                    e = i[0];
                    //console.log(this.data);
                    //console.log(e._index) // Index
                    //console.log(this.data.datasets[0].backgroundColor[e._index]);
                    var x_value = this.data.labels[e._index];
                    var bg_color = this.data.datasets[0].backgroundColor[e._index];
                    var y_value = this.data.datasets[0].data[e._index];
                    //console.log(x_value); // Label
                    //console.log(y_value); // Value
                    getDetailJualUnitTahun(x_value,bg_color);
                },
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : true,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'right',
                    fontSize: 9,
                    boxWidth: 20
                },
                title: {
                    display: true,
                    text: 'Penjualan Top 10 Unit Per Periode'
                }
              };
              var config = {
                  type: 'pie',
                  data: {
                          datasets: [{
                              data: PData,
                          backgroundColor: [
                              window.chartColors.a,
                              window.chartColors.b,
                              window.chartColors.c,
                              window.chartColors.d,
                              window.chartColors.e,
                              window.chartColors.f,
                              window.chartColors.g,
                              window.chartColors.h,
                              window.chartColors.i,
                              window.chartColors.j
                          ],
                              label: 'Dataset 1'
                          }],
                          labels:PLabel
                      },
                  options: pieOptions
              };
              var ctx = $('#pieChart').get(0).getContext('2d');
              if(typeof myPie != 'undefined' ){
                  myPie.destroy();
              }
              myPie = new Chart(ctx, config);
          },
          error:function(event, textStatus, errorThrown) {
              $('.overlay').hide();
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  }
  
  function getDetailJualUnitTahun(nmunit,bg_color){
      var namaunit = nmunit.substring(0, nmunit.indexOf("[") - 1);
      var periode_awal = $("#periode_awal").val();
      var periode_akhir = $("#periode_akhir").val();
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getDetailJualUnitTahun');?>",
          data: {"periode_awal":periode_awal
                  ,"periode_akhir":periode_akhir
                  ,"namaunit":namaunit},
          success: function(resp){   
              $('.detail-unit').show();
              var obj = jQuery.parseJSON(resp);
              var PDataDetailUnit = [];
              var PLabelDetailUnit = [];
              $.each(obj, function(key, data){
                  PDataDetailUnit.push(data.jml);
                  PLabelDetailUnit.push(bulan_short[data.bulan]);
              });
              var pieOptions = {       
                showTooltips: false,
                onAnimationComplete: function () {

                    var ctx = this.chart.ctx;
                    ctx.font = this.scale.font;
                    ctx.fillStyle = this.scale.textColor
                    ctx.textAlign = "center";
                    ctx.textBaseline = "bottom";

                    this.datasets.forEach(function (dataset) {
                        dataset.bars.forEach(function (bar) {
                            ctx.fillText(bar.value, bar.x, bar.y - 5);
                        });
                    });
                },
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : true,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                    chartArea: {
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    }
              };

              var config = {
                  type: 'line',
                  data: {
                          datasets: [{
                                data: PDataDetailUnit,
                                backgroundColor: bg_color,
                                borderColor: bg_color,
                                fill: false,
                                lineTension:0.5,
                                label: 'PENJUALAN ' + namaunit
                          }],
                          labels:PLabelDetailUnit
                      },
                  options: pieOptions
              };
              var Sales = $('#barChartDetailUnit').get(0).getContext('2d');
              if(typeof myDetailPieUnit != 'undefined' ){
                  myDetailPieUnit.destroy();
              }
              myDetailPieUnit = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
    });
  }
  // Grafik untuk box area unit end
  
  // Grafik untuk box area sales start
  function getJualHeaderSalesTahun(){
      var periode_awal = $("#periode_awal").val();
      var periode_akhir = $("#periode_akhir").val();
      $('.sales').hide();
      $('.detail-sales').hide();
      $('.overlay').show();
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualHeaderSalesTahun');?>",
          data: {"periode_awal":periode_awal
                  ,"periode_akhir":periode_akhir},
          success: function(resp){   
              $('.overlay').hide();
              var obj = jQuery.parseJSON(resp);
              var PDataSales = [];
              var PLabelSales = [];
              var totalunit = 0;
              $.each(obj, function(key, data){
                  PDataSales.push(data.jml);
                  PLabelSales.push(data.nmsales_header + ' [' + data.jml + ']');
                  totalunit+=Number(data.jml);
              });
              $(".total-header-sales").html("Total Penjualan SPV Sales " + numeral(totalunit).format('0,0'));
              var pieOptions     = {
                onClick : function(e,i){
                    //console.log(e);
                    e = i[0];
                    //console.log(this.data);
                    //console.log(e._index) // Index
                    //console.log(this.data.datasets[0].backgroundColor[e._index]);
                    var x_value = this.data.labels[e._index];
                    var bg_color = this.data.datasets[0].backgroundColor[e._index];
                    var y_value = this.data.datasets[0].data[e._index];
                    //console.log(x_value); // Label
                    //console.log(y_value); // Value
                    getJualSalesTahun(x_value);
                },
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : true,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Penjualan Top 10 Sales Header Per Periode'
                }
              };

              var config = {
                  type: 'pie',
                  data: {
                          datasets: [{
                              data: PDataSales,
                          backgroundColor: [
                              window.chartColors.a,
                              window.chartColors.b,
                              window.chartColors.c,
                              window.chartColors.d,
                              window.chartColors.e,
                              window.chartColors.f,
                              window.chartColors.g,
                              window.chartColors.h,
                              window.chartColors.i,
                              window.chartColors.j
                          ],
                              label: 'Dataset 1'
                          }],
                          labels:PLabelSales
                      },
                  options: pieOptions
              };
              var Sales = $('#pieChartHeaderSales').get(0).getContext('2d');
              if(typeof myPieHeaderSales != 'undefined' ){
                  myPieHeaderSales.destroy();
              }
              myPieHeaderSales = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              $('.overlay').hide();
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  }
  
  function getJualSalesTahun(slheader){
      var periode_awal = $("#periode_awal").val();
      var periode_akhir = $("#periode_akhir").val();
      var nmsales_header = slheader.substring(0, slheader.indexOf("[") - 1);
      $('.detail-sales').hide();
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getJualSalesTahun');?>",
          data: {"periode_awal":periode_awal
                  ,"periode_akhir":periode_akhir
                 ,"nmsales_header":nmsales_header},
          success: function(resp){   
              $('.sales').show();
              var obj = jQuery.parseJSON(resp);
              var PDataSales = [];
              var PLabelSales = [];
              var totalunit = 0;
              $.each(obj, function(key, data){
                  PDataSales.push(data.jml);
                  PLabelSales.push(data.nmsales + ' [' + data.jml + ']');
                  totalunit+=Number(data.jml);
              });
              $(".total-sales").html("Total Penjualan Sales " + numeral(totalunit).format('0,0'));
              var pieOptions     = {
                onClick : function(e,i){
                    //console.log(e);
                    e = i[0];
                    //console.log(this.data);
                    //console.log(e._index) // Index
                    //console.log(this.data.datasets[0].backgroundColor[e._index]);
                    var x_value = this.data.labels[e._index];
                    var bg_color = this.data.datasets[0].backgroundColor[e._index];
                    var y_value = this.data.datasets[0].data[e._index];
                    //console.log(x_value); // Label
                    //console.log(y_value); // Value
                    getDetailJualSalesTahun(x_value,bg_color);
                },
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : true,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                legend: {
                    display: true,
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Penjualan Top 10 Sales Per Periode - Sales Header : ' + nmsales_header
                }
              };

              var config = {
                  type: 'doughnut',
                  data: {
                          datasets: [{
                              data: PDataSales,
                          backgroundColor: [
                              window.chartColors.a,
                              window.chartColors.b,
                              window.chartColors.c,
                              window.chartColors.d,
                              window.chartColors.e,
                              window.chartColors.f,
                              window.chartColors.g,
                              window.chartColors.h,
                              window.chartColors.i,
                              window.chartColors.j
                          ],
                              label: 'Dataset 1'
                          }],
                          labels:PLabelSales
                      },
                  options: pieOptions
              };
              var Sales = $('#pieChartSales').get(0).getContext('2d');
              if(typeof myPieSales != 'undefined' ){
                  myPieSales.destroy();
              }
              myPieSales = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
  }
  
  function getDetailJualSalesTahun(nmsales,bg_color){
      var namasales = nmsales.substring(0, nmsales.indexOf("[") - 1);
      var periode_awal = $("#periode_awal").val();
      var periode_akhir = $("#periode_akhir").val();
      $.ajax({
          type: "POST",
          url: "<?=site_url('dashboard/getDetailJualSalesTahun');?>",
          data: {"periode_awal":periode_awal
                  ,"periode_akhir":periode_akhir
                  ,"namasales":namasales},
          success: function(resp){   
              $('.detail-sales').show();
              var obj = jQuery.parseJSON(resp);
              var PDataDetailUnit = [];
              var PLabelDetailUnit = [];
              $.each(obj, function(key, data){
                  PDataDetailUnit.push(data.jml);
                  PLabelDetailUnit.push(bulan_short[data.bulan]);
              });
              var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : true,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                    chartArea: {
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    }
              };

              var config = {
                  type: 'line',
                  data: {
                          datasets: [{
                                data: PDataDetailUnit,
                                backgroundColor: bg_color,
                                borderColor: bg_color,
                                fill: false,
                                lineTension:0.5,
                                label: 'PENJUALAN ' + namasales
                          }],
                          labels:PLabelDetailUnit
                      },
                  options: pieOptions
              };
              var Sales = $('#barChartDetailSales').get(0).getContext('2d');
              if(typeof myDetailPieSales != 'undefined' ){
                  myDetailPieSales.destroy();
              }
              myDetailPieSales = new Chart(Sales, config);
          },
          error:function(event, textStatus, errorThrown) {
              console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
    });
  }
  // Grafik untuk box area sales end
</script>
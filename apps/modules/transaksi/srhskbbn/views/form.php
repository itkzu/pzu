<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
</style>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->
                <div class="box-header">
                    <button type="button" class="btn btn-primary btn-submit">
                       Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                       Batal
                    </button>
                </div>
                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-xs-5">
                                    <div class="row"> 
                                    </div>
                                </div>  


                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label> Surat/Kelengkapan Unit yang diterima </label>
                                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-xs-5">
                                    <div class="row">
                                        <div class="col-xs-3"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['nosin']['placeholder']); ?> 
                                            </div>
                                        </div>

                                        <div class="col-xs-8"> 
                                            <?php
                                                echo form_input($form['nosin']);
                                                echo form_error('nosin','<div class="note">','</div>');
                                            ?> 
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-7">
                                    <div class="row">
                                        <div class="col-xs-3"> 
                                            <div class="form-group">
                                                <?php echo form_label('JENIS'); ?>
                                                <div style="border-top: 1px solid #ddd;"></div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label('DITERIMA'); ?>
                                                <div style="border-top: 1px solid #ddd;"></div>
                                            </div>
                                        </div> 

                                        <div class="col-xs-2"> 
                                            <div class="form-group">
                                                <?php echo form_label('DISERAHKAN'); ?>
                                                <div style="border-top: 1px solid #ddd;"></div>
                                            </div>
                                        </div> 

                                        <div class="col-xs-5"> 
                                            <div class="form-group">
                                                <?php echo form_label('INFORMASI TAMBAHAN'); ?>
                                                <div style="border-top: 1px solid #ddd;"></div>
                                            </div>                                                
                                        </div> 
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-xs-5">
                                    <div class="row">
                                        <div class="col-xs-3"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['nora']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-8"> 
                                            <?php
                                                echo form_input($form['nora']);
                                                echo form_error('nora','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-7">
                                    <div class="row">
                                        <div class="col-xs-3"> 
                                            <div class="form-group">
                                                <?php echo form_label('NOTICE'); ?>
                                                <div style="border-top: 1px solid #ddd;"></div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">  
                                            <label>
                                                <?php
                                                    echo form_checkbox($form['trm_nts']);
                                                ?>
                                            </label> 
                                            <label class="tnya">Ya</label> 
                                            <label class="tnno">Tidak</label> 
                                            <div style="border-top: 1px solid #ddd;"></div>
                                        </div> 

                                        <div class="col-xs-2">  
                                            <label>
                                                <?php
                                                    echo form_checkbox($form['srh_nts']);
                                                ?>
                                            </label> 
                                            <label class="snya">Ya</label> 
                                            <label class="snno">Tidak</label> 
                                            <div style="border-top: 1px solid #ddd;"></div>
                                        </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>Nopol</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['nopol']);
                                                        echo form_error('nopol','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['kdtipe']);
                                                        echo form_error('kdtipe','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('STNK'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['trm_stnk']);
                                                                ?>
                                                          </label>
                                                          <label class="tsya">Ya</label> 
                                                          <label class="tsno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['srh_stnk']);
                                                                ?>
                                                          </label> 
                                                          <label class="ssya">Ya</label> 
                                                          <label class="ssno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div>  
                                            </div>
                                        </div> 
                                    </div>
                                </div>      

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-md-5 col-lg-5">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <br>
                                                    <!-- <div style="border-top: 1px solid #ddd; height: 10px;"></div> -->
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('TNKB'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['trm_tnkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="ttya">Ya</label> 
                                                          <label class="ttno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['srh_tnkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="stya">Ya</label> 
                                                          <label class="stno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div>  
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('Nama'); ?>
                                                        <!-- <?php echo form_label($form['nama']['placeholder']); ?> --> 
                                                    </div>
                                                </div>

                                                <div class="col-xs-9"> 
                                                    <?php
                                                        echo form_input($form['nama']);
                                                        echo form_error('nama','<div class="note">','</div>');
                                                    ?> 
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('BPKB'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['trm_bpkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="tbya">Ya</label> 
                                                          <label class="tbno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['srh_bpkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="sbya">Ya</label> 
                                                          <label class="sbno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>No. BPKB</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['nobpkb']);
                                                        echo form_error('nobpkb','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div>   
                                    </div>
                                </div>      

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('Alamat'); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-9"> 
                                                    <?php
                                                        echo form_input($form['alamat']);
                                                        echo form_error('alamat','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>   
                                    </div>
                                </div>  

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                    </div>
                                                </div>

                                                <div class="col-xs-9"> 
                                                    <?php
                                                        echo form_input($form['kota']);
                                                        echo form_error('kota','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>  

                                <div class="col-xs-12">
                                    <div class="row"> 
                                        <br>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['jnsbayar']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <?php
                                                        echo form_input($form['jnsbayar']);
                                                        echo form_error('jnsbayar','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>    

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['nodo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <div class="form-group"> 
                                                        <?php
                                                            echo form_input($form['nodo']);
                                                            echo form_error('nodo','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>   

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tgldo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <div class="form-group"> 
                                                        <?php
                                                            echo form_input($form['tgldo']);
                                                            echo form_error('tgldo','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>    

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['nmsales']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <div class="form-group"> 
                                                        <?php
                                                            echo form_input($form['nmsales']);
                                                            echo form_error('nmsales','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>  
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>  

<script type="text/javascript">
    $(document).ready(function () {  
        disabled();
        autoNum();
        cek();   
        $(".btn-submit").click(function(){ 
              ceksbt(); //submit(); 
        });

        $("#srh_nts").click(function(){
            cek();
        });

        $("#srh_stnk").click(function(){
            cek();
        });

        $("#srh_tnkb").click(function(){
            cek();
        });

        $("#srh_bpkb").click(function(){
            cek();
        });

        $(".btn-batal").click(function(){
            batal();
        });
    });

    function disabled(){ 
        if($("#trm_nts").prop("checked")){
            $("#trm_nts").attr('disabled',true);
            $("#srh_nts").attr('disabled',false);   
        } else {
            $("#trm_nts").attr('disabled',true);
            $("#srh_nts").attr('disabled',true);   
        }
        if($("#trm_stnk").prop("checked")){
            $("#trm_stnk").attr('disabled',true);
            $("#srh_stnk").attr('disabled',false);   
        } else {
            $("#trm_stnk").attr('disabled',true);
            $("#srh_stnk").attr('disabled',true);   
        }
        if($("#trm_tnkb").prop("checked")){
            $("#trm_tnkb").attr('disabled',true);
            $("#srh_tnkb").attr('disabled',false);  
        } else {
            $("#trm_tnkb").attr('disabled',true);
            $("#srh_tnkb").attr('disabled',true);   
        }
        if($("#trm_bpkb").prop("checked")){
            $("#trm_bpkb").attr('disabled',true);
            $("#srh_bpkb").attr('disabled',false);
        } else {
            $("#trm_bpkb").attr('disabled',true);
            $("#srh_bpkb").attr('disabled',true);   
        }
        $('#nopol').prop('disabled',true);
        $('#nobpkb').prop('disabled',true);
    }

    function autoNum(){
        $('#pajak').autoNumeric('init');
    }

    function cek(){
        if ($("#trm_nts").prop("checked")){  
            $('.tnya').show();
            $('.tnno').hide();
            if ($("#srh_nts").prop("checked")){
                $('.snya').show();
                $('.snno').hide();
            } else { 
                $('.snya').hide();
                $('.snno').show();
            }   
        } else {  
            $('.tnya').hide();
            $('.tnno').show();
            if ($("#srh_nts").prop("checked")){
                $('.snya').show();
                $('.snno').hide();
            } else { 
                $('.snya').hide();
                $('.snno').show();
            }   
        }
        if ($("#trm_stnk").prop("checked")){ 
            $('.tsya').show();
            $('.tsno').hide();
            if ($("#srh_stnk").prop("checked")){ 
                $('.ssya').show();
                $('.ssno').hide();
            } else {  
                $('.ssya').hide();
                $('.ssno').show();
            } 
        } else {  
            $('.tsya').hide();
            $('.tsno').show();
            if ($("#srh_stnk").prop("checked")){ 
                $('.ssya').show();
                $('.ssno').hide();
            } else {  
                $('.ssya').hide();
                $('.ssno').show();
            }   
        }
        if ($("#trm_tnkb").prop("checked")){ 
            $('.ttya').show();
            $('.ttno').hide();
            if ($("#srh_tnkb").prop("checked")){
                $('.stya').show();
                $('.stno').hide();
            } else { 
                $('.stya').hide();
                $('.stno').show();
            }
        } else {  
            $('.ttya').hide();
            $('.ttno').show();
            if ($("#srh_tnkb").prop("checked")){
                $('.stya').show();
                $('.stno').hide();
            } else { 
                $('.stya').hide();
                $('.stno').show();
            } 
        }
        if ($("#trm_bpkb").prop("checked")){ 
            $('.tbya').show();
            $('.tbno').hide();
            if ($("#srh_bpkb").prop("checked")){ 
                $('.sbya').show();
                $('.sbno').hide();
            } else {  
                $('.sbya').hide();
                $('.sbno').show();
            } 
        } else {  
            $('.tbya').hide();
            $('.tbno').show();
            if ($("#srh_bpkb").prop("checked")){
                $('.sbya').show();
                $('.sbno').hide();
            } else { 
                $('.sbya').hide();
                $('.sbno').show();
            }
        }
    }     

    function batal(){    
        swal({
            title: "Konfirmasi Batal!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            window.location.href = '<?=site_url('srhskbbn');?>';
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () { 
            var nodo    = $("#nodo").val();
            var nodo    = nodo.toUpperCase();    

            if ($("#srh_nts").prop("checked")){
                var p_notis = true;
            } else {
                var p_notis = false;
            }
            if ($("#srh_stnk").prop("checked")){
                var p_stnk = true;
            } else {
                var p_stnk = false;
            }
            if ($("#srh_tnkb").prop("checked")){
                var p_tnkb = true;
            } else {
                var p_tnkb = false;
            }
            if ($("#srh_bpkb").prop("checked")){
                var p_bpkb = true;
            } else {
                var p_bpkb = false;
            }   
            $.ajax({
                type: "POST",
                url: "<?=site_url("srhskbbn/submit");?>",
                data: {"nodo":nodo 
                    ,"p_notis":p_notis
                    ,"p_stnk":p_stnk
                    ,"p_tnkb":p_tnkb
                    ,"p_bpkb":p_bpkb},
                success: function(resp){
                    var obj = JSON.parse(resp);
                        if(obj.skbbn_kons_ins=1){
                            swal({
                                title: 'Data Berhasil Disimpan',
                                text: obj.msg,
                                type: 'success'
                            }, function(){
                                window.location.href = '<?=site_url('srhskbbn');?>';
                            }); 
                        }else{ 
                            swal({
                                title: 'Data Gagal Disimpan',
                                text: obj.msg,
                                type: 'error'
                            }); 
                        } 
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function ceksbt(){ 

        if ($("#trm_stnk").prop("checked")){
            var nostnk = $('#nostnk').val();
            if(nostnk===''){
                swal({
                    title: "Data tidak boleh kosong",
                    text: "",
                    type: "error"
                }, function(){
                    $('#nostnk').focus();
                    $('#nostnk').css("border", "2px solid red");
                }); 
            } else {
                $('#nostnk').blur();
                $('#nostnk').css("border", "1px solid gainsboro");
                submit(); 
            }  
        } 

        if ($("#trm_bpkb").prop("checked")){
            var nobpkb = $('#nobpkb').val();
            if(nobpkb===''){
                swal({
                    title: "Data tidak boleh kosong",
                    text: "",
                    type: "error"
                }, function(){
                    $('#nobpkb').focus();
                    $('#nobpkb').css("border", "2px solid red");
                }); 
            } else {
                $('#nobpkb').blur();
                $('#nobpkb').css("border", "1px solid gainsboro");
                submit(); 
            }  
        } 
    }

</script>

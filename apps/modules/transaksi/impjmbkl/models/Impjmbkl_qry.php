<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Impjmbkl_qry
 *
 * @author
 */

class Impjmbkl_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataCabang() {
        $this->db->select('*');
//        $kddiv = $this->input->post('kddiv');
//        $this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_divisi_bkl()");
        return $q->result_array();
    }

    public function getDataPos() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("bkl.get_div_unit_bkl()");
        return $q->result_array();
    }

    public function insertBeli($data)
    {
//        $kddiv = $this->input->post('kddiv');
// echo $data;
        $this->db->empty_table('glr.tmp_imp_jurnal');
        $this->db->insert_batch('glr.tmp_imp_jurnal', $data);
        // echo $this->db->last_query();
        if ($this->db->affected_rows() > 0){
              $q = $this->db->query("select title,msg,tipe from glr.imp_jmbkl('".$this->session->userdata("username")."')");
                     // echo $this->db->last_query();
              if($q->num_rows()>0){
                  $res = $q->result_array();
              }else{
                  $res = "";
              }
        } else{
            return 'Error! '.$this->db->_error_message();
        }

        return json_encode($res);
    } 


    public function json_dgview_detail() {
        error_reporting(-1); 
        if( isset($_GET['kddiv']) ){
            if($_GET['kddiv']){
                $kddiv = $_GET['kddiv'];
            }else{
                $kddiv = '';
            }
        }else{
            $kddiv = '';
        }
        $aColumns = array('kdakun',
                          'noref',
                          'nmakun',
                          'debet',
                          'kredit',
                          'nourut', 
                          'dk',
                          'ket',
                          'nominal');
        $sIndexColumn = "kdakun";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        
        if($this->session->userdata("su")===TRUE){
            $sTable = " ( SELECT * FROM glr.v_jurnal_temp where noref like 'JM%'  and kddiv = '".$kddiv."') AS a"; 
        } else {
            $sTable = "(SELECT * FROM glr.v_jurnal_temp where noref like 'JM%' and userentry = '".$this->session->userdata("username")."' and kddiv = '".$kddiv."') AS a"; 
        }

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ) {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" ){
                $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = " WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                $ix = $i - 1;
                if ( $sWhere == "" ){
                    $sWhere = " WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        // echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );
 
        foreach ( $rResult->result_array() as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            } 

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );
    }


    public function tambah() { 
        $tgljurnal = $this->apps->dateConvert($this->input->post('tgljurnal')); 
        $kddiv = $this->input->post('kddiv');
        $q = $this->db->query("select title,msg,tipe from glr.jm_ins_dash( '". $tgljurnal ."','". $kddiv ."','".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "kosong";
        } 
        return json_encode($res);
    }

    public function batal() { 
        $tgljurnal = $this->apps->dateConvert($this->input->post('tgljurnal')); 
        $kddiv = $this->input->post('kddiv');
        $q = $this->db->query("select title,msg,tipe from glr.jm_btl_dash( '".$this->session->userdata("username")."')");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Kwajustnk_qry
 *
 * @author adi
 */
class Kwajustnk_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
    }

    public function getkota() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("pzu.bbn_tarif");
        return $q->result_array();
    }

    public function getNoID() {
      $tanggal = $this->input->post('tanggal');
      $query = $this->db->query("select a.kode, (count(b.*)+1) as jml from api.kode a join pzu.t_do b on a.kode = left(b.nodo,1) where left(a.kddiv,9) = '". $this->session->userdata('data')['kddiv'] ."' group by a.kode");//where noso = '' UNION select * from api.v_so where noso = '" .$noso. "' idspk =  '" .$nospk. "' AND
      // echo $this->db->last_query();
      $res = $query->result_array();
      return json_encode($res);
    }

    public function ctk($nokps) {
        $this->db->select("*");
        $this->db->where('nokps',$nokps);
        $q = $this->db->get("pzu.vb_kps");
        // echo $this->db->last_query();
        $res = $q->result_array();
        return $res;
    }

    public function set_nosin() {
      $nosin = $this->input->post('nosin');
      $query = $this->db->query("select * from pzu.vm_kps where nosin = '" . $nosin . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function set_noaju() {
      $nokps = $this->input->post('nokps');
      $query = $this->db->query("select * from pzu.vm_kps where nokps = '" . $nokps . "'");
      // echo $this->db->last_query();
      if($query->num_rows()>0){
          $res = $query->result_array();
      }else{
          $res = "empty";
      }
      return json_encode($res);
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $nodf = str_replace('-','/',$no);
        $this->db->select('*, kddiv as kddiv2');
        $this->db->where('nodf',$nodf);
        $q = $this->db->get("pzu.vm_df");
        $res = $q->result_array();
        return $res;
    } 

    public function submit() {     
        $tglkps = $this->apps->dateConvert($this->input->post('tglkps'));
        // $tglkps = $this->input->post('tglkps'); 
        $nodo = $this->input->post('nodo');
        $nama_s = $this->input->post('nama_s');
        $alamat_s = $this->input->post('alamat_s'); 
        $kel_s = $this->input->post('kel_s'); 
        $kec_s = $this->input->post('kec_s'); 
        $kota_s = $this->input->post('kota_s'); 
        $kdcabang = $this->input->post('kdcabang'); 
        $harga = $this->input->post('harga');  
        $q = $this->db->query("select * from pzu.kps_ins( '". $tglkps ."'::date,
                                                                        '". $nodo ."'::varchar,
                                                                        '". $nama_s ."'::varchar,
                                                                        '". $alamat_s ."'::varchar,
                                                                        '". $kel_s ."'::varchar,
                                                                        '". $kec_s ."'::varchar,
                                                                        '". $kota_s ."'::varchar,
                                                                        '". $kdcabang ."'::varchar, 
                                                                        ". $harga ."::numeric,  
                                                                        '".$this->session->userdata("username")."'::varchar)");
        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }
}

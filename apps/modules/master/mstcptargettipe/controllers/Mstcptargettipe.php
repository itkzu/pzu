<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstcptargettipe
 *
 * @author adi
 */
class Mstcptargettipe extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('mstcptargettipe/submit'),
            'add' => site_url('mstcptargettipe/add'),
            'edit' => site_url('mstcptargettipe/edit'),
            'reload' => site_url('mstcptargettipe'),
        );
        $this->load->model('mstcptargettipe_qry');
        $kddiv = $this->mstcptargettipe_qry->getDataSeries();
        $this->data['kdtipesr'][''] = '-- PILIH SERIES --';
        foreach ($kddiv as $value) {
            $this->data['kdtipesr'][$value['kdtipesr']] = $value['nmtipesr'];
        }        
        
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){  
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function json_dgview() {
        echo $this->mstcptargettipe_qry->json_dgview();
    }
    
    public function select_data() {
        $res = $this->mstcptargettipe_qry->select_data();
        echo json_encode($res);
    }
    
    public function submit() {  
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        
        if($this->validate($id,$stat) == TRUE){
            $res = $this->mstcptargettipe_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($id);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($id)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_check_id($id);
                $this->template->build('form', $this->data);
            }
        }
    }
    
    private function _init_add(){
        
        
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'id',
                    'name'        => 'id',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'periode'=> array(
                    'placeholder' => 'Periode Target',
                    'id'      => 'periode',
                    'name'        => 'periode',
                    'value'       => date('m-Y'),
                    'class'       => 'form-control month',
                    'required'    => '',
            ),
            'kdtipesr'=> array(
                    'attr'        => array(
                        'id'    => 'kdtipesr',
                        'class' => 'form-control  chosen-select',
                    ),
                    'data'     => $this->data['kdtipesr'],
                    'value'    => set_value('kdtipesr'),
                    'name'     => 'kdtipesr',
                    'required'    => '',
            ),
           'jml_hr_kerja'=> array(
                    'placeholder' => 'Jumlah Hari Kerja',
                    'id'      => 'jml_hr_kerja',
                    'name'        => 'jml_hr_kerja',
                    'value'       => set_value('jml_hr_kerja'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_total'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Bulan',
                    'id'      => 'cp_target_total',
                    'name'        => 'cp_target_total',
                    'value'       => set_value('cp_target_total'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_harian'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Hari',
                    'id'      => 'cp_target_harian',
                    'name'        => 'cp_target_harian',
                    'value'       => set_value('cp_target_harian'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
        );
    }
    
    private function _init_edit(){
        $groupid = $this->uri->segment(3);
        $this->_check_id($groupid);
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'id',
                    'name'        => 'id',
                    'value'       => $this->val[0]['id'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
           'periode'=> array(
                    'placeholder' => 'Periode Target',
                    'id'      => 'periode',
                    'name'        => 'periode',
                    'value'       => $this->val[0]['periode'],
                    'class'       => 'form-control month',
                    'required'    => '',
                    
            ),
            'kdtipesr'=> array(
                    'attr'        => array(
                        'id'    => 'kdtipesr',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     => $this->data['kdtipesr'],
                    'value'    => $this->val[0]['kdtipesr'],
                    'name'     => 'kdtipesr',
                    'required'    => '',
            ),
           'jml_hr_kerja'=> array(
                    'placeholder' => 'Jumlah Hari Kerja',
                    'id'      => 'jml_hr_kerja',
                    'name'        => 'jml_hr_kerja',
                    'value'       => $this->val[0]['jml_hr_kerja'],
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_total'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Bulan',
                    'id'      => 'cp_target_total',
                    'name'        => 'cp_target_total',
                    'value'       => $this->val[0]['cp_target_total'],
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'cp_target_harian'=> array(
                    'placeholder' => 'Jumlah Capaian Target Per Hari',
                    'id'      => 'cp_target_harian',
                    'name'        => 'cp_target_harian',
                    'value'       => $this->val[0]['cp_target_harian'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'readonly'    => '',
            ),
        );
    }
    
    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }
        
        $this->val= $this->mstcptargettipe_qry->select_data($id);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'periode',
                    'label' => 'Periode Target',
                    'rules' => 'required|max_length[10]',
                ),
            array(
                    'field' => 'cp_target_total',
                    'label' => 'Jumlah Capaian Target Per Bulan',
                    'rules' => 'required|integer',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : 
 * Email : 
 * Description : 
 * ***************************************************************
 */

?>
<style type="text/css">
    .form-control{
        text-transform: uppercase;
    }
</style>

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                echo form_open($submit,$attributes); 
            ?>
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12 col-lg-12">
						<div class="form-group">
							<?php
								echo form_input($form['kdgift']);
							
								echo form_label($form['nmgift']['placeholder']);
								echo form_input($form['nmgift']);
								echo form_error('nmgift','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-md-6 col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label($form['tglawal']['placeholder']);
								echo form_input($form['tglawal']);
								echo form_error('tglawal','<div class="note">','</div>');
							?>
						</div>
					</div>

					<div class="col-md-6 col-lg-6">
						<div class="form-group">
							<?php 
								echo form_label($form['tglakhir']['placeholder']);
								echo form_input($form['tglakhir']);
								echo form_error('tglakhir','<div class="note">','</div>');
							?>
						</div>
					</div>


					<div class="col-lg-12">
						<div class="form-group">
							<?php 
								echo form_label($form['ket']['placeholder']);
								echo form_textarea($form['ket']);
								echo form_error('ket','<div class="note">','</div>');
							?>
						</div>
					</div>

                </div>
                <!-- </div> -->

				<br>

				<div id="detail" class="box box-primary box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Detail</h3>
						<div class="box-tools pull-right">
							<span>
								<button type="button" class="btn btn-danger btn-add"><i class="fa fa-plus"> Tambah Data</i></button>
							</span>
						</div>
					</div>

					<div class="box-body">
						<div class="table-responsive">
							<table id="table" class="dataTable table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th style="width: 10px;text-align: left;">No</th>
										<th style="width: 170px;text-align: left;">Kode</th>
										<th style="width: 170px;text-align: center;">Tipe Unit</th>
										<th style="text-align: center;">Jenis Unit</th>
										<th style="width: 50px;text-align: center;"><i class="fa fa-th-large"></i></th>
									</tr>
								</thead>
								<tbody></tbody>
								<tfoot>
									<tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>

						<!--
						<div class="text-right">
							<button type="button" class="btn btn-primary btn-add">Tambah Data</button>
						</div>
						-->

					</div>                    
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    Simpan
                </button>
                <a href="<?php echo $reload;?>" class="btn btn-default">
                    Batal
                </a>
            </div>

            <?php echo form_close(); ?>

        </div>
        <!-- /.box -->
    </div>
</div>



<!-- MODAL -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog">

		<div class="modal-content">

			<form role="form" id="newModalForm">

				<div class="modal-header">
					<!--
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
					-->
					<h4 class="modal-title" id="ModalTitle"></h4>
				</div>
				
				<div class="modal-body">

					<div class="row">

						<div class="col-md-12 col-lg-12">

							<div class='form-group'>

                                <div class="table-responsive">
                                    <table id="table_detail" class="dataTable table table-bordered table-striped table-hover"  style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px;text-align: left;">No</th>
                                                <th style="width: 10px;text-align: left;">Kode</th>
                                                <th style="text-align: left;">Tipe Unit</th>
                                                <th style="text-align: left;">Jenis Unit</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

							</div>
						</div>


					</div>

				</div>

				<div class="modal-footer">
					<button id="savedet" type="button" class="btn btn-primary btn-tambah">Simpan</button>
					<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Batal</button>
				</div>

			</form>

		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

		var column = [];
		column.push({ 
			"aTargets": [ 0,1,2,3,4 ],
			"searchable": false,
			"orderable": false  
		});

		table = $('#table').DataTable({
			"aoColumnDefs": column,
			"bProcessing": true,
			"bServerSide": true,
			//"order": [[ 0, 'asc' ]],
			// "pagingType": "simple",

			"sAjaxSource": "<?=site_url('msthadiah/getDetailEd');?>",

			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "kdgift", "value": $("#kdgift").val()} );
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},

			
			"sDom": " t ",
			//"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
			}

			//,"select": true
			//,"select": 'single'
			
		});		
		
		
		$(document).on('click', '.btn-add', function(e){
			e.preventDefault();

			$("#rowid").val('');
			//$("#akun").val('').trigger("chosen:updated");
			//$("#dk").val('').trigger("chosen:updated");
			//$("#nofaktur").val('');
			//$("#keterangan").val('');

			//$("#nominal").val('');
			//nominalAuto.clear();

				
			isAdd = true;

			$('.modal-title').html('Tambah Detail Transaksi');
			$('.modal').modal({
				backdrop: 'static'
			});	
		});





		//var kdgift = $('#nmgift').value;
		var kdgift = document.getElementById("kdgift").value;
		//alert(kdgift);

		var column = [];
		column.push({ 
				"aTargets": [ 0 ],
				"searchable": false,
				"orderable": false  
		});

		table_detail = $('#table_detail').DataTable({
			"aoColumnDefs": column,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 1, 'asc' ]],
			// "pagingType": "simple",
			"sAjaxSource": "<?=site_url('msthadiah/ListKodeUnitEd');?>",

			//"fnServerParams": function ( aoData ) {
			
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push( { "name": "kdgift", "value": kdgift} );
				$.ajax( {
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				} );
			},


			
			//"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
			"oLanguage": {
				"sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
			}

			//,"select": true
			,"select": 'single'
			
		});		


		$('#exampleModal').on('shown.bs.modal', function (e) {
	  		//alert('do something...');
			//$('#tabledetail_filter input').html("");
			table_detail.ajax.reload();
			$('#tabledetail_filter input').focus();
		});
   			

		/*
		$(document).on('click', '#tabledetail', function (e) {

			$('#exampleModal').modal('hide');
		});
		*/

		// ON SAVE DETAIL
		$(document).on('click', '#savedet', function(e){


			var rowdata = table_detail.rows('.selected').data().toArray();

			var kode = rowdata[0][1]
			//alert(rowdata[0][1]);

			
			$.ajax({
				type: "POST",
				url: "<?=site_url('msthadiah/SimpanDetailEd');?>",
				data: {
					"kdgift": kdgift,
					"kode": kode
					},
				success: function(resp){
					table.ajax.reload();
					$('#exampleModal').modal('hide');

					/*
					var obj = jQuery.parseJSON(resp);
					if(obj.state==="1"){
						swal({
							title: "Terhapus",
							text: obj.msg,
							type: "success"
						}, function(){
							table_detail.ajax.reload();
						});
					}else{
						swal("Gagal!", obj.msg, "error");
					}
					*/
				},
				error:function(event, textStatus, errorThrown) {
					swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
				}
			});
			


			localStorage.setItem("gift", JSON.stringify({"kode":kode
				}));



		});		
        
	});
	

	function deletedet(id){
		swal({
			title: "Konfirmasi Hapus !",
			text: "Data yang dihapus tidak dapat dikembalikan!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#c9302c",
			confirmButtonText: "Ya, Lanjutkan!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false
		}, function () {
				$.ajax({
					type: "POST",
					url: "<?=site_url('msthadiah/HapusDetailEd');?>",
					data: {"nourut":id},
					success: function(resp){   
						var obj = jQuery.parseJSON(resp);
						if(obj.state==="1"){
							swal({
								title: "Terhapus",
								text: obj.msg,
								type: "success"
							}, function(){
								table.ajax.reload();
							});
							table.ajax.reload();

							localStorage.removeItem("gift");

						}else{
							swal("Gagal!", obj.msg, "error");
						}
						
					},
					error:function(event, textStatus, errorThrown) {
						swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
					}
				});
		});
	}	
</script>
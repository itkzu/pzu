<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Pobkl
 *
 * @author
 */
class Pobkl extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('pobkl/submit'),
            'add' => site_url('pobkl/add'),
            'edit' => site_url('pobkl/edit'),
            'reload' => site_url('pobkl'),
        );
        $this->load->model('pobkl_qry');
        $supplier = $this->pobkl_qry->getDataSupplier();
        foreach ($supplier as $value) {
            $this->data['kdsup'][$value['kdsup']] = $value['nmsup'];
        }
        $getdiv = $this->pobkl_qry->getdiv();
        foreach ($getdiv as $value) {
            $this->data['kddiv'][$value['kddiv']] = $value['nmdiv'];
        }     
        /*
        $kdaks = $this->pobkl_qry->getKodepobkl();
        $this->data['kdaks'] = array(
            "" => "-- Pilih Kode Aksesoris --",
          );
        foreach ($kdaks as $value) {
            $this->data['kdaks'][$value['kdaks']] = $value['kdaks']." - ".$value['nmaks'];
          }
          */

        $this->data['statppn'] = array(
            "I" => "INCLUDE",
            "E" => "EXCLUDE",
            "N" => "NON-PPN",
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    // public function add(){
    //     $this->_init_add();
    //     $this->template
    //         ->title($this->data['msg_main'],$this->apps->name)
    //         ->set_layout('main')
    //         ->build('form',$this->data);
    // }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function getKdGrup() {
        echo $this->pobkl_qry->getKdGrup();
    }

    public function json_dgview() {
        echo $this->pobkl_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->pobkl_qry->json_dgview_detail();
    }
    public function addDetail() {
        echo $this->pobkl_qry->addDetail();
    }
    public function save() {
        echo $this->pobkl_qry->save();
    } 

    public function approve() {
        echo $this->pobkl_qry->approve();
    }
    private function _init_add(){


    		if(isset($_POST['hrgblmada']) && strtoupper($_POST['hrgblmada']) == 'OK'){
    			$chk_hrg = TRUE;
    		} else{
    			$chk_hrg = FALSE;
    		}

        $this->data['form'] = array(

            'kddiv' => array(
                  'placeholder' => 'Pilih Cabang',
                  'attr'        => array(
                      'id'      => 'kddiv',
                      'class'   => 'form-control',
                  ),
                  'data'        => $this->data['kddiv'],
                  'class'       => 'form-control',
                  'value'       => set_value('kddiv'),
                  'name'        => 'kddiv',
                    // 'readonly' => '',
            ),
           'nopo'=> array(
                    'placeholder' => 'No. PO',
                    //'type'        => 'hidden',
                    'id'          => 'nopo',
                    'name'        => 'nopo',
                    'value'       => set_value('nopo'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglpo'=> array(
                    'placeholder' => 'Tanggal PO',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => $this->apps->dateConvert(set_value('tglpo')),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'nmsup'=> array(
                    'id'          => 'nmsup',
                    'value'    => set_value('nmsup'),
                    'name'     => 'nmsup',
                    'class'       => 'form-control',
                    'placeholder' => 'Supplier',
                    'readonly'    => '',
            ),
            'alamat'=> array(
                    'id'          => 'alamat',
                    'value'    => set_value('alamat'),
                    'name'     => 'alamat',
                    'class'       => 'form-control',
                    'placeholder' => 'Alamat',
                    'readonly'    => '',
            ),
           'nofaktur'=> array(
                    'placeholder' => 'No. Faktur',
                    'id'          => 'nofaktur',
                    'name'        => 'nofaktur',
                   'value'       => set_value('nofaktur'),
                    'class'       => 'form-control',
                    'readonly'    => '',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'total'=> array(
                    'placeholder' => 'Total',
                    'id'      => 'total',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('total'),
                    'name'     => 'total',
                    'readonly'    => '',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
      		   'tot_disc'=> array(
                     'placeholder' => 'Discount',
                     'id'      => 'tot_disc',
                     'class' => 'form-control',
                     'data'     => '',
                     'value'    => set_value('tot_disc'),
                     'name'     => 'tot_disc',
                     'readonly'    => '',
      			),
            'total_net'=> array(
                    'placeholder' => 'Total Net',
                    'id'      => 'total_net',
                    // 'id'    => 'nourut',
                    // 'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('total_net'),
                    'name'     => 'total_net',
                    'readonly'    => '',
            ),
            'dpp'=> array(
                    'placeholder' => 'DPP',
                    'id'      => 'dpp',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('dpp'),
                    'name'     => 'dpp',
                    'readonly'    => '',
            ),
            'ppn'=> array(
                    'placeholder' => 'PPN',
                    'id'      => 'ppn',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ppn'),
                    'name'     => 'ppn',
                    'readonly'    => '',
            ),
            'grno'=> array(
                    'placeholder' => 'GRNO',
                    'id'      => 'grno',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('grno'),
                    'name'     => 'grno',
                    'readonly'    => '',
            ),
            'bkuno'=> array(
                    'placeholder' => 'BKUNO',
                    'id'      => 'bkuno',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('bkuno'),
                    'name'     => 'bkuno',
                    'readonly'    => '',
            ),
            'kdpart'=> array(
                    'placeholder' => 'Kode Part',
                    'id'    => 'kdpart',
                    'class' => 'form-control',
                    'value'    => set_value('kdpart'),
                    'name'     => 'kdpart',
                    'required'    => '',
            ),
            'nmpart'=> array(
                    'placeholder' => 'Nama Part',
                    'id'    => 'nmpart',
                    'class' => 'form-control',
                    'value'    => set_value('nmpart'),
                    'name'     => 'nmpart',
            ),
            'kdgrup'=> array(
                    'placeholder' => 'Kode Group',
                    'id'    => 'kdgrup',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('kdgrup'),
                    'name'     => 'kdgrup',
                    'readonly' => ''
            )
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $nopo = $this->uri->segment(3);
        }
        $this->_check_id($nopo);
        $this->data['form'] = array(
           'nopo'=> array(
                    'placeholder' => 'No. PO',
                    //'type'        => 'hidden',
                    'id'          => 'nopo',
                    'name'        => 'nopo',
                    'value'       => $this->val[0]['nopo'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglpo'=> array(
                    'placeholder' => 'Tanggal PO',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => $this->apps->dateConvert($this->val[0]['tglpo']),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
            'nmsup'=> array(
                    'id'          => 'nmsup',
                    'value'    => $this->val[0]['nmsup'],
                    'name'     => 'nmsup',
                    'class'       => 'form-control',
                    'placeholder' => 'Supplier',
                    'readonly'    => '',
            ),
            'alamat'=> array(
                    'id'          => 'alamat',
                    'value'    => $this->val[0]['alamat'],
                    'name'     => 'alamat',
                    'class'       => 'form-control',
                    'placeholder' => 'Alamat',
                    'readonly'    => '',
            ),
           'nofaktur'=> array(
                    'placeholder' => 'No. Faktur',
                    'id'          => 'nofaktur',
                    'name'        => 'nofaktur',
                   'value'       => $this->val[0]['nofaktur'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'total'=> array(
                    'placeholder' => 'Total',
                    'id'      => 'total',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => number_format($this->val[0]['total'],2,",","."),
                    'name'     => 'total',
                    'readonly'    => '',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
      		   'tot_disc'=> array(
                     'placeholder' => 'Discount',
                     'id'      => 'tot_disc',
                     'class' => 'form-control',
                     'data'     => '',
                     'value'    => number_format($this->val[0]['tot_disc'],2,",","."),
                     'name'     => 'tot_disc',
                     'readonly'    => '',
      			),
            'total_net'=> array(
                    'placeholder' => 'Total Net',
                    'id'      => 'total_net',
                    // 'id'    => 'nourut',
                    // 'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => number_format($this->val[0]['total_net'],2,",","."),
                    'name'     => 'total_net',
                    'readonly'    => '',
            ),
            'dpp'=> array(
                    'placeholder' => 'DPP',
                    'id'      => 'dpp',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => number_format($this->val[0]['dpp'],2,",","."),
                    'name'     => 'dpp',
                    'readonly'    => '',
            ),
            'ppn'=> array(
                    'placeholder' => 'PPN',
                    'id'      => 'ppn',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => number_format($this->val[0]['ppn'],2,",","."),
                    'name'     => 'ppn',
                    'readonly'    => '',
            ),
            'grno'=> array(
                    'placeholder' => 'GRNO',
                    'id'      => 'grno',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['grno'],
                    'name'     => 'grno',
                    'readonly'    => '',
            ),
            'bkuno'=> array(
                    'placeholder' => 'BKUNO',
                    'id'      => 'bkuno',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['bkuno'],
                    'name'     => 'bkuno',
                    'readonly'    => '',
            ),
            'kdpart'=> array(
                    'placeholder' => 'Kode Part',
                    'id'    => 'kdpart',
                    'class' => 'form-control',
                    'value'    => set_value('kdpart'),
                    'name'     => 'kdpart',
                    'readonly'    => '',
            ),
            'nmpart'=> array(
                    'placeholder' => 'Nama Part',
                    'id'    => 'nmpart',
                    'class' => 'form-control',
                    'value'    => set_value('nmpart'),
                    'name'     => 'nmpart',
                    'readonly'    => '',
            ),
            'kdgrup'=> array(
                    'attr'        => array(
                        'id'    => 'kdgrup',
                        'class' => 'form-control  select',
                    ),
                    'placeholder' => 'Kode Group',
                    'data'     => '',
                    'value'    => set_value('kdgrup'),
                    'name'     => 'kdgrup',
                    'readonly' => ''
            )
        );
    }

    private function _check_id($nopo){
        if(empty($nopo)){
            redirect($this->data['add']);
        }

        $this->val= $this->pobkl_qry->select_data($nopo);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaks',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<style>
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: none;
    }
    .radio {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    
    .checkbox label, .radio label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 5px;
        font-weight: bold;
        cursor: pointer;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'target' => '_blank'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes); 
        ?> 
          <div class="box-body">
            <div class="form-group">
                <?php
                    echo '<div class="radio">';
                    echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_multi'),'true') . ' Pilih Kode Akun <small>Bisa dipilih lebih dari satu akun</small>');
                    echo '</div>';
                    echo form_dropdown($form['akun_multi']['name'],$form['akun_multi']['data'] ,$form['akun_multi']['value'] ,$form['akun_multi']['attr']);
                    echo form_error('akun_multi','<div class="note">','</div>'); 
                ?>
            </div>  
            <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                      <?php
                          echo '<div class="radio">';
                          echo form_label(form_radio(array('name' => 'rdakun','id'=>'rdakun_between'),'true') . ' Pilih Kode Akun Awal');
                          echo '</div>';
                          echo form_dropdown($form['akun_awal']['name'],$form['akun_awal']['data'] ,$form['akun_awal']['value'] ,$form['akun_awal']['attr']);
                          echo form_error('akun_awal','<div class="note">','</div>'); 
                      ?>
                  </div>  
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <?php
                          echo form_label('Pilih Kode Akun Akhir');
                          echo form_dropdown($form['akun_akhir']['name'],$form['akun_akhir']['data'] ,$form['akun_akhir']['value'] ,$form['akun_akhir']['attr']);
                          echo form_error('akun_akhir','<div class="note">','</div>'); 
                      ?>
                  </div>  

                </div>
            </div>
              <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">    
                        <?php 
                            echo form_label('Periode Awal');
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>'); 
                        ?>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">    
                        <?php 
                            echo form_label('Periode Akhir');
                            echo form_input($form['periode_akhir']);
                            echo form_error('periode_akhir','<div class="note">','</div>'); 
                        ?>
                    </div>
                  </div>
              </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" name="submit" value="html">
                <i class="fa fa-print"></i> Cetak
            </button>
            <button type="submit" class="btn btn-success" name="submit" value="excel">
                <i class="fa fa-file-excel-o"></i> Export Ke Excel
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>    
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#akun_multi").prop("disabled", true);
        $("#akun_awal").prop("disabled", true);
        $("#akun_akhir").prop("disabled", true);
        $("#rdakun_multi").click(function(){
            setAkun();
        });
        $("#rdakun_between").click(function(){
            setAkun();
        });
        $("#akun_multi").select2({
            placeholder: "Pilih Satu Kode Akun Atau Lebih"
        });
        $("#akun_awal").select2({
            placeholder: "Pilih Kode Akun Awal"
        });
        $("#akun_akhir").select2({
            placeholder: "Pilih Kode Akun Akhir"
        });
        $('#periode_awal').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"      
        });
        $('#periode_akhir').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"       
        });
    });
    
    function setAkun(){
        var check = $('#rdakun_multi').is(':checked');
        if(check){
            $("#akun_multi").prop("disabled", false);
            $("#akun_awal").prop("disabled", true);
            $("#akun_akhir").prop("disabled", true);
        }else{
            $("#akun_multi").prop("disabled", true);
            $("#akun_awal").prop("disabled", false);
            $("#akun_akhir").prop("disabled", false);
            
        }      
    }
</script>
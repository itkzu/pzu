<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Kbins
 *
 * @author adi
 */
class Kbins extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('kbins/submit'),
            'add' => site_url('kbins/add'),
            'edit' => site_url('kbins/edit'),
            'ctk'      => site_url('kbins/ctk'),
            'html'      => site_url('kbins/html'),
            'pdf'      => site_url('kbins/pdf'),
            'reload' => site_url('kbins'),
        );
        $this->load->model('kbins_qry');
        $kdkb = $this->kbins_qry->getKB();
        foreach ($kdkb as $value) {
            $this->data['kdkb'][$value['kdkb']] = $value['nmkb'];
        } 
        $jnstrx = $this->kbins_qry->getjnstrans();
        foreach ($jnstrx as $value) {
            $this->data['jnstrx'][$value['jnstrx']] = $value['jnstrx'];
        }  
        $kdleas = $this->kbins_qry->getleas();
        foreach ($kdleas as $value) {
            $this->data['kdleasing'][$value['kdleasing']] = $value['kdleasing'];
        }     


        //manual
        $this->data['jnsrleas'] = array(
            "MX" => "REFUND BELAKANG (MX)",
            "JP" => "REFUND DEPAN (JP)"
        );
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    } 
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function html() {
        $nokb = $this->uri->segment(4);   
        $tglkb = $this->uri->segment(5);   
        $this->data['data'] = $this->kbins_qry->html($nokb,$tglkb);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function pdf() {  
        $nokb = $this->uri->segment(4);   
        $tglkb = $this->uri->segment(5);  

        $this->load->library('dompdf_lib');

        $this->dompdf_lib->filename = "Nota_BBM.pdf";
        $customPaper = array(0,0,360,360);

        $this->data['data'] = $this->kbins_qry->html($nokb,$tglkb);
        // $this->sum['sum'] = $this->tkbbkl_qry->sum($nokb);
        //
        $this->dompdf_lib->load_view('html', $this->data );
        // $this->dompdf_lib->load_view('ctk_stk', $this->data );
    }
    
    public function ctk_do() {  
        $nodo = $this->uri->segment(3); 
        $this->data['data'] = $this->donew_qry->ctk_do($nodo);  
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('ctk_do',$this->data);  
    } 

    public function getNoID() {
        echo $this->kbins_qry->getNoID();
    }

    public function getKdjenis() {
        echo $this->kbins_qry->getKdjenis();
    }

    public function set_kb() {
        echo $this->kbins_qry->set_kb();
    }

    public function set_nokasbon() {
        echo $this->kbins_qry->set_nokasbon();
    }

    public function set_pkons() {
        echo $this->kbins_qry->set_pkons();
    }

    public function set_pleas() {
        echo $this->kbins_qry->set_pleas();
    }

    public function set_rleas() {
        echo $this->kbins_qry->set_rleas();
    }

    public function set_kbm() {
        echo $this->kbins_qry->set_kbm();
    }

    public function set_blkkons() {
        echo $this->kbins_qry->set_blkkons();
    }

    public function set_blkkonsnoso() {
        echo $this->kbins_qry->set_blkkonsnoso();
    }

    public function set_blkkonsnodo() {
        echo $this->kbins_qry->set_blkkonsnodo();
    }

    public function refkb() {
        echo $this->kbins_qry->refkb();
    }

    public function set_pkonsnoso() {
        echo $this->kbins_qry->set_pkonsnoso();
    }

    public function set_pkonsnodo() {
        echo $this->kbins_qry->set_pkonsnodo();
    }

    public function set_pleasnoso() {
        echo $this->kbins_qry->set_pleasnoso();
    }

    public function set_pleasnodo() {
        echo $this->kbins_qry->set_pleasnodo();
    }

    public function set_nocetak() {
        echo $this->kbins_qry->set_nocetak();
    }

    public function set_jmlctk() {
        echo $this->kbins_qry->set_jmlctk();
    }

    public function pkons_submit() {
        echo $this->kbins_qry->pkons_submit();
    }

    public function pleas_submit() {
        echo $this->kbins_qry->pleas_submit();
    }

    public function rleas_submit() {
        echo $this->kbins_qry->rleas_submit();
    }

    public function blkkons_submit() {
        echo $this->kbins_qry->blkkons_submit();
    }

    public function tambah() {
        echo $this->kbins_qry->tambah();
    }

    public function rkasbon_ins() {
        echo $this->kbins_qry->rkasbon_ins();
    }

    public function batal() {
        echo $this->kbins_qry->batal();
    }

    public function delDetail() {
        echo $this->kbins_qry->delDetail();
    }

    public function json_dgview() {
        echo $this->kbins_qry->json_dgview();
    }

    public function kb_json_dgview() {
        echo $this->kbins_qry->kb_json_dgview();
    }

    public function kasbon_json_dgview() {
        echo $this->kbins_qry->kasbon_json_dgview();
    }

    public function set_nokb() {
        echo $this->kbins_qry->set_nokb();
    }

    public function get_pkons() {
        echo $this->kbins_qry->get_pkons();
    }

    public function get_pleas() {
        echo $this->kbins_qry->get_pleas();
    }

    public function get_blkkons() {
        echo $this->kbins_qry->get_blkkons();
    }

    public function kbm_submit() {
        echo $this->kbins_qry->kbm_submit();
    }

    public function kbm_del() {
        echo $this->kbins_qry->kbm_del();
    }

    public function kasbon_del() {
        echo $this->kbins_qry->kasbon_del();
    }

    public function kbsubmit() {
        echo $this->kbins_qry->kbsubmit();
    }

    public function rkasbon_submit() {
        echo $this->kbins_qry->rkasbon_submit();
    }

    public function proses_dk() {
        echo $this->kbins_qry->proses_dk();
    } 

    private function _init_add(){ 
        $this->data['form'] = array(
            //header
           'nokb'=> array(
                    'placeholder' => 'No. Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
            'refkb'=> array(
                    'placeholder' => 'Ref. Kas/Bank',
                    'attr'        => array(
                        'id'    => 'refkb',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdkb'],
                    'value'    => set_value('refkb'),
                    'name'     => 'refkb',
                    // 'readonly'    => '',
            ),
            'jnstrans'=> array(
                    'placeholder' => 'Jenis Transaksi',
                    'attr'        => array(
                        'id'    => 'jnstrans',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['jnstrx'], //$this->data['kdsales_ins'],
                    'value'    => set_value('jnstrans'),
                    'name'     => 'jnstrans',
                    // 'readonly'    => '',
            ),
            'day'=> array(
                    'placeholder' => '',
                    //'type'        => 'hidden',
                    'id'          => 'day',
                    'name'        => 'day',
                    'value'       => set_value('day'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'dk'=> array(
                    'placeholder' => '',
                    'type'        => 'hidden',
                    'id'          => 'dk',
                    'name'        => 'dk',
                    'value'       => set_value('dk'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'periode'=> array(
                    'placeholder' => 'PERIODE KAS/BANK',
                    //'type'        => 'hidden',
                    'id'          => 'periode',
                    'name'        => 'periode',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 

            //detail pembayaran konsumen
                'byrkons_noso'=> array(
                        'placeholder' => 'No. SPK',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_noso',
                        'name'        => 'byrkons_noso',
                        'value'       => set_value('byrkons_noso'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_nodo'=> array(
                        'placeholder' => 'No. DO',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nodo',
                        'name'        => 'byrkons_nodo',
                        'value'       => set_value('nodo'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_tglso'=> array(
                        'placeholder' => 'Tanggal SPK',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_tglso',
                        'name'        => 'byrkons_tglso',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_tgldo',
                        'name'        => 'byrkons_tgldo',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nmsales'=> array(
                        'placeholder' => 'Sales',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nmsales',
                        'name'        => 'byrkons_nmsales',
                        'value'       => set_value('byrkons_nmsales'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nmspv'=> array(
                        'placeholder' => 'Supervisor',
                        'id'          => 'byrkons_nmspv',
                        'name'        => 'byrkons_nmspv',
                        'value'       => set_value('byrkons_nmspv'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nama'=> array(
                        'placeholder' => 'Konsumen',
                        'id'          => 'byrkons_nama',
                        'name'        => 'byrkons_nama',
                        'value'       => set_value('byrkons_nama'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum();',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_alamat'=> array( 
                        'placeholder' => 'Alamat',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_alamat',
                        'name'        => 'byrkons_alamat',
                        'value'       => set_value('byrkons_alamat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_kdleas'=> array( 
                        'placeholder' => 'Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_kdleas',
                        'name'        => 'byrkons_kdleas',
                        'value'       => set_value('byrkons_kdleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_kdprogleas'=> array( 
                        'placeholder' => 'Prog. Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_kdprogleas',
                        'name'        => 'byrkons_kdprogleas',
                        'value'       => set_value('byrkons_kdprogleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nosin'=> array(
                        'placeholder' => 'No. Mesin/ Rk',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nosin',
                        'name'        => 'byrkons_nosin',
                        'value'       => set_value('byrkons_nosin'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                        'required'    => '',
                ),
                'byrkons_nora'=> array(
                        'placeholder' => 'No. Rangka',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nora',
                        'name'        => 'byrkons_nora',
                        'value'       => set_value('byrkons_nora'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_kdtipe'=> array(
                        'placeholder' => 'Tipe SMH',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_kdtipe',
                        'name'        => 'byrkons_kdtipe',
                        'value'       => set_value('byrkons_kdtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_nmtipe'=> array(
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nmtipe',
                        'name'        => 'byrkons_nmtipe',
                        'value'       => set_value('byrkons_nmtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_warna'=> array(
                        'placeholder' => 'Warna/Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_warna',
                        'name'        => 'byrkons_warna',
                        'value'       => set_value('byrkons_warna'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_tahun'=> array(
                        'placeholder' => 'Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_tahun',
                        'name'        => 'byrkons_tahun',
                        'value'       => set_value('byrkons_tahun'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_status'=> array(
                        'placeholder' => 'Status OTR',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_status',
                        'name'        => 'byrkons_status',
                        'value'       => set_value('byrkons_status'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 

                //nominal pembayaran konsumen
                'byrkons_hjnet'=> array(
                        'placeholder' => 'Harga Jual Net',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_hjnet',
                        'name'        => 'byrkons_hjnet',
                        'value'       => set_value('byrkons_hjnet'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum1();',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),
                'byrkons_um'=> array(
                        'placeholder' => 'Uang Muka',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_um',
                        'name'        => 'byrkons_um',
                        'value'       => set_value('byrkons_um'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum1();',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),
                'byrkons_pelunasan'=> array( 
                        'placeholder' => 'Pelunasan',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_pelunasan',
                        'name'        => 'byrkons_pelunasan',
                        'value'       => set_value('byrkons_pelunasan'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_byr1'=> array( 
                        'placeholder' => 'Terbayar',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_byr1',
                        'name'        => 'byrkons_byr1',
                        'value'       => set_value('byrkons_byr1'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_byr2'=> array( 
                        'placeholder' => 'Terbayar',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_byr2',
                        'name'        => 'byrkons_byr2',
                        'value'       => set_value('byrkons_byr2'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_nbyr'=> array( 
                        'placeholder' => 'Nilai Bayar',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nbyr',
                        'name'        => 'byrkons_nbyr',
                        'value'       => set_value('byrkons_nbyr'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_saldoum'=> array( 
                        'placeholder' => 'Saldo Uang Muka',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_saldoum',
                        'name'        => 'byrkons_saldoum',
                        'value'       => set_value('byrkons_saldoum'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_saldo_pelunasan'=> array( 
                        'placeholder' => 'Saldo Pelunasan',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_saldo_pelunasan',
                        'name'        => 'byrkons_saldo_pelunasan',
                        'value'       => set_value('byrkons_saldo_pelunasan'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),  
                'byrkons_nocetak'=> array( 
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nocetak',
                        'name'        => 'byrkons_nocetak',
                        'value'       => set_value('byrkons_nocetak'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: center;',
                        'readonly'    => '',
                ), 
                'byrkons_noctk'=> array( 
                        'placeholder' => '',
                        'type'        => 'hidden',
                        'id'          => 'byrkons_noctk',
                        'name'        => 'byrkons_noctk',
                        'value'       => set_value('byrkons_noctk'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: center;',
                        'readonly'    => '',
                ), 
                'byrkons_jmltrans'=> array( 
                        'placeholder' => 'Jumlah Transaksi',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_jmltrans',
                        'name'        => 'byrkons_jmltrans',
                        'value'       => set_value('byrkons_jmltrans'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),  

            //detail pembayaran leasing
                'pleas_noso'=> array(
                        'placeholder' => 'No. SPK',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_noso',
                        'name'        => 'pleas_noso',
                        'value'       => set_value('pleas_noso'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_nodo'=> array(
                        'placeholder' => 'No. DO',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nodo',
                        'name'        => 'pleas_nodo',
                        'value'       => set_value('nodo'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_tglso'=> array(
                        'placeholder' => 'Tanggal SPK',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_tglso',
                        'name'        => 'pleas_tglso',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_tgldo',
                        'name'        => 'pleas_tgldo',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nmsales'=> array(
                        'placeholder' => 'Sales',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nmsales',
                        'name'        => 'pleas_nmsales',
                        'value'       => set_value('pleas_nmsales'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nmspv'=> array(
                        'placeholder' => 'Supervisor',
                        'id'          => 'pleas_nmspv',
                        'name'        => 'pleas_nmspv',
                        'value'       => set_value('pleas_nmspv'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nama'=> array(
                        'placeholder' => 'Konsumen',
                        'id'          => 'pleas_nama',
                        'name'        => 'pleas_nama',
                        'value'       => set_value('pleas_nama'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum();',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_alamat'=> array( 
                        'placeholder' => 'Alamat',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_alamat',
                        'name'        => 'pleas_alamat',
                        'value'       => set_value('pleas_alamat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_kdleas'=> array( 
                        'placeholder' => 'Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_kdleas',
                        'name'        => 'pleas_kdleas',
                        'value'       => set_value('pleas_kdleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_kdprogleas'=> array( 
                        'placeholder' => 'Prog. Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_kdprogleas',
                        'name'        => 'pleas_kdprogleas',
                        'value'       => set_value('pleas_kdprogleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nosin'=> array(
                        'placeholder' => 'No. Mesin/ Rk',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nosin',
                        'name'        => 'pleas_nosin',
                        'value'       => set_value('pleas_nosin'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                        'required'    => '',
                ),
                'pleas_nora'=> array(
                        'placeholder' => 'No. Rangka',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nora',
                        'name'        => 'pleas_nora',
                        'value'       => set_value('pleas_nora'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_kdtipe'=> array(
                        'placeholder' => 'Tipe SMH',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_kdtipe',
                        'name'        => 'pleas_kdtipe',
                        'value'       => set_value('pleas_kdtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_nmtipe'=> array(
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nmtipe',
                        'name'        => 'pleas_nmtipe',
                        'value'       => set_value('pleas_nmtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_warna'=> array(
                        'placeholder' => 'Warna/Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_warna',
                        'name'        => 'pleas_warna',
                        'value'       => set_value('pleas_warna'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_tahun'=> array(
                        'placeholder' => 'Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_tahun',
                        'name'        => 'pleas_tahun',
                        'value'       => set_value('pleas_tahun'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_status'=> array(
                        'placeholder' => 'Status OTR',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_status',
                        'name'        => 'pleas_status',
                        'value'       => set_value('pleas_status'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 

                //nominal pembayaran leasing
                   'pleas_pelunasan'=> array( 
                            'placeholder' => 'Pelunasan',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_pelunasan',
                            'name'        => 'pleas_pelunasan',
                            'value'       => set_value('pleas_pelunasan'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_jp'=> array(
                            'placeholder' => 'JP(Joint Promo)',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_jp',
                            'name'        => 'pleas_jp',
                            'value'       => set_value('pleas_jp'),
                            'class'       => 'form-control',
                            // 'onkeyup'     => 'sum1();',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_byr1'=> array( 
                            'placeholder' => 'Terbayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_byr1',
                            'name'        => 'pleas_byr1',
                            'value'       => set_value('pleas_byr1'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_byr2'=> array( 
                            'placeholder' => 'Terbayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_byr2',
                            'name'        => 'pleas_byr2',
                            'value'       => set_value('pleas_byr2'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_nbyr1'=> array( 
                            'placeholder' => 'Nilai Bayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_nbyr1',
                            'name'        => 'pleas_nbyr1',
                            'value'       => set_value('pleas_nbyr1'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_nbyr2'=> array( 
                            'placeholder' => 'Nilai Bayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_nbyr2',
                            'name'        => 'pleas_nbyr2',
                            'value'       => set_value('pleas_nbyr2'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),  
                   'pleas_nocetak'=> array( 
                            'placeholder' => '',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_nocetak',
                            'name'        => 'pleas_nocetak',
                            'value'       => set_value('pleas_nocetak'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: center;',
                            'readonly'    => '',
                    ), 
                   'pleas_noctk'=> array( 
                            'placeholder' => '',
                            'type'        => 'hidden',
                            'id'          => 'pleas_noctk',
                            'name'        => 'pleas_noctk',
                            'value'       => set_value('pleas_noctk'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: center;',
                            'readonly'    => '',
                    ), 
                   'pleas_jmltrans'=> array( 
                            'placeholder' => 'Jumlah Transaksi',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_jmltrans',
                            'name'        => 'pleas_jmltrans',
                            'value'       => set_value('pleas_jmltrans'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),  

            //detail refund leasing
                'rleas_kdleas'=> array(
                        'placeholder' => 'Leasing',
                        'attr'        => array(
                            'id'    => 'rleas_kdleas',
                            'class' => 'form-control',
                        ),
                        'data'     => $this->data['kdleasing'],
                        'value'    => set_value('rleas_kdleas'),
                        'name'     => 'rleas_kdleas',
                        // 'readonly'    => '',
                ),
                'rleas_jenis'=> array(
                        'placeholder' => 'Jenis',
                        'attr'        => array(
                            'id'    => 'rleas_jenis',
                            'class' => 'form-control',
                        ),
                        'data'     => $this->data['jnsrleas'],
                        'value'    => set_value('rleas_jenis'),
                        'name'     => 'rleas_jenis',
                        // 'readonly'    => '',
                ), 
                'rleas_nilai'=> array( 
                        'placeholder' => 'Nilai',
                        //'type'        => 'hidden',
                        'id'          => 'rleas_nilai',
                        'name'        => 'rleas_nilai',
                        'value'       => set_value('rleas_nilai'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'rleas_ket'=> array( 
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'rleas_ket',
                        'name'        => 'rleas_ket',
                        'value'       => set_value('rleas_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  

            //detail pencairan subsidi
                'psub_kdleas'=> array(
                        'placeholder' => 'Kontibutor',
                        'attr'        => array(
                            'id'    => 'psub_kdleas',
                            'class' => 'form-control',
                        ),
                        'data'     => $this->data['kdleasing'],
                        'value'    => set_value('psub_kdleas'),
                        'name'     => 'psub_kdleas',
                        // 'readonly'    => '',
                ),
                'psub_ncair'=> array(
                        'placeholder' => 'Nilai Pencairan',
                        //'type'        => 'hidden',
                        'id'          => 'psub_ncair',
                        'name'        => 'psub_ncair',
                        'value'       => set_value('psub_ncair'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '', 
                ), 
                'psub_selisih'=> array( 
                        'placeholder' => 'Selisih',
                        //'type'        => 'hidden',
                        'id'          => 'psub_selisih',
                        'name'        => 'psub_selisih',
                        'value'       => set_value('psub_selisih'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'psub_ket'=> array( 
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'psub_ket',
                        'name'        => 'psub_ket',
                        'value'       => set_value('psub_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  
                'psub_totsub'=> array( 
                        'placeholder' => 'Total Subsidi',
                        //'type'        => 'hidden',
                        'id'          => 'psub_totsub',
                        'name'        => 'psub_totsub',
                        'value'       => set_value('psub_totsub'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'psub_selsbg'=> array( 
                        'placeholder' => 'Selisih Sebagai',
                        'attr'        => array(
                            'id'    => 'psub_selsbg',
                            'class' => 'form-control',
                        ),
                        'data'     => '', //$this->data['kdsales_ins'],
                        'value'    => set_value('psub_selsbg'),
                        'name'     => 'psub_selsbg',
                        // 'readonly'    => '',
                ),  

                    //more detail pencairan subsidi
                        'psub_nokasbon'=> array(
                                'placeholder' => 'No. Kasbon',
                                //'type'        => 'hidden',
                                'id'          => 'psub_nokasbon',
                                'name'        => 'psub_nokasbon',
                                'value'       => set_value('psub_nokasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '', 
                        ),
                        'psub_tglkasbon'=> array(
                                'placeholder' => 'Nilai Pencairan',
                                //'type'        => 'hidden',
                                'id'          => 'psub_tglkasbon',
                                'name'        => 'psub_tglkasbon',
                                'value'       => date('d-m-Y'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '', 
                        ), 
                        'psub_ketkasbon'=> array( 
                                'placeholder' => 'Keterangan Kasbon',
                                //'type'        => 'hidden',
                                'id'          => 'psub_ketkasbon',
                                'name'        => 'psub_ketkasbon',
                                'value'       => set_value('psub_ketkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '',
                        ), 
                        'psub_catkasbon'=> array( 
                                'placeholder' => 'Catatan',
                                //'type'        => 'hidden',
                                'id'          => 'psub_catkasbon',
                                'name'        => 'psub_catkasbon',
                                'value'       => set_value('psub_catkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '',
                        ),  
                        'psub_nsubkasbon'=> array( 
                                'placeholder' => 'Nilai Subsidi',
                                //'type'        => 'hidden',
                                'id'          => 'psub_nsubkasbon',
                                'name'        => 'psub_nsubkasbon',
                                'value'       => set_value('psub_nsubkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: right;',
                                'readonly'    => '',
                        ),  
                        'psub_ncairkasbon'=> array( 
                                'placeholder' => 'Nilai Pencairan',
                                //'type'        => 'hidden',
                                'id'          => 'psub_ncairkasbon',
                                'name'        => 'psub_ncairkasbon',
                                'value'       => set_value('psub_ncairkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: right;',
                                'readonly'    => '',
                        ),  

            //detail pengembalian konsumen
                'blkkons_noso'=> array(
                        'placeholder' => 'No. SPK',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_noso',
                        'name'        => 'blkkons_noso',
                        'value'       => set_value('blkkons_noso'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_nodo'=> array(
                        'placeholder' => 'No. DO',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nodo',
                        'name'        => 'blkkons_nodo',
                        'value'       => set_value('nodo'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_tglso'=> array(
                        'placeholder' => 'Tanggal SPK',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_tglso',
                        'name'        => 'blkkons_tglso',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_tgldo',
                        'name'        => 'blkkons_tgldo',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nmsales'=> array(
                        'placeholder' => 'Sales',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nmsales',
                        'name'        => 'blkkons_nmsales',
                        'value'       => set_value('blkkons_nmsales'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nmspv'=> array(
                        'placeholder' => 'Supervisor',
                        'id'          => 'blkkons_nmspv',
                        'name'        => 'blkkons_nmspv',
                        'value'       => set_value('blkkons_nmspv'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nama'=> array(
                        'placeholder' => 'Konsumen',
                        'id'          => 'blkkons_nama',
                        'name'        => 'blkkons_nama',
                        'value'       => set_value('blkkons_nama'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum();',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_alamat'=> array( 
                        'placeholder' => 'Alamat',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_alamat',
                        'name'        => 'blkkons_alamat',
                        'value'       => set_value('blkkons_alamat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_kdleas'=> array( 
                        'placeholder' => 'Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_kdleas',
                        'name'        => 'blkkons_kdleas',
                        'value'       => set_value('blkkons_kdleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_kdprogleas'=> array( 
                        'placeholder' => 'Prog. Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_kdprogleas',
                        'name'        => 'blkkons_kdprogleas',
                        'value'       => set_value('blkkons_kdprogleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nosin'=> array(
                        'placeholder' => 'No. Mesin/ Rk',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nosin',
                        'name'        => 'blkkons_nosin',
                        'value'       => set_value('blkkons_nosin'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                        'required'    => '',
                ),
                'blkkons_nora'=> array(
                        'placeholder' => 'No. Rangka',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nora',
                        'name'        => 'blkkons_nora',
                        'value'       => set_value('blkkons_nora'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_kdtipe'=> array(
                        'placeholder' => 'Tipe SMH',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_kdtipe',
                        'name'        => 'blkkons_kdtipe',
                        'value'       => set_value('blkkons_kdtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_nmtipe'=> array(
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nmtipe',
                        'name'        => 'blkkons_nmtipe',
                        'value'       => set_value('blkkons_nmtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_warna'=> array(
                        'placeholder' => 'Warna/Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_warna',
                        'name'        => 'blkkons_warna',
                        'value'       => set_value('blkkons_warna'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_tahun'=> array(
                        'placeholder' => 'Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_tahun',
                        'name'        => 'blkkons_tahun',
                        'value'       => set_value('blkkons_tahun'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_status'=> array(
                        'placeholder' => 'Status OTR',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_status',
                        'name'        => 'blkkons_status',
                        'value'       => set_value('blkkons_status'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 

                    //nominal pembayaran konsumen
                    'blkkons_hjnet'=> array(
                            'placeholder' => 'Harga Jual Net',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_hjnet',
                            'name'        => 'blkkons_hjnet',
                            'value'       => set_value('blkkons_hjnet'),
                            'class'       => 'form-control',
                            // 'onkeyup'     => 'sum1();',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),
                    'blkkons_pl'=> array(
                            'placeholder' => 'AR Pelunasan',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_pl',
                            'name'        => 'blkkons_pl',
                            'value'       => set_value('blkkons_pl'),
                            'class'       => 'form-control',
                            // 'onkeyup'     => 'sum1();',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                    'blkkons_byr1'=> array( 
                            'placeholder' => 'Terbayar',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_byr1',
                            'name'        => 'blkkons_byr1',
                            'value'       => set_value('blkkons_byr1'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),  
                    'blkkons_pengembalian'=> array( 
                            'placeholder' => 'Pengembalian',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_pengembalian',
                            'name'        => 'blkkons_pengembalian',
                            'value'       => set_value('blkkons_pengembalian'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                    'blkkons_lbhbyr'=> array( 
                            'placeholder' => 'Lebih Bayar',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_lbhbyr',
                            'name'        => 'blkkons_lbhbyr',
                            'value'       => set_value('blkkons_lbhbyr'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),   

            //detail realisasi kasbon
                'rkasbon_nokasbon'=> array(
                        'placeholder' => 'No. Kasbon',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_nokasbon',
                        'name'        => 'rkasbon_nokasbon',
                        'value'       => set_value('rkasbon_nokasbon'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        // 'readonly'    => '', 
                ), 
                'rkasbon_tglkasbon'=> array( 
                        'placeholder' => 'Tanggal Kasbon',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_tglkasbon',
                        'name'        => 'rkasbon_tglkasbon',
                        'value'       => date("d-m-Y"),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ), 
                'rkasbon_kb'=> array( 
                        'placeholder' => 'Kas/Bank',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_kb',
                        'name'        => 'rkasbon_kb',
                        'value'       => set_value('rkasbon_kb'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  
                'rkasbon_nama'=> array( 
                        'placeholder' => 'Penerima',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_nama',
                        'name'        => 'rkasbon_nama',
                        'value'       => set_value('rkasbon_nama'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ), 
                'rkasbon_ket'=> array( 
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_ket',
                        'name'        => 'rkasbon_ket',
                        'value'       => set_value('rkasbon_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  
                'rkasbon_nkasbon'=> array( 
                        'placeholder' => 'Nilai Kasbon',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_nkasbon',
                        'name'        => 'rkasbon_nkasbon',
                        'value'       => set_value('rkasbon_nkasbon'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),   

        );
    }
    
    private function _init_edit(){
        // if(!$no){
        //     $nokb = $this->uri->segment(3);
        // }
        // $this->_check_id($nokb); 
        $this->data['form'] = array(
            //header
           'nokb'=> array(
                    'placeholder' => 'No. Kas/Bank',
                    //'type'        => 'hidden',
                    'id'          => 'nokb',
                    'name'        => 'nokb',
                    'value'       => set_value('nokb'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ),
            'refkb'=> array(
                    'placeholder' => 'Ref. Kas/Bank',
                    'attr'        => array(
                        'id'    => 'refkb',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kdkb'],
                    'value'    => set_value('refkb'),
                    'name'     => 'refkb',
                    // 'readonly'    => '',
            ),
            'jnstrans'=> array(
                    'placeholder' => 'Jenis Transaksi',
                    'attr'        => array(
                        'id'    => 'jnstrans',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['jnstrx'], //$this->data['kdsales_ins'],
                    'value'    => set_value('jnstrans'),
                    'name'     => 'jnstrans',
                    // 'readonly'    => '',
            ),
            'dk'=> array(
                    'placeholder' => '',
                    'type'        => 'hidden',
                    'id'          => 'dk',
                    'name'        => 'dk',
                    'value'       => set_value('dk'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'day'=> array(
                    'placeholder' => '',
                    //'type'        => 'hidden',
                    'id'          => 'day',
                    'name'        => 'day',
                    'value'       => set_value('day'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 
            'periode'=> array(
                    'placeholder' => 'PERIODE KAS/BANK',
                    //'type'        => 'hidden',
                    'id'          => 'periode',
                    'name'        => 'periode',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'readonly'    => '',
            ), 

            //detail pembayaran konsumen
                'byrkons_cari'=> array(
                        'placeholder' => 'Cari <small> ( No.SPK / Nama / No. DO ) </small>',
                        //'type'        => 'hidden',
                        'attr'        => array(
                            'id'    => 'byrkons_cari',
                            'class' => 'form-control',
                        ),
                        'data'     => '', //$this->data['kdsales_ins'],
                        'value'    => set_value('byrkons_cari'),
                        'name'     => 'byrkons_cari',
                        // 'readonly'    => '',
                ),
                'byrkons_noso'=> array(
                        'placeholder' => 'No. SPK',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_noso',
                        'name'        => 'byrkons_noso',
                        'value'       => set_value('byrkons_noso'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ),
                'byrkons_nodo'=> array(
                        'placeholder' => 'No. DO',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nodo',
                        'name'        => 'byrkons_nodo',
                        'value'       => set_value('nodo'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'byrkons_tglso'=> array(
                        'placeholder' => 'Tanggal SPK',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_tglso',
                        'name'        => 'byrkons_tglso',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_tgldo',
                        'name'        => 'byrkons_tgldo',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nmsales'=> array(
                        'placeholder' => 'Sales',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nmsales',
                        'name'        => 'byrkons_nmsales',
                        'value'       => set_value('byrkons_nmsales'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nmspv'=> array(
                        'placeholder' => 'Supervisor',
                        'id'          => 'byrkons_nmspv',
                        'name'        => 'byrkons_nmspv',
                        'value'       => set_value('byrkons_nmspv'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nama'=> array(
                        'placeholder' => 'Konsumen',
                        'id'          => 'byrkons_nama',
                        'name'        => 'byrkons_nama',
                        'value'       => set_value('byrkons_nama'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum();',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_alamat'=> array( 
                        'placeholder' => 'Alamat',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_alamat',
                        'name'        => 'byrkons_alamat',
                        'value'       => set_value('byrkons_alamat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_kdleas'=> array( 
                        'placeholder' => 'Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_kdleas',
                        'name'        => 'byrkons_kdleas',
                        'value'       => set_value('byrkons_kdleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_kdprogleas'=> array( 
                        'placeholder' => 'Prog. Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_kdprogleas',
                        'name'        => 'byrkons_kdprogleas',
                        'value'       => set_value('byrkons_kdprogleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_nosin'=> array(
                        'placeholder' => 'No. Mesin/ Rk',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nosin',
                        'name'        => 'byrkons_nosin',
                        'value'       => set_value('byrkons_nosin'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                        'required'    => '',
                ),
                'byrkons_nora'=> array(
                        'placeholder' => 'No. Rangka',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nora',
                        'name'        => 'byrkons_nora',
                        'value'       => set_value('byrkons_nora'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_kdtipe'=> array(
                        'placeholder' => 'Tipe SMH',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_kdtipe',
                        'name'        => 'byrkons_kdtipe',
                        'value'       => set_value('byrkons_kdtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_nmtipe'=> array(
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nmtipe',
                        'name'        => 'byrkons_nmtipe',
                        'value'       => set_value('byrkons_nmtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_warna'=> array(
                        'placeholder' => 'Warna/Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_warna',
                        'name'        => 'byrkons_warna',
                        'value'       => set_value('byrkons_warna'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'byrkons_tahun'=> array(
                        'placeholder' => 'Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_tahun',
                        'name'        => 'byrkons_tahun',
                        'value'       => set_value('byrkons_tahun'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'byrkons_status'=> array(
                        'placeholder' => 'Status OTR',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_status',
                        'name'        => 'byrkons_status',
                        'value'       => set_value('byrkons_status'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 

                //nominal pembayaran konsumen
                'byrkons_hjnet'=> array(
                        'placeholder' => 'Harga Jual Net',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_hjnet',
                        'name'        => 'byrkons_hjnet',
                        'value'       => set_value('byrkons_hjnet'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum1();',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),
                'byrkons_um'=> array(
                        'placeholder' => 'Uang Muka',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_um',
                        'name'        => 'byrkons_um',
                        'value'       => set_value('byrkons_um'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum1();',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),
                'byrkons_pelunasan'=> array( 
                        'placeholder' => 'Pelunasan',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_pelunasan',
                        'name'        => 'byrkons_pelunasan',
                        'value'       => set_value('byrkons_pelunasan'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_byr1'=> array( 
                        'placeholder' => 'Terbayar',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_byr1',
                        'name'        => 'byrkons_byr1',
                        'value'       => set_value('byrkons_byr1'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_byr2'=> array( 
                        'placeholder' => 'Terbayar',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_byr2',
                        'name'        => 'byrkons_byr2',
                        'value'       => set_value('byrkons_byr2'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_nbyr'=> array( 
                        'placeholder' => 'Nilai Bayar',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nbyr',
                        'name'        => 'byrkons_nbyr',
                        'value'       => set_value('byrkons_nbyr'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        // 'readonly'    => '',
                ), 
                'byrkons_saldoum'=> array( 
                        'placeholder' => 'Saldo Uang Muka',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_saldoum',
                        'name'        => 'byrkons_saldoum',
                        'value'       => set_value('byrkons_saldoum'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'byrkons_saldo_pelunasan'=> array( 
                        'placeholder' => 'Saldo Pelunasan',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_saldo_pelunasan',
                        'name'        => 'byrkons_saldo_pelunasan',
                        'value'       => set_value('byrkons_saldo_pelunasan'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),  
                'byrkons_nocetak'=> array( 
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_nocetak',
                        'name'        => 'byrkons_nocetak',
                        'value'       => set_value('byrkons_nocetak'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: center;',
                        'readonly'    => '',
                ), 
                'byrkons_noctk'=> array( 
                        'placeholder' => '',
                        'type'        => 'hidden',
                        'id'          => 'byrkons_noctk',
                        'name'        => 'byrkons_noctk',
                        'value'       => set_value('byrkons_noctk'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: center;',
                        'readonly'    => '',
                ), 
                'byrkons_jmltrans'=> array( 
                        'placeholder' => 'Jumlah Transaksi',
                        //'type'        => 'hidden',
                        'id'          => 'byrkons_jmltrans',
                        'name'        => 'byrkons_jmltrans',
                        'value'       => set_value('byrkons_jmltrans'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),  

            //detail pembayaran leasing
                'pleas_cari'=> array(
                        'placeholder' => 'Cari <small> ( No.SPK / Nama / No. DO ) </small>',
                        //'type'        => 'hidden',
                        'attr'        => array(
                            'id'    => 'pleas_cari',
                            'class' => 'form-control',
                        ),
                        'data'     => '', //$this->data['kdsales_ins'],
                        'value'    => set_value('pleas_cari'),
                        'name'     => 'pleas_cari',
                        // 'readonly'    => '',
                ),
                'pleas_noso'=> array(
                        'placeholder' => 'No. SPK',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_noso',
                        'name'        => 'pleas_noso',
                        'value'       => set_value('pleas_noso'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ),
                'pleas_nodo'=> array(
                        'placeholder' => 'No. DO',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nodo',
                        'name'        => 'pleas_nodo',
                        'value'       => set_value('nodo'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'pleas_tglso'=> array(
                        'placeholder' => 'Tanggal SPK',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_tglso',
                        'name'        => 'pleas_tglso',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_tgldo',
                        'name'        => 'pleas_tgldo',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nmsales'=> array(
                        'placeholder' => 'Sales',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nmsales',
                        'name'        => 'pleas_nmsales',
                        'value'       => set_value('pleas_nmsales'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nmspv'=> array(
                        'placeholder' => 'Supervisor',
                        'id'          => 'pleas_nmspv',
                        'name'        => 'pleas_nmspv',
                        'value'       => set_value('pleas_nmspv'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nama'=> array(
                        'placeholder' => 'Konsumen',
                        'id'          => 'pleas_nama',
                        'name'        => 'pleas_nama',
                        'value'       => set_value('pleas_nama'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum();',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_alamat'=> array( 
                        'placeholder' => 'Alamat',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_alamat',
                        'name'        => 'pleas_alamat',
                        'value'       => set_value('pleas_alamat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_kdleas'=> array( 
                        'placeholder' => 'Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_kdleas',
                        'name'        => 'pleas_kdleas',
                        'value'       => set_value('pleas_kdleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_kdprogleas'=> array( 
                        'placeholder' => 'Prog. Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_kdprogleas',
                        'name'        => 'pleas_kdprogleas',
                        'value'       => set_value('pleas_kdprogleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_nosin'=> array(
                        'placeholder' => 'No. Mesin/ Rk',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nosin',
                        'name'        => 'pleas_nosin',
                        'value'       => set_value('pleas_nosin'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                        'required'    => '',
                ),
                'pleas_nora'=> array(
                        'placeholder' => 'No. Rangka',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nora',
                        'name'        => 'pleas_nora',
                        'value'       => set_value('pleas_nora'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_kdtipe'=> array(
                        'placeholder' => 'Tipe SMH',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_kdtipe',
                        'name'        => 'pleas_kdtipe',
                        'value'       => set_value('pleas_kdtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_nmtipe'=> array(
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_nmtipe',
                        'name'        => 'pleas_nmtipe',
                        'value'       => set_value('pleas_nmtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_warna'=> array(
                        'placeholder' => 'Warna/Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_warna',
                        'name'        => 'pleas_warna',
                        'value'       => set_value('pleas_warna'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'pleas_tahun'=> array(
                        'placeholder' => 'Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_tahun',
                        'name'        => 'pleas_tahun',
                        'value'       => set_value('pleas_tahun'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'pleas_status'=> array(
                        'placeholder' => 'Status OTR',
                        //'type'        => 'hidden',
                        'id'          => 'pleas_status',
                        'name'        => 'pleas_status',
                        'value'       => set_value('pleas_status'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 

                //nominal pembayaran leasing
                   'pleas_pelunasan'=> array( 
                            'placeholder' => 'Pelunasan',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_pelunasan',
                            'name'        => 'pleas_pelunasan',
                            'value'       => set_value('pleas_pelunasan'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_jp'=> array(
                            'placeholder' => 'JP(Joint Promo)',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_jp',
                            'name'        => 'pleas_jp',
                            'value'       => set_value('pleas_jp'),
                            'class'       => 'form-control',
                            // 'onkeyup'     => 'sum1();',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_byr1'=> array( 
                            'placeholder' => 'Terbayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_byr1',
                            'name'        => 'pleas_byr1',
                            'value'       => set_value('pleas_byr1'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_byr2'=> array( 
                            'placeholder' => 'Terbayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_byr2',
                            'name'        => 'pleas_byr2',
                            'value'       => set_value('pleas_byr2'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                   'pleas_nbyr1'=> array( 
                            'placeholder' => 'Nilai Bayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_nbyr1',
                            'name'        => 'pleas_nbyr1',
                            'value'       => set_value('pleas_nbyr1'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            // 'readonly'    => '',
                    ), 
                   'pleas_nbyr2'=> array( 
                            'placeholder' => 'Nilai Bayar',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_nbyr2',
                            'name'        => 'pleas_nbyr2',
                            'value'       => set_value('pleas_nbyr2'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            // 'readonly'    => '',
                    ),  
                   'pleas_nocetak'=> array( 
                            'placeholder' => '',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_nocetak',
                            'name'        => 'pleas_nocetak',
                            'value'       => set_value('pleas_nocetak'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: center;',
                            'readonly'    => '',
                    ), 
                   'pleas_noctk'=> array( 
                            'placeholder' => '',
                            'type'        => 'hidden',
                            'id'          => 'pleas_noctk',
                            'name'        => 'pleas_noctk',
                            'value'       => set_value('pleas_noctk'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: center;',
                            'readonly'    => '',
                    ), 
                   'pleas_jmltrans'=> array( 
                            'placeholder' => 'Jumlah Transaksi',
                            //'type'        => 'hidden',
                            'id'          => 'pleas_jmltrans',
                            'name'        => 'pleas_jmltrans',
                            'value'       => set_value('pleas_jmltrans'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),  

            //detail refund leasing
                'rleas_kdleas'=> array(
                        'placeholder' => 'Leasing',
                        'attr'        => array(
                            'id'    => 'rleas_kdleas',
                            'class' => 'form-control',
                        ),
                        'data'     => $this->data['kdleasing'],
                        'value'    => set_value('rleas_kdleas'),
                        'name'     => 'rleas_kdleas',
                        // 'readonly'    => '',
                ),
                'rleas_jenis'=> array(
                        'placeholder' => 'Jenis',
                        'attr'        => array(
                            'id'    => 'rleas_jenis',
                            'class' => 'form-control',
                        ),
                        'data'     => $this->data['jnsrleas'],
                        'value'    => set_value('rleas_jenis'),
                        'name'     => 'rleas_jenis',
                        // 'readonly'    => '',
                ), 
                'rleas_nilai'=> array( 
                        'placeholder' => 'Nilai',
                        //'type'        => 'hidden',
                        'id'          => 'rleas_nilai',
                        'name'        => 'rleas_nilai',
                        'value'       => set_value('rleas_nilai'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        // 'readonly'    => '',
                ), 
                'rleas_ket'=> array( 
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'rleas_ket',
                        'name'        => 'rleas_ket',
                        'value'       => set_value('rleas_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left; text-transform: uppercase;',
                        // 'readonly'    => '',
                ),  

            //detail pencairan subsidi
                'psub_kdleas'=> array(
                        'placeholder' => 'Kontibutor',
                        'attr'        => array(
                            'id'    => 'psub_kdleas',
                            'class' => 'form-control',
                        ),
                        'data'     => $this->data['kdleasing'],
                        'value'    => set_value('psub_kdleas'),
                        'name'     => 'psub_kdleas',
                        // 'readonly'    => '',
                ),
                'psub_ncair'=> array(
                        'placeholder' => 'Nilai Pencairan',
                        //'type'        => 'hidden',
                        'id'          => 'psub_ncair',
                        'name'        => 'psub_ncair',
                        'value'       => set_value('psub_ncair'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '', 
                ), 
                'psub_selisih'=> array( 
                        'placeholder' => 'Selisih',
                        //'type'        => 'hidden',
                        'id'          => 'psub_selisih',
                        'name'        => 'psub_selisih',
                        'value'       => set_value('psub_selisih'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'psub_ket'=> array( 
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'psub_ket',
                        'name'        => 'psub_ket',
                        'value'       => set_value('psub_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  
                'psub_totsub'=> array( 
                        'placeholder' => 'Total Subsidi',
                        //'type'        => 'hidden',
                        'id'          => 'psub_totsub',
                        'name'        => 'psub_totsub',
                        'value'       => set_value('psub_totsub'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ), 
                'psub_selsbg'=> array( 
                        'placeholder' => 'Selisih Sebagai',
                        'attr'        => array(
                            'id'    => 'psub_selsbg',
                            'class' => 'form-control',
                        ),
                        'data'     => '', //$this->data['kdsales_ins'],
                        'value'    => set_value('psub_selsbg'),
                        'name'     => 'psub_selsbg',
                        // 'readonly'    => '',
                ),  

                    //more detail pencairan subsidi
                        'psub_nokasbon'=> array(
                                'placeholder' => 'No. Kasbon',
                                //'type'        => 'hidden',
                                'id'          => 'psub_nokasbon',
                                'name'        => 'psub_nokasbon',
                                'value'       => set_value('psub_nokasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '', 
                        ),
                        'psub_tglkasbon'=> array(
                                'placeholder' => 'Nilai Pencairan',
                                //'type'        => 'hidden',
                                'id'          => 'psub_tglkasbon',
                                'name'        => 'psub_tglkasbon',
                                'value'       => date('d-m-Y'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '', 
                        ), 
                        'psub_ketkasbon'=> array( 
                                'placeholder' => 'Keterangan Kasbon',
                                //'type'        => 'hidden',
                                'id'          => 'psub_ketkasbon',
                                'name'        => 'psub_ketkasbon',
                                'value'       => set_value('psub_ketkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '',
                        ), 
                        'psub_catkasbon'=> array( 
                                'placeholder' => 'Catatan',
                                //'type'        => 'hidden',
                                'id'          => 'psub_catkasbon',
                                'name'        => 'psub_catkasbon',
                                'value'       => set_value('psub_catkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: left;',
                                'readonly'    => '',
                        ),  
                        'psub_nsubkasbon'=> array( 
                                'placeholder' => 'Nilai Subsidi',
                                //'type'        => 'hidden',
                                'id'          => 'psub_nsubkasbon',
                                'name'        => 'psub_nsubkasbon',
                                'value'       => set_value('psub_nsubkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: right;',
                                'readonly'    => '',
                        ),  
                        'psub_ncairkasbon'=> array( 
                                'placeholder' => 'Nilai Pencairan',
                                //'type'        => 'hidden',
                                'id'          => 'psub_ncairkasbon',
                                'name'        => 'psub_ncairkasbon',
                                'value'       => set_value('psub_ncairkasbon'),
                                'class'       => 'form-control',
                                'style'       => 'text-align: right;',
                                'readonly'    => '',
                        ),  

            //detail pengembalian konsumen
                'blkkons_cari'=> array(
                        'placeholder' => 'Cari <small> ( No.SPK / Nama / No. DO ) </small>',
                        //'type'        => 'hidden',
                        'attr'        => array(
                            'id'    => 'blkkons_cari',
                            'class' => 'form-control',
                        ),
                        'data'     => '', //$this->data['kdsales_ins'],
                        'value'    => set_value('blkkons_cari'),
                        'name'     => 'blkkons_cari',
                        // 'readonly'    => '',
                ),
                'blkkons_noso'=> array(
                        'placeholder' => 'No. SPK',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_noso',
                        'name'        => 'blkkons_noso',
                        'value'       => set_value('blkkons_noso'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ),
                'blkkons_nodo'=> array(
                        'placeholder' => 'No. DO',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nodo',
                        'name'        => 'blkkons_nodo',
                        'value'       => set_value('nodo'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'blkkons_tglso'=> array(
                        'placeholder' => 'Tanggal SPK',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_tglso',
                        'name'        => 'blkkons_tglso',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_tgldo'=> array(
                        'placeholder' => 'Tanggal DO',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_tgldo',
                        'name'        => 'blkkons_tgldo',
                        'value'       => date('d-m-Y'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nmsales'=> array(
                        'placeholder' => 'Sales',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nmsales',
                        'name'        => 'blkkons_nmsales',
                        'value'       => set_value('blkkons_nmsales'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nmspv'=> array(
                        'placeholder' => 'Supervisor',
                        'id'          => 'blkkons_nmspv',
                        'name'        => 'blkkons_nmspv',
                        'value'       => set_value('blkkons_nmspv'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nama'=> array(
                        'placeholder' => 'Konsumen',
                        'id'          => 'blkkons_nama',
                        'name'        => 'blkkons_nama',
                        'value'       => set_value('blkkons_nama'),
                        'class'       => 'form-control',
                        // 'onkeyup'     => 'sum();',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_alamat'=> array( 
                        'placeholder' => 'Alamat',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_alamat',
                        'name'        => 'blkkons_alamat',
                        'value'       => set_value('blkkons_alamat'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_kdleas'=> array( 
                        'placeholder' => 'Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_kdleas',
                        'name'        => 'blkkons_kdleas',
                        'value'       => set_value('blkkons_kdleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_kdprogleas'=> array( 
                        'placeholder' => 'Prog. Leasing',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_kdprogleas',
                        'name'        => 'blkkons_kdprogleas',
                        'value'       => set_value('blkkons_kdprogleas'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_nosin'=> array(
                        'placeholder' => 'No. Mesin/ Rk',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nosin',
                        'name'        => 'blkkons_nosin',
                        'value'       => set_value('blkkons_nosin'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                        'required'    => '',
                ),
                'blkkons_nora'=> array(
                        'placeholder' => 'No. Rangka',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nora',
                        'name'        => 'blkkons_nora',
                        'value'       => set_value('blkkons_nora'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_kdtipe'=> array(
                        'placeholder' => 'Tipe SMH',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_kdtipe',
                        'name'        => 'blkkons_kdtipe',
                        'value'       => set_value('blkkons_kdtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_nmtipe'=> array(
                        'placeholder' => '',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_nmtipe',
                        'name'        => 'blkkons_nmtipe',
                        'value'       => set_value('blkkons_nmtipe'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_warna'=> array(
                        'placeholder' => 'Warna/Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_warna',
                        'name'        => 'blkkons_warna',
                        'value'       => set_value('blkkons_warna'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ),
                'blkkons_tahun'=> array(
                        'placeholder' => 'Tahun',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_tahun',
                        'name'        => 'blkkons_tahun',
                        'value'       => set_value('blkkons_tahun'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 
                'blkkons_status'=> array(
                        'placeholder' => 'Status OTR',
                        //'type'        => 'hidden',
                        'id'          => 'blkkons_status',
                        'name'        => 'blkkons_status',
                        'value'       => set_value('blkkons_status'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        'readonly'    => '',
                ), 

                    //nominal pembayaran konsumen
                    'blkkons_hjnet'=> array(
                            'placeholder' => 'Harga Jual Net',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_hjnet',
                            'name'        => 'blkkons_hjnet',
                            'value'       => set_value('blkkons_hjnet'),
                            'class'       => 'form-control',
                            // 'onkeyup'     => 'sum1();',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),
                    'blkkons_arum'=> array(
                            'placeholder' => 'AR Uang Muka',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_arum',
                            'name'        => 'blkkons_arum',
                            'value'       => set_value('blkkons_arum'),
                            'class'       => 'form-control',
                            // 'onkeyup'     => 'sum1();',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ), 
                    'blkkons_byr1'=> array( 
                            'placeholder' => 'Terbayar',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_byr1',
                            'name'        => 'blkkons_byr1',
                            'value'       => set_value('blkkons_byr1'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),  
                    'blkkons_pengembalian'=> array( 
                            'placeholder' => 'Pengembalian',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_pengembalian',
                            'name'        => 'blkkons_pengembalian',
                            'value'       => set_value('blkkons_pengembalian'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            // 'readonly'    => '',
                    ), 
                    'blkkons_lbhbyr'=> array( 
                            'placeholder' => 'Lebih Bayar',
                            //'type'        => 'hidden',
                            'id'          => 'blkkons_lbhbyr',
                            'name'        => 'blkkons_lbhbyr',
                            'value'       => set_value('blkkons_lbhbyr'),
                            'class'       => 'form-control',
                            'style'       => 'text-align: right;',
                            'readonly'    => '',
                    ),   

            //detail realisasi kasbon
                'rkasbon_nokasbon'=> array(
                        'placeholder' => 'No. Kasbon',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_nokasbon',
                        'name'        => 'rkasbon_nokasbon',
                        'value'       => set_value('rkasbon_nokasbon'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        // 'readonly'    => '', 
                ), 
                'rkasbon_tglkasbon'=> array( 
                        'placeholder' => 'Tanggal Kasbon',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_tglkasbon',
                        'name'        => 'rkasbon_tglkasbon',
                        'value'       => date("d-m-Y"),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ), 
                'rkasbon_kb'=> array( 
                        'placeholder' => 'Kas/Bank',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_kb',
                        'name'        => 'rkasbon_kb',
                        'value'       => set_value('rkasbon_kb'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  
                'rkasbon_nama'=> array( 
                        'placeholder' => 'Penerima',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_nama',
                        'name'        => 'rkasbon_nama',
                        'value'       => set_value('rkasbon_nama'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ), 
                'rkasbon_ket'=> array( 
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_ket',
                        'name'        => 'rkasbon_ket',
                        'value'       => set_value('rkasbon_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: left;',
                        'readonly'    => '',
                ),  
                'rkasbon_nkasbon'=> array( 
                        'placeholder' => 'Nilai Kasbon',
                        //'type'        => 'hidden',
                        'id'          => 'rkasbon_nkasbon',
                        'name'        => 'rkasbon_nkasbon',
                        'value'       => set_value('rkasbon_nkasbon'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),   

            //detail kas bank masuk umum
                'kbm_nourut'=> array( 
                        'placeholder' => 'No Urut',
                        'type'        => 'hidden',
                        'id'          => 'kbm_nourut',
                        'name'        => 'rkasbon_nkasbon',
                        'value'       => set_value('kbm_nourut'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        'readonly'    => '',
                ),   

                'kbm_jns'=> array(
                        'placeholder'   => 'Jenis Penerimaan',
                        'attr'        => array(
                            'id'      => 'kbm_jns',
                            'class'   => 'form-control',
                        ),
                        'data'        => '',//$this->data['kddebet'],
                        'value'       => set_value('kbm_jns'),
                        'name'        => 'kbm_jns',
                        // 'readonly'    => '',
                ),
                'kbm_drkpd'=> array(
                        'placeholder' => 'Diterima Dari',
                        //'type'        => 'hidden',
                        'id'          => 'kbm_drkpd',
                        'name'        => 'kbm_drkpd',
                        'value'       => set_value('kbm_drkpd'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'kbm_nofaktur'=> array(
                        'placeholder' => 'No. Faktur/Nota',
                        //'type'        => 'hidden',
                        'id'          => 'kbm_nofaktur',
                        'name'        => 'kbm_nofaktur',
                        'value'       => set_value('kbm_nofaktur'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'kbm_ket'=> array(
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'kbm_ket',
                        'name'        => 'kbm_ket',
                        'value'       => set_value('kbm_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'kbm_nilai'=> array(
                        'placeholder' => 'Nilai',
                        //'type'        => 'hidden',
                        'id'          => 'kbm_nilai',
                        'name'        => 'kbm_nilai',
                        'value'       => set_value('kbm_nilai'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        // 'readonly'    => '',
                ), 

            //detail kas bank keluar umum
                'kbk_jns'=> array(
                        'placeholder'   => 'Jenis Penerimaan',
                        'attr'        => array(
                            'id'      => 'kbk_jns',
                            'class'   => 'form-control select2',
                        ),
                        'data'        => '',//$this->data['kddebet'],
                        'value'       => set_value('kbk_jns'),
                        'name'        => 'kbk_jns',
                        // 'readonly'    => '',
                ),
                'kbk_drkpd'=> array(
                        'placeholder' => 'Diterima Dari',
                        //'type'        => 'hidden',
                        'id'          => 'kbk_drkpd',
                        'name'        => 'kbk_drkpd',
                        'value'       => set_value('kbk_drkpd'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'kbk_nofaktur'=> array(
                        'placeholder' => 'No. Faktur/Nota',
                        //'type'        => 'hidden',
                        'id'          => 'kbk_nofaktur',
                        'name'        => 'kbk_nofaktur',
                        'value'       => set_value('kbk_nofaktur'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'kbk_ket'=> array(
                        'placeholder' => 'Keterangan',
                        //'type'        => 'hidden',
                        'id'          => 'kbk_ket',
                        'name'        => 'kbk_ket',
                        'value'       => set_value('kbk_ket'),
                        'class'       => 'form-control',
                        'style'       => 'text-transform: uppercase;',
                        // 'readonly'    => '',
                ), 
                'kbk_nilai'=> array(
                        'placeholder' => 'Nilai',
                        //'type'        => 'hidden',
                        'id'          => 'kbk_nilai',
                        'name'        => 'kbk_nilai',
                        'value'       => set_value('kbk_nilai'),
                        'class'       => 'form-control',
                        'style'       => 'text-align: right;',
                        // 'readonly'    => '',
                ), 
        );
    }

    private function _check_id($nokb){
        if(empty($nokb)){
            redirect($this->data['add']);
        }

        $this->val= $this->tkbbkl_qry->select_data($nokb);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    } 

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstaksgr
 *
 * @author adi
 */
class Mstaksgr extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstaksgr/submit'),
            'add' => site_url('mstaksgr/add'),
            'edit' => site_url('mstaksgr/edit'),
            'reload' => site_url('mstaksgr'),
        );
        $this->load->model('mstaksgr_qry');
        $kdakun = $this->mstaksgr_qry->getKodeAkun();
        foreach ($kdakun as $value) {
            $this->data['kdakun'][$value['kdakun']] = $value['nmakun'];
        }

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->mstaksgr_qry->json_dgview();
    }

    public function submit() {
        echo $this->mstaksgr_qry->submit();
    }

    public function update() {
        echo $this->mstaksgr_qry->update();
    }

    public function delete() {
        echo $this->mstaksgr_qry->delete();
    }

    private function _init_add(){

        if(isset($_POST['faktif']) && strtoupper($_POST['faktif']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdaksgr'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Kelompok Aksesoris',
                    'id'          => 'kdaksgr',
                    'name'        => 'kdaksgr',
                    'value'       => set_value('kdaksgr'),
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmaksgr'=> array(
                    'placeholder' => 'Nama Kelompok Aksesoris',
                    'id'      => 'nmaksgr',
                    'name'        => 'nmaksgr',
                    'value'       => set_value('nmaksgr'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdakun'],
                    'value'    => set_value('kdakun'),
                    'name'     => 'kdakun',
                    'placeholder' => 'Pilih Akun',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => '',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktif,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdaks = $this->uri->segment(3);
        }
        $this->_check_id($kdaks);

        if($this->val[0]['faktif'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdaksgr'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Kelompok Aksesoris',
                    'id'          => 'kdaksgr',
                    'name'        => 'kdaksgr',
                    'value'       => $this->val[0]['kdaksgr'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmaksgr'=> array(
                    'placeholder' => 'Nama Kelompok Aksesoris',
                    'id'          => 'nmaksgr',
                    'name'        => 'nmaksgr',
                    'value'       => $this->val[0]['nmaksgr'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ),
            'kdakun'=> array(
                    'attr'        => array(
                        'id'    => 'kdakun',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdakun'],
                    'value'       => $this->val[0]['kdakun'],
                    'name'     => 'kdakun',
                    'placeholder' => 'Pilih Akun',
                    'required' => ''
            ),
      		   'faktif'=> array(
                'placeholder' => 'Status Kelompok Aksesoris',
      					'id'          => 'faktif',
      					'value'       => 't',
      					'checked'     => $faktifx,
      					'class'       => 'custom-control-input',
      					'name'		  => 'faktif',
      					'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdaks){
        if(empty($kdaks)){
            redirect($this->data['add']);
        }

        $this->val= $this->mstaksgr_qry->select_data($kdaks);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdaks,$stat,$nmaks,$faktif) {
        if(!empty($kdaks) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kdaksgr',
                    'label' => 'Kode Kelompok Aksesoris',
                    'rules' => 'required|integer',
                ),
            array(
                    'field' => 'nmaksgr',
                    'label' => 'Nama Kelompok Aksesoris',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

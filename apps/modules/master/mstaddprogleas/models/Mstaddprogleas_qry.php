<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstaddprogleas_qry
 *
 * @author adi
 */
class Mstaddprogleas_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }

    public function select_data($kdprogleas) {
        $this->db->select("*");
        $this->db->where('kdprogleas',$kdprogleas);
        $query = $this->db->get('pzu.program_leasing');
        return $query->result_array();
    }

    public function getLeasing() {
        $this->db->select("kdleasing");
        $this->db->order_by('kdleasing');
        $query = $this->db->get('pzu.v_leasing');
        return $query->result_array();
    } 

    public function json_dgview() {
        error_reporting(-1);
        if( isset($_GET['kdprogleas']) ){
            $id = $_GET['kdprogleas'];
        }else{
            $id = '';
        }

        $aColumns = array('no',
                            'kdleasing',
                            'nmprogleas',
                            'ket',
                            'jp',
                            'matriks',
                            'total',
                            'status_ppnx',
                            'dpp_refund',
                            'ppn_refund',
                            'tunai_leasx',
                            'faktifx',
                            'kdprogleas');
	$sIndexColumn = "kdprogleas";

        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT ''::varchar as no, *, CASE WHEN tunai_leas=true THEN 'YA' ELSE 'TIDAK' END AS tunai_leasx FROM pzu.v_program_leasing ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
      {
          if($_GET['iDisplayStart']>0){
              $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                      intval( $_GET['iDisplayStart'] );
          }
      }

      $sOrder = "";
      if ( isset( $_GET['iSortCol_0'] ) )
      {
              $sOrder = " ORDER BY  ";
              for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
              {
                      if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                      {
                              $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                      ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                      }
              }

              $sOrder = substr_replace( $sOrder, "", -2 );
              if ( $sOrder == " ORDER BY" )
              {
                      $sOrder = "";
              }
      }
      $sWhere = "";

      if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
      {
  $sWhere = " Where (";
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    $sWhere .= "lower(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  }
  $sWhere = substr_replace( $sWhere, "", -3 );
  $sWhere .= ')';
      }

      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
          {
              if ( $sWhere == "" )
              {
                  $sWhere = " WHERE ";
              }
              else
              {
                  $sWhere .= " AND ";
              }
              //echo $sWhere."<br>";
              $sWhere .= "lower(".$aColumns[$i].")  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
          }
      }


      /*
       * SQL queries
       * QUERY YANG AKAN DITAMPILKAN
       */
      $sQuery = "
              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
              FROM   $sTable
              $sWhere
              $sOrder
              $sLimit
              ";

      //echo $sQuery;

      $rResult = $this->db->query( $sQuery);

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";    //SELECT FOUND_ROWS()

      $rResultFilterTotal = $this->db->query( $sQuery);
      $aResultFilterTotal = $rResultFilterTotal->result_array();
      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

      $sQuery = "
              SELECT COUNT(".$sIndexColumn.") AS jml
              FROM $sTable
              $sWhere";
      $rResultTotal = $this->db->query( $sQuery);
      $aResultTotal = $rResultTotal->result_array();
      $iTotal = $aResultTotal[0]['jml'];

      $output = array(
              "sEcho" => intval($_GET['sEcho']),
              "iTotalRecords" => $iTotal,
              "iTotalDisplayRecords" => $iFilteredTotal,
              "aaData" => array()
      );


      foreach ( $rResult->result_array() as $aRow )
      {
                foreach ($aRow as $key => $value) {
                    if(is_numeric($value)){
                        $aRow[$key] = (float) $value;
                    }else{
                        $aRow[$key] = $value;
                    }
                } 

                $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('mstaddprogleas/edit/'.$aRow['kdprogleas'])."\">Edit</a>";
                $aRow['hapus'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdprogleas']."');\">Hapus</button>";
                $output['aaData'][] = $aRow;
      }
      echo  json_encode( $output );
    } 

    public function submit() { 
        try {
            $array = $this->input->post();   
            $array['kdleasing']       = strtoupper($array['kdleasing']);
            $array['nmprogleas']      = strtoupper($array['nmprogleas']);
            $array['ket']             = strtoupper($array['ket']); 
            $array['jp']              = $array['jp']; 
            $array['matriks']         = $array['matriks']; 
            $array['status_ppn']      = strtoupper($array['status_ppn']); 
            $array['tunai_leas']      = strtoupper($array['tunai_leas']); 
            $array['faktif']          = 'true'; 

            $resl = $this->db->insert('program_leasing',$array);
            if( ! $resl){
                $err = $this->db->error();
                $this->res = " Error : ". $this->apps->err_code($err['message']);
                $this->state = "0";
            }else{
                $this->res = "Data Tersimpan";
                $this->state = "1";
            }
        }catch (Exception $e) {           
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array( 
            'state' => $this->state, 
            'msg' => $this->res,
            );
        // $this->session->set_flashdata('statsubmit', json_encode($arr));
        return json_encode($arr); 
    }   

    public function update() {
      $kdprogleas   = $this->input->post('kdprogleas');
      $kdleasing    = $this->input->post('kdleasing');
      $nmprogleas   = $this->input->post('nmprogleas');
      $ket          = $this->input->post('ket');
      $jp           = $this->input->post('jp');
      $matriks      = $this->input->post('matriks');
      $status_ppn   = $this->input->post('status_ppn');
      $tunai_leas   = $this->input->post('tunai_leas');
      $faktif       = $this->input->post('faktif');

      $q = $this->db->query("select title,msg,tipe from pzu.progleas_upd_w(" . $kdprogleas . "::integer,
                                                                          '" . $kdleasing . "'::varchar,
                                                                          '" . $nmprogleas . "'::varchar,
                                                                          " . $jp . "::numeric,
                                                                          " . $matriks . "::numeric,
                                                                          '" . $status_ppn . "'::varchar,
                                                                          '" . $ket . "'::varchar,
                                                                          '" . $tunai_leas . "'::boolean,
                                                                          '" . $faktif . "'::boolean,
                                                                          '".$this->session->userdata("username")."')");

      //echo $this->db->last_query();
      if($q->num_rows()>0){
          $res = $q->result_array();
      }else{
          $res = "";
      }

      return json_encode($res);
    }

    public function delete() {
        $kdprogleas = $this->input->post('kdprogleas');
        $q = $this->db->query("select title,msg,tipe from pzu.progleas_del_w('". $kdprogleas ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}

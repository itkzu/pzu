<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : 
 *  Email : 
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Btlso
 *
 * @author
 */

class Btlso extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('btlso/submit'),
            'add' => site_url('btlso/add'),
            'edit' => site_url('btlso/edit'),
            'reload' => site_url('btlso'),
        );
        $this->load->model('btlso_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function getData() {
        echo $this->btlso_qry->getData();
    } 

    public function prosesHead(){
        echo $this->btlso_qry->prosesHead();
    }

    /*
    public function submit() {
        if($this->validate() == TRUE){
            $res = $this->btlso_qry->submit();
            echo $res;
        }else{
            $this->_init_add();
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('main')
                ->build('index',$this->data);
        }
    }
    */
    
    private function _init_add(){
        $this->data['form'] = array(
           'noref'=> array(
                    'placeholder' => 'Nomor Direct Part',
                    'id'          => 'noref',
                    'name'        => 'noref',
                    'value'       => set_value('noref'),
                    'class'       => 'form-control',
                    //'style'       => 'margin-left: 5px;',
                    'required'    => '',
            ),
        );
    }
    
    /*
    private function validate() {
        $config = array(
            array(
                    'field' => 'nodo',
                    'label' => 'No. DO',
                    'rules' => 'required|max_length[10]',
                ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
    */
}

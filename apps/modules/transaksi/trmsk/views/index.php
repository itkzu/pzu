<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */
?>
<style type="text/css">
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }

    #myform .errors {
        color: red;
    }

    #modalform .errors {
        color: red;
    }

    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
            outline: none;
            border: none;
    }

    .radio {
            margin-top: 0px;
            margin-bottom: 0px;
    }

    .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 20px;
            margin-bottom: 5px;
            font-weight: bold;
            cursor: pointer;
    }
    .pass-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    } 
    #load{
        width: 80%;
        height: 80%;
        position: fixed;
        text-indent: 100%;
        background: #ebebeb url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>

<div id="load">Loading...</div> 
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">   
              <!-- form start -->
              <!-- <?php
                  $attributes = array(
                      'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data'
                    , 'data-validate' => 'parsley');
                  echo form_open($submit,$attributes);
              ?>            -->

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-xs-1">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                                </div>
                            </div>

                            <div class="col-xs-3">
                              <div class="box-header box-view">
                                <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()" > Ubah</button> 
                              </div>
                            </div>

                            <div class="col-xs-3"> 
                                <div class="box-header">
                                    <button type="button" class="btn btn-primary btn-imp">Import Assist</button> 
                                </div>
                            </div>

                            <!-- <div class="col-xs-2">
                                <div class="box-header box-view">
                                    <button type="button" class="btn btn-cetak"><i class="fa fa-print"></i> Cetak L/R</button>
                                 </div>
                            </div> -->

                            <!-- <div class="col-xs-3">
                                <div class="box-header form-group">
                                    <?php
                                        echo form_input($form['ket']);
                                        echo form_error('ket','<div class="note">','</div>');
                                    ?>
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link " id="list-tfa" name="list-tfa" value="0" data-toggle="tab" href="#unit" role="tab" aria-controls="unit" aria-selected="true">Daftar BBN yang belum terima Surat/Kelengkapan secara komplit</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="input-tfa" name="input-tfa" value="1" data-toggle="tab" href="#piutang" role="tab" aria-controls="piutang" aria-selected="false">Input Terima Surat/Kelengkapan Unit</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" id="unit" role="tabpanel" aria-labelledby="list-tfa">

                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered dataTable display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;">Nama Konsumen</th>
                                                    <th style="text-align: center;">Alamat Konsumen</th> 
                                                    <th style="text-align: center;">No. Mesin</th>
                                                    <th style="text-align: center;">Tipe Unit</th>  
                                                    <th style="text-align: center;">Nopol</th>
                                                    <th style="text-align: center;">Notis</th> 
                                                    <th style="text-align: center;">STNK</th> 
                                                    <th style="text-align: center;">TNKB</th> 
                                                    <th style="text-align: center;">BPKB</th> 
                                                    <th style="text-align: center;">No. DO</th> 
                                                    <th style="text-align: center;">Tgl. DO</th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 10px;text-align: center;">No.</th>
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th>  
                                                    <th style="text-align: center;"></th>
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="text-align: center;"></th> 
                                                    <th style="width: 10px;text-align: center;">Edit</th>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="piutang" role="tabpanel" aria-labelledby="input-tfa"> 

                                <div class="col-xs-12">
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['cari']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_dropdown($form['cari']['name'],$form['cari']['data'] ,$form['cari']['value'] ,$form['cari']['attr']);
                                                        echo form_error('cari','<div class="note">','</div>');
                                                        // echo form_label($form['nospk']['placeholder']);
                                                        // echo form_input($form['nospk']);
                                                        // echo form_error('nospk','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  


                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label> Surat/Kelengkapan Unit yang diterima </label>
                                                    <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['nosin']['placeholder']); ?> 
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['nosin']);
                                                        echo form_error('nosin','<div class="note">','</div>');
                                                    ?> 
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('JENIS'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('DITERIMA'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('DISERAHKAN'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div> 

                                                <div class="col-xs-5"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('INFORMASI TAMBAHAN'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-xs-5">
                                    <div class="row">
                                        <div class="col-xs-3"> 
                                            <div class="form-group">
                                                <?php echo form_label($form['nora']['placeholder']); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-8"> 
                                            <?php
                                                echo form_input($form['nora']);
                                                echo form_error('nora','<div class="note">','</div>');
                                            ?>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-7">
                                    <div class="row">
                                        <div class="col-xs-3"> 
                                            <div class="form-group">
                                                <?php echo form_label('NOTICE'); ?>
                                                <div style="border-top: 1px solid #ddd;"></div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">  
                                            <label>
                                                <?php
                                                    echo form_checkbox($form['trm_nts']);
                                                ?>
                                            </label> 
                                            <label class="tnya">Ya</label> 
                                            <label class="tnno">Tidak</label> 
                                            <div style="border-top: 1px solid #ddd;"></div>
                                        </div> 

                                        <div class="col-xs-2">  
                                            <label>
                                                <?php
                                                    echo form_checkbox($form['srh_nts']);
                                                ?>
                                            </label> 
                                            <label class="snya">Ya</label> 
                                            <label class="snno">Tidak</label> 
                                            <div style="border-top: 1px solid #ddd;"></div>
                                        </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>Nopol</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['nopol']);
                                                        echo form_error('nopol','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label($form['kdtipe']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8"> 
                                                    <?php
                                                        echo form_input($form['kdtipe']);
                                                        echo form_error('kdtipe','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('STNK'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['trm_stnk']);
                                                                ?>
                                                          </label>
                                                          <label class="tsya">Ya</label> 
                                                          <label class="tsno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['srh_stnk']);
                                                                ?>
                                                          </label> 
                                                          <label class="ssya">Ya</label> 
                                                          <label class="ssno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>No. Notis</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['nonotis']);
                                                        echo form_error('nonotis','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>      

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-md-5 col-lg-5">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <br>
                                                    <!-- <div style="border-top: 1px solid #ddd; height: 10px;"></div> -->
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('TNKB'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['trm_tnkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="ttya">Ya</label> 
                                                          <label class="ttno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['srh_tnkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="stya">Ya</label> 
                                                          <label class="stno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>No. STNK</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['nostnk']);
                                                        echo form_error('nostnk','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('Nama'); ?>
                                                        <!-- <?php echo form_label($form['nama']['placeholder']); ?> --> 
                                                    </div>
                                                </div>

                                                <div class="col-xs-9"> 
                                                    <?php
                                                        echo form_input($form['nama']);
                                                        echo form_error('nama','<div class="note">','</div>');
                                                    ?> 
                                                </div> 
                                            </div>
                                        </div> 

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('BPKB'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['trm_bpkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="tbya">Ya</label> 
                                                          <label class="tbno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2">  
                                                          <label>
                                                                <?php
                                                                    echo form_checkbox($form['srh_bpkb']);
                                                                ?>
                                                          </label> 
                                                          <label class="sbya">Ya</label> 
                                                          <label class="sbno">Tidak</label> 
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>Pajak</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['pajak']);
                                                        echo form_error('pajak','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>      

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('Alamat'); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-9"> 
                                                    <?php
                                                        echo form_input($form['alamat']);
                                                        echo form_error('alamat','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-7"> 
                                                    <div class="form-group">
                                                        
                                                    </div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>Tgl Penetapan</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['tglttp']);
                                                        echo form_error('tglttp','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>  

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                    </div>
                                                </div>

                                                <div class="col-xs-9"> 
                                                    <?php
                                                        echo form_input($form['kota']);
                                                        echo form_error('kota','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  

                                        <div class="col-xs-7">
                                            <div class="row">
                                                <div class="col-xs-7"> 
                                                    <div class="form-group">
                                                        
                                                    </div>
                                                </div> 

                                                <div class="col-xs-2"> 
                                                    <div class="form-group">
                                                        <?php echo form_label('<small>No. BPKB</small>'); ?>
                                                        <div style="border-top: 1px solid #ddd;"></div>
                                                    </div>                                                
                                                </div> 

                                                <div class="col-xs-3">  
                                                    <?php
                                                        echo form_input($form['nobpkb']);
                                                        echo form_error('nobpkb','<div class="note">','</div>');
                                                    ?>                                            
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>  

                                <div class="col-xs-12">
                                    <div class="row"> 
                                        <br>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['jnsbayar']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <?php
                                                        echo form_input($form['jnsbayar']);
                                                        echo form_error('jnsbayar','<div class="note">','</div>');
                                                    ?>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>    

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['nodo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <div class="form-group"> 
                                                        <?php
                                                            echo form_input($form['nodo']);
                                                            echo form_error('nodo','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>   

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['tgldo']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <div class="form-group"> 
                                                        <?php
                                                            echo form_input($form['tgldo']);
                                                            echo form_error('tgldo','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>    

                                <div class="col-xs-12">
                                    <div class="row"> 

                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="col-xs-3"> 
                                                    <div class="form-group"> 
                                                        <?php echo form_label($form['nmsales']['placeholder']); ?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-5"> 
                                                    <div class="form-group"> 
                                                        <?php
                                                            echo form_input($form['nmsales']);
                                                            echo form_error('nmsales','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>  
                                    </div>
                                </div>    

                            </div>
                        </div>
                    </div>
              </div>
          </div>
            <!-- /.box-body -->
        <?php echo form_close(); ?>
        </div>
        <!-- /.box -->

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#load").hide();
        upd_DOCH();
        reset();
        disabled();
        autonum();
        init_search();
        $('#nosin').mask('***-***-***-***')
        // cek();
        $('.btn-ubah').hide(); 

        $('#btn-refresh').show();   

        $('.btn-refresh').click(function(){
            table.ajax.reload();
        });

        $('#nosin').keyup(function(){
            var jml = $('#nosin').val();
            var nosin = jml.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("-","");
            var nosin = nosin.replace("_","");
            var n = nosin.length;
            
            if(n===12){
                // console.log(n);    
                set_nosin();   
            } 
        });

        $('#cari').change(function(){
            set_trmsk();
        });

        $('#input-tfa').click(function(){
            $('.btn-ubah').show();  
            $('.btn-ubah').attr("disabled",true);  
            $('.btn-refresh').hide();
        });

        $('.btn-imp').click(function () {
            var date7 = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-DD HH:mm:ss');
            var date1 = new Date(Date.now() + 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD HH:mm:ss'); 
            get_DOCH(rdate1,rdate7);
        });

        $('#list-tfa').click(function(){
            $('.btn-ubah').hide();   
            $('.btn-refresh').show();
        }); 
        table = $('.dataTable').DataTable({  
            // "aoColumnDefs": column,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mData" : "nama",
                    "mData" : "nodo",
                    "mRender": function (data, type, row) {
                        var btn = '<a href="trmsk/edit/' + row.nodo +'">' + row.nama + ' </a>';
                        return btn;
                    }
                },
                {
                    "aTargets": [ 6,7,8,9 ],
                    "sClass": "center"
                    
                },
                {
                    "aTargets": [ 11 ],
                    "mRender": function (data, type, full) {
                        return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
                    },
                    "sClass": "center"
                    
                }
                // {
                //     "aTargets": [ 6 ],
                //     "mRender": function (data, type, full) {
                //         return type === 'export' ? data : numeral(data).format('0,0.00');
                //     },
                //     "sClass": "right"
                    
                // } 
                ],
            // "aoColumnDefs": column, 
            "columns": [
                { "data": "no"},
                { "data": "nama" },
                { "data": "alamat" },
                { "data": "nosin"},
                { "data": "kdtipe" },
                { "data": "nopol"},
                { "data": "status_notis"},
                { "data": "status_stnk"},
                { "data": "status_tnkb"},
                { "data": "status_bpkb"},
                { "data": "nodo"},
                { "data": "tgldo"} ,
                { "data": "edit"}
            ],
            // "lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[5,10,25,50, 100,500,1000, -1], [5,10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                //{ "name": "periode_awal", "value": $("#periode_awal").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('trmsk/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',
            buttons: [ 
            ],
            // "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="No." ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });   
    });

    function autonum(){
        $('#pajak').autoNumeric('init'); 
    }

    function disabled(){
        $("#trm_nts").prop('disabled',true);
        $("#trm_stnk").prop('disabled',true);
        $("#trm_tnkb").prop('disabled',true);
        $("#trm_bpkb").prop('disabled',true);
        $("#srh_nts").prop('disabled',true);
        $("#srh_stnk").prop('disabled',true);
        $("#srh_tnkb").prop('disabled',true);
        $("#srh_bpkb").prop('disabled',true);
        cek();
    }

    function init_search(){
        $("#cari").select2({
            ajax: {
                url: "<?=site_url('trmsk/getDtsk');?>",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
          processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Masukkan No DO / No Mesin / Nama Konsumen ...',
            dropdownAutoWidth : true, 
            width: '100%',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 3,
                        templateResult: format_nama, // omitted for brevity, see the source of this page
                        templateSelection: format_nama_terpilih // omitted for brevity, see the source of this page
                    });
    }

    function format_nama_terpilih (repo) {
        return repo.full_name || repo.id;
    }

    function format_nama (repo) {
        if (repo.loading) return "Mencari data ... ";


        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'><b style='font-size: 14px;'>" + repo.nama + "</b></div>";

        markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.id +
        "</div>" +
        "<div class='select2-result-repository__forks'><i class=\"fa fa-book\"></i> " + repo.text +
        "</div>" +
        "</div>" +
        "</div>";
        return markup;
    }

    // DOCH
    function get_DOCH(rdate1,rdate7){ 
        $("#load").show();
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_DOCH'); ?>",
            data: { "rdate1" : rdate1 , "rdate7" : rdate7 }, 
            success: function (resp) {  
                ins_DOCH(resp); 
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_DOCH(resp){ 
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_DOCH'); ?>",
            data: {"info" : resp}, 
            success: function (resp) {
                save_DOCH(); 
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_DOCH(){   
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_DOCH'); ?>",
            data: {}, 
            success: function (resp) {
                $("#load").fadeOut(); 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    upd_DOCH();
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }   

    function upd_DOCH(){   
        var status = 'SERAH';
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/upd_DOCH'); ?>",
            data: {"status":status}, 
            success: function (resp) {
                // $("#load").fadeOut(); 
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    table.ajax.reload();
                });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }  

    function set_trmsk(){
        var cari = $('#cari').val();    
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmsk/set_trmsk");?>",
            data: {"cari":cari },
            success: function(resp){ 
                var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){
                        $('#nosin').val(data.nosin);
                        $('#nora').val(data.nora);
                        $('#kdtipe').val(data.kdtipe);
                        $('#nama').val(data.nama);
                        $('#alamat').val(data.alamat);
                        $('#kota').val(data.kota);
                        $('#jnsbayar').val(data.tk);
                        $('#nodo').val(data.nodo);
                        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                        $('#nmsales').val(data.nmsales); 

                        //cheklist
                        if(data.tgl_trm_notis===null){
                            $("#trm_nts").prop("checked",false);
                        } else {
                            $("#trm_nts").prop("checked",true);
                        } 
                        if(data.tgl_trm_stnk===null){
                            $("#trm_stnk").prop("checked",false);
                        } else {
                            $("#trm_stnk").prop("checked",true);
                        } 
                        if(data.tgl_trm_tnkb===null){
                            $("#trm_tnkb").prop("checked",false);
                        } else {
                            $("#trm_tnkb").prop("checked",true);
                        } 
                        if(data.tgl_trm_bpkb===null){
                            $("#trm_bpkb").prop("checked",false);
                        } else {
                            $("#trm_bpkb").prop("checked",true);
                        } 
                        if(data.tgl_srh_notis===null){
                            $("#srh_nts").prop("checked",false);
                        } else {
                            $("#srh_nts").prop("checked",true);
                        } 
                        if(data.tgl_srh_stnk===null){
                            $("#srh_stnk").prop("checked",false);
                        } else {
                            $("#srh_stnk").prop("checked",true);
                        } 
                        if(data.tgl_srh_tnkb===null){
                            $("#srh_tnkb").prop("checked",false);
                        } else {
                            $("#srh_tnkb").prop("checked",true);
                        } 
                        if(data.tgl_srh_bpkb===null){
                            $("#srh_bpkb").prop("checked",false);
                        } else {
                            $("#srh_bpkb").prop("checked",true);
                        } 

                        //detail
                        $('#nopol').val(data.nopol); 
                        $('#nonotis').val(data.nonotis); 
                        $('#nostnk').val(data.nostnk); 
                        $('#pajak').autoNumeric('set',data.pajak_bbn);
                        $("#tglttp").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgl_penetapan_notis)));
                        $('#nobpkb').val(data.nobpkb); 

                        $('.btn-ubah').prop('disabled',false);
                        cek();
                    });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                // refresh();
            }
        });
    } 

    function set_nosin(){
        var nosin = $('#nosin').val();    
        $.ajax({
            type: "POST",
            url: "<?=site_url("trmsk/set_nosin");?>",
            data: {"nosin":nosin },
            success: function(resp){ 
                var obj = jQuery.parseJSON(resp);
                    $.each(obj, function(key, data){
                        $('#nosin').val(data.nosin);
                        $('#nora').val(data.nora);
                        $('#kdtipe').val(data.kdtipe);
                        $('#nama').val(data.nama);
                        $('#alamat').val(data.alamat);
                        $('#kota').val(data.kota);
                        $('#jnsbayar').val(data.tk);
                        $('#nodo').val(data.nodo);
                        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgldo)));
                        $('#nmsales').val(data.nmsales); 

                        //cheklist
                        if(data.tgl_trm_notis===null){
                            $("#trm_nts").prop("checked",false);
                        } else {
                            $("#trm_nts").prop("checked",true);
                        } 
                        if(data.tgl_trm_stnk===null){
                            $("#trm_stnk").prop("checked",false);
                        } else {
                            $("#trm_stnk").prop("checked",true);
                        } 
                        if(data.tgl_trm_tnkb===null){
                            $("#trm_tnkb").prop("checked",false);
                        } else {
                            $("#trm_tnkb").prop("checked",true);
                        } 
                        if(data.tgl_trm_bpkb===null){
                            $("#trm_bpkb").prop("checked",false);
                        } else {
                            $("#trm_bpkb").prop("checked",true);
                        } 
                        if(data.tgl_srh_notis===null){
                            $("#srh_nts").prop("checked",false);
                        } else {
                            $("#srh_nts").prop("checked",true);
                        } 
                        if(data.tgl_srh_stnk===null){
                            $("#srh_stnk").prop("checked",false);
                        } else {
                            $("#srh_stnk").prop("checked",true);
                        } 
                        if(data.tgl_srh_tnkb===null){
                            $("#srh_tnkb").prop("checked",false);
                        } else {
                            $("#srh_tnkb").prop("checked",true);
                        } 
                        if(data.tgl_srh_bpkb===null){
                            $("#srh_bpkb").prop("checked",false);
                        } else {
                            $("#srh_bpkb").prop("checked",true);
                        } 

                        //detail
                        $('#nopol').val(data.nopol); 
                        $('#nonotis').val(data.nonotis); 
                        $('#nostnk').val(data.nostnk); 
                        $('#pajak').autoNumeric('set',data.pajak_bbn);
                        $("#tglttp").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgl_penetapan_notis)));
                        $('#nobpkb').val(data.nobpkb); 

                        $('.btn-ubah').prop('disabled',false);
                        cek();
                    });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                // refresh();
            }
        });
    } 

    function ubah(){
      var nodo = $('#nodo').val();
      // alert('trmsk/edit/'+nodo);
      window.location.href = 'trmsk/edit/'+nodo;
    } 

    function clear(){ 
        $('#nosin').val('');
        $('#nora').val('');
        $('#kdtipe').val('');
        $('#nama').val('');
        $('#alamat').val('');
        $('#kota').val('');
        $('#jnsbayar').val('');
        $('#nodo').val('');
        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#nmsales').val('');

        //checklist
        $("#trm_nts").prop("checked",false);
        $("#trm_stnk").prop("checked",false);
        $("#trm_tnkb").prop("checked",false);
        $("#trm_bpkb").prop("checked",false);
        $("#srh_nts").prop("checked",false);
        $("#srh_stnk").prop("checked",false);
        $("#srh_bpkb").prop("checked",false);
        $("#srh_tnkb").prop("checked",false);

        //det
        $('#nopol').val('');
        $('#nonotis').val('');
        $('#nostnk').val('');
        $('#pajak').val('');
        $("#tglttp").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#nobpkb').val('');
    }

    function reset(){
        $('#cari').val('');
        $('#nosin').val('');
        $('#nora').val('');
        $('#kdtipe').val('');
        $('#nama').val('');
        $('#alamat').val('');
        $('#kota').val('');
        $('#jnsbayar').val('');
        $('#nodo').val('');
        $("#tgldo").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#nmsales').val('');

        //checklist
        $("#trm_nts").prop("checked",false);
        $("#trm_stnk").prop("checked",false);
        $("#trm_tnkb").prop("checked",false);
        $("#trm_bpkb").prop("checked",false);
        $("#srh_nts").prop("checked",false);
        $("#srh_stnk").prop("checked",false);
        $("#srh_bpkb").prop("checked",false);
        $("#srh_tnkb").prop("checked",false);

        //det
        $('#nopol').val('');
        $('#nonotis').val('');
        $('#nostnk').val('');
        $('#pajak').val('');
        $("#tglttp").val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#nobpkb').val('');
    }


    function dateback(tgl) {
      var less = $("#jth_tmp").val();
      var new_date = moment(tgl, "DD-MM-YYYY").add('days', less);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var res = day + '-' + month + '-' + year;
        return res;
    }

    function cek(){
        if ($("#srh_nts").prop("checked")){ 
            $('.snya').show();
            $('.snno').hide();
            if ($("#trm_nts").prop("checked")){
                $('.tnya').show();
                $('.tnno').hide();
            } else { 
                $('.tnya').hide();
                $('.tnno').show();
            }  
        } else {  
            $('.snya').hide();
            $('.snno').show();
            if ($("#trm_nts").prop("checked")){
                $('.tnya').show();
                $('.tnno').hide();
            } else { 
                $('.tnya').hide();
                $('.tnno').show();
            }   
        }
        if ($("#srh_stnk").prop("checked")){ 
            $('.ssya').show();
            $('.ssno').hide();
            if ($("#trm_stnk").prop("checked")){ 
                $('.tsya').show();
                $('.tsno').hide();
            } else {  
                $('.tsya').hide();
                $('.tsno').show();
            } 
        } else {  
            $('.ssya').hide();
            $('.ssno').show();
            if ($("#trm_stnk").prop("checked")){ 
                $('.tsya').show();
                $('.tsno').hide();
            } else {  
                $('.tsya').hide();
                $('.tsno').show();
            }   
        }
        if ($("#srh_tnkb").prop("checked")){ 
            $('.stya').show();
            $('.stno').hide();
            if ($("#trm_tnkb").prop("checked")){
                $('.ttya').show();
                $('.ttno').hide();
            } else { 
                $('.ttya').hide();
                $('.ttno').show();
            }
        } else {  
            $('.stya').hide();
            $('.stno').show();
            if ($("#trm_tnkb").prop("checked")){
                $('.ttya').show();
                $('.ttno').hide();
            } else { 
                $('.ttya').hide();
                $('.ttno').show();
            } 
        }
        if ($("#srh_bpkb").prop("checked")){ 
            $('.sbya').show();
            $('.sbno').hide();
            if ($("#trm_bpkb").prop("checked")){ 
                $('.tbya').show();
                $('.tbno').hide();
            } else {  
                $('.tbya').hide();
                $('.tbno').show();
            } 
        } else {  
            $('.sbya').hide();
            $('.sbno').show();
            if ($("#trm_bpkb").prop("checked")){
                $('.tbya').show();
                $('.tbno').hide();
            } else { 
                $('.tbya').hide();
                $('.tbno').show();
            }
        }
    }   
</script>

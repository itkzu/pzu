<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-danger">
                <?php
                    $attributes = array(
            					'role=' => 'form'
            					, 'id' => 'form_add'
            					, 'name' => 'form_add'
            					, 'enctype' => 'multipart/form-data'
            					, 'target' => '_blank'
            					, 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                			<div class="box-body">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">

                                <div class="col-xs-4">
                                    <div class="form-group">
                            					<?php
                            						echo form_label('Pilih Ref Kas/Bank');
                            						echo form_dropdown( $form['kdkb']['name'],
                            											$form['kdkb']['data'] ,
                            											$form['kdkb']['value'] ,
                            											$form['kdkb']['attr']);
                            						echo form_error('kdkb','<div class="note">','</div>');
                            					?>
                                    </div>
                                </div>

                                <div class="col-xs-4">
                                    <div class="form-group">
                                      <?php
                                        echo form_label($form['tglkb']['placeholder']);
                            						echo form_input($form['tglkb']);
                            						echo form_error('tglkb','<div class="note">','</div>');
                            					?>
                                    </div>
                                </div>

                  				</div>
                				</div>

                          <div class="col-md-12 col-lg-12">
                              <div class="row">

                                  <div class="col-xs-4">
                                      <div class="form-group">
                              					<?php
                                          echo form_label('Pilih Tanggal Laporan');
                          								echo form_input($form['periode_awal']);
                          								echo form_error('periode_awal','<div class="note">','</div>');
                              					?>
                                      </div>
                                  </div>

                                  <div class="col-xs-4">
                                      <div class="form-group">
                                      </div>
                                  </div>

                    				</div>
                  				</div>
                  				<!-- /.box-body -->

                  				<div class="box-footer">
                            <hr>
                            <button type="submit" class="btn btn-primary btn-tampil" name="submit">Tampil</button> 
                  				</div>

          <?php echo form_close(); ?>
            </div>
          <!-- /.box-body -->
          </div>
        <!-- /.box -->

        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
      tampil();
      tglkb();
 
      // if()
        // $(".btn-submit").click(function(){
        //     // submit();
        // });

        $("#kdkb").change(function(){
            tglkb();
            // tampil();
        });
        $("#periode_awal").change(function(){
            tampil();
        }); 
    });

    function tampil(){ 
        var tglkb1 = $("#tglkb").val().substr(0,2);
        var tglkb2 = $("#tglkb").val().substr(3,2);
        var tglkb3 = $("#tglkb").val().substr(6,4);
        var tglclose = tglkb3+tglkb2+tglkb1; 
        var tglplh1 = $("#periode_awal").val().substr(0,2);
        var tglplh2 = $("#periode_awal").val().substr(3,2);
        var tglplh3 = $("#periode_awal").val().substr(6,4);
        var tglplh = tglplh3+tglplh2+tglplh1; 
        if (tglplh <= tglclose ){
          $(".btn-tampil").attr("disabled",false);
          // alert('false');
        } else {
          $(".btn-tampil").attr("disabled",true);
          // alert('true');
        } 
    }

    function tglkb(){
      var kdkb = $("#kdkb").val();

      $.ajax({
          type: "POST",
          url: "<?=site_url("rptkbbklhr/tglkb");?>",
          data: {"kdkb":kdkb },
          success: function(resp){
            var obj = JSON.parse(resp);
            $.each(obj, function(key, data){
                $("#tglkb").val($.datepicker.formatDate('dd-mm-yy', new Date(data.tglkb)));
                tampil();
            });
          },
          error:function(event, textStatus, errorThrown) {
              swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
          }
      });
    }

    // function submit(){
    //   var kdkb = $("#kdkb").val();
    //   var periode_awal = $("#periode_awal").val();
    //   var periode_akhir = $("#periode_akhir").val();
		//
    //   $.ajax({
    //       type: "POST",
    //       url: "<?=site_url("rptkbbklhr/submit");?>",
    //       data: {"kdkb":kdkb
    //               , "periode_awal":periode_awal
    //               , "periode_akhir":periode_akhir},
    //       success: function(resp){
    //         var obj = JSON.parse(resp);
    //         $.each(obj, function(key, data){
    //         });
    //       },
    //       error:function(event, textStatus, errorThrown) {
    //           swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
    //       }
    //   });
    // }
</script>

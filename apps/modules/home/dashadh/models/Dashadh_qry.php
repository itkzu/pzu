<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashadh_qry
 *
 * @author adi
 */ 
class Dashadh_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

    public function detail_prog_bbn() {
      //  $nourut = $this->input->post('nourut');
        $q = $this->db->query("SELECT nama, alamat, kel, kec, kota, nohp, kdtipe, nosin, nora, jnsbayarx, nmsales, nodo, tgldo
                                FROM pzu.vl_bbn_status WHERE tgl_aju_fa IS NULL");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function getProgBBNDetail() {
        error_reporting(-1);

        if( isset($_GET['nourut']) ){
            if($_GET['nourut']){
                $nourut = $_GET['nourut'];
            }else{
                $nourut = '';
            }
        }else{
            $nourut = '';
        }

        if($nourut==1){
          $sNoWhere = " WHERE tgl_aju_fa IS NULL ";
        } else if($nourut==2) {
          $sNoWhere = " WHERE tgl_aju_fa IS NOT NULL AND tgl_trm_fa IS NULL ";
        } else if($nourut==3) {
          $sNoWhere = " WHERE tgl_trm_fa IS NOT NULL AND tgl_kps IS NULL ";
        } else if($nourut==4) {
          $sNoWhere = " WHERE status_otr = 'N' AND tgl_kps IS NOT NULL AND tgl_aju_bbn IS NULL";
        } else if($nourut==5) {
          $sNoWhere = " WHERE tgl_aju_bbn IS NOT NULL AND tgl_trm_bbn IS NULL ";
        } else if($nourut==6) {
          $sNoWhere = " WHERE tgl_trm_bbn IS NOT NULL AND tgl_trm_notis IS NULL ";
        } else if($nourut==7) {
          $sNoWhere = " WHERE tgl_trm_bbn IS NOT NULL AND tgl_trm_stnk IS NULL ";
        } else if($nourut==8) {
          $sNoWhere = " WHERE tgl_trm_bbn IS NOT NULL AND tgl_trm_tnkb IS NULL ";
        } else if($nourut==9) {
          $sNoWhere = " WHERE tgl_trm_bbn IS NOT NULL AND tgl_trm_bpkb IS NULL ";
        } else if($nourut==10) {
          $sNoWhere = " WHERE tgl_trm_notis IS NOT NULL AND tgl_srh_notis IS NULL ORDER BY nodo ";
        } else if($nourut==11) {
          $sNoWhere = " WHERE tgl_trm_stnk IS NOT NULL AND tgl_srh_stnk IS NULL ORDER BY nodo ";
        } else if($nourut==12) {
          $sNoWhere = " WHERE tgl_trm_tnkb IS NOT NULL AND tgl_srh_tnkb IS NULL ORDER BY nodo ";
        } else if($nourut==13) {
          $sNoWhere = " WHERE tgl_trm_bpkb IS NOT NULL AND tgl_srh_bpkb IS NULL AND jnsbayarx != 'TUNAI' ORDER BY nodo ";
        }
        $aColumns = array('nama', 'alamat', 'kel', 'kec', 'kota', 'nohp', 'kdtipe', 'nosin', 'nora', 'jnsbayarx', 'nmsales',
                          'nodo', 'tgldo', 'tgl_aju_fa', 'nmsup', 'tgl_buat_fa',  'tgl_trm_fa', 'tgl_aju_bbn', 'tgl_trm_bbn',
                          'nopol', 'tgl_trm_notis', 'tgl_srh_notis', 'tgl_trm_stnk', 'tgl_srh_stnk', 'tgl_trm_tnkb', 'tgl_srh_tnkb',
                          'tgl_trm_bpkb', 'tgl_srh_bpkb', 'nobpkb');

        $sIndexColumn = "nama";
        $sLimit = "";

        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT  nama, alamat, kel, kec, kota, nohp, kdtipe, nosin, nora, jnsbayarx, nmsales, nodo, tgldo,
                              tgl_aju_fa, nmsup, tgl_buat_fa, tgl_trm_fa, tgl_aju_bbn, tgl_trm_bbn, nopol, tgl_trm_notis, tgl_srh_notis,
                              tgl_trm_stnk, tgl_srh_stnk, tgl_trm_tnkb, tgl_srh_tnkb, tgl_trm_bpkb, tgl_srh_bpkb, nobpkb
                      FROM pzu.vl_bbn_status ".$sNoWhere." ) AS a";


        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                                   intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
            {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                    {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                            {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                            }
                    }

                    $sOrder = substr_replace( $sOrder, "", -2 );
                    if ( $sOrder == " ORDER BY" )
                        {
                            $sOrder = "";
                        }
                    }
                    $sWhere = "";

                    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
                    {
              		       $sWhere = " AND (";
              		           for ( $i=0 ; $i<count($aColumns) ; $i++ )
              		             {
              			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
              		            }
              		$sWhere = substr_replace( $sWhere, "", -3 );
              		$sWhere .= ")";
                      }

                      for ( $i=0 ; $i<count($aColumns) ; $i++ )
                      {

                          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
                          {
                              if ( $sWhere == "" )
                              {
                                  $sWhere = " WHERE ";
                              }
                              else
                              {
                                  $sWhere .= " AND ";
                              }
                              //echo $sWhere."<br>";
                              $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                          }
                      }


                      /*
                       * SQL queries
                       */
                      $sQuery = "
                              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                              FROM   $sTable
                              $sWhere
                              $sOrder
                              $sLimit
                              ";

                      //echo $sQuery;

                      $rResult = $this->db->query( $sQuery);

                      $sQuery = "
                              SELECT COUNT(".$sIndexColumn.") AS jml
                              FROM $sTable
                              $sWhere";    //SELECT FOUND_ROWS()

                      $rResultFilterTotal = $this->db->query( $sQuery);
                      $aResultFilterTotal = $rResultFilterTotal->result_array();
                      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

                      $sQuery = "
                              SELECT COUNT(".$sIndexColumn.") AS jml
                              FROM $sTable
                              $sWhere";
                      $rResultTotal = $this->db->query( $sQuery);
                      $aResultTotal = $rResultTotal->result_array();
                      $iTotal = $aResultTotal[0]['jml'];

                      $output = array(
                              "sEcho" => intval($_GET['sEcho']),
                              "iTotalRecords" => $iTotal,
                              "iTotalDisplayRecords" => $iFilteredTotal,
                              "aaData" => array()
                      );
                      $no = 1;
                      foreach ( $rResult->result_array() as $aRow )
                      {
                              $row = array();
                              for ( $i=0 ; $i<count($aColumns) ; $i++ )
                              {
                                  if($aRow[ $aColumns[$i] ]===null){
                                      $aRow[ $aColumns[$i] ] = '-';
                                  }
                                  if(is_numeric($aRow[ $aColumns[$i] ])){
                                      $row[] = (float) $aRow[ $aColumns[$i] ];
                                  }else{
                                      $row[] = $aRow[ $aColumns[$i] ];
                                  }
                              }
                              //23 - 28
                             $output['aaData'][] = $row;
                             $no++;
              	}
              	echo  json_encode( $output );

    }

    public function getProgBBNDetail_leas() {
        error_reporting(-1);


        $aColumns = array('nama', 'alamat', 'kel', 'kec', 'kota', 'nohp', 'kdtipe', 'nosin', 'nora', 'jnsbayarx', 'nmsales',
                          'nodo', 'tgldo', 'tgl_aju_fa', 'nmsup', 'tgl_buat_fa',  'tgl_trm_fa', 'tgl_aju_bbn', 'tgl_trm_bbn',
                          'nopol', 'tgl_trm_notis', 'tgl_srh_notis', 'tgl_trm_stnk', 'tgl_srh_stnk', 'tgl_trm_tnkb', 'tgl_srh_tnkb',
                          'tgl_trm_bpkb', 'tgl_srh_bpkb', 'nobpkb');

        $sIndexColumn = "nama";
        $sLimit = "";

        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT  nama, alamat, kel, kec, kota, nohp, kdtipe, nosin, nora, jnsbayarx, nmsales, nodo, tgldo,
                              tgl_aju_fa, nmsup, tgl_buat_fa, tgl_trm_fa, tgl_aju_bbn, tgl_trm_bbn, nopol, tgl_trm_notis, tgl_srh_notis,
                              tgl_trm_stnk, tgl_srh_stnk, tgl_trm_tnkb, tgl_srh_tnkb, tgl_trm_bpkb, tgl_srh_bpkb, nobpkb
                      FROM pzu.vl_bbn_status WHERE tgl_trm_bpkb IS NOT NULL AND tgl_srh_bpkb IS NULL AND jnsbayarx = 'TUNAI' ORDER BY nodo ) AS a";


        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                                   intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
            {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                    {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                            {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                            }
                    }

                    $sOrder = substr_replace( $sOrder, "", -2 );
                    if ( $sOrder == " ORDER BY" )
                        {
                            $sOrder = "";
                        }
                    }
                    $sWhere = "";

                    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
                    {
              		       $sWhere = " AND (";
              		           for ( $i=0 ; $i<count($aColumns) ; $i++ )
              		             {
              			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
              		            }
              		$sWhere = substr_replace( $sWhere, "", -3 );
              		$sWhere .= ")";
                      }

                      for ( $i=0 ; $i<count($aColumns) ; $i++ )
                      {

                          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
                          {
                              if ( $sWhere == "" )
                              {
                                  $sWhere = " WHERE ";
                              }
                              else
                              {
                                  $sWhere .= " AND ";
                              }
                              //echo $sWhere."<br>";
                              $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                          }
                      }


                      /*
                       * SQL queries
                       */
                      $sQuery = "
                              SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                              FROM   $sTable
                              $sWhere
                              $sOrder
                              $sLimit
                              ";

                      //echo $sQuery;

                      $rResult = $this->db->query( $sQuery);

                      $sQuery = "
                              SELECT COUNT(".$sIndexColumn.") AS jml
                              FROM $sTable
                              $sWhere";    //SELECT FOUND_ROWS()

                      $rResultFilterTotal = $this->db->query( $sQuery);
                      $aResultFilterTotal = $rResultFilterTotal->result_array();
                      $iFilteredTotal = $aResultFilterTotal[0]['jml'];

                      $sQuery = "
                              SELECT COUNT(".$sIndexColumn.") AS jml
                              FROM $sTable
                              $sWhere";
                      $rResultTotal = $this->db->query( $sQuery);
                      $aResultTotal = $rResultTotal->result_array();
                      $iTotal = $aResultTotal[0]['jml'];

                      $output = array(
                              "sEcho" => intval($_GET['sEcho']),
                              "iTotalRecords" => $iTotal,
                              "iTotalDisplayRecords" => $iFilteredTotal,
                              "aaData" => array()
                      );
                      $no = 1;
                      foreach ( $rResult->result_array() as $aRow )
                      {
                              $row = array();
                              for ( $i=0 ; $i<count($aColumns) ; $i++ )
                              {
                                  if($aRow[ $aColumns[$i] ]===null){
                                      $aRow[ $aColumns[$i] ] = '-';
                                  }
                                  if(is_numeric($aRow[ $aColumns[$i] ])){
                                      $row[] = (float) $aRow[ $aColumns[$i] ];
                                  }else{
                                      $row[] = $aRow[ $aColumns[$i] ];
                                  }
                              }
                              //23 - 28
                             $output['aaData'][] = $row;
                             $no++;
              	}
              	echo  json_encode( $output );

    }


    public function adh_json_trn_harian() {
        error_reporting(-1);
        $aColumns = array('no',
                            'periode',
                            'ket',
                            'qty',);
        $sIndexColumn = "ket";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() OVER () AS no,
                        periodex as periode,
                        ket,
                        qty
                      FROM pzu.v_dbadh_trx ) AS a";
    /*
        if( isset($_GET['periode_awal']) ){
            if($_GET['periode_awal']){
                $dt_awal = explode("-", $_GET['periode_awal']);
                $periode_awal = $dt_awal[1].$dt_awal[0];
            }else{
                $periode_awal = '';
            }
        }else{
            $periode_awal = '';
        }

        if( isset($_GET['periode_akhir']) ){
            if($_GET['periode_akhir']){
                $dt_akhir = explode("-", $_GET['periode_akhir']);
                $periode_akhir = $dt_akhir[1].$dt_akhir[0];
            }else{
                $periode_akhir = '';
            }
        }else{
            $periode_akhir = '';
        }

        $aColumns = array('no',
                            'ket',
                            'qty',);
	   $sIndexColumn = "ket";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() OVER () AS no,
                        ket,
                        qty
                      FROM pzu.v_dbadh_trx
                      WHERE periode between '{$periode_awal}' AND '{$periode_akhir}') AS a";
        */

        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

//        echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        $no = 1;
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '-';
                    }
                    if(is_numeric($aRow[ $aColumns[$i] ])){
                        $row[] = (float) $aRow[ $aColumns[$i] ];
                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                //23 - 28
               $output['aaData'][] = $row;
               $no++;
	}
	echo  json_encode( $output );
    }

    public function adh_json_trn_kas_bank() {
        error_reporting(-1);
        $aColumns = array('no',
                            'nmkb',
                            'tglkb',);
	$sIndexColumn = "nmkb";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT row_number() OVER () AS no,
                        nmkb,
                        tglkb
                      FROM pzu.v_dbadh_kb ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

//        echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        $no = 1;
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '-';
                    }
                    if(is_numeric($aRow[ $aColumns[$i] ])){
                        $row[] = (float) $aRow[ $aColumns[$i] ];
                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                //23 - 28
               $output['aaData'][] = $row;
               $no++;
	}
	echo  json_encode( $output );
    }

    public function adh_json_trn_prog_bbn() {
        error_reporting(-1);
        $aColumns = array('nourut',
                            'ket',
                            'qty',);
	$sIndexColumn = "ket";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " (SELECT nourut,
                        ket,
                        qty
                      FROM pzu.v_dbadh_bbn ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {

            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //echo $sWhere."<br>";
                $sWhere .= "lower(".$aColumns[$i]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

//        echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        $no = 1;
        foreach ( $rResult->result_array() as $aRow )
        {
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    if($aRow[ $aColumns[$i] ]===null){
                        $aRow[ $aColumns[$i] ] = '-';
                    }
                    if(is_numeric($aRow[ $aColumns[$i] ])){
                        $row[] = (float) $aRow[ $aColumns[$i] ];
                    }else{
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                //23 - 28
               $output['aaData'][] = $row;
               $no++;
	}
	echo  json_encode( $output );
    }

    public function get_blm_trm_po_leasing() {
        try {
            $str = "select kdleasing
                                , count(nodo)
                        from pzu.vm_trm_po_leasing
                                where tgltrmpo is null
                        group by kdleasing
                        order by kdleasing";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = $query->result_array();
                return $res;
            }else{
                return null;
            }
        } catch (Exception $e) {
            return 0;
        }
    }

    public function get_blm_trm_po_leasing_detail() {
        $kdleasing = $this->input->post('kdleasing');
        try {
            $str = "select kdleasing
                    , nmprogleas
                    , pl
                    , nodo
                    , tgldo
                    , nama_s
                    , nmtipe
                    , nmwarna
                    from pzu.vm_trm_po_leasing
                    where tgltrmpo is null
                        and kdleasing = '{$kdleasing}'
                    order by nodo";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = $query->result_array();
                return $res;
            }else{
                return null;
            }
        } catch (Exception $e) {
            return 0;
        }
    }
}

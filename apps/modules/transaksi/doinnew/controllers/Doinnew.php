<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Doinnew
 *
 * @author adi
 */
class Doinnew extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('doinnew/submit'),
            'add' => site_url('doinnew/add'),
            'edit' => site_url('doinnew/edit'),
            'reload' => site_url('doinnew'),
        );
        $this->load->model('doinnew_qry'); 
        $cabang = $this->doinnew_qry->getDataCabang();
        foreach ($cabang as $value) {
            $this->data['kdsup'][$value['kdsup']] = $value['nmsup'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    } 

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function GetNoID() {
        echo $this->doinnew_qry->GetNoID();
    }
 
    public function GetNoID_idx() {
        echo $this->doinnew_qry->GetNoID_idx();
    }

    public function setnodoin() {
        echo $this->doinnew_qry->setnodoin();
    }

    public function setnosin() {
        echo $this->doinnew_qry->setnosin();
    }

    public function setnoid() {
        echo $this->doinnew_qry->setnoid();
    }

    public function add_doin() {
        echo $this->doinnew_qry->add_doin();
    }

    private function _init_add(){

        if(isset($_POST['finden']) && strtoupper($_POST['finden']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'nodoin'=> array(
                    'placeholder' => 'No. DO Intern',
                    //'type'        => 'hidden',
                    'id'          => 'nodoin',
                    'name'        => 'nodoin',
                    'value'       => set_value('nodoin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
            ),
           'tgldoin'=> array(
                    'placeholder' => 'Tanggal DO Intern',
                    'id'          => 'tgldoin',
                    'name'        => 'tgldoin',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
           'nmcabang'=> array(
                    // 'id'          => 'kota2',
                    // 'class'       => 'form-control',
                    'attr'        => array(
                        'id'          => 'nmcabang',
                        'class'       => 'form-control',
                    ), 
                    'data'     => $this->data['kdsup'], 
                    'placeholder' => 'Cabang PZU',
                    'value'       => set_value('nmcabang'),
                    'name'        => 'nmcabang',
                    'style'       => 'text-transform: uppercase;',
            ),
           'nosin'=> array(
                    'placeholder' => 'No. Mesin / Rk', 
                    'id'          => 'nosin',
                    'name'        => 'nosin',
                    'value'       => set_value('nosin'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => ''
            ), 
           'nora'=> array(
                    'placeholder' => '', 
                    'id'          => 'nora',
                    'name'        => 'nora',
                    'value'       => set_value('nora'),
                    'class'       => 'form-control',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ), 
            'nopo'=> array(
                     'placeholder' => 'Faktur',
                     'id'          => 'nopo',
                     'name'        => 'nopo',
                     'value'       => set_value('nopo'),
                     'class'       => 'form-control',
                     'style'       => '',
                     'readonly'    => '',
            ),
            'kdtipe'=> array(
                    'placeholder' => 'Tipe Unit',
                    'value'       => set_value('kdtipe'),
                    'id'          => 'kdtipe',
                    'name'        => 'kdtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'nmtipe'=> array(
                    'placeholder' => '',
                    'value'       => set_value('nmtipe'),
                    'id'          => 'nmtipe',
                    'name'        => 'nmtipe',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'tglpo'=> array(
                    'placeholder' => 'Tgl Faktur',
                    'id'          => 'tglpo',
                    'name'        => 'tglpo',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '', 
                     'readonly'    => '',
            ),
            'warna'=> array(
                    'placeholder' => 'Warna / Tahun',
                    'value'       => set_value('warna'),
                    'id'          => 'warna',
                    'name'        => 'warna',
                    'class'       => 'form-control',
                    // 'type'        => 'hidden',
                    'style'       => 'text-transform: uppercase;',
                     'readonly'    => '',
            ),
            'tahun'=> array(
                    'placeholder' => '',
                    'value'       => set_value('tahun'),
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'hrgjual'=> array(
                    'placeholder' => 'Harga Jual (DPP)',
                    // 'type'        =>  'hidden',
                    'value'       => set_value('hrgjual'),
                    'id'          => 'hrgjual',
                    'name'        => 'hrgjual',
                    'class'       => 'form-control',
                    'style'       => '',
                     'readonly'    => '',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'value'       => set_value('ket'),
                    'id'          => 'ket',
                    'name'        => 'ket',
                    'class'       => 'form-control', 
                    // 'readonly'    => '',
                    'style'       => 'text-transform: uppercase;',
                    'required'    => ''
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $no = $this->uri->segment(3);
            $nodf = str_replace('-','/',$no);
        }
        $this->_check_id($nodf);
        $this->data['form'] = array(   
        );
    }

    private function _check_id($nodf){
        if(empty($nodf)){
            redirect($this->data['add']);
        }

        $this->val= $this->doinnew_qry->select_data($nodf);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($nodf,$stat) {
        if(!empty($nodf) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'ket',
                    'label' => 'Catatan',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'tgldf',
                    'label' => 'Tanggal DF',
                    'rules' => 'required',
                    ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}

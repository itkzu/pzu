  <?php

  /* 
   * ***************************************************************
   * Script : html.php
   * Version : 
   * Date : Oct 17, 2017 10:32:23 AM
   * Author : Pudyasto Adi W.
   * Email : mr.pudyasto@gmail.com
   * Description : 
   * ***************************************************************
   */
  ?>
  <style> 
      caption {
          padding-top: 1px;
          padding-bottom: 1px;
          color: #2c2c2c;
          text-align: center;
      }
      body{
          overflow-x: auto; 
      } 

      table.center {
        margin-left: auto; 
        margin-right: auto;
      }
  </style>
  <?php 

        function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
        $temp = "";
        if ($nilai < 12) {
          $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
          $temp = penyebut($nilai - 10). " BELAS";
        } else if ($nilai < 100) {
          $temp = penyebut($nilai/10)." PULUH". penyebut($nilai % 10);
        } else if ($nilai < 200) {
          $temp = " SERATUS" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
          $temp = penyebut($nilai/100) . " RATUS" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
          $temp = " SERIBU" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
          $temp = penyebut($nilai/1000) . " RIBU" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
          $temp = penyebut($nilai/1000000) . " JUTA" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
          $temp = penyebut($nilai/1000000000) . " MILIAR" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
          $temp = penyebut($nilai/1000000000000) . " TRILIUN" . penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
      } 

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 3800);
  $this->load->library('table'); 
  $namaheader = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 18
                          , 'style' => 'text-align: center; width: 72%; font-size: 8px;'), 
      array('data' => '<i><b>Laporan Laba Rugi Unit versi 2 - 01/02/2016</b></i>'
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 28%; font-size: 8px;'),  
  );
  // Caption text
  // $this->table->set_caption($caption);
  $this->table->add_row($namaheader);   
  $col1 = array(
      array('data' => 'NO. DO'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['nodo']
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => 'Customer'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'),
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['nama']
                          , 'colspan' => 15
                          , 'style' => 'text-align: left; width: 60%; font-size: 11px;'), 
  );
  $col2 = array(
      array('data' => 'Tgl'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'.date_format(date_create($data[0]['tgldo']),"d/m/Y")
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => 'Type'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'),
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['kode']." - ".$data[0]['kdtipe']." - ".$data[0]['nmtipe']
                          , 'colspan' => 15
                          , 'style' => 'text-align: left; width: 60%; font-size: 11px;'), 
  );
  $col3 = array(
      array('data' => 'Sales'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['nmsales']
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => 'Leasing'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'),
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['nmleasing']
                          , 'colspan' => 15
                          , 'style' => 'text-align: left; width: 60%; font-size: 11px;'),
  );
  $col4 = array(
      array('data' => 'Supervisor'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['nmsales_header']
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => 'Program'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'),
      array('data' => ':&nbsp;&nbsp;&nbsp;'.$data[0]['nmprogleas']
                          , 'colspan' => 15
                          , 'style' => 'text-align: left; width: 60%; font-size: 11px;'),  
  );
  $col5 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
    );
  $col6 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Harga OTR'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => number_format($data[0]['harga_otr'],2,",",".").'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'), 
  );
  if($data[0]['disc']>0){
    $disc = number_format($data[0]['disc'],2,",",".");
  } else {
    $disc = " - ";
  }

  $col7 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Discount Unit'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $disc.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'), 
  );

  if($data[0]['sub_dealer']>0){
    $subdealer = number_format($data[0]['sub_dealer'],2,",",".");
  } else {
    $subdealer = " - ";
  }
  $col8 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi Dealer'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $subdealer.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),    
  );

  if($data[0]['sub_md']>0){
    $submd = number_format($data[0]['sub_md'],2,",",".");
  } else {
    $submd = " - ";
  }
  $col9 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi HSO/MD'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $submd.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'), 
  );

  if($data[0]['sub_md']>0){
    $submd = number_format($data[0]['sub_ahm'],2,",",".");
  } else {
    $submd = " - ";
  }
  $col10 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi AHM'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $submd.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'), 
  );

  if($data[0]['sub_md']>0){
    $subfincoy = number_format($data[0]['sub_fincoy'],2,",",".");
  } else {
    $subfincoy = " - ";
  }
  $col11 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi Finance Company'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $subfincoy.'&nbsp;'
                          , 'colspan' => 5 
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'), 
  );
  $this->table->add_row($col1);
  $this->table->add_row($col2); 
  $this->table->add_row($col3); 
  $this->table->add_row($col4); 
  $this->table->add_row($col5); 
  $this->table->add_row($col6); 
  $this->table->add_row($col7); 
  $this->table->add_row($col8); 
  $this->table->add_row($col9); 
  $this->table->add_row($col10);  
  $this->table->add_row($col11);

  $template = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template); 
  echo $this->table->generate();  

  $p1 = array(    
      array('data' => ''
                          , 'colspan' => 18
                          , 'style' => 'text-align: right; width: 72%; font-size: 1px;'),  
      array('data' => ''
                          , 'colspan' => 7
                          , 'style' => 'text-align: right; width: 28%; font-size: 1px;'),  
  );
  $this->table->add_row($p1);

  $template2 = array(
          'table_open'            => '<table style="border-collapse: collapse;" align="right" width="72%" border="1" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template2); 
  echo $this->table->generate();     

  $col12 = array(  
      array('data' => '&nbsp;'
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 18
                          , 'style' => 'text-align: left; width: 72%; font-size: 1px;'),  
  );
  $hdd = $data[0]['harga_otr']-$data[0]['disc']-$data[0]['sub_dealer']-$data[0]['sub_md']-$data[0]['sub_ahm']-$data[0]['sub_fincoy'];
  
  $col13 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => 'HARGA DITERIMA DEALER'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
      array('data' => ':&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => number_format($hdd,2,",",".").'&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 8%; font-size: 11px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 28%; font-size: 11px;'), 
  ); 
  $asoidhsiod = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 1px;'),
    );
  
  if($data[0]['um']>0){
    $um=number_format($data[0]['um'],2,",",".");
  } else {
    $um=" - ";
  } 
  $col15 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Uang Muka Customer'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $um.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  
  if($data[0]['disc2']>0){
    $disc2=number_format($data[0]['disc2'],2,",",".");
  } else {
    $disc2=" - ";
  }
  $col16 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Discount Unit'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $disc2.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'), 
  );
  
  if($data[0]['sub_dealer2']>0){
    $subdealer2=number_format($data[0]['sub_dealer2'],2,",",".");
  } else {
    $subdealer2=" - ";
  }
  $col17 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi Dealer'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $subdealer2.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),
  );
  
  if($data[0]['sub_md2']>0){
    $sub_md2=number_format($data[0]['sub_md2'],2,",",".");
  } else {
    $sub_md2=" - ";
  }
  $col18 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi HSO/MD'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $sub_md2.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),
  );
  
  if($data[0]['sub_ahm2']>0){
    $sub_ahm2=number_format($data[0]['sub_ahm2'],2,",",".");
  } else {
    $sub_ahm2=" - ";
  }
  $col19 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi AHM'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $sub_ahm2.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),
  );
  
  if($data[0]['sub_fincoy2']>0){
    $sub_fincoy2=number_format($data[0]['sub_fincoy2'],2,",",".");
  } else {
    $sub_fincoy2=" - ";
  } 
  $um1 = $data[0]['um']-$data[0]['disc2']-$data[0]['sub_dealer2']-$data[0]['sub_md2']-$data[0]['sub_ahm2']-$data[0]['sub_fincoy2'];
  if($um1>0){
    $um2 = number_format($um1,2,",",".");
  } else {
    $um2 = "&nbsp;-&nbsp;";
  }
  $col20 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi Finance Company'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $sub_fincoy2.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => '(&nbsp;' 
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => $um2.'&nbsp;)'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 8%; font-size: 11px;'), 
      array('data' => ''
                          , 'colspan' => 7
                          , 'style' => 'text-align: left; width: 28%; font-size: 11px;'),
  );
  // $this->table->add_row('<hr>'); 
  $this->table->add_row($col12);  
  $this->table->add_row($col13);  
  $this->table->add_row($asoidhsiod);   
  $this->table->add_row($col15);  
  $this->table->add_row($col16);  
  $this->table->add_row($col17);  
  $this->table->add_row($col18);  
  $this->table->add_row($col19);  
  $this->table->add_row($col20);  
  $template3 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template3); 
  echo $this->table->generate();  
 
  $col21 = array(
      array('data' => ''
                          , 'colspan' => 18
                          , 'style' => 'text-align: right; width: 72%; font-size: 1px;'),  
  );   
  $this->table->add_row($col21);  

  $template4 = array(
          'table_open'            => '<table style="border-collapse: collapse;" align="right" width="72%" border="1" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template4); 
  echo $this->table->generate(); 
  $lunas = $hdd-$um1;       
  $col23 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => 'PELUNASAN'
                          , 'colspan' => 11
                          , 'style' => 'text-align: left; width: 44%; font-size: 11px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 4%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 12%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => number_format($lunas,2,",",".").'&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
  );

  $c1 = array(  
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 1px;'), 
  );
  $col24 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Harga Diterima Dealer'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => number_format($hdd,2,",",".").'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  $col25 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Cadangan BBN '
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;('
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => number_format($data[0]['bbn'],2,",",".").'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => ')&nbsp;'
                          , 'colspan' => 4
                          , 'style' => 'text-align: left; width: 16%; font-size: 11px;'), 
      array('data' => '1 / 1 '
                          , 'colspan' => 8
                          , 'style' => 'text-align: left; width: 32%; font-size: 11px;'), 
  );

  $this->table->add_row($col23);   
  $this->table->add_row($c1);
  $this->table->add_row($col24);
  $this->table->add_row($col25);
  $template5 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template5); 
  echo $this->table->generate();    
  
  $p2 = array(
      array('data' => ''
                          , 'colspan' => 18
                          , 'style' => 'text-align: right; width: 72%; font-size: 1px;'),  
  );   
  $this->table->add_row($p2);  
 
  $template6 = array(
          'table_open'            => '<table style="border-collapse: collapse;" align="center" width="40%" border="1"  cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template6); 
  echo $this->table->generate();    
  
  if($data[0]['tgldo']>'2022-03-31'){
    $hdd_dpp = round(($hdd - $data[0]['bbn'])/1.11);
  } else {
    $hdd_dpp = round(($hdd - $data[0]['bbn'])/1.1);
  }
                
  $col26 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Harga Diterima Dealer (DPP)'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => number_format($hdd_dpp,2,",",".").'&nbsp;' 
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),   
  ); 

  if($data[0]['sub_hso']>0){
      $sub_hso = number_format($data[0]['sub_hso'],2,",",".");
  } else {
      $sub_hso = "&nbsp;-&nbsp;";
  }
  
  $col27 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Subsidi HSO/MD & AHM'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $sub_hso.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),
  );

  if($data[0]['ins_leasing']>0){
    $ins_leasing = number_format($data[0]['ins_leasing'],2,",",".");
  } else {
    $ins_leasing = "&nbsp;-&nbsp;";
  }
  $col28 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Insentif Finance (DPP)'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $ins_leasing.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),
  );

  if($data[0]['ins_ai']>0){
    $ins_ai = number_format($data[0]['ins_ai'],2,",",".");
  } else {
    $ins_ai = "&nbsp;-&nbsp;";
  }
  $col29 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Insentif AI (Inc Owner)'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $ins_ai.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),
  );
  $this->table->add_row($col26);
  $this->table->add_row($col27);
  $this->table->add_row($col28);
  $this->table->add_row($col29);
  $template7 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );

  $this->table->set_template($template7); 
  echo $this->table->generate();  

  
  $p3 = array(
      array('data' => ''
                          , 'colspan' => 18
                          , 'style' => 'text-align: right; width: 72%; font-size: 1px;'),  
  );   
  $this->table->add_row($p3);   
  $template8 = array(
          'table_open'            => '<table style="border-collapse: collapse;" align="right" width="72%" border="1"  cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );

  $this->table->set_template($template8); 
  echo $this->table->generate();    
  $total = $hdd_dpp+$data[0]['sub_hso']+$data[0]['ins_leasing']+$data[0]['ins_ai']; 

  $col30 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 12%; font-size: 11px;'), 
      array('data' => 'TOTAL'
                          , 'colspan' => 11
                          , 'style' => 'text-align: left; width: 44%; font-size: 11px;'),  
      array('data' => ':&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => number_format($total,2,",",".").'&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 8%; font-size: 11px;'),  
      array('data' => '&nbsp;'
                          , 'colspan' => 7
                          , 'style' => 'text-align: right; width: 28%; font-size: 11px;'),  
  );
  $c2 = array(  
      array('data' => '&nbsp;'
                          , 'colspan' => 25
                          , 'style' => 'text-align: center; font-size: 1px;'), 
  );
  if($data[0]['harga_beli']>0){
    $hrgbeli = number_format($data[0]['harga_beli'],2,",",".");
  } else {
    $hrgbeli = "&nbsp;-&nbsp;";
                }
  $col31 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Harga Beli Unit (DPP)'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $hrgbeli.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  if($data[0]['jaket']>0){
    $jaket = number_format($data[0]['jaket'],2,",",".");
  } else {
    $jaket = "&nbsp;-&nbsp;";
  }
  $col32 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Jaket '
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $jaket.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  if($data[0]['ins_sales']>0){
    $ins_sales = number_format($data[0]['ins_sales'],2,",",".");
  } else {
    $ins_sales = "&nbsp;-&nbsp;";
  }
  $col33 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Insentif Sales '
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $ins_sales.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  if($data[0]['biops']>0){
    $biops = number_format($data[0]['biops'],2,",",".");
  } else {
    $biops = "&nbsp;-&nbsp;";
  }
  $col34 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Biops'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $biops.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  if($data[0]['komisi']>0){
    $komisi = number_format($data[0]['komisi'],2,",",".");
  } else {
    $komisi = "&nbsp;-&nbsp;";
  }
  $col35 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Komisi'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $komisi.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 12
                          , 'style' => 'text-align: left; width: 48%; font-size: 11px;'),  
  );
  if($data[0]['topcover']>0){
    $topcover = number_format($data[0]['topcover'],2,",",".");
  } else {
    $topcover = "&nbsp;-&nbsp;";
  }
  $hpp = $data[0]['harga_beli']+$data[0]['jaket']+$data[0]['ins_sales']+$data[0]['biops']+$data[0]['komisi']+$data[0]['topcover'];
  if($hpp>0){
    $hpp1 = number_format($hpp,2,",",".");
  } else {
    $hpp1 = "&nbsp;-&nbsp;";
  }
  $col36 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => 'Lain - lain'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
      array('data' => ':&nbsp;&nbsp;&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => $topcover.'&nbsp;'
                          , 'colspan' => 5
                          , 'style' => 'text-align: right; width: 20%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'),
      array('data' => '(&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'),
      array('data' => $hpp1.'&nbsp;&nbsp;&nbsp;)'
                          , 'colspan' => 2
                          , 'style' => 'text-align: right; width: 8%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 9
                          , 'style' => 'text-align: left; width: 36%; font-size: 11px;'),
  );
  $this->table->add_row($col30);
  $this->table->add_row($c2);
  $this->table->add_row($col31);
  $this->table->add_row($col32);
  $this->table->add_row($col33);
  $this->table->add_row($col34);
  $this->table->add_row($col35);  
  $this->table->add_row($col36); 
  $template9 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );

  $this->table->set_template($template9); 
  echo $this->table->generate();  
  $p4 = array(
      array('data' => ''
                          , 'colspan' => 18
                          , 'style' => 'text-align: right; width: 72%; font-size: 1px;'),  
  );   
  $this->table->add_row($p4);    
  $template10 = array(
          'table_open'            => '<table style="border-collapse: collapse;" align="right" width="72%" border="1" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );
  $this->table->set_template($template10); 
  echo $this->table->generate();  

  $lr_unit = $total - $hpp;
  
  $col37 = array(
      array('data' => '&nbsp;'
                          , 'colspan' => 2
                          , 'style' => 'text-align: left; width: 8%; font-size: 11px;'), 
      array('data' => 'LABA (RUGI) UNIT'
                          , 'colspan' => 11
                          , 'style' => 'text-align: left; width: 44%; font-size: 11px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: right; width: 4%; font-size: 11px;'),
      array('data' => '&nbsp;'
                          , 'colspan' => 3
                          , 'style' => 'text-align: left; width: 12%; font-size: 11px;'), 
      array('data' => ':'
                          , 'colspan' => 1
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;'
                          , 'colspan' => 1
                          , 'style' => 'text-align: left; width: 4%; font-size: 11px;'), 
      array('data' => number_format($lr_unit,2,",",".").'&nbsp;'
                          , 'colspan' => 6
                          , 'style' => 'text-align: left; width: 24%; font-size: 11px;'), 
  );
 
  $col99 = array(
      // array('data' => '&nbsp;'
      //                     , 'colspan' => 24
      //                     , 'style' => 'text-align: center; width: 100%; font-size: 11px;'),
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;'
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'), 
      array('data' => '&nbsp;' 
                          , 'style' => 'text-align: center; width: 4%; font-size: 11px;'),    
  );
  $this->table->add_row($col37);  
  // $this->table->add_row($col99); 
  $template11 = array(
          'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="0" cellspacing="1">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td>',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
  );

  $this->table->set_template($template11); 
  echo $this->table->generate();    
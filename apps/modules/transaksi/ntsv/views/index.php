<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author :
 * Email :
 * Description :
 * ***************************************************************
 */

?>
<style type="text/css">
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
    td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_open.png');?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?=base_url('assets/plugins/dtables/resource/details_close.png');?>') no-repeat center center;
    }
    #load{
        width: 100%;
        height: 100%;
        position: fixed;
        text-indent: 100%;
        background: #e0e0e0 url('assets/dist/img/load.gif') no-repeat center;
        z-index: 1;
        opacity: 0.6;
        background-size: 10%;
    }
    #spinner-div {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        text-align: center;
        background-color: rgba(255, 255, 255, 0.8);
        z-index: 2;
    }
</style>
<div id="load">Loading...</div> 
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-danger">
            <!--
            <div class="box-header with-border">
                <h3 class="box-title">{msg_main}</h3>
            </div>
            -->
            <!-- /.box-header -->

            <!-- form start -->
            <?php
                $attributes = array(
                    'role=' => 'form'
                    , 'id' => 'form_add'
                    , 'name' => 'form_add'
                    , 'enctype' => 'multipart/form-data');
                echo form_open($submit,$attributes);
            ?>

            <div class="box-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-md-1">
                              <div class="box-header box-view">
                                <button type="button" class="btn btn-primary btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                              </div>
                            </div>

                            <div class="col-md-3">
                              <div class="box-header box-view">
                                <button type="button" class="btn btn-primary btn-add"> Tambah</button>
                                <button type="button" class="btn btn-primary btn-ubah" onclick="ubah()"> Ubah</button>
                                <button type="button" class="btn btn-cetak"><i class="fa fa-print"></i> Cetak Nota</button>
                                <!-- <button type="button" class="btn btn-primary btn-hapus" disabled> Hapus</button> -->
                                <!-- <button type="button" class="btn btn-danger btn-batal">Batal</button> -->
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="box-header box-view">
                              </div>
                            </div>


                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-imp"> <i class="fa fa-download"></i> Tarik Assist </button>    
                                </div>
                            </div>

                            <!-- <div class="col-md-3">
                              <div class="form-group">
                                <?php
                                    echo form_input($form['ket']);
                                    echo form_error('ket','<div class="note">','</div>');
                                ?>
                              </div>
                            </div> -->
                        </div>
                    </div>


                    

                    <div class="col-md-12">
                        <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                    </div>




                    <!-- <div class="main-form" style="width: 900px;"> -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active">
                                    <a href="#tab_1_1" class="tab_sv" data-toggle="tab"> List Belum Ada Nota </a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" class="tab_in" data-toggle="tab"> Input Nota </a>
                                </li> 
                                <li>
                                    <a href="#tab_1_3" class="tab_h" data-toggle="tab"> List History Nota </a>
                                </li> 
                            </ul>

                            <div class="tab-content">

                                <!--tab_1_1-->
                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="row"> 

                                        <div class="col-md-12"> 
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered display nowrap dataTable" style="width: 100%;" class="display select">
                                                    <thead>
                                                        <tr> 
                                                            <th style="width: 1%;text-align: center;">No</th>
                                                            <th style="width: 15%;text-align: center;">No PKB</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Servis</th>
                                                            <th style="width: 10%;text-align: center;">No Polisi</th>
                                                            <th style="width: 10%;text-align: center;">No Mesin</th>
                                                            <th style="width: 15%;text-align: center;">No Rangka</th>
                                                            <th style="width: 20%;text-align: center;">Nama Pembawa</th>
                                                            <th style="width: 20%;text-align: center;">Nama Mekanik</th>                                                                                                                        
                                                            <th style="width: 9%;text-align: center;">Dealer ID</th> 
                                                            <th style="width: 1%;text-align: center;">Edit</th> 
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr> 
                                                            <th style="width: 1%;text-align: center;">No</th>
                                                            <th style="width: 15%;text-align: center;">No PKB</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Servis</th>
                                                            <th style="width: 10%;text-align: center;">No Polisi</th>
                                                            <th style="width: 10%;text-align: center;">No Mesin</th>
                                                            <th style="width: 15%;text-align: center;">No Rangka</th>
                                                            <th style="width: 20%;text-align: center;">Nama Pembawa</th>
                                                            <th style="width: 20%;text-align: center;">Nama Mekanik</th>                                                                                                                        
                                                            <th style="width: 9%;text-align: center;">Dealer ID</th> 
                                                            <th style="width: 1%;text-align: center;">Edit</th> 
                                                        </tr>
                                                    </tfoot>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>
                                </div> 

                                <!-- tab_1_2 -->
                                <div class="tab-pane" id="tab_1_2">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="row">

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['nopkb']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['nopkb']);
                                                            echo form_error('nopkb','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['tglpkb1']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['tglpkb1']);
                                                            echo form_error('tglpkb1','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['tglpkb2']);
                                                            echo form_error('tglpkb2','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <a class="btn btn-group btn-primary btn-search"><i class="fa fa-search" aria-hidden="true"></i> Cari</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row">

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['tglsv']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['tglsv']);
                                                            echo form_error('tglsv','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['nopol']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['nopol']);
                                                            echo form_error('nopol','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 

                                                <div class="col-md-2">
                                                    <?php echo form_label($form['nosin']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['nosin']);
                                                            echo form_error('nosin','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['nora']);
                                                            echo form_error('nora','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row">

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['nama']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['nama']);
                                                            echo form_error('nama','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['nama_s']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['nama_s']);
                                                            echo form_error('nama_s','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 

                                                <div class="col-md-2">
                                                    <?php echo form_label($form['alamat_s']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['alamat_s']);
                                                            echo form_error('alamat_s','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row">

                                                <div class="col-md-1">
                                                    <?php echo form_label($form['ketsv']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['ketsv']);
                                                            echo form_error('ketsv','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div>  

                                                <div class="col-md-2">
                                                    <?php echo form_label($form['solvesv']['placeholder']); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <?php
                                                            echo form_input($form['solvesv']);
                                                            echo form_error('solvesv','<div class="note">','</div>');
                                                        ?>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="col-md-12"> 
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered display nowrap detailTable" style="width: 100%;" class="display select">
                                                    <thead>
                                                        <tr> 
                                                            <th style="width: 1%;text-align: center;">No</th>
                                                            <th style="text-align: center;">Grup</th>
                                                            <th style="text-align: center;">Nama</th>
                                                            <th style="text-align: center;">Jenis</th>
                                                            <th style="text-align: center;">No Part</th>
                                                            <th style="text-align: center;">Kuantitas</th>
                                                            <th style="text-align: center;">Harga</th>
                                                            <th style="text-align: center;">Diskon</th> 
                                                            <th style="text-align: center;">Total</th>  
                                                        </tr>
                                                    </thead> 
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>  

                                    </div>
                                </div> 

                                <!--tab_1_3-->
                                <div class="tab-pane" id="tab_1_3">
                                    <div class="row"> 

                                        <div class="col-md-12"> 
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered display nowrap hTable" style="width: 100%;" class="display select">
                                                    <thead>
                                                        <tr> 
                                                            <th style="width: 1%;text-align: center;">No</th>
                                                            <th style="width: 15%;text-align: center;">No PKB</th>
                                                            <th style="width: 15%;text-align: center;">No Nota Servis</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Servis</th>
                                                            <th style="width: 10%;text-align: center;">No Polisi</th>
                                                            <th style="width: 10%;text-align: center;">No Mesin</th>
                                                            <th style="width: 15%;text-align: center;">No Rangka</th>
                                                            <th style="width: 20%;text-align: center;">Nama Pembawa</th>
                                                            <th style="width: 20%;text-align: center;">Nama Mekanik</th>                                                            
                                                            <th style="width: 9%;text-align: center;">Dealer ID</th>  
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr> 
                                                            <th style="width: 1%;text-align: center;">No</th>
                                                            <th style="width: 15%;text-align: center;">No PKB</th>
                                                            <th style="width: 15%;text-align: center;">No Nota Servis</th>
                                                            <th style="width: 10%;text-align: center;">Tgl Servis</th>
                                                            <th style="width: 10%;text-align: center;">No Polisi</th>
                                                            <th style="width: 10%;text-align: center;">No Mesin</th>
                                                            <th style="width: 15%;text-align: center;">No Rangka</th>
                                                            <th style="width: 20%;text-align: center;">Nama Pembawa</th>
                                                            <th style="width: 20%;text-align: center;">Nama Mekanik</th>                                                                                                                        
                                                            <th style="width: 9%;text-align: center;">Dealer ID</th>  
                                                        </tr>
                                                    </tfoot>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>
                                </div> 

                                <!--end tab-pane-->
                            </div>
                        </div>



                        
                    </div>
                </div>  

            <?php echo form_close(); ?>
        </div>
        <!-- /.box -->
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('.btn-ubah').hide();
        $('.btn-hapus').hide(); 
        $('.btn-cetak').hide(); 
        $('#btn-refresh').show();
        // $('#nopkb').mask('99999-PKB-9999999');
        $('#nosin').mask('***-***-***-***');
        $('#nora').mask('***-***-***-**-***-***');
        $("#load").hide();
        disabled();

        var date7 = new Date(Date.now() - 2*24*60*60*1000).toISOString().slice(0,10);
        var rdate7 = moment(date7).format('YYYY-MM-01 00:00:00');
        var date1 = new Date(Date.now() + 2*24*60*60*1000).toISOString().slice(0,10);
        var rdate1 = moment(date1).format('YYYY-MM-DD 23:00:00'); 
        var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
        var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
        var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
        var kddiv       = 'SHOWROOM'; 
        sv_set(secret_key,api_key,req_time,rdate1,rdate7,kddiv); 

        $('.btn-refresh').click(function() {
            table.ajax.reload();
        });

        $('.btn-cetak').click(function(){
            var nopkb = $('#nopkb').val().toUpperCase();
            window.open('/ntsv/ctk_nt/'+nopkb, '_blank');
        });

        $('.tab_sv').click(function(){
            $('.btn-ubah').hide();
            $('.btn-hapus').hide();
            $('.btn-cetak').hide(); 
            $('.btn-refresh').show();
            table.ajax.reload();
        });

        $('.tab_in').click(function(){
            $('.btn-ubah').show();
            $('.btn-hapus').show();
            $('.btn-cetak').show();
            $('.btn-ubah').attr("disabled",true);
            $('.btn-cetak').attr("disabled",true); 
            $('.btn-refresh').hide();
            $('#nopkb').val('');
            $('#tglsv').val('');
            $('#nopol').val('');
            $('#nosin').val('');
            $('#nora').val('');
            $('#nama').val('');
            $('#nama_s').val('');
            $('#alamat_s').val('');
            $('#ketsv').val('');
            $('#solvesv').val('');
            getTable_sv();
        });

        $('.btn-set').click(function(event){
            enabled();
            getkb();
        });

        $(".btn-reset").click(function(){
            //disabled();
            window.location.reload();
        });

        $("#fileinputKB").click(function(){
            $(".upload_kb").show();
        });
 

        //pkb 
        $('.btn-search').click(function () {  
            cek_data();
        });

        $('.btn-upload-sv').click(function () {   
            save_SV(); 
        });

        $('.tab_sv').click(function () {  
            getTable_sv();
        }); 

        //button
        $('.btn-imp').click(function(){ 

            var date7 = new Date(Date.now() - 1*24*60*60*1000).toISOString().slice(0,10);
            var rdate7 = moment(date7).format('YYYY-MM-01 00:00:00');
            var date1 = new Date(Date.now() + 2*24*60*60*1000).toISOString().slice(0,10);
            var rdate1 = moment(date1).format('YYYY-MM-DD 23:00:00'); 
            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
            var kddiv       = 'SHOWROOM'; 
             sv_set(secret_key,api_key,req_time,rdate1,rdate7,kddiv);            
        });
           
        table = $('.dataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "aoColumnDefs": [
                {
                    "aTargets": [0],
                    "orderable": false, 
                },
                {
                    "aTargets": [1],
                    "mData" : "noworkorder",
                    "mRender": function (data, type, row) {
                    var btn = '<a href="/pzu/ntsv/edit/' + data +'">' + data + ' </a>';
                    return btn;
                }
                }],
            "columns": [
                { "data": "no"},
                { "data": "noworkorder"},
                { "data": "tanggalservis" },
                { "data": "nopolisi"},
                { "data": "nomesin" },
                { "data": "norangka" },
                { "data": "namapembawa" },
                { "data": "nmmekanik" },                 
                { "data": "dealerid" },  
                { "data": "edit"},
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('ntsv/json_dgview');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                    .search( this.value )
                    .draw();
                //}
            });
        });

        //row number
        table.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        }); 

        h_table = $('.hTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "aoColumnDefs": [
                {
                    "aTargets": [0],
                    "orderable": false, 
                }, 
                {
                    "aTargets": [1],
                    "mData" : "nopkb",
                    "mRender": function (data, type, row) {
                      var btn = '<a href="ntsv/ctk_nt/' + data +'">' + data + ' </a>';
                      return btn;
                }
                }],
            "columns": [
                { "data": "no"},
                { "data": "nopkb"},
                { "data": "nomor_nota_servis"},
                { "data": "tgl_nota_servis" },
                { "data": "no_polisi"},
                { "data": "nomor_mesin" },
                { "data": "nomor_rangka" },
                { "data": "nama_pembawa" },
                { "data": "nama_mekanik" },
                { "data": "no_ahass" }, 
            ],
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('ntsv/json_dgview_history');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        });

        $('.hTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.hTable tfoot th').each( function () {
            var title = $('.hTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Hapus"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        });

        h_table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                that
                    .search( this.value )
                    .draw();
                //}
            });
        });

        //row number
        h_table.on( 'draw.dt', function () {
        var PageInfo = $('.hTable').DataTable().page.info();
                h_table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        }); 

        function disabled(){
            $(".upload_kb").hide();
            $("#kddiv").attr("disabled",false);
            $(".btn-set").show();
            $(".btn-reset").hide();
            $('.form-import').hide();
            //$("#fileKB").remove();
            //$("#fileBeli").remove();
            //$("#fileJual").remove();
            //$("#fileServis").remove();
        }

        function enabled(){
            $("#kddiv").attr("disabled",true);
            $(".btn-set").hide();
            $(".btn-reset").show();
            $('.form-import').show();
        }


    }); 

    function cek_data(){
        var nopkb = $('#nopkb').val().toUpperCase();
        $.ajax({
            type: "POST",
            url: "<?= site_url('ntsv/cekdata'); ?>",
            data: {"nopkb":nopkb}, 
            success: function (resp) {   
                // console.log(resp);
                var obj = JSON.parse(resp);
                if(resp==='false'){ 
                    var d1 = $('#tglpkb1').val().substring(0,2);
                    var m1 = $('#tglpkb1').val().substring(3,5);
                    var y1 = $('#tglpkb1').val().substring(6,10);
                    var d2 = $('#tglpkb2').val().substring(0,2);
                    var m2 = $('#tglpkb2').val().substring(3,5);
                    var y2 = $('#tglpkb2').val().substring(6,10);
                    var date7       = new Date(Date.now(y1+"-"+m1+"-"+d1) - 2*24*60*60*1000).toISOString().slice(0,10);
                    // var date7       = new Date(Date.now() - 19*24*60*60*1000).toISOString().slice(0,10);

                    var rdate7      = moment(date7).format('YYYY-MM-DD 00:00:00');
                    var date1       = new Date(Date.now(y2+"-"+m2+"-"+d2) + 2*24*60*60*1000).toISOString().slice(0,10);
                    // var date1       = new Date(Date.now() - 17*24*60*60*1000).toISOString().slice(0,10);

                    var rdate1      = moment(date1).format('YYYY-MM-DD 23:00:00');  
                    var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
                    var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
                    var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
                    var kddiv       = 'SHOWROOM';
                    var nopkb       = $('#nopkb').val().toUpperCase();
                    get_SV(secret_key,api_key,req_time,rdate1,rdate7,kddiv,nopkb);  
                } else {
                    $.each(obj, function(key, data){
                        // alert(data.nopkb);
                        if(data.nopkb===''||data.nopkb===null){
                            var d1 = $('#tglpkb1').val().substring(0,2);
                            var m1 = $('#tglpkb1').val().substring(3,5);
                            var y1 = $('#tglpkb1').val().substring(6,10);
                            var d2 = $('#tglpkb2').val().substring(0,2);
                            var m2 = $('#tglpkb2').val().substring(3,5);
                            var y2 = $('#tglpkb2').val().substring(6,10);
                            var date7       = new Date(Date.now(y1+"-"+m1+"-"+d1) - 2*24*60*60*1000).toISOString().slice(0,10);
                            // var date7       = new Date(Date.now() - 19*24*60*60*1000).toISOString().slice(0,10);

                            var rdate7      = moment(date7).format('YYYY-MM-DD 00:00:00');
                            var date1       = new Date(Date.now(y2+"-"+m2+"-"+d2) + 2*24*60*60*1000).toISOString().slice(0,10);
                            // var date1       = new Date(Date.now() - 17*24*60*60*1000).toISOString().slice(0,10);

                            var rdate1      = moment(date1).format('YYYY-MM-DD 23:00:00');  
                            var req_time    = Math.round(new Date(Date.now()).getTime() / 1000.0);
                            var secret_key  = 'dgi-secret-live:57C60455-231C-4429-8EB1-F7CB940887AA'; 
                            var api_key     = 'dgi-key-live:4D023875-C265-4CE6-A388-2676022816CE';
                            var kddiv       = 'SHOWROOM';
                            var nopkb       = $('#nopkb').val().toUpperCase();
                            get_SV(secret_key,api_key,req_time,rdate1,rdate7,kddiv,nopkb); 
                        } else {
                            set_NT();
                        }
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 

    }

    function ubah(){
        var nopkb = $('#nopkb').val().toUpperCase();
        window.location.href = '/pzu/ntsv/edit/'+nopkb;
    }

    //PKB
    function get_SV(secret_key,api_key,req_time,rdate1,rdate7,kddiv,nopkb){ 
        // var periode = $("#periode_awal").val();

        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SV'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7, "kddiv" : kddiv  , "nopkb" : nopkb },
            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                console.log(secret_key,api_key,req_time,rdate1,rdate7,kddiv,nopkb);
                ins_SV(resp);
                // console.log(jQuery.parseJSON(resp));
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function ins_SV(resp){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/ins_SV'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){ 
                    save_SV(); 
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function save_SV(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/save_SV'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    set_SV(); 
                    table.ajax.reload();
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }   

    function sv_set(secret_key,api_key,req_time,rdate1,rdate7,kddiv){ 
        // var periode = $("#periode_awal").val();
        var nopkb = '';
        // secret_key,api_key,req_time,rdate1,rdate7,kddiv,nopkb
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('msttrk_hso/get_SV'); ?>",
            data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7, "kddiv" : kddiv , "nopkb" : nopkb },
            // data: { "secret_key":secret_key , "api_key":api_key , "req_time":req_time , "rdate1" : rdate1 , "rdate7" : rdate7, "kddiv" : kddiv , "nopkb" : nopkb },

            // beforeSend: function () {
            //     trn_penjualan.processing( true );
            // },
            success: function (resp) {
                $("#load").fadeOut();
                console.log(secret_key,api_key,req_time,rdate1,rdate7,kddiv);
                ins_SV(resp);
                console.log(resp);
                // var obj = jQuery.parseJSON(resp);
                // trn_penjualan.clear().draw();
                // $.each(obj, function (key, data) {
                //     trn_penjualan.rows.add(data).draw();
                //     ;
                // });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function sv_ins(resp){  
        // var periode = $("#periode_awal").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url('ntsv/sv_ins'); ?>",
            data: {"info" : resp}, 
            success: function (resp) { 
                // console.log(resp);
                if (resp='success'){ 
                    sv_save(); 
                } else {
                    swal({
                        title: "Data Gagal Disimpan",
                        text: "Data Tidak Tersimpan",
                        type: "error"
                    });
                }
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    } 

    function sv_save(){ 
        // var periode = $("#periode_awal").val();
        $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('ntsv/sv_save'); ?>",
            data: {}, 
            success: function (resp) { 
                $("#load").fadeOut();
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){ 
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    }, function(){
                        table.ajax.reload();
                    });
                });
            },
            complete: function(){
                $('#load').hide();
            },
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }

    function set_SV(){ 
        var nopkb = $("#nopkb").val().toUpperCase();
        // $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('ntsv/set_SV'); ?>",
            data: {"nopkb":nopkb}, 
            success: function (resp) {   
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $('#tglsv').val(data.tanggalservis);
                    $('#nopol').val(data.nopolisi);
                    $('#nosin').val(data.nomesin);
                    $('#nora').val(data.norangka);
                    $('#nosin').mask('***-***-***-***');
                    $('#nora').mask('***-***-***-**-***-***');
                    $('#nama').val(data.namapembawa);
                    $('#nama_s').val(data.namapemilik);
                    $('#alamat_s').val(data.alamat_s);
                    $('#ketsv').val(data.keluhankonsumen);
                    $('#solvesv').val(data.saranmekanik);
                    if(data.nopkb===''){ 
                        $('.btn-ubah').attr("disabled",true);
                        $('.btn-cetak').attr("disabled",false); 
                    } else {
                        $('.btn-ubah').attr("disabled",false);
                        $('.btn-cetak').attr("disabled",true); 
                    }
                    getTable_sv();
                });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }   

    function set_NT(){ 
        var nopkb = $("#nopkb").val().toUpperCase();
        // $("#load").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('ntsv/set_NT'); ?>",
            data: {"nopkb":nopkb}, 
            success: function (resp) {   
                // console.log(resp);
                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    $('#tglsv').val($.datepicker.formatDate('dd-mm-yy', new Date(data.tgl_nota_servis)));
                    $('#nopol').val(data.no_polisi);
                    $('#nosin').val(data.nomor_mesin);
                    $('#nora').val(data.nomor_rangka);
                    $('#nosin').mask('***-***-***-***');
                    $('#nora').mask('***-***-***-**-***-***');
                    $('#nama').val(data.nama_pembawa);
                    $('#nama_s').val(data.nama_pemilik);
                    $('#alamat_s').val(data.alamat);
                    $('#ketsv').val('SUDAH CETAK NOTA');
                    $('#solvesv').val('SUDAH CETAK NOTA'); 
                    $('.btn-ubah').attr("disabled",true);
                    $('.btn-cetak').attr("disabled",false);  
                    getTable_sv();
                });
            }, 
            error: function (event, textStatus, errorThrown) {
                console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            } 
        }); 
    }    

    function getTable_sv(){ 
        var nopkb = $('#nopkb').val().toUpperCase();
        dettable = $('.detailTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "orderCellsTop": true,
            "fixedHeader": true,
            // "scrollY": 500,
            // "scrollX": true,
            // "aoColumnDefs": column,
            "aoColumnDefs": [
                {
                    "aTargets": [6,7,8],
                    "mRender": function (data, type, full) {
                        return type === 'export' ? data : numeral(data).format('0,0.00');
                    },
                    "sClass": "right"  
                }],
            "columns": [
                { "data": "no"},
                { "data": "jenis"},
                { "data": "namapekerjaan" },
                { "data": "jenispekerjaan"},
                { "data": "partsnumber" },
                { "data": "kuantitas" },
                { "data": "harga" },
                { "data": "diskon" }, 
                { "data": "totalharga"},
            ], 
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "nopkb", "value": nopkb }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            // "pagingType": "simple",
            "sAjaxSource": "<?=site_url('ntsv/json_dgview_detail');?>",
                "oLanguage": {
                    "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
                },
                //dom: '<"html5buttons"B>lTfgitp',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Please Wait ..."
            },
        });

        $('.detailTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });  

        //row number
        dettable.on( 'draw.dt', function () {
        var PageInfo = $('.dataTable').DataTable().page.info();
                dettable.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                });
        });
    }
</script>

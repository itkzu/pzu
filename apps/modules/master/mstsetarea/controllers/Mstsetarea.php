<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Mstsetarea
 *
 * @author adi
 */
class Mstsetarea extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('mstsetarea/submit'),
            'add' => site_url('mstsetarea/add'),
            'edit' => site_url('mstsetarea/edit'),
            'reload' => site_url('mstsetarea'),
        );
        $this->load->model('mstsetarea_qry');
        $kec = $this->mstsetarea_qry->getarea();
        foreach ($kec as $value) {
            $this->data['kec'][$value['kec_s']] = $value['kec_s'];
        }
        
        $kddiv = $this->mstsetarea_qry->getPos();
        if($kddiv){
            foreach ($kddiv as $value) {
                $this->data['kddiv'][$value['kddiv']] = $value['status'] . ' | ' . $value['nmdiv'];
            }
        }
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){  
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }
    
    public function json_dgview() {
        echo $this->mstsetarea_qry->json_dgview();
    }
    
    public function getnamaarea() {
        echo $this->mstsetarea_qry->getnamaarea();
    }
    
    public function submit() {  
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        
        if($this->validate($id,$stat) == TRUE){
            $res = $this->mstsetarea_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($id)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($id);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($id)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_check_id($id);
                $this->template->build('form', $this->data);
            }
        }
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'id',
                    'name'        => 'id',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kddiv'],
                    'value'    => '',
                    'name'     => 'kddiv',
            ),
           'kec'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kec',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
           'nama'=> array(
                    'placeholder' => 'Otomatis dari sistem',
                    'id'      => 'nama',
                    'name'        => 'nama',
                    'value'       => '',
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
            'area'=> array(
                    'attr'        => array(
                        'id'    => 'area',
                        'class' => 'form-control',
                        'multiple' => '',
                    ),
                    'data'     => $this->data['kec'],
                    'value'    => '',
                    'name'     => 'area[]',
                    'required'    => '',
            ),
        );
    }
    
    private function _init_edit(){
        $groupid = $this->uri->segment(3);
        $this->_check_id($groupid);
        $kddiv = $this->mstsetarea_qry->getPos();
        
        foreach ($kddiv as $value) {
            if($this->val[0]['kddiv']===$value['kddiv']){
                $this->data['kddivedit'][$value['kddiv']] = $value['status'] . ' | ' . $value['nmdiv'];
            }
        }        
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'id',
                    'name'        => 'id',
                    'value'       => $this->val[0]['id'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
            'kddiv'=> array(
                    'attr'        => array(
                        'id'    => 'kddiv',
                        'class' => 'form-control',
                    ),
                    'data'     => $this->data['kddivedit'],
                    'value'    => $this->val[0]['kddiv'],
                    'name'     => 'kddiv',
            ),
           'kec'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'ID',
                    'id'      => 'kec',
                    'value'       => $this->val[0]['area'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
            ),
           'nama'=> array(
                    'placeholder' => 'Otomatis dari sistem',
                    'id'      => 'nama',
                    'name'        => 'nama',
                    'value'       => $this->val[0]['nama'],
                    'class'       => 'form-control',
                    'readonly'    => ''  
                    
            ),
            'area'=> array(
                    'attr'        => array(
                        'id'    => 'area',
                        'class' => 'form-control',
                        'multiple' => '',
                    ),
                    'data'     => $this->data['kec'],
                    'value'    => '',
                    'name'     => 'area[]',
                    'required'    => '',
            ),
        );
    }
    
    private function _check_id($id){
        if(empty($id)){
            redirect($this->data['add']);
        }
        
        $this->val= $this->mstsetarea_qry->select_data($id);
        
        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }
    
    private function validate($id,$stat) {
        return true;
//        if(!empty($id) && !empty($stat)){
//            return true;
//        }
//        $config = array(
//            array(
//                    'field' => 'nama',
//                    'label' => 'Nama Area',
//                    'rules' => 'required|max_length[50]',
//                ),
//        );
//        
//        $this->form_validation->set_rules($config);   
//        if ($this->form_validation->run() == FALSE)
//        {
//            return false;
//        }else{
//            return true;
//        }
    }
}
